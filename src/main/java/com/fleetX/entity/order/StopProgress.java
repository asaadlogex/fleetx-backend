package com.fleetX.entity.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.dropdown.DStopType;
import com.fleetX.entity.dropdown.DVehicleOwnership;
import com.fleetX.entity.employee.Employee;

@Entity
@Table(name = "stop_progress")
@Where(clause = "is_deleted=false")
public class StopProgress implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stop_progress_id")
	private Integer stopProgressId;
	@ManyToOne
	@JoinColumn(name = "stop_type", referencedColumnName = "stop_type_name")
	private DStopType stopProgressType;
	@OneToOne
	@JoinColumn(name = "stop_id", referencedColumnName = "stop_id")
	private Stop stop;
	@ManyToOne
	@JoinColumn(name = "tractor_id", referencedColumnName = "tractor_id")
	private Tractor tractor;
	@ManyToOne
	@JoinColumn(name = "trailer_id", referencedColumnName = "trailer_id")
	private Trailer trailer;
	@ManyToOne
	@JoinColumn(name = "container_id", referencedColumnName = "container_id")
	private Container container;
	@ManyToOne
	@JoinColumn(name = "driver1_id", referencedColumnName = "employee_id")
	private Employee driver1;
	@ManyToOne
	@JoinColumn(name = "driver2_id", referencedColumnName = "employee_id")
	private Employee driver2;
	@ManyToOne
	@JoinColumn(name = "vehicle_ownership", referencedColumnName = "vehicle_ownership_name")
	private DVehicleOwnership vehicleOwnership;
	@ManyToOne
	@JoinColumn(name = "rental_vehicle_id", referencedColumnName = "rental_vehicle_id")
	private RentalVehicle rentalVehicle;
	@Column(name = "current_km")
	private Double currentKm;
	@Column(name = "arrival_time")
	private Date arrivalTime;
	@Column(name = "departure_time")
	private Date departureTime;
	@Column(name = "detention")
	private Double detentionInHrs;
	@Column(name = "instruction")
	private String instruction;
	@ManyToOne
	@JoinColumn(name = "warehouse_id", referencedColumnName = "warehouse_id")
	private Warehouse warehouse;
	@ManyToOne
	@JoinColumn(name = "rwb_id", referencedColumnName = "rwb_id")
	private RoadwayBill roadwayBill;
	@ManyToOne
	@JoinColumn(name = "job_id", referencedColumnName = "job_id")
	private Job job;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public DStopType getStopProgressType() {
		return stopProgressType;
	}

	public void setStopProgressType(DStopType stopProgressType) {
		this.stopProgressType = stopProgressType;
	}

	public Double getCurrentKm() {
		return currentKm;
	}

	public void setCurrentKm(Double currentKm) {
		this.currentKm = currentKm;
	}

	public Tractor getTractor() {
		return tractor;
	}

	public void setTractor(Tractor tractor) {
		this.tractor = tractor;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Employee getDriver1() {
		return driver1;
	}

	public void setDriver1(Employee driver1) {
		this.driver1 = driver1;
	}

	public Employee getDriver2() {
		return driver2;
	}

	public void setDriver2(Employee driver2) {
		this.driver2 = driver2;
	}

	public Integer getStopProgressId() {
		return stopProgressId;
	}

	public void setStopProgressId(Integer stopProgressId) {
		this.stopProgressId = stopProgressId;
	}

	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Double getDetentionInHrs() {
		return detentionInHrs;
	}

	public void setDetentionInHrs(Double detentionInHrs) {
		this.detentionInHrs = detentionInHrs;
	}

	public DVehicleOwnership getVehicleOwnership() {
		return vehicleOwnership;
	}

	public void setVehicleOwnership(DVehicleOwnership vehicleOwnership) {
		this.vehicleOwnership = vehicleOwnership;
	}

	public RentalVehicle getRentalVehicle() {
		return rentalVehicle;
	}

	public void setRentalVehicle(RentalVehicle rentalVehicle) {
		this.rentalVehicle = rentalVehicle;
	}

	public RoadwayBill getRoadwayBill() {
		return roadwayBill;
	}

	public void setRoadwayBill(RoadwayBill roadwayBill) {
		this.roadwayBill = roadwayBill;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

}
