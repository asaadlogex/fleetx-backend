package com.fleetX.entity.order;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.Route;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DCustomerType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DPaymentMode;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.dropdown.DVehicleOwnership;
import com.fleetX.entity.employee.Employee;

@Entity
@Table(name = "roadway_bill")
@Where(clause = "is_deleted=false")
public class RoadwayBill implements Serializable {
	@Id
	@GeneratedValue(generator = "rwb-generator")
	@GenericGenerator(name = "rwb-generator", parameters = { @Parameter(name = "prefix", value = "RWB"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "rwb_id")
	private String rwbId;
	@Column(name = "rwb_order_number")
	private String orderNumber;
	@Column(name = "rwb_load_number")
	private String loadNumber;
	@ManyToOne
	@JoinColumn(name = "customer_type", referencedColumnName = "customer_type_name")
	private DCustomerType customerType;
	@ManyToOne
	@JoinColumn(name = "tractor_id", referencedColumnName = "tractor_id")
	private Tractor tractor;
	@ManyToOne
	@JoinColumn(name = "trailer_id", referencedColumnName = "trailer_id")
	private Trailer trailer;
	@ManyToOne
	@JoinColumn(name = "container_id", referencedColumnName = "container_id")
	private Container container;
	@ManyToOne
	@JoinColumn(name = "tracker_id", referencedColumnName = "tracker_id")
	private Tracker tracker;
	@ManyToOne
	@JoinColumn(name = "driver1_id", referencedColumnName = "employee_id")
	private Employee driver1;
	@ManyToOne
	@JoinColumn(name = "driver2_id", referencedColumnName = "employee_id")
	private Employee driver2;
	@ManyToOne
	@JoinColumn(name = "equipment_type", referencedColumnName = "trailer_type_name")
	private DTrailerType equipmentType;
	@ManyToOne
	@JoinColumn(name = "invoice_status", referencedColumnName = "invoice_status_name")
	private DInvoiceStatus invoiceStatus;
	@ManyToOne
	@JoinColumn(name = "tax_state", referencedColumnName = "state_name")
	private DState taxState;
	@Column(name = "rwb_weight")
	private Double weightInTon;
	@Column(name = "rwb_total_km")
	private Double totalKm;
	@Column(name = "tms_km")
	private Double tmsKm;
	@Column(name = "tracker_km")
	private Double trackerKm;
	@Column(name = "rate_per_km")
	private Double ratePerKm;
	@Column(name = "rate_per_ton")
	private Double ratePerTon;
	@Column(name = "detention_rate")
	private Double detentionRate;
	@Column(name = "halting_rate")
	private Double haltingRate;
	@Column(name = "rate_status")
	private Boolean rateStatus = false;
	@ManyToOne
	@JoinColumn(name = "vehicle_ownership", referencedColumnName = "vehicle_ownership_name")
	private DVehicleOwnership vehicleOwnership;
	@ManyToOne
	@JoinColumn(name = "rental_vehicle_id", referencedColumnName = "rental_vehicle_id")
	private RentalVehicle rentalVehicle;
	@ManyToOne
	@JoinColumn(name = "rwb_status", referencedColumnName = "rwb_status_name")
	private DRWBStatus rwbStatus;
	@Column(name = "rwb_date")
	private Date rwbDate;
	@Column(name = "asset_combination_id")
	private String assetCombinationId;
	@Column(name = "rwb_bol_number")
	private String bolNumber;
	@ManyToOne
	@JoinColumn(name = "payment_mode", referencedColumnName = "payment_mode_name")
	private DPaymentMode paymentMode;
	@ManyToOne
	@JoinColumn(name = "company_id", referencedColumnName = "company_id")
	private Company customer; // when load is contractual
	@ManyToOne
	@JoinColumn(name = "broker_id", referencedColumnName = "company_id")
	private Company broker;
	@Column(name = "is_area")
	private Boolean isArea;
	@ManyToOne
	@JoinColumn(name = "city_id", referencedColumnName = "city_id")
	private DCity city;
	@ManyToOne
	@JoinColumn(name = "route_id", referencedColumnName = "route_id")
	private Route route;
	@Column(name = "rwb_instruction")
	private String instructions;
	@Column(name = "is_empty")
	private Boolean isEmpty;
	@ManyToOne
	@JoinColumn(name = "job_id", referencedColumnName = "job_id")
	private Job job;
	@OneToMany(mappedBy = "roadwayBill", cascade = CascadeType.REMOVE)
	private List<Stop> stops;
	@OneToMany(mappedBy = "roadwayBill", cascade = CascadeType.REMOVE)
	private List<Product> products;
	@OneToMany(mappedBy = "roadwayBill", cascade = CascadeType.REMOVE)
	private List<Income> incomes;
	@OneToMany(mappedBy = "roadwayBill", cascade = CascadeType.REMOVE)
	private List<Expense> expenses;
	@OneToMany(mappedBy = "roadwayBill", cascade = CascadeType.REMOVE)
	private List<StopProgress> stopProgresses;
	@Column(name = "pod_url")
	private String podUrl;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	// urls
	public String getRwbId() {
		return rwbId;
	}

	public DVehicleOwnership getVehicleOwnership() {
		return vehicleOwnership;
	}

	public Boolean getRateStatus() {
		return rateStatus;
	}

	public void setRateStatus(Boolean rateStatus) {
		this.rateStatus = rateStatus;
	}

	public void setVehicleOwnership(DVehicleOwnership vehicleOwnership) {
		this.vehicleOwnership = vehicleOwnership;
	}

	public Tractor getTractor() {
		return tractor;
	}

	public void setTractor(Tractor tractor) {
		this.tractor = tractor;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Tracker getTracker() {
		return tracker;
	}

	public void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}

	public Employee getDriver1() {
		return driver1;
	}

	public void setDriver1(Employee driver1) {
		this.driver1 = driver1;
	}

	public Employee getDriver2() {
		return driver2;
	}

	public void setDriver2(Employee driver2) {
		this.driver2 = driver2;
	}

	public DTrailerType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(DTrailerType equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public RentalVehicle getRentalVehicle() {
		return rentalVehicle;
	}

	public void setRentalVehicle(RentalVehicle rentalVehicle) {
		this.rentalVehicle = rentalVehicle;
	}

	public void setRwbId(String rwbId) {
		this.rwbId = rwbId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getLoadNumber() {
		return loadNumber;
	}

	public void setLoadNumber(String loadNumber) {
		this.loadNumber = loadNumber;
	}

	public DCustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(DCustomerType customerType) {
		this.customerType = customerType;
	}

	public DRWBStatus getRwbStatus() {
		return rwbStatus;
	}

	public void setRwbStatus(DRWBStatus rwbStatus) {
		this.rwbStatus = rwbStatus;
	}

	public Date getRwbDate() {
		return rwbDate;
	}

	public void setRwbDate(Date rwbDate) {
		this.rwbDate = rwbDate;
	}

	public String getBolNumber() {
		return bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public DPaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(DPaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public Company getCustomer() {
		return customer;
	}

	public void setCustomer(Company customer) {
		this.customer = customer;
	}

	public Company getBroker() {
		return broker;
	}

	public void setBroker(Company broker) {
		this.broker = broker;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public List<Stop> getStops() {
		return stops;
	}

	public void setStops(List<Stop> stops) {
		this.stops = stops;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<StopProgress> getStopProgresses() {
		if (stopProgresses == null)
			return null;
		this.stopProgresses.sort(new Comparator<StopProgress>() {
			@Override
			public int compare(StopProgress x, StopProgress y) {
				if (x.getArrivalTime() == null && y.getArrivalTime() == null)
					return 0;
				if (x.getArrivalTime() == null)
					return 1;
				else if (y.getArrivalTime() == null)
					return -1;
				else
					return x.getArrivalTime().compareTo(y.getArrivalTime());
			}

		});
		return this.stopProgresses;
	}

	public List<Income> getIncomes() {
		return incomes;
	}

	public void setIncomes(List<Income> incomes) {
		this.incomes = incomes;
	}

	public List<Expense> getExpenses() {
		return expenses;
	}

	public void setExpenses(List<Expense> expenses) {
		this.expenses = expenses;
	}

	public void setStopProgresses(List<StopProgress> stopProgresses) {
		this.stopProgresses = stopProgresses;
	}

	public String getAssetCombinationId() {
		return assetCombinationId;
	}

	public void setAssetCombinationId(String assetCombinationId) {
		this.assetCombinationId = assetCombinationId;
	}

	public String getPodUrl() {
		return podUrl;
	}

	public void setPodUrl(String podUrl) {
		this.podUrl = podUrl;
	}

	public Boolean getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(Boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

	public DInvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(DInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public DState getTaxState() {
		return taxState;
	}

	public void setTaxState(DState taxState) {
		this.taxState = taxState;
	}

	public Boolean getIsArea() {
		return isArea;
	}

	public void setIsArea(Boolean isArea) {
		this.isArea = isArea;
	}

	public DCity getCity() {
		return city;
	}

	public void setCity(DCity city) {
		this.city = city;
	}

	public Double getWeightInTon() {
		return weightInTon;
	}

	public void setWeightInTon(Double weightInTon) {
		this.weightInTon = weightInTon;
	}

	public Double getRatePerKm() {
		return ratePerKm;
	}

	public void setRatePerKm(Double ratePerKm) {
		this.ratePerKm = ratePerKm;
	}

	public Double getRatePerTon() {
		return ratePerTon;
	}

	public void setRatePerTon(Double ratePerTon) {
		this.ratePerTon = ratePerTon;
	}

	public Double getTmsKm() {
		return tmsKm;
	}

	public void setTmsKm(Double tmsKm) {
		this.tmsKm = tmsKm;
	}

	public Double getTrackerKm() {
		return trackerKm;
	}

	public void setTrackerKm(Double trackerKm) {
		this.trackerKm = trackerKm;
	}

	public Double getDetentionRate() {
		return detentionRate;
	}

	public void setDetentionRate(Double detentionRate) {
		this.detentionRate = detentionRate;
	}

	public Double getHaltingRate() {
		return haltingRate;
	}

	public void setHaltingRate(Double haltingRate) {
		this.haltingRate = haltingRate;
	}
}
