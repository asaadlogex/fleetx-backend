package com.fleetX.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

@Entity
@Table(name="rental_vehicle")
@Where(clause="is_deleted=false")
public class RentalVehicle implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="rental_vehicle_id")
	private Integer rentalVehicleId;
	@ManyToOne
	@JoinColumn(name="broker_id",referencedColumnName="company_id")
	private Company broker;
	@ManyToOne
	@JoinColumn(name="equipment_type",referencedColumnName="trailer_type_name")
	private DTrailerType equipmentType;
	@ManyToOne
	@JoinColumn(name="equipment_size",referencedColumnName="trailer_size_name")
	private DTrailerSize equipmentSize;
	@Column(name="vehicle_number")
	private String vehicleNumber;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getRentalVehicleId() {
		return rentalVehicleId;
	}
	public void setRentalVehicleId(Integer rentalVehicleId) {
		this.rentalVehicleId = rentalVehicleId;
	}
	public Company getBroker() {
		return broker;
	}
	public void setBroker(Company broker) {
		this.broker = broker;
	}
	public DTrailerType getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(DTrailerType equipmentType) {
		this.equipmentType = equipmentType;
	}
	public DTrailerSize getEquipmentSize() {
		return equipmentSize;
	}
	public void setEquipmentSize(DTrailerSize equipmentSize) {
		this.equipmentSize = equipmentSize;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	
}
