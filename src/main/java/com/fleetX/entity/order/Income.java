package com.fleetX.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DIncomeType;

@Entity
@Table(name="income")
@Where(clause="is_deleted=false")
public class Income implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="income_id")
	private Integer incomeId;
	@ManyToOne
	@JoinColumn(name="income_type",referencedColumnName="income_type_name")
	private DIncomeType incomeType;
	@Column(name="income_description")
	private String description;
	@Column(name="amount")
	private Double amount;
	@ManyToOne
	@JoinColumn(name = "rwb_id",referencedColumnName = "rwb_id")
	private RoadwayBill roadwayBill;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public DIncomeType getIncomeType() {
		return incomeType;
	}
	public void setIncomeType(DIncomeType incomeType) {
		this.incomeType = incomeType;
	}
	public Integer getIncomeId() {
		return incomeId;
	}
	public void setIncomeId(Integer incomeId) {
		this.incomeId = incomeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public RoadwayBill getRoadwayBill() {
		return roadwayBill;
	}

	public void setRoadwayBill(RoadwayBill roadwayBill) {
		this.roadwayBill = roadwayBill;
	}
}
