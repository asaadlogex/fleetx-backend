package com.fleetX.entity.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DStopType;

@Entity
@Table(name = "stop")
@Where(clause = "is_deleted=false")
public class Stop implements Serializable {
	@Id
	@GeneratedValue(generator = "stop-generator")
	@GenericGenerator(name = "stop-generator", parameters = { @Parameter(name = "prefix", value = "STOP"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "stop_id")
	private String stopId;
	@Column(name = "stop_sequence")
	private Integer stopSequenceNumber;
	@ManyToOne
	@JoinColumn(name = "stop_type", referencedColumnName = "stop_type_name")
	private DStopType stopType;
	@OneToOne
	@JoinColumn(name = "warehouse_id", referencedColumnName = "warehouse_id")
	private Warehouse warehouse;
	@Column(name = "appointment_start")
	private Date appointmentStartTime;
	@Column(name = "appointment_end")
	private Date appointmentEndTime;
	@Column(name = "reference_number")
	private String referenceNumber;
	@ManyToOne
	@JoinColumn(name = "rwb_id", referencedColumnName = "rwb_id")
	private RoadwayBill roadwayBill;
	@OneToOne(mappedBy = "stop", cascade = CascadeType.REMOVE)
	@JoinColumn(name = "stop_progress_id", referencedColumnName = "stop_progress_id")
	private StopProgress stopProgress;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public StopProgress getStopProgress() {
		return stopProgress;
	}

	public void setStopProgress(StopProgress stopProgress) {
		this.stopProgress = stopProgress;
	}

	public String getStopId() {
		return stopId;
	}

	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

	public Integer getStopSequenceNumber() {
		return stopSequenceNumber;
	}

	public void setStopSequenceNumber(Integer stopSequenceNumber) {
		this.stopSequenceNumber = stopSequenceNumber;
	}

	public DStopType getStopType() {
		return stopType;
	}

	public void setStopType(DStopType stopType) {
		this.stopType = stopType;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public Date getAppointmentStartTime() {
		return appointmentStartTime;
	}

	public void setAppointmentStartTime(Date appointmentStartTime) {
		this.appointmentStartTime = appointmentStartTime;
	}

	public Date getAppointmentEndTime() {
		return appointmentEndTime;
	}

	public void setAppointmentEndTime(Date appointmentEndTime) {
		this.appointmentEndTime = appointmentEndTime;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public RoadwayBill getRoadwayBill() {
		return roadwayBill;
	}

	public void setRoadwayBill(RoadwayBill roadwayBill) {
		this.roadwayBill = roadwayBill;
	}

}
