package com.fleetX.entity.order;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DReimbursementStatus;
import com.fleetX.entity.employee.Employee;

@Entity
@Table(name = "job")
@Where(clause = "is_deleted=false")
public class Job implements Serializable {
	@Id
	@GeneratedValue(generator = "job-generator")
	@GenericGenerator(name = "job-generator", parameters = { @Parameter(name = "prefix", value = "JOB"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "job_id")
	private String jobId;
	@ManyToOne
	@JoinColumn(name = "job_status", referencedColumnName = "job_status_name")
	private DJobStatus jobStatus;
	@ManyToOne
	@JoinColumn(name = "reimbursement_status", referencedColumnName = "reimbursement_status_name")
	private DReimbursementStatus reimbursementStatus;
	@Column(name = "job_advance")
	private Double jobAdvance;
	@Column(name = "job_start_km")
	private Double startKm;
	@Column(name = "job_end_km")
	private Double endKm;
	@Column(name = "job_km")
	private Double totalKm;
	@Column(name = "job_odometer_km")
	private Double odometerKm;
	@Column(name = "job_tms_km")
	private Double tmsKm;
	@Column(name = "job_tracker_km")
	private Double trackerKm;
	@Column(name = "job_start_date")
	private Date startDate;
	@Column(name = "job_end_date")
	private Date endDate;
	@ManyToOne
	@JoinColumn(name = "tractor_id", referencedColumnName = "tractor_id")
	private Tractor tractor;
	@ManyToOne
	@JoinColumn(name = "trailer_id", referencedColumnName = "trailer_id")
	private Trailer trailer;
	@ManyToOne
	@JoinColumn(name = "container_id", referencedColumnName = "container_id")
	private Container container;
	@ManyToOne
	@JoinColumn(name = "tracker_id", referencedColumnName = "tracker_id")
	private Tracker tracker;
	@ManyToOne
	@JoinColumn(name = "driver1_id", referencedColumnName = "employee_id")
	private Employee driver1;
	@ManyToOne
	@JoinColumn(name = "driver2_id", referencedColumnName = "employee_id")
	private Employee driver2;
	@ManyToOne
	@JoinColumn(name = "rental_vehicle_id", referencedColumnName = "rental_vehicle_id")
	private RentalVehicle rentalVehicle;
	@ManyToOne
	@JoinColumn(name = "start_base_station", referencedColumnName = "base_station_name")
	private DBaseStation startBaseStation;
	@ManyToOne
	@JoinColumn(name = "end_base_station", referencedColumnName = "base_station_name")
	private DBaseStation endBaseStation;
	@OneToMany(mappedBy = "job", cascade = CascadeType.REMOVE)
	private List<JobFuelDetail> jobFuelDetails;
	@OneToMany(mappedBy = "job", cascade = CascadeType.REMOVE)
	private List<RoadwayBill> roadwayBills;
	@OneToMany(mappedBy = "job", cascade = CascadeType.REMOVE)
	private List<StopProgress> stopProgresses;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public DJobStatus getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(DJobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

	public Double getJobAdvance() {
		return jobAdvance;
	}

	public void setJobAdvance(Double jobAdvance) {
		this.jobAdvance = jobAdvance;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public DBaseStation getStartBaseStation() {
		return startBaseStation;
	}

	public void setStartBaseStation(DBaseStation startBaseStation) {
		this.startBaseStation = startBaseStation;
	}

	public DBaseStation getEndBaseStation() {
		return endBaseStation;
	}

	public void setEndBaseStation(DBaseStation endBaseStation) {
		this.endBaseStation = endBaseStation;
	}

	public List<JobFuelDetail> getJobFuelDetails() {
		return jobFuelDetails;
	}

	public void setJobFuelDetails(List<JobFuelDetail> jobFuelDetails) {
		this.jobFuelDetails = jobFuelDetails;
	}

	public List<RoadwayBill> getRoadwayBills() {
		return this.roadwayBills.stream().sorted((RoadwayBill x, RoadwayBill y) -> {
			return y.getRwbDate().compareTo(x.getRwbDate());
		}).collect(Collectors.toList());
	}

	public void setRoadwayBills(List<RoadwayBill> roadwayBills) {
		this.roadwayBills = roadwayBills;
	}

	public List<StopProgress> getStopProgresses() {
		return stopProgresses;
	}

	public void setStopProgresses(List<StopProgress> stopProgresses) {
		this.stopProgresses = stopProgresses;
	}

	public Double getStartKm() {
		return startKm;
	}

	public void setStartKm(Double startKm) {
		this.startKm = startKm;
	}

	public Double getEndKm() {
		return endKm;
	}

	public void setEndKm(Double endKm) {
		this.endKm = endKm;
	}

	public DReimbursementStatus getReimbursementStatus() {
		return reimbursementStatus;
	}

	public void setReimbursementStatus(DReimbursementStatus reimbursementStatus) {
		this.reimbursementStatus = reimbursementStatus;
	}

	public Tractor getTractor() {
		return tractor;
	}

	public void setTractor(Tractor tractor) {
		this.tractor = tractor;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Tracker getTracker() {
		return tracker;
	}

	public void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}

	public Employee getDriver1() {
		return driver1;
	}

	public void setDriver1(Employee driver1) {
		this.driver1 = driver1;
	}

	public Employee getDriver2() {
		return driver2;
	}

	public void setDriver2(Employee driver2) {
		this.driver2 = driver2;
	}

	public Double getTmsKm() {
		return tmsKm;
	}

	public void setTmsKm(Double tmsKm) {
		this.tmsKm = tmsKm;
	}

	public Double getTrackerKm() {
		return trackerKm;
	}

	public void setTrackerKm(Double trackerKm) {
		this.trackerKm = trackerKm;
	}

	public RentalVehicle getRentalVehicle() {
		return rentalVehicle;
	}

	public void setRentalVehicle(RentalVehicle rentalVehicle) {
		this.rentalVehicle = rentalVehicle;
	}

	public Double getOdometerKm() {
		return odometerKm;
	}

	public void setOdometerKm(Double odometerKm) {
		this.odometerKm = odometerKm;
	}
}
