package com.fleetX.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DCategory;

@Entity
@Table(name="product")
@Where(clause="is_deleted=false")
public class Product implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="product_id")
	private Integer productId;
	@ManyToOne
	@JoinColumn(name="category_name",referencedColumnName = "category_name")
	private DCategory category;
	@Column(name="commodity")
	private String commodity;
	@Column(name="product_weight")
	private Double weight;
	@Column(name="product_weight_unit")
	private String weightUnit;
	@Column(name="package_count")
	private Integer packageCount;
	@Column(name="temperature")
	private Double temperature;
	@ManyToOne
	@JoinColumn(name="rwb_id",referencedColumnName = "rwb_id")
	private RoadwayBill roadwayBill;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public DCategory getCategory() {
		return category;
	}
	public void setCategory(DCategory category) {
		this.category = category;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	public Integer getPackageCount() {
		return packageCount;
	}
	public void setPackageCount(Integer packageCount) {
		this.packageCount = packageCount;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public RoadwayBill getRoadwayBill() {
		return roadwayBill;
	}
	public void setRoadwayBill(RoadwayBill roadwayBill) {
		this.roadwayBill = roadwayBill;
	}	
}
