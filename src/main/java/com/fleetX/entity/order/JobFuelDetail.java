package com.fleetX.entity.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.dropdown.DFuelTankType;

@Entity
@Table(name = "job_fuel_detail")
@Where(clause = "is_deleted=false")
public class JobFuelDetail implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jfd_id")
	private Integer jobFuelDetailId;
	@ManyToOne
	@JoinColumn(name = "supplier_id", referencedColumnName = "company_id")
	private Company supplier;
	@ManyToOne
	@JoinColumn(name = "tractor_id", referencedColumnName = "tractor_id")
	private Tractor tractor;
	@ManyToOne
	@JoinColumn(name = "trailer_id", referencedColumnName = "trailer_id")
	private Trailer trailer;
	@Column(name = "jfd_date")
	private Date fuelDate;
	@Column(name = "jfd_slip_number")
	private String fuelSlipNumber;
	@ManyToOne
	@JoinColumn(name = "fuel_card_id", referencedColumnName = "fuel_card_id")
	private FuelCard fuelCard;
	@Column(name = "jfd_current_km")
	private Double currentKm;
	@Column(name = "jfd_fuel_in_litre")
	private Double fuelInLitre;
	@Column(name = "jfd_rate_per_litre")
	private Double ratePerLitre;
	@Column(name = "jfd_total_amount")
	private Double totalAmount;
	@ManyToOne
	@JoinColumn(name = "fuel_tank_type", referencedColumnName = "fuel_tank_type_name")
	private DFuelTankType fuelTankType;
	@ManyToOne
	@JoinColumn(name = "job_id", referencedColumnName = "job_id")
	private Job job;
	@OneToOne
	@JoinColumn(name = "stop_progress_id", referencedColumnName = "stop_progress_id")
	private StopProgress fuelProgress;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getJobFuelDetailId() {
		return jobFuelDetailId;
	}

	public void setJobFuelDetailId(Integer jobFuelDetailId) {
		this.jobFuelDetailId = jobFuelDetailId;
	}

	public Company getSupplier() {
		return supplier;
	}

	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}

	public Date getFuelDate() {
		return fuelDate;
	}

	public void setFuelDate(Date fuelSlipDate) {
		this.fuelDate = fuelSlipDate;
	}

	public String getFuelSlipNumber() {
		return fuelSlipNumber;
	}

	public void setFuelSlipNumber(String fuelSlipNumber) {
		this.fuelSlipNumber = fuelSlipNumber;
	}

	public FuelCard getFuelCard() {
		return fuelCard;
	}

	public void setFuelCard(FuelCard fuelCard) {
		this.fuelCard = fuelCard;
	}

	public Double getCurrentKm() {
		return currentKm;
	}

	public void setCurrentKm(Double currentKm) {
		this.currentKm = currentKm;
	}

	public Double getFuelInLitre() {
		return fuelInLitre;
	}

	public void setFuelInLitre(Double fuelInLitre) {
		this.fuelInLitre = fuelInLitre;
	}

	public Double getRatePerLitre() {
		return ratePerLitre;
	}

	public void setRatePerLitre(Double ratePerLitre) {
		this.ratePerLitre = ratePerLitre;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public DFuelTankType getFuelTankType() {
		return fuelTankType;
	}

	public void setFuelTankType(DFuelTankType fuelTankType) {
		this.fuelTankType = fuelTankType;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Tractor getTractor() {
		return tractor;
	}

	public void setTractor(Tractor tractor) {
		this.tractor = tractor;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public StopProgress getFuelProgress() {
		return fuelProgress;
	}

	public void setFuelProgress(StopProgress fuelProgress) {
		this.fuelProgress = fuelProgress;
	}

}
