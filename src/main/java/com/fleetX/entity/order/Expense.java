package com.fleetX.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DExpenseType;

@Entity
@Table(name = "expense")
@Where(clause="is_deleted=false")
public class Expense implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "expense_id")
	private Integer expenseId;
	@ManyToOne
	@JoinColumn(name="expense_type",referencedColumnName="expense_type_name")
	private DExpenseType expenseType;
	@Column(name = "expense_description")
	private String description;
	@Column(name = "amount")
	private Double amount;
	@ManyToOne
	@JoinColumn(name = "rwb_id",referencedColumnName = "rwb_id")
	private RoadwayBill roadwayBill;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public DExpenseType getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(DExpenseType expenseType) {
		this.expenseType = expenseType;
	}
	public Integer getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(Integer expenseId) {
		this.expenseId = expenseId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public RoadwayBill getRoadwayBill() {
		return roadwayBill;
	}

	public void setRoadwayBill(RoadwayBill roadwayBill) {
		this.roadwayBill = roadwayBill;
	}
}
