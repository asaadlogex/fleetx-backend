package com.fleetX.entity.asset;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DMake;

@Entity
@Table(name="asset")
public class Asset implements Serializable {
	@Id
	@GeneratedValue(generator = "asset-generator")
	@GenericGenerator(name = "asset-generator", 
    parameters = {@Parameter(name = "prefix", value = "ASSET"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="asset_id")
	private String assetId;
	@Column(name="asset_induction")
	private Date induction;	
	@ManyToOne
	@JoinColumn(name="supplier_id",referencedColumnName="company_id")
	private Company supplier;
	@Column(name="vin",unique=true)
	private String vin;
	@ManyToOne
	@JoinColumn(name="make_name",referencedColumnName="make_name")
	private DMake assetMake;
	@Column(name="model",nullable=true)
	private String model;
	@Column(name="year")
	private Integer year;
	@Column(name="color")
	private String color;
	@Column(name="status")
	private String status;
	@Column(name="tyre_size")
	private String tyreSize;
	@Column(name="number_of_tyres")
	private Integer numberOfTyres;
	@OneToMany(mappedBy="asset")
	private List<Tyre> tyres=new ArrayList<>();
	@ManyToOne
	@JoinColumn(name="lease_type_name",referencedColumnName="lease_type_name")
	private DLeaseType leaseType;
	@Column(name="gross_weight")
	private Double grossWeight;
	@Column(name="weight_unit")
	private String weightUnit;
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public DMake getAssetMake() {
		return assetMake;
	}
	public void setAssetMake(DMake assetMake) {
		this.assetMake = assetMake;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getTyreSize() {
		return tyreSize;
	}
	public void setTyreSize(String tyreSize) {
		this.tyreSize = tyreSize;
	}
	public Integer getNumberOfTyres() {
		return numberOfTyres;
	}
	public void setNumberOfTyres(Integer numberOfTyres) {
		this.numberOfTyres = numberOfTyres;
	}
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	public Date getInduction() {
		return induction;
	}
	public void setInduction(Date induction) {
		this.induction = induction;
	}	
	public Company getSupplier() {
		return supplier;
	}
	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Tyre> getTyres() {
		return tyres;
	}
	public void setTyres(List<Tyre> tyres) {
		this.tyres = tyres;
	}
	public Double getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
}
