package com.fleetX.entity.asset;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DLeaseType;

@Entity
@Table(name="tyre")
public class Tyre implements Serializable {
	@Id
	@GeneratedValue(generator = "tyre-generator")
	@GenericGenerator(name = "tyre-generator", 
    parameters = {@Parameter(name = "prefix", value = "TYRE"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="tyre_id")
	private String tyreId;
	@Column(name="tyre_serial_number",unique=true)
	private String tyreSerialNumber;
	@Column(name="make")
	private String make;
	@Column(name="starting_km")
	private Double startingKm=0d;
	@Column(name="total_km")
	private Double totalKm=0d;
	@Column(name="status")
	private String status;
	@ManyToOne
	@JoinColumn(name="lease_type_name",referencedColumnName="lease_type_name")
	private DLeaseType leaseType;
	@ManyToOne
	@JoinColumn(name="supplier_id",referencedColumnName="company_id")
	private Company supplier;
	@ManyToOne
	@JoinColumn(name="ac_id",referencedColumnName="ac_id")
	private AdminCompany adminCompany;
	@ManyToOne
	@JoinColumn(name="asset_id",referencedColumnName="asset_id")
	private Asset asset;
	@Column(name="availability")
	private String availability;
	@Column(name="moving_status")
	private String movingStatus;
	
	public String getMovingStatus() {
		return movingStatus;
	}
	public void setMovingStatus(String movingStatus) {
		this.movingStatus = movingStatus;
	}
	
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public Asset getAsset() {
		return asset;
	}
	public void setAsset(Asset asset) {
		this.asset = asset;
	}
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	public Company getSupplier() {
		return supplier;
	}
	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}
	public AdminCompany getAdminCompany() {
		return adminCompany;
	}
	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}	
	public String getTyreId() {
		return tyreId;
	}
	public void setTyreId(String tyreId) {
		this.tyreId = tyreId;
	}
	public String getTyreSerialNumber() {
		return tyreSerialNumber;
	}
	public void setTyreSerialNumber(String tyreSerialNumber) {
		this.tyreSerialNumber = tyreSerialNumber;
	}
	public Double getStartingKm() {
		return startingKm;
	}
	public void setStartingKm(Double startingKm) {
		this.startingKm = startingKm;
	}
	public Double getTotalKm() {
		return totalKm;
	}
	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Tyre [tyreId=" + tyreId + ", tyreSerialNumber=" + tyreSerialNumber + ", make=" + make + ", startingKm="
				+ startingKm + ", totalKm=" + totalKm + ", status=" + status + ", leaseType=" + leaseType
				+ ", supplierBusiness=" + supplier + ", adminCompany=" + adminCompany + "]";
	}
}
