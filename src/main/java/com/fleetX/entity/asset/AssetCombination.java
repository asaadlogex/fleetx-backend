package com.fleetX.entity.asset;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.employee.Employee;

@Entity
@Table(name = "asset_combination")
public class AssetCombination {
	@Id
	@GeneratedValue(generator = "asset-generator")
	@GenericGenerator(name = "asset-generator", parameters = { @Parameter(name = "prefix", value = "AC"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "asset_combination_id")
	private String assetCombinationId;
	@OneToOne
	@JoinColumn(name = "tractor_id", referencedColumnName = "tractor_id")
	private Tractor tractor;
	@OneToOne
	@JoinColumn(name = "trailer_id", referencedColumnName = "trailer_id")
	private Trailer trailer;
	@OneToOne
	@JoinColumn(name = "tracker_id", referencedColumnName = "tracker_id")
	private Tracker tracker;
	@OneToOne
	@JoinColumn(name = "container_id", referencedColumnName = "container_id")
	private Container container;
	@OneToOne
	@JoinColumn(name = "driver_1", referencedColumnName = "employee_id")
	private Employee driver1;
	@OneToOne
	@JoinColumn(name = "driver_2", referencedColumnName = "employee_id")
	private Employee driver2;
	@Column(name = "status")
	private String status;
	@ManyToOne
	@JoinColumn(name = "company_contract_id", referencedColumnName = "company_contract_id")
	private CompanyContract companyContract;
	@Column(name = "note")
	private String note;
	@Column(name = "availability")
	private String availability;
	@Column(name = "job_availability")
	private String jobAvailability;

	public String getJobAvailability() {
		return jobAvailability;
	}

	public void setJobAvailability(String jobAvailability) {
		this.jobAvailability = jobAvailability;
	}

	public CompanyContract getCompanyContract() {
		return companyContract;
	}

	public void setCompanyContract(CompanyContract companyContract) {
		this.companyContract = companyContract;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getAssetCombinationId() {
		return assetCombinationId;
	}

	public void setAssetCombinationId(String assetCombinationId) {
		this.assetCombinationId = assetCombinationId;
	}

	public Tractor getTractor() {
		return tractor;
	}

	public void setTractor(Tractor tractor) {
		this.tractor = tractor;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public Tracker getTracker() {
		return tracker;
	}

	public void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Employee getDriver1() {
		return driver1;
	}

	public void setDriver1(Employee driver1) {
		this.driver1 = driver1;
	}

	public Employee getDriver2() {
		return driver2;
	}

	public void setDriver2(Employee driver2) {
		this.driver2 = driver2;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
