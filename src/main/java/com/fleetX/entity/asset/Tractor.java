package com.fleetX.entity.asset;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.AdminCompany;

@Entity
@Table(name = "tractor")
public class Tractor implements Serializable {
	@Id
	@GeneratedValue(generator = "tractor-generator")
	@GenericGenerator(name = "tractor-generator", parameters = { @Parameter(name = "prefix", value = "TRACTOR"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "tractor_id")
	private String tractorId;
	@Column(name = "engine_type")
	private String engineType;
	@Column(name = "hp")
	private String hp;
	@Column(name = "engine_number", unique = true)
	private String engineNumber;
	@Column(name = "transmission")
	private String transmission;
	@Column(name = "number_plate", unique = true)
	private String numberPlate;
	@Column(name = "number_of_gears")
	private Integer numberOfGears;
	@Column(name = "number_of_fuel_tanks")
	private Integer numberOfFuelTanks;
	@Column(name = "fuel_capacity")
	private Double fuelCapacity;
	@Column(name = "starting_odometer")
	private Double startingOdometer;
	@Column(name = "current_odometer")
	private Double currentOdometer;
	@Column(name = "odometer_unit")
	private String odometerUnit;
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "asset_id", referencedColumnName = "asset_id")
	private Asset asset;
	@ManyToOne
	@JoinColumn(name = "ac_id", referencedColumnName = "ac_id")
	private AdminCompany adminCompany;
	@Column(name = "availability")
	private String availability;
	@Column(name = "moving_status")
	private String movingStatus;

	public String getMovingStatus() {
		return movingStatus;
	}

	public void setMovingStatus(String movingStatus) {
		this.movingStatus = movingStatus;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public AdminCompany getAdminCompany() {
		return adminCompany;
	}

	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}

	public String getTractorId() {
		return tractorId;
	}

	public void setTractorId(String tractorId) {
		this.tractorId = tractorId;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public Integer getNumberOfGears() {
		return numberOfGears;
	}

	public void setNumberOfGears(Integer numberOfGears) {
		this.numberOfGears = numberOfGears;
	}

	public Integer getNumberOfFuelTanks() {
		return numberOfFuelTanks;
	}

	public void setNumberOfFuelTanks(Integer numberOfFuelTanks) {
		this.numberOfFuelTanks = numberOfFuelTanks;
	}

	public Double getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(Double fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public Asset getAsset() {
		return asset;
	}

	public void setAsset(Asset asset) {
		this.asset = asset;
	}

	public String getNumberPlate() {
		return numberPlate;
	}

	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public Double getStartingOdometer() {
		return startingOdometer;
	}

	public void setStartingOdometer(Double startingOdometer) {
		this.startingOdometer = startingOdometer;
	}

	public Double getCurrentOdometer() {
		return currentOdometer;
	}

	public void setCurrentOdometer(Double currentOdometer) {
		this.currentOdometer = currentOdometer;
	}

	public String getOdometerUnit() {
		return odometerUnit;
	}

	public void setOdometerUnit(String odometerUnit) {
		this.odometerUnit = odometerUnit;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}
}
