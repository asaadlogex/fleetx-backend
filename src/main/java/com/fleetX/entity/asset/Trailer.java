package com.fleetX.entity.asset;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

@Entity
@Table(name = "trailer")
public class Trailer implements Serializable {
	@Id
	@GeneratedValue(generator = "trailer-generator")
	@GenericGenerator(name = "trailer-generator", parameters = { @Parameter(name = "prefix", value = "TRAILER"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "trailer_id")
	private String trailerId;
	@ManyToOne
	@JoinColumn(name = "trailer_type_name", referencedColumnName = "trailer_type_name")
	private DTrailerType trailerType;
	@ManyToOne
	@JoinColumn(name = "trailer_size_name", referencedColumnName = "trailer_size_name")
	private DTrailerSize trailerSize;
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "asset_id", referencedColumnName = "asset_id")
	private Asset asset;
	@ManyToOne
	@JoinColumn(name = "ac_id", referencedColumnName = "ac_id")
	private AdminCompany adminCompany;
	@Column(name = "availability")
	private String availability;
	@Column(name = "moving_status")
	private String movingStatus;

	public String getMovingStatus() {
		return movingStatus;
	}

	public void setMovingStatus(String movingStatus) {
		this.movingStatus = movingStatus;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public AdminCompany getAdminCompany() {
		return adminCompany;
	}

	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}

	public String getTrailerId() {
		return trailerId;
	}

	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}

	public DTrailerType getTrailerType() {
		return trailerType;
	}

	public void setTrailerType(DTrailerType trailerType) {
		this.trailerType = trailerType;
	}

	public DTrailerSize getTrailerSize() {
		return trailerSize;
	}

	public void setTrailerSize(DTrailerSize trailerSize) {
		this.trailerSize = trailerSize;
	}

	public Asset getAsset() {
		return asset;
	}

	public void setAsset(Asset asset) {
		this.asset = asset;
	}
}
