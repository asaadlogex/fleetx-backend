package com.fleetX.entity.asset;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DContainerType;
import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DTrailerSize;

@Entity
@Table(name="container")
public class Container {
	@Id
	@GeneratedValue(generator = "container-generator")
	@GenericGenerator(name = "container-generator", 
    parameters = {@Parameter(name = "prefix", value = "CONTAINER"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="container_id")
	private String containerId;
	@Column(name="container_weight")
	private Double containerWeight;
	@Column(name="container_weight_unit")
	private String containerWeightUnit;
	@Column(name="container_number",unique=true)
	private String containerNumber;
	@Column(name="container_year")
	private String containerYear;
	@Column(name="container_make")
	private String containerMake;
	@Column(name="purchase_date")
	private Date purchaseDate;
	@ManyToOne
	@JoinColumn(name="container_size",referencedColumnName="trailer_size_name")
	private DTrailerSize containerSize;
	@ManyToOne
	@JoinColumn(name="lease_type_name",referencedColumnName="lease_type_name")
	private DLeaseType leaseType;
	@ManyToOne
	@JoinColumn(name="container_type",referencedColumnName="container_type_name")	
	private DContainerType containerType;
	@ManyToOne
	@JoinColumn(name="supplier_id",referencedColumnName="company_id")
	private Company supplier;
	@ManyToOne
	@JoinColumn(name="ac_id",referencedColumnName="ac_id")
	private AdminCompany adminCompany;
	@Column(name="container_status")
	private String status;
	@Column(name="availability")
	private String availability;
	@Column(name="moving_status")
	private String movingStatus;
	
	public String getMovingStatus() {
		return movingStatus;
	}
	public void setMovingStatus(String movingStatus) {
		this.movingStatus = movingStatus;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public Company getSupplier() {
		return supplier;
	}
	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}
	public AdminCompany getAdminCompany() {
		return adminCompany;
	}
	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	public Double getContainerWeight() {
		return containerWeight;
	}
	public void setContainerWeight(Double containerWeight) {
		this.containerWeight = containerWeight;
	}
	public String getContainerWeightUnit() {
		return containerWeightUnit;
	}
	public void setContainerWeightUnit(String containerWeightUnit) {
		this.containerWeightUnit = containerWeightUnit;
	}
	public DTrailerSize getContainerSize() {
		return containerSize;
	}
	public void setContainerSize(DTrailerSize containerSize) {
		this.containerSize = containerSize;
	}
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	public DContainerType getContainerType() {
		return containerType;
	}
	public void setContainerType(DContainerType containerType) {
		this.containerType = containerType;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getContainerYear() {
		return containerYear;
	}
	public void setContainerYear(String containerYear) {
		this.containerYear = containerYear;
	}
	public String getContainerMake() {
		return containerMake;
	}
	public void setContainerMake(String containerMake) {
		this.containerMake = containerMake;
	}
	
}
