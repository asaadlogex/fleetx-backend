package com.fleetX.entity.asset;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DLeaseType;

@Entity
@Table(name="tracker")
public class Tracker {
	@Id
	@GeneratedValue(generator = "tracker-generator")
	@GenericGenerator(name = "tracker-generator", 
    parameters = {@Parameter(name = "prefix", value = "TRACKER"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="tracker_id")
	private String trackerId;
	@Column(name="tracker_status")
	private String status;
	@Column(name="tracker_number")
	private String trackerNumber;
	@Column(name="tracker_name")
	private String trackerName;
	@Column(name="contract_terms")
	private String contractTerms;
	@Column(name="installation_date")
	private Date installationDate;
	@Column(name="return_date")
	private Date returnDate;
	@ManyToOne
	@JoinColumn(name="lease_type_name",referencedColumnName="lease_type_name")
	private DLeaseType leaseType;
	@ManyToOne
	@JoinColumn(name="supplier_id",referencedColumnName="company_id")
	private Company supplier;
	@ManyToOne
	@JoinColumn(name="ac_id",referencedColumnName="ac_id")
	private AdminCompany adminCompany;
	@Column(name="availability")
	private String availability;
	@Column(name="moving_status")
	private String movingStatus;
	
	public String getMovingStatus() {
		return movingStatus;
	}
	public void setMovingStatus(String movingStatus) {
		this.movingStatus = movingStatus;
	}
	
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public Company getSupplier() {
		return supplier;
	}
	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}
	public AdminCompany getAdminCompany() {
		return adminCompany;
	}
	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}	
	public String getTrackerId() {
		return trackerId;
	}
	public void setTrackerId(String trackerId) {
		this.trackerId = trackerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getContractTerms() {
		return contractTerms;
	}
	public void setContractTerms(String contractTerms) {
		this.contractTerms = contractTerms;
	}
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	
	public Date getInstallationDate() {
		return installationDate;
	}
	public void setInstallationDate(Date installationDate) {
		this.installationDate = installationDate;
	}
	
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public String getTrackerNumber() {
		return trackerNumber;
	}
	public void setTrackerNumber(String trackerNumber) {
		this.trackerNumber = trackerNumber;
	}
	public String getTrackerName() {
		return trackerName;
	}
	public void setTrackerName(String trackerName) {
		this.trackerName = trackerName;
	}
}
