package com.fleetX.entity.dropdown;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="d_container_type")
public class DContainerType implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="container_type_id")
	private Integer id;
	@Column(name="container_type_name")
	private String value;
	@Column(name="container_type_code")
	private String code;
	@Column(name="container_type_description")
	private String description;
	@Column(name="status")
	private String status;
	
	public DContainerType() {}
	public DContainerType(String value) {
		this.value=value;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getValue() {
		return value;

	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
