package com.fleetX.entity.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fleetX.entity.Address;
import com.fleetX.entity.company.AdminCompany;

@Entity
@Table(name = "inventory_warehouse")
public class InventoryWarehouse {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inventory_warehouse_id")
	private Integer inventoryWarehouseId;
	@Column(name = "inventory_warehouse_name")
	private String name;
	@Column(name = "inventory_warehouse_address")
	private Address address;
	@Column(name = "inventory_warehouse_contact")
	private String contactNumber;
	@Column(name = "inventory_warehouse_person")
	private String contactPerson;
	@ManyToOne
	@JoinColumn(name = "admin_company", referencedColumnName = "ac_id")
	private AdminCompany adminCompany;

	public Integer getInventoryWarehouseId() {
		return inventoryWarehouseId;
	}

	public void setInventoryWarehouseId(Integer inventoryWarehouseId) {
		this.inventoryWarehouseId = inventoryWarehouseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public AdminCompany getAdminCompany() {
		return adminCompany;
	}

	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}

}
