package com.fleetX.entity.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fleetX.entity.dropdown.DBrand;
import com.fleetX.entity.dropdown.DItemCategory;
import com.fleetX.entity.dropdown.DItemType;
import com.fleetX.entity.dropdown.DUnit;

@Entity
@Table(name = "item")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_id")
	private Integer itemId;
	@Column(name = "item_name")
	private String itemName;
	@Column(name = "item_sku")
	private String itemSku;
	@Column(name = "item_price")
	private Double itemPrice;
	@ManyToOne
	@JoinColumn(name = "item_type", referencedColumnName = "item_type_name")
	private DItemType itemType;
	@ManyToOne
	@JoinColumn(name = "item_unit", referencedColumnName = "unit_name")
	private DUnit itemUnit;
	@ManyToOne
	@JoinColumn(name = "item_category", referencedColumnName = "item_category_name")
	private DItemCategory itemCategory;
	@ManyToOne
	@JoinColumn(name = "item_brand", referencedColumnName = "brand_name")
	private DBrand itemBrand;
	@Column(name = "item_length")
	private Double itemLength;
	@Column(name = "item_breadth")
	private Double itemBreadth;
	@Column(name = "item_height")
	private Double itemHeight;
	@Column(name = "item_weight_in_kg")
	private Double weightInKg;
	@Column(name = "item_upc")
	private String itemUpc;
	@Column(name = "item_ean")
	private String itemEan;
	@Column(name = "item_mpn")
	private String itemMpn;
	@Column(name = "item_isbn")
	private String itemIsbn;
	@Column(name = "critical_qty")
	private Integer criticalQty;
	@Column(name = "is_refundable")
	private Boolean isRefundable;
	@Column(name = "item_pic_url")
	private String itemPicUrl;

	@OneToOne(mappedBy = "item")

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemSku() {
		return itemSku;
	}

	public void setItemSku(String itemSku) {
		this.itemSku = itemSku;
	}

	public DItemType getItemType() {
		return itemType;
	}

	public void setItemType(DItemType itemType) {
		this.itemType = itemType;
	}

	public DUnit getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(DUnit itemUnit) {
		this.itemUnit = itemUnit;
	}

	public DItemCategory getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(DItemCategory itemCategory) {
		this.itemCategory = itemCategory;
	}

	public DBrand getItemBrand() {
		return itemBrand;
	}

	public void setItemBrand(DBrand itemBrand) {
		this.itemBrand = itemBrand;
	}

	public Double getItemLength() {
		return itemLength;
	}

	public void setItemLength(Double itemLength) {
		this.itemLength = itemLength;
	}

	public Double getItemBreadth() {
		return itemBreadth;
	}

	public void setItemBreadth(Double itemBreadth) {
		this.itemBreadth = itemBreadth;
	}

	public Double getItemHeight() {
		return itemHeight;
	}

	public void setItemHeight(Double itemHeight) {
		this.itemHeight = itemHeight;
	}

	public Double getWeightInKg() {
		return weightInKg;
	}

	public void setWeightInKg(Double weightInKg) {
		this.weightInKg = weightInKg;
	}

	public String getItemUpc() {
		return itemUpc;
	}

	public void setItemUpc(String itemUpc) {
		this.itemUpc = itemUpc;
	}

	public String getItemEan() {
		return itemEan;
	}

	public void setItemEan(String itemEan) {
		this.itemEan = itemEan;
	}

	public String getItemMpn() {
		return itemMpn;
	}

	public void setItemMpn(String itemMpn) {
		this.itemMpn = itemMpn;
	}

	public String getItemIsbn() {
		return itemIsbn;
	}

	public void setItemIsbn(String itemIsbn) {
		this.itemIsbn = itemIsbn;
	}

	public Boolean getIsRefundable() {
		return isRefundable;
	}

	public void setIsRefundable(Boolean isRefundable) {
		this.isRefundable = isRefundable;
	}

	public String getItemPicUrl() {
		return itemPicUrl;
	}

	public void setItemPicUrl(String itemPicUrl) {
		this.itemPicUrl = itemPicUrl;
	}

	public Double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public Integer getCriticalQty() {
		return criticalQty;
	}

	public void setCriticalQty(Integer criticalQty) {
		this.criticalQty = criticalQty;
	}

}
