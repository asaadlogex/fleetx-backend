package com.fleetX.entity.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "work_order_item_detail")
public class WorkOrderItemDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "work_order_item_detail_id")
	private Integer workOrderItemDetailId;
	@ManyToOne
	@JoinColumn(name = "item_id", referencedColumnName = "item_id")
	private Item item;
	@Column(name = "qty_used")
	private Integer qtyUsed;
	@Column(name = "rate")
	private Double rate;
	@Column(name = "amount")
	private Double amount;
	@ManyToOne
	@JoinColumn(name = "work_order_id", referencedColumnName = "work_order_id")
	private WorkOrder workOrder;

	public Integer getWorkOrderItemDetailId() {
		return workOrderItemDetailId;
	}

	public void setWorkOrderItemDetailId(Integer workOrderItemDetailId) {
		this.workOrderItemDetailId = workOrderItemDetailId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQtyUsed() {
		return qtyUsed;
	}

	public void setQtyUsed(Integer qtyUsed) {
		this.qtyUsed = qtyUsed;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public WorkOrder getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

}
