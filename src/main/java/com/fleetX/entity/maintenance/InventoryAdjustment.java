package com.fleetX.entity.maintenance;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventory_adjustment")
public class InventoryAdjustment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inventory_adjustment_id")
	private Integer inventoryAdjustmentId;
	@OneToOne
	@JoinColumn(name = "item_id", referencedColumnName = "item_id")
	private Item item;
	@Column(name = "inventory_adjustment_date")
	private Date inventoryAdjustmentDate;
	@Column(name = "adjustment_qty")
	private Integer adjustmentQty;
	@Column(name = "reference_number")
	private String referenceNumber;
	@Column(name = "adjustment_reason")
	private String adjustmentReason;

	public Integer getInventoryAdjustmentId() {
		return inventoryAdjustmentId;
	}

	public void setInventoryAdjustmentId(Integer inventoryAdjustmentId) {
		this.inventoryAdjustmentId = inventoryAdjustmentId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Date getInventoryAdjustmentDate() {
		return inventoryAdjustmentDate;
	}

	public void setInventoryAdjustmentDate(Date inventoryAdjustmentDate) {
		this.inventoryAdjustmentDate = inventoryAdjustmentDate;
	}

	public Integer getAdjustmentQty() {
		return adjustmentQty;
	}

	public void setAdjustmentQty(Integer adjustmentQty) {
		this.adjustmentQty = adjustmentQty;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getAdjustmentReason() {
		return adjustmentReason;
	}

	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}

}
