package com.fleetX.entity.maintenance;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventory_transaction_info")
public class InventoryTransactionInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inventory_transaction_info_id")
	private Integer inventoryTransactionInfoId;
	@Column(name = "action")
	private String action;
	@Column(name = "transaction_description")
	private String description;
	@Column(name = "transaction_date")
	private Date date;
	@Column(name = "transaction_qty")
	private Integer qty;
	@Column(name = "transaction_cost")
	private Integer cost;
	@Column(name = "qty_on_hand")
	private Integer qtyOnHand;
	@Column(name = "transaction_total")
	private Integer totalValue;
	@ManyToOne
	@JoinColumn(name = "purchase_order_id", referencedColumnName = "purchase_order_id")
	private PurchaseOrder purchaseOrder;
	@ManyToOne
	@JoinColumn(name = "work_order_id", referencedColumnName = "work_order_id")
	private WorkOrder workOrder;
	@ManyToOne
	@JoinColumn(name = "inventory_id", referencedColumnName = "inventory_id")
	private Inventory inventory;

	public Integer getInventoryTransactionInfoId() {
		return inventoryTransactionInfoId;
	}

	public void setInventoryTransactionInfoId(Integer inventoryTransactionInfoId) {
		this.inventoryTransactionInfoId = inventoryTransactionInfoId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(Integer qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public Integer getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Integer totalValue) {
		this.totalValue = totalValue;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public WorkOrder getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

}
