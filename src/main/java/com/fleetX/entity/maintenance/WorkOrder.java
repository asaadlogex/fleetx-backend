package com.fleetX.entity.maintenance;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DPriority;
import com.fleetX.entity.dropdown.DWorkOrderCategory;
import com.fleetX.entity.dropdown.DWorkOrderStatus;
import com.fleetX.entity.dropdown.DWorkOrderSubCategory;

@Entity
@Table(name = "work_order")
public class WorkOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "work_order_id")
	private Integer workOrderId;
	@ManyToOne
	@JoinColumn(name = "tractor_id", referencedColumnName = "tractor_id")
	private Tractor tractor;
	@Column(name = "tractor_km")
	private Double tractorKm;
	@ManyToOne
	@JoinColumn(name = "base_station_id", referencedColumnName = "base_station_id")
	private DBaseStation baseStation;
	@ManyToOne
	@JoinColumn(name = "priority_id", referencedColumnName = "priority_id")
	private DPriority priority;
	@Column(name = "work_order_date")
	private Date workOrderDate;
	@Column(name = "work_order_completion_date")
	private Date workOrderCompletionDate;
	@ManyToOne
	@JoinColumn(name = "work_order_status", referencedColumnName = "work_order_status_name")
	private DWorkOrderStatus workOrderStatus;
	@ManyToOne
	@JoinColumn(name = "work_order_category", referencedColumnName = "work_order_category_name")
	private DWorkOrderCategory workOrderCategory;
	@ManyToOne
	@JoinColumn(name = "work_order_sub_category", referencedColumnName = "work_order_sub_category_name")
	private DWorkOrderSubCategory workOrderSubCategory;
	@OneToMany(mappedBy = "workOrder", cascade = CascadeType.REMOVE)
	private List<WorkOrderItemDetail> workOrderItemDetail;
	@Column(name = "reference_number")
	private String referenceNumber;
	@Column(name = "sender_name")
	private String senderName;
	@Column(name = "total_amount")
	private Double totalAmount;
	@Column(name = "activity_detail")
	private String activityDetail;
	@ManyToOne
	@JoinColumn(name = "workshop", referencedColumnName = "company_id")
	private Company workshop;

	public Integer getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(Integer workOrderId) {
		this.workOrderId = workOrderId;
	}

	public Tractor getTractor() {
		return tractor;
	}

	public void setTractor(Tractor tractor) {
		this.tractor = tractor;
	}

	public Double getTractorKm() {
		return tractorKm;
	}

	public void setTractorKm(Double tractorKm) {
		this.tractorKm = tractorKm;
	}

	public DBaseStation getBaseStation() {
		return baseStation;
	}

	public void setBaseStation(DBaseStation baseStation) {
		this.baseStation = baseStation;
	}

	public DPriority getPriority() {
		return priority;
	}

	public void setPriority(DPriority priority) {
		this.priority = priority;
	}

	public Date getWorkOrderDate() {
		return workOrderDate;
	}

	public void setWorkOrderDate(Date workOrderDate) {
		this.workOrderDate = workOrderDate;
	}

	public Date getWorkOrderCompletionDate() {
		return workOrderCompletionDate;
	}

	public void setWorkOrderCompletionDate(Date workOrderCompletionDate) {
		this.workOrderCompletionDate = workOrderCompletionDate;
	}

	public DWorkOrderStatus getWorkOrderStatus() {
		return workOrderStatus;
	}

	public void setWorkOrderStatus(DWorkOrderStatus workOrderStatus) {
		this.workOrderStatus = workOrderStatus;
	}

	public DWorkOrderCategory getWorkOrderCategory() {
		return workOrderCategory;
	}

	public void setWorkOrderCategory(DWorkOrderCategory workOrderCategory) {
		this.workOrderCategory = workOrderCategory;
	}

	public DWorkOrderSubCategory getWorkOrderSubCategory() {
		return workOrderSubCategory;
	}

	public void setWorkOrderSubCategory(DWorkOrderSubCategory workOrderSubCategory) {
		this.workOrderSubCategory = workOrderSubCategory;
	}

	public List<WorkOrderItemDetail> getWorkOrderItemDetail() {
		return workOrderItemDetail;
	}

	public void setWorkOrderItemDetail(List<WorkOrderItemDetail> workOrderItemDetails) {
		this.workOrderItemDetail = workOrderItemDetails;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getActivityDetail() {
		return activityDetail;
	}

	public void setActivityDetail(String activityDetail) {
		this.activityDetail = activityDetail;
	}

	public Company getWorkshop() {
		return workshop;
	}

	public void setWorkshop(Company workshop) {
		this.workshop = workshop;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}