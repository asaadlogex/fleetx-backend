package com.fleetX.entity.maintenance;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DPurchaseOrderStatus;

@Entity
@Table(name = "purchase_order")
public class PurchaseOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "purchase_order_id")
	private Integer purchaseOrderId;
	@ManyToOne
	@JoinColumn(name = "vendor", referencedColumnName = "company_id")
	private Company vendor;
	@Column(name = "reference_number")
	private String referenceNumber;
	@Column(name = "ordered_on")
	private Date orderedOn;
	@Column(name = "expected_on")
	private Date expectedOn;
	@Column(name = "received_on")
	private Date receivedOn;
	@ManyToOne
	@JoinColumn(name = "purchase_order_status", referencedColumnName = "purchase_order_status_name")
	private DPurchaseOrderStatus purchaseOrderStatus;
	@OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.REMOVE)
	private List<ItemPurchaseDetail> itemPurchaseDetail;
	@Column(name = "gross_amount")
	private Double grossAmount;
	@Column(name = "discount_in_amount")
	private Double discountInAmount;
	@Column(name = "total_amount")
	private Double totalAmount;

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getOrderedOn() {
		return orderedOn;
	}

	public void setOrderedOn(Date orderedOn) {
		this.orderedOn = orderedOn;
	}

	public Date getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public DPurchaseOrderStatus getPurchaseOrderStatus() {
		return purchaseOrderStatus;
	}

	public void setPurchaseOrderStatus(DPurchaseOrderStatus purchaseOrderStatus) {
		this.purchaseOrderStatus = purchaseOrderStatus;
	}

	public List<ItemPurchaseDetail> getItemPurchaseDetail() {
		return itemPurchaseDetail;
	}

	public void setItemPurchaseDetail(List<ItemPurchaseDetail> itemPurchaseDetail) {
		this.itemPurchaseDetail = itemPurchaseDetail;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Company getVendor() {
		return vendor;
	}

	public void setVendor(Company vendor) {
		this.vendor = vendor;
	}

	public Double getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(Double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Double getDiscountInAmount() {
		return discountInAmount;
	}

	public void setDiscountInAmount(Double discountInAmount) {
		this.discountInAmount = discountInAmount;
	}

	public Date getExpectedOn() {
		return expectedOn;
	}

	public void setExpectedOn(Date expectedOn) {
		this.expectedOn = expectedOn;
	}

}
