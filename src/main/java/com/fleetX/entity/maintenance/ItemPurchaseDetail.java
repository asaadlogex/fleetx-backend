package com.fleetX.entity.maintenance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item_purchase_detail")
public class ItemPurchaseDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_purchase_detail_id")
	private Integer itemPurchaseDetailId;
	@ManyToOne
	@JoinColumn(name = "item_id", referencedColumnName = "item_id")
	private Item item;
	@Column(name = "qty_ordered")
	private Integer qtyOrdered;
	@Column(name = "qty_received")
	private Integer qtyReceived;
	@Column(name = "qty_remaining")
	private Integer qtyRemaining;
	@Column(name = "rate")
	private Double rate;
	@Column(name = "amount")
	private Double amount;
	@ManyToOne
	@JoinColumn(name = "purchase_order_id", referencedColumnName = "purchase_order_id")
	private PurchaseOrder purchaseOrder;

	public Integer getItemPurchaseDetailId() {
		return itemPurchaseDetailId;
	}

	public void setItemPurchaseDetailId(Integer itemPurchaseDetailId) {
		this.itemPurchaseDetailId = itemPurchaseDetailId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(Integer qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public Integer getQtyReceived() {
		return qtyReceived;
	}

	public void setQtyReceived(Integer qtyReceived) {
		this.qtyReceived = qtyReceived;
	}

	public Integer getQtyRemaining() {
		return qtyRemaining;
	}

	public void setQtyRemaining(Integer qtyRemaining) {
		this.qtyRemaining = qtyRemaining;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

}
