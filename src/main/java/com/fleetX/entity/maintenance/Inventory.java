package com.fleetX.entity.maintenance;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventory")
public class Inventory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inventory_id")
	private Integer inventoryId;
	@OneToOne
	@JoinColumn(name = "item_id", referencedColumnName = "item_id")
	private Item item;
	@Column(name = "inventory_start_date")
	private Date inventoryStartDate;
	@Column(name = "last_updated_on")
	private Date lastUpdatedOn;
	@Column(name = "starting_qty")
	private Integer startingQty;
	@Column(name = "starting_cost")
	private Double startingCost;
	@Column(name = "critical_qty")
	private Integer criticalQty;
	@Column(name = "expected_qty")
	private Integer expectedQty;
	@Column(name = "qty_on_hand")
	private Integer qtyOnHand;
	@Column(name = "qty_committed")
	private Integer qtyCommitted;
	@Column(name = "adjusted_up")
	private Integer adjustedUp;
	@Column(name = "adjusted_down")
	private Integer adjustedDown;
	@Column(name = "qty_in")
	private Integer qtyIn;
	@Column(name = "qty_out")
	private Integer qtyOut;
	@Column(name = "avg_cost")
	private Double avgCost;
	@OneToMany(mappedBy = "inventory", cascade = CascadeType.REMOVE)
	private List<InventoryTransactionInfo> inventoryTransactions;

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Date getInventoryStartDate() {
		return inventoryStartDate;
	}

	public void setInventoryStartDate(Date inventoryStartDate) {
		this.inventoryStartDate = inventoryStartDate;
	}

	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public Integer getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(Integer qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public Integer getQtyCommitted() {
		return qtyCommitted;
	}

	public void setQtyCommitted(Integer qtyCommitted) {
		this.qtyCommitted = qtyCommitted;
	}

	public Double getAvgCost() {
		return avgCost;
	}

	public void setAvgCost(Double avgCost) {
		this.avgCost = avgCost;
	}

	public Integer getStartingQty() {
		return startingQty;
	}

	public void setStartingQty(Integer startingQty) {
		this.startingQty = startingQty;
	}

	public Double getStartingCost() {
		return startingCost;
	}

	public void setStartingCost(Double startingCost) {
		this.startingCost = startingCost;
	}

	public List<InventoryTransactionInfo> getInventoryTransactions() {
		return inventoryTransactions;
	}

	public void setInventoryTransactions(List<InventoryTransactionInfo> inventoryTransactions) {
		this.inventoryTransactions = inventoryTransactions;
	}

	public Integer getCriticalQty() {
		return criticalQty;
	}

	public void setCriticalQty(Integer criticalQty) {
		this.criticalQty = criticalQty;
	}

	public Integer getExpectedQty() {
		return expectedQty;
	}

	public void setExpectedQty(Integer expectedQty) {
		this.expectedQty = expectedQty;
	}

	public Integer getQtyIn() {
		return qtyIn;
	}

	public void setQtyIn(Integer qtyIn) {
		this.qtyIn = qtyIn;
	}

	public Integer getQtyOut() {
		return qtyOut;
	}

	public void setQtyOut(Integer qtyOut) {
		this.qtyOut = qtyOut;
	}

	public Integer getAdjustedUp() {
		return adjustedUp;
	}

	public void setAdjustedUp(Integer adjustedUp) {
		this.adjustedUp = adjustedUp;
	}

	public Integer getAdjustedDown() {
		return adjustedDown;
	}

	public void setAdjustedDown(Integer adjustedDown) {
		this.adjustedDown = adjustedDown;
	}

}
