package com.fleetX.entity.account;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_payment")
public class InvoicePayment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_payment_id")
	private Integer invoicePaymentId;
	@Column(name = "reference_number")
	private String referenceNumber;
	@Column(name = "payment_date")
	private Date paymentDate;
	@Column(name = "payment_amount")
	private Double paymentAmount;
	@Column(name = "payment_note")
	private String paymentNote;
	@ManyToOne
	@JoinColumn(name = "invoice_id", referencedColumnName = "invoice_id")
	private Invoice invoice;
	@ManyToOne
	@JoinColumn(name = "dh_invoice_id", referencedColumnName = "dh_invoice_id")
	private DHInvoice dhInvoice;

	public Integer getInvoicePaymentId() {
		return invoicePaymentId;
	}

	public void setInvoicePaymentId(Integer invoicePaymentId) {
		this.invoicePaymentId = invoicePaymentId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentNote() {
		return paymentNote;
	}

	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public DHInvoice getDhInvoice() {
		return dhInvoice;
	}

	public void setDhInvoice(DHInvoice dhInvoice) {
		this.dhInvoice = dhInvoice;
	}

}
