package com.fleetX.entity.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fleetX.entity.asset.AssetCombination;

@Entity
@Table(name = "fixed_line_item")
public class FixedLineItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fixed_line_item_id")
	private Integer fixedLineItemId;
	@OneToOne
	@JoinColumn(name = "asset_combination_id", referencedColumnName = "asset_combination_id")
	private AssetCombination assetCombination;
	@ManyToOne
	@JoinColumn(name = "invoice_id", referencedColumnName = "invoice_id")
	private Invoice invoice;

	public Integer getFixedLineItemId() {
		return fixedLineItemId;
	}

	public void setFixedLineItemId(Integer fixedLineItemId) {
		this.fixedLineItemId = fixedLineItemId;
	}

	public AssetCombination getAssetCombination() {
		return assetCombination;
	}

	public void setAssetCombination(AssetCombination assetCombinations) {
		this.assetCombination = assetCombinations;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

}
