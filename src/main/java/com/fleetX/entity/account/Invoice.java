package com.fleetX.entity.account;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DState;

@Entity
@Table(name = "invoice")
public class Invoice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_id")
	private Integer invoiceId;
	@Column(name = "invoice_date")
	private Date invoiceDate;
	@ManyToOne
	@JoinColumn(name = "sender_id", referencedColumnName = "ac_id")
	private AdminCompany sender;
	@ManyToOne
	@JoinColumn(name = "recipient_id", referencedColumnName = "company_id")
	private Company recipient;
	@ManyToOne
	@JoinColumn(name = "state_id", referencedColumnName = "state_id")
	private DState taxState;
	@Column(name = "tax_in_percent")
	private Double tax;
	@Column(name = "total_amount")
	private Double totalAmount;
	@Column(name = "amount_paid")
	private Double amountPaid;
	@ManyToOne
	@JoinColumn(name = "payment_status", referencedColumnName = "payment_status_name")
	private DPaymentStatus paymentStatus;
	@ManyToOne
	@JoinColumn(name = "invoice_type", referencedColumnName = "invoice_type_name")
	private DInvoiceType invoiceType;
	@Column(name = "note")
	private String note;
	@OneToMany(mappedBy = "invoice", cascade = CascadeType.REMOVE)
	private List<LineItem> lineItems;
	@Column(name = "invoice_month")
	private Integer invoiceMonth;
	@Column(name = "invoice_year")
	private Integer invoiceYear;
	@OneToMany(mappedBy = "invoice", cascade = CascadeType.REMOVE)
	private List<FixedLineItem> fixedLineItems;
	@OneToMany(mappedBy = "invoice")
	private List<InvoicePayment> invoicePayments;
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "dh_invoice_id", referencedColumnName = "dh_invoice_id")
	private DHInvoice dhInvoice;

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public AdminCompany getSender() {
		return sender;
	}

	public void setSender(AdminCompany sender) {
		this.sender = sender;
	}

	public Company getRecipient() {
		return recipient;
	}

	public void setRecipient(Company recipient) {
		this.recipient = recipient;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public DPaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(DPaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public DInvoiceType getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(DInvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public List<FixedLineItem> getFixedLineItems() {
		return fixedLineItems;
	}

	public void setFixedLineItems(List<FixedLineItem> fixedLineItems) {
		this.fixedLineItems = fixedLineItems;
	}

	public Integer getInvoiceMonth() {
		return invoiceMonth;
	}

	public void setInvoiceMonth(Integer invoiceMonth) {
		this.invoiceMonth = invoiceMonth;
	}

	public Integer getInvoiceYear() {
		return invoiceYear;
	}

	public void setInvoiceYear(Integer invoiceYear) {
		this.invoiceYear = invoiceYear;
	}

	public DState getTaxState() {
		return taxState;
	}

	public void setTaxState(DState taxState) {
		this.taxState = taxState;
	}

	public List<InvoicePayment> getInvoicePayments() {
		return invoicePayments;
	}

	public void setInvoicePayments(List<InvoicePayment> invoicePayments) {
		this.invoicePayments = invoicePayments;
	}

	public DHInvoice getDhInvoice() {
		return dhInvoice;
	}

	public void setDhInvoice(DHInvoice dhInvoice) {
		this.dhInvoice = dhInvoice;
	}

}
