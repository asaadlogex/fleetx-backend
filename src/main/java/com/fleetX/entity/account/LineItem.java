package com.fleetX.entity.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fleetX.entity.order.RoadwayBill;

@Entity
@Table(name = "line_item")
public class LineItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "line_item_id")
	private Integer lineItemId;
	@ManyToOne
	@JoinColumn(name = "roadwayBill_id", referencedColumnName = "rwb_id")
	private RoadwayBill roadwayBill;
	@ManyToOne
	@JoinColumn(name = "invoice_id", referencedColumnName = "invoice_id")
	private Invoice invoice;
	@ManyToOne
	@JoinColumn(name = "dh_invoice_id", referencedColumnName = "dh_invoice_id")
	private DHInvoice dhInvoice;

	public Integer getLineItemId() {
		return lineItemId;
	}

	public void setLineItemId(Integer lineItemId) {
		this.lineItemId = lineItemId;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public RoadwayBill getRoadwayBill() {
		return roadwayBill;
	}

	public void setRoadwayBill(RoadwayBill roadwayBill) {
		this.roadwayBill = roadwayBill;
	}

	public DHInvoice getDhInvoice() {
		return dhInvoice;
	}

	public void setDhInvoice(DHInvoice dhInvoice) {
		this.dhInvoice = dhInvoice;
	}

}
