package com.fleetX.entity.account;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.RoadwayBill;

@Entity
@Table(name = "reimbursement_info")
public class ReimbursementInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reimbursement_info_id")
	private Integer reimbursementInfoId;
	@Column(name = "reference_number")
	private String referenceNumber;
	@Column(name = "reimbursement_date")
	private Date reimbursementDate;
	@Column(name = "reimbursement_amount")
	private Double reimbursementAmount;
	@Column(name = "reimbursement_note")
	private String reimbursementNote;
	@OneToMany
	@JoinTable(name = "reimbursment_jobs", joinColumns = @JoinColumn(name = "reimbursement_info_id", referencedColumnName = "reimbursement_info_id"), inverseJoinColumns = @JoinColumn(name = "job_id", referencedColumnName = "job_id"))
	private List<Job> jobs;

	public Integer getReimbursementInfoId() {
		return reimbursementInfoId;
	}

	public void setReimbursementInfoId(Integer reimbursementInfoId) {
		this.reimbursementInfoId = reimbursementInfoId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getReimbursementDate() {
		return reimbursementDate;
	}

	public void setReimbursementDate(Date reimbursementDate) {
		this.reimbursementDate = reimbursementDate;
	}

	public Double getReimbursementAmount() {
		return reimbursementAmount;
	}

	public void setReimbursementAmount(Double reimbursementAmount) {
		this.reimbursementAmount = reimbursementAmount;
	}

	public String getReimbursementNote() {
		return reimbursementNote;
	}

	public void setReimbursementNote(String reimbursementNote) {
		this.reimbursementNote = reimbursementNote;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

}
