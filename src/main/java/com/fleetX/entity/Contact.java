package com.fleetX.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DChannel;

import io.jsonwebtoken.lang.Assert;

@Entity
@Table(name = "contact")
@Where(clause="is_deleted=false")
public class Contact implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contact_id")
	private Integer contactId;
	@Column(name = "contact_data")
	private String data;
	@ManyToOne
	@JoinColumn(name = "channel_name", referencedColumnName = "channel_name")
	private DChannel channel;
	@Column(name = "isDeleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public DChannel getChannel() {
		return channel;
	}

	public void setChannel(DChannel channel) {
		this.channel = channel;
	}

	@Override
	public boolean equals(Object obj) {
		Assert.isInstanceOf(Contact.class, obj);
		Contact that = (Contact) obj;
		return obj != null && this.contactId != null && that.contactId != null ? this.contactId == that.getContactId()
				: false;
	}
}
