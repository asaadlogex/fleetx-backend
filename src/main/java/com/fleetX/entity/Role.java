package com.fleetX.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fleetX.entity.dropdown.DPrivilege;

@Entity
@Table(name = "role")
public class Role implements Serializable {
	@Id
	@Column(name = "role_name")
	private String roleName;
	@ManyToMany(mappedBy = "roles")
	private List<User> users;
	@ManyToMany
	@JoinTable(name = "roles_privileges", joinColumns = @JoinColumn(name = "role_name", referencedColumnName = "role_name"), inverseJoinColumns = @JoinColumn(name = "privilege_name", referencedColumnName = "privilege_name"))
	private List<DPrivilege> privileges=new ArrayList<>();

	public List<User> getUsers() {
		return users;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<DPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<DPrivilege> privileges) {
		this.privileges = privileges;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
