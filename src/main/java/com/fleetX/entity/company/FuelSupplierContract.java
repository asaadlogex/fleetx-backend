package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "fuel_supplier_contract")
@Where(clause="is_deleted=false")
public class FuelSupplierContract implements Serializable {
	@Id
	@GeneratedValue(generator = "fsc-generator")
	@GenericGenerator(name = "fsc-generator", parameters = { @Parameter(name = "prefix", value = "FSC"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "fsc_id")
	private String fscId;
	@ManyToOne
	@JoinColumn(name = "ac_id", referencedColumnName = "ac_id")
	private AdminCompany adminCompany;
	@ManyToOne
	@JoinColumn(name = "supplier_id", referencedColumnName = "company_id")
	private Company supplier;
	@Column(name = "fsc_status")
	private String fscStatus;
	@OneToMany(mappedBy = "fuelSupplierContract", cascade = CascadeType.REMOVE)
	private List<FuelRate> fuelRates;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getFscStatus() {
		return fscStatus;
	}

	public void setFscStatus(String fscStatus) {
		this.fscStatus = fscStatus;
	}

	public String getFscId() {
		return fscId;
	}

	public void setFscId(String fscId) {
		this.fscId = fscId;
	}

	public AdminCompany getAdminCompany() {
		return adminCompany;
	}

	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}

	public Company getSupplier() {
		return supplier;
	}

	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}

	public List<FuelRate> getFuelRates() {
		return fuelRates;
	}

	public void setFuelRates(List<FuelRate> fuelRates) {
		this.fuelRates = fuelRates;
	}
}
