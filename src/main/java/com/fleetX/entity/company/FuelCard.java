package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name="fuel_card")
@Where(clause="is_deleted=false")
public class FuelCard implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="fuel_card_id")
	private Integer fuelCardId;
	@Column(name="fuel_card_number")
	private String fuelCardNumber;
	@Column(name="fuel_card_expiry")
	private Date expiry;
	@Column(name="fuel_card_limit")
	private Double limit;
	@ManyToOne
	@JoinColumn(name="supplier_id",referencedColumnName = "company_id")
	private Company supplier;
	@Column(name="fuel_card_status")
	private String status;
	@ManyToOne
	@JoinColumn(name="customer_id",referencedColumnName = "company_id")
	private Company customer;
	@ManyToOne
	@JoinColumn(name="ac_id",referencedColumnName = "ac_id")
	private AdminCompany adminCompany;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getFuelCardId() {
		return fuelCardId;
	}
	public void setFuelCardId(Integer fuelCardId) {
		this.fuelCardId = fuelCardId;
	}
	public String getFuelCardNumber() {
		return fuelCardNumber;
	}
	public void setFuelCardNumber(String fuelCardNumber) {
		this.fuelCardNumber = fuelCardNumber;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	public Double getLimit() {
		return limit;
	}
	public void setLimit(Double limit) {
		this.limit = limit;
	}
	public Company getSupplier() {
		return supplier;
	}
	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}
	public Company getCustomer() {
		return customer;
	}
	public void setCustomer(Company customer) {
		this.customer = customer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public AdminCompany getAdminCompany() {
		return adminCompany;
	}
	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}
}
