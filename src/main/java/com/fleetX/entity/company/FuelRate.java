package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name="fuel_rate")
@Where (clause="is_deleted=false")
public class FuelRate implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="fuel_rate_id")
	private Integer fuelRateId;
	@Column(name="fuel_rate_start")
	private Date effectiveFrom;
	@Column(name="fuel_rate_end")
	private Date effectiveTill;
	@Column(name="rate_per_litre")
	private Double ratePerLitre;
	@ManyToOne
	@JoinColumn(name="fsc_id",referencedColumnName = "fsc_id")
	private FuelSupplierContract fuelSupplierContract;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public FuelSupplierContract getFuelSupplierContract() {
		return fuelSupplierContract;
	}
	public void setFuelSupplierContract(FuelSupplierContract fuelSupplierContract) {
		this.fuelSupplierContract = fuelSupplierContract;
	}
	public Integer getFuelRateId() {
		return fuelRateId;
	}
	public void setFuelRateId(Integer fuelRateId) {
		this.fuelRateId = fuelRateId;
	}
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(Date startDate) {
		this.effectiveFrom = startDate;
	}
	public Date getEffectiveTill() {
		return effectiveTill;
	}
	public void setEffectiveTill(Date endDate) {
		this.effectiveTill = endDate;
	}
	public Double getRatePerLitre() {
		return ratePerLitre;
	}
	public void setRatePerLitre(Double ratePerLitre) {
		this.ratePerLitre = ratePerLitre;
	}
}
