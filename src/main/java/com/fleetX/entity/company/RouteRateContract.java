package com.fleetX.entity.company;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "route_rate_contract")
@Where(clause="is_deleted=false")
public class RouteRateContract implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "route_rate_contract_id")
	private Integer routeRateContractId;
	@ManyToOne
	@JoinColumn(name = "route_id", referencedColumnName = "route_id")
	private Route route;
	@OneToMany(mappedBy = "routeRateContract", cascade = CascadeType.REMOVE)
	private List<RouteRate> routeRates;
	@Column(name = "status")
	private String status;
	@ManyToOne
	@JoinColumn(name = "company_contract_id", referencedColumnName = "company_contract_id")
	private CompanyContract companyContract;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getRouteRateContractId() {
		return routeRateContractId;
	}

	public void setRouteRateContractId(Integer routeRateContractId) {
		this.routeRateContractId = routeRateContractId;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public List<RouteRate> getRouteRates() throws ParseException {
		return this.routeRates.stream().sorted((RouteRate x, RouteRate y) -> {
			return y.getEffectiveFrom().compareTo(x.getEffectiveFrom());
		}).collect(Collectors.toList());
	}

	public void setRouteRates(List<RouteRate> routeRates) {
		this.routeRates = routeRates;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CompanyContract getCompanyContract() {
		return companyContract;
	}

	public void setCompanyContract(CompanyContract companyContract) {
		this.companyContract = companyContract;
	}
}
