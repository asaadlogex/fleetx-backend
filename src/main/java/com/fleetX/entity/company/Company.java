package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.dropdown.DBusinessType;
import com.fleetX.entity.dropdown.DCompanyType;
import com.fleetX.entity.dropdown.DFormation;
import com.fleetX.entity.dropdown.DSupplierType;
import com.fleetX.entity.order.Warehouse;

@Entity
@Table(name = "company")
@Where(clause = "is_deleted=false")
public class Company implements Serializable {
	@Id
	@GeneratedValue(generator = "company-generator")
	@GenericGenerator(name = "company-generator", parameters = { @Parameter(name = "prefix", value = "COMPANY"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "company_id")
	private String companyId;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "company_code")
	private String companyCode;
	@ManyToOne
	@JoinColumn(name = "company_type", referencedColumnName = "company_type_name")
	private DCompanyType companyType;
	@ManyToOne
	@JoinColumn(name = "formation_type", referencedColumnName = "formation_type_name")
	private DFormation companyFormation;
	@Column(name = "ntn_number")
	private String ntnNumber;
	@Column(name = "strn_number")
	private String strnNumber;
	@Column(name = "status")
	private String status;
	@Column(name = "pra")
	private String pra;
	@Column(name = "kra")
	private String kra;
	@Column(name = "bra")
	private String bra;
	@Column(name = "sra")
	private String sra;
	@ManyToOne
	@JoinColumn(name = "business_type", referencedColumnName = "business_type_name")
	private DBusinessType businessType;
	@Column(name = "document_url")
	private String documentUrl;
	@ManyToOne
	@JoinColumn(name = "supplier_type", referencedColumnName = "supplier_type_name")
	private DSupplierType supplierType;
	@OneToMany(mappedBy = "customer", cascade = CascadeType.REMOVE)
	private List<FuelCard> fuelCards = new ArrayList<>();
	@OneToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "company_address", joinColumns = @JoinColumn(name = "company_id", referencedColumnName = "company_id"), inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "address_id"))
	private List<Address> companyAddresses = new ArrayList<>();
	@OneToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "company_contact", joinColumns = @JoinColumn(name = "company_id", referencedColumnName = "company_id"), inverseJoinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "contact_id"))
	private List<Contact> companyContacts = new ArrayList<>();
	@OneToMany(mappedBy = "customer", cascade = CascadeType.REMOVE)
	private List<CompanyContract> companyContracts = new ArrayList<>();
	@OneToMany(mappedBy = "supplier", cascade = CascadeType.REMOVE)
	private List<FuelSupplierContract> companyFuelContracts = new ArrayList<>();
	@ManyToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "company_warehouse", joinColumns = @JoinColumn(name = "company_id", referencedColumnName = "company_id"), inverseJoinColumns = @JoinColumn(name = "warehouse_id", referencedColumnName = "warehouse_id"))
	private List<Warehouse> warehouses = new ArrayList<>();
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<Warehouse> getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(List<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public DCompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(DCompanyType companyType) {
		this.companyType = companyType;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public DFormation getCompanyFormation() {
		return companyFormation;
	}

	public void setCompanyFormation(DFormation companyFormation) {
		this.companyFormation = companyFormation;
	}

	public String getNtnNumber() {
		return ntnNumber;
	}

	public void setNtnNumber(String ntnNumber) {
		this.ntnNumber = ntnNumber;
	}

	public String getStrnNumber() {
		return strnNumber;
	}

	public void setStrnNumber(String strnNumber) {
		this.strnNumber = strnNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPra() {
		return pra;
	}

	public void setPra(String pra) {
		this.pra = pra;
	}

	public String getKra() {
		return kra;
	}

	public void setKra(String kra) {
		this.kra = kra;
	}

	public String getBra() {
		return bra;
	}

	public void setBra(String bra) {
		this.bra = bra;
	}

	public DBusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(DBusinessType businessType) {
		this.businessType = businessType;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public DSupplierType getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(DSupplierType supplierType) {
		this.supplierType = supplierType;
	}

	public List<FuelCard> getFuelCards() {
		return fuelCards;
	}

	public void setFuelCards(List<FuelCard> fuelCards) {
		this.fuelCards = fuelCards;
	}

	public List<Address> getCompanyAddresses() {
		return companyAddresses;
	}

	public void setCompanyAddresses(List<Address> companyAddresses) {
		this.companyAddresses = companyAddresses;
	}

	public List<Contact> getCompanyContacts() {
		return companyContacts;
	}

	public void setCompanyContacts(List<Contact> companyContacts) {
		this.companyContacts = companyContacts;
	}

	public List<CompanyContract> getCompanyContracts() {
		return companyContracts;
	}

	public void setCompanyContracts(List<CompanyContract> companyContracts) {
		this.companyContracts = companyContracts;
	}

	public List<FuelSupplierContract> getCompanyFuelContracts() {
		return companyFuelContracts;
	}

	public void setCompanyFuelContracts(List<FuelSupplierContract> companyFuelContracts) {
		this.companyFuelContracts = companyFuelContracts;
	}

	public String getSra() {
		return sra;
	}

	public void setSra(String sra) {
		this.sra = sra;
	}

}
