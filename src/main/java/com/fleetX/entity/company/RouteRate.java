package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

@Entity
@Table(name = "route_rate")
@Where(clause = "is_deleted=false")
public class RouteRate implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "route_rate_id")
	private Integer routeRateId;
	@ManyToOne
	@JoinColumn(name = "trailer_type", referencedColumnName = "trailer_type_name")
	private DTrailerType trailerType;
	@ManyToOne
	@JoinColumn(name = "trailer_size", referencedColumnName = "trailer_size_name")
	private DTrailerSize trailerSize;
	@Column(name = "weight_low")
	private Double weightLow;
	@Column(name = "weight_high")
	private Double weightHigh;
	@Column(name = "rate_per_km")
	private Double ratePerKm;
	@Column(name = "rate_per_ton")
	private Double ratePerTon;
	@Column(name = "effective_from")
	private Date effectiveFrom;
	@Column(name = "effective_till")
	private Date effectiveTill;
	@Column(name = "total_amount")
	private Double totalAmount;
	@Column(name = "status")
	private String status;
	@ManyToOne
	@JoinColumn(name = "route_rate_contract_id", referencedColumnName = "route_rate_contract_id")
	private RouteRateContract routeRateContract;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getRouteRateId() {
		return routeRateId;
	}

	public void setRouteRateId(Integer routeRateId) {
		this.routeRateId = routeRateId;
	}

	public Double getRatePerKm() {
		return ratePerKm;
	}

	public void setRatePerKm(Double ratePerKm) {
		this.ratePerKm = ratePerKm;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTill() {
		return effectiveTill;
	}

	public void setEffectiveTill(Date effectiveTill) {
		this.effectiveTill = effectiveTill;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RouteRateContract getRouteRateContract() {
		return routeRateContract;
	}

	public void setRouteRateContract(RouteRateContract routeRateContract) {
		this.routeRateContract = routeRateContract;
	}

	public DTrailerType getTrailerType() {
		return trailerType;
	}

	public void setTrailerType(DTrailerType trailerType) {
		this.trailerType = trailerType;
	}

	public DTrailerSize getTrailerSize() {
		return trailerSize;
	}

	public void setTrailerSize(DTrailerSize trailerSize) {
		this.trailerSize = trailerSize;
	}

	public Double getWeightLow() {
		return weightLow;
	}

	public void setWeightLow(Double weightLow) {
		this.weightLow = weightLow;
	}

	public Double getWeightHigh() {
		return weightHigh;
	}

	public void setWeightHigh(Double weightHigh) {
		this.weightHigh = weightHigh;
	}

	public Double getRatePerTon() {
		return ratePerTon;
	}

	public void setRatePerTon(Double ratePerTon) {
		this.ratePerTon = ratePerTon;
	}
}
