package com.fleetX.entity.company;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DCity;

@Entity
@Table(name = "route")
@Where(clause = "is_deleted=false")
public class Route implements Serializable {
	@Id
	@GeneratedValue(generator = "route-generator")
	@GenericGenerator(name = "route-generator", parameters = { @Parameter(name = "prefix", value = "ROUTE"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "route_id")
	private String routeId;
	@ManyToOne
	@JoinColumn(name = "route_to", referencedColumnName = "city_code")
	private DCity routeTo;
	@ManyToOne
	@JoinColumn(name = "route_from", referencedColumnName = "city_code")
	private DCity routeFrom;
	@Column(name = "avg_distance_in_km")
	private Double avgDistanceInKM;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRouteId() {
		return routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public DCity getRouteTo() {
		return routeTo;
	}

	public void setRouteTo(DCity routeTo) {
		this.routeTo = routeTo;
	}

	public DCity getRouteFrom() {
		return routeFrom;
	}

	public void setRouteFrom(DCity routeFrom) {
		this.routeFrom = routeFrom;
	}

	public Double getAvgDistanceInKM() {
		return avgDistanceInKM;
	}

	public void setAvgDistanceInKM(Double avgDistanceInKM) {
		this.avgDistanceInKM = avgDistanceInKM;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Route))
			return false;
		Route other = (Route) obj;
		if (routeFrom == null) {
			if (other.routeFrom != null)
				return false;
		} else if (!routeFrom.getCode().equals(other.routeFrom.getCode()))
			return false;
		if (routeTo == null) {
			if (other.routeTo != null)
				return false;
		} else if (!routeTo.getCode().equals(other.routeTo.getCode()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return routeFrom.getCode() + "-" + routeTo.getCode();
	}

}
