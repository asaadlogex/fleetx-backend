package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.dropdown.DBusinessType;
import com.fleetX.entity.dropdown.DFormation;
import com.fleetX.entity.employee.Employee;

@Entity
@Table(name = "admin_company")
@Where(clause = "is_deleted=false")
public class AdminCompany implements Serializable {
	@Id
	@GeneratedValue(generator = "admin-company-generator")
	@GenericGenerator(name = "admin-company-generator", parameters = { @Parameter(name = "prefix", value = "SUPER"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "ac_id")
	private String adminCompanyId;
	@Column(name = "ac_name")
	private String adminCompanyName;
	@Column(name = "ac_code")
	private String adminCompanyCode;
	@ManyToOne
	@JoinColumn(name = "ac_formation", referencedColumnName = "formation_type_name")
	private DFormation adminCompanyFormation;
	@Column(name = "ac_ntn_number")
	private String ntnNumber;
	@Column(name = "ac_strn_number")
	private String strnNumber;
	@Column(name = "ac_status")
	private String status;
	@Column(name = "ac_pra")
	private String pra;
	@Column(name = "ac_kra")
	private String kra;
	@Column(name = "ac_bra")
	private String bra;
	@Column(name = "ac_sra")
	private String sra;
	@ManyToOne
	@JoinColumn(name = "ac_business_type", referencedColumnName = "business_type_name")
	private DBusinessType businessType;
	@OneToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "admin_company_address", joinColumns = @JoinColumn(name = "ac_id", referencedColumnName = "ac_id"), inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "address_id"))
	private List<Address> adminCompanyAddresses;
	@OneToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "admin_company_contact", joinColumns = @JoinColumn(name = "ac_id", referencedColumnName = "ac_id"), inverseJoinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "contact_id"))
	private List<Contact> adminCompanyContacts;
	@Column(name = "ac_document_url")
	private String documentUrl;
	@OneToMany(mappedBy = "adminCompany", cascade = CascadeType.REMOVE)
	private List<Employee> adminCompanyEmployees;
	@OneToMany(mappedBy = "adminCompany", cascade = CascadeType.REMOVE)
	private List<FuelCard> fuelCards;
	@Column(name = "isDeleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<FuelCard> getFuelCards() {
		return fuelCards;
	}

	public void setFuelCards(List<FuelCard> fuelCards) {
		this.fuelCards = fuelCards;
	}

	public List<Address> getAdminCompanyAddresses() {
		return adminCompanyAddresses;
	}

	public void setAdminCompanyAddresses(List<Address> adminCompanyAddresses) {
		this.adminCompanyAddresses = adminCompanyAddresses;
	}

	public List<Contact> getAdminCompanyContacts() {
		return adminCompanyContacts;
	}

	public void setAdminCompanyContacts(List<Contact> adminCompanyContacts) {
		this.adminCompanyContacts = adminCompanyContacts;
	}

	public String getAdminCompanyId() {
		return adminCompanyId;
	}

	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}

	public String getAdminCompanyName() {
		return adminCompanyName;
	}

	public void setAdminCompanyName(String adminCompanyName) {
		this.adminCompanyName = adminCompanyName;
	}

	public String getAdminCompanyCode() {
		return adminCompanyCode;
	}

	public void setAdminCompanyCode(String adminCompanyCode) {
		this.adminCompanyCode = adminCompanyCode;
	}

	public DFormation getAdminCompanyFormation() {
		return adminCompanyFormation;
	}

	public void setAdminCompanyFormation(DFormation adminCompanyFormation) {
		this.adminCompanyFormation = adminCompanyFormation;
	}

	public List<Employee> getAdminCompanyEmployees() {
		return adminCompanyEmployees;
	}

	public void setAdminCompanyEmployees(List<Employee> adminCompanyEmployees) {
		this.adminCompanyEmployees = adminCompanyEmployees;
	}

	public String getNtnNumber() {
		return ntnNumber;
	}

	public void setNtnNumber(String ntnNumber) {
		this.ntnNumber = ntnNumber;
	}

	public String getStrnNumber() {
		return strnNumber;
	}

	public void setStrnNumber(String strnNumber) {
		this.strnNumber = strnNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPra() {
		return pra;
	}

	public void setPra(String pra) {
		this.pra = pra;
	}

	public String getKra() {
		return kra;
	}

	public void setKra(String kra) {
		this.kra = kra;
	}

	public String getBra() {
		return bra;
	}

	public void setBra(String bra) {
		this.bra = bra;
	}

	public DBusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(DBusinessType businessType) {
		this.businessType = businessType;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public String getSra() {
		return sra;
	}

	public void setSra(String sra) {
		this.sra = sra;
	}
}
