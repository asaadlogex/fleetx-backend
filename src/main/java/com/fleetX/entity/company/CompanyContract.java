package com.fleetX.entity.company;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.dropdown.DContractType;

@Entity
@Table(name = "company_contract")
@Where(clause = "is_deleted=false")
public class CompanyContract implements Serializable {
	@Id
	@GeneratedValue(generator = "contract-generator")
	@GenericGenerator(name = "contract-generator", parameters = { @Parameter(name = "prefix", value = "CONTRACT"),
			@Parameter(name = "initialValue", value = "1") }, strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "company_contract_id")
	private String companyContractId;
	@OneToOne
	@JoinColumn(name = "ac_id", referencedColumnName = "ac_id")
	private AdminCompany adminCompany;
	@ManyToOne
	@JoinColumn(name = "customer_id", referencedColumnName = "company_id")
	private Company customer;
	@Column(name = "contract_start")
	private Date contractStart;
	@Column(name = "contract_end")
	private Date contractEnd;
	@ManyToOne
	@JoinColumn(name = "contract_type", referencedColumnName = "contract_type_name")
	private DContractType contractType;
	@Column(name = "fixed_rate")
	private Double fixedRate;
	@Column(name = "halting_rate")
	private Double haltingRate;
	@Column(name = "detention_rate")
	private Double detentionRate;
	@OneToMany(mappedBy = "companyContract")
	private List<AssetCombination> assetCombinations;
	@OneToMany(mappedBy = "companyContract", cascade = CascadeType.REMOVE)
	private List<RouteRateContract> routeRateContracts;
	@Column(name = "status")
	private String status;
	@Column(name = "contract_doc_url")
	private String contractDocUrl;
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCompanyContractId() {
		return companyContractId;
	}

	public void setCompanyContractId(String companyContractId) {
		this.companyContractId = companyContractId;
	}

	public AdminCompany getAdminCompany() {
		return adminCompany;
	}

	public void setAdminCompany(AdminCompany adminCompany) {
		this.adminCompany = adminCompany;
	}

	public Company getCustomer() {
		return customer;
	}

	public void setCustomer(Company customer) {
		this.customer = customer;
	}

	public Date getContractStart() {
		return contractStart;
	}

	public void setContractStart(Date contractStart) {
		this.contractStart = contractStart;
	}

	public Date getContractEnd() {
		return contractEnd;
	}

	public void setContractEnd(Date contractEnd) {
		this.contractEnd = contractEnd;
	}

	public DContractType getContractType() {
		return contractType;
	}

	public void setContractType(DContractType contractType) {
		this.contractType = contractType;
	}

	public Double getFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(Double fixedRate) {
		this.fixedRate = fixedRate;
	}

	public List<AssetCombination> getAssetCombinations() {
		return assetCombinations;
	}

	public void setAssetCombinations(List<AssetCombination> assetCombinations) {
		this.assetCombinations = assetCombinations;
	}

	public List<RouteRateContract> getRouteRateContracts() {
		return routeRateContracts;
	}

	public void setRouteRateContracts(List<RouteRateContract> routeRates) {
		this.routeRateContracts = routeRates;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String contractStatus) {
		this.status = contractStatus;
	}

	public String getContractDocUrl() {
		return contractDocUrl;
	}

	public void setContractDocUrl(String contractDocUrl) {
		this.contractDocUrl = contractDocUrl;
	}

	public Double getHaltingRate() {
		return haltingRate;
	}

	public void setHaltingRate(Double haltingRate) {
		this.haltingRate = haltingRate;
	}

	public Double getDetentionRate() {
		return detentionRate;
	}

	public void setDetentionRate(Double detentionRate) {
		this.detentionRate = detentionRate;
	}
}
