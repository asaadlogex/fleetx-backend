package com.fleetX.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DState;

import io.jsonwebtoken.lang.Assert;

@Entity
@Table(name = "address")
@Where(clause = "is_deleted=false")
public class Address implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "address_id")
	private Integer addressId;
	@Column(name = "country")
	private String country;
	@ManyToOne
	@JoinColumn(name = "city", referencedColumnName = "city_name")
	private DCity city;
	@ManyToOne
	@JoinColumn(name = "state", referencedColumnName = "state_name")
	private DState state;
	@Column(name = "street1")
	private String street1;
	@Column(name = "street2")
	private String street2;
	@Column(name = "zip_code")
	private String zipCode;
	@Column(name = "longitude")
	private Double longitude;
	@Column(name = "latitude")
	private Double latitude;
	@Column(name = "isDeleted")
	private Boolean isDeleted = false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer AddressId) {
		this.addressId = AddressId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public DCity getCity() {
		return city;
	}

	public void setCity(DCity city) {
		this.city = city;
	}

	public DState getState() {
		return state;
	}

	public void setState(DState state) {
		this.state = state;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public boolean equals(Object obj) {
		Assert.isInstanceOf(Address.class, obj);
		Address that = (Address) obj;
		return obj != null && this.addressId != null && that.addressId != null ? this.addressId == that.getAddressId()
				: false;
	}

	@Override
	public String toString() {
		return street1 + ", " + city.getValue() + " " + zipCode + ", " + state.getValue() + ", " + country;
	}

}
