package com.fleetX.entity.employee;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

@Entity
@Table(name="training_profile")
@Where(clause="is_deleted=false")
public class TrainingProfile implements Serializable {
	@Id
	@GeneratedValue(generator = "tp-generator")
	@GenericGenerator(name = "tp-generator", 
    parameters = {@Parameter(name = "prefix", value = "TP"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "training_profile_id")
	private String trainingProfileId;
	@Column(name = "training_type")
	private String trainingType;
	@Column(name = "training_issued_date")
	private Date issuedDate;
	@Column(name = "training_expiry_date")
	private Date expiryDate;
	@Column(name = "trainer_name")
	private String trainerName;
	@Column(name = "training_station")
	private String trainingStation;
	@ManyToOne
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id")
	private Employee employee;
	@Column(name="training_certificate_url")
	private String trainingCertificateUrl;
	@Column(name="attendance_sheet_url")
	private String attendanceSheetUrl;
	@Column(name="picture_with_trainer_url")
	private String pictureWithTrainerUrl;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getTrainingCertificateUrl() {
		return trainingCertificateUrl;
	}

	public void setTrainingCertificateUrl(String trainingCertificateUrl) {
		this.trainingCertificateUrl = trainingCertificateUrl;
	}

	public String getAttendanceSheetUrl() {
		return attendanceSheetUrl;
	}

	public void setAttendanceSheetUrl(String attendanceSheetUrl) {
		this.attendanceSheetUrl = attendanceSheetUrl;
	}

	public String getPictureWithTrainerUrl() {
		return pictureWithTrainerUrl;
	}

	public void setPictureWithTrainerUrl(String pictureWithTrainerUrl) {
		this.pictureWithTrainerUrl = pictureWithTrainerUrl;
	}

	public String getTrainingProfileId() {
		return trainingProfileId;
	}

	public void setTrainingProfileId(String trainingProfileId) {
		this.trainingProfileId = trainingProfileId;
	}

	public String getTrainingType() {
		return trainingType;
	}

	public void setTrainingType(String trainingType) {
		this.trainingType = trainingType;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getTrainingStation() {
		return trainingStation;
	}

	public void setTrainingStation(String trainingStation) {
		this.trainingStation = trainingStation;
	}

	 public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}
