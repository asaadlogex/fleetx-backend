package com.fleetX.entity.employee;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

@Entity
@Table(name="heavy_vehicle_experience")
@Where(clause="is_deleted=false")
public class HeavyVehicleExperience implements Serializable {
	@Id
	@GeneratedValue(generator = "hve-generator")
	@GenericGenerator(name = "hve-generator", 
    parameters = {@Parameter(name = "prefix", value = "HVE"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name = "heavy_vehicle_experience_id")
	private String heavyVehicleExperienceId;
	@Column(name = "experience_start_date")
	private Date experienceStartDate;
	@Column(name = "experience_end_date")
	private Date experienceEndDate;
	@Column(name = "trucks_operated")
	private String trucksOperated;
	@Column(name = "experience_company_name")
	private String experienceCompanyName;
	@ManyToOne
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id")
	private Employee employee;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getHeavyVehicleExperienceId() {
		return heavyVehicleExperienceId;
	}

	public void setHeavyVehicleExperienceId(String heavyVehicleExperienceId) {
		this.heavyVehicleExperienceId = heavyVehicleExperienceId;
	}

	public Date getExperienceStartDate() {
		return experienceStartDate;
	}

	public void setExperienceStartDate(Date experienceStartDate) {
		this.experienceStartDate = experienceStartDate;
	}

	public Date getExperienceEndDate() {
		return experienceEndDate;
	}

	public void setExperienceEndDate(Date experienceEndDate) {
		this.experienceEndDate = experienceEndDate;
	}

	public String getTrucksOperated() {
		return trucksOperated;
	}

	public void setTrucksOperated(String trucksOperated) {
		this.trucksOperated = trucksOperated;
	}

	public String getExperienceCompanyName() {
		return experienceCompanyName;
	}

	public void setExperienceCompanyName(String experienceCompanyName) {
		this.experienceCompanyName = experienceCompanyName;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Employee getEmployee() {
		return employee;
	}
}
