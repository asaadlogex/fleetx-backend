package com.fleetX.entity.employee;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

@Entity
@Table(name="medical_test_report")
@Where(clause="is_deleted=false")
public class MedicalTestReport implements Serializable {
	@Id
	@GeneratedValue(generator = "mtr-generator")
	@GenericGenerator(name = "mtr-generator", 
    parameters = {@Parameter(name = "prefix", value = "MTR"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="mtr_id")
	private String mtrId;
	@Column(name="mtr_type")
	private String mtrType;
	@Column(name="mtr_date")
	private Date mtrDate;
	@Column(name="mtr_hospital")
	private String mtrHospital;
	@Column(name="mtr_station")
	private String mtrStation;
	@Column(name="mtr_result")
	private String mtrResult;
	@ManyToOne
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id")
	private Employee employee;
	@Column(name="url")
	private String url;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getMtrId() {
		return mtrId;
	}
	public void setMtrId(String mtrId) {
		this.mtrId = mtrId;
	}
	public String getMtrType() {
		return mtrType;
	}
	public void setMtrType(String mtrType) {
		this.mtrType = mtrType;
	}
	public Date getMtrDate() {
		return mtrDate;
	}
	public void setMtrDate(Date mtrDate) {
		this.mtrDate = mtrDate;
	}
	public String getMtrHospital() {
		return mtrHospital;
	}
	public void setMtrHospital(String mtrHospital) {
		this.mtrHospital = mtrHospital;
	}
	public String getMtrStation() {
		return mtrStation;
	}
	public void setMtrStation(String mtrStation) {
		this.mtrStation = mtrStation;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getMtrResult() {
		return mtrResult;
	}

	public void setMtrResult(String mtrResult) {
		this.mtrResult = mtrResult;
	}
	
	
}
