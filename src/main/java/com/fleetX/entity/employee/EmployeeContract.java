package com.fleetX.entity.employee;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name="employee_contract")
@Where(clause="is_deleted=false")
public class EmployeeContract implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="employee_contract_id")
	private Integer employeeContractId;
	@Column(name="employee_contract_start_date")
	private Date startDate;
	@Column(name="employee_contract_end_date")
	private Date endDate;
	@Column(name="employee_contract_salary")
	private Double salary;
	@Column(name="employee_contract_status")
	private String contractStatus;
	@Column(name="employee_separation_reason")
	private String separationReason;
	@Column(name="employee_working_experience")
	private String workingExperience;
	@Column(name="employee_previous_employer1")
	private String previousEmployer1;
	@Column(name="employee_previous_employer2")
	private String previousEmployer2;
	@ManyToOne
	@JoinColumn(name="employee_id",referencedColumnName="employee_id")
	private Employee employee;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Employee getEmployee() {
		return employee;
	}
	public Integer getEmployeeContractId() {
		return employeeContractId;
	}
	public void setEmployeeContractId(Integer employeeContractId) {
		this.employeeContractId = employeeContractId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public String getSeparationReason() {
		return separationReason;
	}
	public void setSeparationReason(String separationReason) {
		this.separationReason = separationReason;
	}
	public String getWorkingExperience() {
		return workingExperience;
	}
	public void setWorkingExperience(String workingExperience) {
		this.workingExperience = workingExperience;
	}
	public String getPreviousEmployer1() {
		return previousEmployer1;
	}
	public void setPreviousEmployer1(String previousEmployer1) {
		this.previousEmployer1 = previousEmployer1;
	}
	public String getPreviousEmployer2() {
		return previousEmployer2;
	}
	public void setPreviousEmployer2(String previousEmployer2) {
		this.previousEmployer2 = previousEmployer2;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
