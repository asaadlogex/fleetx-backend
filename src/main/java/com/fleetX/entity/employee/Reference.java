package com.fleetX.entity.employee;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

@Entity
@Table(name="reference")
@Where(clause="is_deleted=false")
public class Reference implements Serializable {
	@Id
	@GeneratedValue(generator = "ref-generator")
	@GenericGenerator(name = "ref-generator", 
    parameters = {@Parameter(name = "prefix", value = "REF"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="reference_id")
	private String referenceId;
	@Column(name="reference_name")
	private String referenceName;
	@Column(name="reference_contact")
	private String referenceContact;
	@Column(name="reference_relation")
	private String referenceRelation;
	@Column(name="reference_address")
	private String referenceAddress;
	@Column(name="reference_cnic")
	private String referenceCnic;
	@Column(name="cnic_url")
	private String cnicUrl;
	@Column(name="reference_form_url")
	private String referenceFormUrl;
	@ManyToOne
	@JoinColumn(name="employee_id",referencedColumnName="employee_id")
	private Employee employee;
	@Column(name="is_deleted")
	private Boolean isDeleted=false;

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getCnicUrl() {
		return cnicUrl;
	}
	public void setCnicUrl(String cnicUrl) {
		this.cnicUrl = cnicUrl;
	}
	public String getReferenceFormUrl() {
		return referenceFormUrl;
	}
	public void setReferenceFormUrl(String referenceFormUrl) {
		this.referenceFormUrl = referenceFormUrl;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getReferenceName() {
		return referenceName;
	}
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}
	public String getReferenceContact() {
		return referenceContact;
	}
	public void setReferenceContact(String referenceContact) {
		this.referenceContact = referenceContact;
	}
	public String getReferenceRelation() {
		return referenceRelation;
	}
	public void setReferenceRelation(String referenceRelation) {
		this.referenceRelation = referenceRelation;
	}
	public String getReferenceAddress() {
		return referenceAddress;
	}
	public void setReferenceAddress(String referenceAddress) {
		this.referenceAddress = referenceAddress;
	}
	public String getReferenceCnic() {
		return referenceCnic;
	}
	public void setReferenceCnic(String referenceCnic) {
		this.referenceCnic = referenceCnic;
	}
		
}
