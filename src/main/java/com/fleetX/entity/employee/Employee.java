package com.fleetX.entity.employee;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DDepartment;
import com.fleetX.entity.dropdown.DEmployeeType;
import com.fleetX.entity.dropdown.DPosition;

@Entity
@Table(name="employee")
@Where(clause="is_deleted=false")
public class Employee implements Serializable {
	@Id
	@GeneratedValue(generator = "employee-generator")
	@GenericGenerator(name = "employee-generator", 
    parameters = {@Parameter(name = "prefix", value = "EMPLOYEE"),@Parameter(name = "initialValue", value = "1")}, 
    strategy = "com.fleetX.config.CustomSequenceGenerator")
	@Column(name="employee_id")
	private String employeeId;
	@Column(name="employee_number", unique=true)
	private String employeeNumber;
	@Column(name="employee_first_name")
	private String firstName;
	@Column(name="employee_last_name")
	private String lastName;
	@Column(name="employee_status")
	private String status;
	@ManyToOne
	@JoinColumn(name="employee_department",referencedColumnName="department_name")
	private DDepartment department;
	@ManyToOne
	@JoinColumn(name="employee_position",referencedColumnName="position_name")
	private DPosition position;
	@ManyToOne
	@JoinColumn(name="ac_id",referencedColumnName="ac_id")
	private AdminCompany adminCompany;
	@ManyToOne
	@JoinColumn(name="address_id",referencedColumnName="address_id")
	private Address acAddress;
	@Column(name="employee_cnic", unique=true)
	private String cnic;
	@Column(name="employee_cnic_expiry")
	private Date cnicExpiry;
	@Column(name="employee_ntn_number")
	private String ntnNumber;
	@Column(name="employee_dob")
	private Date dateOfBirth;	
	@Column(name="employee_nok")
	private String nextOfKin;
	@Column(name="employee_nok_relation")
	private String nextOfKinRelation;
	@OneToMany(mappedBy="employee",cascade=CascadeType.REMOVE)
	private List<EmployeeContract> employeeContracts;
	@OneToMany(cascade=CascadeType.REMOVE)
	@JoinTable(name = "employee_address",joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "employee_id"), inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "address_id"))
	private List<Address> employeeAddresses;
	@OneToMany(cascade=CascadeType.REMOVE)
	@JoinTable(name = "employee_contact",joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "employee_id"), inverseJoinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "contact_id"))
	private List<Contact> employeeContacts;
	@Column(name="employee_picture_url")
	private String pictureUrl;
	@ManyToOne
	@JoinColumn(name="employee_type",referencedColumnName="employee_type_name")
	private DEmployeeType employeeType;
	@ManyToOne
	@JoinColumn(name="supplier_id",referencedColumnName="company_id")
	private Company supplier;
	@Column(name="employee_licence_number")
	private String licenceNumber;
	@Column(name="employee_licence_expiry")
	private Date licenceExpiry;
	@Column(name="employee_licence_class")
	private String licenceClass;
	@OneToMany(mappedBy="employee",cascade=CascadeType.REMOVE)
	private List<HeavyVehicleExperience> heavyVehicleExperience;
	@OneToMany(mappedBy="employee",cascade=CascadeType.REMOVE)
	private List<TrainingProfile> trainingProfiles;
	@OneToMany(mappedBy="employee",cascade=CascadeType.REMOVE)
	private List<MedicalTestReport> medicalTestReports;
	@OneToMany(mappedBy="employee",cascade=CascadeType.REMOVE)
	private List<Reference> references;
	@Column(name="availability")
	private String availability;
	@Column(name="moving_status")
	private String movingStatus;
	@Column(name = "isDeleted")
	private Boolean isDeleted=false;
	
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getMovingStatus() {
		return movingStatus;
	}
	public void setMovingStatus(String movingStatus) {
		this.movingStatus = movingStatus;
	}
	
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	
	public DEmployeeType getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(DEmployeeType employeeType) {
		this.employeeType = employeeType;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DDepartment getDepartment() {
		return department;
	}
	public void setDepartment(DDepartment department) {
		this.department = department;
	}
	public DPosition getPosition() {
		return position;
	}
	public void setPosition(DPosition position) {
		this.position = position;
	}
	public Address getAcAddress() {
		return acAddress;
	}
	public void setAcAddress(Address acAddress) {
		this.acAddress = acAddress;
	}
	public void setReferences(List<Reference> references) {
		this.references = references;
	}
	public String getCnic() {
		return cnic;
	}
	public void setCnic(String cnic) {
		this.cnic = cnic;
	}
	public Date getCnicExpiry() {
		return cnicExpiry;
	}
	public void setCnicExpiry(Date cnicExpiry) {
		this.cnicExpiry = cnicExpiry;
	}
	public String getNtnNumber() {
		return ntnNumber;
	}
	public void setNtnNumber(String ntnNumber) {
		this.ntnNumber = ntnNumber;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getNextOfKin() {
		return nextOfKin;
	}
	public void setNextOfKin(String nextOfKin) {
		this.nextOfKin = nextOfKin;
	}
	public String getNextOfKinRelation() {
		return nextOfKinRelation;
	}
	public void setNextOfKinRelation(String nextOfKinRelation) {
		this.nextOfKinRelation = nextOfKinRelation;
	}
	public List<EmployeeContract> getEmployeeContracts() {
		return employeeContracts;
	}
	public void setEmployeeContracts(List<EmployeeContract> employeeContracts) {
		this.employeeContracts = employeeContracts;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public List<Address> getEmployeeAddresses() {
		return employeeAddresses;
	}
	public void setEmployeeAddresses(List<Address> employeeAddresses) {
		this.employeeAddresses = employeeAddresses;
	}
	public List<Contact> getEmployeeContacts() {
		return employeeContacts;
	}
	public void setEmployeeContacts(List<Contact> employeeContacts) {
		this.employeeContacts = employeeContacts;
	}
	public void setAdminCompany(AdminCompany company) {
		this.adminCompany = company;
	}
	public Company getSupplier() {
		return supplier;
	}
	public void setSupplier(Company supplier) {
		this.supplier = supplier;
	}
	public AdminCompany getAdminCompany() {
		return adminCompany;
	}
	public String getLicenceNumber() {
		return licenceNumber;
	}
	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	public Date getLicenceExpiry() {
		return licenceExpiry;
	}
	public void setLicenceExpiry(Date licenceExpiry) {
		this.licenceExpiry = licenceExpiry;
	}
	public String getLicenceClass() {
		return licenceClass;
	}
	public void setLicenceClass(String licenceClass) {
		this.licenceClass = licenceClass;
	}
	public List<HeavyVehicleExperience> getHeavyVehicleExperience() {
		return heavyVehicleExperience;
	}
	public void setHeavyVehicleExperience(List<HeavyVehicleExperience> heavyVehicleExperience) {
		this.heavyVehicleExperience = heavyVehicleExperience;
	}
	public List<TrainingProfile> getTrainingProfiles() {
		return trainingProfiles;
	}
	public void setTrainingProfiles(List<TrainingProfile> trainingProfiles) {
		this.trainingProfiles = trainingProfiles;
	}
	public List<MedicalTestReport> getMedicalTestReports() {
		return medicalTestReports;
	}
	public void setMedicalTestReports(List<MedicalTestReport> medicalTestReports) {
		this.medicalTestReports = medicalTestReports;
	}
	public List<Reference> getReferences() {
		return references;
	}
	public void setReference(List<Reference> references) {
		this.references = references;
	}	
}
