package com.fleetX.config;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.fleetX.dto.ContactDTO;

import io.jsonwebtoken.lang.Assert;

@Component
public class SystemUtil {

//	private final SimpleDateFormat dateTimeFormat = new SimpleDateFormat(SystemType.DATE_TIME_FORMAT);
//	private final SimpleDateFormat dateFormat = new SimpleDateFormat(SystemType.DATE_FORMAT);
	private final DecimalFormat decimalFormat = new DecimalFormat(SystemType.TIME_DIFFERENCE_FORMAT);

	private SimpleDateFormat dateTimeFormatter() {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat(SystemType.DATE_TIME_FORMAT);
		dateTimeFormat.setTimeZone(TimeZone.getTimeZone(SystemType.TIME_ZONE));
		return dateTimeFormat;
	}

	private SimpleDateFormat dateFormatter() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(SystemType.DATE_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone(SystemType.TIME_ZONE));
		return dateFormat;
	}

	public Date getCurrentDate() {
		return new Date();
	}

	public Date getCustomDate(long msAfter) {
		return new Date(System.currentTimeMillis() + msAfter);
	}

	public Date getCustomDate(int day, int month, int year) {
		return new Date(year, month, day);
	}

	public Date convertToDateTime(String dateTime) throws ParseException {
		return dateTimeFormatter().parse(dateTime);
	}

	public Date convertToDate(String date) throws ParseException {
		if (date == null)
			return null;
		return dateFormatter().parse(date);
	}

	public String convertToDateTimeString(Date dateTime) {
		return dateTimeFormatter().format(dateTime);
	}

	public boolean isDateInBetween(Date dateToBeChecked, Date start, Date end) {
		if (dateToBeChecked == null)
			return true;
		if (start == null && end == null)
			return true;
		if (start == null && end != null)
			return dateToBeChecked.compareTo(end) <= 0;
		if (end == null && start != null)
			return dateToBeChecked.compareTo(start) >= 0;
		return dateToBeChecked.compareTo(start) >= 0 && dateToBeChecked.compareTo(end) <= 0;
	}

	public String convertToDateString(Date date) {
		return date == null ? null : dateFormatter().format(date);
	}

	public boolean isStartBeforeEnd(Date start, Date end) {
		if (start == null || end == null)
			return true;
		return start.compareTo(end) <= 0;
	}

	public boolean isDatePassed(Date date) {
		return date.compareTo(getCurrentDate()) < 0;
	}

	public Double differenceInDateTime(Date start, Date end) {
		if (start == null || end == null)
			return null;
		decimalFormat.setRoundingMode(RoundingMode.UP);
		Double endTime = Double.valueOf(end.getTime());
		Double startTime = Double.valueOf(start.getTime());
		Double difference = Math.abs(endTime - startTime);
		Double differenceInHours = difference / (1000 * 60 * 60);
		return Double.valueOf(decimalFormat.format(differenceInHours));
	}

	public boolean validateContact(ContactDTO contactDto) {
		switch (contactDto.getChannel().getValue()) {
		case SystemType.CHANNEL_PHONE:
			return validatePhone(contactDto.getData());
		case SystemType.CHANNEL_EMAIL:
			return validateEmail(contactDto.getData());
		case SystemType.CHANNEL_MOBILE:
			return validateMobile(contactDto.getData());
		default:
			return false;
		}
	}

	public boolean validatePhone(String data) {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean validateEmail(String data) {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean validateMobile(String data) {
		// TODO Auto-generated method stub
		return true;
	}

	public String getExtensionFromFileName(String fileName) {
		String[] parts = fileName.split("\\.");
		return parts.length > 0 ? parts[parts.length - 1] : null;
	}

//	public AdminCompany getAdminCompany() {
//		return adminService.findAllAdminCompany().get(0);
//	}
//
//	public String getAdminCompanyId() {
//		return adminService.findAllAdminCompany().get(0).getAdminCompanyId();
//	}
//
//	public Address getAdminCompanyAddress() {
//		return adminService.findAllAdminCompany().get(0).getAdminCompanyAddresses().get(0);
//	}
//
//	public AddressDTO getAdminCompanyAddressDTO() {
//		return generalMapper
//				.mapToAddressDTO(adminService.findAllAdminCompany().get(0).getAdminCompanyAddresses().get(0));
//	}

	public String getAmountInWords(Double amount) {

		return null;
	}

	public int getMonth(Date date) {
		return date.getMonth() + 1;
	}

	public int getYear(Date date) {
		return date.getYear() + 1900;
	}

	public Double roundOff(Double d) {
		decimalFormat.setRoundingMode(RoundingMode.UP);
		return Double.valueOf(decimalFormat.format(d));
	}

	public List paginate(List list, Integer page, Integer size) {
		Assert.notNull(list, "List must not be null");
		if ((page != null && page > 0) && (size != null && size > 0 && size <= list.size())) {
			int start = (page - 1) * size, end = start + size;
			if (list.size() > start) {
				if (list.size() >= end)
					return list.subList(start, end);
				else
					return list.subList(start, list.size() - 1);
			} else {
				return new ArrayList<>();
			}
		}
		return list;
	}
}
