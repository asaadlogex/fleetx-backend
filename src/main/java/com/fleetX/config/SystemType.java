package com.fleetX.config;

import java.util.Arrays;
import java.util.List;

import io.jsonwebtoken.SignatureAlgorithm;

public class SystemType {
	public static final String API_PREFIX = "/api/v1/";
	public static final SignatureAlgorithm SIGNING_ALGORITHM = SignatureAlgorithm.HS256;
	public static final long ACCESS_TOKEN_VALIDITY = 5 * (60 * 60 * 1000); // in milliseconds
	public static final String SIGNING_KEY = "rukhshan123";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String AUTHORITIES_KEY = "scopes";
	
	public static final long DAY_IN_MS = 1000 * 60 * 60 * 24;

	public static final String[] ACTIONS_ON_ENTITY = { "_CREATE", "_READ", "_UPDATE", "_DELETE" };

	public static final String EMPLOYEE_CNIC = "cnic";

	// dropdowns
	public static final String ASSET_TYPE = "ATD";
	public static final String BASE_STATION_TYPE = "BSTD";
	public static final String BUSINESS_TYPE = "BTD";
	public static final String CATEGORY_TYPE = "CYTD";
	public static final String CHANNEL_TYPE = "CTD";
	public static final String CITY_TYPE = "CTTD";
	public static final String COMPANY_DOCUMENT_TYPE = "CDD";
	public static final String CONTAINER_TYPE = "CND";
	public static final String CONTRACT_TYPE = "CRTD";
	public static final String CUSTOMER_TYPE = "CUTD";
	public static final String DEPARTMENT_TYPE = "DTD";
	public static final String EMPLOYEE_DOCUMENT_TYPE = "EDD";
	public static final String EMPLOYEE_TYPE = "ETD";
	public static final String FORMATION_TYPE = "FTD";
	public static final String FUEL_TANK_TYPE = "FTT";
	public static final String JOB_STATUS_TYPE = "JSD";
	public static final String LEASE_TYPE = "LTD";
	public static final String LOAD_STATUS_TYPE = "LSD";
	public static final String MAKE_TYPE = "MTD";
	public static final String PAYMENT_MODE_TYPE = "PMD";
	public static final String POSITION_TYPE = "PTD";
	public static final String USER_PRIVILEGE_TYPE = "UPD";
	public static final String RWB_STATUS_TYPE = "RSD";
	public static final String STOP_TYPE = "SPD";
	public static final String SUPPLIER_TYPE = "STD";
	public static final String TRAILER_SIZE_TYPE = "TSD";
	public static final String TRAILER_TYPE = "TTD";
	public static final String VEHICLE_OWNERSHIP_TYPE = "VOT";
	public static final String INCOME_TYPE = "ITD";
	public static final String EXPENSE_TYPE = "XTD";
	public static final String COMPANY_TYPE = "COTD";
	public static final String STATE_TYPE = "STTD";
	public static final String STOP_PROGRESS_TYPE = "SPTD";
	public static final String REIMBURSEMENT_STATUS_TYPE = "RITD";
	public static final String AREA_TYPE = "ARTD";
	public static final String INVOICE_STATUS_TYPE = "INTD";
	public static final String PAYMENT_STATUS_TYPE = "PSTD";
	public static final String ITEM_TYPE = "ITMD";
	public static final String ITEM_CATEGORY = "ITCT";
	public static final String ITEM_UNIT = "ITUT";
	public static final String BRAND = "BRD";
	public static final String PURCHASE_ORDER_STATUS = "POS";
	public static final String WORK_ORDER_STATUS = "WOS";
	public static final String WORK_ORDER_CATEGORY = "WOC";
	public static final String WORK_ORDER_SUB_CATEGORY = "WOSC";
	public static final String PRIORITY = "PRTD";

	public static final String CONTRACTUAL = "Contractual";

	public static final String CUSTOMER_TYPE_CONTRACTUAL = "Contractual";
	public static final String CUSTOMER_TYPE_OPEN_MARKET = "Open market";

	public static final String POSITION_DRIVER = "Driver";

	public static final String STATUS_ACTIVE = "Active";
	public static final String STATUS_INACTIVE = "Inactive";
	public static final String STATUS_IN_USE = "In Use";
	public static final String STATUS_AVAILABLE = "Free";

	public static final String STATUS_MOVING_FREE = "Free";
	public static final String STATUS_MOVING_ENROUTE = "Enroute";

	public static final String STATUS_JOB_OPEN = "Open";
	public static final String STATUS_JOB_CLOSE = "Close";

	public static final String STATUS_RWB_DISPATCHED = "Dispatched";
	public static final String STATUS_RWB_COMPLETED = "Completed";
	public static final String STATUS_RWB_LOADED = "Loaded";
	public static final String STATUS_RWB_EMPTY = "Empty";
	public static final String STATUS_RWB_BOOKED = "Booked";

	public static final String DEDICATED = "Dedicated";

	public static final String OWNERSHIP_RENTAL = "Rental";
	public static final String OWNERSHIP_OWN = "Owned";

	public static final String LEASE_TYPE_CONTRACTUAL = "Contractual";
	public static final String LEASE_TYPE_PURCHASED = "Purchased";

	public static final String EQUIPMENT_TYPE_REEFER = "Reefer";

	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"; // time in 24 hours format
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_DIFFERENCE_FORMAT = "#.##";

	// Channel type
	public static final String CHANNEL_PHONE = "Phone";
	public static final String CHANNEL_EMAIL = "Email";
	public static final String CHANNEL_MOBILE = "Mobile";

	// ERROR MESSAGES
	public static final String MUST_NOT_BE_NULL = "Required parameter must not be null!";
	public static final String NO_SUCH_ELEMENT = "No such element found!";
	public static final String ALREADY_EXISTS = "Already exists!";
	public static final String NOT_ACTIVE = "Not active!";
	public static final String INVALID_DATA = "Invalid!";
	public static final String FTP_UPLOAD_FAILED = "File upload/download failed!";

	public static final String TIME_ZONE = "UTC";

	public static final String FTP_SERVER_URL = "103.213.115.187";
	public static final String FTP_USERNAME = "Logex FTP";
	public static final String FTP_PASSWORD = "admin@12345";
//	public static final String FTP_RETREIVE_URL = "http://103.213.115.187:9000/api/v1/files/";
	public static final String FTP_RETREIVE_URL = "http://103.213.115.187:9010/api/v1/files/"; //production
	public static final String FTP_PATH_EMPLOYEE = "Employee/";
	public static final String FTP_PATH_JOB = "Jobs/";
	public static final String FTP_PATH_CONTRACT = "Contracts/";

	public static final String TRAILER_TYPE_BOX = "Box Trailer";

	public static final String FILE_TYPE_OPENDOCUMENT_SPREADSHEET = "ods";
	public static final String EXCEL_TRIP_DETAIL_FILE_NAME = "Trip sheet";
	public static final String EXCEL_TRACTORS_FILE_NAME = "Tractors";
	public static final String EXCEL_TRAILERS_FILE_NAME = "Trailers";
	public static final String EXCEL_EMPLOYEES_FILE_NAME = "Employees";
	public static final String EXCEL_WORK_ORDER_FILE_NAME = "Work orders";
	public static final String EXCEL_INVENTORY_FILE_NAME = "Inventories";

	public static final List<String> TRIP_SHEET_COLUMNS = Arrays.asList("RWB ID", "JOB ID", "RWB DATE", "ROUTE",
			"VEHICLE TYPE", "VEHICLE #", "LEASE TYPE", "CLIENT", "JOB DURATION", "RWB DURATION", "OD KM", "TR KM",
			"RWB TR KM", "JOB REVENUE", "JOB COST", "RWB REVENUE", "RWB COST", "NET REVENUE", "STATUS",
			"TOTAL FUEL COST", "AVG FUEL COST PER KM", "FUEL AVG", "GENSET FUEL",
//			"WEIGHT",
			"CLOSE DATE");
	public static final List<String> TRACTOR_EXPORT_COLUMNS = Arrays.asList("TRACTOR ID", "ENGINE TYPE", "ENGINE NO.",
			"TRANSMISSION", "NUMBER PLATE", "NUMBER OF GEARS", "NUMBER OF FUEL TANKS", "FUEL CAPACITY",
			"STARTING ODOMETER", "CURRENT ODOMETER", "ODOMETER UNIT", "INDUCTION", "LEASE TYPE", "SUPPLIER ID", "VIN",
			"ASSET MAKE", "MODEL", "YEAR", "COLOR", "STATUS", "TYRE SIZE", "NO. OF TYRES", "GROSS WEIGHT",
			"WEIGHT UNIT");
	public static final List<String> TRAILER_EXPORT_COLUMNS = Arrays.asList("TRAILER ID", "TRAILER NUMBER",
			"TRAILER TYPE", "TRAILER SIZE", "INDUCTION", "LEASE TYPE", "SUPPLIER ID", "YEAR", "NO. OF TYRES",
			"GROSS WEIGHT", "WEIGHT UNIT");
	public static final List<String> EMPLOYEE_EXPORT_COLUMNS = Arrays.asList("EMPLOYEE ID", "EMPLOYEE TYPE",
			"SUPPLIER ID", "FIRST NAME", "LAST NAME", "EMPLOYEE NO.", "DATE OF BIRTH", "CNIC #", "CNIC EXPIRY",
			"LICENSE NO.", "LICENSE CLASS", "LICENSE EXPIRY", "JOINING DATE", "ADDRESS 1", "ADDRESS 2", "CITY", "STATE",
			"COUNTRY", "POSITION", "CONTACT", "NEXT OF KIN", "NEXT OF KIN RELATION", "SALARY");
	public static final List<String> WORK_ORDER_EXPORT_COLUMNS = Arrays.asList("WO ID", "PRIORITY", "ASSET", "KM",
			"BASE STATION", "WO DATE", "STATUS", "CATEGORY", "SUB CATEGORY", "ITEM ID", "ITEM", "ITEM TYPE", "QTY",
			"RATE", "LEASE TYPE", "CREATED ON", "UPDATED ON", "CLOSED ON", "DPT NAME", "EST DURATION",
			"ACTIVITY DETAIL", "SENDER", "OWNER", "DELAY HRS", "WORKSHOP", "TOTAL AMOUNT");
	public static final List<String> INVENTORY_EXPORT_COLUMNS = Arrays.asList("INVENTORY ID", "ITEM NAME", "SKU",
			"QTY IN", "QTY OUT", "STOCK COMMITTED", "STOCK ON HAND", "STOCK EXPECTED");

	public static final String FILE_TYPE_EXCEL_SPREADSHEET = "xlsx";
	public static final String BUSINESS_TYPE_SUPPLIER = "Supplier";

	public static final String INCOME_FREIGHT_RATE = "Freight Rate";
	public static final String INCOME_LOADING_CHARGES = "Loading Charges";
	public static final String INCOME_OFFLOADING_CHARGES = "Offloading Charges";
	public static final String INCOME_DETENTION_CHARGES = "Detention Charges";
	public static final String INCOME_OTHER_CHARGES = "Other Charges";
	public static final String INCOME_HALTING_CHARGES = "Halting Charges";

	public static final String EXPENSE_TYRE_COST = "Tyre Cost";

	public static final String STOP_FUEL = "Fuel";

	public static final Double FIXED_TYRE_COST = 50000d;
	public static final Double FIXED_TYRE_LIFE = 1200000d;

	public static final List REIMBURSEMENT_EXPENSES = Arrays.asList("Toll Taxes", "Fuel", "Miscellaneous", "Penalty",
			"Un-Receipted");

	public static final String REIMBURSEMENT_STATUS_PENDING = "Pending";
	public static final String REIMBURSEMENT_STATUS_SENT = "Sent";
	public static final String REIMBURSEMENT_STATUS_REIMBURSED = "Reimbursed";

	public static final String INVOICE_STATUS_PENDING = "Pending";
	public static final String INVOICE_STATUS_GENERATED = "Generated";

	public static final String PAYMENT_STATUS_PENDING = "Pending";
	public static final String PAYMENT_STATUS_PARTIAL = "Partial Paid";
	public static final String PAYMENT_STATUS_PAID = "Paid";

	public static final String STATE_SINDH = "Sindh";
	public static final String STATE_BALUCHISTAN = "Baluchistan";
	public static final String STATE_PUNJAB = "Punjab";
	public static final String STATE_KPK = "KPK";

	public static final Double TAX_PERC_SINDH = 13d;
	public static final Double TAX_PERC_PUNJAB = 15d;
	public static final Double TAX_PERC_BALUCHISTAN = 12d;
	public static final Double TAX_PERC_KPK = 12d;

	public static final String INVOICE_DEFAULT_NOTE = "Invoice default note";

	public static final String INVOICE_TYPE_PER_TRIP = "Per Trip";
	public static final String INVOICE_TYPE_PER_TON = "Per Ton";
	public static final String INVOICE_TYPE_FIXED = "Fixed";
	public static final String INVOICE_TYPE_DEDICATED = "Dedicated";

	public static final String CONTRACT_TYPE_PER_TRIP = "Per Trip";
	public static final String CONTRACT_TYPE_DEDICATED = "Dedicated";
	public static final String CONTRACT_TYPE_PER_TON = "Per Ton";

	public static final String STOP_TYPE_PICKUP = "Pickup";
	public static final String STOP_TYPE_DELIVERY = "Delivery";

	public static final String ADMIN_COMPANY_ID = "1";

	public static final String DEPARTMENT_OPERATIONS = "Operations";

	public static final String PURCHASE_ORDER_STATUS_PENDING = "Pending";
	public static final String PURCHASE_ORDER_STATUS_RECEIVED = "Received";
	public static final String PURCHASE_ORDER_STATUS_PARTIALLY_RECEIVED = "Partially Received";

	public static final String WORK_ORDER_STATUS_OPEN = "Open";
	public static final String WORK_ORDER_STATUS_IN_PROGRESS = "In Progress";
	public static final String WORK_ORDER_STATUS_CLOSED = "Closed";

	public static final String FUEL_TANK_TYPE_NORMAL = "Normal";
	public static final String FUEL_TANK_TYPE_GENSET = "Genset";

	public static final String ITEM_TYPE_GOODS = "Goods";
	public static final String ITEM_TYPE_SERVICE = "Services";

}
