package com.fleetX.model;

public class SuccessResponse extends Response {
	private String message;
	private int totalRecords;

	public SuccessResponse(int code, String message, Object data) {
		super(true, code, data);
		this.message = message;
	}

	public SuccessResponse(int code, String message, Object data, int totalRecords) {
		super(true, code, data);
		this.message = message;
		this.totalRecords = totalRecords;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

}
