package com.fleetX.model;

public class ErrorResponse extends Response{
	private String errorMessage;
	
	public ErrorResponse(int code,String errorMessage,Object data) {
		super(false,code,data);
		this.errorMessage=errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
