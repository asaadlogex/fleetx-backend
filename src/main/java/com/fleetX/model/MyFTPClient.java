package com.fleetX.model;

import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Component;

import com.fleetX.config.SystemType;

@Component
public class MyFTPClient {

	public FTPClient getFTPClient() throws SocketException, IOException {
		FTPClient ftpClient = new FTPClient();
		ftpClient.connect(SystemType.FTP_SERVER_URL, 21);
		ftpClient.login(SystemType.FTP_USERNAME, SystemType.FTP_PASSWORD);
		return ftpClient;
	}
}
