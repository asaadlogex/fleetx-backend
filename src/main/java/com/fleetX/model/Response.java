package com.fleetX.model;

public class Response {
	private boolean success;
	private int code;
	private Object data;

	public Response(boolean success, int code, Object data) {
		this.success = success;
		this.code = code;
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
