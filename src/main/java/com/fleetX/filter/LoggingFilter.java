package com.fleetX.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fleetX.config.SystemUtil;

@Component
public class LoggingFilter extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (request != null && response != null) {
			System.out.println("===== REQUEST LOG =====");
			System.out.println("Username: "
					+ (request.getUserPrincipal() == null ? "Not known" : request.getUserPrincipal().getName()));
			System.out.println("Path: " + request.getRequestURI());
			System.out.println("Method: " + request.getMethod());
			System.out.println("Content-type: " + request.getContentType());

			System.out.println("\n===== RESPONSE LOG =====");
			System.out.println("Content-type: " + response.getContentType());
			System.out.println("Response code: " + response.getStatus());

		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception exception) throws Exception {
	}
}
