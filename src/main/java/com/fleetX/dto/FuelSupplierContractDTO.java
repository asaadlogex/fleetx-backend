package com.fleetX.dto;

import java.util.List;

public class FuelSupplierContractDTO {
	private String fscId;
	private AdminCompanyDTO adminCompany;
	private CompanyDTO supplier;
	private String fscStatus;
	private List<FuelRateDTO> fuelRates;
	
	public String getFscId() {
		return fscId;
	}
	public void setFscId(String fscId) {
		this.fscId = fscId;
	}
	public AdminCompanyDTO getAdminCompany() {
		return adminCompany;
	}
	public void setAdminCompany(AdminCompanyDTO adminCompany) {
		this.adminCompany = adminCompany;
	}
	public CompanyDTO getSupplier() {
		return supplier;
	}
	public void setSupplier(CompanyDTO supplier) {
		this.supplier = supplier;
	}
	public String getFscStatus() {
		return fscStatus;
	}
	public void setFscStatus(String fscStatus) {
		this.fscStatus = fscStatus;
	}
	public List<FuelRateDTO> getFuelRates() {
		return fuelRates;
	}
	public void setFuelRates(List<FuelRateDTO> fuelRates) {
		this.fuelRates = fuelRates;
	}
}
