package com.fleetX.dto;

public class TrainingProfileDTO {
	private String trainingProfileId;
	private String trainingType;
	private String issuedDate;
	private String expiryDate;
	private String trainerName;
	private String trainingStation;
	private String employeeId;
	private String trainingCertificateUrl;
	private String attendanceSheetUrl;
	private String pictureWithTrainerUrl;
	
	public TrainingProfileDTO() {
	}
	
	public TrainingProfileDTO(String trainingProfileId, String trainingType, String issuedDate, String expiryDate,
			String trainerName, String trainingStation, String employeeId,String trainingCertificateUrl,String attendanceSheetUrl,String pictureWithTrainerUrl) {
		this.trainingProfileId = trainingProfileId;
		this.trainingType = trainingType;
		this.issuedDate = issuedDate;
		this.expiryDate = expiryDate;
		this.trainerName = trainerName;
		this.trainingStation = trainingStation;
		this.employeeId = employeeId;
		this.trainingCertificateUrl=trainingCertificateUrl;
		this.attendanceSheetUrl=attendanceSheetUrl;
		this.pictureWithTrainerUrl=pictureWithTrainerUrl;
	}

	public String getTrainingCertificateUrl() {
		return trainingCertificateUrl;
	}

	public void setTrainingCertificateUrl(String trainingCertificateUrl) {
		this.trainingCertificateUrl = trainingCertificateUrl;
	}

	public String getAttendanceSheetUrl() {
		return attendanceSheetUrl;
	}

	public void setAttendanceSheetUrl(String attendanceSheetUrl) {
		this.attendanceSheetUrl = attendanceSheetUrl;
	}

	public String getPictureWithTrainerUrl() {
		return pictureWithTrainerUrl;
	}

	public void setPictureWithTrainerUrl(String pictureWithTrainerUrl) {
		this.pictureWithTrainerUrl = pictureWithTrainerUrl;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getTrainingProfileId() {
		return trainingProfileId;
	}
	public void setTrainingProfileId(String trainingProfileId) {
		this.trainingProfileId = trainingProfileId;
	}
	public String getTrainingType() {
		return trainingType;
	}
	public void setTrainingType(String trainingType) {
		this.trainingType = trainingType;
	}
	public String getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getTrainerName() {
		return trainerName;
	}
	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}
	public String getTrainingStation() {
		return trainingStation;
	}
	public void setTrainingStation(String trainingStation) {
		this.trainingStation = trainingStation;
	}
	public String getDriverId() {
		return employeeId;
	}
	public void setDriverId(String employeeId) {
		this.employeeId = employeeId;
	}
}
