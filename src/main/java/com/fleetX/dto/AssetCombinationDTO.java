package com.fleetX.dto;

public class AssetCombinationDTO {
	private String assetCombinationId;
	private TractorSummaryDTO tractor;
	private TrailerSummaryDTO trailer;
	private ContainerDTO container;
	private TrackerDTO tracker;
	private EmployeeSummaryDTO driver1;
	private EmployeeSummaryDTO driver2;
	private String status;
	private String note;

	public String getAssetCombinationId() {
		return assetCombinationId;
	}

	public void setAssetCombinationId(String assetCombinationId) {
		this.assetCombinationId = assetCombinationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ContainerDTO getContainer() {
		return container;
	}

	public void setContainer(ContainerDTO container) {
		this.container = container;
	}

	public TrackerDTO getTracker() {
		return tracker;
	}

	public void setTracker(TrackerDTO tracker) {
		this.tracker = tracker;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public TractorSummaryDTO getTractor() {
		return tractor;
	}

	public void setTractor(TractorSummaryDTO tractor) {
		this.tractor = tractor;
	}

	public TrailerSummaryDTO getTrailer() {
		return trailer;
	}

	public void setTrailer(TrailerSummaryDTO trailer) {
		this.trailer = trailer;
	}

	public EmployeeSummaryDTO getDriver1() {
		return driver1;
	}

	public void setDriver1(EmployeeSummaryDTO driver1) {
		this.driver1 = driver1;
	}

	public EmployeeSummaryDTO getDriver2() {
		return driver2;
	}

	public void setDriver2(EmployeeSummaryDTO driver2) {
		this.driver2 = driver2;
	}
}
