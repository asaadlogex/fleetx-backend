package com.fleetX.dto;

public class InventoryDTO {
	private Integer inventoryId;
	private ItemDTO item;
	private String inventoryStartDate;
	private String lastUpdatedOn;
	private Integer startingQty;
	private Double startingCost;
	private Integer criticalQty;
	private Integer expectedQty;
	private Integer qtyOnHand;
	private Integer qtyCommitted;
	private Integer adjustedUp;
	private Integer adjustedDown;
	private Integer qtyIn;
	private Integer qtyOut;
	private Double avgCost;

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public ItemDTO getItem() {
		return item;
	}

	public void setItem(ItemDTO item) {
		this.item = item;
	}

	public String getInventoryStartDate() {
		return inventoryStartDate;
	}

	public void setInventoryStartDate(String inventoryStartDate) {
		this.inventoryStartDate = inventoryStartDate;
	}

	public String getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(String lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public Integer getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(Integer qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public Integer getQtyCommitted() {
		return qtyCommitted;
	}

	public void setQtyCommitted(Integer qtyCommitted) {
		this.qtyCommitted = qtyCommitted;
	}

	public Double getAvgCost() {
		return avgCost;
	}

	public void setAvgCost(Double avgCost) {
		this.avgCost = avgCost;
	}

	public Integer getStartingQty() {
		return startingQty;
	}

	public void setStartingQty(Integer startingQty) {
		this.startingQty = startingQty;
	}

	public Double getStartingCost() {
		return startingCost;
	}

	public void setStartingCost(Double startingCost) {
		this.startingCost = startingCost;
	}

	public Integer getCriticalQty() {
		return criticalQty;
	}

	public void setCriticalQty(Integer criticalQty) {
		this.criticalQty = criticalQty;
	}

	public Integer getExpectedQty() {
		return expectedQty;
	}

	public void setExpectedQty(Integer expectedQty) {
		this.expectedQty = expectedQty;
	}

	public Integer getQtyIn() {
		return qtyIn;
	}

	public void setQtyIn(Integer qtyIn) {
		this.qtyIn = qtyIn;
	}

	public Integer getQtyOut() {
		return qtyOut;
	}

	public void setQtyOut(Integer qtyOut) {
		this.qtyOut = qtyOut;
	}

	public Integer getAdjustedUp() {
		return adjustedUp;
	}

	public void setAdjustedUp(Integer adjustedUp) {
		this.adjustedUp = adjustedUp;
	}

	public Integer getAdjustedDown() {
		return adjustedDown;
	}

	public void setAdjustedDown(Integer adjustedDown) {
		this.adjustedDown = adjustedDown;
	}

}
