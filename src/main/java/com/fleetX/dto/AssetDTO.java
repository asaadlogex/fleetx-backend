package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DMake;

public class AssetDTO {
	private String assetId;
	private String induction;
	private DLeaseType leaseType;
	private String supplierId;	
	private String vin;
	private DMake assetMake;
	private String model;
	private Integer year;
	private String color;
	private String status;
	private String tyreSize;
	private Integer numberOfTyres;
	private List<TyreDTO> tyres;
	private Double grossWeight;
	private String weightUnit;
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getInduction() {
		return induction;
	}
	public void setInduction(String induction) {
		this.induction = induction;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public DMake getAssetMake() {
		return assetMake;
	}
	public void setAssetMake(DMake assetMake) {
		this.assetMake = assetMake;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTyreSize() {
		return tyreSize;
	}
	public void setTyreSize(String tyreSize) {
		this.tyreSize = tyreSize;
	}
	public Integer getNumberOfTyres() {
		return numberOfTyres;
	}
	public void setNumberOfTyres(Integer numberOfTyres) {
		this.numberOfTyres = numberOfTyres;
	}
	public List<TyreDTO> getTyres() {
		return tyres;
	}
	public void setTyres(List<TyreDTO> tyres) {
		this.tyres = tyres;
	}
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	public Double getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	@Override
	public String toString() {
		return "AssetDTO [assetId=" + assetId + ", induction=" + induction + ", supplierBusinessId="
				+ supplierId + ", vin=" + vin + ", assetMake=" + assetMake + ", model=" + model + ", year="
				+ year + ", color=" + color + ", status=" + status + ", tyreSize=" + tyreSize + ", numberOfTyres="
				+ numberOfTyres + ", tyres=" + tyres + ", leaseType=" + leaseType + ", grossWeight=" + grossWeight
				+ ", weightUnit=" + weightUnit + "]";
	}
}
