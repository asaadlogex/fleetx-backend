package com.fleetX.dto;

import com.fleetX.entity.dropdown.DStopType;

public class StopDTO {
	private String stopId;
	private Integer stopSequenceNumber;
	private DStopType stopType;
	private WarehouseDTO warehouse;
	private String appointmentStartTime;
	private String appointmentEndTime;
	private String referenceNumber;
	private String roadwayBillId;
	
	public String getRoadwayBillId() {
		return roadwayBillId;
	}
	public void setRoadwayBillId(String roadwayBillId) {
		this.roadwayBillId = roadwayBillId;
	}
	public String getStopId() {
		return stopId;
	}
	public void setStopId(String stopId) {
		this.stopId = stopId;
	}
	public Integer getStopSequenceNumber() {
		return stopSequenceNumber;
	}
	public void setStopSequenceNumber(Integer stopSequenceNumber) {
		this.stopSequenceNumber = stopSequenceNumber;
	}
	public DStopType getStopType() {
		return stopType;
	}
	public void setStopType(DStopType stopType) {
		this.stopType = stopType;
	}
	public WarehouseDTO getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(WarehouseDTO warehouse) {
		this.warehouse = warehouse;
	}
	public String getAppointmentStartTime() {
		return appointmentStartTime;
	}
	public void setAppointmentStartTime(String appointmentStartTime) {
		this.appointmentStartTime = appointmentStartTime;
	}
	public String getAppointmentEndTime() {
		return appointmentEndTime;
	}
	public void setAppointmentEndTime(String appointmentEndTime) {
		this.appointmentEndTime = appointmentEndTime;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
}
