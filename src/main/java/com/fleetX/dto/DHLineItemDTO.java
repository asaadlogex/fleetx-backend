package com.fleetX.dto;

import com.fleetX.entity.dropdown.DStopType;

public class DHLineItemDTO {
	private Integer lineItemId;
	private String rwbId;
	private String vehicleTypeAndSize;
	private RouteDTO route;
	private String date;
	private String loadNumber;
	private String vehicleNumber;
	private DStopType stopType;
	private String arrivalTime;
	private String departureTime;
	private Integer dh;
	private Double dhRate;
	private Double totalAmount;

	public Integer getLineItemId() {
		return lineItemId;
	}

	public void setLineItemId(Integer lineItemId) {
		this.lineItemId = lineItemId;
	}

	public String getRwbId() {
		return rwbId;
	}

	public void setRwbId(String rwbId) {
		this.rwbId = rwbId;
	}

	public String getVehicleTypeAndSize() {
		return vehicleTypeAndSize;
	}

	public void setVehicleTypeAndSize(String vehicleTypeAndSize) {
		this.vehicleTypeAndSize = vehicleTypeAndSize;
	}

	public RouteDTO getRoute() {
		return route;
	}

	public void setRoute(RouteDTO route) {
		this.route = route;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLoadNumber() {
		return loadNumber;
	}

	public void setLoadNumber(String loadNumber) {
		this.loadNumber = loadNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public DStopType getStopType() {
		return stopType;
	}

	public void setStopType(DStopType stopType) {
		this.stopType = stopType;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public Integer getDh() {
		return dh;
	}

	public void setDh(Integer dh) {
		this.dh = dh;
	}

	public Double getDhRate() {
		return dhRate;
	}

	public void setDhRate(Double dhRate) {
		this.dhRate = dhRate;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
