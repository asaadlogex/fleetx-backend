package com.fleetX.dto;

public class InventoryTransactionInfoDTO {
	private Integer inventoryTransactionInfoId;
	private String action;
	private String description;
	private String date;
	private Integer qty;
	private Integer cost;
	private Integer qtyOnHand;
	private Integer totalValue;
	private Integer purchaseOrderId;
	private Integer workOrderId;
	private Integer inventoryId;

	public Integer getInventoryTransactionInfoId() {
		return inventoryTransactionInfoId;
	}

	public void setInventoryTransactionInfoId(Integer inventoryTransactionInfoId) {
		this.inventoryTransactionInfoId = inventoryTransactionInfoId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(Integer qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public Integer getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Integer totalValue) {
		this.totalValue = totalValue;
	}

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public Integer getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(Integer workOrderId) {
		this.workOrderId = workOrderId;
	}

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

}
