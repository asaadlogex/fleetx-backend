package com.fleetX.dto;

import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

public class RouteRateDTO {
	private Integer routeRateId;
	private DTrailerType trailerType;
	private DTrailerSize trailerSize;
	private Double weightLow;
	private Double weightHigh;
	private Double ratePerKm;
	private Double ratePerTon;
	private String effectiveFrom;
	private String effectiveTill;
	private Double totalAmount;
	private String status;

	public Integer getRouteRateId() {
		return routeRateId;
	}

	public void setRouteRateId(Integer routeRateId) {
		this.routeRateId = routeRateId;
	}

	public Double getRatePerKm() {
		return ratePerKm;
	}

	public void setRatePerKm(Double ratePerKm) {
		this.ratePerKm = ratePerKm;
	}

	public String getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getEffectiveTill() {
		return effectiveTill;
	}

	public void setEffectiveTill(String effectiveTill) {
		this.effectiveTill = effectiveTill;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DTrailerType getTrailerType() {
		return trailerType;
	}

	public void setTrailerType(DTrailerType trailerType) {
		this.trailerType = trailerType;
	}

	public DTrailerSize getTrailerSize() {
		return trailerSize;
	}

	public void setTrailerSize(DTrailerSize trailerSize) {
		this.trailerSize = trailerSize;
	}

	public Double getWeightLow() {
		return weightLow;
	}

	public void setWeightLow(Double weightLow) {
		this.weightLow = weightLow;
	}

	public Double getWeightHigh() {
		return weightHigh;
	}

	public void setWeightHigh(Double weightHigh) {
		this.weightHigh = weightHigh;
	}

	public Double getRatePerTon() {
		return ratePerTon;
	}

	public void setRatePerTon(Double ratePerTon) {
		this.ratePerTon = ratePerTon;
	}
}
