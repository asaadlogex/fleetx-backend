package com.fleetX.dto;

public class InvoiceStatsDTO {
	private String companyName;
	private Integer perTrip;
	private Integer perTon;
	private Integer dedicated;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getPerTrip() {
		return perTrip;
	}

	public void setPerTrip(Integer perTrip) {
		this.perTrip = perTrip;
	}

	public Integer getPerTon() {
		return perTon;
	}

	public void setPerTon(Integer perTon) {
		this.perTon = perTon;
	}

	public Integer getDedicated() {
		return dedicated;
	}

	public void setDedicated(Integer dedicated) {
		this.dedicated = dedicated;
	}

}
