package com.fleetX.dto;

import com.fleetX.entity.dropdown.DContainerType;
import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DTrailerSize;

public class ContainerDTO {
	private String containerId;
	private Double containerWeight;
	private String containerWeightUnit;
	private String containerNumber;
	private String containerMake;
	private String containerYear;
	private String purchaseDate;
	private String status;
	private DTrailerSize containerSize;
	private DLeaseType leaseType;	
	private DContainerType containerType;
	private String supplierId;
	private String adminCompanyId;
	
	public String getAdminCompanyId() {
		return adminCompanyId;
	}
	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	public Double getContainerWeight() {
		return containerWeight;
	}
	public void setContainerWeight(Double containerWeight) {
		this.containerWeight = containerWeight;
	}
	public String getContainerWeightUnit() {
		return containerWeightUnit;
	}
	public void setContainerWeightUnit(String containerWeightUnit) {
		this.containerWeightUnit = containerWeightUnit;
	}
	
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getContainerMake() {
		return containerMake;
	}
	public void setContainerMake(String containerMake) {
		this.containerMake = containerMake;
	}
	public String getContainerYear() {
		return containerYear;
	}
	public void setContainerYear(String containerYear) {
		this.containerYear = containerYear;
	}
	
	public DTrailerSize getContainerSize() {
		return containerSize;
	}
	public void setContainerSize(DTrailerSize containerSize) {
		this.containerSize = containerSize;
	}
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	public DContainerType getContainerType() {
		return containerType;
	}
	public void setContainerType(DContainerType containerType) {
		this.containerType = containerType;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
