package com.fleetX.dto;

import com.fleetX.entity.dropdown.DBusinessType;

public class CompanySummaryDTO {
	private String companyId;
	private String companyName;
	private DBusinessType businessType;
	private AddressDTO address;
	private String status;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public DBusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(DBusinessType businessType) {
		this.businessType = businessType;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
