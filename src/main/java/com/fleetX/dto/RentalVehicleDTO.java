package com.fleetX.dto;

import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

public class RentalVehicleDTO {
	private Integer rentalVehicleId;
	private CompanyDTO broker;
	private DTrailerType equipmentType;
	private DTrailerSize equipmentSize;
	private String vehicleNumber;
	
	public Integer getRentalVehicleId() {
		return rentalVehicleId;
	}
	public void setRentalVehicleId(Integer rentalVehicleId) {
		this.rentalVehicleId = rentalVehicleId;
	}
	public CompanyDTO getBroker() {
		return broker;
	}
	public void setBroker(CompanyDTO broker) {
		this.broker = broker;
	}
	public DTrailerType getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(DTrailerType equipmentType) {
		this.equipmentType = equipmentType;
	}
	public DTrailerSize getEquipmentSize() {
		return equipmentSize;
	}
	public void setEquipmentSize(DTrailerSize equipmentSize) {
		this.equipmentSize = equipmentSize;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
}
