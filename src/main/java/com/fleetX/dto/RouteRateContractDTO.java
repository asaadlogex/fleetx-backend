package com.fleetX.dto;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.fleetX.config.SystemUtil;

public class RouteRateContractDTO {
	private Integer routeRateContractId;
	private RouteDTO route;
	private List<RouteRateDTO> routeRates;
	private String status;

	@Autowired
	private SystemUtil systemUtil;

	public RouteDTO getRoute() {
		return route;
	}

	public Integer getRouteRateContractId() {
		return routeRateContractId;
	}

	public void setRouteRateContractId(Integer routeRateContractId) {
		this.routeRateContractId = routeRateContractId;
	}

	public void setRoute(RouteDTO route) {
		this.route = route;
	}

	public List<RouteRateDTO> getRouteRates() {
		return this.routeRates;
//				.stream().sorted(
//			(RouteRateDTO x,RouteRateDTO y) ->
//			{
//				try {
//					return systemUtil.convertToDate(x.getEffectiveFrom()).compareTo(systemUtil.convertToDate(y.getEffectiveFrom()));
//				} catch (ParseException e) {
//					System.err.println(e.getMessage());
//					return 0;
//				}
//			})
//		.collect(Collectors.toList());
	}

	public void setRouteRates(List<RouteRateDTO> routeRates) {
		this.routeRates = routeRates;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
