package com.fleetX.dto;

public class InvoicePaymentDTO {
	private Integer invoicePaymentId;
	private String referenceNumber;
	private String paymentDate;
	private Double paymentAmount;
	private String paymentNote;
	private Integer invoiceId;
	private Integer dhInvoiceId;

	public Integer getInvoicePaymentId() {
		return invoicePaymentId;
	}

	public void setInvoicePaymentId(Integer invoicePaymentId) {
		this.invoicePaymentId = invoicePaymentId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentNote() {
		return paymentNote;
	}

	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Integer getDhInvoiceId() {
		return dhInvoiceId;
	}

	public void setDhInvoiceId(Integer dhInvoiceId) {
		this.dhInvoiceId = dhInvoiceId;
	}

}
