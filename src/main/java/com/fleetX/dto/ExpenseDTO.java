package com.fleetX.dto;

import com.fleetX.entity.dropdown.DExpenseType;

public class ExpenseDTO {
	private Integer expenseId;
	private String description;
	private Double amount;
	private DExpenseType expenseType;
	private String roadwayBillId;
	
	public DExpenseType getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(DExpenseType expenseType) {
		this.expenseType = expenseType;
	}
	public Integer getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(Integer expenseId) {
		this.expenseId = expenseId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getRoadwayBillId() {
		return roadwayBillId;
	}
	public void setRoadwayBillId(String roadwayBillId) {
		this.roadwayBillId = roadwayBillId;
	}
}
