package com.fleetX.dto;

import java.util.List;

public class ReimbursementInfoSummaryDTO {
	private Integer reimbursementInfoId;
	private String referenceNumber;
	private String reimbursementDate;
	private Double reimbursementAmount;
	private String reimbursementNote;
	private List<String> jobIds;

	public Integer getReimbursementInfoId() {
		return reimbursementInfoId;
	}

	public void setReimbursementInfoId(Integer reimbursementInfoId) {
		this.reimbursementInfoId = reimbursementInfoId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReimbursementDate() {
		return reimbursementDate;
	}

	public void setReimbursementDate(String reimbursementDate) {
		this.reimbursementDate = reimbursementDate;
	}

	public Double getReimbursementAmount() {
		return reimbursementAmount;
	}

	public void setReimbursementAmount(Double reimbursementAmount) {
		this.reimbursementAmount = reimbursementAmount;
	}

	public String getReimbursementNote() {
		return reimbursementNote;
	}

	public void setReimbursementNote(String reimbursementNote) {
		this.reimbursementNote = reimbursementNote;
	}

	public List<String> getJobIds() {
		return jobIds;
	}

	public void setJobIds(List<String> jobIds) {
		this.jobIds = jobIds;
	}

}
