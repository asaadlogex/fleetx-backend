package com.fleetX.dto;

import java.util.List;

public class TrialBalanceDTO {
	private String periodStart;
	private String periodEnd;
	private List<TrialBalanceEntryDTO> entries;
	private Boolean isAdjusted = false;

	public String getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}

	public List<TrialBalanceEntryDTO> getEntries() {
		return entries;
	}

	public void setEntries(List<TrialBalanceEntryDTO> entries) {
		this.entries = entries;
	}

	public Boolean getIsAdjusted() {
		return isAdjusted;
	}

	public void setIsAdjusted(Boolean isAdjusted) {
		this.isAdjusted = isAdjusted;
	}
}
