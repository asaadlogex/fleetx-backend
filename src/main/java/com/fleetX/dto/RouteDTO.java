package com.fleetX.dto;

import com.fleetX.entity.dropdown.DCity;

public class RouteDTO {
	private String routeId;
	private DCity routeTo;
	private DCity routeFrom;
	private Double avgDistanceInKM;
	
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	public DCity getRouteTo() {
		return routeTo;
	}
	public void setRouteTo(DCity routeTo) {
		this.routeTo = routeTo;
	}
	public DCity getRouteFrom() {
		return routeFrom;
	}
	public void setRouteFrom(DCity routeFrom) {
		this.routeFrom = routeFrom;
	}
	public Double getAvgDistanceInKM() {
		return avgDistanceInKM;
	}
	public void setAvgDistanceInKM(Double avgDistanceInKM) {
		this.avgDistanceInKM = avgDistanceInKM;
	}
	@Override
	public String toString() {
		return routeTo.getCode() + "-" + routeFrom.getCode();
	}
	
}
