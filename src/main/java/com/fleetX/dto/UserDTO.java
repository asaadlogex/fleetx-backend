package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DPrivilege;

public class UserDTO {
	private String username;
	private String firstName;
	private String lastName;
	private String password;
	private String status;
	private List<RoleDTO> roles;
	private List<DPrivilege> additionalPrivileges;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<RoleDTO> getRoles() {
		return roles;
	}
	public void setRoles(List<RoleDTO> roles) {
		this.roles = roles;
	}
	public List<DPrivilege> getAdditionalPrivileges() {
		return additionalPrivileges;
	}
	public void setAdditionalPrivileges(List<DPrivilege> additionalPrivileges) {
		this.additionalPrivileges = additionalPrivileges;
	}
}
