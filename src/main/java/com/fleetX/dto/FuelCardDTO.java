package com.fleetX.dto;

public class FuelCardDTO {
	private Integer fuelCardId;
	private String fuelCardNumber;
	private String expiry;
	private Double limit;
	private String supplierId;
	private String customerId;
	private String status;
	private String adminCompanyId;
	
	public String getAdminCompanyId() {
		return adminCompanyId;
	}
	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}
	public Integer getFuelCardId() {
		return fuelCardId;
	}
	public void setFuelCardId(Integer fuelCardId) {
		this.fuelCardId = fuelCardId;
	}
	public String getFuelCardNumber() {
		return fuelCardNumber;
	}
	public void setFuelCardNumber(String fuelCardNumber) {
		this.fuelCardNumber = fuelCardNumber;
	}
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public Double getLimit() {
		return limit;
	}
	public void setLimit(Double limit) {
		this.limit = limit;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
