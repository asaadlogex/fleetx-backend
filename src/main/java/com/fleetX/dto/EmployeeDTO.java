package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DDepartment;
import com.fleetX.entity.dropdown.DEmployeeType;
import com.fleetX.entity.dropdown.DPosition;

public class EmployeeDTO {
	private String employeeId;
	private String employeeNumber;
	private String firstName;
	private String lastName;
	private String status;
	private DDepartment department;
	private DPosition position;
	private String companyId;
	private AddressDTO businessAddress;
	private String cnic;
	private String cnicExpiry;
	private String ntnNumber;
	private String dateOfBirth;	
	private String nextOfKin;
	private String nextOfKinRelation;
	private List<EmployeeContractDTO> employeeContracts;
	private List<AddressDTO> employeeAddresses;
	private List<ContactDTO> employeeContacts;
	private String pictureUrl;
	private DEmployeeType employeeType;
	private String supplierId;
	private String licenceNumber;
	private String licenceExpiry;
	private String licenceClass;
	
	public String getLicenceNumber() {
		return licenceNumber;
	}
	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	public String getLicenceExpiry() {
		return licenceExpiry;
	}
	public void setLicenceExpiry(String licenceExpiry) {
		this.licenceExpiry = licenceExpiry;
	}
	public String getLicenceClass() {
		return licenceClass;
	}
	public void setLicenceClass(String licenceClass) {
		this.licenceClass = licenceClass;
	}
	public DEmployeeType getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(DEmployeeType employeeType) {
		this.employeeType = employeeType;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DDepartment getDepartment() {
		return department;
	}
	public void setDepartment(DDepartment department) {
		this.department = department;
	}
	public DPosition getPosition() {
		return position;
	}
	public void setPosition(DPosition position) {
		this.position = position;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public AddressDTO getBusinessAddress() {
		return businessAddress;
	}
	public void setBusinessAddress(AddressDTO businessAddress) {
		this.businessAddress = businessAddress;
	}
	public String getCnic() {
		return cnic;
	}
	public void setCnic(String cnic) {
		this.cnic = cnic;
	}
	public String getCnicExpiry() {
		return cnicExpiry;
	}
	public void setCnicExpiry(String cnicExpiry) {
		this.cnicExpiry = cnicExpiry;
	}
	public String getNtnNumber() {
		return ntnNumber;
	}
	public void setNtnNumber(String ntnNumber) {
		this.ntnNumber = ntnNumber;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getNextOfKin() {
		return nextOfKin;
	}
	public void setNextOfKin(String nextOfKin) {
		this.nextOfKin = nextOfKin;
	}
	public String getNextOfKinRelation() {
		return nextOfKinRelation;
	}
	public void setNextOfKinRelation(String nextOfKinRelation) {
		this.nextOfKinRelation = nextOfKinRelation;
	}
	public List<EmployeeContractDTO> getEmployeeContracts() {
		return employeeContracts;
	}
	public void setEmployeeContracts(List<EmployeeContractDTO> employeeContracts) {
		this.employeeContracts = employeeContracts;
	}
	public List<AddressDTO> getEmployeeAddresses() {
		return employeeAddresses;
	}
	public void setEmployeeAddresses(List<AddressDTO> employeeAddresses) {
		this.employeeAddresses = employeeAddresses;
	}
	public List<ContactDTO> getEmployeeContacts() {
		return employeeContacts;
	}
	public void setEmployeeContacts(List<ContactDTO> employeeContacts) {
		this.employeeContacts = employeeContacts;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
}
