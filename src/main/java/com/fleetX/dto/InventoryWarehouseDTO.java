package com.fleetX.dto;

public class InventoryWarehouseDTO {
	private Integer inventoryWarehouseId;
	private String name;
	private AddressDTO address;
	private String contactNumber;
	private String contactPerson;

	public Integer getInventoryWarehouseId() {
		return inventoryWarehouseId;
	}

	public void setInventoryWarehouseId(Integer inventoryWarehouseId) {
		this.inventoryWarehouseId = inventoryWarehouseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

}
