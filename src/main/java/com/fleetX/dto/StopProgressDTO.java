package com.fleetX.dto;

import com.fleetX.entity.dropdown.DStopType;
import com.fleetX.entity.dropdown.DVehicleOwnership;

public class StopProgressDTO {
	private Integer stopProgressId;
	private DStopType stopProgressType;
	private TractorSummaryDTO tractor;
	private TrailerSummaryDTO trailer;
	private ContainerDTO container;
	private EmployeeSummaryDTO driver1;
	private EmployeeSummaryDTO driver2;
	private DVehicleOwnership vehicleOwnership;
	private RentalVehicleDTO rentalVehicle;
	private Double currentKm;
	private String arrivalTime;
	private String departureTime;
	private Double detentionInHrs;
	private String instruction;
	private String stopId;
	private String roadwayBillId;
	private WarehouseDTO warehouse;

	public Integer getStopProgressId() {
		return stopProgressId;
	}

	public void setStopProgressId(Integer stopProgressId) {
		this.stopProgressId = stopProgressId;
	}

	public DStopType getStopProgressType() {
		return stopProgressType;
	}

	public void setStopProgressType(DStopType stopProgressType) {
		this.stopProgressType = stopProgressType;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public ContainerDTO getContainer() {
		return container;
	}

	public void setContainer(ContainerDTO container) {
		this.container = container;
	}

	public DVehicleOwnership getVehicleOwnership() {
		return vehicleOwnership;
	}

	public void setVehicleOwnership(DVehicleOwnership vehicleOwnership) {
		this.vehicleOwnership = vehicleOwnership;
	}

	public RentalVehicleDTO getRentalVehicle() {
		return rentalVehicle;
	}

	public void setRentalVehicle(RentalVehicleDTO rentalVehicle) {
		this.rentalVehicle = rentalVehicle;
	}

	public Double getCurrentKm() {
		return currentKm;
	}

	public void setCurrentKm(Double currentKm) {
		this.currentKm = currentKm;
	}

	public WarehouseDTO getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(WarehouseDTO warehouse) {
		this.warehouse = warehouse;
	}

	public String getStopId() {
		return stopId;
	}

	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public Double getDetentionInHrs() {
		return detentionInHrs;
	}

	public void setDetentionInHrs(Double detentionInHrs) {
		this.detentionInHrs = detentionInHrs;
	}

	public String getRoadwayBillId() {
		return roadwayBillId;
	}

	public void setRoadwayBillId(String roadwayBillId) {
		this.roadwayBillId = roadwayBillId;
	}

	public TractorSummaryDTO getTractor() {
		return tractor;
	}

	public void setTractor(TractorSummaryDTO tractor) {
		this.tractor = tractor;
	}

	public TrailerSummaryDTO getTrailer() {
		return trailer;
	}

	public void setTrailer(TrailerSummaryDTO trailer) {
		this.trailer = trailer;
	}

	public EmployeeSummaryDTO getDriver1() {
		return driver1;
	}

	public void setDriver1(EmployeeSummaryDTO driver1) {
		this.driver1 = driver1;
	}

	public EmployeeSummaryDTO getDriver2() {
		return driver2;
	}

	public void setDriver2(EmployeeSummaryDTO driver2) {
		this.driver2 = driver2;
	}
}
