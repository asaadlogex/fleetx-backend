package com.fleetX.dto;

import com.fleetX.entity.dropdown.DContractType;

public class CompanyContractSummaryDTO {
	private String companyContractId;
	private String customerName;
	private DContractType contractType;
	private String contractStatus;

	public String getCompanyContractId() {
		return companyContractId;
	}

	public void setCompanyContractId(String companyContractId) {
		this.companyContractId = companyContractId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public DContractType getContractType() {
		return contractType;
	}

	public void setContractType(DContractType contractType) {
		this.contractType = contractType;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

}
