package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DContractType;

public class CompanyContractDTO {
	private String companyContractId;
	private String adminCompanyId;
	private String customerId;
	private String contractStart;
	private String contractEnd;
	private DContractType contractType;
	private Double fixedRate;
	private Double haltingRate;
	private Double detentionRate;
	private List<String> assetCombinationId;
	private List<RouteRateContractDTO> routeRateContracts;
	private String contractStatus;
	private String contractDocUrl;

	public String getCompanyContractId() {
		return companyContractId;
	}

	public void setCompanyContractId(String companyContractId) {
		this.companyContractId = companyContractId;
	}

	public String getAdminCompanyId() {
		return adminCompanyId;
	}

	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getContractStart() {
		return contractStart;
	}

	public void setContractStart(String contractStart) {
		this.contractStart = contractStart;
	}

	public String getContractEnd() {
		return contractEnd;
	}

	public void setContractEnd(String contractEnd) {
		this.contractEnd = contractEnd;
	}

	public DContractType getContractType() {
		return contractType;
	}

	public void setContractType(DContractType contractType) {
		this.contractType = contractType;
	}

	public Double getFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(Double fixedRate) {
		this.fixedRate = fixedRate;
	}

	public List<String> getAssetCombinationId() {
		return assetCombinationId;
	}

	public void setAssetCombinationId(List<String> assetCombinationId) {
		this.assetCombinationId = assetCombinationId;
	}

	public List<RouteRateContractDTO> getRouteRateContracts() {
		return routeRateContracts;
	}

	public void setRouteRateContracts(List<RouteRateContractDTO> routeRateContracts) {
		this.routeRateContracts = routeRateContracts;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractDocUrl() {
		return contractDocUrl;
	}

	public void setContractDocUrl(String contractDocUrl) {
		this.contractDocUrl = contractDocUrl;
	}

	public Double getHaltingRate() {
		return haltingRate;
	}

	public void setHaltingRate(Double haltingRate) {
		this.haltingRate = haltingRate;
	}

	public Double getDetentionRate() {
		return detentionRate;
	}

	public void setDetentionRate(Double detentionRate) {
		this.detentionRate = detentionRate;
	}
}
