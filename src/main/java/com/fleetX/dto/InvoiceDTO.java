package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DState;

public class InvoiceDTO {
	private Integer invoiceId;
	private String invoiceDate;
	private SenderDTO sender;
	private RecipientDTO recipient;
	private List<LineItemDTO> lineItems;
	private Double grossAmount;
	private DState taxState;
	private Double tax;
	private String taxPercent;
	private Double totalAmount;
	private String amountInWords;
	private Double amountPaid;
	private Double remainingAmount;
	private Integer month;
	private Integer year;
	private DPaymentStatus paymentStatus;
	private DInvoiceType invoiceType;
	private DHInvoiceDTO dhInvoice;
	private String note;

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public SenderDTO getSender() {
		return sender;
	}

	public void setSender(SenderDTO sender) {
		this.sender = sender;
	}

	public RecipientDTO getRecipient() {
		return recipient;
	}

	public void setRecipient(RecipientDTO recipient) {
		this.recipient = recipient;
	}

	public List<LineItemDTO> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItemDTO> lineItems) {
		this.lineItems = lineItems;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public DPaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(DPaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public DInvoiceType getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(DInvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public DState getTaxState() {
		return taxState;
	}

	public void setTaxState(DState taxState) {
		this.taxState = taxState;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(String taxPercent) {
		this.taxPercent = taxPercent;
	}

	public Double getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(Double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAmountInWords() {
		return amountInWords;
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public DHInvoiceDTO getDhInvoice() {
		return dhInvoice;
	}

	public void setDhInvoice(DHInvoiceDTO dhInvoice) {
		this.dhInvoice = dhInvoice;
	}

}
