package com.fleetX.dto;

public class EmployeeContractDTO {
	private Integer employeeContractId;
	private String startDate;
	private String endDate;
	private Double salary;
	private String contractStatus;
	private String separationReason;
	private String workingExperience;
	private String previousEmployer1;
	private String previousEmployer2;

	public Integer getEmployeeContractId() {
		return employeeContractId;
	}

	public void setEmployeeContractId(Integer employeeContractId) {
		this.employeeContractId = employeeContractId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getSeparationReason() {
		return separationReason;
	}

	public void setSeparationReason(String separationReason) {
		this.separationReason = separationReason;
	}

	public String getWorkingExperience() {
		return workingExperience;
	}

	public void setWorkingExperience(String workingExperience) {
		this.workingExperience = workingExperience;
	}

	public String getPreviousEmployer1() {
		return previousEmployer1;
	}

	public void setPreviousEmployer1(String previousEmployer1) {
		this.previousEmployer1 = previousEmployer1;
	}

	public String getPreviousEmployer2() {
		return previousEmployer2;
	}

	public void setPreviousEmployer2(String previousEmployer2) {
		this.previousEmployer2 = previousEmployer2;
	}
}
