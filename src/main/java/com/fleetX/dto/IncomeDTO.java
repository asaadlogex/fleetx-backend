package com.fleetX.dto;

import com.fleetX.entity.dropdown.DIncomeType;

public class IncomeDTO {
	private Integer incomeId;
	private String description;
	private Double amount;
	private DIncomeType incomeType;
	private String roadwayBillId;
	
	public DIncomeType getIncomeType() {
		return incomeType;
	}
	public void setIncomeType(DIncomeType incomeType) {
		this.incomeType = incomeType;
	}
	public Integer getIncomeId() {
		return incomeId;
	}
	public void setIncomeId(Integer incomeId) {
		this.incomeId = incomeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getRoadwayBillId() {
		return roadwayBillId;
	}
	public void setRoadwayBillId(String roadwayBillId) {
		this.roadwayBillId = roadwayBillId;
	}
}
