package com.fleetX.dto;

import com.fleetX.entity.dropdown.DBrand;
import com.fleetX.entity.dropdown.DItemCategory;
import com.fleetX.entity.dropdown.DItemType;
import com.fleetX.entity.dropdown.DUnit;

public class ItemDTO {
	private Integer itemId;
	private String itemName;
	private String itemSku;
	private Double itemPrice;
	private DItemType itemType;
	private DUnit itemUnit;
	private DItemCategory itemCategory;
	private DBrand itemBrand;
	private DimensionDTO itemDimension;
	private Double weightInKg;
	private String itemUpc;
	private String itemEan;
	private String itemMpn;
	private String itemIsbn;
	private Integer criticalQty;
	private Boolean isRefundable;
	private String itemPicUrl;
	private InventoryDetailDTO inventory;

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemSku() {
		return itemSku;
	}

	public void setItemSku(String itemSku) {
		this.itemSku = itemSku;
	}

	public DItemType getItemType() {
		return itemType;
	}

	public void setItemType(DItemType itemType) {
		this.itemType = itemType;
	}

	public DUnit getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(DUnit itemUnit) {
		this.itemUnit = itemUnit;
	}

	public DItemCategory getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(DItemCategory itemCategory) {
		this.itemCategory = itemCategory;
	}

	public DBrand getItemBrand() {
		return itemBrand;
	}

	public void setItemBrand(DBrand itemBrand) {
		this.itemBrand = itemBrand;
	}

	public DimensionDTO getItemDimension() {
		return itemDimension;
	}

	public void setItemDimension(DimensionDTO itemDimension) {
		this.itemDimension = itemDimension;
	}

	public Double getWeightInKg() {
		return weightInKg;
	}

	public void setWeightInKg(Double weightInKg) {
		this.weightInKg = weightInKg;
	}

	public String getItemUpc() {
		return itemUpc;
	}

	public void setItemUpc(String itemUpc) {
		this.itemUpc = itemUpc;
	}

	public String getItemEan() {
		return itemEan;
	}

	public void setItemEan(String itemEan) {
		this.itemEan = itemEan;
	}

	public String getItemMpn() {
		return itemMpn;
	}

	public void setItemMpn(String itemMpn) {
		this.itemMpn = itemMpn;
	}

	public String getItemIsbn() {
		return itemIsbn;
	}

	public void setItemIsbn(String itemIsbn) {
		this.itemIsbn = itemIsbn;
	}

	public String getItemPicUrl() {
		return itemPicUrl;
	}

	public void setItemPicUrl(String itemPicUrl) {
		this.itemPicUrl = itemPicUrl;
	}

	public Boolean getIsRefundable() {
		return isRefundable;
	}

	public void setIsRefundable(Boolean isRefundable) {
		this.isRefundable = isRefundable;
	}

	public Double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public Integer getCriticalQty() {
		return criticalQty;
	}

	public void setCriticalQty(Integer criticalQty) {
		this.criticalQty = criticalQty;
	}

	public InventoryDetailDTO getInventory() {
		return inventory;
	}

	public void setInventory(InventoryDetailDTO inventory) {
		this.inventory = inventory;
	}

}
