package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DPurchaseOrderStatus;

public class PurchaseOrderDTO {
	private Integer purchaseOrderId;
	private String referenceNumber;
	private CompanySummaryDTO vendor;
	private String orderedOn;
	private String expectedOn;
	private String receivedOn;
	private DPurchaseOrderStatus purchaseOrderStatus;
	private List<ItemPurchaseDetailDTO> itemPurchaseDetail;
	private Double grossAmount;
	private Double discountInAmount;
	private Double totalAmount;

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getOrderedOn() {
		return orderedOn;
	}

	public void setOrderedOn(String orderedOn) {
		this.orderedOn = orderedOn;
	}

	public String getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(String receivedOn) {
		this.receivedOn = receivedOn;
	}

	public DPurchaseOrderStatus getPurchaseOrderStatus() {
		return purchaseOrderStatus;
	}

	public void setPurchaseOrderStatus(DPurchaseOrderStatus purchaseOrderStatus) {
		this.purchaseOrderStatus = purchaseOrderStatus;
	}

	public List<ItemPurchaseDetailDTO> getItemPurchaseDetail() {
		return itemPurchaseDetail;
	}

	public void setItemPurchaseDetail(List<ItemPurchaseDetailDTO> itemPurchaseDetail) {
		this.itemPurchaseDetail = itemPurchaseDetail;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getDiscountInAmount() {
		return discountInAmount;
	}

	public void setDiscountInAmount(Double discountInAmount) {
		this.discountInAmount = discountInAmount;
	}

	public Double getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(Double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public String getExpectedOn() {
		return expectedOn;
	}

	public void setExpectedOn(String expectedOn) {
		this.expectedOn = expectedOn;
	}

	public CompanySummaryDTO getVendor() {
		return vendor;
	}

	public void setVendor(CompanySummaryDTO vendor) {
		this.vendor = vendor;
	}

}
