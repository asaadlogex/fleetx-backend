package com.fleetX.dto;

import java.util.ArrayList;
import java.util.List;

import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DReimbursementStatus;

public class JobDTO {
	private String jobId;
	private DJobStatus jobStatus;
	private DReimbursementStatus reimbursementStatus;
	private Double jobAdvance;
	private Double startKm;
	private Double endKm;
	private Double totalKm;
	private Double odometerKm;
	private Double tmsKm;
	private Double trackerKm;
	private String startDate;
	private String endDate;
	private TractorSummaryDTO tractor;
	private TrailerSummaryDTO trailer;
	private ContainerDTO container;
	private TrackerDTO tracker;
	private EmployeeSummaryDTO driver1;
	private EmployeeSummaryDTO driver2;
	private RentalVehicleDTO rentalVehicle;
	private Double reimbursementExpense;
	private DBaseStation startBaseStation;
	private DBaseStation endBaseStation;
	private List<JobFuelDetailDTO> jobFuelDetails = new ArrayList<>();
	private List<RoadwayBillSummaryDTO> roadwayBills = new ArrayList<>();

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public DJobStatus getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(DJobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

	public Double getJobAdvance() {
		return jobAdvance;
	}

	public void setJobAdvance(Double jobAdvance) {
		this.jobAdvance = jobAdvance;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public DBaseStation getStartBaseStation() {
		return startBaseStation;
	}

	public void setStartBaseStation(DBaseStation startBaseStation) {
		this.startBaseStation = startBaseStation;
	}

	public DBaseStation getEndBaseStation() {
		return endBaseStation;
	}

	public void setEndBaseStation(DBaseStation endBaseStation) {
		this.endBaseStation = endBaseStation;
	}

	public List<JobFuelDetailDTO> getJobFuelDetails() {
		return jobFuelDetails;
	}

	public void setJobFuelDetails(List<JobFuelDetailDTO> jobFuelDetails) {
		this.jobFuelDetails = jobFuelDetails;
	}

	public List<RoadwayBillSummaryDTO> getRoadwayBills() {
		return roadwayBills;
	}

	public void setRoadwayBills(List<RoadwayBillSummaryDTO> roadwayBills) {
		this.roadwayBills = roadwayBills;
	}

	public Double getStartKm() {
		return startKm;
	}

	public void setStartKm(Double startKm) {
		this.startKm = startKm;
	}

	public Double getEndKm() {
		return endKm;
	}

	public void setEndKm(Double endKm) {
		this.endKm = endKm;
	}

	public DReimbursementStatus getReimbursementStatus() {
		return reimbursementStatus;
	}

	public void setReimbursementStatus(DReimbursementStatus reimbursementStatus) {
		this.reimbursementStatus = reimbursementStatus;
	}

	public Double getReimbursementExpense() {
		return reimbursementExpense;
	}

	public void setReimbursementExpense(Double reimbursementExpense) {
		this.reimbursementExpense = reimbursementExpense;
	}

	public ContainerDTO getContainer() {
		return container;
	}

	public void setContainer(ContainerDTO container) {
		this.container = container;
	}

	public TrackerDTO getTracker() {
		return tracker;
	}

	public void setTracker(TrackerDTO tracker) {
		this.tracker = tracker;
	}

	public Double getTmsKm() {
		return tmsKm;
	}

	public void setTmsKm(Double tmsKm) {
		this.tmsKm = tmsKm;
	}

	public Double getTrackerKm() {
		return trackerKm;
	}

	public void setTrackerKm(Double trackerKm) {
		this.trackerKm = trackerKm;
	}

	public TractorSummaryDTO getTractor() {
		return tractor;
	}

	public void setTractor(TractorSummaryDTO tractor) {
		this.tractor = tractor;
	}

	public TrailerSummaryDTO getTrailer() {
		return trailer;
	}

	public void setTrailer(TrailerSummaryDTO trailer) {
		this.trailer = trailer;
	}

	public EmployeeSummaryDTO getDriver1() {
		return driver1;
	}

	public void setDriver1(EmployeeSummaryDTO driver1) {
		this.driver1 = driver1;
	}

	public EmployeeSummaryDTO getDriver2() {
		return driver2;
	}

	public void setDriver2(EmployeeSummaryDTO driver2) {
		this.driver2 = driver2;
	}

	public RentalVehicleDTO getRentalVehicle() {
		return rentalVehicle;
	}

	public void setRentalVehicle(RentalVehicleDTO rentalVehicle) {
		this.rentalVehicle = rentalVehicle;
	}

	public Double getOdometerKm() {
		return odometerKm;
	}

	public void setOdometerKm(Double odometerKm) {
		this.odometerKm = odometerKm;
	}
}
