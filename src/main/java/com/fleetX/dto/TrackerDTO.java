package com.fleetX.dto;

import com.fleetX.entity.dropdown.DLeaseType;

public class TrackerDTO {
	private String trackerId;
	private String status;
	private String contractTerms;
	private DLeaseType leaseType;
	private String supplierId;
	private String adminCompanyId;
	private String installationDate;
	private String returnDate;
	private String trackerNumber;
	private String trackerName;

	public String getAdminCompanyId() {
		return adminCompanyId;
	}

	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getTrackerId() {
		return trackerId;
	}

	public void setTrackerId(String trackerId) {
		this.trackerId = trackerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContractTerms() {
		return contractTerms;
	}

	public void setContractTerms(String contractTerms) {
		this.contractTerms = contractTerms;
	}

	public DLeaseType getLeaseType() {
		return leaseType;
	}

	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getTrackerNumber() {
		return trackerNumber;
	}

	public void setTrackerNumber(String trackerNumber) {
		this.trackerNumber = trackerNumber;
	}

	public String getTrackerName() {
		return trackerName;
	}

	public void setTrackerName(String trackerName) {
		this.trackerName = trackerName;
	}
}
