package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DCustomerType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DPaymentMode;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.dropdown.DVehicleOwnership;

public class RoadwayBillDTO {
	private String rwbId;
	private String orderNumber;
	private String loadNumber;
	private Double weightInTon;
	private Double totalKm;
	private Double tmsKm;
	private Double trackerKm;
	private Double totalIncome;
	private Double totalExpense;
	private Double netIncome;
	private DCustomerType customerType;
	private TractorSummaryDTO tractor;
	private TrailerSummaryDTO trailer;
	private ContainerDTO container;
	private TrackerDTO tracker;
	private EmployeeSummaryDTO driver1;
	private EmployeeSummaryDTO driver2;
	private String assetCombinationId;
	private DTrailerType equipmentType;
	private DVehicleOwnership vehicleOwnership;
	private RentalVehicleDTO rentalVehicle;
	private DRWBStatus rwbStatus;
	private Boolean rateStatus;
	private DInvoiceStatus invoiceStatus;
	private DState taxState;
	private String rwbDate;
	private String bolNumber;
	private DPaymentMode paymentMode;
	private CompanySummaryDTO customer;
	private CompanySummaryDTO broker;
	private Boolean isArea;
	private DCity city;
	private RouteDTO route;
	private String instructions;
	private String jobId;
	private List<StopDTO> stops;
	private List<ProductDTO> products;
	private List<IncomeDTO> incomes;
	private List<ExpenseDTO> expenses;
	private List<StopProgressDTO> stopProgresses;
	private String podUrl;
	private Boolean isEmpty;
	private Double durationInHrs;

	public String getRwbId() {
		return rwbId;
	}

	public void setRwbId(String rwbId) {
		this.rwbId = rwbId;
	}

	public Boolean getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(Boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

	public Boolean getRateStatus() {
		return rateStatus;
	}

	public void setRateStatus(Boolean rateStatus) {
		this.rateStatus = rateStatus;
	}

	public RentalVehicleDTO getRentalVehicle() {
		return rentalVehicle;
	}

	public void setRentalVehicle(RentalVehicleDTO rentalVehicle) {
		this.rentalVehicle = rentalVehicle;
	}

	public DVehicleOwnership getVehicleOwnership() {
		return vehicleOwnership;
	}

	public void setVehicleOwnership(DVehicleOwnership vehicleOwnership) {
		this.vehicleOwnership = vehicleOwnership;
	}

	public ContainerDTO getContainer() {
		return container;
	}

	public void setContainer(ContainerDTO container) {
		this.container = container;
	}

	public TrackerDTO getTracker() {
		return tracker;
	}

	public void setTracker(TrackerDTO tracker) {
		this.tracker = tracker;
	}

	public String getAssetCombinationId() {
		return assetCombinationId;
	}

	public void setAssetCombinationId(String assetCombinationId) {
		this.assetCombinationId = assetCombinationId;
	}

	public DTrailerType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(DTrailerType equipmentType) {
		this.equipmentType = equipmentType;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getLoadNumber() {
		return loadNumber;
	}

	public void setLoadNumber(String loadNumber) {
		this.loadNumber = loadNumber;
	}

	public DCustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(DCustomerType customerType) {
		this.customerType = customerType;
	}

	public DRWBStatus getRwbStatus() {
		return rwbStatus;
	}

	public void setRwbStatus(DRWBStatus rwbStatus) {
		this.rwbStatus = rwbStatus;
	}

	public String getRwbDate() {
		return rwbDate;
	}

	public void setRwbDate(String rwbDate) {
		this.rwbDate = rwbDate;
	}

	public String getBolNumber() {
		return bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public DPaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(DPaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public RouteDTO getRoute() {
		return route;
	}

	public void setRoute(RouteDTO route) {
		this.route = route;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public List<StopDTO> getStops() {
		return stops;
	}

	public void setStops(List<StopDTO> stops) {
		this.stops = stops;
	}

	public List<ProductDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}

	public List<IncomeDTO> getIncomes() {
		return incomes;
	}

	public void setIncomes(List<IncomeDTO> incomes) {
		this.incomes = incomes;
	}

	public List<ExpenseDTO> getExpenses() {
		return expenses;
	}

	public void setExpenses(List<ExpenseDTO> expenses) {
		this.expenses = expenses;
	}

	public List<StopProgressDTO> getStopProgresses() {
		return stopProgresses;
	}

	public void setStopProgresses(List<StopProgressDTO> stopProgresses) {
		this.stopProgresses = stopProgresses;
	}

	public String getPodUrl() {
		return podUrl;
	}

	public void setPodUrl(String podUrl) {
		this.podUrl = podUrl;
	}

	public DInvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(DInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public DState getTaxState() {
		return taxState;
	}

	public void setTaxState(DState taxState) {
		this.taxState = taxState;
	}

	public Boolean getIsArea() {
		return isArea;
	}

	public void setIsArea(Boolean isArea) {
		this.isArea = isArea;
	}

	public DCity getCity() {
		return city;
	}

	public void setCity(DCity city) {
		this.city = city;
	}

	public Double getWeightInTon() {
		return weightInTon;
	}

	public void setWeightInTon(Double weightInTon) {
		this.weightInTon = weightInTon;
	}

	public Double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(Double totalIncome) {
		this.totalIncome = totalIncome;
	}

	public Double getTotalExpense() {
		return totalExpense;
	}

	public void setTotalExpense(Double totalExpense) {
		this.totalExpense = totalExpense;
	}

	public Double getNetIncome() {
		return netIncome;
	}

	public void setNetIncome(Double netIncome) {
		this.netIncome = netIncome;
	}

	public Double getTmsKm() {
		return tmsKm;
	}

	public void setTmsKm(Double tmsKm) {
		this.tmsKm = tmsKm;
	}

	public Double getTrackerKm() {
		return trackerKm;
	}

	public void setTrackerKm(Double trackerKm) {
		this.trackerKm = trackerKm;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public TractorSummaryDTO getTractor() {
		return tractor;
	}

	public void setTractor(TractorSummaryDTO tractor) {
		this.tractor = tractor;
	}

	public TrailerSummaryDTO getTrailer() {
		return trailer;
	}

	public void setTrailer(TrailerSummaryDTO trailer) {
		this.trailer = trailer;
	}

	public EmployeeSummaryDTO getDriver1() {
		return driver1;
	}

	public void setDriver1(EmployeeSummaryDTO driver1) {
		this.driver1 = driver1;
	}

	public EmployeeSummaryDTO getDriver2() {
		return driver2;
	}

	public void setDriver2(EmployeeSummaryDTO driver2) {
		this.driver2 = driver2;
	}

	public CompanySummaryDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CompanySummaryDTO customer) {
		this.customer = customer;
	}

	public CompanySummaryDTO getBroker() {
		return broker;
	}

	public void setBroker(CompanySummaryDTO broker) {
		this.broker = broker;
	}

	public Double getDurationInHrs() {
		return durationInHrs;
	}

	public void setDurationInHrs(Double durationInHrs) {
		this.durationInHrs = durationInHrs;
	}

}
