package com.fleetX.dto;

public class ReferenceDTO {
	private String referenceId;
	private String referenceName;
	private String referenceContact;
	private String referenceRelation;
	private String referenceAddress;
	private String referenceCnic;
	private String employeeId;
	private String cnicUrl;
	private String referenceFormUrl;
	
	public ReferenceDTO() {}
	
	public ReferenceDTO(String referenceId, String referenceName, String referenceContact, String referenceRelation,
			String referenceAddress, String referenceCnic, String employeeId,String cnicUrl,String referenceFormUrl) {
		this.referenceId = referenceId;
		this.referenceName = referenceName;
		this.referenceContact = referenceContact;
		this.referenceRelation = referenceRelation;
		this.referenceAddress = referenceAddress;
		this.referenceCnic = referenceCnic;
		this.employeeId = employeeId;
		this.cnicUrl=cnicUrl;
		this.referenceFormUrl=referenceFormUrl;
	}

	public String getCnicUrl() {
		return cnicUrl;
	}

	public void setCnicUrl(String cnicUrl) {
		this.cnicUrl = cnicUrl;
	}

	public String getReferenceFormUrl() {
		return referenceFormUrl;
	}

	public void setReferenceFormUrl(String referenceFormUrl) {
		this.referenceFormUrl = referenceFormUrl;
	}

	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getReferenceName() {
		return referenceName;
	}
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}
	public String getReferenceContact() {
		return referenceContact;
	}
	public void setReferenceContact(String referenceContact) {
		this.referenceContact = referenceContact;
	}
	public String getReferenceRelation() {
		return referenceRelation;
	}
	public void setReferenceRelation(String referenceRelation) {
		this.referenceRelation = referenceRelation;
	}
	public String getReferenceAddress() {
		return referenceAddress;
	}
	public void setReferenceAddress(String referenceAddress) {
		this.referenceAddress = referenceAddress;
	}
	public String getReferenceCnic() {
		return referenceCnic;
	}
	public void setReferenceCnic(String referenceCnic) {
		this.referenceCnic = referenceCnic;
	}
}
