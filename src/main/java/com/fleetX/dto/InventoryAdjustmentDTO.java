package com.fleetX.dto;

public class InventoryAdjustmentDTO {
	private Integer inventoryAdjustmentId;
	private ItemDTO item;
	private String inventoryAdjustmentDate;
	private Integer adjustmentQty;
	private String referenceNumber;
	private String adjustmentReason;

	public Integer getInventoryAdjustmentId() {
		return inventoryAdjustmentId;
	}

	public void setInventoryAdjustmentId(Integer inventoryAdjustmentId) {
		this.inventoryAdjustmentId = inventoryAdjustmentId;
	}

	public ItemDTO getItem() {
		return item;
	}

	public void setItem(ItemDTO item) {
		this.item = item;
	}

	public String getInventoryAdjustmentDate() {
		return inventoryAdjustmentDate;
	}

	public void setInventoryAdjustmentDate(String inventoryAdjustmentDate) {
		this.inventoryAdjustmentDate = inventoryAdjustmentDate;
	}

	public Integer getAdjustmentQty() {
		return adjustmentQty;
	}

	public void setAdjustmentQty(Integer adjustmentQty) {
		this.adjustmentQty = adjustmentQty;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getAdjustmentReason() {
		return adjustmentReason;
	}

	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}

}
