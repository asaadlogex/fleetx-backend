package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DPriority;
import com.fleetX.entity.dropdown.DWorkOrderCategory;
import com.fleetX.entity.dropdown.DWorkOrderStatus;
import com.fleetX.entity.dropdown.DWorkOrderSubCategory;

public class WorkOrderDTO {
	private Integer workOrderId;
	private TractorSummaryDTO tractor;
	private Double tractorKm;
	private DBaseStation baseStation;
	private DPriority priority;
	private String workOrderDate;
	private String workOrderCompletionDate;
	private DWorkOrderStatus workOrderStatus;
	private DWorkOrderCategory workOrderCategory;
	private DWorkOrderSubCategory workOrderSubCategory;
	private List<WorkOrderItemDetailDTO> workOrderItemDetails;
	private String referenceNumber;
	private String senderName;
	private Double totalAmount;
	private String activityDetail;
	private CompanySummaryDTO workshop;

	public Integer getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(Integer workOrderId) {
		this.workOrderId = workOrderId;
	}

	public TractorSummaryDTO getTractor() {
		return tractor;
	}

	public void setTractor(TractorSummaryDTO tractor) {
		this.tractor = tractor;
	}

	public Double getTractorKm() {
		return tractorKm;
	}

	public void setTractorKm(Double tractorKm) {
		this.tractorKm = tractorKm;
	}

	public DBaseStation getBaseStation() {
		return baseStation;
	}

	public void setBaseStation(DBaseStation baseStation) {
		this.baseStation = baseStation;
	}

	public DPriority getPriority() {
		return priority;
	}

	public void setPriority(DPriority priority) {
		this.priority = priority;
	}

	public String getWorkOrderDate() {
		return workOrderDate;
	}

	public void setWorkOrderDate(String workOrderDate) {
		this.workOrderDate = workOrderDate;
	}

	public String getWorkOrderCompletionDate() {
		return workOrderCompletionDate;
	}

	public void setWorkOrderCompletionDate(String workOrderCompletionDate) {
		this.workOrderCompletionDate = workOrderCompletionDate;
	}

	public DWorkOrderStatus getWorkOrderStatus() {
		return workOrderStatus;
	}

	public void setWorkOrderStatus(DWorkOrderStatus workOrderStatus) {
		this.workOrderStatus = workOrderStatus;
	}

	public DWorkOrderCategory getWorkOrderCategory() {
		return workOrderCategory;
	}

	public void setWorkOrderCategory(DWorkOrderCategory workOrderCategory) {
		this.workOrderCategory = workOrderCategory;
	}

	public DWorkOrderSubCategory getWorkOrderSubCategory() {
		return workOrderSubCategory;
	}

	public void setWorkOrderSubCategory(DWorkOrderSubCategory workOrderSubCategory) {
		this.workOrderSubCategory = workOrderSubCategory;
	}

	public List<WorkOrderItemDetailDTO> getWorkOrderItemDetails() {
		return workOrderItemDetails;
	}

	public void setWorkOrderItemDetails(List<WorkOrderItemDetailDTO> workOrderItemDetails) {
		this.workOrderItemDetails = workOrderItemDetails;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getActivityDetail() {
		return activityDetail;
	}

	public void setActivityDetail(String activityDetail) {
		this.activityDetail = activityDetail;
	}

	public CompanySummaryDTO getWorkshop() {
		return workshop;
	}

	public void setWorkshop(CompanySummaryDTO workshop) {
		this.workshop = workshop;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
