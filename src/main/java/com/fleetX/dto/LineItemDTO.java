package com.fleetX.dto;

public class LineItemDTO {
	private Integer lineItemId;
	private String rwbId;
	private String assetCombinationId;
	private String vehicleTypeAndSize;
	private RouteDTO route;
	private Double standardKm;
	private Double weightInTon;
	private String date;
	private String loadNumber;
	private String vehicleNumber;
	private Double ratePerKm;
	private Double ratePerTon;
	private Double tripCharges;
	private Double loadingCharges;
	private Double offLoadingCharges;
	private Double otherCharges;
	private Double totalAmount;

	public Integer getLineItemId() {
		return lineItemId;
	}

	public void setLineItemId(Integer lineItemId) {
		this.lineItemId = lineItemId;
	}

	public String getRwbId() {
		return rwbId;
	}

	public void setRwbId(String rwbId) {
		this.rwbId = rwbId;
	}

	public RouteDTO getRoute() {
		return route;
	}

	public void setRoute(RouteDTO route) {
		this.route = route;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLoadNumber() {
		return loadNumber;
	}

	public void setLoadNumber(String loadNumber) {
		this.loadNumber = loadNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Double getTripCharges() {
		return tripCharges;
	}

	public void setTripCharges(Double tripCharges) {
		this.tripCharges = tripCharges;
	}

	public Double getLoadingCharges() {
		return loadingCharges;
	}

	public void setLoadingCharges(Double loadingCharges) {
		this.loadingCharges = loadingCharges;
	}

	public Double getOffLoadingCharges() {
		return offLoadingCharges;
	}

	public void setOffLoadingCharges(Double offLoadingCharges) {
		this.offLoadingCharges = offLoadingCharges;
	}

	public Double getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(Double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAssetCombinationId() {
		return assetCombinationId;
	}

	public void setAssetCombinationId(String assetCombinationId) {
		this.assetCombinationId = assetCombinationId;
	}

	public Double getRatePerKm() {
		return ratePerKm;
	}

	public void setRatePerKm(Double ratePerKm) {
		this.ratePerKm = ratePerKm;
	}

	public Double getRatePerTon() {
		return ratePerTon;
	}

	public void setRatePerTon(Double ratePerTon) {
		this.ratePerTon = ratePerTon;
	}

	public Double getStandardKm() {
		return standardKm;
	}

	public void setStandardKm(Double standardKm) {
		this.standardKm = standardKm;
	}

	public Double getWeightInTon() {
		return weightInTon;
	}

	public void setWeightInTon(Double weightInTon) {
		this.weightInTon = weightInTon;
	}

	public String getVehicleTypeAndSize() {
		return vehicleTypeAndSize;
	}

	public void setVehicleTypeAndSize(String vehicleTypeAndSize) {
		this.vehicleTypeAndSize = vehicleTypeAndSize;
	}
}
