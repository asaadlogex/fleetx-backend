package com.fleetX.dto;

public class SenderDTO {
	private String name;
	private String address;
	private String NTN;
	private String STRN;
	private String PRA;
	private String BRA;
	private String KRA;
	private String SRA;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNTN() {
		return NTN;
	}

	public void setNTN(String nTN) {
		NTN = nTN;
	}

	public String getSTRN() {
		return STRN;
	}

	public void setSTRN(String sTRN) {
		STRN = sTRN;
	}

	public String getPRA() {
		return PRA;
	}

	public void setPRA(String Pra) {
		PRA = Pra;
	}

	public String getBRA() {
		return BRA;
	}

	public void setBRA(String bRA) {
		BRA = bRA;
	}

	public String getKRA() {
		return KRA;
	}

	public void setKRA(String kRA) {
		KRA = kRA;
	}

	public String getSRA() {
		return SRA;
	}

	public void setSRA(String sRA) {
		SRA = sRA;
	}

}
