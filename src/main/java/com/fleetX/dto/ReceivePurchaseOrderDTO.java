package com.fleetX.dto;

import java.util.Date;
import java.util.List;

public class ReceivePurchaseOrderDTO {
	private Integer purchaseOrderId;
	private Date date;
	private List<ReceiveItemDTO> itemReceived;

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<ReceiveItemDTO> getItemReceived() {
		return itemReceived;
	}

	public void setItemReceived(List<ReceiveItemDTO> itemReceived) {
		this.itemReceived = itemReceived;
	}
}
