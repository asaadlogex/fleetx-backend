package com.fleetX.dto;

import com.fleetX.entity.dropdown.DMake;

public class TractorSummaryDTO {
	private String tractorId;
	private String engineNumber;
	private String numberPlate;
	private String vin;
	private DMake assetMake;
	private String model;
	private String status;

	public String getTractorId() {
		return tractorId;
	}

	public void setTractorId(String tractorId) {
		this.tractorId = tractorId;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getNumberPlate() {
		return numberPlate;
	}

	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public DMake getAssetMake() {
		return assetMake;
	}

	public void setAssetMake(DMake assetMake) {
		this.assetMake = assetMake;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
