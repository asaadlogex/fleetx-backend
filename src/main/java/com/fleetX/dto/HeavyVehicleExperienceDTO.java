package com.fleetX.dto;

public class HeavyVehicleExperienceDTO {
	private String heavyVehicleExperienceId;
	private String experienceStartDate;
	private String experienceEndDate;
	private String trucksOperated;
	private String experienceCompanyName;
	private String employeeId;
	
	public String getHeavyVehicleExperienceId() {
		return heavyVehicleExperienceId;
	}
	public void setHeavyVehicleExperienceId(String heavyVehicleExperienceId) {
		this.heavyVehicleExperienceId = heavyVehicleExperienceId;
	}
	public String getExperienceStartDate() {
		return experienceStartDate;
	}
	public void setExperienceStartDate(String experienceStartDate) {
		this.experienceStartDate = experienceStartDate;
	}
	public String getExperienceEndDate() {
		return experienceEndDate;
	}
	public void setExperienceEndDate(String experienceEndDate) {
		this.experienceEndDate = experienceEndDate;
	}
	public String getTrucksOperated() {
		return trucksOperated;
	}
	public void setTrucksOperated(String trucksOperated) {
		this.trucksOperated = trucksOperated;
	}
	public String getExperienceCompanyName() {
		return experienceCompanyName;
	}
	public void setExperienceCompanyName(String experienceCompanyName) {
		this.experienceCompanyName = experienceCompanyName;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
}
