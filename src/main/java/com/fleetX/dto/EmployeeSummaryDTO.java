package com.fleetX.dto;

import com.fleetX.entity.dropdown.DDepartment;
import com.fleetX.entity.dropdown.DPosition;

public class EmployeeSummaryDTO {
	private String employeeId;
	private String employeeNumber;
	private String employeeName;
	private DDepartment department;
	private DPosition position;
	private String cnic;
	private String dateOfBirth;
	private String licenceNumber;
	private String status;

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public DDepartment getDepartment() {
		return department;
	}

	public void setDepartment(DDepartment department) {
		this.department = department;
	}

	public DPosition getPosition() {
		return position;
	}

	public void setPosition(DPosition position) {
		this.position = position;
	}

	public String getCnic() {
		return cnic;
	}

	public void setCnic(String cnic) {
		this.cnic = cnic;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
