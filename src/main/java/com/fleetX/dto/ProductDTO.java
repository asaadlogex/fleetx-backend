package com.fleetX.dto;

import com.fleetX.entity.dropdown.DCategory;

public class ProductDTO {
	private Integer productId;
	private DCategory category;
	private String commodity;
	private Double weight;
	private String weightUnit;
	private Integer packageCount;
	private Double temperature;
	private String roadwayBillId;
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public DCategory getCategory() {
		return category;
	}
	public void setCategory(DCategory category) {
		this.category = category;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	public Integer getPackageCount() {
		return packageCount;
	}
	public void setPackageCount(Integer packageCount) {
		this.packageCount = packageCount;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public String getRoadwayBillId() {
		return roadwayBillId;
	}
	public void setRoadwayBillId(String roadwayBillId) {
		this.roadwayBillId = roadwayBillId;
	}
}
