package com.fleetX.dto;

public class ReceiveItemDTO {
	private Integer itemPurchaseDetailId;
	private Integer qtyReceived;

	public Integer getItemPurchaseDetailId() {
		return itemPurchaseDetailId;
	}

	public void setItemPurchaseDetailId(Integer itemPurchaseDetailId) {
		this.itemPurchaseDetailId = itemPurchaseDetailId;
	}

	public Integer getQtyReceived() {
		return qtyReceived;
	}

	public void setQtyReceived(Integer qtyReceived) {
		this.qtyReceived = qtyReceived;
	}

}