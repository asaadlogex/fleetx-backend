package com.fleetX.dto;

public class MedicalTestReportDTO {
	private String mtrId;
	private String mtrType;
	private String mtrDate;
	private String mtrHospital;
	private String mtrStation;
	private String mtrResult;
	private String employeeId;
	private String url;
	
	public MedicalTestReportDTO() {}

	public MedicalTestReportDTO(String mtrId, String mtrType, String mtrDate, String mtrHospital, String mtrStation,
			String mtrResult, String employeeId, String url) {
		super();
		this.mtrId = mtrId;
		this.mtrType = mtrType;
		this.mtrDate = mtrDate;
		this.mtrHospital = mtrHospital;
		this.mtrStation = mtrStation;
		this.employeeId = employeeId;
		this.mtrResult = mtrResult;
		this.url = url;
	}

	public String getMtrId() {
		return mtrId;
	}

	public void setMtrId(String mtrId) {
		this.mtrId = mtrId;
	}

	public String getMtrType() {
		return mtrType;
	}

	public void setMtrType(String mtrType) {
		this.mtrType = mtrType;
	}

	public String getMtrDate() {
		return mtrDate;
	}

	public void setMtrDate(String mtrDate) {
		this.mtrDate = mtrDate;
	}

	public String getMtrHospital() {
		return mtrHospital;
	}

	public void setMtrHospital(String mtrHospital) {
		this.mtrHospital = mtrHospital;
	}

	public String getMtrStation() {
		return mtrStation;
	}

	public void setMtrStation(String mtrStation) {
		this.mtrStation = mtrStation;
	}
	
	public String getMtrResult() {
		return mtrResult;
	}

	public void setMtrResult(String mtrResult) {
		this.mtrResult = mtrResult;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
