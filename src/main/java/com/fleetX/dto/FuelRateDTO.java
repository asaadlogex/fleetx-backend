package com.fleetX.dto;

public class FuelRateDTO {
	private Integer fuelRateId;
	private String startDate;
	private String endDate;
	private Double ratePerLitre;
	private String fscId;

	public Integer getFuelRateId() {
		return fuelRateId;
	}

	public void setFuelRateId(Integer fuelRateId) {
		this.fuelRateId = fuelRateId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Double getRatePerLitre() {
		return ratePerLitre;
	}

	public void setRatePerLitre(Double ratePerLitre) {
		this.ratePerLitre = ratePerLitre;
	}

	public String getFscId() {
		return fscId;
	}

	public void setFscId(String fscId) {
		this.fscId = fscId;
	}
}
