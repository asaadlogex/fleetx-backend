package com.fleetX.dto;

public class TractorDTO {
	private String tractorId;
	private String engineType;
	private String engineNumber;
	private String hp;
	private String transmission;
	private String numberPlate;
	private Integer numberOfGears;
	private Integer numberOfFuelTanks;
	private Double fuelCapacity;
	private Double startingOdometer;
	private Double currentOdometer;
	private String odometerUnit;
	private AssetDTO asset;
	private String adminCompanyId;

	public String getAdminCompanyId() {
		return adminCompanyId;
	}

	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}

	public String getTractorId() {
		return tractorId;
	}

	public void setTractorId(String tractorId) {
		this.tractorId = tractorId;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public String getNumberPlate() {
		return numberPlate;
	}

	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public Integer getNumberOfGears() {
		return numberOfGears;
	}

	public void setNumberOfGears(Integer numberOfGears) {
		this.numberOfGears = numberOfGears;
	}

	public Integer getNumberOfFuelTanks() {
		return numberOfFuelTanks;
	}

	public void setNumberOfFuelTanks(Integer numberOfFuelTanks) {
		this.numberOfFuelTanks = numberOfFuelTanks;
	}

	public Double getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(Double fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public Double getStartingOdometer() {
		return startingOdometer;
	}

	public void setStartingOdometer(Double startingOdometer) {
		this.startingOdometer = startingOdometer;
	}

	public Double getCurrentOdometer() {
		return currentOdometer;
	}

	public void setCurrentOdometer(Double currentOdometer) {
		this.currentOdometer = currentOdometer;
	}

	public String getOdometerUnit() {
		return odometerUnit;
	}

	public void setOdometerUnit(String odometerUnit) {
		this.odometerUnit = odometerUnit;
	}

	public AssetDTO getAsset() {
		return asset;
	}

	public void setAsset(AssetDTO asset) {
		this.asset = asset;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	@Override
	public String toString() {
		return "TractorDTO [tractorId=" + tractorId + ", engineType=" + engineType + ", engineNumber=" + engineNumber
				+ ", transmission=" + transmission + ", numberPlate=" + numberPlate + ", numberOfGears=" + numberOfGears
				+ ", numberOfFuelTanks=" + numberOfFuelTanks + ", fuelCapacity=" + fuelCapacity + ", startingOdometer="
				+ startingOdometer + ", currentOdometer=" + currentOdometer + ", odometerUnit=" + odometerUnit
				+ ", asset=" + asset + ", adminCompanyId=" + adminCompanyId + "]";
	}

}
