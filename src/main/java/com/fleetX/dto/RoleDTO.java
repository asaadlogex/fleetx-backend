package com.fleetX.dto;

import java.util.ArrayList;
import java.util.List;

import com.fleetX.entity.dropdown.DPrivilege;

public class RoleDTO {
	private String roleName;
	private List<DPrivilege> privileges=new ArrayList<>();
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<DPrivilege> getPrivileges() {
		return privileges;
	}
	public void setPrivileges(List<DPrivilege> privileges) {
		this.privileges = privileges;
	}	
}
