package com.fleetX.dto;

import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

public class TrailerSummaryDTO {
	private String trailerId;
	private String vin;
	private DLeaseType leaseType;
	private DTrailerType trailerType;
	private DTrailerSize trailerSize;
	private String status;

	public String getTrailerId() {
		return trailerId;
	}

	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public DLeaseType getLeaseType() {
		return leaseType;
	}

	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}

	public DTrailerType getTrailerType() {
		return trailerType;
	}

	public void setTrailerType(DTrailerType trailerType) {
		this.trailerType = trailerType;
	}

	public DTrailerSize getTrailerSize() {
		return trailerSize;
	}

	public void setTrailerSize(DTrailerSize trailerSize) {
		this.trailerSize = trailerSize;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
