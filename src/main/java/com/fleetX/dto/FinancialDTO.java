package com.fleetX.dto;

import java.util.List;

public class FinancialDTO {
	private Integer financialId;
	private List<IncomeDTO> incomes;
	private List<ExpenseDTO> expenses;
	private Double totalIncome;
	private Double totalExpense;
	private Double netIncome;
	private String roadwayBillId;
	
	public Integer getFinancialId() {
		return financialId;
	}
	public void setFinancialId(Integer financialId) {
		this.financialId = financialId;
	}
	public List<IncomeDTO> getIncomes() {
		return incomes;
	}
	public void setIncomes(List<IncomeDTO> incomes) {
		this.incomes = incomes;
	}
	public List<ExpenseDTO> getExpenses() {
		return expenses;
	}
	public void setExpenses(List<ExpenseDTO> expenses) {
		this.expenses = expenses;
	}
	public Double getTotalIncome() {
		return totalIncome;
	}
	public void setTotalIncome(Double totalIncome) {
		this.totalIncome = totalIncome;
	}
	public Double getTotalExpense() {
		return totalExpense;
	}
	public void setTotalExpense(Double totalExpense) {
		this.totalExpense = totalExpense;
	}
	public Double getNetIncome() {
		return netIncome;
	}
	public void setNetIncome(Double netIncome) {
		this.netIncome = netIncome;
	}
	public String getRoadwayBillId() {
		return roadwayBillId;
	}
	public void setRoadwayBillId(String roadwayBillId) {
		this.roadwayBillId = roadwayBillId;
	}
}
