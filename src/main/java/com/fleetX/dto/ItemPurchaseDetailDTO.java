package com.fleetX.dto;

public class ItemPurchaseDetailDTO {
	private Integer itemPurchaseDetailId;
	private ItemDTO item;
	private Integer qtyOrderedTotal;
	private Integer qtyReceived;
	private Integer qtyReceivedTotal;
	private Integer qtyRemaining;
	private Double rate;
	private Double amount;

	public Integer getItemPurchaseDetailId() {
		return itemPurchaseDetailId;
	}

	public void setItemPurchaseDetailId(Integer itemPurchaseDetailId) {
		this.itemPurchaseDetailId = itemPurchaseDetailId;
	}

	public ItemDTO getItem() {
		return item;
	}

	public void setItem(ItemDTO item) {
		this.item = item;
	}

	public Integer getQtyReceived() {
		return qtyReceived;
	}

	public void setQtyReceived(Integer qtyReceived) {
		this.qtyReceived = qtyReceived;
	}

	public Integer getQtyRemaining() {
		return qtyRemaining;
	}

	public void setQtyRemaining(Integer qtyRemaining) {
		this.qtyRemaining = qtyRemaining;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getQtyOrderedTotal() {
		return qtyOrderedTotal;
	}

	public void setQtyOrderedTotal(Integer qtyOrderedTotal) {
		this.qtyOrderedTotal = qtyOrderedTotal;
	}

	public Integer getQtyReceivedTotal() {
		return qtyReceivedTotal;
	}

	public void setQtyReceivedTotal(Integer qtyReceivedTotal) {
		this.qtyReceivedTotal = qtyReceivedTotal;
	}

}
