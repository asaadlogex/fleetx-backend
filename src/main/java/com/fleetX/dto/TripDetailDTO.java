package com.fleetX.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fleetX.entity.dropdown.DJobStatus;

public class TripDetailDTO {
	private String jobId;
	private Map<String, Double> expenses = new HashMap<String, Double>();
	private Map<String, Double> incomes = new HashMap<String, Double>();
	private String jobOpenDate;
	private String jobCloseDate;
	private Double totalIncome;
	private Double totalExpense;
	private Double netIncome;
	private DJobStatus jobStatus;
	private Double durationInHrs;
	private Double totalKm;
	private Double odometerKm;
	private Double tmsKm;
	private Double trackerKm;
	private Double totalFuelInLtr;
	private Double totalFuelCost;
	private Double avgFuelCostPerKm;
	private Double fuelAvg;
	private Double trackerFuelAvg;
	private List<RoadwayBillSummaryDTO> roadwayBills = new ArrayList<>();
	private List<StopProgressDTO> stopProgresses = new ArrayList<>();
	private List<JobFuelDetailDTO> fuelDetails = new ArrayList<>();

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Double getDurationInHrs() {
		return durationInHrs;
	}

	public void setDurationInHrs(Double duration) {
		this.durationInHrs = duration;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public Double getFuelAvg() {
		return fuelAvg;
	}

	public void setFuelAvg(Double fuelAvg) {
		this.fuelAvg = fuelAvg;
	}

	public Map<String, Double> getExpenses() {
		return expenses;
	}

	public void setExpenses(Map<String, Double> expenses) {
		this.expenses = expenses;
	}

	public Map<String, Double> getIncomes() {
		return incomes;
	}

	public void setIncomes(Map<String, Double> incomes) {
		this.incomes = incomes;
	}

	public Double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(Double totalIncome) {
		this.totalIncome = totalIncome;
	}

	public Double getTotalExpense() {
		return totalExpense;
	}

	public void setTotalExpense(Double totalExpense) {
		this.totalExpense = totalExpense;
	}

	public Double getNetIncome() {
		return netIncome;
	}

	public void setNetIncome(Double netIncome) {
		this.netIncome = netIncome;
	}

	public DJobStatus getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(DJobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

	public List<RoadwayBillSummaryDTO> getRoadwayBills() {
		return roadwayBills;
	}

	public void setRoadwayBills(List<RoadwayBillSummaryDTO> roadwayBills) {
		this.roadwayBills = roadwayBills;
	}

	public List<StopProgressDTO> getStopProgresses() {
		return stopProgresses;
	}

	public void setStopProgresses(List<StopProgressDTO> stopProgresses) {
		this.stopProgresses = stopProgresses;
	}

	public Double getTotalFuelCost() {
		return totalFuelCost;
	}

	public void setTotalFuelCost(Double totalFuelCost) {
		this.totalFuelCost = totalFuelCost;
	}

	public Double getAvgFuelCostPerKm() {
		return avgFuelCostPerKm;
	}

	public void setAvgFuelCostPerKm(Double avgFuelCostPerKm) {
		this.avgFuelCostPerKm = avgFuelCostPerKm;
	}

	public List<JobFuelDetailDTO> getFuelDetails() {
		return fuelDetails;
	}

	public void setFuelDetails(List<JobFuelDetailDTO> fuelDetails) {
		this.fuelDetails = fuelDetails;
	}

	public String getJobOpenDate() {
		return jobOpenDate;
	}

	public void setJobOpenDate(String jobOpenDate) {
		this.jobOpenDate = jobOpenDate;
	}

	public String getJobCloseDate() {
		return jobCloseDate;
	}

	public void setJobCloseDate(String jobCloseDate) {
		this.jobCloseDate = jobCloseDate;
	}

	public Double getOdometerKm() {
		return odometerKm;
	}

	public void setOdometerKm(Double odometerKm) {
		this.odometerKm = odometerKm;
	}

	public Double getTmsKm() {
		return tmsKm;
	}

	public void setTmsKm(Double tmsKm) {
		this.tmsKm = tmsKm;
	}

	public Double getTrackerKm() {
		return trackerKm;
	}

	public void setTrackerKm(Double trackerKm) {
		this.trackerKm = trackerKm;
	}

	public Double getTotalFuelInLtr() {
		return totalFuelInLtr;
	}

	public void setTotalFuelInLtr(Double totalFuelInLtr) {
		this.totalFuelInLtr = totalFuelInLtr;
	}

	public Double getTrackerFuelAvg() {
		return trackerFuelAvg;
	}

	public void setTrackerFuelAvg(Double trackerFuelAvg) {
		this.trackerFuelAvg = trackerFuelAvg;
	}
	
}
