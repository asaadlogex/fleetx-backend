package com.fleetX.dto;

import com.fleetX.entity.dropdown.DCustomerType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DPaymentMode;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.dropdown.DVehicleOwnership;

public class RoadwayBillSummaryDTO {
	private String rwbId;
	private String orderNumber;
	private String loadNumber;
	private DVehicleOwnership vehicleOwnership;
	private String vehicleOwner;
	private DCustomerType customerType;
	private DRWBStatus rwbStatus;
	private DInvoiceStatus invoiceStatus;
	private DState taxState;
	private String tractorNumber;
	private String trailerNumber;
	private String containerNumber;
	private String trackerNumber;
	private String driver1;
	private String driver2;
	private String trailerTypeAndSize;
	private Double trackerKm;
	private Boolean rateStatus;
	private String rwbDate;
	private DPaymentMode paymentMode;
	private String customer;
	private String broker;
	private RouteDTO route;
	private Double reimbursementExpense;
	private Double totalIncome;
	private Double totalExpense;
	private Double durationInHrs;

	public String getRwbId() {
		return rwbId;
	}

	public void setRwbId(String rwbId) {
		this.rwbId = rwbId;
	}

	public String getTractorNumber() {
		return tractorNumber;
	}

	public void setTractorNumber(String tractorNumber) {
		this.tractorNumber = tractorNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getLoadNumber() {
		return loadNumber;
	}

	public void setLoadNumber(String loadNumber) {
		this.loadNumber = loadNumber;
	}

	public DCustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(DCustomerType customerType) {
		this.customerType = customerType;
	}

	public DRWBStatus getRwbStatus() {
		return rwbStatus;
	}

	public void setRwbStatus(DRWBStatus rwbStatus) {
		this.rwbStatus = rwbStatus;
	}

	public Boolean getRateStatus() {
		return rateStatus;
	}

	public void setRateStatus(Boolean rateStatus) {
		this.rateStatus = rateStatus;
	}

	public String getRwbDate() {
		return rwbDate;
	}

	public void setRwbDate(String rwbDate) {
		this.rwbDate = rwbDate;
	}

	public DPaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(DPaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public RouteDTO getRoute() {
		return route;
	}

	public void setRoute(RouteDTO route) {
		this.route = route;
	}

	public DVehicleOwnership getVehicleOwnership() {
		return vehicleOwnership;
	}

	public void setVehicleOwnership(DVehicleOwnership vehicleOwnership) {
		this.vehicleOwnership = vehicleOwnership;
	}

	public String getVehicleOwner() {
		return vehicleOwner;
	}

	public void setVehicleOwner(String vehicleOwner) {
		this.vehicleOwner = vehicleOwner;
	}

	public String getTrailerTypeAndSize() {
		return trailerTypeAndSize;
	}

	public void setTrailerTypeAndSize(String trailerTypeAndSize) {
		this.trailerTypeAndSize = trailerTypeAndSize;
	}

	public Double getReimbursementExpense() {
		return reimbursementExpense;
	}

	public void setReimbursementExpense(Double reimbursementExpense) {
		this.reimbursementExpense = reimbursementExpense;
	}

	public DInvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(DInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public DState getTaxState() {
		return taxState;
	}

	public void setTaxState(DState taxState) {
		this.taxState = taxState;
	}

	public Double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(Double totalIncome) {
		this.totalIncome = totalIncome;
	}

	public String getTrailerNumber() {
		return trailerNumber;
	}

	public void setTrailerNumber(String trailerNumber) {
		this.trailerNumber = trailerNumber;
	}

	public String getContainerNumber() {
		return containerNumber;
	}

	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}

	public String getTrackerNumber() {
		return trackerNumber;
	}

	public void setTrackerNumber(String trackerNumber) {
		this.trackerNumber = trackerNumber;
	}

	public String getDriver1() {
		return driver1;
	}

	public void setDriver1(String driver1) {
		this.driver1 = driver1;
	}

	public String getDriver2() {
		return driver2;
	}

	public void setDriver2(String driver2) {
		this.driver2 = driver2;
	}

	public Double getDurationInHrs() {
		return durationInHrs;
	}

	public void setDurationInHrs(Double durationInHrs) {
		this.durationInHrs = durationInHrs;
	}

	public Double getTrackerKm() {
		return trackerKm;
	}

	public void setTrackerKm(Double trackerKm) {
		this.trackerKm = trackerKm;
	}

	public Double getTotalExpense() {
		return totalExpense;
	}

	public void setTotalExpense(Double totalExpense) {
		this.totalExpense = totalExpense;
	}

}
