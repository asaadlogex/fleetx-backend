package com.fleetX.dto;

import com.fleetX.entity.dropdown.DLeaseType;

public class TyreDTO {
	private String tyreId;
	private String tyreSerialNumber;
	private String make;
	private Double startingKm;
	private Double totalKm;
	private String status;
	private DLeaseType leaseType;
	private String supplierId;
	private String adminCompanyId;
	
	public DLeaseType getLeaseType() {
		return leaseType;
	}
	public void setLeaseType(DLeaseType leaseType) {
		this.leaseType = leaseType;
	}
	public String getAdminCompanyId() {
		return adminCompanyId;
	}
	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getTyreId() {
		return tyreId;
	}
	public void setTyreId(String tyreId) {
		this.tyreId = tyreId;
	}
	public String getTyreSerialNumber() {
		return tyreSerialNumber;
	}
	public void setTyreSerialNumber(String tyreSerialNumber) {
		this.tyreSerialNumber = tyreSerialNumber;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public Double getStartingKm() {
		return startingKm;
	}
	public void setStartingKm(Double startingKm) {
		this.startingKm = startingKm;
	}
	public Double getTotalKm() {
		return totalKm;
	}
	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TyreDTO [tyreId=" + tyreId + ", tyreSerialNumber=" + tyreSerialNumber + ", make=" + make
				+ ", startingKm=" + startingKm + ", totalKm=" + totalKm + ", status=" + status + ", supplierBusinessId="
				+ supplierId + "]";
	}
	
}
