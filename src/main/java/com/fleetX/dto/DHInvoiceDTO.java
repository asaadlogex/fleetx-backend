package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DState;

public class DHInvoiceDTO {
	private Integer invoiceId;
	private String dhInvoiceId;
	private String invoiceDate;
	private SenderDTO sender;
	private RecipientDTO recipient;
	private List<DHLineItemDTO> dhLineItems;
	private Double grossAmount;
	private DState taxState;
	private Double tax;
	private String taxPercent;
	private Double totalAmount;
	private String amountInWords;
	private Double amountPaid;
	private Double remainingAmount;
	private Integer month;
	private Integer year;
	private DPaymentStatus paymentStatus;
	private DInvoiceType invoiceType;
	private String note;

	public String getDhInvoiceId() {
		return dhInvoiceId;
	}

	public void setDhInvoiceId(String dhInvoiceId) {
		this.dhInvoiceId = dhInvoiceId;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public SenderDTO getSender() {
		return sender;
	}

	public void setSender(SenderDTO sender) {
		this.sender = sender;
	}

	public RecipientDTO getRecipient() {
		return recipient;
	}

	public void setRecipient(RecipientDTO recipient) {
		this.recipient = recipient;
	}

	public List<DHLineItemDTO> getDhLineItems() {
		return dhLineItems;
	}

	public void setDhLineItems(List<DHLineItemDTO> dhLineItems) {
		this.dhLineItems = dhLineItems;
	}

	public DState getTaxState() {
		return taxState;
	}

	public void setTaxState(DState taxState) {
		this.taxState = taxState;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public String getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(String taxPercent) {
		this.taxPercent = taxPercent;
	}

	public Double getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(Double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAmountInWords() {
		return amountInWords;
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public DPaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(DPaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public DInvoiceType getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(DInvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
}
