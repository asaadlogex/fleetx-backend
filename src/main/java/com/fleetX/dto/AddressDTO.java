package com.fleetX.dto;

import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DState;

public class AddressDTO {
	private Integer addressId;
	private String country;
	private DCity city;
	private DState state;
	private String street1;
	private String street2;
	private String zipCode;
	private Double longitude;
	private Double latitude;

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public DCity getCity() {
		return city;
	}

	public void setCity(DCity city) {
		this.city = city;
	}

	public DState getState() {
		return state;
	}

	public void setState(DState state) {
		this.state = state;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "AddressDTO [country=" + country + ", city=" + city + ", state=" + state + ", street1=" + street1
				+ ", street2=" + street2 + ", zipCode=" + zipCode + ", longitude=" + longitude + ", latitude="
				+ latitude + "]";
	}
}
