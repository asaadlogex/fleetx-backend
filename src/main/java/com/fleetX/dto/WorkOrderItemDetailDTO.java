package com.fleetX.dto;

public class WorkOrderItemDetailDTO {
	private Integer workOrderItemDetailId;
	private ItemDTO item;
	private Integer qtyUsed;
	private Double rate;
	private Double amount;

	public Integer getWorkOrderItemDetailId() {
		return workOrderItemDetailId;
	}

	public void setWorkOrderItemDetailId(Integer workOrderItemDetailId) {
		this.workOrderItemDetailId = workOrderItemDetailId;
	}

	public ItemDTO getItem() {
		return item;
	}

	public void setItem(ItemDTO item) {
		this.item = item;
	}

	public Integer getQtyUsed() {
		return qtyUsed;
	}

	public void setQtyUsed(Integer qtyUsed) {
		this.qtyUsed = qtyUsed;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
