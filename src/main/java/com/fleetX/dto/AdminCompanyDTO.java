package com.fleetX.dto;

import java.util.List;

import com.fleetX.entity.dropdown.DBusinessType;
import com.fleetX.entity.dropdown.DFormation;

public class AdminCompanyDTO {
	private String companyId;
	private String companyName;
	private String companyCode;
	private DFormation formation;
	private String ntnNumber;
	private String strnNumber;
	private String status;
	private DBusinessType businessType;
	private String pra;
	private String kra;
	private String bra;
	private String sra;
	private List<AddressDTO> addresses;
	private List<ContactDTO> contacts;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public DFormation getFormation() {
		return formation;
	}

	public void setFormation(DFormation formation) {
		this.formation = formation;
	}

	public String getNtnNumber() {
		return ntnNumber;
	}

	public void setNtnNumber(String ntnNumber) {
		this.ntnNumber = ntnNumber;
	}

	public String getStrnNumber() {
		return strnNumber;
	}

	public void setStrnNumber(String strnNumber) {
		this.strnNumber = strnNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DBusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(DBusinessType businessType) {
		this.businessType = businessType;
	}

	public String getPra() {
		return pra;
	}

	public void setPra(String pra) {
		this.pra = pra;
	}

	public String getKra() {
		return kra;
	}

	public void setKra(String kra) {
		this.kra = kra;
	}

	public String getBra() {
		return bra;
	}

	public void setBra(String bra) {
		this.bra = bra;
	}

	public List<AddressDTO> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressDTO> addresses) {
		this.addresses = addresses;
	}

	public List<ContactDTO> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContactDTO> contacts) {
		this.contacts = contacts;
	}

	public String getSra() {
		return sra;
	}

	public void setSra(String sra) {
		this.sra = sra;
	}
}
