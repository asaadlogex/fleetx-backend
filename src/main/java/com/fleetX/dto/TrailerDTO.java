package com.fleetX.dto;

import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;

public class TrailerDTO {
	private String trailerId;
	private DTrailerType trailerType;
	private DTrailerSize trailerSize;
	private AssetDTO asset;
	private String adminCompanyId;
	
	public String getAdminCompanyId() {
		return adminCompanyId;
	}
	public void setAdminCompanyId(String adminCompanyId) {
		this.adminCompanyId = adminCompanyId;
	}	
	public String getTrailerId() {
		return trailerId;
	}
	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}
	public DTrailerType getTrailerType() {
		return trailerType;
	}
	public void setTrailerType(DTrailerType trailerType) {
		this.trailerType = trailerType;
	}
	public DTrailerSize getTrailerSize() {
		return trailerSize;
	}
	public void setTrailerSize(DTrailerSize trailerSize) {
		this.trailerSize = trailerSize;
	}
	public AssetDTO getAsset() {
		return asset;
	}
	public void setAsset(AssetDTO asset) {
		this.asset = asset;
	}
}
