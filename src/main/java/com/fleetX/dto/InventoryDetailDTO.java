package com.fleetX.dto;

public class InventoryDetailDTO {
	private Integer inventoryId;
	private Integer startingQty;
	private Double startingCost;
	private Integer criticalQty;
	private Integer expectedQty;
	private Integer qtyOnHand;
	private Integer qtyCommitted;
	private Double avgCost;

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Integer getStartingQty() {
		return startingQty;
	}

	public void setStartingQty(Integer startingQty) {
		this.startingQty = startingQty;
	}

	public Double getStartingCost() {
		return startingCost;
	}

	public void setStartingCost(Double startingCost) {
		this.startingCost = startingCost;
	}

	public Integer getCriticalQty() {
		return criticalQty;
	}

	public void setCriticalQty(Integer criticalQty) {
		this.criticalQty = criticalQty;
	}

	public Integer getExpectedQty() {
		return expectedQty;
	}

	public void setExpectedQty(Integer expectedQty) {
		this.expectedQty = expectedQty;
	}

	public Integer getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(Integer qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public Integer getQtyCommitted() {
		return qtyCommitted;
	}

	public void setQtyCommitted(Integer qtyCommitted) {
		this.qtyCommitted = qtyCommitted;
	}

	public Double getAvgCost() {
		return avgCost;
	}

	public void setAvgCost(Double avgCost) {
		this.avgCost = avgCost;
	}

}
