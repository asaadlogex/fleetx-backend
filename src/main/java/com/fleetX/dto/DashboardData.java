package com.fleetX.dto;

import java.util.Map;

public class DashboardData {
	private Map<String, Long> jobs;
	private Map<String, Long> roadwayBills;
	private Map<String, Map<String, Long>> assets;
	private Map<String, Long> payments;
	private Map<String, Map<String, Long>> invoices;
	private Map<String, Map<String, Double>> financials;

	public Map<String, Long> getJobs() {
		return jobs;
	}

	public void setJobs(Map<String, Long> jobs) {
		this.jobs = jobs;
	}

	public Map<String, Long> getRoadwayBills() {
		return roadwayBills;
	}

	public void setRoadwayBills(Map<String, Long> roadwayBills) {
		this.roadwayBills = roadwayBills;
	}

	public Map<String, Map<String, Long>> getAssets() {
		return assets;
	}

	public void setAssets(Map<String, Map<String, Long>> assets) {
		this.assets = assets;
	}

	public Map<String, Map<String, Long>> getInvoices() {
		return invoices;
	}

	public void setInvoices(Map<String, Map<String, Long>> invoices) {
		this.invoices = invoices;
	}

	public Map<String, Map<String, Double>> getFinancials() {
		return financials;
	}

	public void setFinancials(Map<String, Map<String, Double>> financials) {
		this.financials = financials;
	}

	public Map<String, Long> getPayments() {
		return payments;
	}

	public void setPayments(Map<String, Long> payments) {
		this.payments = payments;
	}

}
