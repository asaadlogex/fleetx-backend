package com.fleetX.dto;

import com.fleetX.entity.dropdown.DFuelTankType;

public class JobFuelDetailDTO {
	private Integer jobFuelDetailId;
	private CompanySummaryDTO supplier;
	private TractorSummaryDTO tractor;
	private TrailerSummaryDTO trailer;
	private ContainerDTO container;
	private EmployeeSummaryDTO driver1;
	private EmployeeSummaryDTO driver2;
	private String fuelSlipDate;
	private String fuelSlipNumber;
	private FuelCardDTO fuelCard;
	private Double currentKm;
	private Double fuelInLitre;
	private Double ratePerLitre;
	private Double totalAmount;
	private DFuelTankType fuelTankType;
	private String jobId;

	public Integer getJobFuelDetailId() {
		return jobFuelDetailId;
	}

	public void setJobFuelDetailId(Integer jobFuelDetailId) {
		this.jobFuelDetailId = jobFuelDetailId;
	}

	public String getFuelSlipDate() {
		return fuelSlipDate;
	}

	public void setFuelSlipDate(String fuelSlipDate) {
		this.fuelSlipDate = fuelSlipDate;
	}

	public ContainerDTO getContainer() {
		return container;
	}

	public void setContainer(ContainerDTO container) {
		this.container = container;
	}

	public String getFuelSlipNumber() {
		return fuelSlipNumber;
	}

	public void setFuelSlipNumber(String fuelSlipNumber) {
		this.fuelSlipNumber = fuelSlipNumber;
	}

	public FuelCardDTO getFuelCard() {
		return fuelCard;
	}

	public void setFuelCard(FuelCardDTO fuelCard) {
		this.fuelCard = fuelCard;
	}

	public Double getCurrentKm() {
		return currentKm;
	}

	public CompanySummaryDTO getSupplier() {
		return supplier;
	}

	public void setSupplier(CompanySummaryDTO supplier) {
		this.supplier = supplier;
	}

	public TractorSummaryDTO getTractor() {
		return tractor;
	}

	public void setTractor(TractorSummaryDTO tractor) {
		this.tractor = tractor;
	}

	public TrailerSummaryDTO getTrailer() {
		return trailer;
	}

	public void setTrailer(TrailerSummaryDTO trailer) {
		this.trailer = trailer;
	}

	public EmployeeSummaryDTO getDriver1() {
		return driver1;
	}

	public void setDriver1(EmployeeSummaryDTO driver1) {
		this.driver1 = driver1;
	}

	public EmployeeSummaryDTO getDriver2() {
		return driver2;
	}

	public void setDriver2(EmployeeSummaryDTO driver2) {
		this.driver2 = driver2;
	}

	public void setCurrentKm(Double currentKm) {
		this.currentKm = currentKm;
	}

	public Double getFuelInLitre() {
		return fuelInLitre;
	}

	public void setFuelInLitre(Double fuelInLitre) {
		this.fuelInLitre = fuelInLitre;
	}

	public Double getRatePerLitre() {
		return ratePerLitre;
	}

	public void setRatePerLitre(Double ratePerLitre) {
		this.ratePerLitre = ratePerLitre;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public DFuelTankType getFuelTankType() {
		return fuelTankType;
	}

	public void setFuelTankType(DFuelTankType fuelTankType) {
		this.fuelTankType = fuelTankType;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
}
