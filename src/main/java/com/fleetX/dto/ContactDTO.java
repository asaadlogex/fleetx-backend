package com.fleetX.dto;

import com.fleetX.entity.dropdown.DChannel;

public class ContactDTO {
	private Integer contactId;
	private DChannel channel;
	private String data;
	
	
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public DChannel getChannel() {
		return channel;
	}
	public void setChannel(DChannel channel) {
		this.channel = channel;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "ContactDTO [channel=" + channel + ", data=" + data +"]";
	}
}
