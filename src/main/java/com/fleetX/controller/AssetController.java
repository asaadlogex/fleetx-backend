package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.AssetCombinationDTO;
import com.fleetX.dto.ContainerDTO;
import com.fleetX.dto.TrackerDTO;
import com.fleetX.dto.TractorDTO;
import com.fleetX.dto.TractorSummaryDTO;
import com.fleetX.dto.TrailerDTO;
import com.fleetX.dto.TrailerSummaryDTO;
import com.fleetX.dto.TyreDTO;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.mapper.AssetMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IAssetService;
import com.fleetX.service.IFileDataService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "assets"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class AssetController {

	@Autowired
	IAssetService assetService;
	@Autowired
	IFileDataService fileDataService;
	@Autowired
	SystemUtil systemUtil;

	@Autowired
	AssetMapper assetMapper;

	// ASSET COMBINATION
	@CrossOrigin
	@PostMapping("/assetCombinations")
	public ResponseEntity<Object> addAssetCombination(@RequestBody AssetCombinationDTO assetCombinationDto) {
		AssetCombination assetCombination = assetMapper.mapToAssetCombination(assetCombinationDto);
		String acId = assetService.addAssetCombination(assetCombination).getAssetCombinationId();
		assetCombinationDto.setAssetCombinationId(acId);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Asset Combination added", assetCombinationDto));
	}

	@CrossOrigin
	@GetMapping("/assetCombinations/{id}")
	public ResponseEntity<Object> findAssetCombinationById(@PathVariable("id") String assetCombinationId)
			throws ParseException {
		AssetCombination assetCombination = assetService.findAssetCombinationById(assetCombinationId);
		AssetCombinationDTO assetCombinationDto = assetMapper.mapToAssetCombinationDTO(assetCombination);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Asset Combination fetched", assetCombinationDto));
	}

	@CrossOrigin
	@GetMapping("/assetCombinations")
	public ResponseEntity<Object> findAllAssetCombination(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<AssetCombination> assetCombinations = assetService.findAllAssetCombination();
		int total = assetCombinations.size();
		List<AssetCombinationDTO> assetCombinationDtos = new ArrayList<>();
		assetCombinations = systemUtil.paginate(assetCombinations, page, size);
		for (AssetCombination assetCombination : assetCombinations) {
			assetCombinationDtos.add(assetMapper.mapToAssetCombinationDTO(assetCombination));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				assetCombinationDtos.size() + " asset combinations fetched", assetCombinationDtos, total));
	}

	@CrossOrigin
	@PutMapping("/assetCombinations")
	public ResponseEntity<Object> updateAssetCombination(@RequestBody AssetCombinationDTO assetCombinationDto) {
		AssetCombination assetCombination = assetMapper.mapToAssetCombination(assetCombinationDto);
		assetService.updateAssetCombination(assetCombination);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Asset Combination updated", assetCombinationDto));
	}

	@CrossOrigin
	@DeleteMapping("/assetCombinations/{id}")
	public ResponseEntity<Object> deleteAssetCombinationById(@PathVariable("id") String assetCombinationId) {
		assetService.deleteAssetCombinationById(assetCombinationId);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Asset Combination deleted", assetCombinationId));
	}

	@CrossOrigin
	@GetMapping("/assetCombinations/filter")
	public ResponseEntity<Object> filterAssetCombination(
			@RequestParam(value = "javail", required = false) String jobAvailability,
			@RequestParam(value = "avail", required = false) String availability) throws ParseException {
		List<AssetCombination> assetCombinations = assetService.filterAssetCombination(availability, jobAvailability);
		List<AssetCombinationDTO> assetCombinationDtos = new ArrayList<>();
		for (AssetCombination assetCombination : assetCombinations) {
			assetCombinationDtos.add(assetMapper.mapToAssetCombinationDTO(assetCombination));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				assetCombinationDtos.size() + " asset combinations fetched", assetCombinationDtos));
	}

	// CONTAINERS
	@CrossOrigin
	@PostMapping("/containers")
	public ResponseEntity<Object> addContainer(@RequestBody ContainerDTO containerDto) throws ParseException {
		Container container = assetMapper.mapToContainer(containerDto);
		Container con = assetService.addContainer(container);
		containerDto.setContainerId(con.getContainerId());
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Container added", containerDto));
	}

	@CrossOrigin
	@GetMapping("/containers")
	public ResponseEntity<Object> findAllContainers(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<Container> con = assetService.findAllContainers();
		int total = con.size();
		con = systemUtil.paginate(con, page, size);
		List<ContainerDTO> containerDtos = new ArrayList<>();
		for (Container container : con) {
			containerDtos.add(assetMapper.mapToContainerDTO(container));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				containerDtos.size() + " containers fetched", containerDtos, total));
	}

	@CrossOrigin
	@GetMapping("/containers/{id}")
	public ResponseEntity<Object> findContainerById(@PathVariable("id") String containerId) throws ParseException {
		Container con = assetService.findContainerById(containerId);
		ContainerDTO containerDto = assetMapper.mapToContainerDTO(con);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Container fetched", containerDto));
	}

	@CrossOrigin
	@GetMapping("/containers/filter")
	public ResponseEntity<Object> filterContainer(@RequestParam(value = "mov", required = false) String movingStatus,
			@RequestParam(value = "rwbId", required = false) String rwbId,
			@RequestParam(value = "avail", required = false) String availability) throws ParseException {
		Container con = new Container();
		con.setAvailability(availability);
		List<Container> containers = assetService.filterContainer(movingStatus, rwbId, availability);
		if (containers == null || containers.size() == 0)
			throw new NoSuchElementException();
		List<ContainerDTO> containersDto = new ArrayList<>();
		for (Container container : containers) {
			containersDto.add(assetMapper.mapToContainerDTO(container));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				containersDto.size() + " containers fetched", containersDto));
	}

	@CrossOrigin
	@PutMapping("/containers")
	public ResponseEntity<Object> updateContainer(@RequestBody ContainerDTO containerDto) throws ParseException {
		Container container = assetMapper.mapToContainer(containerDto);
		Container con = assetService.updateContainer(container);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Container updated", containerDto));
	}

	@CrossOrigin
	@DeleteMapping("/containers/{id}")
	public ResponseEntity<Object> deleteContainer(@PathVariable("id") String containerId) {
		assetService.deleteContainer(containerId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Container deleted", containerId));
	}

	// TRACTORS

	@CrossOrigin
	@PostMapping("/tractors")
	public ResponseEntity<Object> addTractor(@RequestBody TractorDTO tractorDto) throws ParseException {
		System.out.println(tractorDto);
		Tractor tractor = assetMapper.mapToTractor(tractorDto);
		Tractor trac = assetService.addTractor(tractor);
		tractorDto.setTractorId(trac.getTractorId());
		tractorDto.getAsset().setAssetId(trac.getAsset().getAssetId());
		List<TyreDTO> tyresDto = tractorDto.getAsset().getTyres();
		List<Tyre> tyres = trac.getAsset().getTyres();
		for (int i = 0; i < tractorDto.getAsset().getTyres().size(); i++) {
			tyresDto.get(i).setTyreId(tyres.get(i).getTyreId());
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tractor added", tractorDto));
	}

	@CrossOrigin
	@PostMapping("/tractors/file")
	public ResponseEntity<Object> addTractorByFile(@RequestParam(value = "file") MultipartFile tractorFile)
			throws IOException, InvalidFormatException, ParseException {
		List<TractorDTO> tractorDtos = fileDataService.mapToTractorDTO((tractorFile));
		List<Tractor> tractors = new ArrayList<>();
		for (TractorDTO tractorDto : tractorDtos) {
			tractors.add(assetMapper.mapToTractor(tractorDto));
		}
		List<Tractor> tractorsAdded = assetService.addAllTractors(tractors);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), tractorsAdded.size() + " tractors added", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/tractors")
//	@PreAuthorize("hasAuthority('INVOICE_CREATE')")
	public ResponseEntity<Object> findAllTractors(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<Tractor> trac = assetService.findAllTractors();
		int total = trac.size();
		trac = systemUtil.paginate(trac, page, size);
		List<TractorSummaryDTO> tractorsDto = new ArrayList<>();
		for (Tractor tractor : trac) {
			tractorsDto.add(assetMapper.mapToTractorSummaryDTO(tractor));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), tractorsDto.size() + " tractors fetched",
				tractorsDto, total));
	}

	@CrossOrigin
	@GetMapping("/tractors/{id}")
	public ResponseEntity<Object> findTractorById(@PathVariable("id") String tractorId) throws ParseException {
		Tractor trac = assetService.findTractorById(tractorId);
		TractorDTO tractorDto = assetMapper.mapToTractorDTO(trac);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tractor fetched", tractorDto));
	}

	@CrossOrigin
	@GetMapping("/tractors/filter")
	public ResponseEntity<Object> filterTractor(@RequestParam(value = "mov", required = false) String movingStatus,
			@RequestParam(value = "rwbId", required = false) String rwbId,
			@RequestParam(value = "avail", required = false) String availability,
			@RequestParam(value = "vin", required = false) String vinNumber,
			@RequestParam(value = "eng", required = false) String engineNumber,
			@RequestParam(value = "num", required = false) String numberPlate) throws ParseException {
		List<Tractor> tractors = assetService.filterTractor(movingStatus, rwbId, availability, vinNumber, engineNumber, numberPlate);
		if (tractors == null || tractors.size() == 0)
			throw new NoSuchElementException();
		List<TractorSummaryDTO> tractorsDto = new ArrayList<>();
		for (Tractor tractor : tractors) {
			tractorsDto.add(assetMapper.mapToTractorSummaryDTO(tractor));
		}
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), tractorsDto.size() + " tractors fetched", tractorsDto));
	}

	@CrossOrigin
	@PutMapping("/tractors")
	public ResponseEntity<Object> updateTractor(@RequestBody TractorDTO tractorDto) throws ParseException {
		Tractor tractor = assetMapper.mapToTractor(tractorDto);
		Tractor trac = assetService.updateTractor(tractor);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tractor updated", tractorDto));
	}

	@CrossOrigin
	@DeleteMapping("/tractors/{id}")
	public ResponseEntity<Object> deleteTractor(@PathVariable("id") String tractorId) {
		assetService.deleteTractor(tractorId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tractor deleted", tractorId));
	}

	@CrossOrigin
	@GetMapping(value = "/tractors/export")
	public ResponseEntity<Object> exportTractors() throws ParseException, IOException {
		List<Tractor> tractors = assetService.findAllTractors();
		if (tractors == null || tractors.size() == 0)
			throw new NoSuchElementException("Tractors: " + SystemType.NO_SUCH_ELEMENT);
		byte[] file = fileDataService.exportTractorsToExcel(tractors, SystemType.EXCEL_TRACTORS_FILE_NAME);
		if (file == null)
			throw new IOException("Could not generate excel file!");
		return ResponseEntity.ok()
				.contentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(file);
	}

	// TRAILERS

	@CrossOrigin
	@PostMapping("/trailers")
	public ResponseEntity<Object> addTrailer(@RequestBody TrailerDTO trailerDto) throws ParseException {
		Trailer trailer = assetMapper.mapToTrailer(trailerDto);
		Trailer trail = assetService.addTrailer(trailer);
		trailerDto.setTrailerId(trail.getTrailerId());
		trailerDto.getAsset().setAssetId(trail.getAsset().getAssetId());
		List<TyreDTO> tyresDto = trailerDto.getAsset().getTyres();
		List<Tyre> tyres = trail.getAsset().getTyres();
		for (int i = 0; i < trailerDto.getAsset().getTyres().size(); i++) {
			tyresDto.get(i).setTyreId(tyres.get(i).getTyreId());
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Trailer added", trailerDto));
	}

	@CrossOrigin
	@PostMapping("/trailers/file")
	public ResponseEntity<Object> addTrailerByFile(@RequestParam(value = "file") MultipartFile trailerFile)
			throws IOException, InvalidFormatException, ParseException {
		List<TrailerDTO> trailerDtos = fileDataService.mapToTrailerDTO((trailerFile));
		List<Trailer> trailers = new ArrayList<>();
		for (TrailerDTO trailerDto : trailerDtos) {
			trailers.add(assetMapper.mapToTrailer(trailerDto));
		}
		List<Trailer> trailersAdded = assetService.addAllTrailers(trailers);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), trailersAdded.size() + " trailers added", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/trailers")
	public ResponseEntity<Object> findAllTrailers(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<Trailer> trail = assetService.findAllTrailers();
		int total = trail.size();
		trail = systemUtil.paginate(trail, page, size);
		List<TrailerSummaryDTO> trailersDto = new ArrayList<>();
		for (Trailer trailer : trail) {
			trailersDto.add(assetMapper.mapToTrailerSummaryDTO(trailer));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), trailersDto.size() + " trailers fetched",
				trailersDto, total));
	}

	@CrossOrigin
	@GetMapping("/trailers/{id}")
	public ResponseEntity<Object> findTrailerById(@PathVariable("id") String trailerId) throws ParseException {
		Trailer trail = assetService.findTrailerById(trailerId);
		TrailerDTO trailerDto = assetMapper.mapToTrailerDTO(trail);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Trailer fetched", trailerDto));
	}

	@CrossOrigin
	@GetMapping("/trailers/filter")
	public ResponseEntity<Object> filterTrailer(@RequestParam(value = "mov", required = false) String movingStatus,
			@RequestParam(value = "rwbId", required = false) String rwbId,
			@RequestParam(value = "avail", required = false) String availability,
			@RequestParam(value = "vin", required = false) String vinNumber) throws ParseException {
		List<Trailer> trailers = assetService.filterTrailer(movingStatus, rwbId, availability, vinNumber);
		if (trailers == null || trailers.size() == 0)
			throw new NoSuchElementException();
		List<TrailerSummaryDTO> trailersDto = new ArrayList<>();
		for (Trailer trailer : trailers) {
			trailersDto.add(assetMapper.mapToTrailerSummaryDTO(trailer));
		}
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), trailersDto.size() + " trailers fetched", trailersDto));
	}

	@CrossOrigin
	@PutMapping("/trailers")
	public ResponseEntity<Object> updateTrailer(@RequestBody TrailerDTO trailerDto) throws ParseException {
		Trailer trailer = assetMapper.mapToTrailer(trailerDto);
		Trailer trail = assetService.updateTrailer(trailer);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Trailer updated", trailerDto));
	}

	@CrossOrigin
	@DeleteMapping("/trailers/{id}")
	public ResponseEntity<Object> deleteTrailer(@PathVariable("id") String trailerId) {
		assetService.deleteTrailer(trailerId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Trailer deleted", trailerId));
	}

	@CrossOrigin
	@GetMapping(value = "/trailers/export")
	public ResponseEntity<Object> exportTrailers() throws ParseException, IOException {
		List<Trailer> trailers = assetService.findAllTrailers();
		if (trailers == null || trailers.size() == 0)
			throw new NoSuchElementException("Trailers: " + SystemType.NO_SUCH_ELEMENT);
		byte[] file = fileDataService.exportTrailersToExcel(trailers, SystemType.EXCEL_TRAILERS_FILE_NAME);
		if (file == null)
			throw new IOException("Could not generate excel file!");
		return ResponseEntity.ok()
				.contentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(file);
	}

	// TRACKERS

	@CrossOrigin
	@PostMapping("/trackers")
	public ResponseEntity<Object> addTracker(@RequestBody TrackerDTO trackerDto) throws ParseException {
		Tracker tracker = assetMapper.mapToTracker(trackerDto);
		Tracker track = assetService.addTracker(tracker);
		trackerDto.setTrackerId(track.getTrackerId());
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tracker added", trackerDto));
	}

	@CrossOrigin
	@GetMapping("/trackers")
	public ResponseEntity<Object> findAllTrackers(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<Tracker> track = assetService.findAllTrackers();
		int total = track.size();
		track = systemUtil.paginate(track, page, size);
		List<TrackerDTO> trackerDtos = new ArrayList<>();
		for (Tracker tracker : track) {
			trackerDtos.add(assetMapper.mapToTrackerDTO(tracker));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), trackerDtos.size() + " trackers fetched",
				trackerDtos, total));
	}

	@CrossOrigin
	@GetMapping("/trackers/{id}")
	public ResponseEntity<Object> findTrackerById(@PathVariable("id") String trackerId) throws ParseException {
		Tracker track = assetService.findTrackerById(trackerId);
		TrackerDTO trackerDto = assetMapper.mapToTrackerDTO(track);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tracker fetched", trackerDto));
	}

	@CrossOrigin
	@GetMapping("/trackers/filter")
	public ResponseEntity<Object> filterTracker(@RequestParam(value = "mov", required = false) String movingStatus,
			@RequestParam(value = "rwbId", required = false) String rwbId,
			@RequestParam(value = "avail", required = false) String availability) throws ParseException {
		Tracker track = new Tracker();
		track.setAvailability(availability);
		List<Tracker> trackers = assetService.filterTracker(movingStatus, rwbId, availability);
		if (trackers == null || trackers.size() == 0)
			throw new NoSuchElementException();
		List<TrackerDTO> trackersDto = new ArrayList<>();
		for (Tracker tracker : trackers) {
			trackersDto.add(assetMapper.mapToTrackerDTO(tracker));
		}
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), trackersDto.size() + " trackers fetched", trackersDto));
	}

	@CrossOrigin
	@PutMapping("/trackers")
	public ResponseEntity<Object> updateTracker(@RequestBody TrackerDTO trackerDto) throws ParseException {
		Tracker tracker = assetMapper.mapToTracker(trackerDto);
		Tracker track = assetService.updateTracker(tracker);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tracker updated", trackerDto));
	}

	@CrossOrigin
	@DeleteMapping("/trackers/{id}")
	public ResponseEntity<Object> deleteTracker(@PathVariable("id") String tracktainerId) {
		assetService.deleteTracker(tracktainerId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tracker deleted", tracktainerId));
	}

	// TYRES

	@CrossOrigin
	@PostMapping("/tyres")
	public ResponseEntity<Object> addTyre(@RequestBody TyreDTO tyreDto) {
		Tyre tyre = assetMapper.mapToTyre(tyreDto);
		Tyre tyr = assetService.addTyre(tyre, null);
		tyreDto.setTyreId(tyr.getTyreId());
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tyre added", tyreDto));
	}

	@CrossOrigin
	@PostMapping("/tyres/file")
	public ResponseEntity<Object> addTyreByFile(@RequestParam(value = "file") MultipartFile trailerFile,
			@RequestParam(value = "tractor") Boolean forTractor)
			throws IOException, InvalidFormatException, ParseException {
		List<Tyre> tyres = fileDataService.mapToTyre(trailerFile, forTractor);
		List<Tyre> tyresAdded = assetService.addAllTyre(tyres);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), tyresAdded.size() + " tyres added", null), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/tyres")
	public ResponseEntity<Object> findAllTyres(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) {
		List<Tyre> tyr = assetService.findAllTyres();
		int total = tyr.size();
		tyr = systemUtil.paginate(tyr, page, size);
		List<TyreDTO> tyreDtos = new ArrayList<>();
		for (Tyre tyre : tyr) {
			tyreDtos.add(assetMapper.mapToTyreDTO(tyre));
		}
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), tyreDtos.size() + " tyres fetched", tyreDtos, total));
	}

	@CrossOrigin
	@GetMapping("/tyres/{id}")
	public ResponseEntity<Object> findTyreById(@PathVariable("id") String tyreId) {
		Tyre tyr = assetService.findTyreById(tyreId);
		TyreDTO tyreDto = assetMapper.mapToTyreDTO(tyr);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tyre fetched", tyreDto));
	}

	@CrossOrigin
	@GetMapping("/tyres/filter")
	public ResponseEntity<Object> filterTyre(@RequestParam(value = "mov", required = false) String movingStatus,
			@RequestParam(value = "avail", required = false) String availability) {
		Tyre tyr = new Tyre();
		tyr.setAvailability(availability);
		List<Tyre> tyres = assetService.filterTyre(movingStatus, availability);
		if (tyres == null || tyres.size() == 0)
			throw new NoSuchElementException();
		List<TyreDTO> tyresDto = new ArrayList<>();
		for (Tyre tyre : tyres) {
			tyresDto.add(assetMapper.mapToTyreDTO(tyre));
		}
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), tyresDto.size() + " tyres fetched", tyresDto));
	}

	@CrossOrigin
	@PutMapping("/tyres")
	public ResponseEntity<Object> updateTyre(@RequestBody TyreDTO tyreDto) {
		Tyre tyre = assetMapper.mapToTyre(tyreDto);
		Tyre tyr = assetService.updateTyre(tyre);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tyre updated", tyreDto));
	}

	@CrossOrigin
	@DeleteMapping("/tyres/{id}")
	public ResponseEntity<Object> deleteTyre(@PathVariable("id") String tyreId) {
		assetService.deleteTyre(tyreId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Tyre deleted", tyreId));
	}
}
