package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.EmployeeDTO;
import com.fleetX.dto.EmployeeSummaryDTO;
import com.fleetX.dto.HeavyVehicleExperienceDTO;
import com.fleetX.dto.MedicalTestReportDTO;
import com.fleetX.dto.ReferenceDTO;
import com.fleetX.dto.TrainingProfileDTO;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;
import com.fleetX.mapper.EmployeeMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IDropDownService;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IFTPService;
import com.fleetX.service.IFileDataService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "employees"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class EmployeeController {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	EmployeeMapper employeeMapper;
	@Autowired
	IFTPService ftpService;
	@Autowired
	IDropDownService dropDownService;
	@Autowired
	IFileDataService fileDataService;

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> addEmployee(@RequestBody EmployeeDTO employeeDto) throws Exception {
		Employee employee = employeeMapper.mapToEmployee(employeeDto);
		employee.setEmployeeId(null);
		String employeeId = employeeService.addEmployee(employee).getEmployeeId();
		employeeDto.setEmployeeId(employeeId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "New Employee Added", employeeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/file")
	public ResponseEntity<Object> addEmployeeByFile(@RequestParam(value = "file") MultipartFile employeeFile)
			throws Exception {
		List<EmployeeDTO> employeeDtos = fileDataService.mapToEmployeeDTO(employeeFile);
		List<Employee> employees = new ArrayList<>();
		for (EmployeeDTO employeeDto : employeeDtos) {
			employees.add(employeeMapper.mapToEmployee(employeeDto));
		}
		List<Employee> employeesAdded = employeeService.addAllEmployees(employees);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), employeesAdded.size() + " employees added", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> getEmployees(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<Employee> employees = employeeService.findAllEmployee();
		int total = employees.size();
		employees = systemUtil.paginate(employees, page, size);
		List<EmployeeSummaryDTO> employeesDto = new ArrayList<>();
		for (Employee employee : employees) {
			employeesDto.add(employeeMapper.mapToEmployeeSummaryDTO(employee));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), employees.size() + " employees fetched",
				employeesDto, total), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<Object> getEmployeeById(@PathVariable("id") String employeeId) throws Exception {
		Employee employee = employeeService.findEmployeeById(employeeId);
		EmployeeDTO employeeDto = employeeMapper.mapToEmployeeDTO(employee);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Employee fetched", employeeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/filter")
	public ResponseEntity<Object> filterEmployees(@RequestParam(value = "mov", required = false) String movingStatus,
			@RequestParam(value = "rwbId", required = false) String rwbId,
			@RequestParam(value = "avail", required = false) String availability,
			@RequestParam(value = "pos", required = false) String position,
			@RequestParam(value = "fname", required = false) String firstName,
			@RequestParam(value = "lname", required = false) String lastName,
			@RequestParam(value = "enum", required = false) String employeeNumber,
			@RequestParam(value = "cnic", required = false) String cnic,
			@RequestParam(value = "lic", required = false) String licenseNumber) throws Exception {
		List<Employee> employees = employeeService.filterEmployees(position, movingStatus, rwbId, availability,
				firstName, lastName, employeeNumber, cnic, licenseNumber);
		List<EmployeeSummaryDTO> employeesDto = new ArrayList<>();
		for (Employee employee : employees) {
			employeesDto.add(employeeMapper.mapToEmployeeSummaryDTO(employee));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), employeesDto.size() + " employees fetched", employeesDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteEmployee(@PathVariable("id") String employeeId) throws Exception {
		employeeService.deleteEmployee(employeeId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Employee deleted", employeeId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping
	public ResponseEntity<Object> updateEmployee(@RequestBody EmployeeDTO employeeDto) throws Exception {
		Employee employee = employeeMapper.mapToEmployee(employeeDto);
		employeeService.updateEmployee(employee);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Employee updated", employeeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping(value = "/export")
	public ResponseEntity<Object> exportEmployees() throws ParseException, IOException {
		List<Employee> employees = employeeService.findAllEmployee();
		if (employees == null || employees.size() == 0)
			throw new NoSuchElementException("Employees: " + SystemType.NO_SUCH_ELEMENT);
		byte[] file = fileDataService.exportEmployeesToExcel(employees, SystemType.EXCEL_EMPLOYEES_FILE_NAME);
		if (file == null)
			throw new IOException("Could not generate excel file!");
		return ResponseEntity.ok()
				.contentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(file);
	}

	@CrossOrigin
	@PostMapping("/documents/dp")
	public ResponseEntity<Object> uploadProfilePicture(@RequestParam(value = "dp", required = false) MultipartFile dp,
			@RequestParam("employeeId") String employeeId) throws IOException {
		String dpUrl = employeeService.addProfilePicture(employeeId, dp);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Profile picture uploaded", dpUrl),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/documents/tp")
	public ResponseEntity<Object> uploadTrainingProfile(
			@RequestParam(value = "trainingCertificate", required = false) MultipartFile trainingCertificate,
			@RequestParam(value = "attendanceSheet", required = false) MultipartFile attendanceSheet,
			@RequestParam(value = "pictureWithTrainer", required = false) MultipartFile pictureWithTrainer,
			@RequestParam("trainingProfileId") String trainingProfileId,
			@RequestParam("trainingType") String trainingType, @RequestParam("issuedDate") String issuedDate,
			@RequestParam("expiryDate") String expiryDate, @RequestParam("trainerName") String trainerName,
			@RequestParam("trainingStation") String trainingStation, @RequestParam("employeeId") String employeeId)
			throws IOException, ParseException {
		TrainingProfileDTO trainingProfileDto = new TrainingProfileDTO(trainingProfileId, trainingType, issuedDate,
				expiryDate, trainerName, trainingStation, employeeId, null, null, null);
		TrainingProfile trainingProfile = employeeMapper.mapToTrainingProfile(trainingProfileDto);
		String tpId = employeeService
				.addTrainingProfile(trainingProfile, trainingCertificate, attendanceSheet, pictureWithTrainer)
				.getTrainingProfileId();
		trainingProfileDto.setTrainingProfileId(tpId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Files uploaded", trainingProfileDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/documents/mtr")
	public ResponseEntity<Object> uploadMedicalTestReport(
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam("employeeId") String employeeId, @RequestParam("mtrId") String mtrId,
			@RequestParam("mtrType") String mtrType, @RequestParam("mtrDate") String mtrDate,
			@RequestParam("mtrHospital") String mtrHospital, @RequestParam("mtrStation") String mtrStation,
			@RequestParam("mtrResult") String mtrResult) throws IOException, ParseException {
		MedicalTestReportDTO medicalTestReportDTO = new MedicalTestReportDTO(mtrId, mtrType, mtrDate, mtrHospital,
				mtrStation, mtrResult, employeeId, null);
		MedicalTestReport medicalTestReport = employeeMapper.mapToMedicalTestReport(medicalTestReportDTO);
		String mtId = employeeService.addMedicalTestReport(medicalTestReport, file).getMtrId();
		medicalTestReportDTO.setMtrId(mtId);

		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Files uploaded", medicalTestReportDTO),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/documents/hve")
	public ResponseEntity<Object> addHeavyVehicleExperience(
			@RequestBody HeavyVehicleExperienceDTO heavyVehicleExperienceDTO) throws Exception {
		HeavyVehicleExperience heavyVehicleExperience = employeeMapper
				.mapToHeavyVehicleExperience(heavyVehicleExperienceDTO);
		heavyVehicleExperience.setHeavyVehicleExperienceId(null);
		String hveId = employeeService.addHeavyVehicleExperience(heavyVehicleExperience).getHeavyVehicleExperienceId();
		heavyVehicleExperienceDTO.setHeavyVehicleExperienceId(hveId);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Heavy vehicle experience Added", heavyVehicleExperienceDTO),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/documents/references")
	public ResponseEntity<Object> addReferences(@RequestParam(value = "cnic", required = false) MultipartFile cnic,
			@RequestParam(value = "referenceForm", required = false) MultipartFile referenceForm,
			@RequestParam(value = "employeeId") String employeeId,
			@RequestParam(value = "referenceId") String referenceId,
			@RequestParam(value = "referenceName") String referenceName,
			@RequestParam(value = "referenceContact") String referenceContact,
			@RequestParam(value = "referenceRelation") String referenceRelation,
			@RequestParam(value = "referenceAddress") String referenceAddress,
			@RequestParam(value = "referenceCnic") String referenceCnic) throws Exception {

		ReferenceDTO referenceDto = new ReferenceDTO(referenceId, referenceName, referenceContact, referenceRelation,
				referenceAddress, referenceCnic, employeeId, null, null);
		Reference reference = employeeMapper.mapToReference(referenceDto);
		String refId = employeeService.addReference(reference, cnic, referenceForm).getReferenceId();
		referenceDto.setReferenceId(refId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Reference Added", referenceDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/documents/tp")
	public ResponseEntity<Object> updateTrainingProfile(
			@RequestParam(value = "trainingCertificate", required = false) MultipartFile trainingCertificate,
			@RequestParam(value = "attendanceSheet", required = false) MultipartFile attendanceSheet,
			@RequestParam(value = "pictureWithTrainer", required = false) MultipartFile pictureWithTrainer,
			@RequestParam(value = "trainingCertificateUrl") String trainingCertificateUrl,
			@RequestParam(value = "attendanceSheetUrl") String attendanceSheetUrl,
			@RequestParam(value = "pictureWithTrainerUrl") String pictureWithTrainerUrl,
			@RequestParam("trainingProfileId") String trainingProfileId,
			@RequestParam("trainingType") String trainingType, @RequestParam("issuedDate") String issuedDate,
			@RequestParam("expiryDate") String expiryDate, @RequestParam("trainerName") String trainerName,
			@RequestParam("trainingStation") String trainingStation, @RequestParam("employeeId") String employeeId)
			throws IOException, ParseException {
		TrainingProfileDTO trainingProfileDto = new TrainingProfileDTO(trainingProfileId, trainingType, issuedDate,
				expiryDate, trainerName, trainingStation, employeeId, trainingCertificateUrl, attendanceSheetUrl,
				pictureWithTrainerUrl);
		TrainingProfile trainingProfile = employeeMapper.mapToTrainingProfile(trainingProfileDto);
		employeeService.updateTrainingProfile(trainingProfile, trainingCertificate, attendanceSheet,
				pictureWithTrainer);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Training profile updated", trainingProfileDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/documents/mtr")
	public ResponseEntity<Object> updateMedicalTestReport(
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam("employeeId") String employeeId, @RequestParam("mtrId") String mtrId,
			@RequestParam("mtrType") String mtrType, @RequestParam("mtrDate") String mtrDate,
			@RequestParam("mtrHospital") String mtrHospital, @RequestParam("mtrStation") String mtrStation,
			@RequestParam("mtrResult") String mtrResult) throws IOException, ParseException {
		MedicalTestReportDTO medicalTestReportDTO = new MedicalTestReportDTO(mtrId, mtrType, mtrDate, mtrHospital,
				mtrStation, mtrResult, employeeId, null);
		MedicalTestReport medicalTestReport = employeeMapper.mapToMedicalTestReport(medicalTestReportDTO);
		employeeService.updateMedicalTestReport(medicalTestReport, file);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Medical test report updated", medicalTestReportDTO),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/documents/hve")
	public ResponseEntity<Object> updateHeavyVehicleExperience(
			@RequestBody HeavyVehicleExperienceDTO heavyVehicleExperienceDTO) throws Exception {
		HeavyVehicleExperience heavyVehicleExperience = employeeMapper
				.mapToHeavyVehicleExperience(heavyVehicleExperienceDTO);
		employeeService.updateHeavyVehicleExperience(heavyVehicleExperience);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Heavy vehicle experience updated",
				heavyVehicleExperienceDTO), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/documents/references")
	public ResponseEntity<Object> updateReferences(@RequestParam(value = "cnic", required = false) MultipartFile cnic,
			@RequestParam(value = "referenceForm", required = false) MultipartFile referenceForm,
			@RequestParam(value = "employeeId") String employeeId,
			@RequestParam(value = "referenceId") String referenceId,
			@RequestParam(value = "referenceName") String referenceName,
			@RequestParam(value = "referenceContact") String referenceContact,
			@RequestParam(value = "referenceRelation") String referenceRelation,
			@RequestParam(value = "referenceAddress") String referenceAddress,
			@RequestParam(value = "referenceCnic") String referenceCnic) throws Exception {

		ReferenceDTO referenceDto = new ReferenceDTO(referenceId, referenceName, referenceContact, referenceRelation,
				referenceAddress, referenceCnic, employeeId, null, null);
		Reference reference = employeeMapper.mapToReference(referenceDto);
		employeeService.updateReference(reference, cnic, referenceForm);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Reference Added", referenceDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/documents/tp")
	public ResponseEntity<Object> getTrainingProfile(@RequestParam("employeeId") String employeeId)
			throws IOException, ParseException {
		List<TrainingProfile> trainingProfiles = employeeService.getAllTrainingProfile(employeeId);
		List<TrainingProfileDTO> trainingProfileDtos = new ArrayList<>();
		for (TrainingProfile trainingProfile : trainingProfiles) {
			trainingProfileDtos.add(employeeMapper.mapToTrainingProfileDTO(trainingProfile));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				trainingProfileDtos.size() + " training profiles fetched", trainingProfileDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/documents/mtr")
	public ResponseEntity<Object> getMedicalTestReport(@RequestParam("employeeId") String employeeId)
			throws IOException, ParseException {
		List<MedicalTestReport> medicalTestReports = employeeService.getAllMedicalTestReport(employeeId);
		List<MedicalTestReportDTO> medicalTestReportDtos = new ArrayList<>();
		for (MedicalTestReport medicalTestReport : medicalTestReports) {
			medicalTestReportDtos.add(employeeMapper.mapToMedicalTestReportDTO(medicalTestReport));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(),
						medicalTestReportDtos.size() + " medical test reports fetched", medicalTestReportDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/documents/hve")
	public ResponseEntity<Object> getHeavyVehicleExperience(@RequestParam("employeeId") String employeeId)
			throws Exception {
		List<HeavyVehicleExperience> heavyVehicleExperiences = employeeService.getAllHeavyVehicleExperience(employeeId);
		List<HeavyVehicleExperienceDTO> heavyVehicleExperienceDtos = new ArrayList<>();
		for (HeavyVehicleExperience heavyVehicleExperience : heavyVehicleExperiences) {
			heavyVehicleExperienceDtos.add(employeeMapper.mapToHeavyVehicleExperienceDTO(heavyVehicleExperience));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				heavyVehicleExperienceDtos.size() + " heavy vehicle experiences fetched", heavyVehicleExperienceDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/documents/references")
	public ResponseEntity<Object> getReferences(@RequestParam("employeeId") String employeeId) throws Exception {
		List<Reference> references = employeeService.getAllReference(employeeId);
		List<ReferenceDTO> referenceDtos = new ArrayList<>();
		for (Reference reference : references) {
			referenceDtos.add(employeeMapper.mapToReferenceDTO(reference));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), referenceDtos.size() + " references fetched", referenceDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/documents/tp/{tpId}")
	public ResponseEntity<Object> deleteTrainingProfile(@PathVariable("tpId") String tpId) {
		employeeService.deleteTrainingProfile(tpId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Training Profile deleted", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/documents/mtr/{mtrId}")
	public ResponseEntity<Object> deleteMedicalTestReport(@PathVariable("mtrId") String mtrId) {
		employeeService.deleteMedicalTestReport(mtrId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Medical test report deleted", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/documents/hve/{hveId}")
	public ResponseEntity<Object> deleteHeaveVehicleExperience(@PathVariable("hveId") String hveId) {
		employeeService.deleteHeavyVehicleExperience(hveId);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Heavy vehicle experience deleted", null), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/documents/references/{refId}")
	public ResponseEntity<Object> deleteReference(@PathVariable("refId") String refId) {
		employeeService.deleteReference(refId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Reference deleted", null),
				HttpStatus.OK);
	}
}
