package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.dto.InventoryAdjustmentDTO;
import com.fleetX.dto.InventoryDTO;
import com.fleetX.dto.ItemDTO;
import com.fleetX.dto.PurchaseOrderDTO;
import com.fleetX.dto.ReceivePurchaseOrderDTO;
import com.fleetX.dto.WorkOrderDTO;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.mapper.InventoryMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IFileDataService;
import com.fleetX.service.IInventoryService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX + "inventory")
public class InventoryController {
	@Autowired
	InventoryMapper inventoryMapper;
	@Autowired
	IInventoryService inventoryService;
	@Autowired
	IFileDataService fileDataService;

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> getAllInventory() {
		List<Inventory> inventories = inventoryService.getAllInventory();
		List<InventoryDTO> inventoryDtos = new ArrayList<>();
		for (Inventory inventory : inventories) {
			inventoryDtos.add(inventoryMapper.mapToInventoryDTO(inventory));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				inventoryDtos.size() + " inventories fetched", inventoryDtos));
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<Object> getInventoryById(@PathVariable("id") Integer inventoryId) {
		Inventory inventory = inventoryService.getInventoryById(inventoryId);
		InventoryDTO inventoryDto = inventoryMapper.mapToInventoryDTO(inventory);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Inventory fetched", inventoryDto));
	}

	@CrossOrigin
	@GetMapping("/filter")
	public ResponseEntity<Object> filterInventory(@RequestParam("item") Integer itemId) {
		List<Inventory> inventories = inventoryService.filterInventory(itemId);
		List<InventoryDTO> inventoryDtos = new ArrayList<>();
		for (Inventory inventory : inventories) {
			inventoryDtos.add(inventoryMapper.mapToInventoryDTO(inventory));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				inventoryDtos.size() + " inventories fetched", inventoryDtos));
	}

	@CrossOrigin
	@GetMapping(value = "/export")
	public ResponseEntity<Object> exportInventories() throws ParseException, IOException {
		List<Inventory> inventories = inventoryService.getAllInventory();
		if (inventories == null || inventories.size() == 0)
			throw new NoSuchElementException("Inventories: " + SystemType.NO_SUCH_ELEMENT);
		byte[] file = fileDataService.exportInventoriesToExcel(inventories, SystemType.EXCEL_INVENTORY_FILE_NAME);
		if (file == null)
			throw new IOException("Could not generate excel file!");
		return ResponseEntity.ok()
				.contentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(file);
	}

	// ITEMS

	@CrossOrigin
	@PostMapping("/items")
	public ResponseEntity<Object> addItem(@RequestParam(name = "qty", required = false) Integer qty,
			@RequestParam(name = "cost", required = false) Double cost, @RequestBody ItemDTO itemDto) {
		Item item = inventoryMapper.mapToItem(itemDto);
		if (qty == null && cost == null)
			item = inventoryService.addItem(item);
		else
			item = inventoryService.addItem(item, qty, cost);
		itemDto.setItemId(item.getItemId());
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Item added", itemDto));
	}

	@CrossOrigin
	@GetMapping("/items")
	public ResponseEntity<Object> getAllItem() {
		List<Item> items = inventoryService.getAllItem();
		List<ItemDTO> itemDtos = new ArrayList<>();
		for (Item item : items) {
			itemDtos.add(inventoryMapper.mapToItemDTO(item));
		}
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), itemDtos.size() + " items fetched", itemDtos));
	}

	@CrossOrigin
	@GetMapping("/items/{id}")
	public ResponseEntity<Object> getItemById(@PathVariable("id") Integer itemId) {
		Item item = inventoryService.getItemById(itemId);
		ItemDTO itemDto = inventoryMapper.mapToItemDTO(item);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Item fetched", itemDto));
	}

	@CrossOrigin
	@PutMapping("/items")
	public ResponseEntity<Object> updateItem(@RequestBody ItemDTO itemDto) {
		Item item = inventoryMapper.mapToItem(itemDto);
		item = inventoryService.updateItem(item);
		itemDto.setItemId(item.getItemId());
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Item updated", itemDto));
	}

	@CrossOrigin
	@DeleteMapping("/items/{id}")
	public ResponseEntity<Object> deleteItemById(@PathVariable("id") Integer itemId) {
		inventoryService.deleteItemById(itemId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Item deleted", null));
	}

	// PURCHASE ORDER

	@CrossOrigin
	@PostMapping("/purchase")
	public ResponseEntity<Object> addPurhaseOrder(@RequestBody PurchaseOrderDTO purchaseOrderDto)
			throws ParseException {
		PurchaseOrder purchaseOrder = inventoryMapper.mapToPurchaseOrder(purchaseOrderDto);
		purchaseOrder = inventoryService.addPurchaseOrder(purchaseOrder);
		purchaseOrderDto = inventoryMapper.mapToPurchaseOrderDTO(purchaseOrder);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Purchase order added", purchaseOrderDto));
	}

	@CrossOrigin
	@GetMapping("/purchase")
	public ResponseEntity<Object> getAllPurchaseOrder() throws ParseException {
		List<PurchaseOrder> purchaseOrders = inventoryService.getAllPurchaseOrder();
		List<PurchaseOrderDTO> purchaseOrderDtos = new ArrayList<>();
		for (PurchaseOrder purchaseOrder : purchaseOrders) {
			purchaseOrderDtos.add(inventoryMapper.mapToPurchaseOrderDTO(purchaseOrder));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				purchaseOrderDtos.size() + " purchase orders fetched", purchaseOrderDtos));
	}

	@CrossOrigin
	@GetMapping("/purchase/{id}")
	public ResponseEntity<Object> getPurchaseOrderById(@PathVariable("id") Integer purchaseOrderId)
			throws ParseException {
		PurchaseOrder purchaseOrder = inventoryService.getPurchaseOrderById(purchaseOrderId);
		PurchaseOrderDTO purchaseOrderDto = inventoryMapper.mapToPurchaseOrderDTO(purchaseOrder);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Purchase order fetched", purchaseOrderDto));
	}

	@CrossOrigin
	@PutMapping("/purchase")
	public ResponseEntity<Object> updatePurchaseOrder(@RequestBody PurchaseOrderDTO purchaseOrderDto)
			throws ParseException {
		PurchaseOrder purchaseOrder = inventoryMapper.mapToPurchaseOrder(purchaseOrderDto);
		purchaseOrder = inventoryService.updatePurchaseOrder(purchaseOrder);
		purchaseOrderDto = inventoryMapper.mapToPurchaseOrderDTO(purchaseOrder);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Purchase order updated", purchaseOrderDto));
	}

	@CrossOrigin
	@DeleteMapping("/purchase/{id}")
	public ResponseEntity<Object> deletePurchaseOrderById(@PathVariable("id") Integer purchaseOrderId) {
		inventoryService.deletePurchaseOrderById(purchaseOrderId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Purchase order deleted", null));
	}

	@CrossOrigin
	@PutMapping("/purchase/{id}/receive")
	public ResponseEntity<Object> receivePurchaseOrder(@PathVariable("id") Integer purchaseOrderId,
			@RequestBody ReceivePurchaseOrderDTO receiveDetail) throws ParseException {
		PurchaseOrder purchaseOrder = inventoryService.receivePurchaseOrder(receiveDetail);
		PurchaseOrderDTO purchaseOrderDto = inventoryMapper.mapToPurchaseOrderDTO(purchaseOrder);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Purchase order updated", purchaseOrderDto));
	}

	// WORK ORDER

	@CrossOrigin
	@PostMapping("/workOrder")
	public ResponseEntity<Object> addPurhaseOrder(@RequestBody WorkOrderDTO workOrderDto) throws ParseException {
		WorkOrder workOrder = inventoryMapper.mapToWorkOrder(workOrderDto);
		workOrder = inventoryService.addWorkOrder(workOrder);
		workOrderDto = inventoryMapper.mapToWorkOrderDTO(workOrder);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Work order added", workOrderDto));
	}

	@CrossOrigin
	@GetMapping("/workOrder")
	public ResponseEntity<Object> getAllWorkOrder() throws ParseException {
		List<WorkOrder> workOrders = inventoryService.getAllWorkOrder();
		List<WorkOrderDTO> workOrderDtos = new ArrayList<>();
		for (WorkOrder workOrder : workOrders) {
			workOrderDtos.add(inventoryMapper.mapToWorkOrderDTO(workOrder));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				workOrderDtos.size() + " work orders fetched", workOrderDtos));
	}

	@CrossOrigin
	@GetMapping("/workOrder/{id}")
	public ResponseEntity<Object> getWorkOrderById(@PathVariable("id") Integer workOrderId) throws ParseException {
		WorkOrder workOrder = inventoryService.getWorkOrderById(workOrderId);
		WorkOrderDTO workOrderDto = inventoryMapper.mapToWorkOrderDTO(workOrder);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Work order fetched", workOrderDto));
	}

	@CrossOrigin
	@PutMapping("/workOrder")
	public ResponseEntity<Object> updateWorkOrder(@RequestBody WorkOrderDTO workOrderDto) throws ParseException {
		WorkOrder workOrder = inventoryMapper.mapToWorkOrder(workOrderDto);
		workOrder = inventoryService.updateWorkOrder(workOrder);
		workOrderDto = inventoryMapper.mapToWorkOrderDTO(workOrder);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Work order updated", workOrderDto));
	}

	@CrossOrigin
	@DeleteMapping("/workOrder/{id}")
	public ResponseEntity<Object> deleteWorkOrderById(@PathVariable("id") Integer workOrderId) {
		inventoryService.deleteWorkOrderById(workOrderId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Work order deleted", null));
	}

	@CrossOrigin
	@PutMapping("/workOrder/{id}/status")
	public ResponseEntity<Object> updateWorkOrderStatus(@PathVariable("id") Integer workOrderId,
			@RequestParam(name = "status") String status) throws ParseException {
		inventoryService.updateWorkOrderStatus(workOrderId, status);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Work order status updated", null));
	}

	@CrossOrigin
	@GetMapping(value = "/workOrder/export")
	public ResponseEntity<Object> exportWorkOrders() throws ParseException, IOException {
		List<WorkOrder> workOrders = inventoryService.getAllWorkOrder();
		if (workOrders == null || workOrders.size() == 0)
			throw new NoSuchElementException("Work Orders: " + SystemType.NO_SUCH_ELEMENT);
		byte[] file = fileDataService.exportWorkOrderToExcel(workOrders, SystemType.EXCEL_WORK_ORDER_FILE_NAME);
		if (file == null)
			throw new IOException("Could not generate excel file!");
		return ResponseEntity.ok()
				.contentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(file);
	}

	// INVENTORY ADJUSTMENT

	@CrossOrigin
	@PostMapping("/adjustments")
	public ResponseEntity<Object> addInventoryAdjustment(@RequestBody InventoryAdjustmentDTO inventoryAdjustmentDto)
			throws ParseException {
		InventoryAdjustment inventoryAdjustment = inventoryMapper.mapToInventoryAdjustment(inventoryAdjustmentDto);
		inventoryAdjustment = inventoryService.addInventoryAdjustment(inventoryAdjustment);
		inventoryAdjustmentDto = inventoryMapper.mapToInventoryAdjustment(inventoryAdjustment);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Inventory adjusted", inventoryAdjustmentDto));
	}

	@CrossOrigin
	@GetMapping("/adjustments")
	public ResponseEntity<Object> getAllInventoryAdjustment() throws ParseException {
		List<InventoryAdjustment> inventoryAdjustments = inventoryService.getAllInventoryAdjustment();
		List<InventoryAdjustmentDTO> inventoryAdjustmentDtos = new ArrayList<>();
		for (InventoryAdjustment inventoryAdjustment : inventoryAdjustments) {
			inventoryAdjustmentDtos.add(inventoryMapper.mapToInventoryAdjustment(inventoryAdjustment));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				inventoryAdjustmentDtos.size() + " inventory adjustment fetched", inventoryAdjustmentDtos));
	}

	@CrossOrigin
	@GetMapping("/adjustments/{id}")
	public ResponseEntity<Object> getInventoryAdjustmentById(@PathVariable("id") Integer id) throws ParseException {
		InventoryAdjustment inventoryAdjustment = inventoryService.getInventoryAdjustmentById(id);
		InventoryAdjustmentDTO inventoryAdjustmentDto = inventoryMapper.mapToInventoryAdjustment(inventoryAdjustment);
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Inventory adjustment fetched", inventoryAdjustmentDto));
	}

}
