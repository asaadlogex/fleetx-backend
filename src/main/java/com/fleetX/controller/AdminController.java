package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.dto.AdminCompanyDTO;
import com.fleetX.dto.FuelCardDTO;
import com.fleetX.dto.RoleDTO;
import com.fleetX.dto.RouteDTO;
import com.fleetX.dto.UserDTO;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;
import com.fleetX.mapper.CompanyMapper;
import com.fleetX.mapper.GeneralMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IFileDataService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX + "admin"
//, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
)
public class AdminController {

	@Autowired
	IAdminService adminService;
	@Autowired
	IFileDataService fileDataService;
	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	GeneralMapper generalMapper;

	@CrossOrigin
	@PostMapping("/companies")
	@PreAuthorize("hasAuthority('C_ADMIN')")
	public ResponseEntity<Object> addAdminCompany(@RequestBody AdminCompanyDTO companyDto) throws Exception {
		AdminCompany adminCompany = companyMapper.mapToAdminCompany(companyDto);
		String adminCompanyId = adminService.addAdminCompany(adminCompany).getAdminCompanyId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Admin company created", adminCompanyId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies")
	public ResponseEntity<Object> getCompanies() {
		List<AdminCompany> adminCompanies = adminService.findAllAdminCompany();
		List<AdminCompanyDTO> companiesDto = new ArrayList<>();
		if (adminCompanies != null && adminCompanies.size() > 0) {
			for (AdminCompany adminCompany : adminCompanies) {
				AdminCompanyDTO companyDto = companyMapper.mapToAdminCompanyDTO(adminCompany);
				companyDto.setCompanyId(adminCompany.getAdminCompanyId());
				companiesDto.add(companyDto);
			}
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				companiesDto.size() + " admin companies fetched", companiesDto), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/{id}")
	@PreAuthorize("hasAuthority('R_ADMIN')")
	public ResponseEntity<Object> getCompanyById(@PathVariable("id") String adminCompanyId) throws Exception {
		AdminCompany adminCompany = adminService.findAdminCompanyById(adminCompanyId);
		AdminCompanyDTO companyDto = companyMapper.mapToAdminCompanyDTO(adminCompany);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Admin company fetched", companyDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/filter")
	public ResponseEntity<Object> filterAdminCompany(@RequestParam("id") String id) throws Exception {
		AdminCompany ac = new AdminCompany();
		// geter setter
		List<AdminCompany> companies = adminService.filterAdminCompanies(Example.of(ac));
		List<AdminCompanyDTO> adminCompaniesDto = new ArrayList<>();
		if (companies != null && companies.size() > 0) {
			for (AdminCompany adminCompany : companies) {
				adminCompaniesDto.add(companyMapper.mapToAdminCompanyDTO(adminCompany));
			}
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				adminCompaniesDto.size() + " admin companies fetched", adminCompaniesDto), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/companies/{id}")
	@PreAuthorize("hasAuthority('D_ADMIN')")
	public ResponseEntity<Object> deleteCompany(@PathVariable("id") String adminCompanyId) throws Exception {
		adminService.deleteAdminCompany(adminCompanyId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Admin company deleted", adminCompanyId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/companies")
	@PreAuthorize("hasAuthority('U_ADMIN')")
	public ResponseEntity<Object> updateCompany(@RequestBody AdminCompanyDTO companyDto) throws Exception {
		AdminCompany company = companyMapper.mapToAdminCompany(companyDto);
		String companyId = adminService.updateAdminCompany(company).getAdminCompanyId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Admin company updated", companyId),
				HttpStatus.OK);
	}

	// FUEL CARDS
	@CrossOrigin
	@PostMapping("/fuelCards")
	@PreAuthorize("hasAuthority('C_FUEL_CARD')")
	public ResponseEntity<Object> addFuelCard(@RequestBody FuelCardDTO fuelCardDto) throws ParseException {
		FuelCard fuelCard = companyMapper.mapToFuelCard(fuelCardDto);
		Integer fuelCardId = adminService.addFuelCard(fuelCard).getFuelCardId();
		fuelCardDto.setFuelCardId(fuelCardId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Fuel card added", fuelCardDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/fuelCards/{id}")
	@PreAuthorize("hasAuthority('R_FUEL_CARD')")
	public ResponseEntity<Object> getFuelCardById(@PathVariable("id") Integer fuelCardId) throws ParseException {
		FuelCard fuelCard = adminService.findFuelCardById(fuelCardId);
		FuelCardDTO fuelCardDto = companyMapper.mapToFuelCardDTO(fuelCard);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Fuel card fetched", fuelCardDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/fuelCards")
	public ResponseEntity<Object> getAllFuelCard() throws ParseException {
		List<FuelCard> fuelCards = adminService.getAllFuelCard();
		List<FuelCardDTO> fuelCardDtos = new ArrayList<>();
		for (FuelCard fuelCard : fuelCards) {
			fuelCardDtos.add(companyMapper.mapToFuelCardDTO(fuelCard));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), fuelCardDtos.size() + " fuel cards fetched", fuelCardDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/fuelCards/filter")
	public ResponseEntity<Object> filterFuelCard(@RequestParam(name = "status", required = false) String status,
			@RequestParam(name = "cId", required = false) String customerId,
			@RequestParam(name = "aId", required = false) String acId) throws ParseException {
		List<FuelCard> fuelCards = adminService.filterFuelCard(customerId, acId, status);
		List<FuelCardDTO> fuelCardDtos = new ArrayList<>();
		for (FuelCard fuelCard : fuelCards) {
			fuelCardDtos.add(companyMapper.mapToFuelCardDTO(fuelCard));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), fuelCardDtos.size() + " fuel cards fetched", fuelCardDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/fuelCards")
	@PreAuthorize("hasAuthority('U_FUEL_CARD')")
	public ResponseEntity<Object> updateFuelCard(@RequestBody FuelCardDTO fuelCardDto) throws ParseException {
		FuelCard fuelCard = companyMapper.mapToFuelCard(fuelCardDto);
		adminService.updateFuelCard(fuelCard);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Fuel card updated", fuelCardDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/fuelCards/{id}")
	@PreAuthorize("hasAuthority('D_FUEL_CARD')")
	public ResponseEntity<Object> deleteFuelCardById(@PathVariable("id") Integer fuelCardId) {
		adminService.deleteFuelCardById(fuelCardId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Fuel card deleted", fuelCardId),
				HttpStatus.OK);
	}

	// USERS
	@CrossOrigin
	@PostMapping("/users")
	@PreAuthorize("hasAuthority('C_USER')")
	public ResponseEntity<Object> createUser(@RequestBody UserDTO userDto) {
		User user = generalMapper.mapToUser(userDto);
		adminService.addUser(user);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Signup successful", userDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/users")
	public ResponseEntity<Object> getAllUser() {
		List<User> users = adminService.getAllUser();
		List<UserDTO> userDtos = new ArrayList<>();
		for (User user : users) {
			userDtos.add(generalMapper.mapToUserDTO(user));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), userDtos.size() + " users fetched", userDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/users/{username}")
	@PreAuthorize("hasAuthority('R_USER')")
	public ResponseEntity<Object> getUser(@PathVariable("username") String username) {
		User user = adminService.getUserByUsername(username);
		UserDTO userDto = generalMapper.mapToUserDTO(user);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "User fetched", userDto), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/users")
	@PreAuthorize("hasAuthority('U_USER')")
	public ResponseEntity<Object> updateUser(@RequestBody UserDTO userDto) {
		User user = generalMapper.mapToUser(userDto);
		adminService.updateUser(user);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "User updated", userDto), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/users/{username}")
	@PreAuthorize("hasAuthority('D_USER')")
	public ResponseEntity<Object> deleteUser(@PathVariable("username") String username) {
		adminService.deleteUserByUsername(username);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "User deleted", username),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/users")
	@PreAuthorize("hasAuthority('D_USER')")
	public ResponseEntity<Object> deleteAllUser() {
		adminService.deleteAllUser();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "All users deleted", null),
				HttpStatus.OK);
	}

	// ROLES
	@CrossOrigin
	@PostMapping("/roles")
	@PreAuthorize("hasAuthority('C_ROLE')")
	public ResponseEntity<Object> createRole(@RequestBody RoleDTO roleDto) {
		Role user = generalMapper.mapToRole(roleDto);
		adminService.addRole(user);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Signup successful", roleDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/roles")
	public ResponseEntity<Object> getAllRole() {
		List<Role> users = adminService.getAllRole();
		List<RoleDTO> roleDtos = new ArrayList<>();
		for (Role user : users) {
			roleDtos.add(generalMapper.mapToRoleDTO(user));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), roleDtos.size() + " users fetched", roleDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/roles/{rolename}")
	@PreAuthorize("hasAuthority('R_ROLE')")
	public ResponseEntity<Object> getRole(@PathVariable("rolename") String rolename) {
		Role user = adminService.getRoleByRoleName(rolename);
		RoleDTO roleDto = generalMapper.mapToRoleDTO(user);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Role fetched", roleDto), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/roles")
	@PreAuthorize("hasAuthority('U_ROLE')")
	public ResponseEntity<Object> updateRole(@RequestBody RoleDTO roleDto) {
		Role user = generalMapper.mapToRole(roleDto);
		adminService.updateRole(user);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Role updated", roleDto), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/roles/{rolename}")
	@PreAuthorize("hasAuthority('D_ROLE')")
	public ResponseEntity<Object> deleteRole(@PathVariable("rolename") String rolename) {
		adminService.deleteRoleByRoleName(rolename);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Role deleted", rolename),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/roles")
	@PreAuthorize("hasAuthority('D_ROLE')")
	public ResponseEntity<Object> deleteAllRole() {
		adminService.deleteAllRole();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "All users deleted", null),
				HttpStatus.OK);
	}

	// ROUTES
	@CrossOrigin
	@PostMapping("/routes")
	@PreAuthorize("hasAuthority('C_ROUTE')")
	public ResponseEntity<Object> addRoute(@RequestBody RouteDTO routeDto) {
		Route route = companyMapper.mapToRoute(routeDto);
		String routeId = adminService.addRoute(route).getRouteId();
		routeDto.setRouteId(routeId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route added", routeDto), HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/routes/file")
	@PreAuthorize("hasAuthority('C_ROUTE')")
	public ResponseEntity<Object> addRouteByFile(@RequestParam(value = "file") MultipartFile routeFile)
			throws IOException, InvalidFormatException {
		List<RouteDTO> routeDtos = fileDataService.mapToRouteDTO(routeFile);
		List<Route> routes = new ArrayList<>();
		for (RouteDTO routeDto : routeDtos) {
			routes.add(companyMapper.mapToRoute(routeDto));
		}
		List<Route> routesAdded = adminService.addAllRoute(routes);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route added", routesAdded),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/routes/{id}")
	@PreAuthorize("hasAuthority('R_ROUTE')")
	public ResponseEntity<Object> getRouteById(@PathVariable("id") String routeId) {
		Route route = adminService.findRouteById(routeId);
		RouteDTO routeDto = companyMapper.mapToRouteDTO(route);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route fetched", routeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/routes")
	public ResponseEntity<Object> getAllRoute(@RequestParam(name = "area", defaultValue = "false") Boolean isArea) {
		List<Route> routes = null;
		if (isArea)
			routes = adminService.getAllIntraCityRoute();
		else
			routes = adminService.getAllInterCityRoute();
		List<RouteDTO> routeDtos = new ArrayList<>();
		for (Route route : routes) {
			routeDtos.add(companyMapper.mapToRouteDTO(route));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), routeDtos.size() + " routes fetched", routeDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/routes/intracity")
	public ResponseEntity<Object> filterRouteByCity(@RequestParam("city") String cityCode) {
		List<Route> routes = adminService.getIntraCityRoutesByCity(cityCode);
		List<RouteDTO> routeDtos = new ArrayList<>();
		for (Route route : routes) {
			routeDtos.add(companyMapper.mapToRouteDTO(route));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), routeDtos.size() + " routes fetched", routeDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/routes/filter")
	public ResponseEntity<Object> filterRoute(@RequestParam(name = "to", required = false) String to,
			@RequestParam(name = "from", required = false) String from) {
		List<Route> routes = adminService.filterRoute(to, from);
		List<RouteDTO> routeDtos = new ArrayList<>();
		for (Route route : routes) {
			routeDtos.add(companyMapper.mapToRouteDTO(route));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), routeDtos.size() + " routes fetched", routeDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/routes")
	@PreAuthorize("hasAuthority('U_ROUTE')")
	public ResponseEntity<Object> updateRoute(@RequestBody RouteDTO routeDto) {
		Route route = companyMapper.mapToRoute(routeDto);
		adminService.updateRoute(route);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route updated", routeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/routes/{id}")
	@PreAuthorize("hasAuthority('D_ROUTE')")
	public ResponseEntity<Object> deleteRouteById(@PathVariable("id") String routeId) {
		adminService.deleteRouteById(routeId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route deleted", routeId),
				HttpStatus.OK);
	}

}
