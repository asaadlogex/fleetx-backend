package com.fleetX.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IFTPService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX + "files")
public class FileController {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IFTPService ftpService;
	@Autowired
	IEmployeeService employeeService;

	@CrossOrigin
	@GetMapping(value = "/{pathBase}/**")
	public ResponseEntity<Object> getFile(@PathVariable(value = "pathBase") String pathBase, HttpServletRequest request)
			throws IOException {
		final String path = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
		final String bestMatchingPattern = request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)
				.toString();

		String arguments = new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);

		String pathName;
		if (null != arguments && !arguments.isEmpty()) {
			pathName = pathBase + '/' + arguments;
		} else {
			pathName = pathBase;
		}

		byte[] file = ftpService.getFile(pathName);

		String ext = systemUtil.getExtensionFromFileName(pathName);
		if (ext.equals("txt")) {
			return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(file);
		} else if (ext.equals("pdf")) {
			return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
					.header("Content-Disposition", "inline;filename=file.pdf").body(file);
		} else if (ext.equals("xlsx")) {
			return ResponseEntity.ok()
					.contentType(MediaType
							.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
					.header("Content-Disposition", "inline;filename=file.xlsx").body(file);
		} else if (ext.equals("jpg") || ext.equals("jpeg")) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(file);
		} else if (ext.equals("png")) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(file);
		} else {
			throw new IOException("Unsupported file format!");
		}
	}

	@CrossOrigin
	@DeleteMapping(value = "/{pathBase}/**")
	public ResponseEntity<Object> deleteFile(@PathVariable(value = "pathBase") String pathBase,
			HttpServletRequest request) throws IOException {
		final String path = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
		final String bestMatchingPattern = request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)
				.toString();

		String arguments = new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);

		String pathName;
		if (null != arguments && !arguments.isEmpty()) {
			pathName = pathBase + '/' + arguments;
		} else {
			pathName = pathBase;
		}
		boolean success = ftpService.deleteFile(pathName);
		if (success)
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "File deleted", null),
					HttpStatus.OK);
		else
			throw new IOException("Could not delete file");
	}
}
