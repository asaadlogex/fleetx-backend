package com.fleetX.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.entity.dropdown.DBrand;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DItemCategory;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IDropDownService;

@CrossOrigin
@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "dropdowns"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class DropDownController {
	@Autowired
	IDropDownService dropDownService;

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> getDropdowns(@RequestParam("type") String type,
			@RequestParam(name = "city", required = false) String cityCode) {
		switch (type) {
		case SystemType.ASSET_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDAssetType()),
					HttpStatus.OK);
		case SystemType.BUSINESS_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDBusinessType()), HttpStatus.OK);
		case SystemType.CHANNEL_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDChannel()),
					HttpStatus.OK);
		case SystemType.COMPANY_DOCUMENT_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDCompanyDocumentType()), HttpStatus.OK);
		case SystemType.DEPARTMENT_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDDepartment()), HttpStatus.OK);
		case SystemType.EMPLOYEE_DOCUMENT_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDEmployeeDocument()), HttpStatus.OK);
		case SystemType.LEASE_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDLeaseType()),
					HttpStatus.OK);
		case SystemType.MAKE_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDMake()),
					HttpStatus.OK);
		case SystemType.POSITION_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDPosition()),
					HttpStatus.OK);
		case SystemType.USER_PRIVILEGE_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDPrivilege()),
					HttpStatus.OK);
		case SystemType.TRAILER_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDTrailerType()), HttpStatus.OK);
		case SystemType.TRAILER_SIZE_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDTrailerSize()), HttpStatus.OK);
		case SystemType.FORMATION_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDFormation()),
					HttpStatus.OK);
		case SystemType.SUPPLIER_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDSupplierType()), HttpStatus.OK);
		case SystemType.EMPLOYEE_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDEmployeeType()), HttpStatus.OK);
		case SystemType.CONTAINER_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDContainerType()), HttpStatus.OK);
		case SystemType.CONTRACT_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDContractType()), HttpStatus.OK);
		case SystemType.CITY_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDCity()),
					HttpStatus.OK);
		case SystemType.BASE_STATION_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDBaseStation()), HttpStatus.OK);
		case SystemType.JOB_STATUS_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDJobStatus()),
					HttpStatus.OK);
		case SystemType.VEHICLE_OWNERSHIP_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDVehicleOwnership()), HttpStatus.OK);
		case SystemType.RWB_STATUS_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDRWBStatus()),
					HttpStatus.OK);
		case SystemType.CATEGORY_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDCategory()),
					HttpStatus.OK);
		case SystemType.STOP_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDStopType()),
					HttpStatus.OK);
		case SystemType.FUEL_TANK_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDFuelTankType()), HttpStatus.OK);
		case SystemType.PAYMENT_MODE_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDPaymentMode()), HttpStatus.OK);
		case SystemType.LOAD_STATUS_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDLoadStatus()), HttpStatus.OK);
		case SystemType.CUSTOMER_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDCustomerType()), HttpStatus.OK);
		case SystemType.INCOME_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDIncomeType()), HttpStatus.OK);
		case SystemType.EXPENSE_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDExpenseType()), HttpStatus.OK);
		case SystemType.COMPANY_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDCompanyType()), HttpStatus.OK);
		case SystemType.STATE_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDState()),
					HttpStatus.OK);
		case SystemType.STOP_PROGRESS_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDStopProgressType()), HttpStatus.OK);
		case SystemType.REIMBURSEMENT_STATUS_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDReimbursementStatus()), HttpStatus.OK);
		case SystemType.AREA_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveArea(cityCode)), HttpStatus.OK);
		case SystemType.INVOICE_STATUS_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDInvoiceStatus()), HttpStatus.OK);
		case SystemType.PAYMENT_STATUS_TYPE:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDPaymentStatus()), HttpStatus.OK);
		case SystemType.ITEM_TYPE:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDItemType()),
					HttpStatus.OK);
		case SystemType.ITEM_CATEGORY:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDItemCategory()), HttpStatus.OK);
		case SystemType.ITEM_UNIT:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDUnit()),
					HttpStatus.OK);
		case SystemType.BRAND:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDBrand()),
					HttpStatus.OK);
		case SystemType.PURCHASE_ORDER_STATUS:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDPurchaseOrderStatus()), HttpStatus.OK);
		case SystemType.WORK_ORDER_STATUS:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDWorkOrderStatus()), HttpStatus.OK);
		case SystemType.WORK_ORDER_CATEGORY:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDWorkOrderCategory()), HttpStatus.OK);
		case SystemType.WORK_ORDER_SUB_CATEGORY:
			return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), type + " fetched",
					dropDownService.retrieveDWorkOrderSubCategory()), HttpStatus.OK);
		case SystemType.PRIORITY:
			return new ResponseEntity<>(
					new SuccessResponse(HttpStatus.OK.value(), type + " fetched", dropDownService.retrieveDPriority()),
					HttpStatus.OK);
		default:
			throw new IllegalArgumentException(type + ": " + SystemType.NO_SUCH_ELEMENT);
		}

	}

	@CrossOrigin
	@PostMapping("/cities")
	public ResponseEntity<Object> addCityDropdowns(@RequestParam(name = "city", required = false) String cityCode,
			@RequestParam(name = "area", defaultValue = "false") Boolean isArea, @RequestBody DCity city) {
		if (isArea)
			city = dropDownService.createArea(cityCode, city);
		else
			city = dropDownService.createDCity(city);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "City/Area added", city), HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/itemCategories")
	public ResponseEntity<Object> addItemCategory(@RequestBody DItemCategory itemCategory) {
		itemCategory = dropDownService.createDItemCategory(itemCategory);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Item category added", itemCategory),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/brands")
	public ResponseEntity<Object> addBrand(@RequestBody DBrand brand) {
		brand = dropDownService.createDBrand(brand);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Brand added", brand), HttpStatus.OK);
	}
}
