package com.fleetX.controller.exception;

import java.io.IOException;
import java.text.ParseException;
import java.util.NoSuchElementException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

import com.fleetX.model.ErrorResponse;

import io.jsonwebtoken.ExpiredJwtException;

@ControllerAdvice
public class DatabaseExceptionController {

	@ExceptionHandler(value = IllegalArgumentException.class)
	public ResponseEntity<Object> illegalArgumentException(IllegalArgumentException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = NoSuchElementException.class)
	public ResponseEntity<Object> noSuchElementException(NoSuchElementException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND.value(), exception.getMessage(), null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = EntityExistsException.class)
	public ResponseEntity<Object> entityExistsException(EntityExistsException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT.value(), exception.getMessage(), null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Object> dataIntegrityViolationException(DataIntegrityViolationException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT.value(), "Command not executed", null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = InternalServerError.class)
	public ResponseEntity<Object> internalServerErrorException(InternalServerError exception) {
		return new ResponseEntity<>(
				new ErrorResponse(HttpStatus.CONFLICT.value(), "Something went wrong, please contact support", null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = EntityNotFoundException.class)
	public ResponseEntity<Object> entityNotFoundException(EntityNotFoundException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Requested entity not Found", null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = IOException.class)
	public ResponseEntity<Object> ioException(IOException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND.value(), exception.getMessage(), null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = BadCredentialsException.class)
	public ResponseEntity<Object> badCredentialsException(BadCredentialsException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = ParseException.class)
	public ResponseEntity<Object> parseException(ParseException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = EmptyResultDataAccessException.class)
	public ResponseEntity<Object> emptyResultDataAccessException(EmptyResultDataAccessException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Expected item not found", null),
				HttpStatus.OK);
	}

	@ExceptionHandler(value = ExpiredJwtException.class)
	public ResponseEntity<Object> expiredJwtException(ExpiredJwtException exception) {
		return new ResponseEntity<>(
				new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), "Token Expired! Please login", null), HttpStatus.OK);
	}

	@ExceptionHandler(value = AccessDeniedException.class)
	public ResponseEntity<Object> accessDeniedException(AccessDeniedException exception) {
		return new ResponseEntity<>(new ErrorResponse(HttpStatus.FORBIDDEN.value(),
				"You don't have permission to access this resource", null), HttpStatus.OK);
	}

}
