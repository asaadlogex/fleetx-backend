package com.fleetX.controller.exception;

public class BaseRuntimeException extends RuntimeException {
    protected String message;

    public BaseRuntimeException(String message) {
        this.message = message;
    }

    public BaseRuntimeException() {
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
