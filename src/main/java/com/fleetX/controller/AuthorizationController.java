package com.fleetX.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.model.ErrorResponse;
import com.fleetX.model.JWTRequest;
import com.fleetX.model.JWTResponse;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IJWTService;
import com.fleetX.service.implementation.MyUserDetailsService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "authorize"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class AuthorizationController {

	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	IJWTService jwtService;
	@Autowired
	MyUserDetailsService userDetailService;

	@CrossOrigin
	@PostMapping("/login")
	public ResponseEntity<Object> getJWT(@RequestBody JWTRequest request) {
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		final String token = jwtService.generateToken(authentication);
		JWTResponse response = new JWTResponse();
		response.setUsername(request.getUsername());
		response.setToken(SystemType.TOKEN_PREFIX + token);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Login successful", response),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> verifyJWT(@RequestBody JWTResponse response) throws Exception {
		UserDetails userDetails = userDetailService.loadUserByUsername(response.getUsername());
		String tokenRaw = null;
		if (response.getToken() != null && response.getToken().length() > SystemType.TOKEN_PREFIX.length()) {
			tokenRaw = response.getToken().substring(SystemType.TOKEN_PREFIX.length());
			if (jwtService.validateToken(tokenRaw, userDetails)) {
				return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Verification successful",
						SystemType.TOKEN_PREFIX + tokenRaw), HttpStatus.OK);
			}
			else {
				return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Verification failed",
						SystemType.TOKEN_PREFIX + tokenRaw), HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Verification failed",
					SystemType.TOKEN_PREFIX + tokenRaw), HttpStatus.OK);
		}

	}
}
