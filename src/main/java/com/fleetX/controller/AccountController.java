package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.CompanyDTO;
import com.fleetX.dto.DHInvoiceDTO;
import com.fleetX.dto.InvoiceDTO;
import com.fleetX.dto.InvoicePaymentDTO;
import com.fleetX.dto.InvoiceStatsDTO;
import com.fleetX.dto.RoadwayBillSummaryDTO;
import com.fleetX.dto.TrialBalanceDTO;
import com.fleetX.dto.TrialBalanceEntryDTO;
import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.mapper.AccountMapper;
import com.fleetX.mapper.CompanyMapper;
import com.fleetX.mapper.OrderMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IAccountService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IFileDataService;
import com.fleetX.service.IOrderService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "accounts", consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE)
public class AccountController {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IFileDataService fileDataService;
	@Autowired
	IAccountService accountService;
	@Autowired
	IOrderService orderService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	AccountMapper accountMapper;
	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	OrderMapper orderMapper;

	@CrossOrigin
	@GetMapping("/trialBalance")
	public ResponseEntity<Object> getTrialBalance(@RequestParam("start") String startDate,
			@RequestParam("end") String endDate, @RequestParam("adjusted") Boolean isAdjusted) throws ParseException {
		Date start = systemUtil.convertToDate(startDate);
		Date end = systemUtil.convertToDate(endDate);
		TrialBalanceDTO trialBalance = accountService.getTrialBalance(start, end, isAdjusted);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Trial balance generated", trialBalance));
	}

	@CrossOrigin
	@PostMapping("/adjustments")
	public ResponseEntity<Object> addAdjustment(@RequestBody TrialBalanceEntryDTO adjustmentEntryDto)
			throws ParseException {
		Adjustment adjustment = accountMapper.mapToAdjustment(adjustmentEntryDto);
		adjustment = accountService.addAdjustmentEntry(adjustment);
		adjustmentEntryDto.setId(adjustment.getAdjustmentId());
		return ResponseEntity
				.ok(new SuccessResponse(HttpStatus.OK.value(), "Adjustment entry added", adjustmentEntryDto));
	}

	@CrossOrigin
	@GetMapping("/adjustments")
	public ResponseEntity<Object> getAllAdjustments() throws ParseException {
		List<Adjustment> adjustments = accountService.getAdjustments();
		List<TrialBalanceEntryDTO> adjustmentDtos = new ArrayList<>();
		for (Adjustment adjustment : adjustments) {
			adjustmentDtos.add(accountMapper.mapToTrialBalanceDTO(adjustment));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				adjustmentDtos.size() + " adjustment entries fetched", adjustmentDtos));
	}

	@CrossOrigin
	@DeleteMapping("/adjustments/{id}")
	public ResponseEntity<Object> deleteAdjustment(@PathVariable("id") Integer id) throws ParseException {
		accountService.deleteAdjustmentById(id);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Adjustment entry deleted", null));
	}

	@CrossOrigin
	@GetMapping("/adjustments/filter")
	public ResponseEntity<Object> filterAdjustments(@RequestParam(name = "head", required = false) String accountHead,
			@RequestParam(name = "start", required = false) String start,
			@RequestParam(name = "end", required = false) String end) throws ParseException {
		Date startDate = systemUtil.convertToDate(start);
		Date endDate = systemUtil.convertToDate(end);
		List<Adjustment> adjustments = accountService.filterAdjustment(accountHead, startDate, endDate);
		List<TrialBalanceEntryDTO> adjustmentDtos = new ArrayList<>();
		for (Adjustment adjustment : adjustments) {
			adjustmentDtos.add(accountMapper.mapToTrialBalanceDTO(adjustment));
		}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				adjustmentDtos.size() + " adjustment entries fetched", adjustmentDtos));
	}

	@CrossOrigin
	@GetMapping("/invoices/stats")
	public ResponseEntity<Object> countPendingInvoices(
			@RequestParam(name = "invoice", defaultValue = "Pending") String invoiceStatus) throws Exception {
		List<InvoiceStatsDTO> stats = accountService.getInvoiceStats(invoiceStatus);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice stats fetched", stats),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/rwb/filter")
	public ResponseEntity<Object> filterRoadwayBill(@RequestParam(name = "start", required = false) String startDate,
			@RequestParam(name = "end", required = false) String endDate, @RequestParam(name = "tax") String taxState,
			@RequestParam(name = "customer") String customerId, @RequestParam(name = "invoice") String invoiceStatus,
			@RequestParam(name = "type") String invoiceType) throws Exception {
		Date start = null;
		Date end = null;
		if (startDate != null)
			start = systemUtil.convertToDate(startDate);
		if (endDate != null)
			end = systemUtil.convertToDate(endDate);
		List<RoadwayBill> rwbs = orderService.filterRoadwayBill(start, end, customerId, taxState, invoiceStatus,
				invoiceType);
		List<RoadwayBillSummaryDTO> rwbDtos = new ArrayList<>();
		for (RoadwayBill rwb : rwbs) {
			rwbDtos.add(orderMapper.mapToRoadwayBillSummaryDTO(rwb));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), rwbDtos.size() + " roadway bills fetched", rwbDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/invoices")
	public ResponseEntity<Object> generateInvoice(@RequestBody List<String> rwbIds,
			@RequestParam(name = "type") String type) throws Exception {
		Invoice invoice = null;
		switch (type) {
		case SystemType.INVOICE_TYPE_PER_TRIP:
			invoice = accountService.generatePerTripInvoice(rwbIds);
			break;
		case SystemType.INVOICE_TYPE_PER_TON:
			invoice = accountService.generatePerTonInvoice(rwbIds);
			break;
		case SystemType.INVOICE_TYPE_DEDICATED:
			invoice = accountService.generatePerTripInvoice(rwbIds);
			break;
		default:
			throw new IllegalArgumentException("Invalid Invoice type!");
		}
		InvoiceDTO invoiceDto = accountMapper.mapToInvoiceDTO(invoice);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice generated", invoiceDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/fixed/filter")
	public ResponseEntity<Object> getCustomerHavingContract(@RequestParam("contract") String contractType)
			throws Exception {
		List<Company> companies = companyService.findCompanyByContractType(contractType);
		List<CompanyDTO> companyDtos = new ArrayList<>();
		for (Company company : companies) {
			companyDtos.add(companyMapper.mapToCompanyDTO(company));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), companyDtos.size() + " customers fetched", companyDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/filter")
	public ResponseEntity<Object> filterInvoice(@RequestParam(name = "customerId", required = false) String companyId,
			@RequestParam(name = "type", required = false) String invoiceType,
			@RequestParam(name = "fixed", required = false) Boolean isFixed,
			@RequestParam(name = "payment", required = false) String paymentStatus,
			@RequestParam(name = "start", required = false) String start,
			@RequestParam(name = "end", required = false) String end,
			@RequestParam(name = "month", required = false) Integer month,
			@RequestParam(name = "year", required = false) Integer year) throws Exception {
		Date startDate = null;
		Date endDate = null;
		if (start != null)
			startDate = systemUtil.convertToDate(start);
		if (end != null)
			endDate = systemUtil.convertToDate(end);
		List<Invoice> invoices = accountService.filterInvoice(companyId, invoiceType, isFixed, paymentStatus, startDate,
				endDate, month, year);
		List<InvoiceDTO> invoiceDtos = new ArrayList<>();
		for (Invoice invoice : invoices) {
			invoiceDtos.add(accountMapper.mapToInvoiceDTO(invoice));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), invoiceDtos.size() + " invoices fetched", invoiceDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/invoices/fixed")
	public ResponseEntity<Object> generateFixedInvoice(@RequestParam("customer") String customerId,
			@RequestParam("date") String dateString) throws Exception {
		Date date = systemUtil.convertToDate(dateString);
		accountService.generateFixedInvoice(customerId, date);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice generated", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices")
	public ResponseEntity<Object> getAllInvoices() throws Exception {
		List<Invoice> invoices = accountService.getAllInvoice();
		List<InvoiceDTO> invoiceDtos = new ArrayList<>();
		for (Invoice invoice : invoices) {
			invoiceDtos.add(accountMapper.mapToInvoiceDTO(invoice));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), invoiceDtos.size() + " invoices fetched", invoiceDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/{id}")
	public ResponseEntity<Object> getInvoiceById(@PathVariable("id") Integer id) throws Exception {
		Invoice invoice = accountService.getInvoiceById(id);
		InvoiceDTO invoiceDto = accountMapper.mapToInvoiceDTO(invoice);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice fetched", invoiceDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/invoices/{id}")
	public ResponseEntity<Object> deleteInvoiceById(@PathVariable("id") Integer id) throws Exception {
		accountService.deleteInvoiceById(id);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice deleted", null), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/{id}/pdf")
	public ResponseEntity<Object> getInvoicePdf(@PathVariable("id") Integer id) throws Exception {
		Invoice invoice = accountService.getInvoiceById(id);
		byte[] file = fileDataService.previewInvoice(invoice);
		if (file == null)
			throw new IOException("Could not preview invoice!");
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf")).body(file);
	}

	@CrossOrigin
	@GetMapping("/invoices/{id}/payments")
	public ResponseEntity<Object> getInvoicePayments(@PathVariable("id") Integer id) throws Exception {
		List<InvoicePayment> payments = accountService.getInvoicePayments(id);
		if (payments == null || payments.size() == 0)
			throw new NoSuchElementException("No payments found against given invoice!");
		List<InvoicePaymentDTO> paymentDtos = new ArrayList<>();
		for (InvoicePayment invoicePayment : payments) {
			paymentDtos.add(accountMapper.mapToInvoicePaymentDTO(invoicePayment));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice payments fetched", paymentDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/invoices/payment")
	public ResponseEntity<Object> receiveInvoice(@RequestBody InvoicePaymentDTO invoicePaymentDto) throws Exception {
		InvoicePayment invoicePayment = accountMapper.mapToInvoicePayment(invoicePaymentDto);
		invoicePayment = accountService.receiveInvoice(invoicePayment);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Invoice payment received!", invoicePaymentDto),
				HttpStatus.OK);
	}
	
	@CrossOrigin
	@GetMapping("/invoices/dh")
	public ResponseEntity<Object> getAllDHInvoices() throws Exception {
		List<DHInvoice> invoices = accountService.getAllDHInvoice();
		List<DHInvoiceDTO> invoiceDtos = new ArrayList<>();
		for (DHInvoice invoice : invoices) {
			invoiceDtos.add(accountMapper.mapToDHInvoiceDTO(invoice));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), invoiceDtos.size() + " invoices fetched", invoiceDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/dh/{id}")
	public ResponseEntity<Object> getDHInvoiceById(@PathVariable("id") Integer id) throws Exception {
		DHInvoice invoice = accountService.getDHInvoiceById(id);
		DHInvoiceDTO invoiceDto = accountMapper.mapToDHInvoiceDTO(invoice);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice fetched", invoiceDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/invoices/dh/{id}")
	public ResponseEntity<Object> deleteDHInvoiceById(@PathVariable("id") Integer id) throws Exception {
		accountService.deleteDHInvoiceById(id);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice deleted", null), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/invoices/dh/{id}/pdf")
	public ResponseEntity<Object> getDHInvoicePdf(@PathVariable("id") Integer id) throws Exception {
		DHInvoice invoice = accountService.getDHInvoiceById(id);
		byte[] file = fileDataService.previewDHInvoice(invoice);
		if (file == null)
			throw new IOException("Could not preview DH invoice!");
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf")).body(file);
	}

	@CrossOrigin
	@GetMapping("/invoices/dh/{id}/payments")
	public ResponseEntity<Object> getDHInvoicePayments(@PathVariable("id") Integer id) throws Exception {
		List<InvoicePayment> payments = accountService.getDHInvoicePayments(id);
		if (payments == null || payments.size() == 0)
			throw new NoSuchElementException("No payments found against given invoice!");
		List<InvoicePaymentDTO> paymentDtos = new ArrayList<>();
		for (InvoicePayment invoicePayment : payments) {
			paymentDtos.add(accountMapper.mapToInvoicePaymentDTO(invoicePayment));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Invoice payments fetched", paymentDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/invoices/dh/payment")
	public ResponseEntity<Object> receiveDHInvoice(@RequestBody InvoicePaymentDTO invoicePaymentDto) throws Exception {
		InvoicePayment invoicePayment = accountMapper.mapToInvoicePayment(invoicePaymentDto);
		invoicePayment = accountService.receiveDHInvoice(invoicePayment);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Invoice payment received!", invoicePaymentDto),
				HttpStatus.OK);
	}


}
