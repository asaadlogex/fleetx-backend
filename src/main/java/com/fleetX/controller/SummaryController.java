package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.TripDetailDTO;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IFileDataService;
import com.fleetX.service.IOrderService;
import com.fleetX.service.ISummaryService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX + "summary")
public class SummaryController {

	@Autowired
	ISummaryService summaryService;
	@Autowired
	IFileDataService fileDataService;
	@Autowired
	IOrderService orderService;
	@Autowired
	SystemUtil systemUtil;

	@CrossOrigin
	@GetMapping("/trip")
	public ResponseEntity<Object> getTripDetail(@RequestParam(name = "jobId") String jobId) throws ParseException {
		TripDetailDTO tripDetail = summaryService.getTripDetail(jobId);

		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Trip detail fetched", tripDetail),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping(value = "/trip/export")
	public ResponseEntity<Object> exportTripDetail(@RequestParam(name = "start") String startDate,
			@RequestParam(name = "end") String endDate) throws ParseException, IOException {
		Date start = systemUtil.convertToDate(startDate);
		Date end = systemUtil.convertToDate(endDate);
		List<String> jobIds = orderService.getJobIdsInRange(start, end);
		if (jobIds == null || jobIds.size() == 0)
			throw new NoSuchElementException("Jobs in given range: " + SystemType.NO_SUCH_ELEMENT);
		List<TripDetailDTO> tripDetails = new ArrayList<>();
		for (String jobId : jobIds) {
			tripDetails.add(summaryService.getTripDetail(jobId));
		}
		byte[] file = fileDataService.exportTripDetailsToExcel(tripDetails,
				SystemType.EXCEL_TRIP_DETAIL_FILE_NAME + " (" + startDate + " - " + endDate + ")");
		if (file == null)
			throw new IOException("Could not generate excel file!");
		return ResponseEntity.ok()
				.contentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.body(file);
	}

}
