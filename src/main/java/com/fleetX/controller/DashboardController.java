package com.fleetX.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.dto.DashboardData;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.ISummaryService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "dashboard"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class DashboardController {
	@Autowired
	ISummaryService summaryService;

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> getDashboardData() throws Exception {
		DashboardData dashboard = summaryService.getDashboardData();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Dashboard data fetched", dashboard),
				HttpStatus.OK);
	}

}
