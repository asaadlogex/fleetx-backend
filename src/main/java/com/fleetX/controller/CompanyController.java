package com.fleetX.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.CompanyDTO;
import com.fleetX.dto.CompanySummaryDTO;
import com.fleetX.dto.WarehouseDTO;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.order.Warehouse;
import com.fleetX.mapper.CompanyMapper;
import com.fleetX.mapper.OrderMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.ICompanyService;

@CrossOrigin
@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "companies"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class CompanyController {

	@Autowired
	ICompanyService companyService;
	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	OrderMapper orderMapper;
	@Autowired
	SystemUtil systemUtil;

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> addCompany(@RequestBody CompanyDTO companyDto) throws Exception {
		Company company = companyMapper.mapToCompany(companyDto);
		company.setCompanyId(null);
		String companyId = companyService.addCompany(company).getCompanyId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "New Company Added", companyId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/files")
	public ResponseEntity<Object> uploadFiles(@RequestParam("files") MultipartFile file,
			@RequestParam("companyId") String companyId) {

		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Files uploaded", companyId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> getCompanies(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws ParseException {
		List<Company> companies = companyService.findAllCompany();
		if (companies == null || companies.size() == 0)
			throw new NoSuchElementException();
		int total = companies.size();
		companies = systemUtil.paginate(companies, page, size);
		List<CompanySummaryDTO> companiesDto = new ArrayList<>();
		for (Company company : companies) {
			CompanySummaryDTO companyDto = companyMapper.mapToCompanySummaryDTO(company);
			companyDto.setCompanyId(company.getCompanyId());
			companiesDto.add(companyDto);
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				companiesDto.size() + " companies fetched", companiesDto, total), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<Object> getCompanyById(@PathVariable("id") String companyId) throws Exception {
		Company company = companyService.findCompanyById(companyId);
		if (company == null)
			throw new NoSuchElementException();
		CompanyDTO companyDto = companyMapper.mapToCompanyDTO(company);
		companyDto.setCompanyId(company.getCompanyId());
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Company fetched", companyDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/filter")
	public ResponseEntity<Object> filterCompany(@RequestParam(name = "bt", required = false) String businessType,
			@RequestParam(name = "status", required = false) String status,
			@RequestParam(name = "st", required = false) String supplierType,
			@RequestParam(name = "contract", required = false) Boolean haveContract) throws ParseException {
		List<Company> companies = companyService.filterCompanies(businessType, status, supplierType, haveContract);
		List<CompanySummaryDTO> companiesDtos = new ArrayList<>();
		for (Company company : companies) {
			companiesDtos.add(companyMapper.mapToCompanySummaryDTO(company));
		}
		return ResponseEntity.ok(
				new SuccessResponse(HttpStatus.OK.value(), companiesDtos.size() + " companies fetched", companiesDtos));
	}

	@CrossOrigin
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteCompany(@PathVariable("id") String companyId) throws Exception {
		companyService.deleteCompany(companyId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Company deleted", companyId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping
	public ResponseEntity<Object> updateCompany(@RequestBody CompanyDTO companyDto) throws Exception {
		Company Company = companyMapper.mapToCompany(companyDto);
		String companyId = companyService.updateCompany(Company).getCompanyId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Company updated", companyId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/warehouses")
	public ResponseEntity<Object> addWarehouse(@RequestBody WarehouseDTO warehouseDto) {
		Warehouse warehouse = orderMapper.mapToWarehouse(warehouseDto);
		Integer warehousesId = companyService.addWarehouse(warehouse).getWarehouseId();
		warehouseDto.setWarehouseId(warehousesId);
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(), "Warehouse added", warehouseDto));
	}

	@CrossOrigin
	@GetMapping("/warehouses")
	public ResponseEntity<Object> getWarehouse() {
		List<Warehouse> warehousess = companyService.getAllWarehouse();
		List<WarehouseDTO> warehousessDtos = new ArrayList<>();
		for (Warehouse warehouses : warehousess) {
			WarehouseDTO warehousesDto = orderMapper.mapToWarehouseDTO(warehouses);
			warehousesDto.setWarehouseId(warehouses.getWarehouseId());
			warehousessDtos.add(warehousesDto);
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				warehousessDtos.size() + " warehouses fetched", warehousessDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/warehouses/{id}")
	public ResponseEntity<Object> getWarehouseById(@PathVariable("id") Integer warehousesId) throws Exception {
		Warehouse warehouse = companyService.getWarehouseById(warehousesId);
		WarehouseDTO warehousesDTO = orderMapper.mapToWarehouseDTO(warehouse);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Warehouse fetched", warehousesDTO),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/warehouses/filter")
	public ResponseEntity<Object> filterWarehouse(
			@RequestParam(name = "companyId", required = false) String companyId) {
		List<WarehouseDTO> warehouseDtos = new ArrayList<>();
		List<Warehouse> warehouses = companyService.filterWarehouse(companyId);
		if (warehouses.size() > 0)
			for (Warehouse warehouse : warehouses) {
				warehouseDtos.add((orderMapper.mapToWarehouseDTO(warehouse)));
			}
		return ResponseEntity.ok(new SuccessResponse(HttpStatus.OK.value(),
				warehouseDtos.size() + " warehouses fetched", warehouseDtos));
	}

	@CrossOrigin
	@DeleteMapping("warehouses/{id}")
	public ResponseEntity<Object> deleteWarehouse(@PathVariable("id") Integer warehouseId) throws Exception {
		companyService.deleteWarehouseById(warehouseId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Warehouse deleted", warehouseId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/warehouses")
	public ResponseEntity<Object> updateWarehouse(@RequestBody WarehouseDTO warehouseDto) throws Exception {
		Warehouse warehouse = orderMapper.mapToWarehouse(warehouseDto);
		companyService.updateWarehouse(warehouse);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Warehouse updated", warehouseDto),
				HttpStatus.OK);
	}
}
