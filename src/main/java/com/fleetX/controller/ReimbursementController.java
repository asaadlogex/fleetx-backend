package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fleetX.config.SystemType;
import com.fleetX.dto.ReimbursementInfoDTO;
import com.fleetX.dto.ReimbursementInfoSummaryDTO;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.entity.order.Job;
import com.fleetX.mapper.AccountMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IAccountService;
import com.fleetX.service.IFileDataService;
import com.fleetX.service.IOrderService;
import com.itextpdf.text.DocumentException;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "reimbursements"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class ReimbursementController {

	@Autowired
	IOrderService orderService;
	@Autowired
	IAccountService accountService;
	@Autowired
	AccountMapper accountMapper;
	@Autowired
	IFileDataService fileDataService;

	@CrossOrigin
	@PostMapping("/send")
	public ResponseEntity<Object> sendReimbursement(@RequestBody List<String> jobIds) {
		orderService.updateReimbursementStatus(jobIds, SystemType.REIMBURSEMENT_STATUS_SENT);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Reimbursements sent!", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> reimburse(@RequestBody ReimbursementInfoSummaryDTO reimbursementInfoDto)
			throws ParseException {
		ReimbursementInfo reimbursementInfo = accountMapper.mapToReimbursementInfo(reimbursementInfoDto);
		accountService.addReimbursement(reimbursementInfo);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Reimbursed!", null), HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/preview")
	public ResponseEntity<Object> previewReimbursement(@RequestBody List<String> jobIds)
			throws IOException, DocumentException, ParseException {
		List<Job> jobs = new ArrayList<>();
		for (String jobId : jobIds) {
			jobs.add(orderService.getJobById(jobId));
		}
		byte[] file = fileDataService.previewReimbursement(jobs);
		if (file == null)
			throw new IOException("Could not preview reimbursements!");
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf")).body(file);
	}

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> getAllReimbursement() throws ParseException {
		List<ReimbursementInfo> reimbursements = accountService.getAllReimbursement();
		List<ReimbursementInfoSummaryDTO> reimbursementDtos = new ArrayList<>();
		for (ReimbursementInfo reimbursement : reimbursements) {
			reimbursementDtos.add(accountMapper.mapToReimbursementInfoSummaryDTO(reimbursement));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				reimbursementDtos.size() + " reimbursements fetched!", reimbursementDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<Object> getReimbursementById(@PathVariable("id") Integer id) throws ParseException {
		ReimbursementInfo reimbursement = accountService.getReimbursementById(id);
		ReimbursementInfoDTO reimbursementDto = accountMapper.mapToReimbursementInfoDTO(reimbursement);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Reimbursement fetched!", reimbursementDto), HttpStatus.OK);
	}
}
