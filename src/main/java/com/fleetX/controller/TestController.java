package com.fleetX.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.fleetX.config.SystemUtil;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IFTPService;

@RestController
@RequestMapping("/tests")
public class TestController {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IFTPService ftpService;

	@CrossOrigin
	@GetMapping
	public ResponseEntity<Object> testDate(@RequestParam String dateTime) throws Exception {
		Date date = systemUtil.convertToDateTime(dateTime);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "date time checked", systemUtil.isDatePassed(date)),
				HttpStatus.OK);
	}

	@GetMapping("/open")
	public ResponseEntity<Object> generalMethod() {
		return ResponseEntity.ok("General method executed");
	}

	@GetMapping("/a")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<Object> adminMethod() {
//		System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
		return ResponseEntity.ok("Admin method executed");
	}

	@GetMapping("/u")
	@Secured("ROLE_USER")
	public ResponseEntity<Object> userMethod() {
//		System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
		return ResponseEntity.ok("user method executed");
	}

	@GetMapping("/ftp")
	public ResponseEntity<Object> uploadFile(@RequestParam(value = "file") MultipartFile file) throws IOException {
		String path = "abc";
		ftpService.uploadFile(path, file, null);
		return ResponseEntity.ok("File uploaded");
	}

	@CrossOrigin
	@GetMapping(value = "/images/{pathBase}/**")
	public ResponseEntity<Object> getFile(@PathVariable(value = "pathBase") String pathBase, HttpServletRequest request)
			throws IOException {
		final String path = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
		final String bestMatchingPattern = request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)
				.toString();

		String arguments = new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);

		String pathName;
		if (null != arguments && !arguments.isEmpty()) {
			pathName = pathBase + '/' + arguments;
		} else {
			pathName = pathBase;
		}

		byte[] file = ftpService.getFile(pathName);

		String ext = systemUtil.getExtensionFromFileName(pathName);
		if (ext.equals("txt"))
			return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(file);
		else if (ext.equals("pdf"))
			return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf")).body(file);
		else if (ext.equals("xlsx"))
			return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
		else if (ext.equals("jpg") || ext.equals("jpeg"))
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(file);
		else if (ext.equals("png"))
			return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(file);
		return ResponseEntity.ok().body(file);
	}

}
