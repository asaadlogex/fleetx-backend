package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.ExpenseDTO;
import com.fleetX.dto.IncomeDTO;
import com.fleetX.dto.JobDTO;
import com.fleetX.dto.JobFuelDetailDTO;
import com.fleetX.dto.ProductDTO;
import com.fleetX.dto.RoadwayBillDTO;
import com.fleetX.dto.StopDTO;
import com.fleetX.dto.StopProgressDTO;
import com.fleetX.dto.TractorSummaryDTO;
import com.fleetX.dto.TrailerSummaryDTO;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.mapper.AssetMapper;
import com.fleetX.mapper.OrderMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IOrderService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "orders"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class OrderController {

	@Autowired
	OrderMapper orderMapper;
	@Autowired
	AssetMapper assetMapper;
	@Autowired
	IOrderService orderService;
	@Autowired
	SystemUtil systemUtil;

	@CrossOrigin
	@PostMapping("/jobs")
	public ResponseEntity<Object> createJob(@RequestBody JobDTO jobDto) throws Exception {
		Job job = orderMapper.mapToJob(jobDto);
		String jobId = orderService.createJob(job).getJobId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job created", jobId), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/jobs")
	public ResponseEntity<Object> getAllJob(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws Exception {
		List<Job> jobs = orderService.getAllJob();
		int total = jobs.size();
		jobs = systemUtil.paginate(jobs, page, size);
		List<JobDTO> jobDtos = new ArrayList<>();
		JobDTO jobDto;
		for (Job job : jobs) {
			jobDto = orderMapper.mapToJobDTO(job);
			jobDto.setJobFuelDetails(null);
			jobDtos.add(jobDto);
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), jobDtos.size() + " jobs fetched", jobDtos, total),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/jobs/{id}")
	public ResponseEntity<Object> getJobById(@PathVariable("id") String jobId) throws Exception {
		Job job = orderService.getJobById(jobId);
		JobDTO jobDto = orderMapper.mapToJobDTO(job);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job fetched", jobDto), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/jobs/{id}/tractors")
	public ResponseEntity<Object> getTractorsByJob(@PathVariable("id") String jobId) throws Exception {
		List<Tractor> tractors = orderService.getTractorsByJob(jobId);
		List<TractorSummaryDTO> tractorDtos = new ArrayList<>();
		for (Tractor tractor : tractors) {
			tractorDtos.add(assetMapper.mapToTractorSummaryDTO(tractor));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), tractorDtos.size() + " tractors fetched", tractorDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/jobs/{id}/trailers")
	public ResponseEntity<Object> getTrailersByJob(@PathVariable("id") String jobId) throws Exception {
		List<Trailer> trailers = orderService.getTrailersByJob(jobId);
		List<TrailerSummaryDTO> trailerDtos = new ArrayList<>();
		for (Trailer trailer : trailers) {
			trailerDtos.add(assetMapper.mapToTrailerSummaryDTO(trailer));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), trailerDtos.size() + " trailers fetched", trailerDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/jobs")
	public ResponseEntity<Object> updateJob(@RequestBody JobDTO jobDto) throws Exception {
		Job job = orderMapper.mapToJob(jobDto);
		orderService.updateJob(job);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job updated", jobDto), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/jobs/{id}")
	public ResponseEntity<Object> deleteJobById(@PathVariable("id") String jobId) throws Exception {
		orderService.deleteJobById(jobId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job deleted", jobId), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/jobs/filter")
	public ResponseEntity<Object> filterJobs(@RequestParam(name = "jobId", required = false) String jobId,
			@RequestParam(name = "rwbId", required = false) String rwbId,
			@RequestParam(name = "lic", required = false) String tractorNumber,
			@RequestParam(name = "start", required = false) String start,
			@RequestParam(name = "end", required = false) String end,
			@RequestParam(name = "rstatus", required = false) String reimbursementStatus,
			@RequestParam(name = "base", required = false) String baseStation,
			@RequestParam(name = "status", required = false) String status,
			@RequestParam(name = "rental", required = false) Boolean isRental) throws Exception {
		Date startDate = null, endDate = null;
		if (start != null)
			startDate = systemUtil.convertToDate(start);
		if (end != null)
			endDate = systemUtil.convertToDate(end);
		List<Job> jobs = orderService.filterJob(jobId, rwbId, tractorNumber, startDate, endDate, status,
				reimbursementStatus, baseStation, isRental);
		List<JobDTO> jobDtos = new ArrayList<>();
		for (Job job : jobs) {
			jobDtos.add(orderMapper.mapToJobDTO(job));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), jobDtos.size() + " jobs fetched", jobDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb")
	public ResponseEntity<Object> createRoadwayBill(@RequestBody RoadwayBillDTO rwbDto) throws Exception {
		RoadwayBill rwb = orderMapper.mapToRoadwayBill(rwbDto);
		String rwbId = orderService.createRoadwayBill(rwb).getRwbId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Roadway bill created", rwbId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb")
	public ResponseEntity<Object> getAllRoadwayBill(@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "size", required = false) Integer size) throws Exception {
		List<RoadwayBill> rwbs = orderService.getAllRoadwayBill();
		int total = rwbs.size();
		rwbs = systemUtil.paginate(rwbs, page, size);
		List<RoadwayBillDTO> rwbDtos = new ArrayList<>();
		for (RoadwayBill rwb : rwbs) {
			rwbDtos.add(orderMapper.mapToRoadwayBillDTO(rwb));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), rwbDtos.size() + " roadway bills fetched", rwbDtos, total),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/{id}")
	public ResponseEntity<Object> getRoadwayBillById(@PathVariable("id") String rwbId) throws Exception {
		RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
		RoadwayBillDTO rwbDto = orderMapper.mapToRoadwayBillDTO(rwb);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "RoadwayBill fetched", rwbDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb")
	public ResponseEntity<Object> updateRoadwayBill(@RequestBody RoadwayBillDTO rwbDto) throws Exception {
		RoadwayBill rwb = orderMapper.mapToRoadwayBill(rwbDto);
		orderService.updateRoadwayBill(rwb);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "RoadwayBill updated", rwbDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/{id}")
	public ResponseEntity<Object> deleteRoadwayBillById(@PathVariable("id") String rwbId) throws Exception {
		orderService.deleteRoadwayBillById(rwbId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "RoadwayBill deleted", rwbId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb/{id}/status")
	public ResponseEntity<Object> changeRoadwayBillStatusById(@PathVariable("id") String rwbId,
			@RequestParam(name = "status") String status) throws Exception {
		RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
		orderService.updateRwbStatus(rwb, status);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "RoadwayBill deleted", rwbId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/filter")
	public ResponseEntity<Object> filterRoadwayBills(@RequestParam(name = "jobId", required = false) String jobId,
			@RequestParam(name = "rwbId", required = false) String rwbId,
			@RequestParam(name = "start", required = false) String startDate,
			@RequestParam(name = "end", required = false) String endDate,
			@RequestParam(name = "rateStatus", required = false) Boolean status) throws Exception {
		Date start = null;
		Date end = null;
		if (startDate != null)
			start = systemUtil.convertToDate(startDate);
		if (endDate != null)
			end = systemUtil.convertToDate(endDate);
		List<RoadwayBill> rwbs = orderService.filterRoadwayBill(jobId, rwbId, start, end, status);
		List<RoadwayBillDTO> rwbDtos = new ArrayList<>();
		for (RoadwayBill rwb : rwbs) {
			rwbDtos.add(orderMapper.mapToRoadwayBillDTO(rwb));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), rwbDtos.size() + " roadway bills fetched", rwbDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb/stops")
	public ResponseEntity<Object> addStop(@RequestBody StopDTO stopDto) throws Exception {
		Stop stop = orderMapper.mapToStop(stopDto);
		String stopId = orderService.addStop(stop).getStopId();
		stopDto.setStopId(stopId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Stop added", stopDto), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/stops")
	public ResponseEntity<Object> getAllStops() throws Exception {
		List<Stop> stops = orderService.getAllStop();
		List<StopDTO> stopDtos = new ArrayList<>();
		for (Stop stop : stops) {
			stopDtos.add(orderMapper.mapToStopDTO(stop));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), stopDtos.size() + " stops fetched", stopDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/stops/{id}")
	public ResponseEntity<Object> getStopById(@PathVariable("id") String stopId) throws Exception {
		Stop stop = orderService.getStopById(stopId);
		StopDTO stopDto = orderMapper.mapToStopDTO(stop);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Stop fetched", stopDto), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb/stops")
	public ResponseEntity<Object> updateStop(@RequestBody StopDTO stopDto) throws Exception {
		Stop stop = orderMapper.mapToStop(stopDto);
		orderService.updateStop(stop);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Stop updated", stopDto), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/stops/{id}")
	public ResponseEntity<Object> deleteStopById(@PathVariable("id") String stopId) throws Exception {
		orderService.deleteStopById(stopId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Stop deleted", stopId), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/stops/filter")
	public ResponseEntity<Object> filterStops(@RequestParam("rwbId") String rwbId) throws Exception {
		List<Stop> stops = orderService.filterStop(rwbId);
		List<StopDTO> stopDtos = new ArrayList<>();
		for (Stop stop : stops) {
			stopDtos.add(orderMapper.mapToStopDTO(stop));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), stopDtos.size() + " stops fetched", stopDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb/stopProgress")
	public ResponseEntity<Object> addStopProgress(@RequestBody StopProgressDTO stopProgressDto) throws Exception {
		StopProgress stopProgress = orderMapper.mapToStopProgress(stopProgressDto);
		Integer stopProgressId = orderService.addStopProgress(stopProgress).getStopProgressId();
		stopProgressDto.setStopProgressId(stopProgressId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Stop progress added", stopProgressDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/stopPogress")
	public ResponseEntity<Object> getAllStopProgresses() throws Exception {
		List<StopProgress> stopProgresses = orderService.getAllStopProgress();
		List<StopProgressDTO> stopProgressDtos = new ArrayList<>();
		for (StopProgress stopProgress : stopProgresses) {
			stopProgressDtos.add(orderMapper.mapToStopProgressDTO(stopProgress));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				stopProgressDtos.size() + " stop progresses fetched", stopProgressDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/stopProgress/{id}")
	public ResponseEntity<Object> getStopProgressById(@PathVariable("id") Integer stopProgressId) throws Exception {
		StopProgress stopProgress = orderService.getStopProgressById(stopProgressId);
		StopProgressDTO stopProgressDto = orderMapper.mapToStopProgressDTO(stopProgress);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Stop progress fetched", stopProgressDto), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb/stopProgress")
	public ResponseEntity<Object> updateStopProgress(@RequestBody StopProgressDTO stopProgressDto) throws Exception {
		StopProgress stopProgress = orderMapper.mapToStopProgress(stopProgressDto);
		orderService.updateStopProgress(stopProgress);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Stop progress updated", stopProgressDto), HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/stopProgress/{id}")
	public ResponseEntity<Object> deleteStopProgressById(@PathVariable("id") Integer stopProgressId) throws Exception {
		orderService.deleteStopProgressById(stopProgressId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Stop progress deleted", stopProgressId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/stopProgress/filter")
	public ResponseEntity<Object> filterStopProgresses(@RequestParam("rwbId") String rwbId) throws Exception {
		List<StopProgress> stopProgresses = orderService.filterStopProgress(rwbId);
		List<StopProgressDTO> stopProgressDtos = new ArrayList<>();
		for (StopProgress stopProgress : stopProgresses) {
			stopProgressDtos.add(orderMapper.mapToStopProgressDTO(stopProgress));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				stopProgressDtos.size() + " stop progresses fetched", stopProgressDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb/products")
	public ResponseEntity<Object> addProduct(@RequestBody ProductDTO productDto) throws Exception {
		Product product = orderMapper.mapToProduct(productDto);
		Integer productId = orderService.addProduct(product).getProductId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Product added", productId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/products")
	public ResponseEntity<Object> getAllProducts() throws Exception {
		List<Product> products = orderService.getAllProduct();
		List<ProductDTO> productDtos = new ArrayList<>();
		for (Product product : products) {
			productDtos.add(orderMapper.mapToProductDTO(product));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), productDtos.size() + " products fetched", productDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/products/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable("id") Integer productId) throws Exception {
		Product product = orderService.getProductById(productId);
		ProductDTO productDto = orderMapper.mapToProductDTO(product);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Product fetched", productDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb/products")
	public ResponseEntity<Object> updateProduct(@RequestBody ProductDTO productDto) throws Exception {
		Product product = orderMapper.mapToProduct(productDto);
		orderService.updateProduct(product);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Product updated", productDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/products/{id}")
	public ResponseEntity<Object> deleteProductById(@PathVariable("id") Integer productId) throws Exception {
		orderService.deleteProductById(productId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Product deleted", productId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/products/filter")
	public ResponseEntity<Object> filterProducts(@RequestParam("rwbId") String rwbId) throws Exception {
		List<Product> products = orderService.filterProduct(rwbId);
		List<ProductDTO> productDtos = new ArrayList<>();
		for (Product product : products) {
			productDtos.add(orderMapper.mapToProductDTO(product));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), productDtos.size() + " products fetched", productDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb/incomes")
	public ResponseEntity<Object> addIncome(@RequestBody IncomeDTO incomeDto) throws Exception {
		Income income = orderMapper.mapToIncome(incomeDto);
		Integer incomeId = orderService.addIncome(income).getIncomeId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Income added", incomeId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/incomes")
	public ResponseEntity<Object> getAllIncomes() throws Exception {
		List<Income> incomes = orderService.getAllIncomes();
		List<IncomeDTO> incomeDtos = new ArrayList<>();
		for (Income income : incomes) {
			incomeDtos.add(orderMapper.mapToIncomeDTO(income));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), incomeDtos.size() + " incomes fetched", incomeDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/incomes/{id}")
	public ResponseEntity<Object> getIncomeById(@PathVariable("id") Integer incomeId) throws Exception {
		Income income = orderService.getIncomeById(incomeId);
		IncomeDTO incomeDto = orderMapper.mapToIncomeDTO(income);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Income fetched", incomeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb/incomes")
	public ResponseEntity<Object> updateIncome(@RequestBody IncomeDTO incomeDto) throws Exception {
		Income income = orderMapper.mapToIncome(incomeDto);
		orderService.updateIncome(income);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Income updated", incomeDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/incomes/{id}")
	public ResponseEntity<Object> deleteIncomeById(@PathVariable("id") Integer incomeId) throws Exception {
		orderService.deleteIncomeById(incomeId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Income deleted", incomeId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/incomes/filter")
	public ResponseEntity<Object> filterIncomes(@RequestParam("rwbId") String rwbId) throws Exception {
		List<Income> incomes = orderService.filterIncome(rwbId);
		List<IncomeDTO> incomeDtos = new ArrayList<>();
		for (Income income : incomes) {
			incomeDtos.add(orderMapper.mapToIncomeDTO(income));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), incomeDtos.size() + " income fetched", incomeDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb/expenses")
	public ResponseEntity<Object> addExpense(@RequestBody ExpenseDTO expenseDto) throws Exception {
		Expense expense = orderMapper.mapToExpense(expenseDto);
		Integer expenseId = orderService.addExpense(expense).getExpenseId();
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Expense added", expenseId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/expenses")
	public ResponseEntity<Object> getAllExpenses() throws Exception {
		List<Expense> expenses = orderService.getAllExpenses();
		List<ExpenseDTO> expenseDtos = new ArrayList<>();
		for (Expense expense : expenses) {
			expenseDtos.add(orderMapper.mapToExpenseDTO(expense));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), expenseDtos.size() + " expenses fetched", expenseDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/expenses/{id}")
	public ResponseEntity<Object> getExpenseById(@PathVariable("id") Integer expenseId) throws Exception {
		Expense expense = orderService.getExpenseById(expenseId);
		ExpenseDTO expenseDto = orderMapper.mapToExpenseDTO(expense);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Expense fetched", expenseDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/rwb/expenses")
	public ResponseEntity<Object> updateExpense(@RequestBody ExpenseDTO expenseDto) throws Exception {
		Expense expense = orderMapper.mapToExpense(expenseDto);
		orderService.updateExpense(expense);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Expense updated", expenseDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/expenses/{id}")
	public ResponseEntity<Object> deleteExpenseById(@PathVariable("id") Integer expenseId) throws Exception {
		orderService.deleteExpenseById(expenseId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Expense deleted", expenseId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/rwb/expenses/filter")
	public ResponseEntity<Object> filterExpenses(@RequestParam("rwbId") String rwbId) throws Exception {
		List<Expense> expenses = orderService.filterExpense(rwbId);
		List<ExpenseDTO> expenseDtos = new ArrayList<>();
		for (Expense expense : expenses) {
			expenseDtos.add(orderMapper.mapToExpenseDTO(expense));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), expenseDtos.size() + " expense fetched", expenseDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/job/fuel")
	public ResponseEntity<Object> addJobFuelDetail(@RequestBody JobFuelDetailDTO fuelDto) throws Exception {
		JobFuelDetail fuel = orderMapper.mapToJobFuelDetail(fuelDto);
		fuel = orderService.addJobFuelDetail(fuel);
		fuelDto = orderMapper.mapToJobFuelDetailDTO(fuel);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job Fuel Detail added", fuelDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/job/fuel")
	public ResponseEntity<Object> getAllJobFuelDetails() throws Exception {
		List<JobFuelDetail> fuels = orderService.getAllJobFuelDetails();
		List<JobFuelDetailDTO> fuelDtos = new ArrayList<>();
		for (JobFuelDetail fuel : fuels) {
			fuelDtos.add(orderMapper.mapToJobFuelDetailDTO(fuel));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), fuelDtos.size() + " fuel fetched", fuelDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/job/fuel/{id}")
	public ResponseEntity<Object> getJobFuelDetailById(@PathVariable("id") Integer fuelId) throws Exception {
		JobFuelDetail fuel = orderService.getJobFuelDetailById(fuelId);
		JobFuelDetailDTO fuelDto = orderMapper.mapToJobFuelDetailDTO(fuel);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job Fuel Detail fetched", fuelDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/job/fuel")
	public ResponseEntity<Object> updateJobFuelDetail(@RequestBody JobFuelDetailDTO fuelDto) throws Exception {
		JobFuelDetail fuel = orderMapper.mapToJobFuelDetail(fuelDto);
		fuel = orderService.updateJobFuelDetail(fuel);
		fuelDto = orderMapper.mapToJobFuelDetailDTO(fuel);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job Fuel Detail updated", fuelDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/job/fuel/{id}")
	public ResponseEntity<Object> deleteJobFuelDetailById(@PathVariable("id") Integer fuelId) throws Exception {
		orderService.deleteJobFuelDetailById(fuelId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Job Fuel Detail deleted", fuelId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/job/fuel/filter")
	public ResponseEntity<Object> filterJobFuelDetails(@RequestParam("jobId") String jobId) throws Exception {

		List<JobFuelDetail> fuels = orderService.filterJobFuelDetail(jobId);
		List<JobFuelDetailDTO> fuelDtos = new ArrayList<>();
		for (JobFuelDetail fuel : fuels) {
			fuelDtos.add(orderMapper.mapToJobFuelDetailDTO(fuel));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), fuelDtos.size() + " fuel fetched", fuelDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/rwb/documents/pod")
	public ResponseEntity<Object> uploadRoadwayBillPOD(@RequestParam(value = "file") MultipartFile file,
			@RequestParam("rwbId") String rwbId) throws IOException, ParseException {
		boolean success = orderService.uploadRoadwayBillPOD(file, rwbId);
		if (!success)
			throw new IllegalArgumentException("Proof of delivery upload failed!");
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Proof of delivery uploaded", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/rwb/documents/pod")
	public ResponseEntity<Object> deleteRoadwayBillPOD(@RequestParam("rwbId") String rwbId) {
		orderService.deleteRoadwayBillPOD(rwbId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Proof of delivery deleted", null),
				HttpStatus.OK);
	}
}
