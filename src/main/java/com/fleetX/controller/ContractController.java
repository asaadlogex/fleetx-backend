package com.fleetX.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.CompanyContractDTO;
import com.fleetX.dto.CompanyContractSummaryDTO;
import com.fleetX.dto.FuelSupplierContractDTO;
import com.fleetX.dto.RouteDTO;
import com.fleetX.dto.RouteRateContractDTO;
import com.fleetX.dto.RouteRateDTO;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.Route;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.mapper.CompanyMapper;
import com.fleetX.model.SuccessResponse;
import com.fleetX.service.IContractService;

@RestController
@RequestMapping(value = SystemType.API_PREFIX
		+ "contracts"
//		, consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE
		)
public class ContractController {

	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	IContractService contractService;
	@Autowired
	SystemUtil systemUtil;

	@CrossOrigin
	@PostMapping("/companies")
	public ResponseEntity<Object> addCompanyContract(@RequestBody CompanyContractDTO companyContractDto)
			throws ParseException {
		CompanyContract companyContract = companyMapper.mapToCompanyContract(companyContractDto);
		String companyContractId = contractService.addCompanyContract(companyContract).getCompanyContractId();
		companyContractDto.setCompanyContractId(companyContractId);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Company contract added", companyContractDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/{id}")
	public ResponseEntity<Object> getCompanyContractById(@PathVariable("id") String companyContractId)
			throws ParseException {
		CompanyContract companyContract = contractService.findCompanyContractById(companyContractId);
		CompanyContractDTO companyContractDto = companyMapper.mapToCompanyContractDTO(companyContract);

		List<RouteRateContractDTO> routeRateContractDtos = companyContractDto.getRouteRateContracts();
		for (RouteRateContractDTO routeRateContractDTO : routeRateContractDtos) {
			List<RouteRateDTO> routeRateDtos = routeRateContractDTO.getRouteRates();
			if (routeRateDtos.size() > 2)
				routeRateContractDTO.setRouteRates(routeRateDtos.subList(0, 2));
		}

		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Company contract fetched", companyContractDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/filter")
	public ResponseEntity<Object> filterCompanyContract(
			@RequestParam(name = "customerId", required = false) String customerId,
			@RequestParam(name = "acId", required = false) String acId,
			@RequestParam(name = "contractType", required = false) String contractType,
			@RequestParam(name = "status", required = false) String status) throws ParseException {
		List<CompanyContract> companyContracts = contractService.filterCompanyContract(customerId, acId, contractType,
				status);
		List<CompanyContractSummaryDTO> companyContractDtos = new ArrayList<>();
		for (CompanyContract companyContract : companyContracts) {
			companyContractDtos.add(companyMapper.mapToCompanyContractSummaryDTO(companyContract));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				companyContractDtos.size() + " company contracts fetched", companyContractDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/companies")
	public ResponseEntity<Object> updateCompanyContract(@RequestBody CompanyContractDTO companyContractDto)
			throws ParseException {
		CompanyContract companyContract = companyMapper.mapToCompanyContract(companyContractDto);
		contractService.updateCompanyContract(companyContract);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Company contract updated", companyContractDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/companies/{id}")
	public ResponseEntity<Object> deleteCompanyContractById(@PathVariable("id") String companyContractId) {
		contractService.deleteCompanyContractById(companyContractId);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Company contract deleted", companyContractId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/companies/{id}/rrc")
	public ResponseEntity<Object> addRouteRateContract(@PathVariable("id") String companyContractId,
			@RequestBody RouteRateContractDTO rrcDto) throws ParseException {
		CompanyContract companyContract = contractService.findCompanyContractById(companyContractId);
		RouteRateContract rrc = companyMapper.mapToRouteRateContract(rrcDto);
		rrc.setCompanyContract(companyContract);
		rrc = contractService.addRouteRateContract(rrc);
		rrcDto.setRouteRateContractId(rrc.getRouteRateContractId());
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route rate contract added", rrcDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/companies/{id}/rrc")
	public ResponseEntity<Object> updateRouteRateContract(@PathVariable("id") String companyContractId,
			@RequestBody RouteRateContractDTO rrcDto) throws ParseException {
		CompanyContract companyContract = contractService.findCompanyContractById(companyContractId);
		RouteRateContract rrc = companyMapper.mapToRouteRateContract(rrcDto);
		rrc.setCompanyContract(companyContract);
		contractService.updateRouteRateContract(rrc);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route rate contract updated", rrcDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/rrc/{rrcId}")
	public ResponseEntity<Object> getRouteRateContract(@PathVariable("rrcId") Integer rrcId) throws ParseException {
		RouteRateContract rrc = contractService.getRouteRateContractById(rrcId);
		RouteRateContractDTO rrcDto = companyMapper.mapToRouteRateContractDTO(rrc);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route rate contract fetched", rrcDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/companies/rrc/{rrcId}")
	public ResponseEntity<Object> deleteRouteRateContract(@PathVariable("rrcId") Integer rrcId) throws ParseException {
		contractService.deleteRouteRateContractById(rrcId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route rate contract deleted", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/companies/rrc/rates/{id}")
	public ResponseEntity<Object> deleteRouteRate(@PathVariable("id") Integer routeRateId) throws ParseException {
		contractService.deleteRouteRateById(routeRateId);
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Route rate deleted", null),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies")
	public ResponseEntity<Object> getAllCompanyContract() throws ParseException {
		List<CompanyContract> companyContracts = contractService.getAllCompanyContract();
		List<CompanyContractSummaryDTO> companyContractDtos = new ArrayList<>();
		for (CompanyContract companyContract : companyContracts) {
			CompanyContractSummaryDTO companyContractDto = companyMapper
					.mapToCompanyContractSummaryDTO(companyContract);
			companyContractDtos.add(companyContractDto);
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				companyContractDtos.size() + " company contracts fetched", companyContractDtos), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/routes")
	public ResponseEntity<Object> getRoutesByCompanyId(@RequestParam(name = "customerId") String customerId)
			throws ParseException {
		List<CompanyContract> companyContracts = contractService.filterCompanyContract(customerId, null, null, null);
		List<Route> routes = new ArrayList<>();
		for (CompanyContract companyContract : companyContracts) {
			for (RouteRateContract rrc : companyContract.getRouteRateContracts()) {
				routes.add(rrc.getRoute());
			}
		}
		List<RouteDTO> routeDtos = new ArrayList<>();
		for (Route route : routes) {
			routeDtos.add(companyMapper.mapToRouteDTO(route));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), routeDtos.size() + " routes fetched", routeDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/companies/routes/rates")
	public ResponseEntity<Object> getRouteRatesByCompanyId(@RequestParam(name = "customerId") String customerId,
			@RequestParam(name = "routeId") String routeId,
			@RequestParam(name = "start", required = false) String start,
			@RequestParam(name = "end", required = false) String end) throws ParseException {
		List<CompanyContract> companyContracts = contractService.filterCompanyContract(customerId, null, null,
				SystemType.STATUS_ACTIVE);
		if (companyContracts == null || companyContracts.size() == 0)
			throw new NoSuchElementException("Company contract: " + SystemType.NO_SUCH_ELEMENT);
		List<RouteRate> routeRates = new ArrayList<>();
		Date startDate = systemUtil.convertToDate(start);
		Date endDate = systemUtil.convertToDate(end);

		for (CompanyContract companyContract : companyContracts) {
			for (RouteRateContract rrc : companyContract.getRouteRateContracts()) {
				if (rrc.getRoute().getRouteId().equals(routeId)) {
					for (RouteRate routeRate : rrc.getRouteRates()) {
						if (systemUtil.isStartBeforeEnd(startDate, routeRate.getEffectiveFrom())
								&& systemUtil.isStartBeforeEnd(routeRate.getEffectiveTill(), endDate)
								&& !routeRates.contains(routeRate)) {
							routeRates.add(routeRate);
						}
					}
				}
			}
		}
		List<RouteRateDTO> routeRateDtos = new ArrayList<>();
		for (RouteRate routeRate : routeRates) {
			routeRateDtos.add(companyMapper.mapToRouteRateDTO(routeRate));
		}
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(),
				routeRateDtos.size() + " route rates fetched", routeRateDtos), HttpStatus.OK);
	}

	// FUEL SUPPLIER CONTRACT
	@CrossOrigin
	@PostMapping("/fuelRates")
	public ResponseEntity<Object> addFuelSupplierContract(@RequestBody FuelSupplierContractDTO fuelSupplierContractDto)
			throws ParseException {
		FuelSupplierContract fuelSupplierContract = companyMapper.mapToFuelSupplierContract(fuelSupplierContractDto);
		String fuelSupplierContractId = contractService.addFuelSupplierContract(fuelSupplierContract).getFscId();
		fuelSupplierContractDto.setFscId(fuelSupplierContractId);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Fuel supplier contract added", fuelSupplierContractDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/fuelRates/{id}")
	public ResponseEntity<Object> getFuelSupplierContractById(@PathVariable("id") String fuelSupplierContractId)
			throws ParseException {
		FuelSupplierContract fuelSupplierContract = contractService
				.findFuelSupplierContractById(fuelSupplierContractId);
		FuelSupplierContractDTO fuelSupplierContractDto = companyMapper
				.mapToFuelSupplierContractDTO(fuelSupplierContract);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Fuel supplier contract fetched", fuelSupplierContractDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/fuelRates")
	public ResponseEntity<Object> getAllFuelSupplierContract() throws ParseException {
		List<FuelSupplierContract> fuelSupplierContracts = contractService.getAllFuelSupplierContract();
		List<FuelSupplierContractDTO> fuelSupplierContractDtos = new ArrayList<>();
		for (FuelSupplierContract fuelSupplierContract : fuelSupplierContracts) {
			fuelSupplierContractDtos.add(companyMapper.mapToFuelSupplierContractDTO(fuelSupplierContract));
		}
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(),
						fuelSupplierContractDtos.size() + " fuel supplier contracts fetched", fuelSupplierContractDtos),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/fuelRates/filter")
	public ResponseEntity<Object> filterFuelSupplierContract(
			@RequestParam(name = "status", required = false) String status) throws ParseException {
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), " company contracts fetched", ""),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/fuelRates")
	public ResponseEntity<Object> updateFuelSupplierContract(
			@RequestBody FuelSupplierContractDTO fuelSupplierContractDto) throws ParseException {
		FuelSupplierContract fuelSupplierContract = companyMapper.mapToFuelSupplierContract(fuelSupplierContractDto);
		contractService.updateFuelSupplierContract(fuelSupplierContract);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Fuel supplier contract updated", fuelSupplierContractDto),
				HttpStatus.OK);
	}

	@CrossOrigin
	@DeleteMapping("/fuelRates/{id}")
	public ResponseEntity<Object> deleteFuelSupplierContractById(@PathVariable("id") String fuelSupplierContractId) {
		contractService.deleteFuelSupplierContractById(fuelSupplierContractId);
		return new ResponseEntity<>(
				new SuccessResponse(HttpStatus.OK.value(), "Fuel supplier contract deleted", fuelSupplierContractId),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/companies/documents")
	public ResponseEntity<Object> uploadContractDoc(@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam("contractId") String contractId) throws IOException, ParseException {
		boolean success = contractService.uploadContractDoc(file, contractId);
		if (!success)
			throw new IllegalArgumentException("Contract document upload failed!");
		return new ResponseEntity<>(new SuccessResponse(HttpStatus.OK.value(), "Contract document uploaded", null),
				HttpStatus.OK);
	}

}
