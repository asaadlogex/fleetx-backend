package com.fleetX.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;

public interface IContractService {	
	public CompanyContract addCompanyContract(CompanyContract companyContract) throws ParseException;
	public List<CompanyContract> getAllCompanyContract();
	public List<CompanyContract> filterCompanyContract(String cbId,String acId,String contractType,String status);
	public CompanyContract updateCompanyContract(CompanyContract companyContract) throws ParseException;
	public void deleteCompanyContractById(String companyContractId);
	public CompanyContract findCompanyContractById(String companyContractId);
	public List<CompanyContract> findCompanyContractByType(DContractType contractType);
	
	public FuelSupplierContract addFuelSupplierContract(FuelSupplierContract fuelSupplierContract);
	public List<FuelSupplierContract> getAllFuelSupplierContract();
	public List<FuelSupplierContract> filterFuelSupplierContract(Example<FuelSupplierContract> fuelSupplierContractExample);
	public FuelSupplierContract updateFuelSupplierContract(FuelSupplierContract fuelSupplierContract);
	public void deleteFuelSupplierContractById(String fuelSupplierContractId);
	public FuelSupplierContract findFuelSupplierContractById(String fuelSupplierContractId);
	
	public RouteRateContract addRouteRateContract(RouteRateContract rrc) throws ParseException;
	public RouteRateContract updateRouteRateContract(RouteRateContract rrc) throws ParseException;
	public void deleteRouteRateContractById(Integer rrcId);
	public RouteRateContract getRouteRateContractById(Integer rrcId);

	public void deleteRouteRateById(Integer routeRateId);
	
	public boolean uploadContractDoc(MultipartFile file, String contractId);
	
	public CompanyContract getCustomerContractType(Company customer, Date date);
	public CompanyContract getCustomerContractByDate(String customerId,Date date);
	
	public EmployeeContract findActiveEmployeeContract(Employee employee, Date date);
}
