package com.fleetX.service;

import java.text.ParseException;
import java.util.List;

import com.fleetX.dto.DashboardData;
import com.fleetX.dto.TripDetailDTO;

public interface ISummaryService {
	public TripDetailDTO getTripDetail(String jobId) throws ParseException;
	public List<String> getAllIncomeType();
	public List<String> getAllExpenseType();
	
	public DashboardData getDashboardData();
}
