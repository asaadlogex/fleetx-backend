package com.fleetX.service;

import java.util.Date;
import java.util.List;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.Route;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.order.Warehouse;

public interface ICompanyService {
	
	public List<Company> findAllCompany();
	public Company findCompanyById(String companyId);
	public List<Company> findCompanyByContractType(String contractType);
	public List<Company> filterCompanies(String businessType,String status, String supplierType,Boolean haveContract);
	public List<Company> findCompanyByType(String type);
	public Company addCompany(Company company) throws Exception;	
	public Company updateCompany(Company company) throws Exception;
	public void deleteCompany(String companyId) throws Exception;
	
	public Warehouse addWarehouse(Warehouse warehouse);
	public Warehouse updateWarehouse(Warehouse warehouse);
	public List<Warehouse> getAllWarehouse();
	public Warehouse getWarehouseById(Integer warehouseId);
	public void deleteWarehouseById(Integer warehouseId);
	public List<Warehouse> filterWarehouse(String referenceId);
	
	public Double getFuelRateByDate(Company supplier, Date fuelDate);
	
	public Double getRouteRate(Company customer, Route route,
			DTrailerType trailerType, DTrailerSize trailerSize, Date rwbDate);
	public Double getRouteRatePerTon(Company customer, Route route, DTrailerType trailerType, DTrailerSize trailerSize,
			Date rwbDate);
	public Double getRouteRatePerKm(Company customer, Route route, DTrailerType trailerType, DTrailerSize trailerSize,
			Date rwbDate);
}
 