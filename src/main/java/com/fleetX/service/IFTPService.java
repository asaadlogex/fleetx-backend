package com.fleetX.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface IFTPService {
	public String uploadFile(String path,MultipartFile file, String filename) throws IOException;
	public byte[] getFile(String path) throws IOException;
	public boolean deleteFile(String path);
}
