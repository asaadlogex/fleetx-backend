package com.fleetX.service;

import java.util.List;

import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;

public interface IAssetService {	
	
	public AssetCombination addAssetCombination(AssetCombination assetCombination);
	public AssetCombination findAssetCombinationById(String assetCombinationId);
	public List<AssetCombination> findAllAssetCombination();
	public List<AssetCombination> filterAssetCombination(String availability,String jobAvailability);
	public AssetCombination updateAssetCombination(AssetCombination assetCombination);
	public void deleteAssetCombinationById(String assetCombinationId);
	
	public Container addContainer(Container Container);
	public Container findContainerById(String containerId);
	public List<Container> findAllContainers();
	public List<Container> filterContainer(String movingStatus, String rwbId,String avail);
	public Container updateContainer(Container container);
	public void deleteContainer(String containerId);

	public Tracker addTracker(Tracker tracker);
	public Tracker findTrackerById(String trackerId);
	public List<Tracker> findAllTrackers();
	public List<Tracker> filterTracker(String movingStatus, String rwbId,String avail);
	public Tracker updateTracker(Tracker tracker);
	public void deleteTracker(String trackerId);

	public Tractor addTractor(Tractor tractor);
	public List<Tractor> addAllTractors(List<Tractor> tractors);
	public Tractor findTractorById(String tractorId);
	public List<Tractor> findAllTractors();
	public List<Tractor> filterTractor(String movingStatus, String rwbId,String avail, String vinNumber, String engineNumber, String numberPlate);
	public Tractor updateTractor(Tractor tractor);
	public void deleteTractor(String tractorId);

	public Trailer addTrailer(Trailer trailer);
	public List<Trailer> addAllTrailers(List<Trailer> trailers);
	public Trailer findTrailerById(String trailerId);
	public List<Trailer> findAllTrailers();
	public List<Trailer> filterTrailer(String movingStatus, String rwbId,String avail, String vinNumber);
	public Trailer updateTrailer(Trailer trailer);
	public void deleteTrailer(String trailerId);

	public Tyre addTyre(Tyre tyre,Asset asset);
	public List<Tyre> addAllTyre(List<Tyre> tyres);
	public Tyre findTyreById(String tyreId);
	public List<Tyre> findAllTyres();
	public List<Tyre> filterTyre(String movingStatus, String avail);
	public Tyre updateTyre(Tyre tyre);
	public void deleteTyre(String tyreId);
	
	public AssetCombination updateStatusAssetCombination(String acId,String availabilityNew,String jobAvailabilityNew);
	public Tractor updateStatusTractor(String tractorId,String availabilityNew,String movingStatusNew);
	public Trailer updateStatusTrailer(String trailerId,String availabilityNew,String movingStatusNew);
	public Container updateStatusContainer(String containerId,String availabilityNew,String movingStatusNew);
	public Tracker updateStatusTracker(String trackerId,String availabilityNew,String movingStatusNew);
	
	public Tractor updateTotalKmTractor(String tractorId,Double kmToAdd);
	public Tyre updateTotalKmTyre(String tyreId,Double kmToAdd);
	
	public Tractor findTractorByNumber(String tractorNumber);
	public Asset findAssetByVin(String vin);
	
	public long countTractorByStatus(String status);
	public long countTrailerByStatus(String status);
	public long countContainerByStatus(String status);
	public long countTrackerByStatus(String status);
}
