package com.fleetX.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;

public interface IEmployeeService {
	public List<Employee> findAllEmployee();
	public Employee findEmployeeById(String employeeId);
	public List<Employee> filterEmployees(String position,String movingStatus, String rwbId,String avail, String firstName, String lastName, String employeeNumber, String cnic, String licenseNumber);
	public Employee addEmployee(Employee employee) throws Exception;	
	public List<Employee> addAllEmployees(List<Employee> employees) throws Exception;
	public Employee updateEmployee(Employee employee) throws Exception;
	public void deleteEmployee(String employeeId) throws Exception;
	
	public TrainingProfile addTrainingProfile(TrainingProfile trainingProfile,MultipartFile certificate,MultipartFile attendanceSheet,MultipartFile picWithTrainer) throws IOException;
	public MedicalTestReport addMedicalTestReport(MedicalTestReport medicalTestReport,MultipartFile file) throws IOException;
	public HeavyVehicleExperience addHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience);
	public Reference addReference(Reference reference,MultipartFile cnic, MultipartFile referenceForm) throws IOException;
	public String addProfilePicture(String employeeId, MultipartFile dp) throws IOException;
	
	public TrainingProfile updateTrainingProfile(TrainingProfile trainingProfile,MultipartFile certificate,MultipartFile attendanceSheet,MultipartFile picWithTrainer) throws IOException;
	public MedicalTestReport updateMedicalTestReport(MedicalTestReport medicalTestReport,MultipartFile file) throws IOException;
	public HeavyVehicleExperience updateHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience);
	public Reference updateReference(Reference reference,MultipartFile cnic, MultipartFile referenceForm) throws IOException;
	
	public List<TrainingProfile> getAllTrainingProfile(String employeeId);
	public List<MedicalTestReport> getAllMedicalTestReport(String employeeId);
	public List<HeavyVehicleExperience> getAllHeavyVehicleExperience(String employeeId);
	public List<Reference> getAllReference(String employeeId);
	
	public void deleteTrainingProfile(String tpId);
	public void deleteMedicalTestReport(String mtrId);
	public void deleteHeavyVehicleExperience(String hveId);
	public void deleteReference(String refId);
	
	public Employee updateStatusEmployee(String employeeId,String availabilityNew,String movingStatusNew);
	
	public long countEmployeeByPositionAndStatus(DPosition position,String status);


}
