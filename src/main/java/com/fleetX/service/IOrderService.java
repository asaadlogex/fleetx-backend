package com.fleetX.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;

public interface IOrderService {
	public Job createJob(Job job);
	public Job updateJob(Job job);
	public List<Job> getAllJob();
	public Job getJobById(String jobId);
	public List<Job> filterJob(String jobId,String rwbId, String tractorNumber, Date start, Date end,String status,String reimbursementStatus,String baseStation,Boolean isRental);
	public void deleteJobById(String jobId);
	public List<Job> getClosedJobInRange(Date start,Date end);
	
	public RoadwayBill createRoadwayBill(RoadwayBill roadwayBill);
	public RoadwayBill updateRoadwayBill(RoadwayBill roadwayBill);
	public List<RoadwayBill> getAllRoadwayBill();
	public RoadwayBill getRoadwayBillById(String rwbId);
	public List<RoadwayBill> filterRoadwayBill(String jobId, String rwbId, Date start, Date end, Boolean rateStatus);
	public List<RoadwayBill> filterRoadwayBill(Date start, Date end, String customerId,String taxState,String invoiceStatus, String invoiceType);
	public void deleteRoadwayBillById(String rwbId);
	
	public Stop addStop(Stop stop);
	public Stop updateStop(Stop stop);
	public List<Stop> getAllStop();
	public Stop getStopById(String stopId);
	public List<Stop> filterStop(String rwbId);
	public void deleteStopById(String stopId);
	
	public StopProgress addStopProgress(StopProgress stopProgress);
	public StopProgress updateStopProgress(StopProgress stopProgress);
	public List<StopProgress> getAllStopProgress();
	public StopProgress getStopProgressById(Integer stopProgressId);
	public List<StopProgress> filterStopProgress(String rwbId);
	public void deleteStopProgressById(Integer stopProgressId);
	
	public Product addProduct(Product product);
	public Product updateProduct(Product product);
	public List<Product> getAllProduct();
	public Product getProductById(Integer productId);
	public List<Product> filterProduct(String rwbId);
	public void deleteProductById(Integer productId);
	
	public Income addIncome(Income income);
	public Income updateIncome(Income income);
	public List<Income> getAllIncomes();
	public Income getIncomeById(Integer incomeId);
	public List<Income> filterIncome(String rwbId);
	public void deleteIncomeById(Integer incomeId);
	
	public Expense addExpense(Expense expense);
	public Expense updateExpense(Expense expense);
	public List<Expense> getAllExpenses();
	public Expense getExpenseById(Integer expenseId);
	public List<Expense> filterExpense(String rwbId);
	public void deleteExpenseById(Integer expenseId);
	
	public JobFuelDetail addJobFuelDetail(JobFuelDetail jobFuelDetail);
	public JobFuelDetail updateJobFuelDetail(JobFuelDetail jobFuelDetail);
	public List<JobFuelDetail> getAllJobFuelDetails();
	public JobFuelDetail getJobFuelDetailById(Integer jobFuelDetailId);
	public List<JobFuelDetail> filterJobFuelDetail(String jobId);
	public void deleteJobFuelDetailById(Integer jobFuelDetailId);
	
	public Job getJobByIdAndStatus(String jobId, String statusJob);
	public List<String> getJobIdsInRange(Date start, Date end);
	
	public boolean uploadRoadwayBillPOD(MultipartFile file, String rwbId) throws IOException;
	public void deleteRoadwayBillPOD(String rwbId);
	
	public List<Tractor> getTractorsByJob(String jobId);
	public List<Trailer> getTrailersByJob(String jobId);
	
	void updateReimbursementStatus(List<String> jobIds, String status);
	
	Double getReimbursementExpenseOfRoadwayBill(RoadwayBill rwb);
	
	public void updateInvoiceStatus(RoadwayBill rwb, String invoiceStatus);
	public void updateRwbStatus(RoadwayBill rwb, String rwbStatus);
	
	List<StopProgress> getStopProgressesByType(RoadwayBill rwb, String stopType);
	
	public long countJobByStatus(DJobStatus status);
	public long countRwbByStatus(DRWBStatus status);
	
	public Double getExpenseOfRoadwayBill(RoadwayBill rwb);
	public Double getIncomeOfRoadwayBill(RoadwayBill rwb);
	
	public Double getIncomeOfJob(Job job);
	public Double getExpenseOfJob(Job job);
	public Double getFuelExpenseOfJob(Job job);
	
}
