package com.fleetX.service;

import java.util.Date;
import java.util.function.Function;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import io.jsonwebtoken.Claims;

public interface IJWTService {
	public String getUsernameFromToken(String token) throws Exception;
	public Date getExpirationDateFromToken(String token) throws Exception;
	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) throws Exception;
	public String generateToken(Authentication authentication);
	public Boolean validateToken(String token, UserDetails userDetails) throws Exception;
	public UsernamePasswordAuthenticationToken getAuthentication(final String token, final Authentication existingAuth, final UserDetails userDetails);
}
