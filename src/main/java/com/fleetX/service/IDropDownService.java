package com.fleetX.service;

import java.util.List;

import com.fleetX.entity.dropdown.DAssetType;
import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DBrand;
import com.fleetX.entity.dropdown.DBusinessType;
import com.fleetX.entity.dropdown.DCategory;
import com.fleetX.entity.dropdown.DChannel;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DCompanyDocumentType;
import com.fleetX.entity.dropdown.DCompanyType;
import com.fleetX.entity.dropdown.DContainerType;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.dropdown.DCustomerType;
import com.fleetX.entity.dropdown.DDepartment;
import com.fleetX.entity.dropdown.DEmployeeDocument;
import com.fleetX.entity.dropdown.DEmployeeType;
import com.fleetX.entity.dropdown.DExpenseType;
import com.fleetX.entity.dropdown.DFormation;
import com.fleetX.entity.dropdown.DFuelTankType;
import com.fleetX.entity.dropdown.DIncomeType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DItemCategory;
import com.fleetX.entity.dropdown.DItemType;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DLoadStatus;
import com.fleetX.entity.dropdown.DMake;
import com.fleetX.entity.dropdown.DPaymentMode;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.dropdown.DPriority;
import com.fleetX.entity.dropdown.DPrivilege;
import com.fleetX.entity.dropdown.DPurchaseOrderStatus;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.dropdown.DReimbursementStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.dropdown.DStopProgressType;
import com.fleetX.entity.dropdown.DStopType;
import com.fleetX.entity.dropdown.DSupplierType;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.dropdown.DUnit;
import com.fleetX.entity.dropdown.DVehicleOwnership;
import com.fleetX.entity.dropdown.DWorkOrderCategory;
import com.fleetX.entity.dropdown.DWorkOrderStatus;
import com.fleetX.entity.dropdown.DWorkOrderSubCategory;

public interface IDropDownService {
	public void createDAssetType(List<String> assetType);
	public List<DAssetType> retrieveDAssetType();
	public void updateDAssetType(DAssetType assetType);

	public void createDBusinessType(List<String> businessType);
	public List<DBusinessType> retrieveDBusinessType();
	public void updateDBusinessType(DBusinessType businessType);
	
	public void createDChannel(List<String> channel);
	public List<DChannel> retrieveDChannel();
	public void updateDChannel(DChannel channel);

	public void createDCompanyDocumentType(List<String> documentType);
	public List<DCompanyDocumentType> retrieveDCompanyDocumentType();
	public void updateDCompanyDocumentType(DCompanyDocumentType documentType);
	
	public void createDDepartment(List<String> department);
	public List<DDepartment> retrieveDDepartment();
	public void updateDDepartment(DDepartment department);
	
	public void createDEmployeeDocument(List<String> employeeDocument);
	public List<DEmployeeDocument> retrieveDEmployeeDocument();
	public void updateDEmployeeDocument(DEmployeeDocument employeeDocument);
	
	public void createDLeaseType(List<String> leaseType);
	public List<DLeaseType> retrieveDLeaseType();
	public void updateDLeaseType(DLeaseType leaseType);
	
	public void createDMake(List<String> make);
	public List<DMake> retrieveDMake();
	public void updateDMake(DMake make);
	
	public void createDPosition(List<String> position);
	public List<DPosition> retrieveDPosition();
	public void updateDPosition(DPosition position);
	
	public void createDPrivilege(List<String> privilege);
	public List<DPrivilege> retrieveDPrivilege();
	public void updateDPrivilege(DPrivilege privilege);
	
	public void createDTrailerSize(List<String> trailerSize);
	public List<DTrailerSize> retrieveDTrailerSize();
	public void updateDTrailerSize(DTrailerSize trailerSize);
	
	public void createDTrailerType(List<String> trailerType);
	public List<DTrailerType> retrieveDTrailerType();
	public void updateDTrailerType(DTrailerType trailerType);
	
	public void createDFormation(List<String> formationType);
	public List<DFormation> retrieveDFormation();
	public void updateDFormation(DFormation formationType);
	
	public void createDSupplierType(List<String> supplierType);
	public List<DSupplierType> retrieveDSupplierType();
	public void updateDSupplierType(DSupplierType supplierType);
	
	public void createDEmployeeType(List<String> employeeType);
	public List<DEmployeeType> retrieveDEmployeeType();
	public void updateDEmployeeType(DEmployeeType employeeType);

	public void createDContainerType(List<String> containerType);
	public List<DContainerType> retrieveDContainerType();
	public void updateDContainerType(DContainerType containerType);
	
	public void createDContractType(List<String> contractType);
	public List<DContractType> retrieveDContractType();
	public void updateDContractType(DContractType contractType);
	
	public void createDCity(List<String> city);
	public List<DCity> retrieveDCity();
	public void updateDCity(DCity city);
	
	public void createDBaseStation(List<String> baseStation);
	public List<DBaseStation> retrieveDBaseStation();
	public void updateDBaseStation(DBaseStation baseStation);

	public void createDJobStatus(List<String> jobStatus);
	public List<DJobStatus> retrieveDJobStatus();
	public void updateDJobStatus(DJobStatus jobStatus);
	
	public void createDVehicleOwnership(List<String> vehicleOwnershipType);
	public List<DVehicleOwnership> retrieveDVehicleOwnership();
	public void updateDVehicleOwnership(DVehicleOwnership vehicleOwnershipType);
	
	public void createDRWBStatus(List<String> rwbStatus);
	public List<DRWBStatus> retrieveDRWBStatus();
	public void updateDRWBStatus(DRWBStatus rwbStatus);
	
	public void createDCategory(List<String> category);
	public List<DCategory> retrieveDCategory();
	public void updateDCategory(DCategory category);
	
	public void createDStopType(List<String> stopType);
	public List<DStopType> retrieveDStopType();
	public void updateDStopType(DStopType stopType);
	
	public void createDFuelTankType(List<String> fuelTankType);
	public List<DFuelTankType> retrieveDFuelTankType();
	public void updateDFuelTankType(DFuelTankType fuelTankType);
	
	public void createDPaymentMode(List<String> paymentMode);
	public List<DPaymentMode> retrieveDPaymentMode();
	public void updateDPaymentMode(DPaymentMode paymentMode);
	
	public void createDLoadStatus(List<String> loadStatus);
	public List<DLoadStatus> retrieveDLoadStatus();
	public void updateDLoadStatus(DLoadStatus loadStatus);
	
	public void createDCustomerType(List<String> customerType);
	public List<DCustomerType> retrieveDCustomerType();
	public void updateDCustomerType(DCustomerType customerType);
	
	public void createDIncomeType(List<String> incomeType);
	public List<DIncomeType> retrieveDIncomeType();
	public void updateDIncomeType(DIncomeType incomeType);
	
	public void createDExpenseType(List<String> expenseType);
	public List<DExpenseType> retrieveDExpenseType();
	public void updateDExpenseType(DExpenseType expenseType);
	
	public void createDCompanyType(List<String> companyType);
	public List<DCompanyType> retrieveDCompanyType();
	public void updateDCompanyType(DCompanyType companyType);
	
	public void createDState(List<String> state);
	public List<DState> retrieveDState();
	public void updateDState(DState state);
	
	public void createDStopProgressType(List<String> stopProgressType);
	public List<DStopProgressType> retrieveDStopProgressType();
	public void updateDStopProgressType(DStopProgressType stopProgressType);
	
	public void createDReimbursementStatus(List<String> reimbursementStatus);
	public List<DReimbursementStatus> retrieveDReimbursementStatus();
	public void updateDReimbursementStatus(DReimbursementStatus reimbursementStatus);
	
	public void createDInvoiceStatus(List<String> invoiceStatus);
	public List<DInvoiceStatus> retrieveDInvoiceStatus();
	public void updateDInvoiceStatus(DInvoiceStatus invoiceStatus);
	
	public void createDInvoiceType(List<String> invoiceType);
	public List<DInvoiceType> retrieveDInvoiceType();
	public void updateDInvoiceType(DInvoiceType invoiceType);
	
	public void createDPaymentStatus(List<String> paymentStatus);
	public List<DPaymentStatus> retrieveDPaymentStatus();
	public void updateDPaymentStatus(DPaymentStatus paymentStatus);
	
	public DCity createArea(String cityCode,DCity city);
	public List<DCity> retrieveArea(String cityCode);

	public DCity createDCity(DCity city);
	
	public DItemCategory createDItemCategory(DItemCategory itemCategory);
	public List<DItemCategory> retrieveDItemCategory();
	
	public DBrand createDBrand(DBrand brand);
	public List<DBrand> retrieveDBrand();
	
	public DUnit createDUnit(DUnit itemUnit);
	public List<DUnit> retrieveDUnit();
	
	public DItemType createDItemType(DItemType itemType);
	public List<DItemType> retrieveDItemType();
	
	public DPurchaseOrderStatus createDPurchaseOrderStatus(DPurchaseOrderStatus purchaseOrderStatus);
	public List<DPurchaseOrderStatus> retrieveDPurchaseOrderStatus();
		
	public DWorkOrderStatus createDWorkOrderStatus(DWorkOrderStatus workOrderStatus);
	public List<DWorkOrderStatus> retrieveDWorkOrderStatus();
	
	public DWorkOrderCategory createDWorkOrderCategory(DWorkOrderCategory workOrderCategory);
	public List<DWorkOrderCategory> retrieveDWorkOrderCategory();
	
	public DWorkOrderSubCategory createDWorkOrderSubCategory(DWorkOrderSubCategory workOrderSubCategory);
	public List<DWorkOrderSubCategory> retrieveDWorkOrderSubCategory();
	
	public DPriority createDPriority(DPriority priority);
	public List<DPriority> retrieveDPriority();
}
