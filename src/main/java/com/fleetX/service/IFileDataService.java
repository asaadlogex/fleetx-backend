package com.fleetX.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.dto.EmployeeDTO;
import com.fleetX.dto.RouteDTO;
import com.fleetX.dto.TractorDTO;
import com.fleetX.dto.TrailerDTO;
import com.fleetX.dto.TripDetailDTO;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.order.Job;
import com.itextpdf.text.DocumentException;

public interface IFileDataService {
	public List<RouteDTO> mapToRouteDTO(MultipartFile file) throws IOException, InvalidFormatException;
	public List<TractorDTO> mapToTractorDTO(MultipartFile file) throws IOException, InvalidFormatException;
	public List<TrailerDTO> mapToTrailerDTO(MultipartFile file) throws IOException, InvalidFormatException;
	public List<EmployeeDTO> mapToEmployeeDTO(MultipartFile file) throws IOException, InvalidFormatException;
	List<Tyre> mapToTyre(MultipartFile file,Boolean forTractor) throws IOException, InvalidFormatException;
	
	public byte[] exportTripDetailsToExcel(List<TripDetailDTO> tripDetails,String filename);
	public byte[] exportTractorsToExcel(List<Tractor> tractors,String filename);
	public byte[] exportTrailersToExcel(List<Trailer> trailers,String filename);
	public byte[] exportEmployeesToExcel(List<Employee> employees,String filename);
	public byte[] exportWorkOrderToExcel(List<WorkOrder> workOrders,String filename);
	public byte[] exportInventoriesToExcel(List<Inventory> inventories, String filename);
	
	public byte[] previewReimbursement(List<Job> jobs) throws DocumentException, ParseException;
	public byte[] previewInvoice(Invoice invoice) throws DocumentException;
	public byte[] previewDHInvoice(DHInvoice invoice) throws DocumentException;

}
