package com.fleetX.service;

import java.util.List;

import com.fleetX.dto.ReceivePurchaseOrderDTO;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;

public interface IInventoryService {
	public List<Inventory> getAllInventory();
	public Inventory getInventoryById(Integer id);
	public List<Inventory> filterInventory(Integer itemId);

	public Item addItem(Item item);
	public Item addItem(Item item, Integer qty, Double cost);
	public List<Item> getAllItem();
	public Item getItemById(Integer id);
	public Item updateItem(Item item);
	public void deleteItemById(Integer id);
	
	public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder);
	public List<PurchaseOrder> getAllPurchaseOrder();
	public PurchaseOrder getPurchaseOrderById(Integer id);
	public PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrder);
	public void deletePurchaseOrderById(Integer id);
	
	public WorkOrder addWorkOrder(WorkOrder workOrder);
	public List<WorkOrder> getAllWorkOrder();
	public WorkOrder getWorkOrderById(Integer id);
	public WorkOrder updateWorkOrder(WorkOrder workOrder);
	public void deleteWorkOrderById(Integer id);
	
	public void updatePurchaseOrderStatus(Integer purchaseOrderId, String status);
	public void updateWorkOrderStatus(Integer workOrderId, String status);
	PurchaseOrder receivePurchaseOrder(ReceivePurchaseOrderDTO receiveDetail);
	
	public InventoryAdjustment addInventoryAdjustment(InventoryAdjustment inventoryAdjustment);
	public List<InventoryAdjustment> getAllInventoryAdjustment();
	public InventoryAdjustment getInventoryAdjustmentById(Integer id);
	
}
