package com.fleetX.service.implementation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.DashboardData;
import com.fleetX.dto.JobFuelDetailDTO;
import com.fleetX.dto.RoadwayBillSummaryDTO;
import com.fleetX.dto.StopProgressDTO;
import com.fleetX.dto.TripDetailDTO;
import com.fleetX.entity.dropdown.DExpenseType;
import com.fleetX.entity.dropdown.DIncomeType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.mapper.OrderMapper;
import com.fleetX.repository.DPositionRepository;
import com.fleetX.service.IAccountService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.IDropDownService;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IOrderService;
import com.fleetX.service.ISummaryService;

@Service
@Transactional
public class SummaryService implements ISummaryService {
	@Autowired
	IOrderService orderService;
	@Autowired
	IAccountService accountService;
	@Autowired
	IAssetService assetService;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	IDropDownService dropdownService;
	@Autowired
	OrderMapper orderMapper;
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	DPositionRepository positionRepository;

	@Override
	public TripDetailDTO getTripDetail(String jobId) throws ParseException {
		TripDetailDTO tripDetail = new TripDetailDTO();
		Job job = orderService.getJobById(jobId);
		tripDetail.setJobId(jobId);
		tripDetail.setJobOpenDate(systemUtil.convertToDateString(job.getStartDate()));
		tripDetail.setJobStatus(job.getJobStatus());

		tripDetail.setTmsKm(job.getTmsKm());
		tripDetail.setTrackerKm(job.getTrackerKm());
		tripDetail.setOdometerKm(job.getOdometerKm());
		tripDetail.setTotalKm(job.getTotalKm());

		Double totalFuelInLtr = 0d;
		Double totalFuelCost = 0d;
		List<JobFuelDetail> fuelDetails = job.getJobFuelDetails();
		for (JobFuelDetail fuelDetail : fuelDetails) {
			totalFuelInLtr += fuelDetail.getFuelInLitre();
			totalFuelCost += fuelDetail.getTotalAmount();
		}
		if (totalFuelInLtr > 0) {
			if (job.getOdometerKm() != null) {
				tripDetail.setFuelAvg(systemUtil.roundOff(job.getOdometerKm() / totalFuelInLtr));
			} else
				tripDetail.setFuelAvg(0d);
			if (job.getTrackerKm() != null) {
				tripDetail.setTrackerFuelAvg(systemUtil.roundOff(job.getTrackerKm() / totalFuelInLtr));
			} else
				tripDetail.setTrackerFuelAvg(0d);
		} else {
			tripDetail.setFuelAvg(0d);
			tripDetail.setTrackerFuelAvg(0d);
		}
		tripDetail.setTotalFuelCost(totalFuelCost);

		List<JobFuelDetailDTO> fuelDetailDtos = new ArrayList<>();
		for (JobFuelDetail fuelDetail : fuelDetails) {
			fuelDetailDtos.add(orderMapper.mapToJobFuelDetailDTO(fuelDetail));
		}
		tripDetail.setFuelDetails(fuelDetailDtos);

		List<RoadwayBill> rwbs = job.getRoadwayBills();
		List<StopProgress> stopProgresses = new ArrayList<>();
		List<Expense> expenses = new ArrayList<>();
		List<Income> incomes = new ArrayList<>();

		Double totalIncome = 0d, totalExpense = 0d;
		Date arrival, departure;
		Double duration = 0d;
		List<RoadwayBillSummaryDTO> roadwayBillDTOs = new ArrayList<>();
		if (rwbs != null && rwbs.size() > 0) {
			for (RoadwayBill rwb : rwbs) {
				expenses.addAll(rwb.getExpenses());
				incomes.addAll(rwb.getIncomes());
				roadwayBillDTOs.add(orderMapper.mapToRoadwayBillSummaryDTO(rwb));
			}
			tripDetail.setRoadwayBills(roadwayBillDTOs);
		}
		stopProgresses = job.getStopProgresses();
		if (stopProgresses.size() > 0) {
			arrival = stopProgresses.get(0).getArrivalTime();
			if (stopProgresses.size() > 1) {
				departure = stopProgresses.get(stopProgresses.size() - 1).getDepartureTime();
			} else {
				departure = stopProgresses.get(0).getDepartureTime();
			}
			duration = systemUtil.differenceInDateTime(arrival, departure);
			tripDetail.setDurationInHrs(duration);

			List<StopProgressDTO> stopProgressDTOs = new ArrayList<>();
			for (StopProgress stopProgress : stopProgresses) {
				stopProgressDTOs.add(orderMapper.mapToStopProgressDTO(stopProgress));
			}
			stopProgressDTOs.sort(new Comparator<StopProgressDTO>() {
				@Override
				public int compare(StopProgressDTO x, StopProgressDTO y) {
					if (x.getArrivalTime() == null && y.getArrivalTime() == null)
						return 0;
					if (x.getArrivalTime() == null)
						return 1;
					else if (y.getArrivalTime() == null)
						return -1;
					else
						return x.getArrivalTime().compareTo(y.getArrivalTime());
				}

			});
			tripDetail.setStopProgresses(stopProgressDTOs);

		}

		Map<String, Double> allExpenses = new HashMap<>();
		for (String expenseType : getAllExpenseType()) {
			allExpenses.put(expenseType, 0d);
		}

		if (expenses.size() > 0) {
			for (Expense expense : expenses) {
				allExpenses.put(expense.getExpenseType().getValue(),
						allExpenses.get(expense.getExpenseType().getValue()) + expense.getAmount());
				totalExpense += expense.getAmount();
			}
		}
		tripDetail.setExpenses(allExpenses);
		tripDetail.setTotalExpense(totalExpense);

		Map<String, Double> allIncomes = new HashMap<>();
		for (String incomeType : getAllIncomeType()) {
			allIncomes.put(incomeType, 0d);
		}

		if (incomes.size() > 0) {
			for (Income income : incomes) {
				allIncomes.put(income.getIncomeType().getValue(),
						allIncomes.get(income.getIncomeType().getValue()) + income.getAmount());
				totalIncome += income.getAmount();
			}
		}
		tripDetail.setIncomes(allIncomes);
		tripDetail.setTotalIncome(totalIncome);
		if (job.getOdometerKm() != null && job.getOdometerKm() > 0)
			tripDetail.setAvgFuelCostPerKm(systemUtil.roundOff(totalFuelCost / job.getOdometerKm()));
		tripDetail.setNetIncome(totalIncome - totalExpense - totalFuelCost);
		tripDetail.setJobCloseDate(systemUtil.convertToDateString(job.getEndDate()));

		return tripDetail;
	}

	@Override
	public List<String> getAllIncomeType() {
		List<DIncomeType> incomeTypes = dropdownService.retrieveDIncomeType();
		List<String> incomeTypeValue = new ArrayList<>();
		for (DIncomeType incomeType : incomeTypes) {
			incomeTypeValue.add(incomeType.getValue());
		}
		return incomeTypeValue;
	}

	@Override
	public List<String> getAllExpenseType() {
		List<DExpenseType> expenseTypes = dropdownService.retrieveDExpenseType();
		List<String> expenseTypeValue = new ArrayList<>();
		for (DExpenseType expenseType : expenseTypes) {
			expenseTypeValue.add(expenseType.getValue());
		}
		return expenseTypeValue;
	}

	@Override
	public DashboardData getDashboardData() {
		DashboardData dashboard = new DashboardData();
		dashboard.setJobs(getJobStats());
		dashboard.setRoadwayBills(getRwbStats());
		dashboard.setAssets(getAssetStats());
		dashboard.setInvoices(getInvoiceStats());
		dashboard.setPayments(getPaymentStats());
		dashboard.setFinancials(getFinancialStats());
		return dashboard;
	}

	private Map<String, Long> getPaymentStats() {
		Map<String, Long> paymentStats = new HashMap<String, Long>();
		List<DPaymentStatus> statuses = dropdownService.retrieveDPaymentStatus();
		for (DPaymentStatus status : statuses) {
			long n = accountService.countInvoiceByPaymentStatus(status);
			paymentStats.put(status.getValue().replaceAll("\\s", ""), n);
		}
		long total = 0l;
		for (long value : paymentStats.values()) {
			total += value;
		}
		paymentStats.put("total", total);
		return paymentStats;
	}

	private Map<String, Map<String, Double>> getFinancialStats() {
		Map<String, Map<String, Double>> financialStats = new HashMap<>();
		financialStats.put("last7Days", getLastXDayFinancialStats(7));
		financialStats.put("last30Days", getLastXDayFinancialStats(30));
		financialStats.put("last90Days", getLastXDayFinancialStats(90));
		financialStats.put("YTD", getYTDFinancialStats());
		return financialStats;
	}

	private Map<String, Double> getYTDFinancialStats() {
		List<Job> jobs = getYTDJobs();
		return getFinancialStats(jobs);
	}

	private Map<String, Double> getLastXDayFinancialStats(int i) {
		List<Job> jobs = getLastXDayJobs(i);
		return getFinancialStats(jobs);
	}

	private Map<String, Double> getFinancialStats(List<Job> jobs) {
		Map<String, Double> stats = new HashMap<>();
		Double income = 0d, expense = 0d, fuelCost = 0d;
		for (Job job : jobs) {
			income += orderService.getIncomeOfJob(job);
			expense += orderService.getExpenseOfJob(job);
			fuelCost += orderService.getFuelExpenseOfJob(job);
		}
		stats.put("income", income);
		stats.put("expense", expense);
		stats.put("fuelExpense", fuelCost);
		stats.put("profit", income - expense - fuelCost);
		return stats;
	}

	private List<Job> getLastXDayJobs(int i) {
		Date currentDate = systemUtil.getCurrentDate();
		List<Job> jobs = orderService.getClosedJobInRange(systemUtil.getCustomDate(-i * SystemType.DAY_IN_MS),
				currentDate);
		return jobs;
	}

	private List<Job> getYTDJobs() {
		Date currentDate = systemUtil.getCurrentDate();
		List<Job> jobs = orderService.getClosedJobInRange(systemUtil.getCustomDate(1, 1, currentDate.getYear()),
				currentDate);
		return jobs;
	}

	private Map<String, Map<String, Long>> getInvoiceStats() {
		Map<String, Map<String, Long>> invoiceStats = new HashMap<String, Map<String, Long>>();
		List<DInvoiceStatus> statuses = dropdownService.retrieveDInvoiceStatus();
		List<DInvoiceType> invoiceTypes = dropdownService.retrieveDInvoiceType();
		for (DInvoiceType invoiceType : invoiceTypes) {
			Map<String, Long> invoiceSubStats = new HashMap<>();
			for (DInvoiceStatus status : statuses) {
				List<RoadwayBill> rwbs = orderService.filterRoadwayBill(null, null, null, null, status.getValue(),
						invoiceType.getValue());
				long n = rwbs.size();
				invoiceSubStats.put(status.getValue(), n);
			}
			long total = 0l;
			for (long value : invoiceSubStats.values()) {
				total += value;
			}
			invoiceSubStats.put("total", total);
			invoiceStats.put(invoiceType.getValue().replaceAll("\\s", ""), invoiceSubStats);
		}
		return invoiceStats;
	}

	private Map<String, Map<String, Long>> getAssetStats() {
		Map<String, Map<String, Long>> assetStats = new HashMap<String, Map<String, Long>>();
		assetStats.put("Tractors", getTractorStats());
		assetStats.put("Trailers", getTrailerStats());
		assetStats.put("Containers", getContainerStats());
		assetStats.put("Trackers", getTrackerStats());
		assetStats.put("Drivers", getDriverStats());
		return assetStats;
	}

	private Map<String, Long> getDriverStats() {
		DPosition position = positionRepository.findByValue(SystemType.POSITION_DRIVER);
		Map<String, Long> driverStats = new HashMap<String, Long>();
		long n = employeeService.countEmployeeByPositionAndStatus(position, SystemType.STATUS_ACTIVE);
		driverStats.put(SystemType.STATUS_ACTIVE, n);
		n = employeeService.countEmployeeByPositionAndStatus(position, SystemType.STATUS_INACTIVE);
		driverStats.put(SystemType.STATUS_INACTIVE, n);
		return driverStats;
	}

	private Map<String, Long> getTrackerStats() {
		Map<String, Long> trackerStats = new HashMap<String, Long>();
		long n = assetService.countTrackerByStatus(SystemType.STATUS_ACTIVE);
		trackerStats.put(SystemType.STATUS_ACTIVE, n);
		n = assetService.countTrackerByStatus(SystemType.STATUS_INACTIVE);
		trackerStats.put(SystemType.STATUS_INACTIVE, n);
		return trackerStats;
	}

	private Map<String, Long> getContainerStats() {
		Map<String, Long> containerStats = new HashMap<String, Long>();
		long n = assetService.countContainerByStatus(SystemType.STATUS_ACTIVE);
		containerStats.put(SystemType.STATUS_ACTIVE, n);
		n = assetService.countContainerByStatus(SystemType.STATUS_INACTIVE);
		containerStats.put(SystemType.STATUS_INACTIVE, n);
		return containerStats;
	}

	private Map<String, Long> getTrailerStats() {
		Map<String, Long> trailerStats = new HashMap<String, Long>();
		long n = assetService.countTrailerByStatus(SystemType.STATUS_ACTIVE);
		trailerStats.put(SystemType.STATUS_ACTIVE, n);
		n = assetService.countTrailerByStatus(SystemType.STATUS_INACTIVE);
		trailerStats.put(SystemType.STATUS_INACTIVE, n);
		return trailerStats;
	}

	private Map<String, Long> getTractorStats() {
		Map<String, Long> tractorStats = new HashMap<String, Long>();
		long n = assetService.countTractorByStatus(SystemType.STATUS_ACTIVE);
		tractorStats.put(SystemType.STATUS_ACTIVE, n);
		n = assetService.countTractorByStatus(SystemType.STATUS_INACTIVE);
		tractorStats.put(SystemType.STATUS_INACTIVE, n);
		return tractorStats;
	}

	private Map<String, Long> getRwbStats() {
		Map<String, Long> rwbStats = new HashMap<String, Long>();
		List<DRWBStatus> statuses = dropdownService.retrieveDRWBStatus();
		for (DRWBStatus status : statuses) {
			long n = orderService.countRwbByStatus(status);
			rwbStats.put(status.getValue().replaceAll("\\s", ""), n);
		}
		long total = 0l;
		for (long value : rwbStats.values()) {
			total += value;
		}
		rwbStats.put("total", total);
		return rwbStats;
	}

	private Map<String, Long> getJobStats() {
		Map<String, Long> jobStats = new HashMap<String, Long>();
		List<DJobStatus> statuses = dropdownService.retrieveDJobStatus();
		for (DJobStatus status : statuses) {
			long n = orderService.countJobByStatus(status);
			jobStats.put(status.getValue().replaceAll("\\s", ""), n);
		}
		long total = 0l;
		for (long value : jobStats.values()) {
			total += value;
		}
		jobStats.put("total", total);
		return jobStats;
	}

}
