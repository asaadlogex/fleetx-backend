package com.fleetX.service.implementation;

import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.IInventoryDAO;
import com.fleetX.dto.ReceiveItemDTO;
import com.fleetX.dto.ReceivePurchaseOrderDTO;
import com.fleetX.entity.dropdown.DPurchaseOrderStatus;
import com.fleetX.entity.dropdown.DWorkOrderStatus;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.InventoryTransactionInfo;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.ItemPurchaseDetail;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.maintenance.WorkOrderItemDetail;
import com.fleetX.repository.DPurchaseOrderStatusRepository;
import com.fleetX.repository.DWorkOrderStatusRepository;
import com.fleetX.service.IInventoryService;
import com.fleetX.validator.IInventoryValidator;

@Service
@Transactional
public class InventoryService implements IInventoryService {
	@Autowired
	IInventoryDAO inventoryDAO;
	@Autowired
	IInventoryValidator inventoryValidator;
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	DPurchaseOrderStatusRepository purchaseOrderStatusRepository;
	@Autowired
	DWorkOrderStatusRepository workOrderStatusRepository;

	private Inventory addInventory(Item item) {
		Inventory inventory = new Inventory();
		inventory.setInventoryStartDate(systemUtil.getCurrentDate());
		inventory.setLastUpdatedOn(systemUtil.getCurrentDate());
		inventory.setItem(item);
		inventory.setQtyCommitted(0);
		inventory.setQtyOnHand(0);
		inventory.setQtyIn(0);
		inventory.setQtyOut(0);
		inventory.setAvgCost(0d);
		inventory.setCriticalQty(0);
		inventory = inventoryDAO.addInventory(inventory);
		return inventory;
	}

	@Override
	public List<Inventory> getAllInventory() {
		return inventoryDAO.getAllInventory();
	}

	@Override
	public Inventory getInventoryById(Integer id) {
		Assert.notNull(id, "Inventory ID: " + SystemType.MUST_NOT_BE_NULL);
		return inventoryDAO.getInventoryById(id);
	}

	@Override
	public List<Inventory> filterInventory(Integer itemId) {
		Assert.notNull(itemId, "Inventory Item ID: " + SystemType.MUST_NOT_BE_NULL);
		Item item = getItemById(itemId);
		Inventory inventory = new Inventory();
		inventory.setItem(item);
		return inventoryDAO.filterInventoryByExample(Example.of(inventory));
	}

	@Override
	public Item addItem(Item item) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validateItem(item);
		item.setItemId(null);
		item = inventoryDAO.addItem(item);
		addInventory(item);
		return item;
	}

	@Override
	public Item addItem(Item item, Integer qty, Double cost) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validateItem(item);
		item.setItemId(null);
		item = inventoryDAO.addItem(item);
		Inventory inventory = addInventory(item);
		inventory.setStartingQty(qty);
		inventory.setQtyOnHand(qty);
		inventory.setQtyIn(qty);
		inventory.setCriticalQty(item.getCriticalQty());
		inventory.setStartingCost(cost);
		inventory.setAvgCost(cost);
		inventoryDAO.updateInventory(inventory);
		return item;
	}

	@Override
	public List<Item> getAllItem() {
		return inventoryDAO.getAllItem();
	}

	@Override
	public Item getItemById(Integer id) {
		Assert.notNull(id, "Item ID: " + SystemType.MUST_NOT_BE_NULL);
		return inventoryDAO.getItemById(id);
	}

	@Override
	public Item updateItem(Item itemNew) {
		Assert.notNull(itemNew, "Item: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validateItem(itemNew);
		Item itemOld = getItemById(itemNew.getItemId());
		itemOld.setItemName(itemNew.getItemName());
		itemOld.setItemSku(itemNew.getItemSku());
		itemOld.setItemPrice(itemNew.getItemPrice());
		itemOld.setItemType(itemNew.getItemType());
		itemOld.setWeightInKg(itemNew.getWeightInKg());
		itemOld.setItemUpc(itemNew.getItemUpc());
		itemOld.setItemEan(itemNew.getItemEan());
		itemOld.setItemIsbn(itemNew.getItemIsbn());
		itemOld.setItemMpn(itemNew.getItemMpn());
		itemOld.setItemCategory(itemNew.getItemCategory());
		itemOld.setItemUnit(itemNew.getItemUnit());
		itemOld.setItemBrand(itemNew.getItemBrand());
		itemOld.setIsRefundable(itemNew.getIsRefundable());
		itemOld.setItemLength(itemNew.getItemLength());
		itemOld.setItemHeight(itemNew.getItemHeight());
		itemOld.setItemBreadth(itemNew.getItemBreadth());
		itemOld.setItemPicUrl(itemNew.getItemPicUrl());
		return inventoryDAO.updateItem(itemOld);
	}

	@Override
	public void deleteItemById(Integer id) {
		Assert.notNull(id, "Item ID: " + SystemType.MUST_NOT_BE_NULL);
		List<Inventory> inventories = filterInventory(id);
		if (inventories != null && inventories.size() > 0) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyOnHand() != 0 || inventory.getExpectedQty() != 0 || inventory.getQtyCommitted() != 0)
				throw new EntityExistsException("Cannot delete item. Item is in use!");
		}
		inventoryDAO.deleteItemById(id);
	}

	@Override
	public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder) {
		Assert.notNull(purchaseOrder, "Purchase order: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validatePurchaseOrder(purchaseOrder);
		List<ItemPurchaseDetail> itemPurchaseDetails = purchaseOrder.getItemPurchaseDetail();
		purchaseOrder.setPurchaseOrderId(null);
		if (purchaseOrder.getOrderedOn() == null)
			purchaseOrder.setOrderedOn(systemUtil.getCurrentDate());
		purchaseOrder.setPurchaseOrderStatus(
				purchaseOrderStatusRepository.findByValue(SystemType.PURCHASE_ORDER_STATUS_PENDING));
		purchaseOrder = inventoryDAO.addPurchaseOrder(purchaseOrder);
		for (ItemPurchaseDetail itemPurchaseDetail : itemPurchaseDetails) {
			itemPurchaseDetail.setItemPurchaseDetailId(null);
			itemPurchaseDetail.setPurchaseOrder(purchaseOrder);
			itemPurchaseDetail = inventoryDAO.addItemPurchaseDetail(itemPurchaseDetail);
			updateItemExpectedQty(itemPurchaseDetail.getItem(), itemPurchaseDetail.getQtyOrdered());
		}
		return purchaseOrder;
	}

	@Override
	public List<PurchaseOrder> getAllPurchaseOrder() {
		return inventoryDAO.getAllPurchaseOrder();
	}

	@Override
	public PurchaseOrder getPurchaseOrderById(Integer id) {
		Assert.notNull(id, "Purchase order ID: " + SystemType.MUST_NOT_BE_NULL);
		return inventoryDAO.getPurchaseOrderById(id);
	}

	@Override
	public PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrderNew) {
		Assert.notNull(purchaseOrderNew, "Purchase order: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validatePurchaseOrder(purchaseOrderNew);
		PurchaseOrder purchaseOrderOld = getPurchaseOrderById(purchaseOrderNew.getPurchaseOrderId());
		if (purchaseOrderOld.getPurchaseOrderStatus().getValue().equals(SystemType.PURCHASE_ORDER_STATUS_RECEIVED))
			throw new IllegalArgumentException("Order already received and cannot be further modified!");
		purchaseOrderOld.setOrderedOn(purchaseOrderNew.getOrderedOn());
		purchaseOrderOld.setExpectedOn(purchaseOrderNew.getExpectedOn());
		purchaseOrderOld.setReceivedOn(purchaseOrderNew.getReceivedOn());
		purchaseOrderOld.setGrossAmount(purchaseOrderNew.getGrossAmount());
		purchaseOrderOld.setDiscountInAmount(purchaseOrderNew.getDiscountInAmount());
		purchaseOrderOld.setTotalAmount(purchaseOrderNew.getTotalAmount());
		purchaseOrderOld.setReferenceNumber(purchaseOrderNew.getReferenceNumber());
		purchaseOrderOld.setPurchaseOrderStatus(purchaseOrderNew.getPurchaseOrderStatus());
		List<ItemPurchaseDetail> itemPurchaseDetailsNew = purchaseOrderNew.getItemPurchaseDetail();
		for (ItemPurchaseDetail itemPurchaseDetailNew : itemPurchaseDetailsNew) {
			if (itemPurchaseDetailNew.getItemPurchaseDetailId() != null) {
				itemPurchaseDetailNew = updateItemPurchaseDetail(itemPurchaseDetailNew);
				updateItemExpectedQty(itemPurchaseDetailNew.getItem(), itemPurchaseDetailNew.getQtyOrdered());
			} else {
				itemPurchaseDetailNew.setPurchaseOrder(purchaseOrderOld);
				itemPurchaseDetailNew = inventoryDAO.addItemPurchaseDetail(itemPurchaseDetailNew);
				updateItemExpectedQty(itemPurchaseDetailNew.getItem(), itemPurchaseDetailNew.getQtyOrdered());
			}
		}
		return inventoryDAO.updatePurchaseOrder(purchaseOrderOld);
	}

	private ItemPurchaseDetail updateItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetailNew) {
		ItemPurchaseDetail itemPurchaseDetailOld = inventoryDAO
				.getItemPurchaseDetailById(itemPurchaseDetailNew.getItemPurchaseDetailId());
		updateItemExpectedQty(itemPurchaseDetailOld.getItem(), -itemPurchaseDetailOld.getQtyOrdered());
		itemPurchaseDetailOld.setQtyOrdered(itemPurchaseDetailNew.getQtyOrdered());
//		itemPurchaseDetailOld.setQtyReceived(itemPurchaseDetailNew.getQtyReceived());
//		itemPurchaseDetailOld
//				.setQtyRemaining(itemPurchaseDetailNew.getQtyOrdered() - itemPurchaseDetailNew.getQtyReceived());
		itemPurchaseDetailOld.setRate(itemPurchaseDetailNew.getRate());
		itemPurchaseDetailOld.setAmount(itemPurchaseDetailNew.getRate() * itemPurchaseDetailNew.getQtyOrdered());
//		itemPurchaseDetailOld.setItem(itemPurchaseDetailNew.getItem());
		return inventoryDAO.updateItemPurchaseDetail(itemPurchaseDetailOld);

	}

	private void receiveInventory(List<ItemPurchaseDetail> itemPurchaseDetails) {
		for (ItemPurchaseDetail itemPurchaseDetail : itemPurchaseDetails) {
			List<Inventory> inventories = filterInventory(itemPurchaseDetail.getItem().getItemId());
			if (inventories == null)
				throw new NoSuchElementException("Inventory not found!");
			Inventory inventory = inventories.get(0);
			Integer qtyOnHand = inventory.getQtyOnHand();
			Integer qtyIn = inventory.getQtyIn();
			if (qtyOnHand != null)
				qtyOnHand += itemPurchaseDetail.getQtyReceived();
			else
				qtyOnHand = itemPurchaseDetail.getQtyReceived();
			if (qtyIn != null)
				qtyIn += itemPurchaseDetail.getQtyReceived();
			else
				qtyIn = itemPurchaseDetail.getQtyReceived();
			inventory.setAvgCost(itemPurchaseDetail.getRate());
			inventory.setLastUpdatedOn(systemUtil.getCurrentDate());
			inventoryDAO.updateInventory(inventory);
		}
	}

	@Override
	public void deletePurchaseOrderById(Integer id) {
		Assert.notNull(id, "Purchase order ID: " + SystemType.MUST_NOT_BE_NULL);
		PurchaseOrder purchaseOrder = getPurchaseOrderById(id);
		if (!purchaseOrder.getPurchaseOrderStatus().getValue().equals(SystemType.PURCHASE_ORDER_STATUS_PENDING))
			throw new IllegalArgumentException("Cannot delete this purchase order!");
		for (ItemPurchaseDetail itemPurchaseDetail : purchaseOrder.getItemPurchaseDetail()) {
			updateItemExpectedQty(itemPurchaseDetail.getItem(), -itemPurchaseDetail.getQtyOrdered());
		}
		inventoryDAO.deletePurchaseOrderById(id);
	}

	@Override
	public WorkOrder addWorkOrder(WorkOrder workOrder) {
		Assert.notNull(workOrder, "Work order: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validateWorkOrder(workOrder);
		List<WorkOrderItemDetail> workOrderItemDetails = workOrder.getWorkOrderItemDetail();
		workOrder.setWorkOrderId(null);
		workOrder.setWorkOrderStatus(workOrderStatusRepository.findByValue(SystemType.WORK_ORDER_STATUS_OPEN));
		workOrder = inventoryDAO.addWorkOrder(workOrder);
		Double totalAmount = 0d;
		for (WorkOrderItemDetail workOrderItemDetail : workOrderItemDetails) {
			if (!isItemAvailable(workOrderItemDetail.getItem(), workOrderItemDetail.getQtyUsed()))
				throw new NoSuchElementException(workOrderItemDetail.getItem().getItemName() + " not available");
			commitItemQty(workOrderItemDetail.getItem(), workOrderItemDetail.getQtyUsed());
			workOrderItemDetail.setAmount(workOrderItemDetail.getRate() * workOrderItemDetail.getQtyUsed());
			workOrderItemDetail.setWorkOrderItemDetailId(null);
			workOrderItemDetail.setWorkOrder(workOrder);
			workOrderItemDetail = inventoryDAO.addWorkOrderItemDetail(workOrderItemDetail);
			totalAmount += workOrderItemDetail.getAmount();
		}
		workOrder.setTotalAmount(totalAmount);
		return inventoryDAO.updateWorkOrder(workOrder);
	}

	@Override
	public List<WorkOrder> getAllWorkOrder() {
		return inventoryDAO.getAllWorkOrder();
	}

	@Override
	public WorkOrder getWorkOrderById(Integer id) {
		Assert.notNull(id, "Work order ID: " + SystemType.MUST_NOT_BE_NULL);
		return inventoryDAO.getWorkOrderById(id);
	}

	@Override
	public WorkOrder updateWorkOrder(WorkOrder workOrderNew) {
		Assert.notNull(workOrderNew, "Work order: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validateWorkOrder(workOrderNew);
		WorkOrder workOrderOld = getWorkOrderById(workOrderNew.getWorkOrderId());
		if (workOrderOld.getWorkOrderStatus().getValue().equals(SystemType.WORK_ORDER_STATUS_CLOSED))
			throw new IllegalArgumentException("Order already completed and cannot be further modified!");
//		workOrderOld.setWorkOrderStatus(workOrderNew.getWorkOrderStatus());
		workOrderOld.setWorkOrderCategory(workOrderNew.getWorkOrderCategory());
		workOrderOld.setWorkOrderSubCategory(workOrderNew.getWorkOrderSubCategory());
		workOrderOld.setWorkshop(workOrderNew.getWorkshop());
		workOrderOld.setWorkOrderDate(workOrderNew.getWorkOrderDate());
		workOrderOld.setBaseStation(workOrderNew.getBaseStation());
		workOrderOld.setPriority(workOrderNew.getPriority());
		workOrderOld.setReferenceNumber(workOrderNew.getReferenceNumber());
		workOrderOld.setActivityDetail(workOrderNew.getActivityDetail());
		workOrderOld.setSenderName(workOrderNew.getSenderName());
		workOrderOld.setTractor(workOrderNew.getTractor());
		workOrderOld.setTractorKm(workOrderNew.getTractorKm());
		Double totalAmount = 0d;
		List<WorkOrderItemDetail> workOrderItemDetailsNew = workOrderNew.getWorkOrderItemDetail();
		for (WorkOrderItemDetail workOrderItemDetailNew : workOrderItemDetailsNew) {
			workOrderItemDetailNew.setAmount(workOrderItemDetailNew.getRate() * workOrderItemDetailNew.getQtyUsed());
			if (workOrderItemDetailNew.getWorkOrderItemDetailId() != null) {
				workOrderItemDetailNew = updateWorkOrderItemDetail(workOrderItemDetailNew);
			} else {
				if (!isItemAvailable(workOrderItemDetailNew.getItem(), workOrderItemDetailNew.getQtyUsed()))
					throw new NoSuchElementException("Some item not available");
				commitItemQty(workOrderItemDetailNew.getItem(), workOrderItemDetailNew.getQtyUsed());
				workOrderItemDetailNew.setWorkOrderItemDetailId(null);
				workOrderItemDetailNew.setWorkOrder(workOrderOld);
				workOrderItemDetailNew = inventoryDAO.addWorkOrderItemDetail(workOrderItemDetailNew);
			}
			totalAmount += workOrderItemDetailNew.getAmount();
		}
		workOrderOld.setTotalAmount(totalAmount);
		return inventoryDAO.updateWorkOrder(workOrderOld);
	}

	@Override
	public PurchaseOrder receivePurchaseOrder(ReceivePurchaseOrderDTO receiveDetail) {
		Assert.notNull(receiveDetail, "Purchase Order Receive Details: " + SystemType.MUST_NOT_BE_NULL);
		PurchaseOrder purchaseOrder = getPurchaseOrderById(receiveDetail.getPurchaseOrderId());
		List<ReceiveItemDTO> itemReceived = receiveDetail.getItemReceived();
		boolean completelyReceived = true;
		for (ReceiveItemDTO receiveItem : itemReceived) {
			ItemPurchaseDetail itemPurchaseDetail = inventoryDAO
					.getItemPurchaseDetailById(receiveItem.getItemPurchaseDetailId());
			Item item = itemPurchaseDetail.getItem();
			Inventory inventory = filterInventory(itemPurchaseDetail.getItem().getItemId()).get(0);
			if (!itemPurchaseDetail.getPurchaseOrder().getPurchaseOrderId().equals(receiveDetail.getPurchaseOrderId()))
				throw new IllegalArgumentException("Invalid item receiving information!");
			if (itemPurchaseDetail.getQtyReceived() != null) {
				if (itemPurchaseDetail.getQtyReceived() + receiveItem.getQtyReceived() > itemPurchaseDetail
						.getQtyOrdered())
					throw new IllegalArgumentException(
							"Total quantity received must not be greater than quantity ordered!");
				itemPurchaseDetail.setQtyReceived(itemPurchaseDetail.getQtyReceived() + receiveItem.getQtyReceived());
				itemPurchaseDetail
						.setQtyRemaining(itemPurchaseDetail.getQtyOrdered() - itemPurchaseDetail.getQtyReceived());
				itemPurchaseDetail = inventoryDAO.updateItemPurchaseDetail(itemPurchaseDetail);
			} else {
				if (receiveItem.getQtyReceived() > itemPurchaseDetail.getQtyOrdered())
					throw new IllegalArgumentException(
							"Total quantity received must not be greater than quantity ordered!");
				itemPurchaseDetail.setQtyReceived(receiveItem.getQtyReceived());
				itemPurchaseDetail
						.setQtyRemaining(itemPurchaseDetail.getQtyOrdered() - itemPurchaseDetail.getQtyReceived());
				itemPurchaseDetail = inventoryDAO.updateItemPurchaseDetail(itemPurchaseDetail);
			}
			inventory.setAvgCost(getAvgCost(inventory.getQtyIn(), inventory.getAvgCost(), receiveItem.getQtyReceived(),
					itemPurchaseDetail.getRate()));
			updateItemExpectedQty(item, -receiveItem.getQtyReceived());
			updateItemQtyIn(item, receiveItem.getQtyReceived());
			updateItemQtyOnHand(item, receiveItem.getQtyReceived());
			if (itemPurchaseDetail.getQtyRemaining() > 0)
				completelyReceived = false;
		}
		if (completelyReceived)
			updatePurchaseOrderStatus(purchaseOrder.getPurchaseOrderId(), SystemType.PURCHASE_ORDER_STATUS_RECEIVED);
		else
			updatePurchaseOrderStatus(purchaseOrder.getPurchaseOrderId(),
					SystemType.PURCHASE_ORDER_STATUS_PARTIALLY_RECEIVED);
		return purchaseOrder;
	}

	@Override
	public void updatePurchaseOrderStatus(Integer purchaseOrderId, String status) {
		PurchaseOrder purchaseOrder = getPurchaseOrderById(purchaseOrderId);
		DPurchaseOrderStatus statusOld = purchaseOrder.getPurchaseOrderStatus();
		DPurchaseOrderStatus statusNew = purchaseOrderStatusRepository.findByValue(status);
		if (statusNew == null)
			throw new NoSuchElementException("Invalid status!");
		if (statusOld.getValue().equals(SystemType.PURCHASE_ORDER_STATUS_RECEIVED))
			throw new IllegalArgumentException("Order already received and cannot be further modified!");
		switch (status) {
		case SystemType.PURCHASE_ORDER_STATUS_PENDING:
			if (!statusOld.getValue().equals(SystemType.PURCHASE_ORDER_STATUS_PENDING))
				throw new IllegalArgumentException("Cannot change order to given status!");
			break;
		case SystemType.PURCHASE_ORDER_STATUS_PARTIALLY_RECEIVED:
			if (statusOld.getValue().equals(SystemType.PURCHASE_ORDER_STATUS_RECEIVED))
				throw new IllegalArgumentException("Cannot change order to given status!");
			break;
		case SystemType.PURCHASE_ORDER_STATUS_RECEIVED:
			break;
		}
		purchaseOrder.setPurchaseOrderStatus(statusNew);
		inventoryDAO.updatePurchaseOrder(purchaseOrder);
	}

	@Override
	public void updateWorkOrderStatus(Integer workOrderId, String status) {
		WorkOrder workOrder = getWorkOrderById(workOrderId);
		DWorkOrderStatus statusOld = workOrder.getWorkOrderStatus();
		DWorkOrderStatus statusNew = workOrderStatusRepository.findByValue(status);
		if (statusNew == null)
			throw new NoSuchElementException("Invalid status!");
		if (statusOld.getValue().equals(SystemType.WORK_ORDER_STATUS_CLOSED))
			throw new IllegalArgumentException("Order already completed and cannot be further modified!");
		switch (status) {
		case SystemType.WORK_ORDER_STATUS_OPEN:
			if (!statusOld.getValue().equals(SystemType.WORK_ORDER_STATUS_OPEN))
				throw new IllegalArgumentException("Cannot change order to given status!");
			break;
		case SystemType.WORK_ORDER_STATUS_IN_PROGRESS:
			if (!statusOld.getValue().equals(SystemType.WORK_ORDER_STATUS_OPEN))
				throw new IllegalArgumentException("Cannot change order to given status!");
			for (WorkOrderItemDetail itemDetail : workOrder.getWorkOrderItemDetail()) {
				proceedCommittedQty(itemDetail.getItem(), itemDetail.getQtyUsed());
			}
			break;
		case SystemType.WORK_ORDER_STATUS_CLOSED:
			if (!statusOld.getValue().equals(SystemType.WORK_ORDER_STATUS_IN_PROGRESS))
				throw new IllegalArgumentException("Cannot change order to given status!");
			if (workOrder.getWorkOrderCompletionDate() == null)
				workOrder.setWorkOrderCompletionDate(systemUtil.getCurrentDate());
			break;
		}
		workOrder.setWorkOrderStatus(statusNew);
		inventoryDAO.updateWorkOrder(workOrder);
	}

	private WorkOrderItemDetail updateWorkOrderItemDetail(WorkOrderItemDetail workOrderItemDetailNew) {
		WorkOrderItemDetail workOrderItemDetailOld = inventoryDAO
				.getWorkOrderItemDetailById(workOrderItemDetailNew.getWorkOrderItemDetailId());
		workOrderItemDetailOld.setItem(workOrderItemDetailNew.getItem());
		workOrderItemDetailOld.setQtyUsed(workOrderItemDetailNew.getQtyUsed());
		workOrderItemDetailOld.setRate(workOrderItemDetailNew.getRate());
		workOrderItemDetailOld.setAmount(workOrderItemDetailNew.getAmount());
		return inventoryDAO.updateWorkOrderItemDetail(workOrderItemDetailOld);

	}

	@Override
	public void deleteWorkOrderById(Integer id) {
		Assert.notNull(id, "Work order ID: " + SystemType.MUST_NOT_BE_NULL);
		WorkOrder workOrder = getWorkOrderById(id);
		if (!workOrder.getWorkOrderStatus().getValue().equals(SystemType.WORK_ORDER_STATUS_OPEN))
			throw new IllegalArgumentException("Cannot delete the given work order!");
		for (WorkOrderItemDetail workOrderItemDetail : workOrder.getWorkOrderItemDetail()) {
			unCommitItemQty(workOrderItemDetail.getItem(), workOrderItemDetail.getQtyUsed());
		}
		inventoryDAO.deleteWorkOrderById(id);
	}

	public boolean isItemAvailable(Item item, Integer qty) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null) {
			Inventory inventory = inventories.get(0);
			return qty == null ? inventory.getQtyOnHand() > 0 : inventory.getQtyOnHand() >= qty;
		}
		return false;
	}

	public void commitItemQty(Item item, Integer qty) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyOnHand() < qty)
				throw new NoSuchElementException("Not enough quantity in inventory!");
			inventory.setQtyOnHand(inventory.getQtyOnHand() - qty);
			inventory.setQtyCommitted(inventory.getQtyCommitted() + qty);
			inventoryDAO.updateInventory(inventory);
		} else
			throw new NoSuchElementException("Inventory not found!");
	}

	public void unCommitItemQty(Item item, Integer qty) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyCommitted() < qty)
				throw new NoSuchElementException("Not enough quantity to un-commit!");
			inventory.setQtyOnHand(inventory.getQtyOnHand() + qty);
			inventory.setQtyCommitted(inventory.getQtyCommitted() - qty);
			inventoryDAO.updateInventory(inventory);
		} else
			throw new NoSuchElementException("Inventory not found!");
	}

	public void proceedCommittedQty(Item item, Integer qty) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyCommitted() < qty)
				throw new NoSuchElementException("Not enough quantity to proceed!");
			updateItemQtyOut(item, qty);
			inventory.setQtyCommitted(inventory.getQtyCommitted() - qty);
			inventoryDAO.updateInventory(inventory);
		} else
			throw new NoSuchElementException("Inventory not found!");
	}

	public void decreaseItemQty(Item item, Integer qty) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyOnHand() < qty)
				throw new NoSuchElementException("Not enough quantity in inventory!");
			updateItemQtyOnHand(item, -qty);
			updateItemQtyOut(item, qty);
			inventoryDAO.updateInventory(inventory);
		} else
			throw new NoSuchElementException("Inventory not found!");
	}

	private void updateItemExpectedQty(Item item, int qty) {
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null && inventories.size() > 0) {
			Inventory inventory = inventories.get(0);
			if (inventory.getExpectedQty() != null) {
				if (inventory.getExpectedQty() + qty < 0)
					throw new IllegalArgumentException("Expected quantity cannot be less than zero!");
				inventory.setExpectedQty(inventory.getExpectedQty() + qty);
			} else {
				inventory.setExpectedQty(qty);
			}
			inventoryDAO.updateInventory(inventory);
		} else
			throw new NoSuchElementException(item.getItemId() + ": " + SystemType.NO_SUCH_ELEMENT);
	}

	private void updateItemQtyIn(Item item, int qty) {
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null && inventories.size() > 0) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyIn() != null) {
				if (inventory.getQtyIn() + qty < 0)
					throw new IllegalArgumentException("Quantity-IN cannot be less than zero!");
				inventory.setQtyIn(inventory.getQtyIn() + qty);
			} else
				inventory.setQtyIn(qty);
		} else
			throw new NoSuchElementException(item.getItemId() + ": " + SystemType.NO_SUCH_ELEMENT);
	}

	private void updateItemQtyOut(Item item, int qty) {
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null && inventories.size() > 0) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyOut() != null) {
				if (inventory.getQtyOut() + qty < 0)
					throw new IllegalArgumentException("Quantity-OUT cannot be less than zero!");
				inventory.setQtyOut(inventory.getQtyOut() + qty);
			} else
				inventory.setQtyOut(qty);
		} else
			throw new NoSuchElementException(item.getItemId() + ": " + SystemType.NO_SUCH_ELEMENT);
	}

	private void updateItemQtyOnHand(Item item, int qty) {
		List<Inventory> inventories = filterInventory(item.getItemId());
		if (inventories != null && inventories.size() > 0) {
			Inventory inventory = inventories.get(0);
			if (inventory.getQtyOnHand() != null) {
				if (inventory.getQtyOnHand() + qty < 0)
					throw new IllegalArgumentException("Quantity on hand cannot be less than zero!");
				inventory.setQtyOnHand(inventory.getQtyOnHand() + qty);
			} else
				inventory.setQtyOnHand(qty);
		} else
			throw new NoSuchElementException(item.getItemId() + ": " + SystemType.NO_SUCH_ELEMENT);
	}

	private InventoryTransactionInfo addInventoryTransactionInfo() {
		InventoryTransactionInfo transaction = new InventoryTransactionInfo();
		return transaction;
	}

	@Override
	public InventoryAdjustment addInventoryAdjustment(InventoryAdjustment inventoryAdjustment) {
		Assert.notNull(inventoryAdjustment, "Inventory Adjustment: " + SystemType.MUST_NOT_BE_NULL);
		inventoryValidator.validateInventoryAdjustment(inventoryAdjustment);
		Integer qty = inventoryAdjustment.getAdjustmentQty();
		Integer itemId = inventoryAdjustment.getItem().getItemId();
		List<Inventory> inventories = filterInventory(itemId);
		if (inventories != null && inventories.size() > 0) {
			inventoryAdjustment.setInventoryAdjustmentId(null);
			inventoryAdjustment = inventoryDAO.addInventoryAdjustment(inventoryAdjustment);
			Inventory inventory = inventories.get(0);
			if (qty > 0) {
				if (inventory.getAdjustedUp() != null)
					inventory.setAdjustedUp(inventory.getAdjustedUp() + qty);
				else
					inventory.setAdjustedUp(qty);
			} else {
				if (inventory.getAdjustedDown() != null)
					inventory.setAdjustedDown(inventory.getAdjustedDown() + qty);
				else
					inventory.setAdjustedDown(qty);
			}
			inventory.setQtyOnHand(inventory.getQtyOnHand() == null ? qty : inventory.getQtyOnHand() + qty);
			inventoryDAO.updateInventory(inventory);
			return inventoryAdjustment;
		} else
			throw new NoSuchElementException("Inventory not found!");
	}

	@Override
	public List<InventoryAdjustment> getAllInventoryAdjustment() {
		return inventoryDAO.getAllInventoryAdjustment();
	}

	@Override
	public InventoryAdjustment getInventoryAdjustmentById(Integer id) {
		Assert.notNull(id, "Inventory Adjustment ID: " + SystemType.MUST_NOT_BE_NULL);
		return inventoryDAO.getInventoryAdjustmentById(id);
	}

	private Double getAvgCost(Integer qtyOld, Double priceOld, Integer qtyNew, Double priceNew) {
		Assert.notNull(qtyOld, "Qyantity OLD: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(priceOld, "Price OLD: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(qtyNew, "Qyantity NEW: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(priceNew, "Price NEW: " + SystemType.MUST_NOT_BE_NULL);
		Double avg = 0d;
		avg = ((qtyOld * priceOld) + (qtyNew * priceNew)) / (qtyOld + qtyNew);
		return avg;
	}

}
