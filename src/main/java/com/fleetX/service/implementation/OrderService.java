package com.fleetX.service.implementation;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.IOrderDAO;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.dropdown.DReimbursementStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RentalVehicle;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.entity.order.Warehouse;
import com.fleetX.repository.DBaseStationRepository;
import com.fleetX.repository.DExpenseTypeRepository;
import com.fleetX.repository.DIncomeTypeRepository;
import com.fleetX.repository.DInvoiceStatusRepository;
import com.fleetX.repository.DJobStatusRepository;
import com.fleetX.repository.DRWBStatusRepository;
import com.fleetX.repository.DReimbursementStatusRepository;
import com.fleetX.repository.DStateRepository;
import com.fleetX.repository.DStopTypeRepository;
import com.fleetX.repository.DVehicleOwnershipRepository;
import com.fleetX.repository.JobRepository;
import com.fleetX.repository.RoadwayBillRepository;
import com.fleetX.service.IAssetService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IFTPService;
import com.fleetX.service.IOrderService;
import com.fleetX.validator.IOrderValidator;

@Service
@Transactional
public class OrderService implements IOrderService {

	@Autowired
	IOrderDAO orderDAO;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IAssetService assetService;
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IOrderValidator orderValidator;
	@Autowired
	RoadwayBillRepository roadwayBillRepository;
	@Autowired
	DIncomeTypeRepository incomeTypeRepository;
	@Autowired
	DExpenseTypeRepository expenseTypeRepository;
	@Autowired
	DStopTypeRepository stopTypeRepository;
	@Autowired
	DVehicleOwnershipRepository vehicleOwnershipRepository;
	@Autowired
	DReimbursementStatusRepository reimbursementStatusRepository;
	@Autowired
	DBaseStationRepository baseStationRepository;
	@Autowired
	JobRepository jobRepository;
	@Autowired
	IFTPService ftpService;
	@Autowired
	IContractService contractService;
	@Autowired
	DJobStatusRepository jobStatusRepository;
	@Autowired
	DInvoiceStatusRepository invoiceStatusRepository;
	@Autowired
	DRWBStatusRepository rwbStatusRepository;
	@Autowired
	DStateRepository stateRepository;

	@Override

	public Job createJob(Job job) {
		Assert.notNull(job, "Job: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateJob(job);
		job.setJobId(null);
		job = orderDAO.createJob(job);
		return job;
	}

	public RentalVehicle addRentalVehicle(RentalVehicle rentalVehicle) {
		Assert.notNull(rentalVehicle, "Rental Vehicle: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateRentalVehicle(rentalVehicle);
		rentalVehicle.setRentalVehicleId(null);
		return orderDAO.addRentalVehicle(rentalVehicle);
	}

	public JobFuelDetail addJobFuelDetail(JobFuelDetail jobFuelDetail) {
		Assert.notNull(jobFuelDetail, "Job Fuel Detail: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateJobFuelDetail(jobFuelDetail);
		if (jobFuelDetail.getJob().getJobStatus().getValue().equals(SystemType.STATUS_JOB_CLOSE))
			throw new IllegalArgumentException("Job is already closed1");
		Double rate = companyService.getFuelRateByDate(jobFuelDetail.getSupplier(), jobFuelDetail.getFuelDate());
		jobFuelDetail.setJobFuelDetailId(null);
		jobFuelDetail.setRatePerLitre(rate);
		jobFuelDetail.setTotalAmount(jobFuelDetail.getFuelInLitre() * rate);
		StopProgress fuelProgress = new StopProgress();
		fuelProgress.setTractor(jobFuelDetail.getTractor());
		fuelProgress.setTrailer(jobFuelDetail.getTrailer());
		fuelProgress.setCurrentKm(jobFuelDetail.getCurrentKm());
		fuelProgress.setArrivalTime(jobFuelDetail.getFuelDate());
		fuelProgress.setDepartureTime(jobFuelDetail.getFuelDate());
		fuelProgress.setDetentionInHrs(0d);
		fuelProgress.setStopProgressType(stopTypeRepository.findByValue(SystemType.STOP_FUEL));
		fuelProgress.setVehicleOwnership(vehicleOwnershipRepository.findByValue(SystemType.OWNERSHIP_OWN));
		Company supplier = jobFuelDetail.getSupplier();
		Warehouse warehouse;
		if (supplier.getWarehouses() != null && supplier.getWarehouses().size() > 0) {
			warehouse = supplier.getWarehouses().get(0);
		} else {
			warehouse = new Warehouse();
			warehouse.setWarehouseName(supplier.getCompanyName());
			List<Address> companyAddresses = supplier.getCompanyAddresses();
			warehouse.getAddresses().add(companyAddresses.get(0));
			String contact = null;
			if (supplier.getCompanyContacts() != null && supplier.getCompanyContacts().size() > 0) {
				for (Contact companyContact : supplier.getCompanyContacts()) {
					if (companyContact.getChannel().getValue().equals(SystemType.CHANNEL_PHONE)) {
						contact = companyContact.getData();
						break;
					}
				}
			}
			warehouse.setPhone(contact);
			warehouse = companyService.addWarehouse(warehouse);
			supplier.getWarehouses().add(warehouse);
		}
		fuelProgress.setWarehouse(warehouse);
		fuelProgress.setJob(jobFuelDetail.getJob());
		fuelProgress = orderDAO.addStopProgress(fuelProgress);
		jobFuelDetail.setFuelProgress(fuelProgress);
		return orderDAO.addJobFuelDetail(jobFuelDetail);
	}

	@Override

	public Job updateJob(Job jobNew) {
		Assert.notNull(jobNew, "Job: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateJob(jobNew);

		Job jobOld = getJobById(jobNew.getJobId());
//		if (jobOld.getJobStatus().getValue().equals(SystemType.STATUS_JOB_CLOSE))
//			throw new IllegalArgumentException("Job is already closed!");
		Double totalKm = 0d;
		for (RoadwayBill rwb : jobOld.getRoadwayBills()) {
			totalKm += rwb.getRoute().getAvgDistanceInKM();
		}
		jobOld.setTotalKm(totalKm);
		jobOld.setJobStatus(jobNew.getJobStatus());
		if (jobOld.getRoadwayBills() == null || jobOld.getRoadwayBills().size() < 1) {
			jobOld.setStartBaseStation(jobNew.getStartBaseStation());
			jobOld.setStartDate(jobNew.getStartDate());
		}
		jobOld.setEndBaseStation(jobNew.getEndBaseStation());
		jobOld.setEndDate(jobNew.getEndDate());
		jobOld.setStartKm(jobNew.getStartKm());
		jobOld.setEndKm(jobNew.getEndKm());
		jobOld.setJobAdvance(jobNew.getJobAdvance());

		if (jobOld.getJobStatus().getValue().equals(SystemType.STATUS_JOB_CLOSE)) {
			double tmsKm = 0d, trackerKm = 0d;
			if (jobOld.getRoadwayBills() != null && jobOld.getRoadwayBills().size() > 0)
				jobOld.getRoadwayBills().stream().forEach(x -> {
					if (!x.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_COMPLETED))
						throw new IllegalArgumentException("Some roadway bills are not completed yet!");
				});
			if (jobOld.getEndDate() == null)
				jobOld.setEndDate(systemUtil.getCurrentDate());
			if (jobOld.getEndKm() == null || jobOld.getStartKm() == null)
				throw new IllegalArgumentException("Job Start and End KM are required to close the job!");
			else if (jobOld.getEndKm() < jobOld.getStartKm())
				throw new IllegalArgumentException("Job End KM must be greater than job Start KM!");
			else {
				if (jobOld.getStartKm() != null && jobOld.getEndKm() != null)
					jobOld.setOdometerKm(jobOld.getEndKm() - jobOld.getStartKm());
				if (jobOld.getRoadwayBills() != null)
					for (RoadwayBill rwb : jobOld.getRoadwayBills()) {
						if (rwb.getTmsKm() != null)
							tmsKm += rwb.getTmsKm();
						if (rwb.getTrackerKm() != null)
							trackerKm += rwb.getTrackerKm();
					}
				jobOld.setTmsKm(tmsKm);
				jobOld.setTrackerKm(trackerKm);
			}
			jobOld.setReimbursementStatus(
					reimbursementStatusRepository.findByValue(SystemType.REIMBURSEMENT_STATUS_PENDING));
		}
		return orderDAO.updateJob(jobOld);
	}

	public JobFuelDetail updateJobFuelDetail(JobFuelDetail jfdNew) {
		Assert.notNull(jfdNew, "Job Fuel Detail: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateJobFuelDetail(jfdNew);
		JobFuelDetail jfdOld = orderDAO.getJobFuelDetailById(jfdNew.getJobFuelDetailId());
		Job job = jfdOld.getJob();
		if (job.getJobStatus().getValue().equals(SystemType.STATUS_JOB_CLOSE))
			throw new IllegalArgumentException("Job is already closed!");
		jfdOld.setTractor(jfdNew.getTractor());
		jfdOld.setTrailer(jfdNew.getTrailer());
		jfdOld.setCurrentKm(jfdNew.getCurrentKm());
		jfdOld.setFuelInLitre(jfdNew.getFuelInLitre());
		jfdOld.setFuelCard(jfdNew.getFuelCard());
		jfdOld.setFuelDate(jfdNew.getFuelDate());
		jfdOld.setFuelSlipNumber(jfdNew.getFuelSlipNumber());
		jfdOld.setRatePerLitre(jfdNew.getRatePerLitre());
		jfdOld.setSupplier(jfdNew.getSupplier());
		jfdOld.setFuelTankType(jfdNew.getFuelTankType());
		jfdOld.setTotalAmount(jfdNew.getFuelInLitre() * jfdNew.getRatePerLitre());
		StopProgress fuelProgress = jfdOld.getFuelProgress();
		fuelProgress.setTractor(jfdOld.getTractor());
		fuelProgress.setTrailer(jfdOld.getTrailer());
		fuelProgress.setCurrentKm(jfdOld.getCurrentKm());
		fuelProgress.setArrivalTime(jfdOld.getFuelDate());
		fuelProgress.setDepartureTime(jfdOld.getFuelDate());
		orderDAO.updateStopProgress(fuelProgress);
		return orderDAO.updateJobFuelDetail(jfdOld);
	}

	public RentalVehicle updateRentalVehicle(RentalVehicle rvNew) {
		Assert.notNull(rvNew, "Rental Vehicle: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateRentalVehicle(rvNew);

		RentalVehicle rvOld = orderDAO.getRentalVehicleById(rvNew.getRentalVehicleId());
		rvOld.setEquipmentSize(rvNew.getEquipmentSize());
		rvOld.setEquipmentType(rvNew.getEquipmentType());
		rvOld.setVehicleNumber(rvNew.getVehicleNumber());
		rvOld.setBroker(rvNew.getBroker());
		return orderDAO.updateRentalVehicle(rvOld);
	}

	@Override

	public List<Job> getAllJob() {
		return orderDAO.getAllJob();
	}

	@Override

	public Job getJobById(String jobId) {
		Assert.notNull(jobId, "Job ID: " + SystemType.MUST_NOT_BE_NULL);
		return orderDAO.getJobById(jobId);
	}

	@Override

	public void deleteJobById(String jobId) {
		Assert.notNull(jobId, "Job ID: " + SystemType.MUST_NOT_BE_NULL);
		Job job = orderDAO.getJobById(jobId);
		for (RoadwayBill rwb : job.getRoadwayBills()) {
			deleteRoadwayBillById(rwb.getRwbId());
		}
		orderDAO.deleteJobById(jobId);
	}

	@Override
	public RoadwayBill createRoadwayBill(RoadwayBill roadwayBill) {
		Assert.notNull(roadwayBill, "Roadway Bill: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateRoadwayBill(roadwayBill);
		roadwayBill.setRwbId(null);
		Job job = roadwayBill.getJob();
		if (job.getRoadwayBills() != null && job.getRoadwayBills().size() > 0) {
			int last = 0;
			String lastRouteCode;
			if (job.getRoadwayBills().get(last).getIsArea())
				lastRouteCode = job.getRoadwayBills().get(last).getRoute().getRouteTo().getDescription().substring(0,
						3);
			else
				lastRouteCode = job.getRoadwayBills().get(last).getRoute().getRouteTo().getCode();
			if (!roadwayBill.getRoute().getRouteFrom().getCode().equals(lastRouteCode))
				throw new IllegalArgumentException("Incorrect route order!");
		}

		if (!roadwayBill.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)
				|| roadwayBill.getIsEmpty()) {
			AssetCombination assetCombination = assetService
					.findAssetCombinationById(roadwayBill.getAssetCombinationId());
			if (assetCombination.getJobAvailability().equals(SystemType.STATUS_MOVING_FREE)) {
				assetService.updateStatusAssetCombination(assetCombination.getAssetCombinationId(), null,
						SystemType.STATUS_MOVING_ENROUTE);
			} else {
				throw new IllegalArgumentException("Asset combination enroute!");
			}
			if (roadwayBill.getContainer() != null) {
				Container container = assetService.findContainerById(roadwayBill.getContainer().getContainerId());
				if (container.getMovingStatus().equals(SystemType.STATUS_MOVING_FREE)) {
					assetService.updateStatusContainer(container.getContainerId(), null,
							SystemType.STATUS_MOVING_ENROUTE);
					job.setContainer(container);
				} else {
					throw new IllegalArgumentException("Container enroute!");
				}
			}
			if (roadwayBill.getTractor() != null) {
				Tractor tractor = assetService.findTractorById(roadwayBill.getTractor().getTractorId());
				if (tractor.getMovingStatus().equals(SystemType.STATUS_MOVING_FREE)) {
					assetService.updateStatusTractor(tractor.getTractorId(), null, SystemType.STATUS_MOVING_ENROUTE);
					job.setTractor(tractor);
				} else {
					throw new IllegalArgumentException("Tractor enroute!");
				}
			}
			if (roadwayBill.getTrailer() != null) {
				Trailer trailer = assetService.findTrailerById(roadwayBill.getTrailer().getTrailerId());
				if (trailer.getMovingStatus().equals(SystemType.STATUS_MOVING_FREE)) {
					assetService.updateStatusTrailer(trailer.getTrailerId(), null, SystemType.STATUS_MOVING_ENROUTE);
					job.setTrailer(trailer);
				} else {
					throw new IllegalArgumentException("Trailer enroute!");
				}
			}
			if (roadwayBill.getTracker() != null) {
				Tracker tracker = assetService.findTrackerById(roadwayBill.getTracker().getTrackerId());
				if (tracker.getMovingStatus().equals(SystemType.STATUS_MOVING_FREE)) {
					assetService.updateStatusTracker(tracker.getTrackerId(), null, SystemType.STATUS_MOVING_ENROUTE);
					job.setTracker(tracker);
				} else {
					throw new IllegalArgumentException("Tracker enroute!");
				}
			}
			if (roadwayBill.getDriver1() != null) {
				Employee employee = employeeService.findEmployeeById(roadwayBill.getDriver1().getEmployeeId());
				if (employee.getMovingStatus().equals(SystemType.STATUS_MOVING_FREE)) {
					employeeService.updateStatusEmployee(employee.getEmployeeId(), null,
							SystemType.STATUS_MOVING_ENROUTE);
					job.setDriver1(employee);
				} else {
					throw new IllegalArgumentException("Driver-1 enroute!");
				}
			}
			if (roadwayBill.getDriver2() != null) {
				Employee employee = employeeService.findEmployeeById(roadwayBill.getDriver2().getEmployeeId());
				if (employee.getMovingStatus().equals(SystemType.STATUS_MOVING_FREE)) {
					employeeService.updateStatusEmployee(employee.getEmployeeId(), null,
							SystemType.STATUS_MOVING_ENROUTE);
					job.setDriver2(employee);
				} else {
					throw new IllegalArgumentException("Driver-2 enroute!");
				}
			}
		} else {
			RentalVehicle rentalVehicle = roadwayBill.getRentalVehicle();
			rentalVehicle = addRentalVehicle(rentalVehicle);
			job.setRentalVehicle(rentalVehicle);
			roadwayBill.setRentalVehicle(rentalVehicle);
		}
		DInvoiceStatus invoiceStatus = invoiceStatusRepository.findByValue(SystemType.INVOICE_STATUS_PENDING);
		roadwayBill.setInvoiceStatus(invoiceStatus);
		DRWBStatus rwbStatus = rwbStatusRepository.findByValue(SystemType.STATUS_RWB_BOOKED);
		roadwayBill.setRwbStatus(rwbStatus);
		roadwayBill = orderDAO.createRoadwayBill(roadwayBill);
		if (!roadwayBill.getIsEmpty())
			if (roadwayBill.getCustomerType().getValue().equals(SystemType.CUSTOMER_TYPE_CONTRACTUAL)) {
				if (roadwayBill.getTrailer() != null) {
					Company customer = roadwayBill.getCustomer();
					CompanyContract contract = contractService.getCustomerContractType(customer,
							roadwayBill.getRwbDate());
					if (contract != null) {
						Double rate = null;
						String contractType = contract.getContractType().getValue();
						if (contractType.equals(SystemType.CONTRACT_TYPE_PER_TON)) {
							if (roadwayBill.getWeightInTon() != null) {
								rate = companyService.getRouteRatePerTon(customer, roadwayBill.getRoute(),
										roadwayBill.getTrailer().getTrailerType(),
										roadwayBill.getTrailer().getTrailerSize(), roadwayBill.getRwbDate());
								roadwayBill.setRatePerTon(rate);
								rate = roadwayBill.getWeightInTon() * rate;
							} else
								throw new IllegalArgumentException("Weight in ton is required!");
						} else {
							if (contractType.equals(SystemType.CONTRACT_TYPE_DEDICATED)) {
								roadwayBill.setRatePerKm(companyService.getRouteRatePerKm(customer,
										roadwayBill.getRoute(), roadwayBill.getTrailer().getTrailerType(),
										roadwayBill.getTrailer().getTrailerSize(), roadwayBill.getRwbDate()));
							}
							rate = companyService.getRouteRate(customer, roadwayBill.getRoute(),
									roadwayBill.getTrailer().getTrailerType(),
									roadwayBill.getTrailer().getTrailerSize(), roadwayBill.getRwbDate());
						}
						if (!contractType.equals(SystemType.CONTRACT_TYPE_DEDICATED)) {
							roadwayBill.setDetentionRate(contract.getDetentionRate());
							roadwayBill.setHaltingRate(contract.getHaltingRate());
						}
						Income income = new Income();
						income.setIncomeType(incomeTypeRepository.findByValue(SystemType.INCOME_FREIGHT_RATE));
						income.setAmount(rate);
						income.setDescription("Freight rate added automatically as of " + roadwayBill.getRwbDate());
						income.setRoadwayBill(roadwayBill);
						income = addIncome(income);
						roadwayBill.setRateStatus(true);

					} else {
						throw new NoSuchElementException("No contract found!");
					}
				}
			}

		if (roadwayBill.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN)) {
			int nTyres = 0;
			if (roadwayBill.getTractor() != null)
				nTyres += roadwayBill.getTractor().getAsset().getNumberOfTyres();
			if (roadwayBill.getTrailer() != null)
				nTyres += roadwayBill.getTrailer().getAsset().getNumberOfTyres();

			Double rate = ((nTyres * SystemType.FIXED_TYRE_COST) / SystemType.FIXED_TYRE_LIFE)
					* roadwayBill.getTotalKm();

			Expense expense = new Expense();
			expense.setExpenseType(expenseTypeRepository.findByValue(SystemType.EXPENSE_TYRE_COST));
			expense.setAmount(Math.ceil(rate));
			expense.setDescription("Tyre cost added automatically");
			expense.setRoadwayBill(roadwayBill);
			expense = addExpense(expense);
		}
		roadwayBill = orderDAO.updateRoadwayBill(roadwayBill);
		updateJobKm(job);
		return roadwayBill;

	}

	public StopProgress addStopProgress(StopProgress stopProgress) {
		Assert.notNull(stopProgress, "Stop Progress: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateStopProgress(stopProgress);

		RoadwayBill rwb = stopProgress.getRoadwayBill();
		if (rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_BOOKED))
			throw new IllegalArgumentException("Please dispatch roadway bill first!");
		Job job = rwb.getJob();
		Company company = null;
		if (rwb.getCustomer() != null)
			company = rwb.getCustomer();
		else
			company = rwb.getBroker();
		if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN)) {
			Container containerOld = rwb.getContainer();
			Tractor tractorOld = rwb.getTractor();
			Trailer trailerOld = rwb.getTrailer();
			Employee driver1Old = rwb.getDriver1();
			Employee driver2Old = rwb.getDriver2();
			if (containerOld != null)
				assetService.updateStatusContainer(containerOld.getContainerId(), null, SystemType.STATUS_MOVING_FREE);
			if (tractorOld != null)
				assetService.updateStatusTractor(tractorOld.getTractorId(), null, SystemType.STATUS_MOVING_FREE);
			if (trailerOld != null)
				assetService.updateStatusTrailer(trailerOld.getTrailerId(), null, SystemType.STATUS_MOVING_FREE);
			if (driver1Old != null)
				employeeService.updateStatusEmployee(driver1Old.getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
			if (driver2Old != null)
				employeeService.updateStatusEmployee(driver2Old.getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (stopProgress.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {

			stopProgress.setContainer(null);
			stopProgress.setTractor(null);
			stopProgress.setTrailer(null);
			stopProgress.setDriver1(null);
			stopProgress.setDriver2(null);

			rwb.setContainer(null);
			rwb.setTractor(null);
			rwb.setTrailer(null);
			rwb.setTracker(null);
			rwb.setDriver1(null);
			rwb.setDriver2(null);

			job.setContainer(null);
			job.setTractor(null);
			job.setTrailer(null);
			job.setDriver1(null);
			job.setDriver2(null);

			RentalVehicle rentalVehicle = stopProgress.getRentalVehicle();
			if (rentalVehicle.getRentalVehicleId() == null) {
				rentalVehicle = orderDAO.addRentalVehicle(rentalVehicle);
			}
			rwb.setRentalVehicle(rentalVehicle);
			job.setRentalVehicle(rentalVehicle);
			stopProgress.setRentalVehicle(rentalVehicle);

		} else {
			rwb.setRentalVehicle(null);
			job.setRentalVehicle(null);
			stopProgress.setRentalVehicle(null);

			Container container = stopProgress.getContainer();
			Tractor tractor = stopProgress.getTractor();
			Trailer trailer = stopProgress.getTrailer();
			Employee driver1 = stopProgress.getDriver1();
			Employee driver2 = stopProgress.getDriver2();
			if (container != null)
				assetService.updateStatusContainer(container.getContainerId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (tractor != null)
				assetService.updateStatusTractor(tractor.getTractorId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (trailer != null)
				assetService.updateStatusTrailer(trailer.getTrailerId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (driver1 != null)
				employeeService.updateStatusEmployee(driver1.getEmployeeId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (driver2 != null)
				employeeService.updateStatusEmployee(driver2.getEmployeeId(), null, SystemType.STATUS_MOVING_ENROUTE);
			rwb.setContainer(container);
			rwb.setTractor(tractor);
			rwb.setTrailer(trailer);
			rwb.setDriver1(driver1);
			rwb.setDriver2(driver2);

			job.setContainer(container);
			job.setTractor(tractor);
			job.setTrailer(trailer);
			job.setDriver1(driver1);
			job.setDriver2(driver2);

//			stopProgress.setContainer(container);
//			stopProgress.setTractor(tractor);
//			stopProgress.setTrailer(trailer);
//			stopProgress.setDriver1(driver1);
//			stopProgress.setDriver2(driver2);
		}
		if (stopProgress.getStop() != null) {
			stopProgress.setWarehouse(stopProgress.getStop().getWarehouse());
		} else {
			Warehouse warehouse = stopProgress.getWarehouse();
			warehouse = companyService.addWarehouse(warehouse);
			company.getWarehouses().add(warehouse);
		}
		rwb = orderDAO.updateRoadwayBill(rwb);
		job = orderDAO.updateJob(job);
		stopProgress.setStopProgressId(null);
		stopProgress.setJob(job);

		stopProgress.setDetentionInHrs(
				systemUtil.differenceInDateTime(stopProgress.getArrivalTime(), stopProgress.getDepartureTime()));
		return orderDAO.addStopProgress(stopProgress);
	}

	public Stop addStop(Stop stop) {
		Assert.notNull(stop, "Stop: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateStop(stop);
		RoadwayBill rwb = stop.getRoadwayBill();
		Company company = null;
		if (rwb.getCustomer() != null)
			company = rwb.getCustomer();
		else
			company = rwb.getBroker();
		Warehouse warehouse = stop.getWarehouse();
		if (warehouse.getWarehouseId() == null) {
			warehouse = companyService.addWarehouse(warehouse);
			company.getWarehouses().add(warehouse);
		}
		stop.setStopId(null);
		stop = orderDAO.addStop(stop);
		StopProgress stopProgress = new StopProgress();
		stopProgress.setStop(stop);
		stopProgress.setRoadwayBill(stop.getRoadwayBill());
		stopProgress.setJob(rwb.getJob());
		stopProgress.setWarehouse(stop.getWarehouse());
		stopProgress.setStopProgressType(stop.getStopType());
		stopProgress.setVehicleOwnership(rwb.getVehicleOwnership());
		if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN)) {
			stopProgress.setTractor(rwb.getTractor());
			stopProgress.setTrailer(rwb.getTrailer());
			stopProgress.setContainer(rwb.getContainer());
			stopProgress.setDriver1(rwb.getDriver1());
			stopProgress.setDriver2(rwb.getDriver2());
		} else {
			stopProgress.setRentalVehicle(rwb.getRentalVehicle());
		}
		orderDAO.addStopProgress(stopProgress);
		return stop;
	}

	public Product addProduct(Product product) {
		Assert.notNull(product, "Product: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateProduct(product);

		product.setProductId(null);
		return orderDAO.addProduct(product);
	}

	public Income addIncome(Income income) {
		Assert.notNull(income, "Income: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateIncome(income);
		RoadwayBill rwb = income.getRoadwayBill();
		List<Income> incomes = null;
		if (rwb != null)
			incomes = rwb.getIncomes();
		if (incomes != null)
			for (Income incomeExisting : incomes) {
				if (income.getIncomeType().getValue().equals(incomeExisting.getIncomeType().getValue()))
					throw new EntityExistsException(
							income.getIncomeType().getValue() + ": " + SystemType.ALREADY_EXISTS);
			}
		if (income.getIncomeType().getValue().equals(SystemType.INCOME_FREIGHT_RATE)) {
			if (rwb.getRateStatus())
				throw new EntityExistsException("Freight rate: " + SystemType.ALREADY_EXISTS);
			else
				rwb.setRateStatus(true);
		}
		income.setIncomeId(null);
		return orderDAO.addIncome(income);
	}

	public Expense addExpense(Expense expense) {
		Assert.notNull(expense, "Expense: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateExpense(expense);
		RoadwayBill rwb = expense.getRoadwayBill();
		List<Expense> expenses = null;
		if (rwb != null)
			expenses = rwb.getExpenses();
		if (expenses != null)
			for (Expense expenseExisting : expenses) {
				if (expense.getExpenseType().getValue().equals(expenseExisting.getExpenseType().getValue()))
					throw new EntityExistsException(
							expense.getExpenseType().getValue() + ": " + SystemType.ALREADY_EXISTS);
			}
		expense.setExpenseId(null);
		return orderDAO.addExpense(expense);
	}

	@Override

	public RoadwayBill updateRoadwayBill(RoadwayBill rwbNew) {
		Assert.notNull(rwbNew, SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateRoadwayBill(rwbNew);

		RoadwayBill rwbOld = orderDAO.getRoadwayBillById(rwbNew.getRwbId());
		if (rwbOld.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_COMPLETED))
			throw new IllegalArgumentException("Roadway Bill is already completed!");
		Job job = rwbOld.getJob();
		if (!rwbOld.getIsEmpty() && rwbNew.getIsEmpty())
			throw new IllegalArgumentException("Cannot change to empty trip!");
		rwbOld.setBolNumber(rwbNew.getBolNumber());
		rwbOld.setBroker(rwbNew.getBroker());
		rwbOld.setCustomer(rwbNew.getCustomer());
		rwbOld.setCustomerType(rwbNew.getCustomerType());
		rwbOld.setInstructions(rwbNew.getInstructions());
		rwbOld.setLoadNumber(rwbNew.getLoadNumber());
		rwbOld.setOrderNumber(rwbNew.getOrderNumber());
		rwbOld.setPaymentMode(rwbNew.getPaymentMode());
		rwbOld.setRoute(rwbNew.getRoute());
		rwbOld.setRwbDate(rwbNew.getRwbDate());
		rwbOld.setEquipmentType(rwbNew.getEquipmentType());
		rwbOld.setVehicleOwnership(rwbNew.getVehicleOwnership());
		rwbOld.setTaxState(rwbNew.getTaxState());
		rwbOld.setTotalKm(rwbNew.getTotalKm());
		rwbOld.setTmsKm(rwbNew.getTmsKm());
		rwbOld.setTrackerKm(rwbNew.getTrackerKm());
//		rwbOld.setIsEmpty(rwbNew.getIsEmpty());
		if (rwbOld.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {
			if (!rwbOld.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_DISPATCHED))
				if (!rwbNew.getRentalVehicle().getRentalVehicleId()
						.equals(rwbOld.getRentalVehicle().getRentalVehicleId())) {
					rwbOld.setRentalVehicle(updateRentalVehicle(rwbNew.getRentalVehicle()));
					job.setRentalVehicle(rwbOld.getRentalVehicle());
				} else {
					rwbOld.setRentalVehicle(addRentalVehicle(rwbNew.getRentalVehicle()));
					job.setRentalVehicle(rwbOld.getRentalVehicle());
				}
		} else {
			if (!rwbOld.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_DISPATCHED)) {
				Container containerOld = rwbOld.getContainer();
				Tractor tractorOld = rwbOld.getTractor();
				Trailer trailerOld = rwbOld.getTrailer();
				Tracker trackerOld = rwbOld.getTracker();
				Employee driver1Old = rwbOld.getDriver1();
				Employee driver2Old = rwbOld.getDriver2();
				if (containerOld != null)
					assetService.updateStatusContainer(containerOld.getContainerId(), null,
							SystemType.STATUS_MOVING_FREE);
				if (tractorOld != null)
					assetService.updateStatusTractor(tractorOld.getTractorId(), null, SystemType.STATUS_MOVING_FREE);
				if (trailerOld != null)
					assetService.updateStatusTrailer(trailerOld.getTrailerId(), null, SystemType.STATUS_MOVING_FREE);
				if (trackerOld != null)
					assetService.updateStatusTracker(trackerOld.getTrackerId(), null, SystemType.STATUS_MOVING_FREE);
				if (driver1Old != null)
					employeeService.updateStatusEmployee(driver1Old.getEmployeeId(), null,
							SystemType.STATUS_MOVING_FREE);
				if (driver2Old != null)
					employeeService.updateStatusEmployee(driver2Old.getEmployeeId(), null,
							SystemType.STATUS_MOVING_FREE);
				Container container = rwbNew.getContainer();
				Tractor tractor = rwbNew.getTractor();
				Trailer trailer = rwbNew.getTrailer();
				Tracker tracker = rwbNew.getTracker();
				Employee driver1 = rwbNew.getDriver1();
				Employee driver2 = rwbNew.getDriver2();
				if (container != null)
					assetService.updateStatusContainer(container.getContainerId(), null,
							SystemType.STATUS_MOVING_ENROUTE);
				if (tractor != null)
					assetService.updateStatusTractor(tractor.getTractorId(), null, SystemType.STATUS_MOVING_ENROUTE);
				if (trailer != null)
					assetService.updateStatusTrailer(trailer.getTrailerId(), null, SystemType.STATUS_MOVING_ENROUTE);
				if (tracker != null)
					assetService.updateStatusTracker(tracker.getTrackerId(), null, SystemType.STATUS_MOVING_ENROUTE);
				if (driver1 != null)
					employeeService.updateStatusEmployee(driver1.getEmployeeId(), null,
							SystemType.STATUS_MOVING_ENROUTE);
				if (driver2 != null)
					employeeService.updateStatusEmployee(driver2.getEmployeeId(), null,
							SystemType.STATUS_MOVING_ENROUTE);
				rwbOld.setContainer(container);
				rwbOld.setTractor(tractor);
				rwbOld.setTrailer(trailer);
				rwbOld.setTracker(tracker);
				rwbOld.setDriver1(driver1);
				rwbOld.setDriver2(driver2);

				job.setContainer(container);
				job.setTractor(tractor);
				job.setTrailer(trailer);
				job.setTracker(tracker);
				job.setDriver1(driver1);
				job.setDriver2(driver2);

			}
		}
		if (rwbOld.getWeightInTon() != null)
			if (rwbOld.getWeightInTon() != rwbNew.getWeightInTon()) {
				List<Income> incomes = rwbOld.getIncomes();
				for (Income income : incomes) {
					if (income.getIncomeType().getValue().equals(SystemType.INCOME_FREIGHT_RATE)) {
						income.setAmount((income.getAmount() / rwbOld.getWeightInTon()) * rwbNew.getWeightInTon());
						updateIncome(income);
					}

				}
			}
		if (!rwbOld.getIsEmpty()) {
			Company customer = rwbOld.getCustomer();
			CompanyContract contract = contractService.getCustomerContractType(customer, rwbOld.getRwbDate());
			if (contract != null) {
				String contractType = contract.getContractType().getValue();
				if (!contractType.equals(SystemType.CONTRACT_TYPE_DEDICATED)) {
					rwbOld.setDetentionRate(contract.getDetentionRate());
					rwbOld.setHaltingRate(contract.getHaltingRate());
				}
			} else
				throw new IllegalArgumentException("Contract not found against given customer and roadway bill date!");
		}
		rwbOld = orderDAO.updateRoadwayBill(rwbOld);
		job = orderDAO.updateJob(job);
		updateJobKm(job);
		if (!rwbOld.getRwbStatus().getValue().equals(rwbNew.getRwbStatus().getValue()))
			updateRwbStatus(rwbOld, rwbNew.getRwbStatus().getValue());
		return rwbOld;
	}

	private void updateJobKm(Job job) {
		List<RoadwayBill> rwbs = job.getRoadwayBills();
		Double totalStdKm = 0d, totalTmsKm = 0d, totalTrackerKm = 0d;
		for (RoadwayBill roadwayBill : rwbs) {
			Double totalKm = roadwayBill.getTotalKm();
			Double tmsKm = roadwayBill.getTmsKm();
			Double trackerKm = roadwayBill.getTrackerKm();
			if (totalKm != null)
				totalStdKm += totalKm;
			if (tmsKm != null)
				totalTmsKm += tmsKm;
			if (trackerKm != null)
				totalTrackerKm += trackerKm;
		}
		job.setTmsKm(totalTmsKm);
		job.setTotalKm(totalStdKm);
		job.setTrackerKm(totalTrackerKm);
		orderDAO.updateJob(job);
	}

	public StopProgress updateStopProgress(StopProgress stopProgressNew) {
		Assert.notNull(stopProgressNew, "Stop Progress: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateStopProgress(stopProgressNew);

		StopProgress stopProgressOld = orderDAO.getStopProgressById(stopProgressNew.getStopProgressId());
		stopProgressOld.setArrivalTime(stopProgressNew.getArrivalTime());
		stopProgressOld.setDepartureTime(stopProgressNew.getDepartureTime());
		stopProgressOld.setDetentionInHrs(
				systemUtil.differenceInDateTime(stopProgressOld.getArrivalTime(), stopProgressOld.getDepartureTime()));

		RoadwayBill rwb = stopProgressOld.getRoadwayBill();
		if (rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_BOOKED))
			throw new IllegalArgumentException("Please dispatch roadway bill first!");
		Job job = rwb.getJob();
		Company company = null;
		if (rwb.getCustomer() != null)
			company = rwb.getCustomer();
		else
			company = rwb.getBroker();
		Container containerOld = rwb.getContainer();
		Tractor tractorOld = rwb.getTractor();
		Trailer trailerOld = rwb.getTrailer();
		Employee driver1Old = rwb.getDriver1();
		Employee driver2Old = rwb.getDriver2();
		if (containerOld != null)
			assetService.updateStatusContainer(containerOld.getContainerId(), null, SystemType.STATUS_MOVING_FREE);
		if (tractorOld != null)
			assetService.updateStatusTractor(tractorOld.getTractorId(), null, SystemType.STATUS_MOVING_FREE);
		if (trailerOld != null)
			assetService.updateStatusTrailer(trailerOld.getTrailerId(), null, SystemType.STATUS_MOVING_FREE);
		if (driver1Old != null)
			employeeService.updateStatusEmployee(driver1Old.getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
		if (driver2Old != null)
			employeeService.updateStatusEmployee(driver2Old.getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);

		if (stopProgressNew.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {
			stopProgressOld.setContainer(null);
			stopProgressOld.setTractor(null);
			stopProgressOld.setTrailer(null);
			stopProgressOld.setDriver1(null);
			stopProgressOld.setDriver2(null);

			rwb.setContainer(null);
			rwb.setTractor(null);
			rwb.setTrailer(null);
			rwb.setDriver1(null);
			rwb.setDriver2(null);

			job.setContainer(null);
			job.setTractor(null);
			job.setTrailer(null);
			job.setDriver1(null);
			job.setDriver2(null);

			RentalVehicle rentalVehicle = stopProgressNew.getRentalVehicle();
			if (rentalVehicle.getRentalVehicleId() == null) {
				rentalVehicle = orderDAO.addRentalVehicle(rentalVehicle);
			}
			rwb.setRentalVehicle(rentalVehicle);
			job.setRentalVehicle(rentalVehicle);
			stopProgressOld.setRentalVehicle(rentalVehicle);

		} else {
			rwb.setRentalVehicle(null);
			job.setRentalVehicle(null);
			stopProgressOld.setRentalVehicle(null);

			Container container = stopProgressNew.getContainer();
			Tractor tractor = stopProgressNew.getTractor();
			Trailer trailer = stopProgressNew.getTrailer();
			Employee driver1 = stopProgressNew.getDriver1();
			Employee driver2 = stopProgressNew.getDriver2();
			if (container != null)
				assetService.updateStatusContainer(container.getContainerId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (tractor != null)
				assetService.updateStatusTractor(tractor.getTractorId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (trailer != null)
				assetService.updateStatusTrailer(trailer.getTrailerId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (driver1 != null)
				employeeService.updateStatusEmployee(driver1.getEmployeeId(), null, SystemType.STATUS_MOVING_ENROUTE);
			if (driver2 != null)
				employeeService.updateStatusEmployee(driver2.getEmployeeId(), null, SystemType.STATUS_MOVING_ENROUTE);
			rwb.setContainer(container);
			rwb.setTractor(tractor);
			rwb.setTrailer(trailer);
			rwb.setDriver1(driver1);
			rwb.setDriver2(driver2);

			job.setContainer(container);
			job.setTractor(tractor);
			job.setTrailer(trailer);
			job.setDriver1(driver1);
			job.setDriver2(driver2);

			stopProgressOld.setContainer(container);
			stopProgressOld.setTractor(tractor);
			stopProgressOld.setTrailer(trailer);
			stopProgressOld.setDriver1(driver1);
			stopProgressOld.setDriver2(driver2);
		}
		if (stopProgressNew.getStop() != null) {
			stopProgressOld.setStop(stopProgressNew.getStop());
			stopProgressOld.setWarehouse(stopProgressNew.getStop().getWarehouse());
		} else {
			Warehouse warehouse = stopProgressNew.getWarehouse();
			warehouse = companyService.addWarehouse(warehouse);
			stopProgressOld.setWarehouse(warehouse);
			company.getWarehouses().add(warehouse);
		}
		rwb = orderDAO.updateRoadwayBill(rwb);
		job = orderDAO.updateJob(job);

		stopProgressOld = orderDAO.updateStopProgress(stopProgressOld);

		List<StopProgress> stopProgresses;
		if (rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_DISPATCHED)) {
			stopProgresses = getStopProgressesByType(rwb, SystemType.STOP_TYPE_PICKUP);
			if (areStopProgressesCompleted(stopProgresses))
				updateRwbStatus(rwb, SystemType.STATUS_RWB_LOADED);
		} else if (rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_LOADED)) {
			stopProgresses = getStopProgressesByType(rwb, SystemType.STOP_TYPE_DELIVERY);
			if (areStopProgressesCompleted(stopProgresses)) {
				updateRwbStatus(rwb, SystemType.STATUS_RWB_EMPTY);
			}
		}
		return stopProgressOld;
	}

	private void freeAssets(RoadwayBill rwb) {
		Double distance = rwb.getRoute().getAvgDistanceInKM();
		if (rwb.getAssetCombinationId() != null)
			assetService.updateStatusAssetCombination(rwb.getAssetCombinationId(), null, SystemType.STATUS_MOVING_FREE);
		if (rwb.getContainer() != null) {
			assetService.updateStatusContainer(rwb.getContainer().getContainerId(), null,
					SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getTractor() != null) {
			assetService.updateStatusTractor(rwb.getTractor().getTractorId(), null, SystemType.STATUS_MOVING_FREE);
			assetService.updateTotalKmTractor(rwb.getTractor().getTractorId(), distance);
			for (Tyre tyre : rwb.getTractor().getAsset().getTyres()) {
				assetService.updateTotalKmTyre(tyre.getTyreId(), distance);
			}
		}
		if (rwb.getTrailer() != null) {
			assetService.updateStatusTrailer(rwb.getTrailer().getTrailerId(), null, SystemType.STATUS_MOVING_FREE);
			for (Tyre tyre : rwb.getTrailer().getAsset().getTyres()) {
				assetService.updateTotalKmTyre(tyre.getTyreId(), distance);
			}
		}
		if (rwb.getTracker() != null) {
			assetService.updateStatusTracker(rwb.getTracker().getTrackerId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getDriver1() != null) {
			employeeService.updateStatusEmployee(rwb.getDriver1().getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getDriver2() != null) {
			employeeService.updateStatusEmployee(rwb.getDriver2().getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
		}
	}

	private boolean areStopProgressesCompleted(List<StopProgress> stopProgresses) {
		for (StopProgress stopProgress : stopProgresses) {
			if (stopProgress.getArrivalTime() == null || stopProgress.getDepartureTime() == null)
				return false;
		}
		return true;
	}

	@Override
	public List<StopProgress> getStopProgressesByType(RoadwayBill rwb, String stopType) {
		List<StopProgress> stopProgresses = rwb.getStopProgresses();
		return stopProgresses.stream()
				.filter(x -> x.getStopProgressType() != null && x.getStopProgressType().getValue().equals(stopType))
				.collect(Collectors.toList());
	}

	public Stop updateStop(Stop stopNew) {
		Assert.notNull(stopNew, "Stop: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateStop(stopNew);
		Stop stopOld = getStopById(stopNew.getStopId());
		stopOld.setAppointmentStartTime(stopNew.getAppointmentStartTime());
		stopOld.setAppointmentEndTime(stopNew.getAppointmentEndTime());
		Warehouse warehouseOld = stopOld.getWarehouse();
		Warehouse warehouseNew = stopNew.getWarehouse();
		if (warehouseOld.getWarehouseId().equals(warehouseNew.getWarehouseId())) {
			stopOld.setWarehouse(companyService.updateWarehouse(stopNew.getWarehouse()));
		} else {
			warehouseNew = companyService.addWarehouse(warehouseNew);
			stopOld.setWarehouse(warehouseNew);
		}
		stopOld.setReferenceNumber(stopNew.getReferenceNumber());
		stopOld.setStopSequenceNumber(stopNew.getStopSequenceNumber());
		stopOld.setStopType(stopNew.getStopType());
		StopProgress stopProgress = stopOld.getStopProgress();
		stopProgress.setWarehouse(stopNew.getWarehouse());
		orderDAO.updateStopProgress(stopProgress);
		return orderDAO.updateStop(stopOld);
	}

	public Product updateProduct(Product productNew) {
		Assert.notNull(productNew, "Product: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateProduct(productNew);

		Product productOld = orderDAO.getProductById(productNew.getProductId());
		productOld.setCategory(productNew.getCategory());
		productOld.setCommodity(productNew.getCommodity());
		productOld.setTemperature(productNew.getTemperature());
		productOld.setWeight(productNew.getWeight());
		productOld.setWeightUnit(productNew.getWeightUnit());
		return orderDAO.updateProduct(productOld);

	}

	public Income updateIncome(Income incomeNew) {
		Assert.notNull(incomeNew, "Income: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateIncome(incomeNew);
		Income incomeOld = getIncomeById(incomeNew.getIncomeId());
		incomeOld.setAmount(incomeNew.getAmount());
		incomeOld.setDescription(incomeNew.getDescription());
		return orderDAO.updateIncome(incomeNew);
	}

	public Expense updateExpense(Expense expenseNew) {
		Assert.notNull(expenseNew, "Expense: " + SystemType.MUST_NOT_BE_NULL);
		orderValidator.validateExpense(expenseNew);
		Expense expenseOld = getExpenseById(expenseNew.getExpenseId());
		expenseOld.setAmount(expenseNew.getAmount());
		expenseOld.setDescription(expenseNew.getDescription());
		return orderDAO.updateExpense(expenseNew);
	}

	@Override
	public List<RoadwayBill> getAllRoadwayBill() {
		return orderDAO.getAllRoadwayBill();
	}

	@Override

	public RoadwayBill getRoadwayBillById(String rwbId) {
		Assert.notNull(rwbId, SystemType.MUST_NOT_BE_NULL);
		return orderDAO.getRoadwayBillById(rwbId);
	}

	@Override
	public void deleteRoadwayBillById(String rwbId) {
		Assert.notNull(rwbId, "Roadway Bill ID: " + SystemType.MUST_NOT_BE_NULL);
		RoadwayBill rwb = getRoadwayBillById(rwbId);
		if (rwb.getAssetCombinationId() != null) {
			AssetCombination assetCombination = assetService.findAssetCombinationById(rwb.getAssetCombinationId());
			assetService.updateStatusAssetCombination(assetCombination.getAssetCombinationId(), null,
					SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getContainer() != null) {
			Container container = assetService.findContainerById(rwb.getContainer().getContainerId());
			assetService.updateStatusContainer(container.getContainerId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getTractor() != null) {
			Tractor tractor = assetService.findTractorById(rwb.getTractor().getTractorId());
			assetService.updateStatusTractor(tractor.getTractorId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getTrailer() != null) {
			Trailer trailer = assetService.findTrailerById(rwb.getTrailer().getTrailerId());
			assetService.updateStatusTrailer(trailer.getTrailerId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getTracker() != null) {
			Tracker tracker = assetService.findTrackerById(rwb.getTracker().getTrackerId());
			assetService.updateStatusTracker(tracker.getTrackerId(), null, SystemType.STATUS_MOVING_FREE);

		}
		if (rwb.getDriver1() != null) {
			Employee employee = employeeService.findEmployeeById(rwb.getDriver1().getEmployeeId());
			employeeService.updateStatusEmployee(employee.getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
		}
		if (rwb.getDriver2() != null) {
			Employee employee = employeeService.findEmployeeById(rwb.getDriver2().getEmployeeId());
			employeeService.updateStatusEmployee(employee.getEmployeeId(), null, SystemType.STATUS_MOVING_FREE);
		}

		orderDAO.deleteRoadwayBillById(rwbId);
	}

	@Override

	public Job getJobByIdAndStatus(String jobId, String jobStatus) {
		Assert.notNull(jobId, "Job ID: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(jobStatus, "Job Status: " + SystemType.MUST_NOT_BE_NULL);
		Job job = orderDAO.getJobById(jobId);
		if (job.getJobStatus().getValue().equals(jobStatus))
			return job;
		else
			throw new NoSuchElementException(
					"Job ID " + jobId + " with status = " + jobStatus + ": " + SystemType.NO_SUCH_ELEMENT);
	}

	@Override

	public List<Stop> getAllStop() {
		return orderDAO.getAllStop();
	}

	@Override

	public Stop getStopById(String stopId) {
		Assert.notNull(stopId, "Stop ID: " + SystemType.MUST_NOT_BE_NULL);
		return orderDAO.getStopById(stopId);
	}

	@Override

	public List<Stop> filterStop(String rwbId) {
		if (rwbId != null)
			return getRoadwayBillById(rwbId).getStops();
		return new ArrayList<>();
	}

	@Override

	public void deleteStopById(String stopId) {
		orderDAO.deleteStopById(stopId);
	}

	@Override

	public List<StopProgress> getAllStopProgress() {
		return orderDAO.getAllStopProgress();
	}

	@Override

	public StopProgress getStopProgressById(Integer stopProgressId) {
		return orderDAO.getStopProgressById(stopProgressId);
	}

	@Override

	public List<StopProgress> filterStopProgress(String rwbId) {
		if (rwbId != null)
			return getRoadwayBillById(rwbId).getStopProgresses();
		return new ArrayList<>();
	}

	@Override

	public void deleteStopProgressById(Integer stopProgressId) {
		orderDAO.deleteStopProgressById(stopProgressId);
	}

	@Override

	public List<Product> getAllProduct() {
		return orderDAO.getAllProduct();
	}

	@Override

	public Product getProductById(Integer productId) {
		return orderDAO.getProductById(productId);
	}

	@Override

	public List<Product> filterProduct(String rwbId) {
		if (rwbId != null)
			return getRoadwayBillById(rwbId).getProducts();
		return new ArrayList<>();
	}

	@Override

	public void deleteProductById(Integer productId) {
		orderDAO.deleteProductById(productId);
	}

	@Override

	public List<Income> getAllIncomes() {
		return orderDAO.getAllIncome();
	}

	@Override

	public Income getIncomeById(Integer incomeId) {
		return orderDAO.getIncomeById(incomeId);
	}

	@Override

	public List<Income> filterIncome(String rwbId) {
		if (rwbId != null)
			return getRoadwayBillById(rwbId).getIncomes();
		return new ArrayList<>();
	}

	@Override

	public void deleteIncomeById(Integer incomeId) {
		orderDAO.deleteIncomeById(incomeId);
	}

	@Override

	public List<Expense> getAllExpenses() {
		return orderDAO.getAllExpense();
	}

	@Override

	public Expense getExpenseById(Integer expenseId) {
		return orderDAO.getExpenseById(expenseId);
	}

	@Override

	public List<Expense> filterExpense(String rwbId) {
		if (rwbId != null)
			return getRoadwayBillById(rwbId).getExpenses();
		return new ArrayList<>();
	}

	@Override

	public void deleteExpenseById(Integer expenseId) {
		orderDAO.deleteExpenseById(expenseId);
	}

	@Override

	public List<JobFuelDetail> getAllJobFuelDetails() {
		return orderDAO.getAllJobFuelDetail();
	}

	@Override

	public JobFuelDetail getJobFuelDetailById(Integer jobFuelDetailId) {
		return orderDAO.getJobFuelDetailById(jobFuelDetailId);
	}

	@Override

	public List<JobFuelDetail> filterJobFuelDetail(String jobId) {
		if (jobId != null)
			return getJobById(jobId).getJobFuelDetails();
		return new ArrayList<>();
	}

	@Override

	public void deleteJobFuelDetailById(Integer jobFuelDetailId) {
		orderDAO.deleteJobFuelDetailById(jobFuelDetailId);
	}

	@Override

	public List<RoadwayBill> filterRoadwayBill(String jobId, String rwbId, Date startDate, Date endDate,
			Boolean rateStatus) {
		List<RoadwayBill> roadwayBills = roadwayBillRepository.filterRoadwayBills(rwbId, jobId, startDate, endDate,
				rateStatus);
		return roadwayBills;
	}

	@Override
	public List<String> getJobIdsInRange(Date start, Date end) {
		if (start == null || end == null)
			return null;
		return jobRepository.getJobIdsInRange(start, end);
	}

	@Override
	public boolean uploadRoadwayBillPOD(MultipartFile file, String rwbId) {
		RoadwayBill rwb = getRoadwayBillById(rwbId);
		String path = SystemType.FTP_PATH_JOB + rwbId;
		String url = null;
		if (file != null) {
			String ext = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
			String filename = rwbId + "_POD." + ext;
			try {
				url = ftpService.uploadFile(path, file, filename);
			} catch (IOException e) {
				return false;
			}
		}
		rwb.setPodUrl(url);
		return true;
	}

	@Override
	public List<Job> filterJob(String jobId, String rwbId, String tractorNumber, Date start, Date end, String status,
			String reimbursementStatus, String baseStation, Boolean isRental) {
		DJobStatus jobStatus = jobStatusRepository.findByValue(status);
		DReimbursementStatus rStatus = reimbursementStatusRepository.findByValue(reimbursementStatus);
		DBaseStation bStation = baseStationRepository.findByValue(baseStation);
		List<Job> jobs = jobRepository.filterJobs(jobId, start, end, jobStatus, rStatus, bStation);
		List<Job> filteredJobs = new ArrayList<>();

//		Tractor tractor = null;
		if (tractorNumber != null || rwbId != null) {
//			if (tractorNumber != null) {
//				tractor = assetService.findTractorByNumber(tractorNumber);
//			}
			List<RoadwayBill> roadwayBills;
			if (isRental == null || !isRental) {
				roadwayBills = orderDAO.findRoadwayBillsByIdAndTractor(rwbId, tractorNumber);
			} else {
				roadwayBills = orderDAO.findRoadwayBillsByIdAndRentalVehicle(rwbId, tractorNumber);
			}
			for (RoadwayBill roadwayBill : roadwayBills) {
				if (jobs.contains(roadwayBill.getJob()))
					filteredJobs.add(roadwayBill.getJob());
			}
			jobs = filteredJobs;
		}
		return jobs;

	}

	@Override
	public List<Tractor> getTractorsByJob(String jobId) {
		Job job = getJobById(jobId);
		List<Tractor> tractors = new ArrayList<>();
		List<RoadwayBill> roadwayBills = job.getRoadwayBills();
		roadwayBills.forEach(x -> {
			if (x.getStopProgresses() != null && x.getStopProgresses().size() > 0)
				x.getStopProgresses().forEach(y -> {
					if (!tractors.contains(y.getTractor()))
						tractors.add(y.getTractor());
				});
		});
		return tractors;
	}

	@Override
	public List<Trailer> getTrailersByJob(String jobId) {
		Job job = getJobById(jobId);
		List<Trailer> trailers = new ArrayList<>();
		List<RoadwayBill> roadwayBills = job.getRoadwayBills();
		roadwayBills.forEach(x -> {
			if (x.getStopProgresses() != null && x.getStopProgresses().size() > 0)
				x.getStopProgresses().forEach(y -> {
					if (!trailers.contains(y.getTrailer()))
						trailers.add(y.getTrailer());
				});
		});
		return trailers;
	}

	@Override
	public void updateReimbursementStatus(List<String> jobIds, String status) {
		Job job;
		for (String jobId : jobIds) {
			job = getJobById(jobId);
			if (job.getJobStatus().getValue().equals(SystemType.STATUS_JOB_OPEN))
				continue;
			job.setReimbursementStatus(reimbursementStatusRepository.findByValue(status));
			orderDAO.updateJob(job);
		}
	}

	@Override
	public List<RoadwayBill> filterRoadwayBill(Date startDate, Date endDate, String customerId, String taxState,
			String invoice, String type) {
		Company customer = null;
		DInvoiceStatus invoiceStatus = null;
		DState tax = null;
		if (customerId != null)
			customer = companyService.findCompanyById(customerId);
		if (invoice != null)
			invoiceStatus = invoiceStatusRepository.findByValue(invoice);
		if (taxState != null)
			tax = stateRepository.findByValue(taxState);
		List<RoadwayBill> roadwayBills = roadwayBillRepository.filterRoadwayBills(startDate, endDate, customer, tax,
				invoiceStatus);
		roadwayBills.removeIf(x -> !x.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_COMPLETED)
				|| !x.getJob().getJobStatus().getValue().equals(SystemType.STATUS_JOB_CLOSE));
		switch (type) {
		case SystemType.INVOICE_TYPE_PER_TRIP:
			roadwayBills.removeIf(x -> x.getRatePerTon() != null || x.getRatePerKm() != null);
			break;
		case SystemType.INVOICE_TYPE_PER_TON:
			roadwayBills.removeIf(x -> x.getRatePerTon() == null);
			break;
		case SystemType.INVOICE_TYPE_DEDICATED:
			roadwayBills.removeIf(x -> x.getRatePerKm() == null);
			break;
		default:
			break;
		}
		return roadwayBills;
	}

	@Override
	public Double getReimbursementExpenseOfRoadwayBill(RoadwayBill rwb) {
		Double total = 0d;
		List<Expense> expenses = rwb.getExpenses();
		for (Expense expense : expenses) {
			if (SystemType.REIMBURSEMENT_EXPENSES.contains(expense.getExpenseType().getValue()))
				total += expense.getAmount();
		}
		return total;
	}

	@Override
	public Double getExpenseOfRoadwayBill(RoadwayBill rwb) {
		Double total = 0d;
		List<Expense> expenses = rwb.getExpenses();
		for (Expense expense : expenses) {
			total += expense.getAmount();
		}
		return total;
	}

	@Override
	public Double getIncomeOfRoadwayBill(RoadwayBill rwb) {
		Double total = 0d;
		List<Income> incomes = rwb.getIncomes();
		for (Income income : incomes) {
			total += income.getAmount();
		}
		return total;
	}

	@Override
	public void updateInvoiceStatus(RoadwayBill rwb, String invoiceStatus) {
		rwb.setInvoiceStatus(invoiceStatusRepository.findByValue(invoiceStatus));
		roadwayBillRepository.save(rwb);
	}

	@Override
	public void updateRwbStatus(RoadwayBill rwb, String rwbStatus) {
		switch (rwbStatus) {
		case SystemType.STATUS_RWB_DISPATCHED:
			if (!rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_BOOKED))
				throw new IllegalArgumentException("Cannot change to given status!");
			break;
		case SystemType.STATUS_RWB_LOADED:
			if (!rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_DISPATCHED))
				throw new IllegalArgumentException("Cannot change to given status!");
			break;
		case SystemType.STATUS_RWB_EMPTY:
			if (!rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_LOADED))
				throw new IllegalArgumentException("Cannot change to given status!");
			else {
				freeAssets(rwb);
			}
			break;
		case SystemType.STATUS_RWB_COMPLETED:
			if (!rwb.getRwbStatus().getValue().equals(SystemType.STATUS_RWB_EMPTY))
				throw new IllegalArgumentException("Cannot change to given status!");
			else {
				freeAssets(rwb);
				calculateDetentionAndHalting(rwb);
			}
			break;
		default:
			throw new IllegalArgumentException("Cannot change to given status!");
		}
		rwb.setRwbStatus(rwbStatusRepository.findByValue(rwbStatus));
		orderDAO.updateRoadwayBill(rwb);
	}

	private void calculateDetentionAndHalting(RoadwayBill rwb) {
		int totalDetention = 0, totalHalting = 0;
		if (rwb.getIsEmpty())
			return;
		List<StopProgress> pickups = getStopProgressesByType(rwb, SystemType.STOP_TYPE_PICKUP);
		List<StopProgress> deliveries = getStopProgressesByType(rwb, SystemType.STOP_TYPE_DELIVERY);
		for (StopProgress stopProgress : pickups) {
			if (stopProgress.getDetentionInHrs() >= 24) {
				totalHalting += stopProgress.getDetentionInHrs() / 24;
			}
		}
		for (StopProgress stopProgress : deliveries) {
			if (stopProgress.getDetentionInHrs() >= 24) {
				totalDetention += stopProgress.getDetentionInHrs() / 24;
			}
		}
		Income income = null;
		if (totalDetention > 0 && rwb.getDetentionRate() != null) {
			income = new Income();
			income.setAmount(totalDetention * rwb.getDetentionRate());
			income.setIncomeType(incomeTypeRepository.findByValue(SystemType.INCOME_DETENTION_CHARGES));
			income.setDescription("Detention charges");
			income.setRoadwayBill(rwb);
			try {
				addIncome(income);
			} catch (EntityExistsException e) {
				List<Income> incomes = rwb.getIncomes();
				for (Income incomeExisting : incomes) {
					if (incomeExisting.getIncomeType().getValue().equals(SystemType.INCOME_DETENTION_CHARGES)) {
						income = incomeExisting;
						income.setAmount(totalDetention * rwb.getDetentionRate());
						orderDAO.updateIncome(income);
						break;
					}
				}
			}
		}
		if (totalHalting > 0 && rwb.getHaltingRate() != null) {
			income = new Income();
			income.setAmount(totalHalting * rwb.getHaltingRate());
			income.setIncomeType(incomeTypeRepository.findByValue(SystemType.INCOME_HALTING_CHARGES));
			income.setDescription("Halting charges");
			income.setRoadwayBill(rwb);
			try {
				addIncome(income);
			} catch (EntityExistsException e) {
				List<Income> incomes = rwb.getIncomes();
				for (Income incomeExisting : incomes) {
					if (incomeExisting.getIncomeType().getValue().equals(SystemType.INCOME_HALTING_CHARGES)) {
						income = incomeExisting;
						income.setAmount(totalHalting * rwb.getHaltingRate());
						orderDAO.updateIncome(income);
						break;
					}
				}
			}
		}

	}

	@Override
	public long countJobByStatus(DJobStatus status) {
		return orderDAO.countJobByStatus(status);
	}

	@Override
	public long countRwbByStatus(DRWBStatus status) {
		return orderDAO.countRoadwayBillByStatus(status);
	}

	@Override
	public void deleteRoadwayBillPOD(String rwbId) {
		RoadwayBill rwb = getRoadwayBillById(rwbId);
		String podUrl = rwb.getPodUrl();
		rwb.setPodUrl(null);
		orderDAO.updateRoadwayBill(rwb);
		if (podUrl == null || podUrl.isBlank())
			try {
				HttpClient client = HttpClient.newHttpClient();
				HttpRequest request = HttpRequest.newBuilder(new URI(podUrl)).DELETE().build();
				client.send(request, null);
			} catch (Exception e) {
				System.out.println("File not deleted from FTP server");
			}
	}

	@Override
	public List<Job> getClosedJobInRange(Date start, Date end) {
		return orderDAO.getClosedJobInRange(start, end);
	}

	@Override
	public Double getIncomeOfJob(Job job) {
		Double income = 0d;
		List<RoadwayBill> rwbs = job.getRoadwayBills();
		for (RoadwayBill rwb : rwbs) {
			income += getIncomeOfRoadwayBill(rwb);
		}
		return income;
	}

	@Override
	public Double getExpenseOfJob(Job job) {
		Double expense = 0d;
		List<RoadwayBill> rwbs = job.getRoadwayBills();
		for (RoadwayBill rwb : rwbs) {
			expense += getExpenseOfRoadwayBill(rwb);
		}
		return expense;
	}

	@Override
	public Double getFuelExpenseOfJob(Job job) {
		Double total = 0d;
		List<JobFuelDetail> fuel = job.getJobFuelDetails();
		for (JobFuelDetail jfd : fuel) {
			total += jfd.getTotalAmount();
		}
		return total;

	}

}
