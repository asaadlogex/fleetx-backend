package com.fleetX.service.implementation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.ICompanyDAO;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.Route;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.dropdown.DBusinessType;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.dropdown.DSupplierType;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.order.Warehouse;
import com.fleetX.repository.AddressRepository;
import com.fleetX.repository.CompanyRepository;
import com.fleetX.repository.ContactRepository;
import com.fleetX.repository.DBusinessTypeRepository;
import com.fleetX.repository.DContractTypeRepository;
import com.fleetX.repository.WarehouseRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IDropDownService;
import com.fleetX.validator.ICompanyValidator;

@Service
@Transactional
public class CompanyService implements ICompanyService {

	@Autowired
	ICompanyDAO companyDAO;
	@Autowired
	IDropDownService dropDownService;
	@Autowired
	DBusinessTypeRepository businessTypeRepository;
	@Autowired
	WarehouseRepository warehouseRepository;
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	ContactRepository contactRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	DContractTypeRepository contractTypeRepository;
	@Autowired
	IAdminService adminService;
	@Autowired
	IContractService contractService;
	@Autowired
	ICompanyValidator companyValidator;
	@Autowired
	SystemUtil systemUtil;

	@Override
	public List<Company> findAllCompany() {
		return companyDAO.findAllCompany();
	}

	@Override
	public Company findCompanyById(String companyId) {
		Assert.notNull(companyId, "Company ID: " + SystemType.MUST_NOT_BE_NULL);
		return companyDAO.findCompanyById(companyId);
	}

	@Override
	public Company addCompany(Company company) throws Exception {
		Assert.notNull(company, "Company: " + SystemType.MUST_NOT_BE_NULL);
		companyValidator.validateCompany(company);

		company.setCompanyId(null);
		company.getCompanyAddresses().stream().forEach(x -> {
			x.setAddressId(null);
			x = addressRepository.save(x);
		});

		company.getCompanyContacts().stream().forEach(x -> {
			x.setContactId(null);
			x = contactRepository.save(x);
		});
		company = companyDAO.createCompany(company);
		return company;
	}

	@Override
	public Company updateCompany(Company companyNew) throws Exception {
		Assert.notNull(companyNew, "Company: " + SystemType.MUST_NOT_BE_NULL);
		companyValidator.validateCompany(companyNew);

		Company companyOld = companyDAO.findCompanyById(companyNew.getCompanyId());
		companyOld.setCompanyCode(companyNew.getCompanyCode());
		companyOld.setCompanyName(companyNew.getCompanyName());
		companyOld.setCompanyFormation(companyNew.getCompanyFormation());
		companyOld.setBra(companyNew.getBra());
		companyOld.setSra(companyNew.getSra());
		companyOld.setBusinessType(companyNew.getBusinessType());
		companyOld.setKra(companyNew.getKra());
		companyOld.setNtnNumber(companyNew.getNtnNumber());
		companyOld.setPra(companyNew.getPra());
		companyOld.setStatus(companyNew.getStatus());
		companyOld.setStrnNumber(companyNew.getStrnNumber());
		companyOld.setSupplierType(companyNew.getSupplierType());
		companyOld.setCompanyType(companyNew.getCompanyType());

		List<Address> addressesOld = companyOld.getCompanyAddresses();
		List<Address> addressesNew = companyNew.getCompanyAddresses();
		addressesNew = adminService.adjustAddresses(addressesOld, addressesNew);

		List<Contact> contactsOld = companyOld.getCompanyContacts();
		List<Contact> contactsNew = companyNew.getCompanyContacts();
		contactsNew = adminService.adjustContacts(contactsOld, contactsNew);

		companyOld.setCompanyAddresses(addressesNew);
		companyOld.setCompanyContacts(contactsNew);
		return companyDAO.updateCompany(companyOld);
	}

	@Override
	public void deleteCompany(String companyId) throws Exception {
		Assert.notNull(companyId, "Company ID: " + SystemType.MUST_NOT_BE_NULL);
		companyDAO.deleteCompany(companyId);
	}

	@Override
	public List<Company> filterCompanies(String businessType, String status, String supplierType,
			Boolean haveContract) {
		DBusinessType bType = null;
		DSupplierType sType = null;
		if (businessType != null) {
			List<DBusinessType> businessTypes = dropDownService.retrieveDBusinessType();
			for (DBusinessType bt : businessTypes) {
				if (bt.getValue().equals(businessType)) {
					bType = bt;
				}
			}
		}
		if (supplierType != null) {
			List<DSupplierType> supplierTypes = dropDownService.retrieveDSupplierType();
			for (DSupplierType st : supplierTypes) {
				if (st.getValue().equals(supplierType)) {
					sType = st;
				}
			}
		}
		Company companyExample = new Company();
		companyExample.setBusinessType(bType);
		companyExample.setStatus(status);
		companyExample.setSupplierType(sType);
		List<Company> companyList = companyDAO.filterCompanyByExample(Example.of(companyExample));
		List<Company> companyListFinal = new ArrayList<>();

		if (haveContract != null) {
			if (haveContract) {
				for (Company company : companyList) {
					for (CompanyContract contract : company.getCompanyContracts()) {
						if (contract.getStatus().equals(SystemType.STATUS_ACTIVE))
							companyListFinal.add(company);
					}
				}
				return companyListFinal;
			} else if (!haveContract) {
				companyListFinal = new ArrayList<>(companyList);
				for (Company company : companyList) {
					for (CompanyContract contract : company.getCompanyContracts()) {
						if (contract.getStatus().equals(SystemType.STATUS_ACTIVE))
							companyListFinal.remove(company);
					}
				}
				return companyListFinal;
			}
		}
		return companyList;
	}

	@Override
	public List<Warehouse> filterWarehouse(String companyId) {
		if (companyId != null)
			return companyDAO.findCompanyById(companyId).getWarehouses();
		else
			return companyDAO.getAllWarehouse();
	}

	@Override
	public Warehouse addWarehouse(Warehouse warehouse) {
		Assert.notNull(warehouse, "Warehouse Details: " + SystemType.MUST_NOT_BE_NULL);
		companyValidator.validateWarehouse(warehouse);
		warehouse.setWarehouseId(null);
		warehouse.getAddresses().stream().forEach(x -> {
//			x.setAddressId(null);
			if (x.getAddressId() == null)
				x = addressRepository.save(x);
		});
		return companyDAO.addWarehouse(warehouse);
	}

	@Override
	public Warehouse updateWarehouse(Warehouse warehouseNew) {
		Assert.notNull(warehouseNew, "Warehouse Details: " + SystemType.MUST_NOT_BE_NULL);
		companyValidator.validateWarehouse(warehouseNew);

		Warehouse warehouseOld = companyDAO.getWarehouseById(warehouseNew.getWarehouseId());
		List<Address> addressesOld = warehouseOld.getAddresses();
		List<Address> addressesNew = warehouseNew.getAddresses();
		addressesNew = adminService.adjustAddresses(addressesOld, addressesNew);
		warehouseOld.setAddresses(addressesNew);

		warehouseOld.setPhone(warehouseNew.getPhone());
		warehouseOld.setEmail(warehouseNew.getEmail());

		return companyDAO.updateWarehouse(warehouseOld);
	}

	@Override
	public List<Warehouse> getAllWarehouse() {
		return companyDAO.getAllWarehouse();
	}

	@Override
	public Warehouse getWarehouseById(Integer warehouseId) {
		Assert.notNull(warehouseId, "Warehouse ID: " + SystemType.MUST_NOT_BE_NULL);
		return companyDAO.getWarehouseById(warehouseId);
	}

	@Override
	public void deleteWarehouseById(Integer warehouseId) {
		Assert.notNull(warehouseId, "Warehouse ID: " + SystemType.MUST_NOT_BE_NULL);
		companyDAO.deleteWarehouseById(warehouseId);
	}

	@Override
	public Double getRouteRate(Company company, Route route, DTrailerType trailerType, DTrailerSize trailerSize,
			Date rwbDate) {
		List<CompanyContract> contracts = company.getCompanyContracts();
		for (CompanyContract companyContract : contracts) {
			if (systemUtil.isDateInBetween(rwbDate, companyContract.getContractStart(),
					companyContract.getContractEnd())) {
				for (RouteRateContract rrc : companyContract.getRouteRateContracts()) {
					if (rrc.getRoute().equals(route))
						try {
							for (RouteRate routeRate : rrc.getRouteRates()) {
								if (routeRate.getTrailerType().getValue().equals(trailerType.getValue())
										&& routeRate.getTrailerSize().getValue().equals(trailerSize.getValue()))
									if (systemUtil.isDateInBetween(rwbDate, routeRate.getEffectiveFrom(),
											routeRate.getEffectiveTill()))
										return routeRate.getTotalAmount();
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
				}
			}
		}
		throw new NoSuchElementException("Cannot find rate for the given route against RWB date");
	}

	@Override
	public Double getFuelRateByDate(Company supplier, Date fuelDate) {
		List<FuelSupplierContract> contracts = supplier.getCompanyFuelContracts();
		for (FuelSupplierContract contract : contracts) {
			for (FuelRate fuelRate : contract.getFuelRates()) {
				if (systemUtil.isDateInBetween(fuelDate, fuelRate.getEffectiveFrom(), fuelRate.getEffectiveTill())) {
					return fuelRate.getRatePerLitre();
				}

			}
		}
		throw new NoSuchElementException("Cannot find fuel rate from the given supplier against Fuel Date");
	}

	@Override
	public List<Company> findCompanyByType(String type) {
		Assert.notNull(type, "Company type: " + SystemType.MUST_NOT_BE_NULL);
		return companyRepository.findAllByType(type);
	}

	@Override
	public Double getRouteRatePerTon(Company company, Route route, DTrailerType trailerType, DTrailerSize trailerSize,
			Date rwbDate) {
		List<CompanyContract> contracts = company.getCompanyContracts();
		for (CompanyContract companyContract : contracts) {
			if (systemUtil.isDateInBetween(rwbDate, companyContract.getContractStart(),
					companyContract.getContractEnd())) {
				for (RouteRateContract rrc : companyContract.getRouteRateContracts()) {
					if (rrc.getRoute().equals(route))
						try {
							for (RouteRate routeRate : rrc.getRouteRates()) {
								if (routeRate.getTrailerType().getValue().equals(trailerType.getValue())
										&& routeRate.getTrailerSize().getValue().equals(trailerSize.getValue()))
									if (systemUtil.isDateInBetween(rwbDate, routeRate.getEffectiveFrom(),
											routeRate.getEffectiveTill()))
										return routeRate.getRatePerTon();
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
				}
			}
		}
		throw new NoSuchElementException("Cannot find rate for the given route against RWB date");
	}

	@Override
	public Double getRouteRatePerKm(Company company, Route route, DTrailerType trailerType, DTrailerSize trailerSize,
			Date rwbDate) {
		List<CompanyContract> contracts = company.getCompanyContracts();
		for (CompanyContract companyContract : contracts) {
			if (systemUtil.isDateInBetween(rwbDate, companyContract.getContractStart(),
					companyContract.getContractEnd())) {
				for (RouteRateContract rrc : companyContract.getRouteRateContracts()) {
					if (rrc.getRoute().equals(route))
						try {
							for (RouteRate routeRate : rrc.getRouteRates()) {
								if (routeRate.getTrailerType().getValue().equals(trailerType.getValue())
										&& routeRate.getTrailerSize().getValue().equals(trailerSize.getValue()))
									if (systemUtil.isDateInBetween(rwbDate, routeRate.getEffectiveFrom(),
											routeRate.getEffectiveTill()))
										return routeRate.getRatePerKm();
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
				}
			}
		}
		throw new NoSuchElementException("Cannot find rate for the given route against RWB date");
	}

	@Override
	public List<Company> findCompanyByContractType(String value) {
		DContractType contractType=contractTypeRepository.findByValue(value);
		List<Company> companies=new ArrayList<>();
		if(contractType==null) throw new NoSuchElementException("Invalid contract type!");
		List<CompanyContract> contracts=contractService.findCompanyContractByType(contractType);
		for (CompanyContract companyContract : contracts) {
			companies.add(companyContract.getCustomer());
		}
		return companies;
	}

}
