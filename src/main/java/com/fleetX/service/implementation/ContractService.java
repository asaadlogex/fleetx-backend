package com.fleetX.service.implementation;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.IContractDAO;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.repository.FuelRateRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IDropDownService;
import com.fleetX.service.IFTPService;
import com.fleetX.validator.IContractValidator;

@Service
@Transactional
public class ContractService implements IContractService {

	@Autowired
	IContractDAO contractDAO;
	@Autowired
	IDropDownService dropDownService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IAdminService adminService;
	@Autowired
	IAssetService assetService;
	@Autowired
	FuelRateRepository fuelRateRepository;
	@Autowired
	IContractValidator contractValidator;
	@Autowired
	IFTPService ftpService;
	@Autowired
	SystemUtil systemUtil;

	@Override
	public CompanyContract addCompanyContract(CompanyContract companyContract) throws ParseException {
		Assert.notNull(companyContract, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateCompanyContract(companyContract);

		List<CompanyContract> existingContracts = filterCompanyContract(companyContract.getCustomer().getCompanyId(),
				companyContract.getAdminCompany().getAdminCompanyId(), null, SystemType.STATUS_ACTIVE);
		if (existingContracts.size() > 0)
			throw new EntityExistsException("Company Contract: " + SystemType.ALREADY_EXISTS);
		if (companyContract.getContractEnd() != null) {
			if (systemUtil.isDatePassed(companyContract.getContractEnd()))
				companyContract.setStatus(SystemType.STATUS_INACTIVE);
			else
				companyContract.setStatus(SystemType.STATUS_ACTIVE);
		} else
			companyContract.setStatus(SystemType.STATUS_ACTIVE);
		List<AssetCombination> assetCombinations = companyContract.getAssetCombinations();
		companyContract = contractDAO.addCompanyContract(companyContract);
		if (companyContract.getContractType().getValue().equals(SystemType.CONTRACT_TYPE_DEDICATED)) {
			if (assetCombinations != null && assetCombinations.size() > 0)
				for (AssetCombination assetCombination : assetCombinations) {
					assetCombination.setCompanyContract(companyContract);
					if (assetCombination.getAvailability().equals(SystemType.STATUS_IN_USE))
						throw new IllegalArgumentException("Asset combination: " + SystemType.ALREADY_EXISTS);
					assetService.updateStatusAssetCombination(assetCombination.getAssetCombinationId(),
							SystemType.STATUS_IN_USE, null);
				}
		}
		return companyContract;
	}

	public RouteRateContract addRouteRateContract(RouteRateContract routeRateContract) throws ParseException {
		Assert.notNull(routeRateContract, "Route Rate Contract: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(routeRateContract.getCompanyContract(), "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateRouteRateContract(routeRateContract);
		CompanyContract companyContract = routeRateContract.getCompanyContract();
		for (RouteRateContract rrc : companyContract.getRouteRateContracts()) {
			if (rrc.getRoute().equals(routeRateContract.getRoute())) {
				List<RouteRate> routeRates = rrc.getRouteRates();
				RouteRate routeRate = routeRateContract.getRouteRates().get(0);
				if (routeRates != null && routeRates.size() > 0) {
					for (RouteRate existingRouteRate : routeRates) {
						if (existingRouteRate.getTrailerType().getValue().equals(routeRate.getTrailerType().getValue())
								&& existingRouteRate.getTrailerSize().getValue()
										.equals(routeRate.getTrailerSize().getValue()))
							if (!systemUtil.isStartBeforeEnd(existingRouteRate.getEffectiveTill(),
									routeRate.getEffectiveFrom()))
								throw new IllegalArgumentException("Incorrect date order!");
					}
				}
				routeRate.setRouteRateId(null);
				routeRate.setRouteRateContract(rrc);
				rrc.getRouteRates().add(contractDAO.addRouteRate(routeRate));
				return rrc;
			}
		}
		List<RouteRate> routeRates = routeRateContract.getRouteRates();
		routeRateContract = contractDAO.addRouteRateContract(routeRateContract);
		for (RouteRate routeRate : routeRates) {
			routeRate.setRouteRateId(null);
			routeRate.setRouteRateContract(routeRateContract);
			routeRate = contractDAO.addRouteRate(routeRate);
		}
		return routeRateContract;
	}

	@Override
	public List<CompanyContract> getAllCompanyContract() {
		return contractDAO.getAllCompanyContract();
	}

	@Override
	public CompanyContract updateCompanyContract(CompanyContract companyContractNew) throws ParseException {
		Assert.notNull(companyContractNew, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateCompanyContract(companyContractNew);

		CompanyContract companyContractOld = contractDAO
				.findCompanyContractById(companyContractNew.getCompanyContractId());
		if (!companyContractOld.getContractType().getValue().equals(companyContractNew.getContractType().getValue()))
			throw new IllegalArgumentException("Company Contract: " + SystemType.ALREADY_EXISTS);
		companyContractOld.setContractStart(companyContractNew.getContractStart());
		companyContractOld.setContractEnd(companyContractNew.getContractEnd());
		if (companyContractOld.getContractEnd() != null) {
			if (systemUtil.isDatePassed(companyContractOld.getContractEnd()))
				companyContractOld.setStatus(SystemType.STATUS_INACTIVE);
			else
				companyContractOld.setStatus(SystemType.STATUS_ACTIVE);
		} else
			companyContractOld.setStatus(SystemType.STATUS_ACTIVE);
//		companyContractOld.setCustomer(companyContractNew.getCustomer());
		companyContractOld.setFixedRate(companyContractNew.getFixedRate());
		companyContractOld.setDetentionRate(companyContractNew.getDetentionRate());
		companyContractOld.setHaltingRate(companyContractNew.getHaltingRate());

		companyContractOld = contractDAO.updateCompanyContract(companyContractOld);

		if (companyContractOld.getContractType().getValue().equals(SystemType.DEDICATED)) {
			List<AssetCombination> assetCombinationsOld = companyContractOld.getAssetCombinations();
			List<AssetCombination> assetCombinationsNew = companyContractNew.getAssetCombinations();
			List<AssetCombination> assetCombinationsTBD = new ArrayList<>(assetCombinationsOld);
			List<AssetCombination> assetCombinationsTBA = new ArrayList<>(assetCombinationsNew);
			for (AssetCombination assetCombinationNew : assetCombinationsNew) {
				for (AssetCombination assetCombinationOld : assetCombinationsOld) {
					if (assetCombinationNew.getAssetCombinationId() != null && assetCombinationNew
							.getAssetCombinationId().equals(assetCombinationOld.getAssetCombinationId())) {
						assetCombinationsTBA.remove(assetCombinationNew);
						assetCombinationsTBD.remove(assetCombinationOld);
					}
				}
			}
			for (AssetCombination assetCombinationNew : assetCombinationsTBA) {
				if (assetCombinationNew.getStatus().equals(SystemType.STATUS_IN_USE))
					throw new EntityExistsException("Asset Combinaton: " + SystemType.ALREADY_EXISTS);
				assetCombinationNew.setAvailability(SystemType.STATUS_IN_USE);
				assetCombinationNew.setCompanyContract(companyContractOld);
				assetService.updateAssetCombination(assetCombinationNew);
			}
			contractDAO.removeAssetCombination(assetCombinationsTBD);
		}

		return companyContractOld;
	}

	public RouteRateContract updateRouteRateContract(RouteRateContract routeRateContractNew) throws ParseException {
		Assert.notNull(routeRateContractNew, "Route Rate Contract: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(routeRateContractNew.getCompanyContract(), "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateRouteRateContract(routeRateContractNew);

		RouteRateContract routeRateContractOld = contractDAO
				.findRouteRateContractById(routeRateContractNew.getRouteRateContractId());
		routeRateContractOld.setRoute(routeRateContractNew.getRoute());
		routeRateContractOld.setCompanyContract(routeRateContractNew.getCompanyContract());
		routeRateContractOld.setStatus(routeRateContractNew.getStatus());
		List<RouteRate> routeRateOld = routeRateContractOld.getRouteRates();
		List<RouteRate> routeRateNew = routeRateContractNew.getRouteRates();
		List<RouteRate> routeRateTBD = new ArrayList<>(routeRateOld);
		List<RouteRate> routeRateTBA = new ArrayList<>(routeRateNew);
		for (RouteRate rrNew : routeRateNew) {
			for (RouteRate rrOld : routeRateOld) {
				if (rrNew.getRouteRateId() != null && rrNew.getRouteRateId().equals(rrOld.getRouteRateId())) {
					routeRateTBA.remove(rrNew);
					routeRateTBD.remove(rrOld);
					rrOld = updateRouteRate(rrNew);
				}
			}
		}
		for (RouteRate routeRate : routeRateTBA) {
			routeRate.setRouteRateContract(routeRateContractOld);
			routeRate.setRouteRateId(null);
			routeRate = contractDAO.addRouteRate(routeRate);
		}
		contractDAO.removeRouteRate(routeRateTBD);
		return routeRateContractOld;
	}

	private RouteRate updateRouteRate(RouteRate routeRateNew) {
		Assert.notNull(routeRateNew, "Route Rate: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(routeRateNew.getRouteRateContract(), "Route Rate Contract: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(routeRateNew.getRouteRateContract().getCompanyContract(),
				"Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateRouteRate(routeRateNew, routeRateNew.getRouteRateContract().getCompanyContract());

		RouteRate routeRateOld = contractDAO.findRouteRateById(routeRateNew.getRouteRateId());
		routeRateOld.setTrailerType(routeRateNew.getTrailerType());
		routeRateOld.setTrailerSize(routeRateNew.getTrailerSize());
		routeRateOld.setWeightLow(routeRateNew.getWeightLow());
		routeRateOld.setWeightHigh(routeRateNew.getWeightHigh());
		routeRateOld.setEffectiveFrom(routeRateNew.getEffectiveFrom());
		routeRateOld.setEffectiveTill(routeRateNew.getEffectiveTill());
		routeRateOld.setRatePerKm(routeRateNew.getRatePerKm());
		routeRateOld.setRatePerTon(routeRateNew.getRatePerTon());
		routeRateOld.setStatus(routeRateNew.getStatus());
		routeRateOld.setTotalAmount(routeRateNew.getTotalAmount());
		return contractDAO.updateRouteRate(routeRateOld);
	}

	@Override
	public void deleteCompanyContractById(String companyContractId) {
		Assert.notNull(companyContractId, "Company Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		CompanyContract companyContract = contractDAO.findCompanyContractById(companyContractId);
		for (AssetCombination assetCombination : companyContract.getAssetCombinations()) {
			assetCombination.setCompanyContract(null);
			assetService.updateStatusAssetCombination(assetCombination.getAssetCombinationId(),
					SystemType.STATUS_AVAILABLE, null);
		}
		contractDAO.deleteCompanyContractById(companyContractId);
	}

	@Override
	public CompanyContract findCompanyContractById(String companyContractId) {
		Assert.notNull(companyContractId, "Company Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		return contractDAO.findCompanyContractById(companyContractId);
	}

	@Override
	public List<CompanyContract> filterCompanyContract(String cbId, String acId, String contractType, String status) {
		Company customer = null;
		AdminCompany adminCompany = null;
		DContractType cType = null;
		if (cbId != null) {
			customer = companyService.findCompanyById(cbId);
		}
		if (acId != null) {
			adminCompany = adminService.findAdminCompanyById(acId);
		}
		if (contractType != null) {
			List<DContractType> contractTypes = dropDownService.retrieveDContractType();
			for (DContractType dContractType : contractTypes) {
				if (dContractType.getValue().equals(contractType)) {
					cType = dContractType;
				}
			}
		}
		CompanyContract companyContractExample = new CompanyContract();
		companyContractExample.setAdminCompany(adminCompany);
		companyContractExample.setCustomer(customer);
		companyContractExample.setContractType(cType);
		companyContractExample.setStatus(status);
		return contractDAO.filterCompanyContract(Example.of(companyContractExample));
	}

	// FUEL SUPPLIER CONTRACTS
	@Override
	public FuelSupplierContract addFuelSupplierContract(FuelSupplierContract fuelSupplierContract) {
		Assert.notNull(fuelSupplierContract, "Fuel Supplier Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateFuelSupplierContract(fuelSupplierContract);

		List<FuelRate> fuelRates = fuelSupplierContract.getFuelRates();
		fuelSupplierContract.setFscId(null);
		fuelSupplierContract = contractDAO.addFuelSupplierContract(fuelSupplierContract);
		for (FuelRate fuelRate : fuelRates) {
			fuelRate.setFuelSupplierContract(fuelSupplierContract);
			addFuelRate(fuelRate);
		}
		return fuelSupplierContract;
	}

	private void addFuelRate(FuelRate fuelRate) {
		Assert.notNull(fuelRate, "Fuel Rate: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateFuelRate(fuelRate);

		fuelRate.setFuelRateId(null);
		fuelRateRepository.save(fuelRate);
	}

	@Override
	public List<FuelSupplierContract> getAllFuelSupplierContract() {
		return contractDAO.getAllFuelSupplierContract();
	}

	@Override
	public List<FuelSupplierContract> filterFuelSupplierContract(
			Example<FuelSupplierContract> fuelSupplierContractExample) {
		Assert.notNull(fuelSupplierContractExample, SystemType.MUST_NOT_BE_NULL);
		return contractDAO.filterFuelSupplierContract(fuelSupplierContractExample);
	}

	@Override
	public FuelSupplierContract updateFuelSupplierContract(FuelSupplierContract fscNew) {
		Assert.notNull(fscNew, "Fuel Supplier Contract: " + SystemType.MUST_NOT_BE_NULL);
		contractValidator.validateFuelSupplierContract(fscNew);

		FuelSupplierContract fscOld = findFuelSupplierContractById(fscNew.getFscId());
		if (!fscNew.getAdminCompany().getAdminCompanyId().equals(fscOld.getAdminCompany().getAdminCompanyId())
				|| !fscNew.getSupplier().getCompanyId().equals(fscOld.getSupplier().getCompanyId()))
			throw new IllegalArgumentException("Something: " + SystemType.MUST_NOT_BE_NULL);
		fscOld.setFscStatus(fscNew.getFscStatus());
		fscOld = contractDAO.updateFuelSupplierContract(fscOld);
		List<FuelRate> fuelRatesOld = fscOld.getFuelRates();
		List<FuelRate> fuelRatesNew = fscNew.getFuelRates();
		List<FuelRate> fuelRatesTBD = new ArrayList<>(fuelRatesOld);
		List<FuelRate> fuelRatesTBA = new ArrayList<>(fuelRatesNew);
		for (FuelRate fuelRateNew : fuelRatesNew) {
			for (FuelRate fuelRateOld : fuelRatesOld) {
				if (fuelRateNew.getFuelRateId() != null
						&& fuelRateNew.getFuelRateId().equals(fuelRateOld.getFuelRateId())) {
					fuelRatesTBA.remove(fuelRateNew);
					fuelRatesTBD.remove(fuelRateOld);
					fuelRateOld.setEffectiveFrom(fuelRateNew.getEffectiveFrom());
					fuelRateOld.setEffectiveTill(fuelRateNew.getEffectiveTill());
					fuelRateOld.setRatePerLitre(fuelRateNew.getRatePerLitre());
				}
			}
		}
		for (FuelRate fuelRateNew : fuelRatesTBA) {
			fuelRateNew.setFuelSupplierContract(fscOld);
			addFuelRate(fuelRateNew);
		}
		contractDAO.removeFuelRates(fuelRatesTBD);
		return fscOld;
	}

	@Override
	public void deleteFuelSupplierContractById(String fuelSupplierContractId) {
		Assert.notNull(fuelSupplierContractId, "Fuel Supplier Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		contractDAO.deleteFuelSupplierContractById(fuelSupplierContractId);
	}

	@Override
	public FuelSupplierContract findFuelSupplierContractById(String fuelSupplierContractId) {
		Assert.notNull(fuelSupplierContractId, "Fuel Supplier Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		return contractDAO.findFuelSupplierContractById(fuelSupplierContractId);
	}

	@Override
	public boolean uploadContractDoc(MultipartFile file, String contractId) {
		CompanyContract contract = findCompanyContractById(contractId);
		String path = SystemType.FTP_PATH_CONTRACT + contractId;
		String url = null;
		if (file != null) {
			String ext = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
			String filename = contractId + "_DOC." + ext;
			try {
				url = ftpService.uploadFile(path, file, filename);
			} catch (IOException e) {
				return false;
			}
		}
		contract.setContractDocUrl(url);
		return true;
	}

	@Override
	public void deleteRouteRateContractById(Integer rrcId) {
		contractDAO.deleteRouteRateContractById(rrcId);
	}

	@Override
	public RouteRateContract getRouteRateContractById(Integer rrcId) {
		return contractDAO.findRouteRateContractById(rrcId);
	}

	@Override
	public void deleteRouteRateById(Integer routeRateId) {
		RouteRate routeRate = contractDAO.findRouteRateById(routeRateId);
		contractDAO.removeRouteRate(Arrays.asList(routeRate));
	}

	@Override
	public CompanyContract getCustomerContractType(Company customer, Date rwbDate) {
		List<CompanyContract> contracts = contractDAO.getCustomerContract(customer.getCompanyId(), rwbDate);
		for (CompanyContract companyContract : contracts) {
			if (companyContract.getStatus().equals(SystemType.STATUS_ACTIVE))
				return companyContract;
		}
		return null;
	}

	@Override
	public CompanyContract getCustomerContractByDate(String customerId, Date date) {
		return contractDAO.getCustomerContractByDate(customerId, date);
	}

	@Override
	public List<CompanyContract> findCompanyContractByType(DContractType contractType) {
		Assert.notNull(contractType, "Contract type: " + SystemType.MUST_NOT_BE_NULL);
		return contractDAO.findCompanyContractByContractType(contractType);
	}

	@Override
	public EmployeeContract findActiveEmployeeContract(Employee employee, Date date) {
		List<EmployeeContract> contracts = employee.getEmployeeContracts();
		for (EmployeeContract employeeContract : contracts) {
			if (date == null)
				date = systemUtil.getCurrentDate();
			if (systemUtil.isDateInBetween(date, employeeContract.getStartDate(), employeeContract.getEndDate()))
				return employeeContract;
		}
		return null;
	}

}
