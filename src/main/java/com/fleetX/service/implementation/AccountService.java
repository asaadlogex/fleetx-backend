package com.fleetX.service.implementation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.IAccountDAO;
import com.fleetX.dto.InvoiceStatsDTO;
import com.fleetX.dto.TrialBalanceDTO;
import com.fleetX.dto.TrialBalanceEntryDTO;
import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.FixedLineItem;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.LineItem;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.dropdown.DExpenseType;
import com.fleetX.entity.dropdown.DIncomeType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.repository.AdjustmentRepository;
import com.fleetX.repository.DExpenseTypeRepository;
import com.fleetX.repository.DIncomeTypeRepository;
import com.fleetX.repository.DInvoiceTypeRepository;
import com.fleetX.repository.DPaymentStatusRepository;
import com.fleetX.service.IAccountService;
import com.fleetX.service.IAdminService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IOrderService;
import com.fleetX.validator.IAccountValidator;

import io.jsonwebtoken.lang.Assert;

@Service
@Transactional
public class AccountService implements IAccountService {

	@Autowired
	IOrderService orderService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IContractService contractService;
	@Autowired
	IAdminService adminService;
	@Autowired
	DExpenseTypeRepository expenseTypeRepository;
	@Autowired
	DIncomeTypeRepository incomeTypeRepository;
	@Autowired
	AdjustmentRepository adjustmentReposiory;
	@Autowired
	DPaymentStatusRepository paymentStatusRepository;
	@Autowired
	DInvoiceTypeRepository invoiceTypeRepository;
	@Autowired
	IAccountDAO accountDAO;
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IAccountValidator accountValidator;

	@Override
	public TrialBalanceDTO getTrialBalance(Date start, Date end, Boolean isAdjusted) {
		List<Job> jobs = orderService.filterJob(null, null, null, start, end, null, null, null, null);
		TrialBalanceDTO trialBalanceDto = new TrialBalanceDTO();
		trialBalanceDto.setPeriodStart(systemUtil.convertToDateString(start));
		trialBalanceDto.setPeriodEnd(systemUtil.convertToDateString(end));
		trialBalanceDto.setIsAdjusted(isAdjusted);

		List<TrialBalanceEntryDTO> trialBalanceEntries = new ArrayList<>();
		Map<String, Double> expenseMap = new HashMap<>();
		for (DExpenseType expense : expenseTypeRepository.findAll()) {
			expenseMap.put(expense.getValue(), 0d);
		}
		Map<String, Double> incomeMap = new HashMap<>();
		for (DIncomeType income : incomeTypeRepository.findAll()) {
			incomeMap.put(income.getValue(), 0d);
		}

		for (Job job : jobs) {
			for (RoadwayBill rwb : job.getRoadwayBills()) {
				for (Expense expense : rwb.getExpenses()) {
//					if (expenseMap.containsKey(expense.getExpenseType().getValue()))
					expenseMap.merge(expense.getExpenseType().getValue(), expense.getAmount(), new BiFunction<>() {
						@Override
						public Double apply(Double t, Double u) {
							return t + u;
						}
					});
				}
				for (Income income : rwb.getIncomes()) {
//					if (incomeMap.containsKey(income.getIncomeType().getValue()))
					incomeMap.merge(income.getIncomeType().getValue(), income.getAmount(), new BiFunction<>() {
						@Override
						public Double apply(Double t, Double u) {
							return t + u;
						}
					});
				}
			}
		}

		expenseMap.forEach((String key, Double value) -> {
			TrialBalanceEntryDTO trialBalanceEntry = new TrialBalanceEntryDTO();
			trialBalanceEntry.setAccountHead(key);
			trialBalanceEntry.setAccountType("Debit");
			trialBalanceEntry.setAmount(value);
			if (trialBalanceDto.getIsAdjusted()) {
				trialBalanceEntry.setAmount(value + adjustmentOf(key, "Debit", start, end));
			}
			trialBalanceEntries.add(trialBalanceEntry);
		});
		incomeMap.forEach((String key, Double value) -> {
			TrialBalanceEntryDTO trialBalanceEntry = new TrialBalanceEntryDTO();
			trialBalanceEntry.setAccountHead(key);
			trialBalanceEntry.setAccountType("Credit");
			trialBalanceEntry.setAmount(value);
			if (trialBalanceDto.getIsAdjusted()) {
				trialBalanceEntry.setAmount(value + adjustmentOf(key, "Credit", start, end));
			}

			trialBalanceEntries.add(trialBalanceEntry);
		});
		trialBalanceDto.setEntries(trialBalanceEntries);
		return trialBalanceDto;
	}

	private Double adjustmentOf(String key, String accountType, Date start, Date end) {
		Double amount = 0d;
		List<Adjustment> adjustments = accountDAO.filterAdjustment(key, start, end);
		if (adjustments != null && adjustments.size() > 0) {
			for (Adjustment adjustment : adjustments) {
				if (adjustment.getAccountType().equals(accountType))
					amount += adjustment.getAmount();
				else
					amount -= adjustment.getAmount();
			}
		}
		return amount;
	}

	@Override
	public Adjustment addAdjustmentEntry(Adjustment adjustment) {
		Assert.notNull(adjustment, "Adjustment: " + SystemType.MUST_NOT_BE_NULL);
		accountValidator.validateAdjustmentEntry(adjustment);
		adjustment.setAdjustmentId(null);
		return accountDAO.addAdjustmentEntry(adjustment);
	}

	@Override
	public List<Adjustment> getAdjustments() {
		return accountDAO.getAllAdjustments();
	}

	@Override
	public void deleteAdjustmentById(Integer id) {
		Assert.notNull(id, "Adjustment ID: " + SystemType.MUST_NOT_BE_NULL);
		accountDAO.deleteAdjustmentById(id);
	}

	@Override
	public List<Adjustment> filterAdjustment(String accountHead, Date start, Date end) {
		if (!systemUtil.isStartBeforeEnd(start, end))
			throw new IllegalArgumentException("Incorrect date order!");
		return accountDAO.filterAdjustment(accountHead, start, end);
	}

	@Override
	public Invoice generatePerTonInvoice(List<String> rwbIds) throws ParseException {
		Assert.notNull(rwbIds, "Roadway Bills: " + SystemType.MUST_NOT_BE_NULL);
		if (rwbIds.size() == 0)
			throw new IllegalArgumentException("Atleast one roadway bill is required!");
		Company customer = null;
		List<LineItem> lineItems = new ArrayList<>();
		RoadwayBill rwb = null;
		LineItem lineItem = null;
		Double totalAmount = 0d;
		for (String rwbId : rwbIds) {
			lineItem = new LineItem();
			rwb = orderService.getRoadwayBillById(rwbId);
			if (!rwb.getInvoiceStatus().getValue().equals(SystemType.INVOICE_STATUS_PENDING))
				throw new EntityExistsException("Invoice of RWB # " + rwb.getRwbId() + " already exists!");
			orderService.updateInvoiceStatus(rwb, SystemType.INVOICE_STATUS_GENERATED);
			lineItem.setRoadwayBill(rwb);
			if (rwb.getIncomes() != null)
				for (Income income : rwb.getIncomes())
					totalAmount += income.getAmount();
			lineItems.add(lineItem);

		}
		if (rwb.getCustomer() != null)
			customer = rwb.getCustomer();
		else
			customer = rwb.getBroker();
		String taxState = null;
		if (rwb.getTaxState() != null)
			taxState = rwb.getTaxState().getValue();
		Invoice invoice = prepareInvoice(customer);
		invoice.setTaxState(rwb.getTaxState());
		Double taxPercent = getTaxPercent(taxState);
		invoice.setTax(taxPercent);
		invoice.setTotalAmount(systemUtil.roundOff(totalAmount));
		invoice.setInvoiceType(invoiceTypeRepository.findByValue(SystemType.INVOICE_TYPE_PER_TON));
		invoice.setDhInvoice(generateDHInvoice(rwbIds));
		invoice = accountDAO.addInvoice(invoice);
		for (LineItem item : lineItems) {
			item.setInvoice(invoice);
			accountDAO.addLineItem(item);
		}
		return invoice;
	}

	private DHInvoice generateDHInvoice(List<String> rwbIds) {
		Assert.notNull(rwbIds, "Roadway Bills: " + SystemType.MUST_NOT_BE_NULL);
		if (rwbIds.size() == 0)
			throw new IllegalArgumentException("Atleast one roadway bill is required!");
		Company customer = null;
		List<LineItem> lineItems = new ArrayList<>();
		RoadwayBill rwb = null;
		LineItem lineItem = null;
		Double totalAmount = 0d;
		for (String rwbId : rwbIds) {
			rwb = orderService.getRoadwayBillById(rwbId);
			if (rwb.getIncomes() != null) {
				lineItem = null;
				for (Income income : rwb.getIncomes()) {
					if (income.getIncomeType().getValue().equals(SystemType.INCOME_DETENTION_CHARGES)
							|| income.getIncomeType().getValue().equals(SystemType.INCOME_HALTING_CHARGES)) {
						if (lineItem == null) {
							lineItem = new LineItem();
							lineItem.setRoadwayBill(rwb);
							lineItems.add(lineItem);
						}
						totalAmount += income.getAmount();
					}
				}
			}
		}
		if (lineItems.size() == 0)
			return null;
		if (rwb.getCustomer() != null)
			customer = rwb.getCustomer();
		else
			customer = rwb.getBroker();
		String taxState = null;
		if (rwb.getTaxState() != null)
			taxState = rwb.getTaxState().getValue();
		DHInvoice invoice = prepareDHInvoice(customer);
		invoice.setTaxState(rwb.getTaxState());
		Double taxPercent = getTaxPercent(taxState);
		invoice.setTax(taxPercent);
		invoice.setTotalAmount(systemUtil.roundOff(totalAmount));
		invoice.setInvoiceType(invoiceTypeRepository.findByValue(SystemType.INVOICE_TYPE_PER_TON));
		invoice = accountDAO.addDHInvoice(invoice);
		for (LineItem item : lineItems) {
			item.setDhInvoice(invoice);
			accountDAO.addLineItem(item);
		}
		return invoice;
	}

	@Override
	public Invoice generatePerTripInvoice(List<String> rwbIds) throws ParseException {
		Assert.notNull(rwbIds, "Roadway Bills: " + SystemType.MUST_NOT_BE_NULL);
		if (rwbIds.size() == 0)
			throw new IllegalArgumentException("Atleast one roadway bill is required!");
//		accountValidator.validateInvoiceDetails(invoiceDetails);
		Company customer = null;
		List<LineItem> lineItems = new ArrayList<>();
		RoadwayBill rwb = null;
		LineItem lineItem = null;
		Double totalAmount = 0d;
		for (String rwbId : rwbIds) {
			lineItem = new LineItem();
			rwb = orderService.getRoadwayBillById(rwbId);
			if (!rwb.getInvoiceStatus().getValue().equals(SystemType.INVOICE_STATUS_PENDING))
				throw new EntityExistsException("Invoice of RWB # " + rwb.getRwbId() + " already exists!");
			orderService.updateInvoiceStatus(rwb, SystemType.INVOICE_STATUS_GENERATED);
			lineItem.setRoadwayBill(rwb);
			if (rwb.getIncomes() != null)
				for (Income income : rwb.getIncomes())
					totalAmount += income.getAmount();
			lineItems.add(lineItem);

		}
		if (rwb.getCustomer() != null)
			customer = rwb.getCustomer();
		else
			customer = rwb.getBroker();
		String taxState = null;
		if (rwb.getTaxState() != null)
			taxState = rwb.getTaxState().getValue();
		Invoice invoice = prepareInvoice(customer);
		invoice.setTaxState(rwb.getTaxState());
		Double taxPercent = getTaxPercent(taxState);
		invoice.setTax(taxPercent);
		invoice.setTotalAmount(systemUtil.roundOff(totalAmount));
		invoice.setInvoiceType(invoiceTypeRepository.findByValue(SystemType.INVOICE_TYPE_PER_TRIP));
		invoice = accountDAO.addInvoice(invoice);
		for (LineItem item : lineItems) {
			item.setInvoice(invoice);
			accountDAO.addLineItem(item);
		}
		return invoice;
	}

	@Override
	public Invoice generateDedicatedInvoice(List<String> rwbIds) throws ParseException {
		Assert.notNull(rwbIds, "Roadway Bills: " + SystemType.MUST_NOT_BE_NULL);
		if (rwbIds.size() == 0)
			throw new IllegalArgumentException("Atleast one roadway bill is required!");
//		accountValidator.validateInvoiceDetails(invoiceDetails);
		Company customer = null;
		List<LineItem> lineItems = new ArrayList<>();
		RoadwayBill rwb = null;
		LineItem lineItem = null;
		Double totalAmount = 0d;
		for (String rwbId : rwbIds) {
			lineItem = new LineItem();
			rwb = orderService.getRoadwayBillById(rwbId);
			if (!rwb.getInvoiceStatus().getValue().equals(SystemType.INVOICE_STATUS_PENDING))
				throw new EntityExistsException("Invoice of RWB # " + rwb.getRwbId() + " already exists!");
			orderService.updateInvoiceStatus(rwb, SystemType.INVOICE_STATUS_GENERATED);
			lineItem.setRoadwayBill(rwb);
			if (rwb.getIncomes() != null)
				for (Income income : rwb.getIncomes())
					totalAmount += income.getAmount();
			lineItems.add(lineItem);
		}
		if (rwb.getCustomer() != null)
			customer = rwb.getCustomer();
		else
			customer = rwb.getBroker();
		String taxState = null;
		if (rwb.getTaxState() != null)
			taxState = rwb.getTaxState().getValue();

		Invoice invoice = prepareInvoice(customer);
		invoice.setTaxState(rwb.getTaxState());
		Double taxPercent = getTaxPercent(taxState);
		invoice.setTax(taxPercent);
		invoice.setTotalAmount(systemUtil.roundOff(totalAmount));
		invoice.setInvoiceType(invoiceTypeRepository.findByValue(SystemType.INVOICE_TYPE_DEDICATED));
		invoice = accountDAO.addInvoice(invoice);
		for (LineItem item : lineItems) {
			item.setInvoice(invoice);
			accountDAO.addLineItem(item);
		}
		return invoice;
	}

	@Override
	public Invoice generateFixedInvoice(String customerId, Date date) {
		Company customer = null;
		Double totalAmount = 0d;
		customer = companyService.findCompanyById(customerId);
		CompanyContract companyContract = contractService.getCustomerContractByDate(customerId, date);
		if (companyContract == null
				|| !companyContract.getContractType().getValue().equals(SystemType.CONTRACT_TYPE_DEDICATED))
			throw new NoSuchElementException("No dedicated contract found for given customer on given date!");
		boolean alreadyCreated = isInvoiceCreated(customer, SystemType.INVOICE_TYPE_FIXED, systemUtil.getMonth(date),
				systemUtil.getYear(date));
		if (alreadyCreated)
			throw new EntityExistsException("Fixed invoice for the given date is already created!");
		Invoice invoice = prepareInvoice(customer);
		DState taxState = customer.getCompanyAddresses().get(0).getState();
		invoice.setTaxState(taxState);
		Double tax = getTaxPercent(taxState.getValue());
		totalAmount = companyContract.getFixedRate();
		invoice.setTax(tax);
		invoice.setTotalAmount((totalAmount * companyContract.getAssetCombinations().size()));
		invoice.setInvoiceMonth(systemUtil.getMonth(date));
		invoice.setInvoiceYear(systemUtil.getYear(date));
		invoice.setInvoiceType(invoiceTypeRepository.findByValue(SystemType.INVOICE_TYPE_FIXED));
		invoice = accountDAO.addInvoice(invoice);
		List<FixedLineItem> lineItems = new ArrayList<>();
		FixedLineItem lineItem;
		for (AssetCombination ac : companyContract.getAssetCombinations()) {
			lineItem = new FixedLineItem();
			lineItem.setAssetCombination(ac);
			lineItem.setInvoice(invoice);
			lineItem = accountDAO.addFixedLineItem(lineItem);
			lineItems.add(lineItem);
		}
		return invoice;
	}

	private boolean isInvoiceCreated(Company customer, String invoiceType, int month, int year) {
		DInvoiceType invoice = invoiceTypeRepository.findByValue(invoiceType);
		List<Invoice> invoices = accountDAO.filterInvoice(customer, invoice, null, null, null, month, year);
		return invoices.size() > 0;
	}

	private Double getTaxPercent(String taxState) {
		switch (taxState) {
		case SystemType.STATE_SINDH:
			return SystemType.TAX_PERC_SINDH;
		case SystemType.STATE_BALUCHISTAN:
			return SystemType.TAX_PERC_BALUCHISTAN;
		case SystemType.STATE_PUNJAB:
			return SystemType.TAX_PERC_PUNJAB;
		case SystemType.STATE_KPK:
			return SystemType.TAX_PERC_KPK;
		default:
			return 0d;
		}
	}

	private Invoice prepareInvoice(Company customer) {
		Invoice invoice = new Invoice();
		invoice.setSender(adminService.findAdminCompanyById(SystemType.ADMIN_COMPANY_ID));
		invoice.setRecipient(customer);
		invoice.setNote(SystemType.INVOICE_DEFAULT_NOTE);
		invoice.setInvoiceDate(systemUtil.getCurrentDate());
		invoice.setPaymentStatus(paymentStatusRepository.findByValue(SystemType.PAYMENT_STATUS_PENDING));
		invoice.setAmountPaid(0d);
		return invoice;
	}

	private DHInvoice prepareDHInvoice(Company customer) {
		DHInvoice invoice = new DHInvoice();
		invoice.setSender(adminService.findAdminCompanyById(SystemType.ADMIN_COMPANY_ID));
		invoice.setRecipient(customer);
		invoice.setNote(SystemType.INVOICE_DEFAULT_NOTE);
		invoice.setInvoiceDate(systemUtil.getCurrentDate());
		invoice.setPaymentStatus(paymentStatusRepository.findByValue(SystemType.PAYMENT_STATUS_PENDING));
		invoice.setAmountPaid(0d);
		return invoice;
	}

	@Override
	public List<InvoiceStatsDTO> getInvoiceStats(String invoiceStatus) {
		List<Company> customers = companyService.findCompanyByType("Customer");
		List<Company> brokers = companyService.findCompanyByType("Broker");
		String[] invoiceTypes = new String[] { SystemType.INVOICE_TYPE_PER_TRIP, SystemType.INVOICE_TYPE_PER_TON,
				SystemType.INVOICE_TYPE_DEDICATED };
		List<InvoiceStatsDTO> stats = new ArrayList<>();
		for (Company company : customers) {
			InvoiceStatsDTO statsDto = new InvoiceStatsDTO();
			boolean flag = false;
			statsDto.setCompanyName(company.getCompanyName());
			for (String invoiceType : invoiceTypes) {
				List<RoadwayBill> rwbs = orderService.filterRoadwayBill(null, null, company.getCompanyId(), null,
						invoiceStatus, invoiceType);
				if (rwbs.size() > 0)
					flag = true;
				if (invoiceType.equals(SystemType.INVOICE_TYPE_PER_TRIP))
					statsDto.setPerTrip(rwbs.size());
				else if (invoiceType.equals(SystemType.INVOICE_TYPE_PER_TON))
					statsDto.setPerTon(rwbs.size());
				else if (invoiceType.equals(SystemType.INVOICE_TYPE_DEDICATED))
					statsDto.setDedicated(rwbs.size());
			}
			if (flag)
				stats.add(statsDto);
		}
		for (Company company : brokers) {
			InvoiceStatsDTO statsDto = new InvoiceStatsDTO();
			boolean flag = false;
			statsDto.setCompanyName(company.getCompanyName());
			for (String invoiceType : invoiceTypes) {
				List<RoadwayBill> rwbs = orderService.filterRoadwayBill(null, null, company.getCompanyId(), null,
						invoiceStatus, invoiceType);
				if (rwbs.size() > 0)
					flag = true;
				if (invoiceType.equals(SystemType.INVOICE_TYPE_PER_TRIP))
					statsDto.setPerTrip(rwbs.size());
				else if (invoiceType.equals(SystemType.INVOICE_TYPE_PER_TON))
					statsDto.setPerTon(rwbs.size());
				else if (invoiceType.equals(SystemType.INVOICE_TYPE_DEDICATED))
					statsDto.setDedicated(rwbs.size());
			}
			if (flag)
				stats.add(statsDto);
		}
		return stats;
	}

	@Override
	public ReimbursementInfo addReimbursement(ReimbursementInfo reimbursementInfo) {
		Assert.notNull(reimbursementInfo, "Reimbursement Info: " + SystemType.MUST_NOT_BE_NULL);
		accountValidator.validateReimbursementInfo(reimbursementInfo);
		List<String> jobIds = new ArrayList<>();
		for (Job job : reimbursementInfo.getJobs()) {
			if (job.getReimbursementStatus() != null
					&& !job.getReimbursementStatus().getValue().equals(SystemType.REIMBURSEMENT_STATUS_SENT))
				throw new IllegalArgumentException(
						"Some jobs are either not sent for reimbursement or already reimbursed!");
			else
				jobIds.add(job.getJobId());
		}
		orderService.updateReimbursementStatus(jobIds, SystemType.REIMBURSEMENT_STATUS_REIMBURSED);
		return accountDAO.addReimbursementInfo(reimbursementInfo);
	}

	@Override
	public ReimbursementInfo getReimbursementById(Integer id) {
		return accountDAO.getReimbursementInfoById(id);
	}

	@Override
	public List<ReimbursementInfo> getAllReimbursement() {
		return accountDAO.getAllReimbursementInfo();
	}

	@Override
	public List<Invoice> filterInvoice(String companyId, String invoiceType, Boolean isFixed, String paymentStatus,
			Date startDate, Date endDate, Integer month, Integer year) {
		DInvoiceType type = null;
		DPaymentStatus status = null;
		Company company = null;
		if (invoiceType != null)
			type = invoiceTypeRepository.findByValue(invoiceType);
		if (paymentStatus != null)
			status = paymentStatusRepository.findByValue(paymentStatus);
		if (companyId != null)
			company = companyService.findCompanyById(companyId);
		List<Invoice> invoices = accountDAO.filterInvoice(company, type, status, startDate, endDate, month, year);
		if (!isFixed)
			invoices.removeIf(x -> x.getInvoiceType().getValue().equals(SystemType.INVOICE_TYPE_FIXED));
		else
			invoices.removeIf(x -> !x.getInvoiceType().getValue().equals(SystemType.INVOICE_TYPE_FIXED));
		return invoices;
	}

	@Override
	public List<Invoice> getAllInvoice() {
		return accountDAO.getAlIInvoice();
	}

	@Override
	public Invoice getInvoiceById(Integer invoiceId) {
		return accountDAO.getInvoiceById(invoiceId);
	}

	@Override
	public void deleteInvoiceById(Integer id) {
		Assert.notNull(id, "Invoice ID: " + SystemType.MUST_NOT_BE_NULL);
		Invoice invoice = getInvoiceById(id);
		if (!invoice.getPaymentStatus().getValue().equals(SystemType.PAYMENT_STATUS_PENDING))
			throw new EntityExistsException("Cannot delete invoice!");
		if (!invoice.getInvoiceType().getValue().equals(SystemType.INVOICE_TYPE_FIXED)) {
			List<LineItem> lineItems = invoice.getLineItems();
			for (LineItem lineItem : lineItems) {
				orderService.updateInvoiceStatus(lineItem.getRoadwayBill(), SystemType.INVOICE_STATUS_PENDING);
			}
		}
		accountDAO.deleteInvoiceById(id);

	}

	@Override
	public InvoicePayment receiveInvoice(InvoicePayment invoicePayment) {
		accountValidator.validateInvoicePayment(invoicePayment);
		Invoice invoice = invoicePayment.getInvoice();
		if (invoice.getPaymentStatus().getValue().equals(SystemType.PAYMENT_STATUS_PAID))
			throw new EntityExistsException("Invoice payment already received!");
		Double invoiceTotal = systemUtil
				.roundOff(invoice.getTotalAmount() + ((invoice.getTax() / 100) * invoice.getTotalAmount()));
		if ((invoice.getAmountPaid() + invoicePayment.getPaymentAmount()) > invoiceTotal)
			throw new IllegalArgumentException("Payment cannot be higher than invoice total amount!");
		else if (invoice.getAmountPaid() + invoicePayment.getPaymentAmount() == invoiceTotal) {
			invoice.setAmountPaid(systemUtil.roundOff(invoice.getAmountPaid() + invoicePayment.getPaymentAmount()));
			invoice.setPaymentStatus(paymentStatusRepository.findByValue(SystemType.PAYMENT_STATUS_PAID));
			accountDAO.updateInvoice(invoice);
		} else {
			invoice.setAmountPaid(systemUtil.roundOff(invoice.getAmountPaid() + invoicePayment.getPaymentAmount()));
			invoice.setPaymentStatus(paymentStatusRepository.findByValue(SystemType.PAYMENT_STATUS_PARTIAL));
			accountDAO.updateInvoice(invoice);
		}
		return accountDAO.addInvoicePayment(invoicePayment);
	}

	@Override
	public List<InvoicePayment> getInvoicePayments(Integer id) {
		Assert.notNull(id, "Invoice ID: " + SystemType.MUST_NOT_BE_NULL);
		return getInvoiceById(id).getInvoicePayments();
	}

	public List<DHInvoice> getAllDHInvoice() {
		return accountDAO.getAlIDHInvoice();
	}

	@Override
	public DHInvoice getDHInvoiceById(Integer invoiceId) {
		return accountDAO.getDHInvoiceById(invoiceId);
	}

	@Override
	public void deleteDHInvoiceById(Integer id) {
		Assert.notNull(id, "Invoice ID: " + SystemType.MUST_NOT_BE_NULL);
		DHInvoice invoice = getDHInvoiceById(id);
		if (!invoice.getPaymentStatus().getValue().equals(SystemType.PAYMENT_STATUS_PENDING))
			throw new EntityExistsException("Cannot delete invoice!");
//		if (!invoice.getInvoiceType().getValue().equals(SystemType.INVOICE_TYPE_FIXED)) {
//			List<LineItem> lineItems = invoice.getLineItems();
//			for (LineItem lineItem : lineItems) {
//				orderService.updateInvoiceStatus(lineItem.getRoadwayBill(), SystemType.INVOICE_STATUS_PENDING);
//			}
//		}
		accountDAO.deleteInvoiceById(id);

	}

	@Override
	public InvoicePayment receiveDHInvoice(InvoicePayment invoicePayment) {
		accountValidator.validateInvoicePayment(invoicePayment);
		DHInvoice invoice = invoicePayment.getDhInvoice();
		if (invoice.getPaymentStatus().getValue().equals(SystemType.PAYMENT_STATUS_PAID))
			throw new EntityExistsException("Invoice payment already received!");
		Double invoiceTotal = systemUtil
				.roundOff(invoice.getTotalAmount() + ((invoice.getTax() / 100) * invoice.getTotalAmount()));
		if ((invoice.getAmountPaid() + invoicePayment.getPaymentAmount()) > invoiceTotal)
			throw new IllegalArgumentException("Payment cannot be higher than invoice total amount!");
		else if (invoice.getAmountPaid() + invoicePayment.getPaymentAmount() == invoiceTotal) {
			invoice.setAmountPaid(systemUtil.roundOff(invoice.getAmountPaid() + invoicePayment.getPaymentAmount()));
			invoice.setPaymentStatus(paymentStatusRepository.findByValue(SystemType.PAYMENT_STATUS_PAID));
			accountDAO.updateDHInvoice(invoice);
		} else {
			invoice.setAmountPaid(systemUtil.roundOff(invoice.getAmountPaid() + invoicePayment.getPaymentAmount()));
			invoice.setPaymentStatus(paymentStatusRepository.findByValue(SystemType.PAYMENT_STATUS_PARTIAL));
			accountDAO.updateDHInvoice(invoice);
		}
		return accountDAO.addInvoicePayment(invoicePayment);
	}

	@Override
	public List<InvoicePayment> getDHInvoicePayments(Integer id) {
		Assert.notNull(id, "Invoice ID: " + SystemType.MUST_NOT_BE_NULL);
		return getDHInvoiceById(id).getInvoicePayments();
	}

	@Override
	public long countInvoiceByPaymentStatus(DPaymentStatus status) {
		return accountDAO.countInvoiceByPaymentStatus(status);
	}

}
