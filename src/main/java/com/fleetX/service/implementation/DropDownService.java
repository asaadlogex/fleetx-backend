package com.fleetX.service.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.entity.dropdown.DAssetType;
import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DBrand;
import com.fleetX.entity.dropdown.DBusinessType;
import com.fleetX.entity.dropdown.DCategory;
import com.fleetX.entity.dropdown.DChannel;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DCompanyDocumentType;
import com.fleetX.entity.dropdown.DCompanyType;
import com.fleetX.entity.dropdown.DContainerType;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.dropdown.DCustomerType;
import com.fleetX.entity.dropdown.DDepartment;
import com.fleetX.entity.dropdown.DEmployeeDocument;
import com.fleetX.entity.dropdown.DEmployeeType;
import com.fleetX.entity.dropdown.DExpenseType;
import com.fleetX.entity.dropdown.DFormation;
import com.fleetX.entity.dropdown.DFuelTankType;
import com.fleetX.entity.dropdown.DIncomeType;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DItemCategory;
import com.fleetX.entity.dropdown.DItemType;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DLoadStatus;
import com.fleetX.entity.dropdown.DMake;
import com.fleetX.entity.dropdown.DPaymentMode;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.dropdown.DPriority;
import com.fleetX.entity.dropdown.DPrivilege;
import com.fleetX.entity.dropdown.DPurchaseOrderStatus;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.dropdown.DReimbursementStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.dropdown.DStopProgressType;
import com.fleetX.entity.dropdown.DStopType;
import com.fleetX.entity.dropdown.DSupplierType;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.dropdown.DUnit;
import com.fleetX.entity.dropdown.DVehicleOwnership;
import com.fleetX.entity.dropdown.DWorkOrderCategory;
import com.fleetX.entity.dropdown.DWorkOrderStatus;
import com.fleetX.entity.dropdown.DWorkOrderSubCategory;
import com.fleetX.repository.DAssetTypeRepository;
import com.fleetX.repository.DBaseStationRepository;
import com.fleetX.repository.DBrandRepository;
import com.fleetX.repository.DBusinessTypeRepository;
import com.fleetX.repository.DCategoryRepository;
import com.fleetX.repository.DChannelRepository;
import com.fleetX.repository.DCityRepository;
import com.fleetX.repository.DCompanyDocumentTypeRepository;
import com.fleetX.repository.DCompanyTypeRepository;
import com.fleetX.repository.DContainerTypeRepository;
import com.fleetX.repository.DContractTypeRepository;
import com.fleetX.repository.DCustomerTypeRepository;
import com.fleetX.repository.DDepartmentRepository;
import com.fleetX.repository.DEmployeeDocumentRepository;
import com.fleetX.repository.DEmployeeTypeRepository;
import com.fleetX.repository.DExpenseTypeRepository;
import com.fleetX.repository.DFormationRepository;
import com.fleetX.repository.DFuelTankTypeRepository;
import com.fleetX.repository.DIncomeTypeRepository;
import com.fleetX.repository.DInvoiceStatusRepository;
import com.fleetX.repository.DInvoiceTypeRepository;
import com.fleetX.repository.DItemCategoryRepository;
import com.fleetX.repository.DItemTypeRepository;
import com.fleetX.repository.DJobStatusRepository;
import com.fleetX.repository.DLeaseTypeRepository;
import com.fleetX.repository.DLoadStatusRepository;
import com.fleetX.repository.DMakeRepository;
import com.fleetX.repository.DPaymentModeRepository;
import com.fleetX.repository.DPaymentStatusRepository;
import com.fleetX.repository.DPositionRepository;
import com.fleetX.repository.DPriorityRepository;
import com.fleetX.repository.DPrivilegeRepository;
import com.fleetX.repository.DPurchaseOrderStatusRepository;
import com.fleetX.repository.DRWBStatusRepository;
import com.fleetX.repository.DReimbursementStatusRepository;
import com.fleetX.repository.DStateRepository;
import com.fleetX.repository.DStopProgressTypeRepository;
import com.fleetX.repository.DStopTypeRepository;
import com.fleetX.repository.DSupplierTypeRepository;
import com.fleetX.repository.DTrailerSizeRepository;
import com.fleetX.repository.DTrailerTypeRepository;
import com.fleetX.repository.DUnitRepository;
import com.fleetX.repository.DVehicleOwnershipRepository;
import com.fleetX.repository.DWorkOrderCategoryRepository;
import com.fleetX.repository.DWorkOrderStatusRepository;
import com.fleetX.repository.DWorkOrderSubCategoryRepository;
import com.fleetX.service.IDropDownService;
import com.fleetX.validator.IGeneralValidator;

import io.jsonwebtoken.lang.Assert;

@Service
@Transactional
public class DropDownService implements IDropDownService {

	@Autowired
	DAssetTypeRepository assetTypeRepository;
	@Autowired
	DBusinessTypeRepository businessTypeRepository;
	@Autowired
	DChannelRepository channelRepository;
	@Autowired
	DDepartmentRepository departmentRepository;
	@Autowired
	DLeaseTypeRepository leaseTypeRepository;
	@Autowired
	DMakeRepository makeRepository;
	@Autowired
	DPositionRepository positionRepository;
	@Autowired
	DPrivilegeRepository privilegeRepository;
	@Autowired
	DTrailerTypeRepository trailerTypeRepository;
	@Autowired
	DTrailerSizeRepository trailerSizeRepository;
	@Autowired
	DCompanyDocumentTypeRepository companyDocumentTypeRepository;
	@Autowired
	DEmployeeDocumentRepository employeeDocumentRepository;
	@Autowired
	DEmployeeTypeRepository employeeTypeRepository;
	@Autowired
	DFormationRepository formationTypeRepository;
	@Autowired
	DSupplierTypeRepository supplierTypeRepository;
	@Autowired
	DContainerTypeRepository containerTypeRepository;
	@Autowired
	DContractTypeRepository contractTypeRepository;
	@Autowired
	DCityRepository cityRepository;
	@Autowired
	DBaseStationRepository baseStationRepository;
	@Autowired
	DJobStatusRepository jobStatusRepository;
	@Autowired
	DVehicleOwnershipRepository vehicleOwnershipRepository;
	@Autowired
	DRWBStatusRepository rwbStatusRepository;
	@Autowired
	DCategoryRepository categoryRepository;
	@Autowired
	DFuelTankTypeRepository fuelTankTypeRepository;
	@Autowired
	DStopTypeRepository stopTypeRepository;
	@Autowired
	DPaymentModeRepository paymentModeRepository;
	@Autowired
	DLoadStatusRepository loadStatusRepository;
	@Autowired
	DCustomerTypeRepository customerTypeRepository;
	@Autowired
	DIncomeTypeRepository incomeTypeRepository;
	@Autowired
	DExpenseTypeRepository expenseTypeRepository;
	@Autowired
	DCompanyTypeRepository companyTypeRepository;
	@Autowired
	DStateRepository stateRepository;
	@Autowired
	DStopProgressTypeRepository stopProgressTypeRepository;
	@Autowired
	DReimbursementStatusRepository reimbursementStatusRepository;
	@Autowired
	DInvoiceStatusRepository invoiceStatusRepository;
	@Autowired
	DPaymentStatusRepository paymentStatusRepository;
	@Autowired
	DBrandRepository brandRepository;
	@Autowired
	DItemTypeRepository itemTypeRepository;
	@Autowired
	DItemCategoryRepository itemCategoryRepository;
	@Autowired
	DPurchaseOrderStatusRepository purchaseOrderStatusRepository;
	@Autowired
	DUnitRepository unitRepository;
	@Autowired
	DWorkOrderStatusRepository workOrderStatusRepository;
	@Autowired
	DWorkOrderCategoryRepository workOrderCategoryRepository;
	@Autowired
	DWorkOrderSubCategoryRepository workOrderSubCategoryRespository;
	@Autowired
	DPriorityRepository priorityRepository;
	@Autowired
	DInvoiceTypeRepository invoiceTypeRepository;
	@Autowired
	IGeneralValidator generalValidator;

	@Override
	public void createDAssetType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DAssetType> assetTypes = new ArrayList<>();
		for (String value : values) {
			if (!assetTypeRepository.existsByValue(value)) {
				DAssetType assetType = new DAssetType();
				assetType.setCode(SystemType.ASSET_TYPE);
				assetType.setDescription("Asset Type");
				assetType.setStatus(SystemType.STATUS_ACTIVE);
				assetType.setValue(value);
				assetTypes.add(assetType);
			}
		}
		assetTypeRepository.saveAll(assetTypes);
	}

	@Override
	public List<DAssetType> retrieveDAssetType() {
		return assetTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDAssetType(DAssetType assetType) {
	}

	@Override
	public void createDBusinessType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DBusinessType> businessTypes = new ArrayList<>();
		for (String value : values) {
			if (!businessTypeRepository.existsByValue(value)) {
				DBusinessType businessType = new DBusinessType();
				businessType.setCode(SystemType.BUSINESS_TYPE);
				businessType.setDescription("Business Type");
				businessType.setStatus(SystemType.STATUS_ACTIVE);
				businessType.setValue(value);
				businessTypes.add(businessType);
			}
		}
		businessTypeRepository.saveAll(businessTypes);
	}

	@Override
	public List<DBusinessType> retrieveDBusinessType() {
		return businessTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDBusinessType(DBusinessType businessType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDEmployeeType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DEmployeeType> employeeTypes = new ArrayList<>();
		for (String value : values) {
			if (!employeeTypeRepository.existsByValue(value)) {
				DEmployeeType employeeType = new DEmployeeType();
				employeeType.setCode(SystemType.EMPLOYEE_TYPE);
				employeeType.setDescription("Employee Type");
				employeeType.setStatus(SystemType.STATUS_ACTIVE);
				employeeType.setValue(value);
				employeeTypes.add(employeeType);
			}
		}
		employeeTypeRepository.saveAll(employeeTypes);
	}

	@Override
	public List<DEmployeeType> retrieveDEmployeeType() {
		return employeeTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDEmployeeType(DEmployeeType employeeType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDContainerType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DContainerType> containerTypes = new ArrayList<>();
		for (String value : values) {
			if (!containerTypeRepository.existsByValue(value)) {
				DContainerType containerType = new DContainerType();
				containerType.setCode(SystemType.CONTAINER_TYPE);
				containerType.setDescription("Container Type");
				containerType.setStatus(SystemType.STATUS_ACTIVE);
				containerType.setValue(value);
				containerTypes.add(containerType);
			}
		}
		containerTypeRepository.saveAll(containerTypes);
	}

	@Override
	public List<DContainerType> retrieveDContainerType() {
		return containerTypeRepository.findAllByStatus("ACTIVE");
	}

	@Override
	public void updateDContainerType(DContainerType containerType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDChannel(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DChannel> channels = new ArrayList<>();
		for (String value : values) {
			if (!channelRepository.existsByValue(value)) {
				DChannel channel = new DChannel();
				channel.setCode(SystemType.CHANNEL_TYPE);
				channel.setDescription("Channel Type");
				channel.setStatus(SystemType.STATUS_ACTIVE);
				channel.setValue(value);
				channels.add(channel);
			}
		}
		channelRepository.saveAll(channels);
	}

	@Override
	public List<DChannel> retrieveDChannel() {
		return channelRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDChannel(DChannel channel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDCompanyDocumentType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DCompanyDocumentType> companyDocumentTypes = new ArrayList<>();
		for (String value : values) {
			if (!companyDocumentTypeRepository.existsByValue(value)) {
				DCompanyDocumentType companyDocumentType = new DCompanyDocumentType();
				companyDocumentType.setCode(SystemType.COMPANY_DOCUMENT_TYPE);
				companyDocumentType.setDescription("Company Document Type");
				companyDocumentType.setStatus(SystemType.STATUS_ACTIVE);
				companyDocumentType.setValue(value);
				companyDocumentTypes.add(companyDocumentType);
			}
		}
		companyDocumentTypeRepository.saveAll(companyDocumentTypes);
	}

	@Override
	public List<DCompanyDocumentType> retrieveDCompanyDocumentType() {
		return companyDocumentTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDCompanyDocumentType(DCompanyDocumentType documentType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDDepartment(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DDepartment> departments = new ArrayList<>();
		for (String value : values) {
			if (!departmentRepository.existsByValue(value)) {
				DDepartment department = new DDepartment();
				department.setCode(SystemType.DEPARTMENT_TYPE);
				department.setDescription("Department Type");
				department.setStatus(SystemType.STATUS_ACTIVE);
				department.setValue(value);
				departments.add(department);
			}
		}
		departmentRepository.saveAll(departments);
	}

	@Override
	public List<DDepartment> retrieveDDepartment() {
		return departmentRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDDepartment(DDepartment department) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDEmployeeDocument(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DEmployeeDocument> employeeDocuments = new ArrayList<>();
		for (String value : values) {
			if (!employeeDocumentRepository.existsByValue(value)) {
				DEmployeeDocument employeeDocument = new DEmployeeDocument();
				employeeDocument.setCode(SystemType.EMPLOYEE_DOCUMENT_TYPE);
				employeeDocument.setDescription("Employee Document Type");
				employeeDocument.setStatus(SystemType.STATUS_ACTIVE);
				employeeDocument.setValue(value);
				employeeDocuments.add(employeeDocument);
			}
		}
		employeeDocumentRepository.saveAll(employeeDocuments);
	}

	@Override
	public List<DEmployeeDocument> retrieveDEmployeeDocument() {
		return employeeDocumentRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDEmployeeDocument(DEmployeeDocument employeeDocument) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDLeaseType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DLeaseType> leaseTypes = new ArrayList<>();
		for (String value : values) {
			if (!leaseTypeRepository.existsByValue(value)) {
				DLeaseType leaseType = new DLeaseType();
				leaseType.setCode(SystemType.LEASE_TYPE);
				leaseType.setDescription("Lease Type");
				leaseType.setStatus(SystemType.STATUS_ACTIVE);
				leaseType.setValue(value);
				leaseTypes.add(leaseType);
			}
		}
		leaseTypeRepository.saveAll(leaseTypes);
	}

	@Override
	public List<DLeaseType> retrieveDLeaseType() {
		return leaseTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDLeaseType(DLeaseType leaseType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDMake(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DMake> makes = new ArrayList<>();
		for (String value : values) {
			if (!makeRepository.existsByValue(value)) {
				DMake make = new DMake();
				make.setCode(SystemType.MAKE_TYPE);
				make.setDescription("Make Type");
				make.setStatus(SystemType.STATUS_ACTIVE);
				make.setValue(value);
				makes.add(make);
			}
		}
		makeRepository.saveAll(makes);
	}

	@Override
	public List<DMake> retrieveDMake() {
		return makeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDMake(DMake make) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDPosition(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DPosition> positions = new ArrayList<>();
		for (String value : values) {
			if (!positionRepository.existsByValue(value)) {
				DPosition position = new DPosition();
				position.setCode(SystemType.POSITION_TYPE);
				position.setDescription("Position Type");
				position.setStatus(SystemType.STATUS_ACTIVE);
				position.setValue(value);
				positions.add(position);
			}
		}
		positionRepository.saveAll(positions);
	}

	@Override
	public List<DPosition> retrieveDPosition() {
		return positionRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDPosition(DPosition position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDPrivilege(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DPrivilege> privileges = new ArrayList<>();
		for (String value : values) {
			if (!privilegeRepository.existsByValue(value)) {
				DPrivilege privilege = new DPrivilege();
				privilege.setCode(SystemType.USER_PRIVILEGE_TYPE);
				privilege.setDescription("Privilege Type");
				privilege.setStatus(SystemType.STATUS_ACTIVE);
				privilege.setValue(value);
				privileges.add(privilege);
			}
		}
		privilegeRepository.saveAll(privileges);
	}

	@Override
	public List<DPrivilege> retrieveDPrivilege() {
		return privilegeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDPrivilege(DPrivilege privilege) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDTrailerSize(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DTrailerSize> trailerSizes = new ArrayList<>();
		for (String value : values) {
			if (!trailerSizeRepository.existsByValue(value)) {
				DTrailerSize trailerSize = new DTrailerSize();
				trailerSize.setCode(SystemType.TRAILER_SIZE_TYPE);
				trailerSize.setDescription("Trailer Size Type");
				trailerSize.setStatus(SystemType.STATUS_ACTIVE);
				trailerSize.setValue(value);
				trailerSizes.add(trailerSize);
			}
		}
		trailerSizeRepository.saveAll(trailerSizes);
	}

	@Override
	public List<DTrailerSize> retrieveDTrailerSize() {
		return trailerSizeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDTrailerSize(DTrailerSize trailerSize) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDTrailerType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DTrailerType> trailerTypes = new ArrayList<>();
		for (String value : values) {
			if (!trailerTypeRepository.existsByValue(value)) {
				DTrailerType trailerType = new DTrailerType();
				trailerType.setCode(SystemType.TRAILER_TYPE);
				trailerType.setDescription("Trailer Type");
				trailerType.setStatus(SystemType.STATUS_ACTIVE);
				trailerType.setValue(value);
				trailerTypes.add(trailerType);
			}
		}
		trailerTypeRepository.saveAll(trailerTypes);
	}

	@Override
	public List<DTrailerType> retrieveDTrailerType() {
		return trailerTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDTrailerType(DTrailerType trailerType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDFormation(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DFormation> formations = new ArrayList<>();
		for (String value : values) {
			if (!formationTypeRepository.existsByValue(value)) {
				DFormation formation = new DFormation();
				formation.setCode(SystemType.FORMATION_TYPE);
				formation.setDescription("Formation Type");
				formation.setStatus(SystemType.STATUS_ACTIVE);
				formation.setValue(value);
				formations.add(formation);
			}
		}
		formationTypeRepository.saveAll(formations);
	}

	@Override
	public List<DFormation> retrieveDFormation() {
		return formationTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDFormation(DFormation formationType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDSupplierType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DSupplierType> supplierTypes = new ArrayList<>();
		for (String value : values) {
			if (!supplierTypeRepository.existsByValue(value)) {
				DSupplierType supplierType = new DSupplierType();
				supplierType.setCode(SystemType.SUPPLIER_TYPE);
				supplierType.setDescription("Supplier Type");
				supplierType.setStatus(SystemType.STATUS_ACTIVE);
				supplierType.setValue(value);
				supplierTypes.add(supplierType);
			}
		}
		supplierTypeRepository.saveAll(supplierTypes);
	}

	@Override
	public List<DSupplierType> retrieveDSupplierType() {
		return supplierTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDSupplierType(DSupplierType supplierType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDContractType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DContractType> contractTypes = new ArrayList<>();
		for (String value : values) {
			if (!contractTypeRepository.existsByValue(value)) {
				DContractType contractType = new DContractType();
				contractType.setCode(SystemType.CONTRACT_TYPE);
				contractType.setDescription("Contract Type");
				contractType.setStatus(SystemType.STATUS_ACTIVE);
				contractType.setValue(value);
				contractTypes.add(contractType);
			}
		}
		contractTypeRepository.saveAll(contractTypes);
	}

	@Override
	public List<DContractType> retrieveDContractType() {
		return contractTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDContractType(DContractType contractType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDCity(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DCity> citys = new ArrayList<>();
		for (String value : values) {
			if (!cityRepository.existsByValue(value)) {
				DCity city = new DCity();
				city.setCode(SystemType.ASSET_TYPE);
				city.setDescription("City Type");
				city.setStatus(SystemType.STATUS_ACTIVE);
				city.setValue(value);
				citys.add(city);
			}
		}
		cityRepository.saveAll(citys);
	}

	@Override
	public List<DCity> retrieveDCity() {
		List<DCity> cities = cityRepository.findAllByDescriptionAndStatus("City", SystemType.STATUS_ACTIVE);

		return cities;
	}

	@Override
	public void updateDCity(DCity city) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDBaseStation(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DBaseStation> baseStations = new ArrayList<>();
		for (String value : values) {
			if (!baseStationRepository.existsByValue(value)) {
				DBaseStation baseStation = new DBaseStation();
				baseStation.setCode(SystemType.BASE_STATION_TYPE);
				baseStation.setDescription("Base Station Type");
				baseStation.setStatus(SystemType.STATUS_ACTIVE);
				baseStation.setValue(value);
				baseStations.add(baseStation);
			}
		}
		baseStationRepository.saveAll(baseStations);

	}

	@Override
	public List<DBaseStation> retrieveDBaseStation() {
		return baseStationRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDBaseStation(DBaseStation baseStation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDJobStatus(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DJobStatus> jobStatuss = new ArrayList<>();
		for (String value : values) {
			if (!jobStatusRepository.existsByValue(value)) {
				DJobStatus jobStatus = new DJobStatus();
				jobStatus.setCode(SystemType.JOB_STATUS_TYPE);
				jobStatus.setDescription("Job Status Type");
				jobStatus.setStatus(SystemType.STATUS_ACTIVE);
				jobStatus.setValue(value);
				jobStatuss.add(jobStatus);
			}
		}
		jobStatusRepository.saveAll(jobStatuss);

	}

	@Override
	public List<DJobStatus> retrieveDJobStatus() {
		return jobStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDJobStatus(DJobStatus jobStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDVehicleOwnership(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DVehicleOwnership> vehicleOwnerships = new ArrayList<>();
		for (String value : values) {
			if (!vehicleOwnershipRepository.existsByValue(value)) {
				DVehicleOwnership vehicleOwnership = new DVehicleOwnership();
				vehicleOwnership.setCode(SystemType.VEHICLE_OWNERSHIP_TYPE);
				vehicleOwnership.setDescription("Vehicle Ownership Type");
				vehicleOwnership.setStatus(SystemType.STATUS_ACTIVE);
				vehicleOwnership.setValue(value);
				vehicleOwnerships.add(vehicleOwnership);
			}
		}
		vehicleOwnershipRepository.saveAll(vehicleOwnerships);
	}

	@Override
	public List<DVehicleOwnership> retrieveDVehicleOwnership() {
		return vehicleOwnershipRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDVehicleOwnership(DVehicleOwnership vehicleOwnershipType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDRWBStatus(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DRWBStatus> rwbStatuss = new ArrayList<>();
		for (String value : values) {
			if (!rwbStatusRepository.existsByValue(value)) {
				DRWBStatus rwbStatus = new DRWBStatus();
				rwbStatus.setCode(SystemType.RWB_STATUS_TYPE);
				rwbStatus.setDescription("RWB Status Type");
				rwbStatus.setStatus(SystemType.STATUS_ACTIVE);
				rwbStatus.setValue(value);
				rwbStatuss.add(rwbStatus);
			}
		}
		rwbStatusRepository.saveAll(rwbStatuss);

	}

	@Override
	public List<DRWBStatus> retrieveDRWBStatus() {
		return rwbStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDRWBStatus(DRWBStatus rwbStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDCategory(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DCategory> categorys = new ArrayList<>();
		for (String value : values) {
			if (!categoryRepository.existsByValue(value)) {
				DCategory category = new DCategory();
				category.setCode(SystemType.CATEGORY_TYPE);
				category.setDescription("Category Type");
				category.setStatus(SystemType.STATUS_ACTIVE);
				category.setValue(value);
				categorys.add(category);
			}
		}
		categoryRepository.saveAll(categorys);

	}

	@Override
	public List<DCategory> retrieveDCategory() {
		return categoryRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDCategory(DCategory category) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDStopType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DStopType> stopTypes = new ArrayList<>();
		for (String value : values) {
			if (!stopTypeRepository.existsByValue(value)) {
				DStopType stopType = new DStopType();
				stopType.setCode(SystemType.STOP_TYPE);
				stopType.setDescription("Stop Type");
				stopType.setStatus(SystemType.STATUS_ACTIVE);
				stopType.setValue(value);
				stopTypes.add(stopType);
			}
		}
		stopTypeRepository.saveAll(stopTypes);

	}

	@Override
	public List<DStopType> retrieveDStopType() {
		return stopTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDStopType(DStopType stopType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDFuelTankType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DFuelTankType> fuelTankTypes = new ArrayList<>();
		for (String value : values) {
			if (!fuelTankTypeRepository.existsByValue(value)) {
				DFuelTankType fuelTankType = new DFuelTankType();
				fuelTankType.setCode(SystemType.FUEL_TANK_TYPE);
				fuelTankType.setDescription("Fuel Tank Type");
				fuelTankType.setStatus(SystemType.STATUS_ACTIVE);
				fuelTankType.setValue(value);
				fuelTankTypes.add(fuelTankType);
			}
		}
		fuelTankTypeRepository.saveAll(fuelTankTypes);

	}

	@Override
	public List<DFuelTankType> retrieveDFuelTankType() {
		return fuelTankTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDFuelTankType(DFuelTankType fuelTankType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDPaymentMode(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DPaymentMode> paymentModes = new ArrayList<>();
		for (String value : values) {
			if (!paymentModeRepository.existsByValue(value)) {
				DPaymentMode paymentMode = new DPaymentMode();
				paymentMode.setCode(SystemType.PAYMENT_MODE_TYPE);
				paymentMode.setDescription("Payment Mode Type");
				paymentMode.setStatus(SystemType.STATUS_ACTIVE);
				paymentMode.setValue(value);
				paymentModes.add(paymentMode);
			}
		}
		paymentModeRepository.saveAll(paymentModes);
	}

	@Override
	public List<DPaymentMode> retrieveDPaymentMode() {
		return paymentModeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDPaymentMode(DPaymentMode paymentMode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDLoadStatus(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DLoadStatus> loadStatuss = new ArrayList<>();
		for (String value : values) {
			if (!loadStatusRepository.existsByValue(value)) {
				DLoadStatus loadStatus = new DLoadStatus();
				loadStatus.setCode(SystemType.LOAD_STATUS_TYPE);
				loadStatus.setDescription("Load Status Type");
				loadStatus.setStatus(SystemType.STATUS_ACTIVE);
				loadStatus.setValue(value);
				loadStatuss.add(loadStatus);
			}
		}
		loadStatusRepository.saveAll(loadStatuss);

	}

	@Override
	public List<DLoadStatus> retrieveDLoadStatus() {
		return loadStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDLoadStatus(DLoadStatus loadStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDCustomerType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DCustomerType> customerTypes = new ArrayList<>();
		for (String value : values) {
			if (!customerTypeRepository.existsByValue(value)) {
				DCustomerType customerType = new DCustomerType();
				customerType.setCode(SystemType.CUSTOMER_TYPE);
				customerType.setDescription("Customer Type");
				customerType.setStatus(SystemType.STATUS_ACTIVE);
				customerType.setValue(value);
				customerTypes.add(customerType);
			}
		}
		customerTypeRepository.saveAll(customerTypes);

	}

	@Override
	public List<DCustomerType> retrieveDCustomerType() {
		return customerTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDCustomerType(DCustomerType customerType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDIncomeType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DIncomeType> incomeTypes = new ArrayList<>();
		for (String value : values) {
			if (!incomeTypeRepository.existsByValue(value)) {
				DIncomeType incomeType = new DIncomeType();
				incomeType.setCode(SystemType.INCOME_TYPE);
				incomeType.setDescription("Income Type");
				incomeType.setStatus(SystemType.STATUS_ACTIVE);
				incomeType.setValue(value);
				incomeTypes.add(incomeType);
			}
		}
		incomeTypeRepository.saveAll(incomeTypes);

	}

	@Override
	public List<DIncomeType> retrieveDIncomeType() {
		return incomeTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDIncomeType(DIncomeType incomeType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDExpenseType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DExpenseType> expenseTypes = new ArrayList<>();
		for (String value : values) {
			if (!expenseTypeRepository.existsByValue(value)) {
				DExpenseType expenseType = new DExpenseType();
				expenseType.setCode(SystemType.EXPENSE_TYPE);
				expenseType.setDescription("Expense Type");
				expenseType.setStatus(SystemType.STATUS_ACTIVE);
				expenseType.setValue(value);
				expenseTypes.add(expenseType);
			}
		}
		expenseTypeRepository.saveAll(expenseTypes);

	}

	@Override
	public List<DExpenseType> retrieveDExpenseType() {
		return expenseTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDExpenseType(DExpenseType expenseType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDCompanyType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DCompanyType> companyTypes = new ArrayList<>();
		for (String value : values) {
			if (!companyTypeRepository.existsByValue(value)) {
				DCompanyType companyType = new DCompanyType();
				companyType.setCode(SystemType.COMPANY_TYPE);
				companyType.setDescription("Company Type");
				companyType.setStatus(SystemType.STATUS_ACTIVE);
				companyType.setValue(value);
				companyTypes.add(companyType);
			}
		}
		companyTypeRepository.saveAll(companyTypes);

	}

	@Override
	public List<DCompanyType> retrieveDCompanyType() {
		return companyTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDCompanyType(DCompanyType companyType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDState(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DState> states = new ArrayList<>();
		for (String value : values) {
			if (!stateRepository.existsByValue(value)) {
				DState state = new DState();
				state.setCode(SystemType.STATE_TYPE);
				state.setDescription("State Type");
				state.setStatus(SystemType.STATUS_ACTIVE);
				state.setValue(value);
				states.add(state);
			}
		}
		stateRepository.saveAll(states);

	}

	@Override
	public List<DState> retrieveDState() {
		return stateRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDState(DState state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDStopProgressType(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DStopProgressType> stopProgressTypes = new ArrayList<>();
		for (String value : values) {
			if (!stopProgressTypeRepository.existsByValue(value)) {
				DStopProgressType stopProgressType = new DStopProgressType();
				stopProgressType.setCode(SystemType.STOP_PROGRESS_TYPE);
				stopProgressType.setDescription("Stop Progress Type");
				stopProgressType.setStatus(SystemType.STATUS_ACTIVE);
				stopProgressType.setValue(value);
				stopProgressTypes.add(stopProgressType);
			}
		}
		stopProgressTypeRepository.saveAll(stopProgressTypes);

	}

	@Override
	public List<DStopProgressType> retrieveDStopProgressType() {
		return stopProgressTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDStopProgressType(DStopProgressType stopProgressType) {
		// TODO Auto-generated method stub

	}

	@Override
	public DCity createDCity(DCity city) {
		Assert.notNull(city, "City: " + SystemType.MUST_NOT_BE_NULL);
		generalValidator.validateCity(city);
		if (cityRepository.existsByValue(city.getValue()) || cityRepository.existsByCode(city.getCode()))
			throw new EntityExistsException("City already exists!");
		city.setId(null);
		city.setDescription("City");
		city.setStatus(SystemType.STATUS_ACTIVE);
		return cityRepository.save(city);
	}

	@Override
	public void createDReimbursementStatus(List<String> values) {
		if (values == null || values.size() == 0)
			return;
		List<DReimbursementStatus> reimbursementStatuses = new ArrayList<>();
		for (String value : values) {
			if (!reimbursementStatusRepository.existsByValue(value)) {
				DReimbursementStatus reimbursementStatus = new DReimbursementStatus();
				reimbursementStatus.setCode(SystemType.STOP_PROGRESS_TYPE);
				reimbursementStatus.setDescription("Stop Progress Type");
				reimbursementStatus.setStatus(SystemType.STATUS_ACTIVE);
				reimbursementStatus.setValue(value);
				reimbursementStatuses.add(reimbursementStatus);
			}
		}
		reimbursementStatusRepository.saveAll(reimbursementStatuses);
	}

	@Override
	public List<DReimbursementStatus> retrieveDReimbursementStatus() {
		return reimbursementStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDReimbursementStatus(DReimbursementStatus reimbursementStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public DCity createArea(String cityCode, DCity area) {
		Assert.notNull(area, "Area: " + SystemType.MUST_NOT_BE_NULL);
		generalValidator.validateArea(area);
		if (cityRepository.existsByValue(area.getValue()) || cityRepository.existsByCode(area.getCode()))
			throw new EntityExistsException("Area already exists!");
		if (!cityRepository.existsByCode(cityCode))
			throw new NoSuchElementException("City does not exist!");
		area.setId(null);
		area.setState(null);
		area.setDescription(cityCode + "-Area");
		area.setStatus(SystemType.STATUS_ACTIVE);
		return cityRepository.save(area);
	}

	@Override
	public List<DCity> retrieveArea(String cityCode) {
		Assert.notNull(cityCode, "City code: " + SystemType.MUST_NOT_BE_NULL);
		return cityRepository.findAllByDescriptionAndStatus(cityCode + "-Area", SystemType.STATUS_ACTIVE);
	}

	@Override
	public void createDInvoiceStatus(List<String> invoiceStatus) {

	}

	@Override
	public List<DInvoiceStatus> retrieveDInvoiceStatus() {
		return invoiceStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDInvoiceStatus(DInvoiceStatus invoiceStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDPaymentStatus(List<String> paymentStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<DPaymentStatus> retrieveDPaymentStatus() {
		return paymentStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDPaymentStatus(DPaymentStatus paymentStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public DItemCategory createDItemCategory(DItemCategory itemCategory) {
		Assert.notNull(itemCategory, "Item Category: " + SystemType.MUST_NOT_BE_NULL);
		if (itemCategoryRepository.existsByValue(itemCategory.getValue()))
			throw new EntityExistsException("Item category already exists!");
		itemCategory.setId(null);
		itemCategory.setDescription("Item Category");
		itemCategory.setStatus(SystemType.STATUS_ACTIVE);
		return itemCategoryRepository.save(itemCategory);
	}

	@Override
	public List<DItemCategory> retrieveDItemCategory() {
		return itemCategoryRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DBrand createDBrand(DBrand brand) {
		Assert.notNull(brand, "Brand: " + SystemType.MUST_NOT_BE_NULL);
		if (brandRepository.existsByValue(brand.getValue()))
			throw new EntityExistsException("Brand already exists!");
		brand.setId(null);
		brand.setDescription("Brand");
		brand.setStatus(SystemType.STATUS_ACTIVE);
		return brandRepository.save(brand);
	}

	@Override
	public List<DBrand> retrieveDBrand() {
		return brandRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DUnit createDUnit(DUnit unit) {
		Assert.notNull(unit, "Unit: " + SystemType.MUST_NOT_BE_NULL);
		if (unitRepository.existsByValue(unit.getValue()))
			throw new EntityExistsException("Unit already exists!");
		unit.setId(null);
		unit.setDescription("Unit");
		unit.setStatus(SystemType.STATUS_ACTIVE);
		return unitRepository.save(unit);
	}

	@Override
	public List<DUnit> retrieveDUnit() {
		return unitRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DItemType createDItemType(DItemType itemType) {
		Assert.notNull(itemType, "Item Type: " + SystemType.MUST_NOT_BE_NULL);
		if (itemTypeRepository.existsByValue(itemType.getValue()))
			throw new EntityExistsException("Item type already exists!");
		itemType.setId(null);
		itemType.setDescription("Item Type");
		itemType.setStatus(SystemType.STATUS_ACTIVE);
		return itemTypeRepository.save(itemType);
	}

	@Override
	public List<DItemType> retrieveDItemType() {
		return itemTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DPurchaseOrderStatus createDPurchaseOrderStatus(DPurchaseOrderStatus purchaseOrderStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DPurchaseOrderStatus> retrieveDPurchaseOrderStatus() {
		return purchaseOrderStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DWorkOrderStatus createDWorkOrderStatus(DWorkOrderStatus workOrderStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DWorkOrderStatus> retrieveDWorkOrderStatus() {
		// TODO Auto-generated method stub
		return workOrderStatusRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DWorkOrderCategory createDWorkOrderCategory(DWorkOrderCategory workOrderCategory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DWorkOrderCategory> retrieveDWorkOrderCategory() {
		// TODO Auto-generated method stub
		return workOrderCategoryRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DWorkOrderSubCategory createDWorkOrderSubCategory(DWorkOrderSubCategory workOrderSubCategory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DWorkOrderSubCategory> retrieveDWorkOrderSubCategory() {
		// TODO Auto-generated method stub
		return workOrderSubCategoryRespository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public DPriority createDPriority(DPriority priority) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DPriority> retrieveDPriority() {
		// TODO Auto-generated method stub
		return priorityRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void createDInvoiceType(List<String> invoiceType) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<DInvoiceType> retrieveDInvoiceType() {
		return invoiceTypeRepository.findAllByStatus(SystemType.STATUS_ACTIVE);
	}

	@Override
	public void updateDInvoiceType(DInvoiceType invoiceType) {
		// TODO Auto-generated method stub

	}

}
