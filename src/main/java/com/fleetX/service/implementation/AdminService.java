package com.fleetX.service.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IAdminDAO;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.repository.AddressRepository;
import com.fleetX.repository.ContactRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IDropDownService;
import com.fleetX.validator.IAdminValidator;
import com.fleetX.validator.IGeneralValidator;

@Service
@Transactional
public class AdminService implements IAdminService {
	@Autowired
	IAdminDAO adminDAO;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IDropDownService dropDownService;
	@Autowired
	IContractService contractService;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	ContactRepository contactRepository;
	@Autowired
	IAdminValidator adminValidator;
	@Autowired
	IGeneralValidator generalValidator;

	@Override
	// @Transactional
	public AdminCompany addAdminCompany(AdminCompany adminCompany) throws Exception {
		Assert.notNull(adminCompany, "Admin Company: " + SystemType.MUST_NOT_BE_NULL);
		adminValidator.validateAdminCompany(adminCompany);

		adminCompany.setAdminCompanyId(null);
		adminCompany.getAdminCompanyAddresses().stream().forEach(x -> {
			x.setAddressId(null);
			x = addressRepository.save(x);
		});

		adminCompany.getAdminCompanyContacts().stream().forEach(x -> {
			x.setContactId(null);
			x = contactRepository.save(x);
		});
		return adminDAO.createAdminCompany(adminCompany);
	}

	@Override
	// @Transactional
	public List<AdminCompany> findAllAdminCompany() {
		return adminDAO.findAllAdminCompany();
	}

	@Override
	// @Transactional
	public AdminCompany findAdminCompanyById(String adminCompanyId) {
		Assert.notNull(adminCompanyId, "Admin Company ID: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.findAdminCompanyById(adminCompanyId);
	}

	@Override
	// @Transactional
	public AdminCompany updateAdminCompany(AdminCompany adminCompanyNew) throws Exception {
		Assert.notNull(adminCompanyNew, "Admin Company: " + SystemType.MUST_NOT_BE_NULL);
		adminValidator.validateAdminCompany(adminCompanyNew);

		AdminCompany companyOld = findAdminCompanyById(adminCompanyNew.getAdminCompanyId());
		companyOld.setAdminCompanyCode(adminCompanyNew.getAdminCompanyCode());
		companyOld.setAdminCompanyName(adminCompanyNew.getAdminCompanyName());
		companyOld.setAdminCompanyFormation(adminCompanyNew.getAdminCompanyFormation());
		companyOld.setBra(adminCompanyNew.getBra());
		companyOld.setBusinessType(adminCompanyNew.getBusinessType());
		companyOld.setKra(adminCompanyNew.getKra());
		companyOld.setNtnNumber(adminCompanyNew.getNtnNumber());
		companyOld.setPra(adminCompanyNew.getPra());
		companyOld.setSra(adminCompanyNew.getSra());
		companyOld.setStatus(adminCompanyNew.getStatus());
		companyOld.setStrnNumber(adminCompanyNew.getStrnNumber());

		List<Address> addressesOld = companyOld.getAdminCompanyAddresses();
		List<Address> addressesNew = adminCompanyNew.getAdminCompanyAddresses();
		addressesNew = adjustAddresses(addressesOld, addressesNew);

		List<Contact> contactsOld = companyOld.getAdminCompanyContacts();
		List<Contact> contactsNew = adminCompanyNew.getAdminCompanyContacts();
		contactsNew = adjustContacts(contactsOld, contactsNew);

		companyOld.setAdminCompanyAddresses(addressesNew);
		companyOld.setAdminCompanyContacts(contactsNew);
		return adminDAO.updateAdminCompany(companyOld);
	}

	@Override
	// @Transactional
	public List<Address> adjustAddresses(List<Address> addressesOld, List<Address> addressesNew) {
		Assert.notNull(addressesOld, "Addresses: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(addressesNew, "Addresses: " + SystemType.MUST_NOT_BE_NULL);
		addressesNew.stream().forEach(x -> {
			if (!addressesOld.contains(x)) {
				x.setAddressId(null);
			}
		});
		addressesOld.removeIf(x -> addressesNew.contains(x));
		addressesOld.stream().forEach(x -> {
			x.setIsDeleted(true);
		});
		addressesNew.addAll(addressesOld);
		addressesNew.stream().forEach(x -> {
			if (x.getAddressId() == null)
				addressRepository.save(x);
		});
		return addressesNew;
	}

	@Override
	// @Transactional
	public List<Contact> adjustContacts(List<Contact> contactsOld, List<Contact> contactsNew) {
		Assert.notNull(contactsOld, "Contacts: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(contactsNew, "Contacts: " + SystemType.MUST_NOT_BE_NULL);
		contactsNew.stream().forEach(x -> {
			if (!contactsOld.contains(x)) {
				x.setContactId(null);
			}
		});
		contactsOld.removeIf(x -> contactsNew.contains(x));
		contactsOld.stream().forEach(x -> {
			x.setIsDeleted(true);
		});
		contactsNew.addAll(contactsOld);
		contactsNew.stream().forEach(x -> contactRepository.save(x));
		return contactsNew;
	}

	@Override
	// @Transactional
	public void deleteAdminCompany(String adminCompanyId) throws Exception {
		Assert.notNull(adminCompanyId, "Admin Company ID: " + SystemType.MUST_NOT_BE_NULL);
//		AdminCompany adminCompany = adminDAO.findAdminCompanyById(adminCompanyId);
//		List<Address> addresses = adminCompany.getAdminCompanyAddresses();
//		addresses.parallelStream().forEach(x -> x.setIsDeleted(true));
//		List<Contact> contacts = adminCompany.getAdminCompanyContacts();
//		contacts.parallelStream().forEach(x -> x.setIsDeleted(true));
//		List<Employee> employees = adminCompany.getAdminCompanyEmployees();
//		employees.parallelStream().forEach(x -> x.setIsDeleted(true));
//		adminCompany.setIsDeleted(true);
		adminDAO.deleteAdminCompany(adminCompanyId);
	}

	@Override
	// @Transactional
	public List<AdminCompany> filterAdminCompanies(Example<AdminCompany> acExample) {
		Assert.notNull(acExample, SystemType.MUST_NOT_BE_NULL);
		return adminDAO.filterAdminCompany(acExample);
	}

	// FUEL CARDS
	@Override
	// @Transactional
	public FuelCard addFuelCard(FuelCard fuelCard) {
		Assert.notNull(fuelCard, "Fuel Card: " + SystemType.MUST_NOT_BE_NULL);
		adminValidator.validateFuelCard(fuelCard);

		fuelCard.setFuelCardId(null);
		return adminDAO.addFuelCard(fuelCard);
	}

	@Override
	// @Transactional
	public List<FuelCard> getAllFuelCard() {
		return adminDAO.getAllFuelCard();
	}

	@Override
	// @Transactional
	public List<FuelCard> filterFuelCard(String customerId, String acId, String status) {
		Company customer = null;
		AdminCompany adminCompany = null;
		if (customerId != null) {
			customer = companyService.findCompanyById(customerId);
		}
		if (acId != null) {
			adminCompany = adminDAO.findAdminCompanyById(acId);
		}
		FuelCard fuelCardExample = new FuelCard();
		fuelCardExample.setCustomer(customer);
		fuelCardExample.setAdminCompany(adminCompany);
		fuelCardExample.setStatus(status);
		return adminDAO.filterFuelCard(Example.of(fuelCardExample));
	}

	@Override
	// @Transactional
	public FuelCard updateFuelCard(FuelCard fuelCardNew) {
		Assert.notNull(fuelCardNew, "Fuel Card: " + SystemType.MUST_NOT_BE_NULL);
		adminValidator.validateFuelCard(fuelCardNew);

		FuelCard fuelCardOld = adminDAO.findFuelCardById(fuelCardNew.getFuelCardId());
		fuelCardOld.setAdminCompany(fuelCardNew.getAdminCompany());
		fuelCardOld.setCustomer(fuelCardNew.getCustomer());
		fuelCardOld.setSupplier(fuelCardNew.getSupplier());
		fuelCardOld.setExpiry(fuelCardNew.getExpiry());
		fuelCardOld.setFuelCardNumber(fuelCardNew.getFuelCardNumber());
		fuelCardOld.setLimit(fuelCardNew.getLimit());
		fuelCardOld.setStatus(fuelCardNew.getStatus());
		return adminDAO.updateFuelCard(fuelCardOld);
	}

	@Override
	// @Transactional
	public void deleteFuelCardById(Integer fuelCardId) {
		Assert.notNull(fuelCardId, "Fuel Card ID: " + SystemType.MUST_NOT_BE_NULL);
		adminDAO.deleteFuelCardById(fuelCardId);
	}

	@Override
	// @Transactional
	public FuelCard findFuelCardById(Integer fuelCardId) {
		Assert.notNull(fuelCardId, "Fuel Card ID: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.findFuelCardById(fuelCardId);
	}

	// ROUTES
	@Override
	// @Transactional
	public Route addRoute(Route route) {
		Assert.notNull(route, "Route: " + SystemType.MUST_NOT_BE_NULL);
		adminValidator.validateRoute(route);

		route.setRouteId(null);
		Route routeExample = new Route();
		routeExample.setRouteFrom(route.getRouteFrom());
		routeExample.setRouteTo(route.getRouteTo());
		List<Route> routes = adminDAO.filterRoute(Example.of(routeExample));
		if (routes.size() > 0)
			throw new EntityExistsException("One or More Routes: " + SystemType.ALREADY_EXISTS);
		return adminDAO.addRoute(route);
	}

	@Override
	// @Transactional
	public List<Route> addAllRoute(List<Route> routes) {
		Assert.notNull(routes, "Route: " + SystemType.MUST_NOT_BE_NULL);
		List<Route> routesAdded = new ArrayList<>();
		for (Route route : routes) {
			routesAdded.add(addRoute(route));
		}
		return routesAdded;
	}

	@Override
	// @Transactional
	public List<Route> getAllInterCityRoute() {
		List<Route> routes = adminDAO.getAllRoute();
		routes.removeIf(x -> !x.getRouteFrom().getDescription().equals("City") || !x.getRouteTo().getDescription().equals("City"));
		return routes;
	}

	@Override
	public List<Route> getAllIntraCityRoute() {
		List<Route> routes = adminDAO.getAllRoute();
		routes.removeIf(x -> x.getRouteFrom().getDescription().equals("City") || x.getRouteTo().getDescription().equals("City"));
		return routes;
	}

	@Override
	// @Transactional
	public Route updateRoute(Route routeNew) {
		Assert.notNull(routeNew, "Route: " + SystemType.MUST_NOT_BE_NULL);
		adminValidator.validateRoute(routeNew);

		Route routeOld = adminDAO.findRouteById(routeNew.getRouteId());
		routeOld.setRouteTo(routeNew.getRouteTo());
		routeOld.setRouteFrom(routeNew.getRouteFrom());
		routeOld.setAvgDistanceInKM(routeNew.getAvgDistanceInKM());
		return adminDAO.updateRoute(routeOld);
	}

	@Override
	// @Transactional
	public void deleteRouteById(String routeId) {
		Assert.notNull(routeId, "Route ID: " + SystemType.MUST_NOT_BE_NULL);
		adminDAO.deleteRouteById(routeId);
	}

	@Override
	// @Transactional
	public Route findRouteById(String routeId) {
		Assert.notNull(routeId, "Route ID: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.findRouteById(routeId);
	}

	@Override
	// @Transactional
	public List<Route> filterRoute(String to, String from) {
		DCity routeTo = null;
		DCity routeFrom = null;
		if (to != null) {
			List<DCity> cities = dropDownService.retrieveDCity();
			for (DCity city : cities) {
				if (city.getCode().equals(to)) {
					routeTo = city;
				}
			}
		}
		if (from != null) {
			List<DCity> cities = dropDownService.retrieveDCity();
			for (DCity city : cities) {
				if (city.getCode().equals(from)) {
					routeFrom = city;
				}
			}
		}
		Route routeExample = new Route();
		routeExample.setRouteTo(routeTo);
		routeExample.setRouteFrom(routeFrom);
		return adminDAO.filterRoute(Example.of(routeExample));
	}

	// ROLES
	@Override
	// @Transactional
	public Role addRole(Role role) {
		Assert.notNull(role, "Role: " + SystemType.MUST_NOT_BE_NULL);
		generalValidator.validateRole(role);

		try {
			Role roleInDB = adminDAO.getRoleByRoleName(role.getRoleName());
			throw new EntityExistsException("Role name: " + SystemType.ALREADY_EXISTS);
		} catch (NoSuchElementException e) {
			return adminDAO.addRole(role);
		}
	}

	@Override
	// @Transactional
	public Role updateRole(Role roleNew) {
		Assert.notNull(roleNew, "Role: " + SystemType.MUST_NOT_BE_NULL);
		generalValidator.validateRole(roleNew);

		Role roleOld = adminDAO.getRoleByRoleName(roleNew.getRoleName());
		roleOld.setRoleName(roleNew.getRoleName());
		roleOld.setPrivileges(roleNew.getPrivileges());
		return adminDAO.updateRole(roleOld);
	}

	@Override
	// @Transactional
	public List<Role> getAllRole() {
		return adminDAO.getAllRole();
	}

	@Override
	// @Transactional
	public Role getRoleByRoleName(String roleName) {
		Assert.notNull(roleName, "Role name: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.getRoleByRoleName(roleName);
	}

	@Override
	// @Transactional
	public void deleteRoleByRoleName(String roleName) {
		Assert.notNull(roleName, "Role name: " + SystemType.MUST_NOT_BE_NULL);
		adminDAO.deleteRoleByRoleName(roleName);
	}

	@Override
	// @Transactional
	public void deleteAllRole() {
		adminDAO.deleteAllRole();
	}

	@Override
	// @Transactional
	public List<String> getPrivilegesByRoleName(String roleName) {
		Assert.notNull(roleName, "Role name: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.getPrivilegesByRoleName(roleName);
	}

	// USERS
	@Override
	// @Transactional
	public User addUser(User user) {
		Assert.notNull(user, "User: " + SystemType.MUST_NOT_BE_NULL);
		generalValidator.validateUser(user);

		try {
			adminDAO.getUserByUsername(user.getUsername());
			throw new EntityExistsException("Username: " + SystemType.ALREADY_EXISTS);
		} catch (NoSuchElementException e) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			adminDAO.addUser(user);
			return user;
		}
	}

	@Override
	// @Transactional
	public User updateUser(User userNew) {
		Assert.notNull(userNew, "User: " + SystemType.MUST_NOT_BE_NULL);
		generalValidator.validateUser(userNew);

		User userOld = adminDAO.getUserByUsername(userNew.getUsername());
		userOld.setFirstName(userNew.getFirstName());
		userOld.setLastName(userNew.getLastName());
		userOld.setStatus(userNew.getStatus());
		if (userNew.getPassword() != null)
			userOld.setPassword(passwordEncoder.encode(userNew.getPassword()));
		userOld.setRoles(userNew.getRoles());
		userOld.setAdditionalPrivileges(userNew.getAdditionalPrivileges());
		return adminDAO.updateUser(userOld);
	}

	@Override
	// @Transactional
	public List<User> getAllUser() {
		return adminDAO.getAllUser();
	}

	@Override
	// @Transactional
	public User getUserByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.getUserByUsername(username);
	}

	@Override
	// @Transactional
	public void deleteUserByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		adminDAO.deleteUserByUsername(username);
	}

	@Override
	// @Transactional
	public void deleteAllUser() {
		adminDAO.deleteAllUser();
	}

	@Override
	// @Transactional
	public List<String> getRoleNamesByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.getRoleNamesByUsername(username);
	}

	@Override
	// @Transactional
	public List<String> getAdditionalPrivilegesByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		return adminDAO.getAdditionalPrivilegesByUsername(username);
	}

	@Override
	public List<Route> getIntraCityRoutesByCity(String cityCode) {
		List<Route> routes = getAllIntraCityRoute();
		return routes.stream().filter(x -> x.getRouteFrom().getDescription().startsWith(cityCode)
				|| x.getRouteTo().getDescription().startsWith(cityCode)).collect(Collectors.toList());
//		return routes;
	}

}
