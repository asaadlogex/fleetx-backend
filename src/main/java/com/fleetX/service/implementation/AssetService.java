package com.fleetX.service.implementation;

import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IAssetDAO;
import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.service.IAssetService;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IOrderService;
import com.fleetX.validator.IAssetValidator;

@Service
@Transactional
public class AssetService implements IAssetService {

	@Autowired
	IAssetDAO assetDAO;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	IAssetValidator assetValidator;
	@Autowired
	IOrderService orderService;

	@Override
	// @Transactional
	public Container addContainer(Container container) {
		Assert.notNull(container, "Container: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateContainer(container);
		container.setContainerId(null);
		container.setAvailability(SystemType.STATUS_AVAILABLE);
		container.setMovingStatus(SystemType.STATUS_MOVING_FREE);
		return assetDAO.createContainer(container);
	}

	@Override
	// @Transactional
	public Container findContainerById(String containerId) {
		Assert.notNull(containerId, "Container ID: " + SystemType.MUST_NOT_BE_NULL);
		return assetDAO.findContainerById(containerId);
	}

	@Override
	// @Transactional
	public List<Container> findAllContainers() {
		return assetDAO.findAllContainers();
	}

	@Override
	// @Transactional
	public List<Container> filterContainer(String movingStatus, String rwbId, String avail) {
		Container container = new Container();
		container.setMovingStatus(movingStatus);
		container.setAvailability(avail);
		List<Container> containers = assetDAO.filterContainerByExample(Example.of(container));
		if (rwbId != null) {
			RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
			if (rwb.getContainer() != null)
				containers.add(rwb.getContainer());
		}
		return containers;
	}

	@Override
	// @Transactional
	public Container updateContainer(Container containerNew) {
		Assert.notNull(containerNew, "Container: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateContainer(containerNew);

		Container containerOld = assetDAO.findContainerById(containerNew.getContainerId());
		containerOld.setAdminCompany(containerNew.getAdminCompany());
		containerOld.setContainerSize(containerNew.getContainerSize());
		containerOld.setContainerType(containerNew.getContainerType());
		containerOld.setContainerWeight(containerNew.getContainerWeight());
		containerOld.setContainerWeightUnit(containerNew.getContainerWeightUnit());
		containerOld.setLeaseType(containerNew.getLeaseType());
		containerOld.setSupplier(containerNew.getSupplier());
		return assetDAO.updateContainer(containerOld);
	}

	@Override
	// @Transactional
	public void deleteContainer(String containerId) {
		Assert.notNull(containerId, "Container ID: " + SystemType.MUST_NOT_BE_NULL);
		assetDAO.deleteContainer(containerId);
	}

	@Override
	// @Transactional
	public Tracker addTracker(Tracker tracker) {
		Assert.notNull(tracker, "Tracker: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTracker(tracker);

		tracker.setTrackerId(null);
		tracker.setAvailability(SystemType.STATUS_AVAILABLE);
		tracker.setMovingStatus(SystemType.STATUS_MOVING_FREE);
		return assetDAO.createTracker(tracker);
	}

	@Override
	// @Transactional
	public Tracker findTrackerById(String trackerId) {
		Assert.notNull(trackerId, "Tracker ID: " + SystemType.MUST_NOT_BE_NULL);
		return assetDAO.findTrackerById(trackerId);
	}

	@Override
	// @Transactional
	public List<Tracker> findAllTrackers() {
		return assetDAO.findAllTrackers();
	}

	@Override
	// @Transactional
	public List<Tracker> filterTracker(String movingStatus, String rwbId, String avail) {
		Tracker tracker = new Tracker();
		tracker.setMovingStatus(movingStatus);
		tracker.setAvailability(avail);
		List<Tracker> trackers = assetDAO.filterTrackerByExample(Example.of(tracker));
		if (rwbId != null) {
			RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
			if (rwb.getTracker() != null)
				trackers.add(rwb.getTracker());
		}
		return trackers;
	}

	@Override
	// @Transactional
	public Tracker updateTracker(Tracker trackerNew) {
		Assert.notNull(trackerNew, "Tracker: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTracker(trackerNew);

		Tracker trackerOld = assetDAO.findTrackerById(trackerNew.getTrackerId());
		trackerOld.setAdminCompany(trackerNew.getAdminCompany());
		trackerOld.setContractTerms(trackerNew.getContractTerms());
		trackerOld.setLeaseType(trackerNew.getLeaseType());
		trackerOld.setStatus(trackerNew.getStatus());
		trackerOld.setSupplier(trackerNew.getSupplier());
		trackerOld.setTrackerNumber(trackerNew.getTrackerNumber());
		trackerOld.setTrackerName(trackerNew.getTrackerName());
		return assetDAO.updateTracker(trackerOld);
	}

	@Override
	// @Transactional
	public void deleteTracker(String trackerId) {
		Assert.notNull(trackerId, "Tracker ID: " + SystemType.MUST_NOT_BE_NULL);
		assetDAO.deleteTracker(trackerId);
	}

	@Override
	// @Transactional
	public Tractor addTractor(Tractor tractor) {
		Assert.notNull(tractor, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTractor(tractor);

		tractor.setTractorId(null);
		tractor.setAvailability(SystemType.STATUS_AVAILABLE);
		tractor.setMovingStatus(SystemType.STATUS_MOVING_FREE);
		Asset asset = assetDAO.createAsset(tractor.getAsset());
		tractor.setAsset(asset);
		return assetDAO.createTractor(tractor);
	}

	@Override
	// @Transactional
	public Tractor findTractorById(String tractorId) {
		Assert.notNull(tractorId, "Tractor ID: " + SystemType.MUST_NOT_BE_NULL);
		return assetDAO.findTractorById(tractorId);
	}

	@Override
	// @Transactional
	public List<Tractor> findAllTractors() {
		return assetDAO.findAllTractors();
	}

	@Override
	// @Transactional
	public List<Tractor> filterTractor(String movingStatus, String rwbId, String avail, String vin,
			String engineNumber, String numberPlate) {
		Tractor tractor = new Tractor();
		tractor.setMovingStatus(movingStatus);
		tractor.setAvailability(avail);
		tractor.setEngineNumber(engineNumber);
		tractor.setNumberPlate(numberPlate);
		Asset asset = new Asset();
		asset.setVin(vin);
		tractor.setAsset(asset);
		ExampleMatcher options = ExampleMatcher.matchingAll()
				.withMatcher("engineNumber", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("asset.vin", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("numberPlate", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());
		List<Tractor> tractors = assetDAO.filterTractorByExample(Example.of(tractor, options));
		if (rwbId != null) {
			RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
			if (rwb.getTractor() != null)
				tractors.add(rwb.getTractor());
		}
		return tractors;
	}

	@Override
	// @Transactional
	public Tractor updateTractor(Tractor tractorNew) {
		Assert.notNull(tractorNew, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTractor(tractorNew);

		Tractor tractorOld = assetDAO.findTractorById(tractorNew.getTractorId());
		tractorOld.setAdminCompany(tractorNew.getAdminCompany());
		tractorOld.setCurrentOdometer(tractorNew.getCurrentOdometer());
		tractorOld.setEngineNumber(tractorNew.getEngineNumber());
		tractorOld.setHp(tractorNew.getHp());
		tractorOld.setEngineType(tractorNew.getEngineType());
		tractorOld.setFuelCapacity(tractorNew.getFuelCapacity());
		tractorOld.setNumberOfFuelTanks(tractorNew.getNumberOfFuelTanks());
		tractorOld.setNumberOfGears(tractorNew.getNumberOfGears());
		tractorOld.setNumberPlate(tractorNew.getNumberPlate());
		tractorOld.setOdometerUnit(tractorNew.getOdometerUnit());
		tractorOld.setStartingOdometer(tractorNew.getStartingOdometer());
		tractorOld.setTransmission(tractorNew.getTransmission());
		tractorOld.setAsset(assetDAO.updateAsset(tractorNew.getAsset()));
		return assetDAO.updateTractor(tractorOld);
	}

	@Override
	// @Transactional
	public void deleteTractor(String tractorId) {
		Assert.notNull(tractorId, "Tractor ID: " + SystemType.MUST_NOT_BE_NULL);
		assetDAO.deleteTractor(tractorId);
	}

	@Override
	// @Transactional
	public Trailer addTrailer(Trailer trailer) {
		Assert.notNull(trailer, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTrailer(trailer);

		trailer.setTrailerId(null);
		trailer.setAvailability(SystemType.STATUS_AVAILABLE);
		trailer.setMovingStatus(SystemType.STATUS_MOVING_FREE);
		Asset asset;
		asset = assetDAO.createAsset(trailer.getAsset());
		trailer.setAsset(asset);
		return assetDAO.createTrailer(trailer);
	}

	@Override
	// @Transactional
	public Trailer findTrailerById(String trailerId) {
		Assert.notNull(trailerId, "Trailer ID: " + SystemType.MUST_NOT_BE_NULL);
		return assetDAO.findTrailerById(trailerId);
	}

	@Override
	// @Transactional
	public List<Trailer> findAllTrailers() {
		return assetDAO.findAllTrailers();
	}

	@Override
	// @Transactional
	public List<Trailer> filterTrailer(String movingStatus, String rwbId, String avail, String vin) {
		Trailer trailer = new Trailer();
		trailer.setMovingStatus(movingStatus);
		trailer.setAvailability(avail);
		Asset asset = new Asset();
		asset.setVin(vin);
		trailer.setAsset(asset);
		ExampleMatcher options = ExampleMatcher.matchingAll().withMatcher("asset.vin",
				ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());
		List<Trailer> trailers = assetDAO.filterTrailerByExample(Example.of(trailer, options));
		if (rwbId != null) {
			RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
			if (rwb.getTrailer() != null)
				trailers.add(rwb.getTrailer());
		}
		return trailers;
	}

	@Override
	// @Transactional
	public Trailer updateTrailer(Trailer trailerNew) {
		Assert.notNull(trailerNew, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTrailer(trailerNew);

		Trailer trailerOld = findTrailerById(trailerNew.getTrailerId());
		trailerOld.setAdminCompany(trailerNew.getAdminCompany());
		trailerOld.setTrailerSize(trailerNew.getTrailerSize());
		trailerOld.setTrailerType(trailerNew.getTrailerType());
		trailerOld.setAsset(assetDAO.updateAsset(trailerNew.getAsset()));
		return assetDAO.updateTrailer(trailerOld);
	}

	@Override
	// @Transactional
	public void deleteTrailer(String trailerId) {
		Assert.notNull(trailerId, "Trailer ID: " + SystemType.MUST_NOT_BE_NULL);
		assetDAO.deleteTrailer(trailerId);
	}

	@Override
	// @Transactional
	public Tyre addTyre(Tyre tyre, Asset asset) {
		Assert.notNull(tyre, "Tyre: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTyre(tyre);

		tyre.setAvailability(SystemType.STATUS_AVAILABLE);
		tyre.setMovingStatus(SystemType.STATUS_MOVING_FREE);
		tyre.setTyreId(null);
		if (asset != null)
			tyre.setAsset(asset);
		return assetDAO.createTyre(tyre);
	}

	@Override
	// @Transactional
	public Tyre findTyreById(String tyreId) {
		Assert.notNull(tyreId, "Tyre ID: " + SystemType.MUST_NOT_BE_NULL);
		return assetDAO.findTyreById(tyreId);
	}

	@Override
	// @Transactional
	public List<Tyre> findAllTyres() {
		return assetDAO.findAllTyres();
	}

	@Override
	// @Transactional
	public List<Tyre> filterTyre(String movingStatus, String avail) {
		Tyre tyre = new Tyre();
		tyre.setMovingStatus(movingStatus);
		tyre.setAvailability(avail);
		return assetDAO.filterTyreByExample(Example.of(tyre));
	}

	@Override
	// @Transactional
	public Tyre updateTyre(Tyre tyreNew) {
		Assert.notNull(tyreNew, "Tyre: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateTyre(tyreNew);

		Tyre tyreOld = assetDAO.findTyreById(tyreNew.getTyreId());
		tyreOld.setAdminCompany(tyreNew.getAdminCompany());
		tyreOld.setLeaseType(tyreNew.getLeaseType());
		tyreOld.setMake(tyreNew.getMake());
		tyreOld.setStartingKm(tyreNew.getStartingKm());
		tyreOld.setStatus(tyreNew.getStatus());
		tyreOld.setSupplier(tyreNew.getSupplier());
		tyreOld.setTotalKm(tyreNew.getTotalKm());
		tyreOld.setTyreSerialNumber(tyreNew.getTyreSerialNumber());
		return assetDAO.updateTyre(tyreOld);
	}

	@Override
	// @Transactional
	public void deleteTyre(String tyreId) {
		Assert.notNull(tyreId, "Tyre ID: " + SystemType.MUST_NOT_BE_NULL);
		assetDAO.deleteTyre(tyreId);
	}

	@Override
	// @Transactional
	public AssetCombination addAssetCombination(AssetCombination assetCombination) {
		Assert.notNull(assetCombination, "Asset Combination: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateAssetCombination(assetCombination);

		Tractor tractor = assetCombination.getTractor();
		Trailer trailer = assetCombination.getTrailer();
		Tracker tracker = assetCombination.getTracker();
		Container container = assetCombination.getContainer();
		Employee driver1 = assetCombination.getDriver1();
		Employee driver2 = assetCombination.getDriver2();

		if (tractor.getAvailability().equals(SystemType.STATUS_IN_USE))
			throw new EntityExistsException("Tractor in some Asset Combination: " + SystemType.ALREADY_EXISTS);
		if (trailer.getAvailability().equals(SystemType.STATUS_IN_USE))
			throw new EntityExistsException("Trailer in some Asset Combination: " + SystemType.ALREADY_EXISTS);

		updateStatusTractor(tractor.getTractorId(), SystemType.STATUS_IN_USE, null);
		updateStatusTrailer(trailer.getTrailerId(), SystemType.STATUS_IN_USE, null);
		if (tracker != null) {
			if (tracker.getAvailability().equals(SystemType.STATUS_IN_USE))
				throw new EntityExistsException("Tracker in some Asset Combination: " + SystemType.ALREADY_EXISTS);
			updateStatusTracker(tracker.getTrackerId(), SystemType.STATUS_IN_USE, null);
		}
		if (container != null) {
			if (container.getAvailability().equals(SystemType.STATUS_IN_USE))
				throw new EntityExistsException("Container in some Asset Combination: " + SystemType.ALREADY_EXISTS);
			updateStatusContainer(container.getContainerId(), SystemType.STATUS_IN_USE, null);
		}
		if (driver1 != null) {
			if (driver1.getAvailability().equals(SystemType.STATUS_IN_USE))
				throw new EntityExistsException("Driver-1 in some Asset Combination: " + SystemType.ALREADY_EXISTS);
			employeeService.updateStatusEmployee(driver1.getEmployeeId(), SystemType.STATUS_IN_USE, null);
		}
		if (driver2 != null) {
			if (driver2.getAvailability().equals(SystemType.STATUS_IN_USE))
				throw new EntityExistsException("Driver-2 in some Asset Combination: " + SystemType.ALREADY_EXISTS);
			employeeService.updateStatusEmployee(driver2.getEmployeeId(), SystemType.STATUS_IN_USE, null);
		}

		assetCombination.setAssetCombinationId(null);
		assetCombination.setAvailability(SystemType.STATUS_AVAILABLE);
		assetCombination.setJobAvailability(SystemType.STATUS_MOVING_FREE);
		return assetDAO.addAssetCombination(assetCombination);
	}

	@Override
	public AssetCombination findAssetCombinationById(String assetCombinationId) {
		Assert.notNull(assetCombinationId, "Asset Combination ID: " + SystemType.MUST_NOT_BE_NULL);
		return assetDAO.findAssetCombinationById(assetCombinationId);
	}

	@Override
	// @Transactional
	public List<AssetCombination> findAllAssetCombination() {
		return assetDAO.findAllAssetCombination();
	}

	@Override
	// @Transactional
	public List<AssetCombination> filterAssetCombination(String availability, String jobAvailability) {
		String avail = null;
		String jAvail = null;
		if (availability != null) {
			avail = availability;
		}
		if (jobAvailability != null) {
			jAvail = jobAvailability;
		}
		AssetCombination assetCombinationExample = new AssetCombination();
		assetCombinationExample.setAvailability(avail);
		assetCombinationExample.setJobAvailability(jAvail);
		return assetDAO.filterAssetCombinationByExample(Example.of(assetCombinationExample));
	}

	@Override
	// @Transactional
	public AssetCombination updateAssetCombination(AssetCombination assetCombinationNew) {
		Assert.notNull(assetCombinationNew, "Asset Combination: " + SystemType.MUST_NOT_BE_NULL);
		assetValidator.validateAssetCombination(assetCombinationNew);

		AssetCombination assetCombinationOld = assetDAO
				.findAssetCombinationById(assetCombinationNew.getAssetCombinationId());

		Tractor tractorNew = assetCombinationNew.getTractor();
		Trailer trailerNew = assetCombinationNew.getTrailer();
		Tracker trackerNew = assetCombinationNew.getTracker();
		Container containerNew = assetCombinationNew.getContainer();
		Employee driver1New = assetCombinationNew.getDriver1();
		Employee driver2New = assetCombinationNew.getDriver2();

		Tractor tractorOld = assetCombinationOld.getTractor();
		Trailer trailerOld = assetCombinationOld.getTrailer();
		Tracker trackerOld = assetCombinationOld.getTracker();
		Container containerOld = assetCombinationOld.getContainer();
		Employee driver1Old = assetCombinationOld.getDriver1();
		Employee driver2Old = assetCombinationOld.getDriver2();

		if (!tractorOld.getTractorId().equals(tractorNew.getTractorId())) {
			if (tractorNew.getAvailability().equals(SystemType.STATUS_IN_USE))
				throw new EntityExistsException("Tractor in some Asset Combination: " + SystemType.ALREADY_EXISTS);
			updateStatusTractor(tractorNew.getTractorId(), SystemType.STATUS_IN_USE, null);
			updateStatusTractor(tractorOld.getTractorId(), SystemType.STATUS_AVAILABLE, null);
			assetCombinationOld.setTractor(tractorNew);
		}

		if (!trailerOld.getTrailerId().equals(trailerNew.getTrailerId())) {
			if (trailerNew.getAvailability().equals(SystemType.STATUS_IN_USE))
				throw new EntityExistsException("Trailer in some Asset Combination: " + SystemType.ALREADY_EXISTS);
			updateStatusTrailer(trailerNew.getTrailerId(), SystemType.STATUS_IN_USE, null);
			updateStatusTrailer(trailerOld.getTrailerId(), SystemType.STATUS_AVAILABLE, null);
			assetCombinationOld.setTrailer(trailerNew);
		}

		if (trackerNew != null) {
			if (trackerOld != null) {
				if (!trackerOld.getTrackerId().equals(trackerNew.getTrackerId())) {
					if (trackerNew.getAvailability().equals(SystemType.STATUS_IN_USE))
						throw new EntityExistsException(
								"Tracker in some Asset Combination: " + SystemType.ALREADY_EXISTS);
					updateStatusTracker(trackerNew.getTrackerId(), SystemType.STATUS_IN_USE, null);
					updateStatusTracker(trackerOld.getTrackerId(), SystemType.STATUS_AVAILABLE, null);
				}

			} else {
				if (trackerNew.getAvailability().equals(SystemType.STATUS_IN_USE))
					throw new EntityExistsException("Tracker in some Asset Combination: " + SystemType.ALREADY_EXISTS);
				updateStatusTracker(trackerNew.getTrackerId(), SystemType.STATUS_IN_USE, null);
				assetCombinationOld.setTracker(trackerNew);
			}
		} else {
			if (trackerOld != null)
				updateStatusTracker(trackerOld.getTrackerId(), SystemType.STATUS_AVAILABLE, null);
			assetCombinationOld.setTracker(null);
		}

		if (containerNew != null) {
			if (containerOld != null) {
				if (!containerOld.getContainerId().equals(containerNew.getContainerId())) {
					if (containerNew.getAvailability().equals(SystemType.STATUS_IN_USE))
						throw new EntityExistsException(
								"Container in some Asset Combination: " + SystemType.ALREADY_EXISTS);
					updateStatusContainer(containerNew.getContainerId(), SystemType.STATUS_IN_USE, null);
					updateStatusContainer(containerOld.getContainerId(), SystemType.STATUS_AVAILABLE, null);
					assetCombinationOld.setContainer(containerNew);
				}
			} else {
				if (containerNew.getAvailability().equals(SystemType.STATUS_IN_USE))
					throw new EntityExistsException(
							"Container in some Asset Combination: " + SystemType.ALREADY_EXISTS);
				updateStatusContainer(containerNew.getContainerId(), SystemType.STATUS_IN_USE, null);
				assetCombinationOld.setContainer(containerNew);
			}
		} else {
			if (containerOld != null)
				updateStatusContainer(containerOld.getContainerId(), SystemType.STATUS_AVAILABLE, null);
			assetCombinationOld.setContainer(null);
		}

		if (driver1New != null) {
			if (driver1Old != null) {
				if (!driver1Old.getEmployeeId().equals(driver1New.getEmployeeId())) {
					if (driver1New.getAvailability().equals(SystemType.STATUS_IN_USE))
						throw new EntityExistsException(
								"Driver-1 in some Asset Combination: " + SystemType.ALREADY_EXISTS);
					employeeService.updateStatusEmployee(driver1New.getEmployeeId(), SystemType.STATUS_IN_USE, null);
					employeeService.updateStatusEmployee(driver1Old.getEmployeeId(), SystemType.STATUS_AVAILABLE, null);
					assetCombinationOld.setDriver1(driver1New);
				}
			} else {
				if (driver1New.getAvailability().equals(SystemType.STATUS_IN_USE))
					throw new EntityExistsException("Driver-1 in some Asset Combination: " + SystemType.ALREADY_EXISTS);
				employeeService.updateStatusEmployee(driver1New.getEmployeeId(), SystemType.STATUS_IN_USE, null);
				assetCombinationOld.setDriver1(driver1New);
			}
		} else {
			if (driver1Old != null)
				employeeService.updateStatusEmployee(driver1Old.getEmployeeId(), SystemType.STATUS_AVAILABLE, null);
			assetCombinationOld.setDriver1(null);
		}

		if (driver2New != null) {
			if (driver2Old != null) {
				if (!driver2Old.getEmployeeId().equals(driver2New.getEmployeeId())) {
					if (driver2New.getAvailability().equals(SystemType.STATUS_IN_USE))
						throw new EntityExistsException(
								"Driver-2 in some Asset Combination: " + SystemType.ALREADY_EXISTS);
					employeeService.updateStatusEmployee(driver2New.getEmployeeId(), SystemType.STATUS_IN_USE, null);
					employeeService.updateStatusEmployee(driver2Old.getEmployeeId(), SystemType.STATUS_AVAILABLE, null);
					assetCombinationOld.setDriver2(driver2New);
				}
			} else {
				if (driver2New.getAvailability().equals(SystemType.STATUS_IN_USE))
					throw new EntityExistsException("Driver-2 in some Asset Combination: " + SystemType.ALREADY_EXISTS);
				employeeService.updateStatusEmployee(driver2New.getEmployeeId(), SystemType.STATUS_IN_USE, null);
				assetCombinationOld.setDriver2(driver2New);
			}
		} else {
			if (driver2Old != null)
				employeeService.updateStatusEmployee(driver2Old.getEmployeeId(), SystemType.STATUS_AVAILABLE, null);
			assetCombinationOld.setDriver2(null);
		}
		assetCombinationOld.setNote(assetCombinationNew.getNote());
		assetCombinationOld.setStatus(assetCombinationNew.getStatus());

		return assetDAO.updateAssetCombination(assetCombinationOld);
	}

	@Override
	// @Transactional
	public void deleteAssetCombinationById(String assetCombinationId) {
		Assert.notNull(assetCombinationId, "Asset Combination ID: " + SystemType.MUST_NOT_BE_NULL);
		AssetCombination assetCombination = assetDAO.findAssetCombinationById(assetCombinationId);

		Tractor tractor = assetCombination.getTractor();
		Trailer trailer = assetCombination.getTrailer();
		Tracker tracker = assetCombination.getTracker();
		Container container = assetCombination.getContainer();
		Employee driver1 = assetCombination.getDriver1();
		Employee driver2 = assetCombination.getDriver2();

		updateStatusTractor(tractor.getTractorId(), SystemType.STATUS_AVAILABLE, null);
		updateStatusTrailer(trailer.getTrailerId(), SystemType.STATUS_AVAILABLE, null);
		if (tracker != null)
			updateStatusTracker(tracker.getTrackerId(), SystemType.STATUS_AVAILABLE, null);
		if (container != null)
			updateStatusContainer(container.getContainerId(), SystemType.STATUS_AVAILABLE, null);
		if (driver1 != null)
			employeeService.updateStatusEmployee(driver1.getEmployeeId(), SystemType.STATUS_AVAILABLE, null);
		if (driver2 != null)
			employeeService.updateStatusEmployee(driver2.getEmployeeId(), SystemType.STATUS_AVAILABLE, null);
		assetDAO.deleteAssetCombination(assetCombinationId);
	}

	@Override
	// @Transactional
	public AssetCombination updateStatusAssetCombination(String acId, String availabilityNew,
			String jobAvailabilityNew) {
		AssetCombination ac = assetDAO.findAssetCombinationById(acId);
		if (availabilityNew != null)
			ac.setAvailability(availabilityNew);
		if (jobAvailabilityNew != null)
			ac.setJobAvailability(jobAvailabilityNew);
		return assetDAO.updateAssetCombination(ac);
	}

	@Override
	// @Transactional
	public Tractor updateStatusTractor(String tractorId, String availabilityNew, String movingNew) {
		Tractor tractor = assetDAO.findTractorById(tractorId);
		if (availabilityNew != null)
			tractor.setAvailability(availabilityNew);
		if (movingNew != null)
			tractor.setMovingStatus(movingNew);
		return assetDAO.updateTractor(tractor);
	}

	@Override
	// @Transactional
	public Trailer updateStatusTrailer(String trailerId, String availabilityNew, String movingNew) {
		Trailer trailer = assetDAO.findTrailerById(trailerId);
		if (availabilityNew != null)
			trailer.setAvailability(availabilityNew);
		if (movingNew != null)
			trailer.setMovingStatus(movingNew);
		return assetDAO.updateTrailer(trailer);
	}

	@Override
	// @Transactional
	public Container updateStatusContainer(String containerId, String availabilityNew, String movingNew) {
		Container container = assetDAO.findContainerById(containerId);
		if (availabilityNew != null)
			container.setAvailability(availabilityNew);
		if (movingNew != null)
			container.setMovingStatus(movingNew);
		return assetDAO.updateContainer(container);
	}

	@Override
	// @Transactional
	public Tracker updateStatusTracker(String trackerId, String availabilityNew, String movingNew) {
		Tracker tracker = assetDAO.findTrackerById(trackerId);
		if (availabilityNew != null)
			tracker.setAvailability(availabilityNew);
		if (movingNew != null)
			tracker.setMovingStatus(movingNew);
		return assetDAO.updateTracker(tracker);
	}

	@Override
	// @Transactional
	public Tractor updateTotalKmTractor(String tractorId, Double kmToAdd) {
		Tractor tractor = findTractorById(tractorId);
		if (tractor.getCurrentOdometer() != null)
			tractor.setCurrentOdometer(tractor.getCurrentOdometer() + kmToAdd);
		else
			tractor.setCurrentOdometer(kmToAdd);
		return assetDAO.updateTractor(tractor);
	}

	@Override
	// @Transactional
	public Tyre updateTotalKmTyre(String tyreId, Double kmToAdd) {
		Tyre tyre = findTyreById(tyreId);
		if (tyre.getTotalKm() == null)
			tyre.setTotalKm(kmToAdd);
		else
			tyre.setTotalKm(tyre.getTotalKm() + kmToAdd);
		return assetDAO.updateTyre(tyre);
	}

	@Override
	public Tractor findTractorByNumber(String tractorNumber) {
		Tractor tractor = assetDAO.findTractorByNumberPlate(tractorNumber);
		if (tractor != null)
			return tractor;
		else
			throw new NoSuchElementException("Tractor not found against tractor # " + tractorNumber);
	}

	@Override
	public Asset findAssetByVin(String vin) {
		Asset asset = new Asset();
		asset.setVin(vin);
		List<Asset> assets = assetDAO.filterAssetByExample(Example.of(asset));
		if (assets.size() > 0)
			return assets.get(0);
		else
			throw new NoSuchElementException("Asset not found against Vin # " + vin);
	}

	@Override
	public List<Tractor> addAllTractors(List<Tractor> tractors) {
		Assert.notNull(tractors, "Tractors: " + SystemType.MUST_NOT_BE_NULL);
		if (tractors.size() > 0) {
			for (Tractor tractor : tractors) {
				tractor = addTractor(tractor);
			}
		} else
			throw new IllegalArgumentException("Tractors: " + SystemType.MUST_NOT_BE_NULL);
		return tractors;
	}

	@Override
	public List<Trailer> addAllTrailers(List<Trailer> trailers) {
		Assert.notNull(trailers, "Trailers: " + SystemType.MUST_NOT_BE_NULL);
		Trailer trailerAdded = null;
		if (trailers.size() > 0) {
			for (Trailer trailer : trailers) {
				trailerAdded = addTrailer(trailer);
				if (trailerAdded != null)
					trailer = trailerAdded;
			}
		} else
			throw new IllegalArgumentException("Trailers: " + SystemType.MUST_NOT_BE_NULL);
		return trailers;
	}

	@Override
	public List<Tyre> addAllTyre(List<Tyre> tyres) {
		Assert.notNull(tyres, "Tyres: " + SystemType.MUST_NOT_BE_NULL);
		if (tyres.size() > 0) {
			for (Tyre tyre : tyres) {
				tyre = addTyre(tyre, tyre.getAsset());
			}
		} else
			throw new IllegalArgumentException("Tyres: " + SystemType.MUST_NOT_BE_NULL);
		return tyres;
	}

	@Override
	public long countTractorByStatus(String status) {
		return assetDAO.countTractorByStatus(status);
	}

	@Override
	public long countTrailerByStatus(String status) {
		return assetDAO.countTrailerByStatus(status);
	}

	@Override
	public long countContainerByStatus(String status) {
		return assetDAO.countContainerByStatus(status);
	}

	@Override
	public long countTrackerByStatus(String status) {
		return assetDAO.countTrackerByStatus(status);
	}

}
