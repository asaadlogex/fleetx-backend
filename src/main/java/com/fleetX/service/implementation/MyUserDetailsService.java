package com.fleetX.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.entity.User;
import com.fleetX.repository.DPrivilegeRepository;
import com.fleetX.service.IAdminService;

@Service
@Transactional
public class MyUserDetailsService implements UserDetailsService {
	@Autowired
	IAdminService adminService;
	@Autowired
	DPrivilegeRepository privilegeRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = adminService.getUserByUsername(username);
		if (!user.getStatus().equals(SystemType.STATUS_ACTIVE))
			throw new BadCredentialsException(username + ": " + SystemType.NOT_ACTIVE);
		List<String> roles = adminService.getRoleNamesByUsername(user.getUsername());
		List<String> permissions = new ArrayList<>();
		for (String role : roles) {
			List<String> privileges = adminService.getPrivilegesByRoleName(role);
			for (String privilege : privileges) {
				if (privilegeRepository.findStatusByPrivilegeName(privilege).equals(SystemType.STATUS_ACTIVE))
					if (!permissions.contains(privilege))
						permissions.add(privilege);
			}
		}
		for (String addPrivilege : adminService.getAdditionalPrivilegesByUsername(user.getUsername())) {
			if (privilegeRepository.findStatusByPrivilegeName(addPrivilege).equals(SystemType.STATUS_ACTIVE))
				if (!permissions.contains(addPrivilege))
					permissions.add(addPrivilege);
		}
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		for (String permission : permissions) {
			grantedAuthorities.add(new SimpleGrantedAuthority(permission));
		}
		return new org.springframework.security.core.userdetails.User(username, user.getPassword(), grantedAuthorities);
	}

}
