package com.fleetX.service.implementation;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.AddressDTO;
import com.fleetX.dto.AssetDTO;
import com.fleetX.dto.ContactDTO;
import com.fleetX.dto.EmployeeContractDTO;
import com.fleetX.dto.EmployeeDTO;
import com.fleetX.dto.RouteDTO;
import com.fleetX.dto.TractorDTO;
import com.fleetX.dto.TrailerDTO;
import com.fleetX.dto.TripDetailDTO;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.FixedLineItem;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.LineItem;
import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.entity.dropdown.DEmployeeType;
import com.fleetX.entity.dropdown.DLeaseType;
import com.fleetX.entity.dropdown.DMake;
import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.dropdown.DTrailerSize;
import com.fleetX.entity.dropdown.DTrailerType;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.maintenance.WorkOrderItemDetail;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.mapper.GeneralMapper;
import com.fleetX.repository.DChannelRepository;
import com.fleetX.repository.DCityRepository;
import com.fleetX.repository.DDepartmentRepository;
import com.fleetX.repository.DEmployeeTypeRepository;
import com.fleetX.repository.DLeaseTypeRepository;
import com.fleetX.repository.DMakeRepository;
import com.fleetX.repository.DPositionRepository;
import com.fleetX.repository.DStateRepository;
import com.fleetX.repository.DTrailerSizeRepository;
import com.fleetX.repository.DTrailerTypeRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IFileDataService;
import com.fleetX.service.ISummaryService;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service
@Transactional
public class FileDataService implements IFileDataService {

	@Autowired
	SystemUtil systemUtil;
	@Autowired
	DCityRepository cityRepository;
	@Autowired
	DLeaseTypeRepository leaseTypeRepository;
	@Autowired
	DTrailerTypeRepository trailerTypeRepository;
	@Autowired
	DTrailerSizeRepository trailerSizeRepository;
	@Autowired
	DEmployeeTypeRepository employeeTypeRepository;
	@Autowired
	DDepartmentRepository departmentRepository;
	@Autowired
	DChannelRepository channelRepository;
	@Autowired
	DStateRepository stateRepository;
	@Autowired
	DPositionRepository positionRepository;
	@Autowired
	DMakeRepository makeRepository;
	@Autowired
	ISummaryService summaryService;
	@Autowired
	IAdminService adminService;
	@Autowired
	IAssetService assetService;
	@Autowired
	IContractService contractService;
	@Autowired
	GeneralMapper generalMapper;

	@Override
	public byte[] exportTripDetailsToExcel(List<TripDetailDTO> tripDetails, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SystemType.EXCEL_TRIP_DETAIL_FILE_NAME);
		Row header = sheet.createRow(0);
		List<String> columns = new ArrayList<>();
		columns.addAll(SystemType.TRIP_SHEET_COLUMNS);
		columns.addAll(summaryService.getAllExpenseType());
		for (int i = 0; i < columns.size(); i++) {
			header.createCell(i, CellType.STRING).setCellValue(columns.get(i));
		}

		for (TripDetailDTO tripDetail : tripDetails) {

			for (int i = 1; i <= tripDetail.getRoadwayBills().size(); i++) {
				Row row = sheet.createRow(i);
				for (int j = sheet.getRow(0).getFirstCellNum(); j < sheet.getRow(0).getLastCellNum(); j++) {
					Cell cell = row.createCell(j);
					switch (sheet.getRow(0).getCell(j).getStringCellValue()) {
					case "RWB ID":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getRwbId());
						break;
					case "JOB ID":
						cell.setCellValue(tripDetail.getJobId());
						break;
					case "RWB DATE":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getRwbDate());
						break;
					case "ROUTE":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getRoute().toString());
						break;
					case "VEHICLE TYPE":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getTrailerTypeAndSize());
						break;
					case "VEHICLE #":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getTractorNumber());
						break;
					case "LEASE TYPE":
						if (tripDetail.getRoadwayBills().get(i - 1).getBroker() != null)
							cell.setCellValue(SystemType.OWNERSHIP_RENTAL);
						else
							cell.setCellValue(SystemType.OWNERSHIP_OWN);
						break;
					case "CLIENT":
						if (tripDetail.getRoadwayBills().get(i - 1).getBroker() != null)
							cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getBroker());
						else
							cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getCustomer());
						break;
					case "JOB DURATION":
						if (tripDetail.getDurationInHrs() != null)
							cell.setCellValue(tripDetail.getDurationInHrs());
						else
							cell.setCellValue("");
						break;
					case "RWB DURATION":
						if (tripDetail.getRoadwayBills().get(i - 1).getDurationInHrs() != null)
							cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getDurationInHrs());
						else
							cell.setCellValue("");
						break;
					case "OD KM":
						if (tripDetail.getOdometerKm() != null)
							cell.setCellValue(tripDetail.getOdometerKm());
						else
							cell.setCellValue("");
						break;
					case "TR KM":
						if (tripDetail.getTrackerKm() != null)
							cell.setCellValue(tripDetail.getTrackerKm());
						else
							cell.setCellValue("");
						break;
					case "RWB TR KM":
						if (tripDetail.getRoadwayBills().get(i - 1).getTrackerKm() != null)
							cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getTrackerKm());
						else
							cell.setCellValue("");
						break;
					case "JOB REVENUE":
						cell.setCellValue(tripDetail.getTotalIncome());
						break;
					case "JOB COST":
						cell.setCellValue(tripDetail.getTotalExpense());
						break;
					case "RWB REVENUE":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getTotalIncome());
						break;
					case "RWB COST":
						cell.setCellValue(tripDetail.getRoadwayBills().get(i - 1).getTotalExpense());
						break;
					case "NET REVENUE":
						cell.setCellValue(tripDetail.getNetIncome());
						break;
					case "STATUS":
						cell.setCellValue(tripDetail.getJobStatus().getValue());
						break;
					case "TOTAL FUEL COST":
						cell.setCellValue(tripDetail.getTotalFuelCost() == null ? 0d : tripDetail.getTotalFuelCost());
						break;
					case "AVG FUEL COST PER KM":
						cell.setCellValue(
								tripDetail.getAvgFuelCostPerKm() == null ? 0d : tripDetail.getAvgFuelCostPerKm());
						break;
					case "FUEL AVG":
						cell.setCellValue(tripDetail.getFuelAvg() == null ? 0d : tripDetail.getFuelAvg());
						break;
					case "GENSET FUEL":
						cell.setCellValue(0);
						break;
//					case "WEIGHT":
//						cell.setCellValue(tripDetail.getTotalKm());
//						break;
					case "CLOSE DATE":
						cell.setCellValue(tripDetail.getJobCloseDate() == null ? "" : tripDetail.getJobCloseDate());
						break;
					default:
						cell.setCellValue(
								tripDetail.getExpenses().get(sheet.getRow(0).getCell(j).getStringCellValue()));
					}
				}
			}
		}
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			workbook.write(outputStream);
			workbook.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public List<RouteDTO> mapToRouteDTO(MultipartFile file) throws IOException, InvalidFormatException {
		Assert.notNull(file, "File: " + SystemType.MUST_NOT_BE_NULL);
		String extension = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
		Assert.notNull(extension, "File extension: " + SystemType.INVALID_DATA);
		if (extension.equalsIgnoreCase(SystemType.FILE_TYPE_EXCEL_SPREADSHEET)) {
			List<RouteDTO> routeDtos = new ArrayList<>();
			Workbook workbook = new XSSFWorkbook(OPCPackage.open(file.getInputStream()));
			// Get first/desired sheet from the workbook
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() == 0)
					continue;
				RouteDTO routeDto = new RouteDTO();
				short firstCell = row.getFirstCellNum();
				short lastCell = row.getLastCellNum();
				for (short i = firstCell; i < lastCell; i++) {
					if (row.getCell(i) == null || row.getCell(i).getCellTypeEnum().equals(CellType.BLANK))
						continue;
					if (i == firstCell)
						routeDto.setRouteId(row.getCell(i).getStringCellValue());
					else if (i == firstCell + 1)
						routeDto.setAvgDistanceInKM(row.getCell(i).getNumericCellValue());
					else if (i == firstCell + 2) {
						DCity cityFrom = cityRepository.findByCode(row.getCell(i).getStringCellValue());
						if (cityFrom != null)
							routeDto.setRouteFrom(cityFrom);
						else
							throw new NoSuchElementException("City: " + SystemType.NO_SUCH_ELEMENT);
					} else if (i == firstCell + 3) {
						DCity cityTo = cityRepository.findByCode(row.getCell(i).getStringCellValue());
						if (cityTo != null)
							routeDto.setRouteTo(cityTo);
						else
							throw new NoSuchElementException("City: " + SystemType.NO_SUCH_ELEMENT);
					} else
						throw new IllegalArgumentException("File Data: " + SystemType.INVALID_DATA);
				}
				routeDtos.add(routeDto);
			}
			return routeDtos;
		} else
			throw new IllegalArgumentException("File extension: " + SystemType.INVALID_DATA);
	}

	@Override
	public List<TractorDTO> mapToTractorDTO(MultipartFile file) throws IOException, InvalidFormatException {
		Assert.notNull(file, "File: " + SystemType.MUST_NOT_BE_NULL);
		String extension = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
		Assert.notNull(extension, "File extension: " + SystemType.INVALID_DATA);
		if (extension.equalsIgnoreCase(SystemType.FILE_TYPE_EXCEL_SPREADSHEET)) {
			List<TractorDTO> tractorDtos = new ArrayList<>();
			Workbook workbook = new XSSFWorkbook(OPCPackage.open(file.getInputStream()));
			// Get first/desired sheet from the workbook
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() == 0)
					continue;
				TractorDTO tractorDto = new TractorDTO();
				AssetDTO assetDto = new AssetDTO();
				short firstCell = row.getFirstCellNum();
				short lastCell = row.getLastCellNum();
				for (short i = firstCell; i < lastCell; i++) {
					if (row.getCell(i) == null || row.getCell(i).getCellTypeEnum().equals(CellType.BLANK))
						continue;
					if (i == firstCell) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setTractorId(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 1) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							tractorDto.setEngineNumber(row.getCell(i).getStringCellValue());
						else if (row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							tractorDto.setEngineNumber(String.valueOf(row.getCell(i).getNumericCellValue()));

					} else if (i == firstCell + 2) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							assetDto.setVin(row.getCell(i).getStringCellValue());
						else if (row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							assetDto.setVin(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 3) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setHp(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 4) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setNumberPlate(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 5) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setFuelCapacity(row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 6) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setStartingOdometer(row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 7) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setCurrentOdometer(row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 8) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tractorDto.setOdometerUnit(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 9) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						Date induction = row.getCell(i).getDateCellValue();
						assetDto.setInduction(systemUtil.convertToDateString(induction));
					} else if (i == firstCell + 10) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DLeaseType leaseType = leaseTypeRepository.findByValue(row.getCell(i).getStringCellValue());
						assetDto.setLeaseType(leaseType);
					} else if (i == firstCell + 11) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setSupplierId(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 12) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DMake assetMake = makeRepository.findByValue(row.getCell(i).getStringCellValue());
						assetDto.setAssetMake(assetMake);
					} else if (i == firstCell + 13) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setModel(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 14) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setYear((int) row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 15) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setColor(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 16) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setNumberOfTyres((int) row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 17) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setGrossWeight(row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 18) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setWeightUnit(row.getCell(i).getStringCellValue());
					} else
						throw new IllegalArgumentException("File Data: " + SystemType.INVALID_DATA);
				}
				tractorDto.setAdminCompanyId(SystemType.ADMIN_COMPANY_ID);
				assetDto.setStatus(SystemType.STATUS_ACTIVE);
				tractorDto.setAsset(assetDto);
				tractorDtos.add(tractorDto);
			}
			return tractorDtos;
		} else
			throw new IllegalArgumentException("File extension: " + SystemType.INVALID_DATA);
	}

	@Override
	public List<TrailerDTO> mapToTrailerDTO(MultipartFile file) throws IOException, InvalidFormatException {
		Assert.notNull(file, "File: " + SystemType.MUST_NOT_BE_NULL);
		String extension = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
		Assert.notNull(extension, "File extension: " + SystemType.INVALID_DATA);
		if (extension.equalsIgnoreCase(SystemType.FILE_TYPE_EXCEL_SPREADSHEET)) {
			List<TrailerDTO> trailerDtos = new ArrayList<>();
			Workbook workbook = new XSSFWorkbook(OPCPackage.open(file.getInputStream()));
			// Get first/desired sheet from the workbook
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() == 0)
					continue;
				TrailerDTO trailerDto = new TrailerDTO();
				AssetDTO assetDto = new AssetDTO();
				short firstCell = row.getFirstCellNum();
				short lastCell = row.getLastCellNum();
				for (short i = firstCell; i < lastCell; i++) {
					if (row.getCell(i) == null || row.getCell(i).getCellTypeEnum().equals(CellType.BLANK))
						continue;
					if (i == firstCell) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						trailerDto.setTrailerId(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 1) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							assetDto.setVin(row.getCell(i).getStringCellValue());
						else if (row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							assetDto.setVin(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 2) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DTrailerType trailerType = trailerTypeRepository
								.findByValue(row.getCell(i).getStringCellValue());
						if (trailerType == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						trailerDto.setTrailerType(trailerType);
					} else if (i == firstCell + 3) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DTrailerSize trailerSize = trailerSizeRepository
								.findByValue(row.getCell(i).getStringCellValue());
						if (trailerSize == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						trailerDto.setTrailerSize(trailerSize);
					} else if (i == firstCell + 4) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						Date induction = row.getCell(i).getDateCellValue();
						assetDto.setInduction(systemUtil.convertToDateString(induction));
					} else if (i == firstCell + 5) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DLeaseType leaseType = leaseTypeRepository.findByValue(row.getCell(i).getStringCellValue());
						if (leaseType == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						assetDto.setLeaseType(leaseType);
					} else if (i == firstCell + 6) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setSupplierId(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 7) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setYear((int) row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 8) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setNumberOfTyres((int) row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 9) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setGrossWeight(row.getCell(i).getNumericCellValue());
					} else if (i == firstCell + 10) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						assetDto.setWeightUnit(row.getCell(i).getStringCellValue());
					} else
						throw new IllegalArgumentException("File Data: " + SystemType.INVALID_DATA);
				}
				trailerDto.setAdminCompanyId(SystemType.ADMIN_COMPANY_ID);
				assetDto.setStatus(SystemType.STATUS_ACTIVE);
				trailerDto.setAsset(assetDto);
				trailerDtos.add(trailerDto);
			}
			return trailerDtos;
		} else
			throw new IllegalArgumentException("File extension: " + SystemType.INVALID_DATA);
	}

	@Override
	public List<EmployeeDTO> mapToEmployeeDTO(MultipartFile file) throws IOException, InvalidFormatException {
		Assert.notNull(file, "File: " + SystemType.MUST_NOT_BE_NULL);
		String extension = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
		Assert.notNull(extension, "File extension: " + SystemType.INVALID_DATA);
		if (extension.equalsIgnoreCase(SystemType.FILE_TYPE_EXCEL_SPREADSHEET)) {
			List<EmployeeDTO> employeeDtos = new ArrayList<>();
			Workbook workbook = new XSSFWorkbook(OPCPackage.open(file.getInputStream()));
			// Get first/desired sheet from the workbook
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() == 0)
					continue;
				EmployeeDTO employeeDto = new EmployeeDTO();
				EmployeeContractDTO contractDto = new EmployeeContractDTO();
				ContactDTO contactDto = new ContactDTO();
				AddressDTO addressDto = new AddressDTO();
				short firstCell = row.getFirstCellNum();
				short lastCell = row.getLastCellNum();
				for (short i = firstCell; i < lastCell; i++) {
					if (row.getCell(i) == null || row.getCell(i).getCellTypeEnum().equals(CellType.BLANK))
						continue;
					if (i == firstCell) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setEmployeeId(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 1) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DEmployeeType employeeType = employeeTypeRepository
								.findByValue(row.getCell(i).getStringCellValue());
						if (employeeType == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						employeeDto.setEmployeeType(employeeType);
					} else if (i == firstCell + 2) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setSupplierId(String.valueOf((int) row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 3) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setFirstName(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 4) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setLastName(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 5) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							employeeDto.setEmployeeNumber(row.getCell(i).getStringCellValue());
						else if (row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							employeeDto.setEmployeeNumber(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 6) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						Date dob = row.getCell(i).getDateCellValue();
						employeeDto.setDateOfBirth(systemUtil.convertToDateString(dob));
					} else if (i == firstCell + 7) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setCnic(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 8) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						Date cnicExp = row.getCell(i).getDateCellValue();
						employeeDto.setCnicExpiry(systemUtil.convertToDateString(cnicExp));
					} else if (i == firstCell + 9) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							employeeDto.setLicenceNumber(row.getCell(i).getStringCellValue());
						else if (row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							employeeDto.setLicenceNumber(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 10) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setLicenceClass(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 11) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						Date licExp = row.getCell(i).getDateCellValue();
						employeeDto.setLicenceExpiry(systemUtil.convertToDateString(licExp));
					} else if (i == firstCell + 12) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						Date start = row.getCell(i).getDateCellValue();
						contractDto.setStartDate(systemUtil.convertToDateString(start));
					} else if (i == firstCell + 13) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						addressDto.setStreet1(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 14) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						addressDto.setStreet2(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 15) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DCity city = cityRepository.findByValue(row.getCell(i).getStringCellValue());
						if (city == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						addressDto.setCity(city);
					} else if (i == firstCell + 16) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DState state = stateRepository.findByValue(row.getCell(i).getStringCellValue());
						if (state == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						addressDto.setState(state);
					} else if (i == firstCell + 17) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						addressDto.setCountry(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 18) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						DPosition position = positionRepository.findByValue(row.getCell(i).getStringCellValue());
						if (position == null)
							throw new NoSuchElementException(
									row.getCell(i).getStringCellValue() + ": " + SystemType.NO_SUCH_ELEMENT);
						employeeDto.setPosition(position);
					} else if (i == firstCell + 19) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							contactDto.setData(row.getCell(i).getStringCellValue());
						else if (row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							contactDto.setData(String.valueOf(row.getCell(i).getNumericCellValue()));
						contactDto.setChannel(channelRepository.findByValue(SystemType.CHANNEL_MOBILE));
					} else if (i == firstCell + 20) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setNextOfKin(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 21) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						employeeDto.setNextOfKinRelation(row.getCell(i).getStringCellValue());
					} else if (i == firstCell + 22) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						contractDto.setSalary(row.getCell(i).getNumericCellValue());
					} else
						throw new IllegalArgumentException("File Data: " + SystemType.INVALID_DATA);
				}
				AdminCompany adminCompany = adminService.findAdminCompanyById(SystemType.ADMIN_COMPANY_ID);
				employeeDto.setCompanyId(SystemType.ADMIN_COMPANY_ID);
				employeeDto.setStatus(SystemType.STATUS_ACTIVE);
				employeeDto.setBusinessAddress(
						generalMapper.mapToAddressDTO(adminCompany.getAdminCompanyAddresses().get(0)));
				employeeDto.setEmployeeAddresses(Arrays.asList(addressDto));
				employeeDto.setEmployeeContacts(Arrays.asList(contactDto));
				employeeDto.setEmployeeContracts(Arrays.asList(contractDto));
				employeeDto.setDepartment(departmentRepository.findByValue(SystemType.DEPARTMENT_OPERATIONS));
				employeeDtos.add(employeeDto);
			}
			return employeeDtos;
		} else
			throw new IllegalArgumentException("File extension: " + SystemType.INVALID_DATA);
	}

	@Override
	public List<Tyre> mapToTyre(MultipartFile file, Boolean forTractor) throws IOException, InvalidFormatException {
		Assert.notNull(file, "File: " + SystemType.MUST_NOT_BE_NULL);
		String extension = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
		Assert.notNull(extension, "File extension: " + SystemType.INVALID_DATA);
		if (extension.equalsIgnoreCase(SystemType.FILE_TYPE_EXCEL_SPREADSHEET)) {
			List<Tyre> tyres = new ArrayList<>();
			Workbook workbook = new XSSFWorkbook(OPCPackage.open(file.getInputStream()));
			// Get first/desired sheet from the workbook
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() == 0)
					continue;
				Tyre tyre = new Tyre();
				short firstCell = row.getFirstCellNum();
				short lastCell = row.getLastCellNum();
				for (short i = firstCell; i < lastCell; i++) {
					if (row.getCell(i) == null || row.getCell(i).getCellTypeEnum().equals(CellType.BLANK))
						continue;
					if (i == firstCell) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tyre.setTyreId(String.valueOf(row.getCell(i).getNumericCellValue()));
					} else if (i == firstCell + 1) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING)
								&& !row.getCell(i).getCellTypeEnum().equals(CellType.NUMERIC))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						if (forTractor) {
							Tractor tractor = null;
							if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
								tractor = assetService.findTractorByNumber(row.getCell(i).getStringCellValue());
							else
								tractor = assetService
										.findTractorByNumber(String.valueOf(row.getCell(i).getNumericCellValue()));
							if (tractor != null)
								tyre.setAsset(tractor.getAsset());
						} else {
							Asset asset;
							if (row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
								asset = assetService.findAssetByVin(row.getCell(i).getStringCellValue());
							else
								asset = assetService
										.findAssetByVin(String.valueOf(row.getCell(i).getNumericCellValue()));
							if (asset != null)
								tyre.setAsset(asset);
						}
					} else if (i == firstCell + 2) {
						if (!row.getCell(i).getCellTypeEnum().equals(CellType.STRING))
							throw new IllegalArgumentException(
									"Invalid data on row # " + (row.getRowNum() + 1) + " and column # " + (i + 1));
						tyre.setTyreSerialNumber(row.getCell(i).getStringCellValue());
					} else
						throw new IllegalArgumentException("File Data: " + SystemType.INVALID_DATA);
				}
				AdminCompany adminCompany = adminService.findAdminCompanyById(SystemType.ADMIN_COMPANY_ID);
				tyre.setAdminCompany(adminCompany);
				tyre.setLeaseType(leaseTypeRepository.findByValue(SystemType.LEASE_TYPE_PURCHASED));
				tyres.add(tyre);
			}
			return tyres;
		} else
			throw new IllegalArgumentException("File extension: " + SystemType.INVALID_DATA);
	}

	@Override
	public byte[] previewReimbursement(List<Job> jobs) throws DocumentException, ParseException {
		Document document = new Document(PageSize.A4, 0, 0, 50, 50);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, baos);
		document.open();
		Paragraph heading = new Paragraph();
		heading.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
		heading.add("Reimbursement Details\n");
		heading.add("\n\n");
		heading.setAlignment(Element.ALIGN_CENTER);
		document.add(heading);

		PdfPTable table = new PdfPTable(new float[] { 1, 1, 3, 3, 2, 2, 2, 2 });
		String[] columns = { "Job ID", "RWB ID", "Broker", "Customer", "Tractor #", "Lease Type", "Job Close Date",
				"Total Expense" };
		Phrase phrase;
		for (int i = 0; i < columns.length; i++) {
			phrase = new Phrase();
			phrase.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10));
			phrase.add(columns[i]);
			table.addCell(phrase);
		}

		for (Job job : jobs) {
			if (job.getJobStatus().getValue().equals(SystemType.STATUS_JOB_OPEN))
				continue;
			String jobId = null, rwbId = null, broker = null, customer = null, tractorNumber = null, leaseType = null,
					jobCloseDate = null;
			Double totalExpense = 0d;
			jobId = job.getJobId();
			jobCloseDate = systemUtil.convertToDateString(job.getEndDate());
			for (RoadwayBill rwb : job.getRoadwayBills()) {
				rwbId = rwb.getRwbId();
				leaseType = rwb.getVehicleOwnership().getValue();
				if (rwb.getCustomer() != null) {
					customer = rwb.getCustomer().getCompanyName();
				} else if (rwb.getBroker() != null) {
					broker = rwb.getBroker().getCompanyName();
				}
				if (rwb.getTractor() != null)
					tractorNumber = rwb.getTractor().getNumberPlate();
				else if (rwb.getRentalVehicle() != null)
					tractorNumber = rwb.getRentalVehicle().getVehicleNumber();
				for (Expense expense : rwb.getExpenses()) {
					if (SystemType.REIMBURSEMENT_EXPENSES.contains(expense.getExpenseType().getValue()))
						totalExpense += expense.getAmount();
				}
				String[] data = { jobId, rwbId, broker, customer, tractorNumber, leaseType, jobCloseDate,
						String.valueOf(totalExpense) };
				for (int i = 0; i < data.length; i++) {
					phrase = new Phrase();
					phrase.setFont(FontFactory.getFont(FontFactory.HELVETICA, 9));
					phrase.add(data[i]);
					table.addCell(phrase);
				}
			}

		}
		document.add(table);

		document.close();
		writer.close();
		return baos.toByteArray();
	}

	@Override
	public byte[] previewInvoice(Invoice invoice) throws DocumentException {
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, baos);
		document.open();
		document.add(getInvoiceInfo(invoice));
		document.add(new Chunk("\n"));
		document.add(getAddressTable(invoice));
		document.add(new Chunk("\n"));
		document.add(getLineItemTable(invoice));
		document.add(getLineItemAmount(invoice));
		document.add(new Chunk("\n"));
		PdfPTable taxTable = getTaxTable(invoice);
		if (taxTable != null) {
			document.add(taxTable);
		}
		document.add(getTotalAmountTable(invoice));
		document.add(new Chunk("\n"));
		document.add(getNote(invoice));

		document.close();
		writer.close();
		return baos.toByteArray();
	}

	private PdfPTable getLineItemAmount(Invoice invoice) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		table.setWidths(new int[] { 80, 20 });
		table.addCell("Amount (in PKR)");
		table.addCell(
				new Phrase(String.valueOf(invoice.getTotalAmount()), FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
		return table;
	}

	private PdfPTable getDHLineItemAmount(DHInvoice invoice) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		table.setWidths(new int[] { 80, 20 });
		table.addCell("Amount (in PKR)");
		table.addCell(
				new Phrase(String.valueOf(invoice.getTotalAmount()), FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
		return table;
	}

	private PdfPTable getTotalAmountTable(Invoice invoice) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(1);
		table.setWidths(new int[] { 80, 20 });
		table.addCell("Total Amount (in PKR)");
		table.addCell(new Phrase(
				String.valueOf(invoice.getTotalAmount() + ((invoice.getTax() / 100) * invoice.getTotalAmount())),
				FontFactory.getFont(FontFactory.HELVETICA_BOLD, 18)));
		return table;
	}

	private PdfPTable getTotalAmountTable(DHInvoice invoice) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(1);
		table.setWidths(new int[] { 80, 20 });
		table.addCell("Total Amount (in PKR)");
		table.addCell(new Phrase(
				String.valueOf(invoice.getTotalAmount() + ((invoice.getTax() / 100) * invoice.getTotalAmount())),
				FontFactory.getFont(FontFactory.HELVETICA_BOLD, 18)));
		return table;
	}

	private Chunk getNote(Invoice invoice) {
		Chunk chunk = new Chunk();
		chunk.append("NOTE: " + (invoice.getNote() != null ? invoice.getNote() : SystemType.INVOICE_DEFAULT_NOTE));
		return chunk;
	}

	private Chunk getNote(DHInvoice invoice) {
		Chunk chunk = new Chunk();
		chunk.append("NOTE: " + (invoice.getNote() != null ? invoice.getNote() : SystemType.INVOICE_DEFAULT_NOTE));
		return chunk;
	}

	private PdfPTable getTaxTable(Invoice invoice) throws DocumentException {
		if (invoice.getTaxState() == null)
			return null;
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		table.setWidths(new int[] { 80, 20 });
		table.addCell("Tax State");
		table.addCell(invoice.getTaxState().getValue());
		table.addCell("Tax Rate (in %)");
		table.addCell(String.valueOf(invoice.getTax()));
		table.addCell("Tax Amount");
		table.addCell(String.valueOf((invoice.getTax() / 100) * invoice.getTotalAmount()));
		return table;
	}

	private PdfPTable getTaxTable(DHInvoice invoice) throws DocumentException {
		if (invoice.getTaxState() == null)
			return null;
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		table.setWidths(new int[] { 80, 20 });
		table.addCell("Tax State");
		table.addCell(invoice.getTaxState().getValue());
		table.addCell("Tax Rate (in %)");
		table.addCell(String.valueOf(invoice.getTax()));
		table.addCell("Tax Amount");
		table.addCell(String.valueOf((invoice.getTax() / 100) * invoice.getTotalAmount()));
		return table;
	}

	private PdfPTable getLineItemTable(Invoice invoice) throws DocumentException {
		String invoiceType = invoice.getInvoiceType().getValue();
		switch (invoiceType) {
		case SystemType.INVOICE_TYPE_DEDICATED:
			return getDedicatedLineItemTable(invoice);
		case SystemType.INVOICE_TYPE_PER_TRIP:
			return getPerTripLineItemTable(invoice);
		case SystemType.INVOICE_TYPE_PER_TON:
			return getPerTonLineItemTable(invoice);
		case SystemType.INVOICE_TYPE_FIXED:
			return getFixedLineItemTable(invoice);
		default:
			throw new DocumentException("Invalid invoice type!");
		}
	}

	private PdfPTable getFixedLineItemTable(Invoice invoice) {
		PdfPTable table = new PdfPTable(new float[] { 1, 2, 2, 2, 2, 2 }); // Sno,rwnNum,route,customerRef,
		table.setWidthPercentage(100);
		table.getDefaultCell().setMinimumHeight(20);
		table.getDefaultCell().setBorder(3);
		Font headingFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8);
		Font dataFont = FontFactory.getFont(FontFactory.HELVETICA, 8);
		table.addCell(new Phrase("S #", headingFont));
		table.addCell(new Phrase("Vehicle No.", headingFont));
		table.addCell(new Phrase("Vehicle Type", headingFont));
		table.addCell(new Phrase("Vehicle Size", headingFont));
		table.addCell(new Phrase("Period", headingFont));
		table.addCell(new Phrase("Amount", headingFont));

		int Sno = 1;
		for (FixedLineItem lineItem : invoice.getFixedLineItems()) {
			AssetCombination ac = lineItem.getAssetCombination();
			table.addCell(new Phrase(String.valueOf(Sno++), dataFont));
			table.addCell(new Phrase(ac.getTractor() != null ? ac.getTractor().getNumberPlate() : "-", dataFont));
			table.addCell(
					new Phrase(ac.getTrailer() != null ? ac.getTrailer().getTrailerType().getValue() : "-", dataFont));
			table.addCell(
					new Phrase(ac.getTrailer() != null ? ac.getTrailer().getTrailerSize().getValue() : "-", dataFont));
			table.addCell(new Phrase(invoice.getInvoiceMonth() + "-" + invoice.getInvoiceYear(), dataFont));
			table.addCell(new Phrase(String.valueOf(invoice.getTotalAmount() / invoice.getFixedLineItems().size()),
					dataFont));
		}
		return table;
	}

	private PdfPTable getDedicatedLineItemTable(Invoice invoice) {
		PdfPTable table = new PdfPTable(new float[] { 1, 2, 2, 3, 2, 2, 2, 2 }); // Sno,rwnNum,route,customerRef,
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(3);
		table.getDefaultCell().setMinimumHeight(20);
		Font headingFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8);
		Font dataFont = FontFactory.getFont(FontFactory.HELVETICA, 8);

		table.addCell(new Phrase("S #", headingFont));
		table.addCell(new Phrase("RWB No.", headingFont));
		table.addCell(new Phrase("Route", headingFont));
		table.addCell(new Phrase("RWB Date", headingFont));
		table.addCell(new Phrase("Vehicle No.", headingFont));
		table.addCell(new Phrase("Std. KM", headingFont));
		table.addCell(new Phrase("Variable rate", headingFont));
		table.addCell(new Phrase("Amount", headingFont));

		int Sno = 1;
		for (LineItem lineItem : invoice.getLineItems()) {
			RoadwayBill rwb = lineItem.getRoadwayBill();
			table.addCell(new Phrase(String.valueOf(Sno++), dataFont));
			table.addCell(new Phrase(rwb.getRwbId(), dataFont));
			table.addCell(new Phrase(rwb.getRoute().toString(), dataFont));
			table.addCell(new Phrase(systemUtil.convertToDateString(rwb.getRwbDate()), dataFont));
			if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
				table.addCell(new Phrase(rwb.getTractor().getNumberPlate(), dataFont));
			else
				table.addCell(new Phrase(rwb.getRentalVehicle().getVehicleNumber(), dataFont));
			table.addCell(new Phrase(String.valueOf(rwb.getRoute().getAvgDistanceInKM()), dataFont));
			table.addCell(new Phrase(String.valueOf(rwb.getRatePerKm()), dataFont));
			table.addCell(
					new Phrase(String.valueOf(rwb.getRoute().getAvgDistanceInKM() * rwb.getRatePerKm()), dataFont));
		}
		return table;
	}

	private PdfPTable getPerTonLineItemTable(Invoice invoice) {
		PdfPTable table = new PdfPTable(new float[] { 1, 1, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2 }); // Sno,rwnNum,route,customerRef,
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(3);
		table.getDefaultCell().setMinimumHeight(20);
		Font headingFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8);
		Font dataFont = FontFactory.getFont(FontFactory.HELVETICA, 8);

		table.addCell(new Phrase("S #", headingFont));
		table.addCell(new Phrase("Rwb No.", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 7)));
		table.addCell(new Phrase("Route", headingFont));
		table.addCell(new Phrase("RWB Date", headingFont));
		table.addCell(new Phrase("Vehicle No.", headingFont));
		table.addCell(new Phrase("Weight (in ton)", headingFont));
		table.addCell(new Phrase("Rate (per ton)", headingFont));
		table.addCell(new Phrase("Trip Charges", headingFont));
		table.addCell(new Phrase("Dtn. Charges", headingFont));
		table.addCell(new Phrase("Ldg. Charges", headingFont));
		table.addCell(new Phrase("Un-ldg. Charges", headingFont));
		table.addCell(new Phrase("Other Charges", headingFont));
		table.addCell(new Phrase("Amount", headingFont));

		int Sno = 1;
		for (LineItem lineItem : invoice.getLineItems()) {
			RoadwayBill rwb = lineItem.getRoadwayBill();
			table.addCell(new Phrase(String.valueOf(Sno++), dataFont));
			table.addCell(new Phrase(rwb.getRwbId(), dataFont));
			table.addCell(new Phrase(rwb.getRoute().toString(), dataFont));
			table.addCell(new Phrase(systemUtil.convertToDateString(rwb.getRwbDate()), dataFont));
			if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
				table.addCell(new Phrase(rwb.getTractor().getNumberPlate(), dataFont));
			else
				table.addCell(new Phrase(rwb.getRentalVehicle().getVehicleNumber(), dataFont));
			table.addCell(new Phrase(String.valueOf(rwb.getWeightInTon()), dataFont));
			table.addCell(new Phrase(String.valueOf(rwb.getRatePerTon()), dataFont));
			List<Income> incomes = rwb.getIncomes();
			Double trip = 0d, detention = 0d, loading = 0d, offLoading = 0d, other = 0d;
			for (Income income : incomes) {
				switch (income.getIncomeType().getValue()) {
				case SystemType.INCOME_FREIGHT_RATE:
					trip = income.getAmount();
					break;
				case SystemType.INCOME_DETENTION_CHARGES:
					detention = income.getAmount();
					break;
				case SystemType.INCOME_LOADING_CHARGES:
					loading = income.getAmount();
					break;
				case SystemType.INCOME_OFFLOADING_CHARGES:
					offLoading = income.getAmount();
					break;
				case SystemType.INCOME_OTHER_CHARGES:
					other = income.getAmount();
					break;
				default:
					continue;
				}
			}
			table.addCell(new Phrase(String.valueOf(trip), dataFont));
			table.addCell(new Phrase(String.valueOf(detention), dataFont));
			table.addCell(new Phrase(String.valueOf(loading), dataFont));
			table.addCell(new Phrase(String.valueOf(offLoading), dataFont));
			table.addCell(new Phrase(String.valueOf(other), dataFont));

			Double amount = trip + detention + loading + offLoading + other;
			table.addCell(new Phrase(String.valueOf(amount), dataFont));
		}
		return table;
	}

	private PdfPTable getPerTripLineItemTable(Invoice invoice) {
		PdfPTable table = new PdfPTable(new float[] { 1, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2 }); // Sno,rwnNum,route,customerRef,
		table.setWidthPercentage(100);
		table.getDefaultCell().setMinimumHeight(20);
		table.getDefaultCell().setBorder(3);
		Font headingFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8);
		Font dataFont = FontFactory.getFont(FontFactory.HELVETICA, 8);

		table.addCell(new Phrase("S #", headingFont));
		table.addCell(new Phrase("RWB No.", headingFont));
		table.addCell(new Phrase("Route", headingFont));
		table.addCell(new Phrase("Ref #", headingFont));
		table.addCell(new Phrase("RWB Date", headingFont));
		table.addCell(new Phrase("Vehicle No.", headingFont));
		table.addCell(new Phrase("Trip Charges", headingFont));
		table.addCell(new Phrase("Dtn. Charges", headingFont));
		table.addCell(new Phrase("Ldg. Charges", headingFont));
		table.addCell(new Phrase("Un-ldg. Charges", headingFont));
		table.addCell(new Phrase("Other Charges", headingFont));
		table.addCell(new Phrase("Amount", headingFont));

		int Sno = 1;
		for (LineItem lineItem : invoice.getLineItems()) {
			RoadwayBill rwb = lineItem.getRoadwayBill();
			table.addCell(new Phrase(String.valueOf(Sno++), dataFont));
			table.addCell(new Phrase(rwb.getRwbId(), dataFont));
			table.addCell(new Phrase(rwb.getRoute().toString(), dataFont));
			table.addCell(new Phrase(rwb.getLoadNumber(), dataFont));
			table.addCell(new Phrase(systemUtil.convertToDateString(rwb.getRwbDate()), dataFont));
			if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
				table.addCell(new Phrase(rwb.getTractor().getNumberPlate(), dataFont));
			else
				table.addCell(new Phrase(rwb.getRentalVehicle().getVehicleNumber(), dataFont));
			List<Income> incomes = rwb.getIncomes();
			Double trip = 0d, detention = 0d, loading = 0d, offLoading = 0d, other = 0d;
			for (Income income : incomes) {
				switch (income.getIncomeType().getValue()) {
				case SystemType.INCOME_FREIGHT_RATE:
					trip = income.getAmount();
					break;
				case SystemType.INCOME_DETENTION_CHARGES:
					detention = income.getAmount();
					break;
				case SystemType.INCOME_LOADING_CHARGES:
					loading = income.getAmount();
					break;
				case SystemType.INCOME_OFFLOADING_CHARGES:
					offLoading = income.getAmount();
					break;
				case SystemType.INCOME_OTHER_CHARGES:
					other = income.getAmount();
					break;
				default:
					continue;
				}
			}
			table.addCell(new Phrase(String.valueOf(trip), dataFont));
			table.addCell(new Phrase(String.valueOf(detention), dataFont));
			table.addCell(new Phrase(String.valueOf(loading), dataFont));
			table.addCell(new Phrase(String.valueOf(offLoading), dataFont));
			table.addCell(new Phrase(String.valueOf(other), dataFont));

			Double amount = trip + detention + loading + offLoading + other;
			table.addCell(new Phrase(String.valueOf(amount), dataFont));
		}
		return table;
	}

	private PdfPTable getDHLineItemTable(DHInvoice invoice) {
		PdfPTable table = new PdfPTable(new float[] { 1, 2, 2, 2, 2, 2, 2, 2, 2 }); // Sno,rwnNum,route,customerRef,
		table.setWidthPercentage(100);
		table.getDefaultCell().setMinimumHeight(20);
		table.getDefaultCell().setBorder(3);
		Font headingFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8);
		Font dataFont = FontFactory.getFont(FontFactory.HELVETICA, 8);
		table.addCell(new Phrase("S #", headingFont));
		table.addCell(new Phrase("RWB ID", headingFont));
		table.addCell(new Phrase("Vehicle No.", headingFont));
		table.addCell(new Phrase("Stop Type", headingFont));
		table.addCell(new Phrase("Arrival", headingFont));
		table.addCell(new Phrase("Departure", headingFont));
		table.addCell(new Phrase("Detention/Halting", headingFont));
		table.addCell(new Phrase("Rate", headingFont));
		table.addCell(new Phrase("Amount", headingFont));

		int Sno = 1;
		for (LineItem lineItem : invoice.getLineItems()) {
			RoadwayBill rwb = lineItem.getRoadwayBill();
			List<StopProgress> stopProgresses = rwb.getStopProgresses();
			for (StopProgress stopProgress : stopProgresses) {
				if (stopProgress.getDetentionInHrs() >= 24) {
					table.addCell(new Phrase(String.valueOf(Sno++), dataFont));
					table.addCell(new Phrase(rwb.getRwbId(), dataFont));
					if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
						table.addCell(new Phrase(stopProgress.getTractor().getNumberPlate(), dataFont));
					else
						table.addCell(new Phrase(stopProgress.getRentalVehicle().getVehicleNumber(), dataFont));
					table.addCell(new Phrase(stopProgress.getStop().getStopType().getValue(), dataFont));
					table.addCell(
							new Phrase(systemUtil.convertToDateTimeString(stopProgress.getArrivalTime()), dataFont));
					table.addCell(
							new Phrase(systemUtil.convertToDateTimeString(stopProgress.getDepartureTime()), dataFont));
					table.addCell(new Phrase(String.valueOf(stopProgress.getDetentionInHrs() / 24), dataFont));
					if (stopProgress.getStop().getStopType().getValue().equals(SystemType.STOP_TYPE_PICKUP)) {
						table.addCell(new Phrase(String.valueOf(rwb.getHaltingRate()), dataFont));
						table.addCell(new Phrase(
								String.valueOf((stopProgress.getDetentionInHrs() / 24) * rwb.getHaltingRate()),
								dataFont));
					} else {
						table.addCell(new Phrase(String.valueOf(rwb.getDetentionRate()), dataFont));
						table.addCell(new Phrase(
								String.valueOf((stopProgress.getDetentionInHrs() / 24) * rwb.getDetentionRate()),
								dataFont));
					}
				}
			}
		}
		return table;
	}

	private Paragraph getInvoiceInfo(Invoice invoice) {
		Paragraph p = new Paragraph();
		p.setAlignment(Element.ALIGN_RIGHT);
		p.add(new Chunk("INVOICE # " + invoice.getInvoiceId(), FontFactory.getFont(FontFactory.HELVETICA_BOLD, 18)));
		p.add("\n");
		p.add(new Chunk(systemUtil.convertToDateString(invoice.getInvoiceDate())));
		return p;
	}

	private Paragraph getInvoiceInfo(DHInvoice invoice) {
		Paragraph p = new Paragraph();
		p.setAlignment(Element.ALIGN_RIGHT);
		p.add(new Chunk("INVOICE # " + invoice.getInvoice().getInvoiceId() + "-DH",
				FontFactory.getFont(FontFactory.HELVETICA_BOLD, 18)));
		p.add("\n");
		p.add(new Chunk(systemUtil.convertToDateString(invoice.getInvoiceDate())));
		return p;
	}

	private PdfPTable getAddressTable(Invoice invoice) {
		Company recipient = invoice.getRecipient();
		AdminCompany sender = invoice.getSender();
		PdfPTable table = new PdfPTable(new float[] { 1, 1 });
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		table.addCell(getPartyAddress("BILLED TO:", recipient.getCompanyName(),
				recipient.getCompanyAddresses().get(0).toString(), recipient.getNtnNumber(), recipient.getStrnNumber(),
				recipient.getSra(), recipient.getPra(), recipient.getBra(), recipient.getKra()));
		table.addCell(getPartyAddress("PAYABLE TO:", sender.getAdminCompanyName(),
				sender.getAdminCompanyAddresses().get(0).toString(), sender.getNtnNumber(), sender.getStrnNumber(),
				sender.getSra(), sender.getPra(), sender.getBra(), sender.getKra()));
		return table;
	}

	private PdfPTable getAddressTable(DHInvoice invoice) {
		Company recipient = invoice.getRecipient();
		AdminCompany sender = invoice.getSender();
		PdfPTable table = new PdfPTable(new float[] { 1, 1 });
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		table.addCell(getPartyAddress("BILLED TO:", recipient.getCompanyName(),
				recipient.getCompanyAddresses().get(0).toString(), recipient.getNtnNumber(), recipient.getStrnNumber(),
				recipient.getSra(), recipient.getPra(), recipient.getBra(), recipient.getKra()));
		table.addCell(getPartyAddress("PAYABLE TO:", sender.getAdminCompanyName(),
				sender.getAdminCompanyAddresses().get(0).toString(), sender.getNtnNumber(), sender.getStrnNumber(),
				sender.getSra(), sender.getPra(), sender.getBra(), sender.getKra()));
		return table;
	}

	private PdfPCell getPartyAddress(String who, String name, String address, String ntn, String strn, String sra,
			String pra, String bra, String kra) {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(0);
		String NEWLINE = "\n";
		Paragraph p = new Paragraph();
		p.setMultipliedLeading(1.0f);
		p.add(new Phrase(who, FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
		p.add(NEWLINE);
		p.add(NEWLINE);
		p.add(name);
		p.add(NEWLINE);
		p.add(NEWLINE);
		p.add(address);
		p.add(NEWLINE);
		p.add(NEWLINE);

		cell.addElement(p);
		cell.addElement(getTaxAccountTable(ntn, strn, sra, pra, bra, kra));
		return cell;
	}

	private PdfPTable getTaxAccountTable(String ntn, String strn, String sra, String pra, String bra, String kra) {
		PdfPTable table = new PdfPTable(new float[] { 1, 1 });
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(0);
		if (ntn != null && !ntn.isBlank()) {
			table.addCell("NTN: " + ntn);
		}
		if (strn != null && !strn.isBlank()) {
			table.addCell("STRN: " + strn);
		}
		if (sra != null && !sra.isBlank()) {
			table.addCell("SRA: " + sra);
		}
		if (pra != null && !pra.isBlank()) {
			table.addCell("PRA: " + pra);
		}
		if (bra != null && !bra.isBlank()) {
			table.addCell("BRA: " + bra);
		}
		if (kra != null && !kra.isBlank()) {
			table.addCell("KRA: " + kra);
		}
		return table;
	}

	@Override
	public byte[] previewDHInvoice(DHInvoice invoice) throws DocumentException {
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, baos);
		document.open();
		document.add(getInvoiceInfo(invoice));
		document.add(new Chunk("\n"));
		document.add(getAddressTable(invoice));
		document.add(new Chunk("\n"));
		document.add(getDHLineItemTable(invoice));
		document.add(getDHLineItemAmount(invoice));
		document.add(new Chunk("\n"));
		PdfPTable taxTable = getTaxTable(invoice);
		if (taxTable != null) {
			document.add(taxTable);
		}
		document.add(getTotalAmountTable(invoice));
		document.add(new Chunk("\n"));
		document.add(getNote(invoice));

		document.close();
		writer.close();
		return baos.toByteArray();
	}

	@Override
	public byte[] exportTractorsToExcel(List<Tractor> tractors, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SystemType.EXCEL_TRIP_DETAIL_FILE_NAME);
		Row header = sheet.createRow(0);
		List<String> columns = new ArrayList<>();
		columns.addAll(SystemType.TRACTOR_EXPORT_COLUMNS);
		for (int i = 0; i < columns.size(); i++) {
			header.createCell(i, CellType.STRING).setCellValue(columns.get(i));
		}

		for (int i = 1; i <= tractors.size(); i++) {
			Tractor tractor = tractors.get(i - 1);
			Asset asset = tractor.getAsset();
			Row row = sheet.createRow(i);
			for (int j = sheet.getRow(0).getFirstCellNum(); j < sheet.getRow(0).getLastCellNum(); j++) {
				Cell cell = row.createCell(j);
				switch (sheet.getRow(0).getCell(j).getStringCellValue()) {
				case "TRACTOR ID":
					cell.setCellValue(tractor.getTractorId() == null ? "" : tractor.getTractorId());
					break;
				case "ENGINE TYPE":
					cell.setCellValue(tractor.getEngineType() == null ? "" : tractor.getEngineType());
					break;
				case "ENGINE NUMBER":
					cell.setCellValue(tractor.getEngineNumber() == null ? "" : tractor.getEngineNumber());
					break;
				case "TRANSMISSION":
					cell.setCellValue(tractor.getTransmission() == null ? "" : tractor.getTransmission());
					break;
				case "NUMBER PLATE":
					cell.setCellValue(tractor.getNumberPlate() == null ? "" : tractor.getNumberPlate());
					break;
				case "NUMBER OF GEARS":
					cell.setCellValue(tractor.getNumberOfGears() == null ? "" : tractor.getNumberOfGears().toString());
					break;
				case "NUMBER OF FUEL TANKS":
					cell.setCellValue(
							tractor.getNumberOfFuelTanks() == null ? "" : tractor.getNumberOfFuelTanks().toString());
					break;
				case "FUEL CAPACITY":
					cell.setCellValue(tractor.getFuelCapacity() == null ? "" : tractor.getFuelCapacity().toString());
					break;
				case "STARTING ODOMETER":
					cell.setCellValue(
							tractor.getStartingOdometer() == null ? "" : tractor.getStartingOdometer().toString());
					break;
				case "CURRENT ODOMETER":
					cell.setCellValue(
							tractor.getCurrentOdometer() == null ? "" : tractor.getCurrentOdometer().toString());
					break;
				case "ODOMETER UNIT":
					cell.setCellValue(tractor.getOdometerUnit() == null ? "" : tractor.getOdometerUnit());
					break;
				case "INDUCTION":
					cell.setCellValue(
							asset.getInduction() == null ? "" : systemUtil.convertToDateString(asset.getInduction()));
					break;
				case "LEASE TYPE":
					cell.setCellValue(asset.getLeaseType() == null ? "" : asset.getLeaseType().getValue());
					break;
				case "SUPPLIER ID":
					cell.setCellValue(asset.getSupplier() == null ? "" : asset.getSupplier().getCompanyId());
					break;
				case "VIN":
					cell.setCellValue(asset.getVin() == null ? "" : asset.getVin());
					break;
				case "ASSET MAKE":
					cell.setCellValue(asset.getAssetMake() == null ? "" : asset.getAssetMake().getValue());
					break;
				case "MODEL":
					cell.setCellValue(asset.getModel() == null ? "" : asset.getModel());
					break;
				case "YEAR":
					cell.setCellValue(asset.getYear() == null ? "" : asset.getYear().toString());
					break;
				case "COLOR":
					cell.setCellValue(asset.getColor() == null ? "" : asset.getColor());
					break;
				case "STATUS":
					cell.setCellValue(asset.getStatus() == null ? "" : asset.getStatus());
					break;
				case "TYRE SIZE":
					cell.setCellValue(asset.getTyreSize() == null ? "" : asset.getTyreSize());
					break;
				case "NUMBER OF TYRES":
					cell.setCellValue(asset.getNumberOfTyres() == null ? "" : asset.getNumberOfTyres().toString());
					break;
				case "GROSS WEIGHT":
					cell.setCellValue(asset.getGrossWeight() == null ? "" : asset.getGrossWeight().toString());
					break;
				case "WEIGHT UNIT":
					cell.setCellValue(asset.getWeightUnit() == null ? "" : asset.getWeightUnit());
					break;
				default:
					cell.setCellValue("");
				}
			}

		}
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			workbook.write(outputStream);
			workbook.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public byte[] exportTrailersToExcel(List<Trailer> trailers, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SystemType.EXCEL_TRIP_DETAIL_FILE_NAME);
		Row header = sheet.createRow(0);
		List<String> columns = new ArrayList<>();
		columns.addAll(SystemType.TRAILER_EXPORT_COLUMNS);
		for (int i = 0; i < columns.size(); i++) {
			header.createCell(i, CellType.STRING).setCellValue(columns.get(i));
		}

		for (int i = 1; i <= trailers.size(); i++) {
			Trailer trailer = trailers.get(i - 1);
			Asset asset = trailer.getAsset();
			Row row = sheet.createRow(i);
			for (int j = sheet.getRow(0).getFirstCellNum(); j < sheet.getRow(0).getLastCellNum(); j++) {
				Cell cell = row.createCell(j);
				switch (sheet.getRow(0).getCell(j).getStringCellValue()) {
				case "TRAILER ID":
					cell.setCellValue(trailer.getTrailerId());
					break;
				case "TRAILER NUMBER":
					cell.setCellValue(asset.getVin() == null ? "" : asset.getVin());
					break;
				case "TRAILER TYPE":
					cell.setCellValue(trailer.getTrailerType() == null ? "" : trailer.getTrailerType().getValue());
					break;
				case "TRAILER SIZE":
					cell.setCellValue(trailer.getTrailerSize() == null ? "" : trailer.getTrailerSize().getValue());
					break;
				case "INDUCTION":
					cell.setCellValue(
							asset.getInduction() == null ? "" : systemUtil.convertToDateString(asset.getInduction()));
					break;
				case "LEASE TYPE":
					cell.setCellValue(asset.getLeaseType() == null ? "" : asset.getLeaseType().getValue());
					break;
				case "SUPPLIER ID":
					cell.setCellValue(asset.getSupplier() == null ? "" : asset.getSupplier().getCompanyId());
					break;
				case "YEAR":
					cell.setCellValue(asset.getYear() == null ? "" : asset.getYear().toString());
					break;
				case "NUMBER OF TYRES":
					cell.setCellValue(asset.getNumberOfTyres() == null ? "" : asset.getNumberOfTyres().toString());
					break;
				case "GROSS WEIGHT":
					cell.setCellValue(asset.getGrossWeight() == null ? "" : asset.getGrossWeight().toString());
					break;
				case "WEIGHT UNIT":
					cell.setCellValue(asset.getWeightUnit() == null ? "" : asset.getWeightUnit());
					break;
				default:
					cell.setCellValue("");
				}
			}

		}
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			workbook.write(outputStream);
			workbook.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public byte[] exportEmployeesToExcel(List<Employee> employees, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SystemType.EXCEL_TRIP_DETAIL_FILE_NAME);
		Row header = sheet.createRow(0);
		List<String> columns = new ArrayList<>();
		columns.addAll(SystemType.EMPLOYEE_EXPORT_COLUMNS);
		for (int i = 0; i < columns.size(); i++) {
			header.createCell(i, CellType.STRING).setCellValue(columns.get(i));
		}

		for (int i = 1; i <= employees.size(); i++) {
			Row row = sheet.createRow(i);
			for (int j = sheet.getRow(0).getFirstCellNum(); j < sheet.getRow(0).getLastCellNum(); j++) {
				Employee employee = employees.get(i - 1);
				EmployeeContract contract = contractService.findActiveEmployeeContract(employee, null);
				Address address = employee.getEmployeeAddresses().get(0);
				Contact contact = employee.getEmployeeContacts().get(0);
				Cell cell = row.createCell(j);
				switch (sheet.getRow(0).getCell(j).getStringCellValue()) {
				case "EMPLOYEE ID":
					cell.setCellValue(employee.getEmployeeId() == null ? "" : employee.getEmployeeId());
					break;
				case "EMPLOYEE TYPE":
					cell.setCellValue(employee.getEmployeeType() == null ? "" : employee.getEmployeeType().getValue());
					break;
				case "SUPPLIER ID":
					cell.setCellValue(employee.getSupplier() == null ? "" : employee.getSupplier().getCompanyId());
					break;
				case "FIRST NAME":
					cell.setCellValue(employee.getFirstName() == null ? "" : employee.getFirstName());
					break;
				case "LAST NAME":
					cell.setCellValue(employee.getLastName() == null ? "" : employee.getLastName());
					break;
				case "EMPLOYEE NO.":
					cell.setCellValue(employee.getEmployeeNumber() == null ? "" : employee.getEmployeeNumber());
					break;
				case "DATE OF BIRTH":
					cell.setCellValue(employee.getDateOfBirth() == null ? ""
							: systemUtil.convertToDateString(employee.getDateOfBirth()));
					break;
				case "CNIC #":
					cell.setCellValue(employee.getCnic() == null ? "" : employee.getCnic());
					break;
				case "CNIC EXPIRY":
					cell.setCellValue(employee.getCnicExpiry() == null ? ""
							: systemUtil.convertToDateString(employee.getCnicExpiry()));
					break;
				case "LICENSE #":
					cell.setCellValue(employee.getLicenceNumber() == null ? "" : employee.getLicenceNumber());
					break;
				case "LICENSE CLASS":
					cell.setCellValue(employee.getLicenceClass() == null ? "" : employee.getLicenceClass());
					break;
				case "LICENSE EXPIRY":
					cell.setCellValue(employee.getLicenceExpiry() == null ? ""
							: systemUtil.convertToDateString(employee.getLicenceExpiry()));
					break;
				case "JOINING DATE":
					cell.setCellValue(contract == null ? "" : systemUtil.convertToDateString(contract.getStartDate()));
					break;
				case "ADDRESS 1":
					cell.setCellValue((address == null || address.getStreet1() == null) ? "" : address.getStreet1());
					break;
				case "ADDRESS 2":
					cell.setCellValue((address == null || address.getStreet2() == null) ? "" : address.getStreet2());
					break;
				case "CITY":
					cell.setCellValue(
							(address == null || address.getCity() == null) ? "" : address.getCity().getValue());
					break;
				case "STATE":
					cell.setCellValue(
							(address == null || address.getState() == null) ? "" : address.getState().getValue());
					break;
				case "COUNTRY":
					cell.setCellValue((address == null || address.getCountry() == null) ? "" : address.getCountry());
					break;
				case "POSITION":
					cell.setCellValue(employee.getPosition() == null ? "" : employee.getPosition().getValue());
					break;
				case "CONTACT":
					cell.setCellValue(contact == null ? "" : contact.getData());
					break;
				case "NEXT OF KIN":
					cell.setCellValue(employee.getNextOfKin() == null ? "" : employee.getNextOfKin());
					break;
				case "NEXT OF KIN RELATION":
					cell.setCellValue(employee.getNextOfKinRelation() == null ? "" : employee.getNextOfKinRelation());
					break;
				case "SALARY":
					cell.setCellValue(
							(contact == null || contract.getSalary() == null) ? "" : contract.getSalary().toString());
					break;
				default:
					cell.setCellValue("");
				}
			}

		}
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			workbook.write(outputStream);
			workbook.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public byte[] exportWorkOrderToExcel(List<WorkOrder> workOrders, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SystemType.EXCEL_TRIP_DETAIL_FILE_NAME);
		Row header = sheet.createRow(0);
		List<String> columns = new ArrayList<>();
		columns.addAll(SystemType.WORK_ORDER_EXPORT_COLUMNS);
		for (int i = 0; i < columns.size(); i++) {
			header.createCell(i, CellType.STRING).setCellValue(columns.get(i));
		}

		for (int i = 1; i <= workOrders.size(); i++) {
			WorkOrder workOrder = workOrders.get(i - 1);
			for (int k = 1; k <= workOrder.getWorkOrderItemDetail().size(); k++) {
				Row row = sheet.createRow(k);
				for (int j = sheet.getRow(0).getFirstCellNum(); j < sheet.getRow(0).getLastCellNum(); j++) {
					WorkOrderItemDetail itemDetail = workOrder.getWorkOrderItemDetail().get(k - 1);
					Cell cell = row.createCell(j);
					switch (sheet.getRow(0).getCell(j).getStringCellValue()) {
					case "WO ID":
						cell.setCellValue(
								workOrder.getWorkOrderId() == null ? "" : workOrder.getWorkOrderId().toString());
						break;
					case "PRIORITY":
						cell.setCellValue(workOrder.getPriority() == null ? "" : workOrder.getPriority().getValue());
						break;
					case "ASSET":
						cell.setCellValue(
								workOrder.getTractor() == null ? "" : workOrder.getTractor().getNumberPlate());
						break;
					case "KM":
						cell.setCellValue(workOrder.getTractorKm() == null ? "" : workOrder.getTractorKm().toString());
						break;
					case "BASE STATION":
						cell.setCellValue(
								workOrder.getBaseStation() == null ? "" : workOrder.getBaseStation().getValue());
						break;
					case "WO DATE":
						cell.setCellValue(workOrder.getWorkOrderDate() == null ? ""
								: systemUtil.convertToDateString(workOrder.getWorkOrderDate()));
						break;
					case "STATUS":
						cell.setCellValue(workOrder.getWorkOrderStatus() == null ? ""
								: workOrder.getWorkOrderStatus().getValue());
						break;
					case "CATEGORY":
						cell.setCellValue(workOrder.getWorkOrderCategory() == null ? ""
								: workOrder.getWorkOrderCategory().getValue());
						break;
					case "SUB CATEGORY":
						cell.setCellValue(workOrder.getWorkOrderSubCategory() == null ? ""
								: workOrder.getWorkOrderSubCategory().getValue());
						break;
					case "ITEM ID":
						cell.setCellValue(
								itemDetail.getItem() == null ? "" : itemDetail.getItem().getItemId().toString());
						break;
					case "ITEM":
						cell.setCellValue(itemDetail.getItem() == null ? "" : itemDetail.getItem().getItemName());
						break;
					case "ITEM TYPE":
						cell.setCellValue(
								(itemDetail.getItem() == null || itemDetail.getItem().getItemType() == null) ? ""
										: itemDetail.getItem().getItemType().getValue());
						break;
					case "QTY":
						cell.setCellValue(itemDetail.getQtyUsed() == null ? "" : itemDetail.getQtyUsed().toString());
						break;
					case "RATE":
						cell.setCellValue(itemDetail.getRate() == null ? "" : itemDetail.getRate().toString());
						break;
					case "LEASE TYPE":
						cell.setCellValue(workOrder.getTractor() == null ? ""
								: workOrder.getTractor().getAsset().getLeaseType().getValue());
						break;
					case "CREATED ON":
						cell.setCellValue(workOrder.getWorkOrderDate() == null ? ""
								: systemUtil.convertToDateString(workOrder.getWorkOrderDate()));
						break;
					case "UPDATED ON":
						cell.setCellValue(workOrder.getWorkOrderDate() == null ? ""
								: systemUtil.convertToDateString(workOrder.getWorkOrderDate()));
						break;
					case "CLOSED ON":
						cell.setCellValue(workOrder.getWorkOrderCompletionDate() == null ? ""
								: systemUtil.convertToDateString(workOrder.getWorkOrderCompletionDate()));
						break;
					case "DPT NAME":
						cell.setCellValue(
								workOrder.getWorkshop() == null ? "" : workOrder.getWorkshop().getCompanyName());
						break;
					case "EST DURATION":
						cell.setCellValue(
								(workOrder.getWorkOrderCompletionDate() == null || workOrder.getWorkOrderDate() == null)
										? ""
										: systemUtil.differenceInDateTime(workOrder.getWorkOrderDate(),
												workOrder.getWorkOrderCompletionDate()).toString());
						break;
					case "ACTIVITY DETAIL":
						cell.setCellValue(workOrder.getActivityDetail() == null ? "" : workOrder.getActivityDetail());
						break;
					case "SENDER":
						cell.setCellValue(workOrder.getSenderName() == null ? "" : workOrder.getSenderName());
						break;
					case "OWNER":
						cell.setCellValue(workOrder.getReferenceNumber() == null ? "" : workOrder.getReferenceNumber());
						break;
//				case "DELAY HRS":
//					cell.setCellValue(employee.getNextOfKinRelation() == null ? "" : employee.getNextOfKinRelation());
//					break;
					case "WORKSHOP":
						cell.setCellValue(
								workOrder.getWorkshop() == null ? "" : workOrder.getWorkshop().getCompanyName());
						break;
					case "TOTAL AMOUNT":
						cell.setCellValue(
								workOrder.getTotalAmount() == null ? "" : workOrder.getTotalAmount().toString());
						break;
					default:
						cell.setCellValue("");
					}
				}
			}

		}
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			workbook.write(outputStream);
			workbook.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public byte[] exportInventoriesToExcel(List<Inventory> inventories, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SystemType.EXCEL_INVENTORY_FILE_NAME);
		Row header = sheet.createRow(0);
		List<String> columns = new ArrayList<>();
		columns.addAll(SystemType.INVENTORY_EXPORT_COLUMNS);
		for (int i = 0; i < columns.size(); i++) {
			header.createCell(i, CellType.STRING).setCellValue(columns.get(i));
		}

		for (int i = 1; i <= inventories.size(); i++) {
			Inventory inventory = inventories.get(i - 1);
			Row row = sheet.createRow(i);
			for (int j = sheet.getRow(0).getFirstCellNum(); j < sheet.getRow(0).getLastCellNum(); j++) {
				Cell cell = row.createCell(j);
				switch (sheet.getRow(0).getCell(j).getStringCellValue()) {
				case "INVENTORY ID":
					cell.setCellValue(inventory.getInventoryId() == null ? "" : inventory.getInventoryId().toString());
					break;
				case "ITEM NAME":
					cell.setCellValue(inventory.getItem() == null ? "" : inventory.getItem().getItemName());
					break;
				case "SKU":
					cell.setCellValue(inventory.getItem() == null ? "" : inventory.getItem().getItemSku());
					break;
				case "QTY IN":
					cell.setCellValue(inventory.getQtyIn() == null ? "" : inventory.getQtyIn().toString());
					break;
				case "QTY OUT":
					cell.setCellValue(inventory.getQtyOut() == null ? "" : inventory.getQtyOut().toString());
					break;
				case "STOCK COMMITTED":
					cell.setCellValue(
							inventory.getQtyCommitted() == null ? "" : inventory.getQtyCommitted().toString());
					break;
				case "STOCK ON HAND":
					cell.setCellValue(inventory.getQtyOnHand() == null ? "" : inventory.getQtyOnHand().toString());
					break;
				case "STOCK EXPECTED":
					cell.setCellValue(inventory.getExpectedQty() == null ? "" : inventory.getExpectedQty().toString());
					break;
				default:
					cell.setCellValue("");
				}
			}

		}
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			workbook.write(outputStream);
			workbook.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

}
