package com.fleetX.service.implementation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.IEmployeeDAO;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.repository.AddressRepository;
import com.fleetX.repository.ContactRepository;
import com.fleetX.repository.DPositionRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IFTPService;
import com.fleetX.service.IOrderService;
import com.fleetX.validator.IEmployeeValidator;

@Service
@Transactional
public class EmployeeService implements IEmployeeService {

	@Autowired
	IEmployeeDAO employeeDAO;
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	ContactRepository contactRepository;
	@Autowired
	IAdminService adminService;
	@Autowired
	IEmployeeValidator employeeValidator;
	@Autowired
	DPositionRepository positionRepository;
	@Autowired
	IOrderService orderService;
	@Autowired
	IFTPService ftpService;
	@Autowired
	SystemUtil systemUtil;

	@Override
	public List<Employee> findAllEmployee() {
		return employeeDAO.findAllEmployees();
	}

	@Override
	public Employee findEmployeeById(String employeeId) {
		return employeeDAO.findEmployeeById(employeeId);
	}

	@Override

	public Employee addEmployee(Employee employee) throws Exception {
		Assert.notNull(employee, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateEmployee(employee);

		employee.setEmployeeId(null);
		employee.setAvailability(SystemType.STATUS_AVAILABLE);
		employee.setMovingStatus(SystemType.STATUS_MOVING_FREE);

		employee.getEmployeeAddresses().stream().forEach(x -> {
			x.setAddressId(null);
			x = addressRepository.save(x);
		});

		employee.getEmployeeContacts().stream().forEach(x -> {
			x.setContactId(null);
			x = contactRepository.save(x);
		});

		Employee employeeInDB = employeeDAO.createEmployee(employee);
		List<EmployeeContract> contracts = employee.getEmployeeContracts();
		if (contracts != null && contracts.size() > 0)
			for (EmployeeContract employeeContract : contracts) {
				employeeContract.setEmployeeContractId(null);
				employeeContract.setEmployee(employeeInDB);
				employeeDAO.addEmployeeContract(employeeContract);
			}
		return employeeInDB;
	}

	@Override

	public Employee updateEmployee(Employee employeeNew) throws Exception {
		Assert.notNull(employeeNew, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateEmployee(employeeNew);

		Employee employeeOld = findEmployeeById(employeeNew.getEmployeeId());
		employeeOld.setStatus(employeeNew.getStatus());
		employeeOld.setPosition(employeeNew.getPosition());
		employeeOld.setLicenceNumber(employeeNew.getLicenceNumber());
		employeeOld.setLicenceExpiry(employeeNew.getLicenceExpiry());
		employeeOld.setLicenceClass(employeeNew.getLicenceClass());
		employeeOld.setPictureUrl(employeeNew.getPictureUrl());
		employeeOld.setNtnNumber(employeeNew.getNtnNumber());
		employeeOld.setNextOfKinRelation(employeeNew.getNextOfKinRelation());
		employeeOld.setNextOfKin(employeeNew.getNextOfKin());
		employeeOld.setFirstName(employeeNew.getFirstName());
		employeeOld.setLastName(employeeNew.getLastName());
		employeeOld.setEmployeeNumber(employeeNew.getEmployeeNumber());
		employeeOld.setDepartment(employeeNew.getDepartment());
		employeeOld.setDateOfBirth(employeeNew.getDateOfBirth());
		employeeOld.setCnic(employeeNew.getCnic());
		employeeOld.setCnicExpiry(employeeNew.getCnicExpiry());
		employeeOld.setAcAddress(employeeNew.getAcAddress());
		employeeOld.setEmployeeType(employeeNew.getEmployeeType());
		employeeOld.setSupplier(employeeNew.getSupplier());
		employeeOld.setAdminCompany(employeeNew.getAdminCompany());

		List<Address> addressesOld = employeeOld.getEmployeeAddresses();
		List<Address> addressesNew = employeeNew.getEmployeeAddresses();
		addressesNew = adminService.adjustAddresses(addressesOld, addressesNew);
		employeeOld.setEmployeeAddresses(addressesNew);

		List<Contact> contactsOld = employeeOld.getEmployeeContacts();
		List<Contact> contactsNew = employeeNew.getEmployeeContacts();
		contactsNew = adminService.adjustContacts(contactsOld, contactsNew);
		employeeOld.setEmployeeContacts(contactsNew);

		List<EmployeeContract> employeeContractsOld = employeeOld.getEmployeeContracts();
		List<EmployeeContract> employeeContractsNew = employeeNew.getEmployeeContracts();
		List<EmployeeContract> employeeContractsTBD = new ArrayList<>(employeeOld.getEmployeeContracts());
		List<EmployeeContract> employeeContractsTBA = new ArrayList<>(employeeNew.getEmployeeContracts());

		for (EmployeeContract contractNew : employeeContractsNew) {
			for (EmployeeContract contractOld : employeeContractsOld) {
				if (contractNew.getEmployeeContractId() != null
						&& contractNew.getEmployeeContractId().equals(contractOld.getEmployeeContractId())) {
					employeeContractsTBA.remove(contractNew);
					employeeContractsTBD.remove(contractOld);
					contractOld.setContractStatus(contractNew.getContractStatus());
					contractOld.setStartDate(contractNew.getStartDate());
					contractOld.setEndDate(contractNew.getEndDate());
					contractOld.setWorkingExperience(contractNew.getWorkingExperience());
					contractOld.setSalary(contractNew.getSalary());
					contractOld.setPreviousEmployer1(contractNew.getPreviousEmployer1());
					contractOld.setPreviousEmployer2(contractNew.getPreviousEmployer2());
					contractOld.setSeparationReason(contractNew.getSeparationReason());
					employeeDAO.updateEmployeeContract(contractOld);
				}
			}
		}
		for (EmployeeContract contractNew : employeeContractsTBA) {
			contractNew.setEmployeeContractId(null);
			contractNew.setEmployee(employeeOld);
			employeeDAO.addEmployeeContract(contractNew);
		}
		employeeDAO.deleteEmployeeContracts(employeeContractsTBD);

		return employeeDAO.updateEmployee(employeeOld);
	}

	@Override
	public void deleteEmployee(String employeeId) throws Exception {
		Assert.notNull(employeeId, "Employee ID: " + SystemType.MUST_NOT_BE_NULL);
		employeeDAO.deleteEmployee(employeeId);
	}

	@Override
	public List<Employee> filterEmployees(String position, String movingStatus, String rwbId, String avail,
			String firstName, String lastName, String employeeNumber, String cnic, String licenseNumber) {
		Employee employee = new Employee();
		employee.setMovingStatus(movingStatus);
		employee.setAvailability(avail);
		employee.setPosition(positionRepository.findByValue(position));
		employee.setFirstName(firstName);
		employee.setLastName(lastName);
		employee.setEmployeeNumber(employeeNumber);
		employee.setCnic(cnic);
		employee.setLicenceNumber(licenseNumber);
		ExampleMatcher options = ExampleMatcher.matchingAll()
				.withMatcher("firstName", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("lastName", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("employeeNumber", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("licenceNumber", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("cnic", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());
		List<Employee> employees = employeeDAO.filterEmployeeByExample(Example.of(employee, options));
		if (rwbId != null) {
			RoadwayBill rwb = orderService.getRoadwayBillById(rwbId);
			if (rwb.getDriver1() != null)
				employees.add(rwb.getDriver1());
			if (rwb.getDriver2() != null)
				employees.add(rwb.getDriver2());
		}
		return employees;
	}

	@Override
	public TrainingProfile addTrainingProfile(TrainingProfile trainingProfile, MultipartFile certificate,
			MultipartFile attendanceSheet, MultipartFile picWithTrainer) throws IOException {
		Assert.notNull(trainingProfile, "Training Profile: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateTrainingProfile(trainingProfile);
		String path = SystemType.FTP_PATH_EMPLOYEE + trainingProfile.getEmployee().getEmployeeNumber();
		path += "/Training Profiles";
		String trainingCertificateUrl = "";
		String attendanceSheetUrl = "";
		String pictureWithTrainerUrl = "";
		if (certificate != null) {
			String ext = systemUtil.getExtensionFromFileName(certificate.getOriginalFilename());
			String filename = trainingProfile.getEmployee().getEmployeeNumber() + "_TP_CERT." + ext;
			trainingCertificateUrl = ftpService.uploadFile(path, certificate, filename);
		}
		if (attendanceSheet != null) {
			String ext = systemUtil.getExtensionFromFileName(attendanceSheet.getOriginalFilename());
			String filename = trainingProfile.getEmployee().getEmployeeNumber() + "_TP_AS." + ext;
			attendanceSheetUrl = ftpService.uploadFile(path, attendanceSheet, filename);
		}
		if (picWithTrainer != null) {
			String ext = systemUtil.getExtensionFromFileName(picWithTrainer.getOriginalFilename());
			String filename = trainingProfile.getEmployee().getEmployeeNumber() + "_TP_PWT." + ext;
			pictureWithTrainerUrl = ftpService.uploadFile(path, picWithTrainer, filename);
		}
		trainingProfile.setTrainingCertificateUrl(trainingCertificateUrl);
		trainingProfile.setAttendanceSheetUrl(attendanceSheetUrl);
		trainingProfile.setPictureWithTrainerUrl(pictureWithTrainerUrl);
		trainingProfile.setTrainingProfileId(null);
		return employeeDAO.addTrainingProfile(trainingProfile);
	}

	@Override
	public MedicalTestReport addMedicalTestReport(MedicalTestReport medicalTestReport, MultipartFile file)
			throws IOException {
		Assert.notNull(medicalTestReport, "Medical Test Report: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateMedicalTestReport(medicalTestReport);
		String path = SystemType.FTP_PATH_EMPLOYEE + medicalTestReport.getEmployee().getEmployeeNumber();
		path += "/Medical Test Reports";
		String url = null;
		if (file != null) {
			String ext = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
			String filename = medicalTestReport.getEmployee().getEmployeeNumber() + "_MTR." + ext;
			url = ftpService.uploadFile(path, file, filename);
		}
		medicalTestReport.setMtrId(null);
		medicalTestReport.setUrl(url);
		return employeeDAO.addMedicalTestReport(medicalTestReport);
	}

	@Override
	public HeavyVehicleExperience addHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience) {
		Assert.notNull(heavyVehicleExperience, "Heavy Vehicle Experience: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateHeavyVehicleExperience(heavyVehicleExperience);
		return employeeDAO.addHeavyVehicleExperience(heavyVehicleExperience);
	}

	@Override
	public Reference addReference(Reference reference, MultipartFile cnic, MultipartFile referenceForm)
			throws IOException {
		Assert.notNull(reference, "Reference: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateReference(reference);
		String cnicUrl = "";
		String referenceFormUrl = "";
		String path = SystemType.FTP_PATH_EMPLOYEE + reference.getEmployee().getEmployeeNumber();
		path += "/References";
		if (cnic != null) {
			String ext = systemUtil.getExtensionFromFileName(cnic.getOriginalFilename());
			String filename = reference.getEmployee().getEmployeeNumber() + "_REF_CNIC." + ext;
			cnicUrl = ftpService.uploadFile(path, cnic, filename);
		}
		if (referenceForm != null) {
			String ext = systemUtil.getExtensionFromFileName(referenceForm.getOriginalFilename());
			String filename = reference.getEmployee().getEmployeeNumber() + "_REF_FORM." + ext;
			referenceFormUrl = ftpService.uploadFile(path, referenceForm, filename);
		}
		reference.setCnicUrl(cnicUrl);
		reference.setReferenceFormUrl(referenceFormUrl);
		reference.setReferenceId(null);
		return employeeDAO.addReference(reference);
	}

	@Override
	public String addProfilePicture(String employeeId, MultipartFile dp) throws IOException {
		Assert.notNull(employeeId, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		Employee employee = findEmployeeById(employeeId);
		String path = SystemType.FTP_PATH_EMPLOYEE + employee.getEmployeeNumber();
		path += "/Profile pictures";
		String dpUrl = null;
		if (dp != null) {
			String ext = systemUtil.getExtensionFromFileName(dp.getOriginalFilename());
			String filename = employee.getEmployeeNumber() + "_DP." + ext;
			dpUrl = ftpService.uploadFile(path, dp, filename);
		}
		employee.setPictureUrl(dpUrl);
		employeeDAO.updateEmployee(employee);
		return dpUrl;
	}

	@Override

	public TrainingProfile updateTrainingProfile(TrainingProfile trainingProfileNew, MultipartFile certificate,
			MultipartFile attendanceSheet, MultipartFile picWithTrainer) throws IOException {
		Assert.notNull(trainingProfileNew, SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateTrainingProfile(trainingProfileNew);

		TrainingProfile trainingProfileOld = employeeDAO
				.findTrainingProfileById(trainingProfileNew.getTrainingProfileId());
		trainingProfileOld.setTrainingType(trainingProfileNew.getTrainingType());
		trainingProfileOld.setTrainingStation(trainingProfileNew.getTrainingStation());
		trainingProfileOld.setTrainerName(trainingProfileNew.getTrainerName());
		trainingProfileOld.setIssuedDate(trainingProfileNew.getIssuedDate());
		trainingProfileOld.setExpiryDate(trainingProfileNew.getExpiryDate());
		trainingProfileOld.setTrainingCertificateUrl(trainingProfileNew.getTrainingCertificateUrl());
		trainingProfileOld.setPictureWithTrainerUrl(trainingProfileNew.getPictureWithTrainerUrl());
		trainingProfileOld.setAttendanceSheetUrl(trainingProfileNew.getAttendanceSheetUrl());
		trainingProfileOld.setEmployee(trainingProfileNew.getEmployee());

		String trainingCertificateUrl = trainingProfileOld.getTrainingCertificateUrl();
		String attendanceSheetUrl = trainingProfileOld.getAttendanceSheetUrl();
		String pictureWithTrainerUrl = trainingProfileOld.getPictureWithTrainerUrl();
		String path = SystemType.FTP_PATH_EMPLOYEE + trainingProfileOld.getEmployee().getEmployeeNumber();
		path += "/Training Profiles";
		if (certificate != null) {
			if (trainingCertificateUrl != null) {
				ftpService.deleteFile(trainingCertificateUrl);
			}
			String ext = systemUtil.getExtensionFromFileName(certificate.getOriginalFilename());
			String filename = trainingProfileOld.getEmployee().getEmployeeNumber() + "_TP_CERT." + ext;
			trainingCertificateUrl = ftpService.uploadFile(path, certificate, filename);
		} else {
			ftpService.deleteFile(trainingCertificateUrl);
			trainingCertificateUrl = null;
		}
		if (attendanceSheet != null) {
			if (attendanceSheetUrl != null) {
				ftpService.deleteFile(attendanceSheetUrl);
			}
			String ext = systemUtil.getExtensionFromFileName(attendanceSheet.getOriginalFilename());
			String filename = trainingProfileOld.getEmployee().getEmployeeNumber() + "_TP_AS." + ext;
			attendanceSheetUrl = ftpService.uploadFile(path, attendanceSheet, filename);
		} else {
			ftpService.deleteFile(attendanceSheetUrl);
			attendanceSheetUrl = null;
		}
		if (picWithTrainer != null) {
			if (pictureWithTrainerUrl != null) {
				ftpService.deleteFile(pictureWithTrainerUrl);
			}
			String ext = systemUtil.getExtensionFromFileName(picWithTrainer.getOriginalFilename());
			String filename = trainingProfileOld.getEmployee().getEmployeeNumber() + "_TP_PWT." + ext;
			pictureWithTrainerUrl = ftpService.uploadFile(path, picWithTrainer, filename);
		} else {
			ftpService.deleteFile(pictureWithTrainerUrl);
			pictureWithTrainerUrl = null;
		}
		trainingProfileOld.setTrainingCertificateUrl(trainingCertificateUrl);
		trainingProfileOld.setAttendanceSheetUrl(attendanceSheetUrl);
		trainingProfileOld.setPictureWithTrainerUrl(pictureWithTrainerUrl);
		return employeeDAO.updateTrainingProfile(trainingProfileOld);
	}

	@Override

	public MedicalTestReport updateMedicalTestReport(MedicalTestReport medicalTestReportNew, MultipartFile file)
			throws IOException {
		Assert.notNull(medicalTestReportNew, "Medical Test Report: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateMedicalTestReport(medicalTestReportNew);

		MedicalTestReport medicalTestReportOld = employeeDAO.findMedicalTestReportById(medicalTestReportNew.getMtrId());
		medicalTestReportOld.setMtrType(medicalTestReportNew.getMtrType());
		medicalTestReportOld.setMtrDate(medicalTestReportNew.getMtrDate());
		medicalTestReportOld.setMtrHospital(medicalTestReportNew.getMtrHospital());
		medicalTestReportOld.setMtrStation(medicalTestReportNew.getMtrStation());
		medicalTestReportOld.setMtrResult(medicalTestReportNew.getMtrResult());
		medicalTestReportOld.setUrl(medicalTestReportNew.getUrl());
		medicalTestReportOld.setEmployee(medicalTestReportNew.getEmployee());

		String path = SystemType.FTP_PATH_EMPLOYEE + medicalTestReportOld.getEmployee().getEmployeeNumber();
		path += "/Medical Test Reports";
		String url = medicalTestReportOld.getUrl();
		if (file != null) {
			if (url != null) {
				ftpService.deleteFile(url);
			}
			String ext = systemUtil.getExtensionFromFileName(file.getOriginalFilename());
			String filename = medicalTestReportOld.getEmployee().getEmployeeNumber() + "_MTR." + ext;
			url = ftpService.uploadFile(path, file, filename);
		} else {
			ftpService.deleteFile(url);
			url = null;
		}
		medicalTestReportOld.setUrl(url);
		return employeeDAO.updateMedicalTestReport(medicalTestReportOld);
	}

	@Override

	public HeavyVehicleExperience updateHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperienceNew) {
		Assert.notNull(heavyVehicleExperienceNew, "Heavy Vehicle Experience: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateHeavyVehicleExperience(heavyVehicleExperienceNew);

		HeavyVehicleExperience heavyVehicleExperienceOld = employeeDAO
				.findHeavyVehicleExperienceById(heavyVehicleExperienceNew.getHeavyVehicleExperienceId());
		heavyVehicleExperienceOld.setExperienceCompanyName(heavyVehicleExperienceNew.getExperienceCompanyName());
		heavyVehicleExperienceOld.setTrucksOperated(heavyVehicleExperienceNew.getTrucksOperated());
		heavyVehicleExperienceOld.setExperienceStartDate(heavyVehicleExperienceNew.getExperienceStartDate());
		heavyVehicleExperienceOld.setExperienceEndDate(heavyVehicleExperienceNew.getExperienceEndDate());
		heavyVehicleExperienceOld.setEmployee(heavyVehicleExperienceNew.getEmployee());
		return employeeDAO.updateHeavyVehicleExperience(heavyVehicleExperienceOld);
	}

	@Override

	public Reference updateReference(Reference referenceNew, MultipartFile cnic, MultipartFile referenceForm)
			throws IOException {
		Assert.notNull(referenceNew, "Reference: " + SystemType.MUST_NOT_BE_NULL);
		employeeValidator.validateReference(referenceNew);

		Reference referenceOld = employeeDAO.findReferenceById(referenceNew.getReferenceId());
		referenceOld.setReferenceName(referenceNew.getReferenceName());
		referenceOld.setReferenceCnic(referenceNew.getReferenceCnic());
		referenceOld.setReferenceAddress(referenceNew.getReferenceAddress());
		referenceOld.setReferenceContact(referenceNew.getReferenceContact());
		referenceOld.setReferenceRelation(referenceNew.getReferenceRelation());
		referenceOld.setCnicUrl(referenceNew.getCnicUrl());
		referenceOld.setReferenceFormUrl(referenceNew.getReferenceFormUrl());
		referenceOld.setEmployee(referenceNew.getEmployee());

		String cnicUrl = referenceOld.getCnicUrl();
		String referenceFormUrl = referenceOld.getReferenceFormUrl();
		String path = SystemType.FTP_PATH_EMPLOYEE + referenceOld.getEmployee().getEmployeeNumber();
		path += "/References";
		if (cnic != null) {
			if (cnicUrl != null) {
				ftpService.deleteFile(cnicUrl);
			}
			String ext = systemUtil.getExtensionFromFileName(cnic.getOriginalFilename());
			String filename = referenceOld.getEmployee().getEmployeeNumber() + "_REF_CNIC." + ext;
			cnicUrl = ftpService.uploadFile(path, cnic, filename);
		} else {
			ftpService.deleteFile(cnicUrl);
			cnicUrl = null;
		}
		if (referenceForm != null) {
			if (referenceFormUrl != null) {
				ftpService.deleteFile(referenceFormUrl);
			}
			String ext = systemUtil.getExtensionFromFileName(referenceForm.getOriginalFilename());
			String filename = referenceOld.getEmployee().getEmployeeNumber() + "_REF_FORM." + ext;
			referenceFormUrl = ftpService.uploadFile(path, referenceForm, filename);
		} else {
			ftpService.deleteFile(referenceFormUrl);
			referenceFormUrl = null;
		}
		referenceOld.setCnicUrl(cnicUrl);
		referenceOld.setReferenceFormUrl(referenceFormUrl);
		return employeeDAO.updateReference(referenceOld);
	}

	@Override
	public List<TrainingProfile> getAllTrainingProfile(String employeeId) {
		return findEmployeeById(employeeId).getTrainingProfiles();
	}

	@Override
	public List<MedicalTestReport> getAllMedicalTestReport(String employeeId) {
		return findEmployeeById(employeeId).getMedicalTestReports();
	}

	@Override
	public List<HeavyVehicleExperience> getAllHeavyVehicleExperience(String employeeId) {
		return findEmployeeById(employeeId).getHeavyVehicleExperience();
	}

	@Override
	public List<Reference> getAllReference(String employeeId) {
		return findEmployeeById(employeeId).getReferences();
	}

	@Override
	public Employee updateStatusEmployee(String employeeId, String availabilityNew, String movingStatusNew) {
		Employee employee = findEmployeeById(employeeId);
		if (availabilityNew != null)
			employee.setAvailability(availabilityNew);
		if (movingStatusNew != null)
			employee.setMovingStatus(movingStatusNew);
		return employeeDAO.updateEmployee(employee);
	}

	@Override
	public void deleteTrainingProfile(String tpId) {
		TrainingProfile tp = employeeDAO.findTrainingProfileById(tpId);
		employeeDAO.deleteTrainingProfileById(tpId);
		ftpService.deleteFile(tp.getTrainingCertificateUrl());
		ftpService.deleteFile(tp.getAttendanceSheetUrl());
		ftpService.deleteFile(tp.getPictureWithTrainerUrl());
	}

	@Override
	public void deleteMedicalTestReport(String mtrId) {
		MedicalTestReport mtr = employeeDAO.findMedicalTestReportById(mtrId);
		employeeDAO.deleteMedicalTestReportById(mtrId);
		ftpService.deleteFile(mtr.getUrl());
	}

	@Override
	public void deleteHeavyVehicleExperience(String hveId) {
		employeeDAO.deleteHeavyVehicleExperienceById(hveId);
	}

	@Override
	public void deleteReference(String refId) {
		Reference reference = employeeDAO.findReferenceById(refId);
		employeeDAO.deleteReferenceById(refId);
		ftpService.deleteFile(reference.getCnicUrl());
		ftpService.deleteFile(reference.getReferenceFormUrl());
	}

	@Override
	public List<Employee> addAllEmployees(List<Employee> employees) throws Exception {
		Assert.notNull(employees, "Employees: " + SystemType.MUST_NOT_BE_NULL);
		if (employees.size() > 0) {
			for (Employee employee : employees) {
				employee = addEmployee(employee);
			}
		}
		return employees;
	}

	@Override
	public long countEmployeeByPositionAndStatus(DPosition position, String status) {
		return employeeDAO.countEmployeeByPositionAndStatus(position, status);
	}

}
