package com.fleetX.service.implementation;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import com.fleetX.config.SystemType;
import com.fleetX.model.MyFTPClient;
import com.fleetX.service.IFTPService;

@Service
@Transactional
public class FTPService implements IFTPService {

	@Autowired
	MyFTPClient myFtpClient;

	@Override
	public String uploadFile(String path, MultipartFile file, String filename) throws IOException {
		FTPClient ftpClient;
		try {
			ftpClient = myFtpClient.getFTPClient();
		} catch (Exception e) {
			throw new IOException(SystemType.FTP_UPLOAD_FAILED);
		}
		String[] directory = path.split("/");
		if (directory != null && directory.length > 0) {
			for (String folder : directory) {
				if (!ftpClient.changeWorkingDirectory(folder)) {
					if (ftpClient.makeDirectory(folder)) {
						ftpClient.changeWorkingDirectory(folder);
					}
				}
			}
		} else
			throw new IllegalArgumentException("File Path: " + SystemType.INVALID_DATA);
		if (filename == null)
			filename = file.getOriginalFilename();
		InputStream fileInputStream = file.getInputStream();
		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
		ftpClient.enterLocalPassiveMode();
		boolean response = ftpClient.storeFile(filename, fileInputStream);
		ftpClient.logout();
		if (!response)
			throw new IOException(SystemType.FTP_UPLOAD_FAILED);
		else
			return path + "/" + filename;
	}

	public byte[] getFile(String path) throws IOException {
		Assert.notNull(path, "File Path: " + SystemType.MUST_NOT_BE_NULL);
		FTPClient ftpClient;
		try {
			ftpClient = myFtpClient.getFTPClient();
		} catch (Exception e) {
			throw new IOException(SystemType.FTP_UPLOAD_FAILED);
		}
		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
		InputStream fileStream = ftpClient.retrieveFileStream(path);

		if (fileStream == null)
			throw new NoSuchElementException("File: " + SystemType.NO_SUCH_ELEMENT);
		byte[] fileByte = fileStream.readAllBytes();
		fileStream.close();
		return fileByte;

	}

	@Override
	public boolean deleteFile(String path) {
		try {
			FTPClient ftpClient = myFtpClient.getFTPClient();
			return ftpClient.deleteFile(path);
		} catch (Exception e) {
			return false;
		}
	}

}
