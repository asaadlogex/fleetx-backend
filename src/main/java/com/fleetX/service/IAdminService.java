package com.fleetX.service;

import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;

public interface IAdminService {
	public AdminCompany addAdminCompany(AdminCompany adminCompany) throws Exception;
	public List<AdminCompany> findAllAdminCompany();
	public AdminCompany findAdminCompanyById(String adminCompanyId);
	public List<AdminCompany> filterAdminCompanies(Example<AdminCompany> exampleAC);
	public AdminCompany updateAdminCompany(AdminCompany adminCompany) throws Exception;
	public void deleteAdminCompany(String adminCompanyId) throws Exception;
	
	public FuelCard addFuelCard(FuelCard fuelCard);
	public List<FuelCard> getAllFuelCard();
	public List<FuelCard> filterFuelCard(String customerId,String acId,String status);
	public FuelCard updateFuelCard(FuelCard fuelCard);
	public void deleteFuelCardById(Integer fuelCardId);
	public FuelCard findFuelCardById(Integer fuelCardId);
	
	public Route addRoute(Route route);
	public List<Route> addAllRoute(List<Route> routes);
	public List<Route> getAllInterCityRoute();
	public List<Route> getAllIntraCityRoute();
	public List<Route> filterRoute(String to,String from);
	public Route updateRoute(Route route);
	public void deleteRouteById(String routeId);
	public Route findRouteById(String routeId);
	
	public Role addRole(Role role);
	public Role updateRole(Role role);
	public List<Role> getAllRole();
	public Role getRoleByRoleName(String roleName);
	public void deleteRoleByRoleName(String roleName);
	public void deleteAllRole();
	public List<String> getPrivilegesByRoleName(String roleName);	
	
	public User addUser(User user);
	public User updateUser(User user);
	public List<User> getAllUser();
	public User getUserByUsername(String username);
	public void deleteUserByUsername(String username);
	public void deleteAllUser();
	public List<String> getAdditionalPrivilegesByUsername(String username);
	public List<String> getRoleNamesByUsername(String username);
	
	public List<Address> adjustAddresses(List<Address> addressesOld, List<Address> addressesNew);
	public List<Contact> adjustContacts(List<Contact> contactsOld, List<Contact> contactsNew);
	
	public List<Route> getIntraCityRoutesByCity(String cityCode);
}
