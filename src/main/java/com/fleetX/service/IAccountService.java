package com.fleetX.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.fleetX.dto.InvoiceStatsDTO;
import com.fleetX.dto.TrialBalanceDTO;
import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.entity.dropdown.DPaymentStatus;

public interface IAccountService {
	public TrialBalanceDTO getTrialBalance(Date start,Date end,Boolean isAdjusted);
	
	public Adjustment addAdjustmentEntry(Adjustment adjustment);
	public List<Adjustment> getAdjustments();
	public void deleteAdjustmentById(Integer id);
	public List<Adjustment> filterAdjustment(String accountHead, Date start, Date end);

	public Invoice generatePerTripInvoice(List<String> rwbIds) throws ParseException;
	public Invoice generatePerTonInvoice(List<String> rwbIds) throws ParseException;
	public Invoice generateDedicatedInvoice(List<String> rwbIds) throws ParseException;
	public Invoice generateFixedInvoice(String customerId,Date date);
	
	public List<Invoice> getAllInvoice();
	public Invoice getInvoiceById(Integer invoiceId);
	public void deleteInvoiceById(Integer id);
	public List<Invoice> filterInvoice(String companyId, String invoiceType, Boolean isFixed, String paymentStatus, Date startDate, Date endDate, Integer month,
			Integer year);
	
	public List<DHInvoice> getAllDHInvoice();
	public DHInvoice getDHInvoiceById(Integer invoiceId);
	public void deleteDHInvoiceById(Integer id);

	public List<InvoiceStatsDTO> getInvoiceStats(String invoiceStatus);
	
	public ReimbursementInfo addReimbursement(ReimbursementInfo reimbursementInfo);
	public List<ReimbursementInfo> getAllReimbursement();
	public ReimbursementInfo getReimbursementById(Integer id);

	public InvoicePayment receiveInvoice(InvoicePayment invoicePayment);
	public InvoicePayment receiveDHInvoice(InvoicePayment invoicePayment);
	
	public List<InvoicePayment> getInvoicePayments(Integer invoiceId);
	public List<InvoicePayment> getDHInvoicePayments(Integer invoiceId);

	public long countInvoiceByPaymentStatus(DPaymentStatus status);

}
