package com.fleetX.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RentalVehicle;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;

public interface IOrderDAO {
	public Job createJob(Job job);
	public Job updateJob(Job job);
	public List<Job> getAllJob();
	public Job getJobById(String jobId);
	public List<Job> filterJob(Example<Job> jobExample);
	public void deleteJobById(String jobId);
	
	public RoadwayBill createRoadwayBill(RoadwayBill roadwayBill);
	public RoadwayBill updateRoadwayBill(RoadwayBill roadwayBill);
	public List<RoadwayBill> getAllRoadwayBill();
	public RoadwayBill getRoadwayBillById(String rwbId);
	public List<RoadwayBill> filterRoadwayBill(Example<RoadwayBill> rwbExample);
	public void deleteRoadwayBillById(String rwbId);
	
	public Stop addStop(Stop stop);
	public Stop updateStop(Stop stop);
	public List<Stop> getAllStop();
	public Stop getStopById(String stopId);
	public List<Stop> filterStop(Example<Stop> stopExample);
	public void deleteStopById(String stopId);
	
	public StopProgress addStopProgress(StopProgress stopProgress);
	public StopProgress updateStopProgress(StopProgress stopProgress);
	public List<StopProgress> getAllStopProgress();
	public StopProgress getStopProgressById(Integer stopProgressId);
	public List<StopProgress> filterStopProgress(Example<StopProgress> stopProgressExample);
	public void deleteStopProgressById(Integer stopProgressId);
	
	public Product addProduct(Product product);
	public Product updateProduct(Product product);
	public List<Product> getAllProduct();
	public Product getProductById(Integer productId);
	public List<Product> filterProduct(Example<Product> productExample);
	public void deleteProductById(Integer productId);
	
	public Income addIncome(Income income);
	public Income updateIncome(Income income);
	public List<Income> getAllIncome();
	public Income getIncomeById(Integer incomeId);
	public List<Income> filterIncome(Example<Income> incomeExample);
	public void deleteIncomeById(Integer incomeId);
	
	public Expense addExpense(Expense expense);
	public Expense updateExpense(Expense expense);
	public List<Expense> getAllExpense();
	public Expense getExpenseById(Integer expenseId);
	public List<Expense> filterExpense(Example<Expense> expenseExample);
	public void deleteExpenseById(Integer expenseId);
	
	public JobFuelDetail addJobFuelDetail(JobFuelDetail jobFuelDetail);
	public JobFuelDetail updateJobFuelDetail(JobFuelDetail jobFuelDetail);
	public List<JobFuelDetail> getAllJobFuelDetail();
	public JobFuelDetail getJobFuelDetailById(Integer jobFuelDetailId);
	public List<JobFuelDetail> filterJobFuelDetail(Example<JobFuelDetail> jobFuelDetailExample);
	public void deleteJobFuelDetailById(Integer jobFuelDetailId);
	
	public RentalVehicle addRentalVehicle(RentalVehicle rentalVehicle);
	public RentalVehicle getRentalVehicleById(Integer rentalVehicleId);
	public RentalVehicle updateRentalVehicle(RentalVehicle rvNew);
	public void removeJobFuelDetail(List<JobFuelDetail> jobFuelDetailTBD);
	public void removeRoadwayBill(List<RoadwayBill> roadwayBillTBD);
	public void removeStopProgress(List<StopProgress> stopProgressesTBD);
	public void removeProduct(List<Product> productsTBD);
	public void removeStop(List<Stop> stopsTBD);
	public void removeIncome(List<Income> incomeTBD);
	public void removeExpense(List<Expense> expenseTBD);
	public List<RoadwayBill> findRoadwayBillsByIdAndTractor(String rwbId, String tractorNumber);
	public List<RoadwayBill> findRoadwayBillsByIdAndRentalVehicle(String rwbId, String tractorNumber);
	
	public long countJobByStatus(DJobStatus status);
	public long countRoadwayBillByStatus(DRWBStatus status);
	
	public List<Job> getClosedJobInRange(Date start, Date end);
}
