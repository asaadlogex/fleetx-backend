package com.fleetX.dao;

import java.util.Date;
import java.util.List;

import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.FixedLineItem;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.LineItem;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;

public interface IAccountDAO {
	public Adjustment addAdjustmentEntry(Adjustment adjustment);
	public List<Adjustment> getAllAdjustments();
	public void deleteAdjustmentById(Integer id);
	public List<Adjustment> filterAdjustment(String accountHead,Date start,Date end);
	
	public Invoice addInvoice(Invoice invoice);
	public List<Invoice> getAlIInvoice();
	public List<Invoice> filterInvoice(Company customer, DInvoiceType invoice,DPaymentStatus paymentStatus, Date startDate, Date endDate, Integer month, Integer year);
	public Invoice getInvoiceById(Integer invoiceId);
	public Invoice updateInvoice(Invoice invoiceNew);
	public void deleteInvoiceById(Integer invoiceId);
	
	public DHInvoice addDHInvoice(DHInvoice invoice);
	public List<DHInvoice> getAlIDHInvoice();
	public DHInvoice getDHInvoiceById(Integer invoiceId);
	public DHInvoice updateDHInvoice(DHInvoice invoiceNew);
	public void deleteDHInvoiceById(Integer invoiceId);
	
	public LineItem addLineItem(LineItem lineItem);
	public FixedLineItem addFixedLineItem(FixedLineItem lineItem);
//	public List<Invoice> filterInvoice();
	
	public ReimbursementInfo addReimbursementInfo(ReimbursementInfo reimbursementInfo);
	public List<ReimbursementInfo> getAllReimbursementInfo();
	public ReimbursementInfo getReimbursementInfoById(Integer id);
	
	public InvoicePayment addInvoicePayment(InvoicePayment invoicePayment);
	
	public long countInvoiceByPaymentStatus(DPaymentStatus status);
	
	
}
