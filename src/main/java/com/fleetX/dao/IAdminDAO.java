package com.fleetX.dao;

import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;

public interface IAdminDAO {
	public AdminCompany createAdminCompany(AdminCompany adminCompany) throws Exception;
	public void deleteAdminCompany(String adminCompanyId);
	public AdminCompany updateAdminCompany(AdminCompany companyNew);
	public List<AdminCompany> findAllAdminCompany();
	public AdminCompany findAdminCompanyById(String adminCompanyId);
	public List<AdminCompany> filterAdminCompany(Example<AdminCompany> adminCompanyExample);

	public Route addRoute(Route route);
	public List<Route> getAllRoute();
	public List<Route> filterRoute(Example<Route> routeExample);
	public Route updateRoute(Route route);
	public void deleteRouteById(String routeId);
	public Route findRouteById(String routeId);
	
	public FuelCard addFuelCard(FuelCard fuelCard);
	public List<FuelCard> getAllFuelCard();
	public List<FuelCard> filterFuelCard(Example<FuelCard> fuelCardExample);
	public FuelCard updateFuelCard(FuelCard fuelCard);
	public void deleteFuelCardById(Integer fuelCardId);
	public FuelCard findFuelCardById(Integer fuelCardId);
	
	public Role addRole(Role role);
	public Role updateRole(Role role);
	public List<Role> getAllRole();
	public Role getRoleByRoleName(String roleName);
	public void deleteRoleByRoleName(String roleName);
	public void deleteAllRole();
	public List<String> getPrivilegesByRoleName(String roleName);	
	
	public User addUser(User user);
	public User updateUser(User user);
	public List<User> getAllUser();
	public User getUserByUsername(String username);
	public void deleteUserByUsername(String username);
	public void deleteAllUser();
	public List<String> getRoleNamesByUsername(String username);
	public List<String> getAdditionalPrivilegesByUsername(String username);
}
