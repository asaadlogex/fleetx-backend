package com.fleetX.dao;

import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.order.Warehouse;

public interface ICompanyDAO{
	public Company createCompany(Company company)throws Exception;
	public List<Company> findAllCompany();
	public Company findCompanyById(String companyId);
	public Company updateCompany(Company company);	
	public void deleteCompany(String companyId);	
	public List<Company> filterCompanyByExample(Example<Company> companyExample);
	
	public Warehouse addWarehouse(Warehouse warehouse);
	public Warehouse updateWarehouse(Warehouse warehouse);
	public List<Warehouse> getAllWarehouse();
	public Warehouse getWarehouseById(Integer warehouseId);
	public List<Warehouse> filterWarehouse(Example<Warehouse> warehouseExample);
	public void deleteWarehouseById(Integer warehouseId);
}
