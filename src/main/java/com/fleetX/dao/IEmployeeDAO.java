package com.fleetX.dao;

import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;

public interface IEmployeeDAO {
	public Employee createEmployee(Employee employee);
	public Employee updateEmployee(Employee employee);
	public Employee findEmployeeById(String employeeId);
	public void deleteEmployee(String employeeId);
	public List<Employee> findAllEmployees();
	public List<Employee> filterEmployeeByExample(Example<Employee> employeeExample);

	public EmployeeContract addEmployeeContract(EmployeeContract contractNew);
	public EmployeeContract updateEmployeeContract(EmployeeContract employeeContract); 
	public void deleteEmployeeContracts(List<EmployeeContract> employeeContracts); 
	
	public TrainingProfile addTrainingProfile(TrainingProfile trainingProfile);
	public MedicalTestReport addMedicalTestReport(MedicalTestReport medicalTestReport);
	public HeavyVehicleExperience addHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience);
	public Reference addReference(Reference reference);
	
	public TrainingProfile findTrainingProfileById(String trainingProfileId);
	public MedicalTestReport findMedicalTestReportById(String medicalTestReportId);
	public HeavyVehicleExperience findHeavyVehicleExperienceById(String heavyVehicleExperienceId);
	public Reference findReferenceById(String referenceId);
	
	public TrainingProfile updateTrainingProfile(TrainingProfile trainingProfile);
	public MedicalTestReport updateMedicalTestReport(MedicalTestReport medicalTestReport);
	public HeavyVehicleExperience updateHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience);
	public Reference updateReference(Reference reference);
	
	public void deleteTrainingProfileById(String trainingProfileId);
	public void deleteMedicalTestReportById(String medicalTestReportId);
	public void deleteHeavyVehicleExperienceById(String heavyVehicleExperienceId);
	public void deleteReferenceById(String referenceId);
	
	public long countEmployeeByPositionAndStatus(DPosition position,String status);
}
