package com.fleetX.dao;

import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;

public interface IAssetDAO {
	public Asset createAsset(Asset asset);
	public Asset findAssetById(String assetId);
	public List<Asset> findAllAssets();
	public List<Asset> filterAssetByExample(Example<Asset> assetExample);
	public Asset updateAsset(Asset asset);
	public void deleteAsset(String assetId);
	
	public Container createContainer(Container Container);
	public Container findContainerById(String containerId);
	public List<Container> findAllContainers();
	public List<Container> filterContainerByExample(Example<Container> containerExample);
	public Container updateContainer(Container container);
	public void deleteContainer(String containerId);

	public Tracker createTracker(Tracker tracker);
	public Tracker findTrackerById(String trackerId);
	public List<Tracker> findAllTrackers();
	public List<Tracker> filterTrackerByExample(Example<Tracker> trackerExample);
	public Tracker updateTracker(Tracker tracker);
	public void deleteTracker(String trackerId);

	public Tractor createTractor(Tractor tractor);
	public Tractor findTractorById(String tractorId);
	public List<Tractor> findAllTractors();
	public List<Tractor> filterTractorByExample(Example<Tractor> tractorExample);
	public Tractor updateTractor(Tractor tractor);
	public void deleteTractor(String tractorId);

	public Trailer createTrailer(Trailer trailer);
	public Trailer findTrailerById(String trailerId);
	public List<Trailer> findAllTrailers();
	public List<Trailer> filterTrailerByExample(Example<Trailer> trailerExample);
	public Trailer updateTrailer(Trailer trailer);
	public void deleteTrailer(String trailerId);

	public Tyre createTyre(Tyre tyre);
	public Tyre findTyreById(String tyreId);
	public List<Tyre> findAllTyres();
	public List<Tyre> filterTyreByExample(Example<Tyre> tyreExample);
	public Tyre updateTyre(Tyre tyre);
	public void deleteTyre(String tyreId);
	
	public AssetCombination addAssetCombination(AssetCombination assetCombination);
	public AssetCombination updateAssetCombination(AssetCombination assetCombination);
	public List<AssetCombination> filterAssetCombinationByExample(Example<AssetCombination> assetCombinationExample);
	public List<AssetCombination> findAllAssetCombination();
	public AssetCombination findAssetCombinationById(String assetCombinationId);
	public void deleteAssetCombination(String assetCombinationId);
	
	public Tractor findTractorByNumberPlate(String tractorNumber);
	
	public long countTractorByStatus(String status);
	public long countTrailerByStatus(String status);
	public long countContainerByStatus(String status);
	public long countTrackerByStatus(String status);
	
}