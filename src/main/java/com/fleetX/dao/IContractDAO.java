package com.fleetX.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.dropdown.DContractType;

public interface IContractDAO {
	public CompanyContract addCompanyContract(CompanyContract companyContract);
	public List<CompanyContract> getAllCompanyContract();
	public List<CompanyContract> filterCompanyContract(Example<CompanyContract> companyContractExample);
	public CompanyContract updateCompanyContract(CompanyContract companyContract);
	public void deleteCompanyContractById(String companyContractId);
	public CompanyContract findCompanyContractById(String companyContractId);
	public List<CompanyContract> findCompanyContractByContractType(DContractType contractType);
	
	public FuelSupplierContract addFuelSupplierContract(FuelSupplierContract fuelSupplierContract);
	public List<FuelSupplierContract> getAllFuelSupplierContract();
	public List<FuelSupplierContract> filterFuelSupplierContract(Example<FuelSupplierContract> fuelSupplierContractExample);
	public FuelSupplierContract updateFuelSupplierContract(FuelSupplierContract fuelSupplierContract);
	public void deleteFuelSupplierContractById(String fuelSupplierContractId);
	public FuelSupplierContract findFuelSupplierContractById(String fuelSupplierContractId);
	
	public RouteRateContract addRouteRateContract(RouteRateContract routeRateContract);
	public List<RouteRateContract> getAllRouteRateContract();
	public List<RouteRateContract> filterRouteRateContract(Example<RouteRateContract> routeRateContractExample);
	public RouteRateContract updateRouteRateContract(RouteRateContract routeRateContract);
	public void deleteRouteRateContractById(Integer routeRateContractId);
	public RouteRateContract findRouteRateContractById(Integer routeRateContractId);
	
	public void removeAssetCombination(List<AssetCombination> assetCombinationsTBD);
	public void removeRouteRateContract(List<RouteRateContract> rrcTBD);
	public void removeFuelRates(List<FuelRate> fuelRatesTBD);
	
	public RouteRate addRouteRate(RouteRate routeRate);
	public RouteRate updateRouteRate(RouteRate routeRateOld);
	public RouteRate findRouteRateById(Integer routeRateId);
	public void removeRouteRate(List<RouteRate> routeRateTBD);
	
	public List<CompanyContract> getCustomerContract(String companyId, Date date);
	public CompanyContract getCustomerContractByDate(String companyId, Date date);
}
