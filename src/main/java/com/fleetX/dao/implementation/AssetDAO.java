package com.fleetX.dao.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IAssetDAO;
import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.repository.AssetCombinationRepository;
import com.fleetX.repository.AssetRepository;
import com.fleetX.repository.ContainerRepository;
import com.fleetX.repository.TrackerRepository;
import com.fleetX.repository.TractorRepository;
import com.fleetX.repository.TrailerRepository;
import com.fleetX.repository.TyreRepository;
import com.fleetX.service.IAdminService;

@Component
public class AssetDAO implements IAssetDAO {

	@Autowired
	AssetRepository assetRepository;
	@Autowired
	ContainerRepository containerRepository;
	@Autowired
	TrackerRepository trackerRepository;
	@Autowired
	TractorRepository tractorRepository;
	@Autowired
	TrailerRepository trailerRepository;
	@Autowired
	TyreRepository tyreRepository;
	@Autowired
	AssetCombinationRepository assetCombinationRepository;
	@Autowired
	IAdminService adminService;

	@Override
	public Asset createAsset(Asset asset) {
		Assert.notNull(asset, SystemType.MUST_NOT_BE_NULL);
		asset.setAssetId(null);
		List<Tyre> tyres = asset.getTyres();
		asset = assetRepository.save(asset);
		if (tyres != null && tyres.size() > 0)
			for (Tyre tyre : tyres) {
				Optional<Tyre> tyreInDB = tyreRepository.findById(tyre.getTyreId());
				if (tyreInDB.isEmpty()) {
					tyre.setTyreId(null);
					tyre.setAsset(asset);
					createTyre(tyre);
				} else {
					tyre = tyreInDB.get();
					if (tyre.getAsset() != null)
						throw new DataIntegrityViolationException("Tyre with tyre ID=\"" + tyre.getTyreId()
								+ "\" already associated with another asset.");
					tyre.setAsset(asset);
					tyreRepository.save(tyre);
				}
			}
		return asset;
	}

	@Override
	public Asset findAssetById(String assetId) {
		Assert.notNull(assetId, SystemType.MUST_NOT_BE_NULL);
		Optional<Asset> asset = assetRepository.findById(assetId);
		if (asset.isEmpty())
			throw new NoSuchElementException();
		return asset.get();
	}

	@Override

	public List<Asset> findAllAssets() {
		return assetRepository.findAll();
	}

	@Override

	public Asset updateAsset(Asset assetNew) {
		Assert.notNull(assetNew, SystemType.MUST_NOT_BE_NULL);
		Optional<Asset> assetInDB = assetRepository.findById(assetNew.getAssetId());
		if (assetInDB.isEmpty())
			throw new EntityNotFoundException();
		Asset assetOld = assetInDB.get();
		assetOld.setAssetMake(assetNew.getAssetMake());
		assetOld.setColor(assetNew.getColor());
		assetOld.setGrossWeight(assetNew.getGrossWeight());
		assetOld.setInduction(assetNew.getInduction());
		assetOld.setLeaseType(assetNew.getLeaseType());
		assetOld.setModel(assetNew.getModel());
		assetOld.setNumberOfTyres(assetNew.getNumberOfTyres());
		assetOld.setStatus(assetNew.getStatus());
		assetOld.setSupplier(assetNew.getSupplier());
		assetOld.setTyreSize(assetNew.getTyreSize());
		assetOld.setVin(assetNew.getVin());
		assetOld.setWeightUnit(assetNew.getWeightUnit());
		assetOld.setYear(assetNew.getYear());
		assetOld = assetRepository.save(assetOld);
		List<Tyre> tyresNew = assetNew.getTyres();
		List<Tyre> tyresOld = assetOld.getTyres();
		List<Tyre> tyresTBA = new ArrayList<>(tyresNew);
		List<Tyre> tyresTBD = new ArrayList<>(tyresOld);
		for (Tyre tyreNew : tyresNew) {
			for (Tyre tyreOld : tyresOld) {
				if (tyreNew.getTyreId() != null && tyreNew.getTyreId().equals(tyreOld.getTyreId())) {
					tyreOld.setLeaseType(tyreNew.getLeaseType());
					tyreOld.setMake(tyreNew.getMake());
					tyreOld.setStartingKm(tyreNew.getStartingKm());
					tyreOld.setStatus(tyreNew.getStatus());
					tyreOld.setSupplier(tyreNew.getSupplier());
					tyreOld.setTotalKm(tyreNew.getTotalKm());
					tyreOld.setTyreSerialNumber(tyreNew.getTyreSerialNumber());
					tyreOld.setAdminCompany(tyreNew.getAdminCompany());
					tyreRepository.save(tyreOld);
					tyresTBA.remove(tyreNew);
					tyresTBD.remove(tyreOld);
				}
			}
		}
		for (Tyre tyreNew : tyresTBA) {
			tyreNew.setTyreId(null);
			tyreNew.setAsset(assetOld);
			tyreRepository.save(tyreNew);
		}
		removeOldTyres(tyresTBD);
		return assetOld;
	}

	private void removeOldTyres(List<Tyre> tyres) {
		if (tyres != null) {
			for (Tyre tyre : tyres) {
				tyre.setAsset(null);
				tyre.setAvailability(SystemType.STATUS_AVAILABLE);
				tyre.setMovingStatus(SystemType.STATUS_MOVING_FREE);
			}
			tyreRepository.saveAll(tyres);
		}
	}

	@Override

	public void deleteAsset(String assetId) {
		Assert.notNull(assetId, SystemType.MUST_NOT_BE_NULL);
		Optional<Asset> assetInDB = assetRepository.findById(assetId);
		if (assetInDB.isEmpty())
			throw new NoSuchElementException();
		Asset asset = assetInDB.get();
		removeOldTyres(asset.getTyres());
		assetRepository.deleteById(assetId);
	}

	@Override

	public Container createContainer(Container container) {
		Assert.notNull(container, "Container: " + SystemType.MUST_NOT_BE_NULL);
		return containerRepository.save(container);
	}

	@Override

	public Container findContainerById(String containerId) {
		Assert.notNull(containerId, "Container ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Container> container = containerRepository.findById(containerId);
		if (container.isEmpty())
			throw new NoSuchElementException(containerId + ": " + SystemType.NO_SUCH_ELEMENT);
		return container.get();
	}

	@Override

	public List<Container> findAllContainers() {
		return containerRepository.findAll();
	}

	@Override

	public Container updateContainer(Container containerNew) {
		Assert.notNull(containerNew, "Container: " + SystemType.MUST_NOT_BE_NULL);
		return containerRepository.save(containerNew);
	}

	@Override

	public void deleteContainer(String containerId) {
		Assert.notNull(containerId, "Container ID: " + SystemType.MUST_NOT_BE_NULL);
		containerRepository.deleteById(containerId);
	}

	@Override

	public Tracker createTracker(Tracker tracker) {
		Assert.notNull(tracker, "Tracker: " + SystemType.MUST_NOT_BE_NULL);
		return trackerRepository.save(tracker);
	}

	@Override

	public Tracker findTrackerById(String trackerId) {
		Assert.notNull(trackerId, "Tracker ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Tracker> tracker = trackerRepository.findById(trackerId);
		if (tracker.isEmpty())
			throw new NoSuchElementException(trackerId + ": " + SystemType.NO_SUCH_ELEMENT);
		return tracker.get();
	}

	@Override

	public List<Tracker> findAllTrackers() {
		return trackerRepository.findAll();
	}

	@Override

	public Tracker updateTracker(Tracker trackerNew) {
		Assert.notNull(trackerNew, "Tracker: " + SystemType.MUST_NOT_BE_NULL);
		return trackerRepository.save(trackerNew);
	}

	@Override

	public void deleteTracker(String trackerId) {
		Assert.notNull(trackerId, "Tracker ID: " + SystemType.MUST_NOT_BE_NULL);
		trackerRepository.deleteById(trackerId);
	}

	@Override

	public Tractor createTractor(Tractor tractor) {
		Assert.notNull(tractor, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		return tractorRepository.save(tractor);
	}

	@Override

	public Tractor findTractorById(String tractorId) {
		Assert.notNull(tractorId, "Tractor ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Tractor> tractor = tractorRepository.findById(tractorId);
		if (tractor.isEmpty())
			throw new NoSuchElementException(tractorId + ": " + SystemType.NO_SUCH_ELEMENT);
		return tractor.get();
	}

	@Override

	public List<Tractor> findAllTractors() {
		return tractorRepository.findAll();
	}

	@Override

	public Tractor updateTractor(Tractor tractorNew) {
		Assert.notNull(tractorNew, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		return tractorRepository.save(tractorNew);
	}

	@Override

	public void deleteTractor(String tractorId) {
		Assert.notNull(tractorId, "Tractor ID: " + SystemType.MUST_NOT_BE_NULL);
		tractorRepository.deleteById(tractorId);
	}

	@Override

	public Trailer createTrailer(Trailer trailer) {
		Assert.notNull(trailer, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		return trailerRepository.save(trailer);
	}

	@Override

	public Trailer findTrailerById(String trailerId) {
		Assert.notNull(trailerId, "Trailer ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Trailer> trailer = trailerRepository.findById(trailerId);
		if (trailer.isEmpty())
			throw new NoSuchElementException(trailerId + ": " + SystemType.NO_SUCH_ELEMENT);
		return trailer.get();
	}

	@Override

	public List<Trailer> findAllTrailers() {
		return trailerRepository.findAll();
	}

	@Override

	public Trailer updateTrailer(Trailer trailerNew) {
		Assert.notNull(trailerNew, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		return trailerRepository.save(trailerNew);
	}

	@Override

	public void deleteTrailer(String trailerId) {
		Assert.notNull(trailerId, "Trailer ID: " + SystemType.MUST_NOT_BE_NULL);
		trailerRepository.deleteById(trailerId);
	}

	@Override

	public Tyre createTyre(Tyre tyre) {
		Assert.notNull(tyre, "Tyre: " + SystemType.MUST_NOT_BE_NULL);
		return tyreRepository.save(tyre);
	}

	@Override

	public Tyre findTyreById(String tyreId) {
		Assert.notNull(tyreId, "Tyre ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Tyre> tyreInDB = tyreRepository.findById(tyreId);
		if (tyreInDB.isEmpty())
			throw new NoSuchElementException(tyreId + ": " + SystemType.NO_SUCH_ELEMENT);
		return tyreInDB.get();
	}

	@Override

	public List<Tyre> findAllTyres() {
		return tyreRepository.findAll();
	}

	@Override

	public Tyre updateTyre(Tyre tyreNew) {
		Assert.notNull(tyreNew, "Tyre: " + SystemType.MUST_NOT_BE_NULL);
		return tyreRepository.save(tyreNew);
	}

	@Override

	public void deleteTyre(String tyreId) {
		Assert.notNull(tyreId, "Tyre ID: " + SystemType.MUST_NOT_BE_NULL);
		tyreRepository.deleteById(tyreId);
	}

	@Override

	public List<Asset> filterAssetByExample(Example<Asset> assetExample) {
		return assetRepository.findAll(assetExample);
	}

	@Override

	public List<Container> filterContainerByExample(Example<Container> containerExample) {
		return containerRepository.findAll(containerExample);
	}

	@Override

	public List<Tracker> filterTrackerByExample(Example<Tracker> trackerExample) {
		return trackerRepository.findAll(trackerExample);
	}

	@Override

	public List<Tractor> filterTractorByExample(Example<Tractor> tractorExample) {
		return tractorRepository.findAll(tractorExample);
	}

	@Override

	public List<Trailer> filterTrailerByExample(Example<Trailer> trailerExample) {
		return trailerRepository.findAll(trailerExample);
	}

	@Override

	public List<Tyre> filterTyreByExample(Example<Tyre> tyreExample) {
		return tyreRepository.findAll(tyreExample);
	}

	@Override

	public AssetCombination addAssetCombination(AssetCombination assetCombination) {
		Assert.notNull(assetCombination, "Asset Combination: " + SystemType.MUST_NOT_BE_NULL);
		return assetCombinationRepository.save(assetCombination);
	}

	@Override

	public AssetCombination updateAssetCombination(AssetCombination assetCombinationNew) {
		Assert.notNull(assetCombinationNew, "Asset Combination: " + SystemType.MUST_NOT_BE_NULL);
		return assetCombinationRepository.save(assetCombinationNew);
	}

	@Override
	public List<AssetCombination> filterAssetCombinationByExample(Example<AssetCombination> assetCombinationExample) {
		return assetCombinationRepository.findAll(assetCombinationExample);
	}

	@Override
	public List<AssetCombination> findAllAssetCombination() {
		return assetCombinationRepository.findAll();
	}

	@Override
	public AssetCombination findAssetCombinationById(String assetCombinationId) {
		Assert.notNull(assetCombinationId, "Asset Combination ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<AssetCombination> assetCombinationInDB = assetCombinationRepository.findById(assetCombinationId);
		if (assetCombinationInDB.isEmpty())
			throw new NoSuchElementException(assetCombinationId + ": " + SystemType.NO_SUCH_ELEMENT);
		return assetCombinationInDB.get();
	}

	@Override
	public void deleteAssetCombination(String assetCombinationId) {
		Assert.notNull(assetCombinationId, SystemType.MUST_NOT_BE_NULL);
		assetCombinationRepository.deleteById(assetCombinationId);
	}

	@Override
	public Tractor findTractorByNumberPlate(String tractorNumber) {
		return tractorRepository.findByNumberPlate(tractorNumber);
	}

	@Override
	public long countTractorByStatus(String status) {
		Tractor tractor = new Tractor();
		Asset asset = new Asset();
		asset.setStatus(status);
		tractor.setAsset(asset);
		return tractorRepository.count(Example.of(tractor));
	}

	@Override
	public long countTrailerByStatus(String status) {
		Trailer trailer = new Trailer();
		Asset asset = new Asset();
		asset.setStatus(status);
		trailer.setAsset(asset);
		return trailerRepository.count(Example.of(trailer));
	}

	@Override
	public long countContainerByStatus(String status) {
		Container container = new Container();
		container.setStatus(status);
		return containerRepository.count(Example.of(container));
	}

	@Override
	public long countTrackerByStatus(String status) {
		Tracker tracker = new Tracker();
		tracker.setStatus(status);
		return trackerRepository.count(Example.of(tracker));
	}

}
