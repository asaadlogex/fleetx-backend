package com.fleetX.dao.implementation;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IContractDAO;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.repository.AssetCombinationRepository;
import com.fleetX.repository.CompanyContractRepository;
import com.fleetX.repository.FuelRateRepository;
import com.fleetX.repository.FuelSupplierContractRepository;
import com.fleetX.repository.RouteRateContractRepository;
import com.fleetX.repository.RouteRateRepository;
import com.fleetX.service.IAssetService;

@Component
public class ContractDAO implements IContractDAO {

	@Autowired
	FuelSupplierContractRepository fuelSupplierContractRepository;
	@Autowired
	CompanyContractRepository companyContractRepository;
	@Autowired
	RouteRateContractRepository routeRateContractRepository;
	@Autowired
	FuelRateRepository fuelRateRepository;
	@Autowired
	AssetCombinationRepository assetCombinationRepository;
	@Autowired
	RouteRateRepository routeRateRepository;
	@Autowired
	IAssetService assetService;

	@Override
	public CompanyContract addCompanyContract(CompanyContract companyContract) {
		Assert.notNull(companyContract, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		return companyContractRepository.save(companyContract);
	}

	@Override
	public List<CompanyContract> getAllCompanyContract() {
		return companyContractRepository.findAll();
	}

	@Override
	public List<CompanyContract> filterCompanyContract(Example<CompanyContract> companyContractExample) {
		return companyContractRepository.findAll(companyContractExample);
	}

	@Override
	public CompanyContract updateCompanyContract(CompanyContract companyContractNew) {
		Assert.notNull(companyContractNew, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		return companyContractRepository.save(companyContractNew);
	}

	@Override
	public void removeRouteRateContract(List<RouteRateContract> rrcTBD) {
		routeRateContractRepository.deleteAll(rrcTBD);
	}

	@Override
	public void deleteCompanyContractById(String companyContractId) {
		Assert.notNull(companyContractId, "Company Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		companyContractRepository.deleteById(companyContractId);
	}

	@Override
	public CompanyContract findCompanyContractById(String companyContractId) {
		Assert.notNull(companyContractId, "Company Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<CompanyContract> companyContractInDB = companyContractRepository.findById(companyContractId);
		if (companyContractInDB.isEmpty())
			throw new NoSuchElementException(companyContractId + ": " + SystemType.NO_SUCH_ELEMENT);
		return companyContractInDB.get();
	}

	@Override

	public FuelSupplierContract addFuelSupplierContract(FuelSupplierContract fuelSupplierContract) {
		Assert.notNull(fuelSupplierContract, "Fuel Supplier Contract: " + SystemType.MUST_NOT_BE_NULL);
		return fuelSupplierContractRepository.save(fuelSupplierContract);
	}

	@Override
	public List<FuelSupplierContract> getAllFuelSupplierContract() {
		return fuelSupplierContractRepository.findAll();
	}

	@Override
	public List<FuelSupplierContract> filterFuelSupplierContract(
			Example<FuelSupplierContract> fuelSupplierContractExample) {
		return fuelSupplierContractRepository.findAll(fuelSupplierContractExample);
	}

	@Override
	public FuelSupplierContract updateFuelSupplierContract(FuelSupplierContract fscNew) {
		Assert.notNull(fscNew, "Fuel Supplier Contract: " + SystemType.MUST_NOT_BE_NULL);
		return fuelSupplierContractRepository.save(fscNew);
	}

	@Override
	public void removeFuelRates(List<FuelRate> fuelRatesTBD) {
		fuelRateRepository.deleteAll(fuelRatesTBD);
	}

	@Override
	public void deleteFuelSupplierContractById(String fuelSupplierContractId) {
		Assert.notNull(fuelSupplierContractId, "Fuel Supplier Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		FuelSupplierContract fsc = findFuelSupplierContractById(fuelSupplierContractId);
		fsc.setFscStatus(SystemType.STATUS_INACTIVE);
		fuelSupplierContractRepository.save(fsc);
	}

	@Override
	public FuelSupplierContract findFuelSupplierContractById(String fuelSupplierContractId) {
		Assert.notNull(fuelSupplierContractId, "Fuel Supplier Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<FuelSupplierContract> fuelSupplierContract = fuelSupplierContractRepository
				.findById(fuelSupplierContractId);
		if (fuelSupplierContract.isEmpty())
			throw new NoSuchElementException(fuelSupplierContractId + ": " + SystemType.NO_SUCH_ELEMENT);
		return fuelSupplierContract.get();
	}

	public void removeAssetCombination(List<AssetCombination> assetCombinations) {
		Assert.notNull(assetCombinations, SystemType.MUST_NOT_BE_NULL);
		for (AssetCombination assetCombination : assetCombinations) {
			assetCombination.setAvailability(SystemType.STATUS_AVAILABLE);
			assetCombination.setCompanyContract(null);
			assetCombinationRepository.save(assetCombination);
		}
	}

	@Override
	public RouteRateContract addRouteRateContract(RouteRateContract routeRateContract) {
		Assert.notNull(routeRateContract, "Route Rate Contract: " + SystemType.MUST_NOT_BE_NULL);
		return routeRateContractRepository.save(routeRateContract);
	}

	@Override
	public List<RouteRateContract> getAllRouteRateContract() {
		return routeRateContractRepository.findAll();
	}

	@Override
	public List<RouteRateContract> filterRouteRateContract(Example<RouteRateContract> routeRateContractExample) {
		return routeRateContractRepository.findAll(routeRateContractExample);
	}

	@Override
	public RouteRateContract updateRouteRateContract(RouteRateContract routeRateContract) {
		Assert.notNull(routeRateContract, "Route Rate Contract: " + SystemType.MUST_NOT_BE_NULL);
		return routeRateContractRepository.save(routeRateContract);
	}

	@Override
	public void deleteRouteRateContractById(Integer routeRateContractId) {
		Assert.notNull(routeRateContractId, "Route Rate Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		routeRateContractRepository.deleteById(routeRateContractId);

	}

	@Override
	public RouteRateContract findRouteRateContractById(Integer routeRateContractId) {
		Assert.notNull(routeRateContractId, "Route Rate Contract ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<RouteRateContract> rrc = routeRateContractRepository.findById(routeRateContractId);
		if (rrc.isEmpty())
			throw new NoSuchElementException(routeRateContractId + ": " + SystemType.NO_SUCH_ELEMENT);
		return rrc.get();
	}

	@Override
	public RouteRate addRouteRate(RouteRate routeRate) {
		Assert.notNull(routeRate, "Route Rate: " + SystemType.MUST_NOT_BE_NULL);
		return routeRateRepository.save(routeRate);
	}

	@Override
	public RouteRate updateRouteRate(RouteRate routeRateOld) {
		Assert.notNull(routeRateOld, "Route Rate: " + SystemType.MUST_NOT_BE_NULL);
		return routeRateRepository.save(routeRateOld);
	}

	@Override
	public RouteRate findRouteRateById(Integer routeRateId) {
		Assert.notNull(routeRateId, "Route Rate ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<RouteRate> routeRate = routeRateRepository.findById(routeRateId);
		if (routeRate.isEmpty())
			throw new NoSuchElementException(routeRateId + ": " + SystemType.NO_SUCH_ELEMENT);
		return routeRate.get();
	}

	@Override
	public void removeRouteRate(List<RouteRate> routeRateTBD) {
		routeRateRepository.deleteAll(routeRateTBD);
	}

	@Override
	public List<CompanyContract> getCustomerContract(String companyId, Date date) {
		return companyContractRepository.filterContract(companyId, date);
	}

	@Override
	public CompanyContract getCustomerContractByDate(String companyId, Date date) {
		List<CompanyContract> contracts = companyContractRepository.filterContract(companyId, date);
		if (contracts != null && contracts.size() > 0)
			return contracts.get(0);
		else
			return null;
	}

	@Override
	public List<CompanyContract> findCompanyContractByContractType(DContractType contractType) {
		return companyContractRepository.findAllByContractType(contractType);
	}

}
