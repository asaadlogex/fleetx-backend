package com.fleetX.dao.implementation;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.IOrderDAO;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RentalVehicle;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.repository.ExpenseRepository;
import com.fleetX.repository.IncomeRepository;
import com.fleetX.repository.JobFuelDetailRepository;
import com.fleetX.repository.JobRepository;
import com.fleetX.repository.ProductRepository;
import com.fleetX.repository.RentalVehicleRepository;
import com.fleetX.repository.RoadwayBillRepository;
import com.fleetX.repository.StopProgressRepository;
import com.fleetX.repository.StopRepository;
import com.fleetX.repository.WarehouseRepository;
import com.fleetX.service.IAssetService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IEmployeeService;

@Component

public class OrderDAO implements IOrderDAO {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	JobRepository jobRepository;
	@Autowired
	JobFuelDetailRepository jfdRepository;
	@Autowired
	RentalVehicleRepository rentalVehicleRepository;
	@Autowired
	RoadwayBillRepository rwbRepository;
	@Autowired
	StopProgressRepository stopProgressRepository;
	@Autowired
	StopRepository stopRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	ExpenseRepository expenseRepository;
	@Autowired
	IncomeRepository incomeRepository;
	@Autowired
	WarehouseRepository warehouseRepository;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IAssetService assetService;

	@Override
	public Job createJob(Job job) {
		Assert.notNull(job, "Job: " + SystemType.MUST_NOT_BE_NULL);
		return jobRepository.save(job);
	}

	@Override
	public JobFuelDetail addJobFuelDetail(JobFuelDetail jfd) {
		Assert.notNull(jfd, "Job Fuel Detail: " + SystemType.MUST_NOT_BE_NULL);
		return jfdRepository.save(jfd);
	}

	@Override
	public Job updateJob(Job jobNew) {
		Assert.notNull(jobNew, "Job: " + SystemType.MUST_NOT_BE_NULL);
		return jobRepository.save(jobNew);
	}

	public JobFuelDetail updateJobFuelDetail(JobFuelDetail jfdNew) {
		Assert.notNull(jfdNew, "Job Fuel Detail: " + SystemType.MUST_NOT_BE_NULL);
		return jfdRepository.save(jfdNew);

	}

	public void removeRoadwayBill(List<RoadwayBill> roadwayBillTBD) {
		Assert.notNull(roadwayBillTBD, SystemType.MUST_NOT_BE_NULL);
		rwbRepository.deleteAll(roadwayBillTBD);
	}

	public void removeJobFuelDetail(List<JobFuelDetail> jobFuelDetails) {
		Assert.notNull(jobFuelDetails, SystemType.MUST_NOT_BE_NULL);
		jfdRepository.deleteAll(jobFuelDetails);
	}

	public RentalVehicle updateRentalVehicle(RentalVehicle rvNew) {
		Assert.notNull(rvNew, "Rental Vehicle: " + SystemType.MUST_NOT_BE_NULL);
		return rentalVehicleRepository.save(rvNew);
	}

	@Override

	public List<Job> getAllJob() {
		return jobRepository.findAll();
	}

	@Override

	public Job getJobById(String jobId) {
		Assert.notNull(jobId, "Job ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Job> job = jobRepository.findById(jobId);
		if (job.isEmpty())
			throw new NoSuchElementException(jobId + ": " + SystemType.NO_SUCH_ELEMENT);
		return job.get();
	}

	@Override

	public List<Job> filterJob(Example<Job> jobExample) {
		return jobRepository.findAll(jobExample);
	}

	@Override

	public void deleteJobById(String jobId) {
		Assert.notNull(jobId, "Job ID: " + SystemType.MUST_NOT_BE_NULL);
		jobRepository.deleteById(jobId);
	}

	@Override

	public RoadwayBill createRoadwayBill(RoadwayBill roadwayBill) {
		Assert.notNull(roadwayBill, "Roadway Bill: " + SystemType.MUST_NOT_BE_NULL);
		return rwbRepository.save(roadwayBill);
	}

	public StopProgress addStopProgress(StopProgress stopProgress) {
		Assert.notNull(stopProgress, "Stop Progress: " + SystemType.MUST_NOT_BE_NULL);
		return stopProgressRepository.save(stopProgress);
	}

	public Stop addStop(Stop stop) {
		Assert.notNull(stop, SystemType.MUST_NOT_BE_NULL);
		return stopRepository.save(stop);
	}

	public Product addProduct(Product product) {
		Assert.notNull(product, SystemType.MUST_NOT_BE_NULL);
		return productRepository.save(product);
	}

	public Expense addExpense(Expense expense) {
		Assert.notNull(expense, "Expense: " + SystemType.MUST_NOT_BE_NULL);
		return expenseRepository.save(expense);
	}

	public Income addIncome(Income income) {
		Assert.notNull(income, "Income: " + SystemType.MUST_NOT_BE_NULL);
		return incomeRepository.save(income);
	}

	@Override

	public RoadwayBill updateRoadwayBill(RoadwayBill rwbNew) {
		Assert.notNull(rwbNew, SystemType.MUST_NOT_BE_NULL);
		return rwbRepository.save(rwbNew);
	}

	public void removeStopProgress(List<StopProgress> stopProgressesTBD) {
		Assert.notNull(stopProgressesTBD, SystemType.MUST_NOT_BE_NULL);
		stopProgressRepository.deleteAll(stopProgressesTBD);
	}

	public void removeStop(List<Stop> stopsTBD) {
		Assert.notNull(stopsTBD, SystemType.MUST_NOT_BE_NULL);
		stopRepository.deleteAll(stopsTBD);
	}

	public Stop updateStop(Stop stopNew) {
		Assert.notNull(stopNew, "Stop: " + SystemType.MUST_NOT_BE_NULL);
		return stopRepository.save(stopNew);
	}

	public StopProgress updateStopProgress(StopProgress stopProgressNew) {
		Assert.notNull(stopProgressNew, "Stop Progress: " + SystemType.MUST_NOT_BE_NULL);
		return stopProgressRepository.saveAndFlush(stopProgressNew);
	}

	public void removeProduct(List<Product> productsTBD) {
		Assert.notNull(productsTBD, SystemType.MUST_NOT_BE_NULL);
		productRepository.deleteAll(productsTBD);
	}

	public Product updateProduct(Product productNew) {
		Assert.notNull(productNew, "Product: " + SystemType.MUST_NOT_BE_NULL);
		return productRepository.save(productNew);
	}

	public void removeExpense(List<Expense> expenseTBD) {
		Assert.notNull(expenseTBD, SystemType.MUST_NOT_BE_NULL);
		expenseRepository.deleteAll(expenseTBD);
	}

	public void removeIncome(List<Income> incomeTBD) {
		Assert.notNull(incomeTBD, SystemType.MUST_NOT_BE_NULL);
		incomeRepository.deleteAll(incomeTBD);
	}

	@Override

	public List<RoadwayBill> getAllRoadwayBill() {
		return rwbRepository.findAll();
	}

	@Override

	public RoadwayBill getRoadwayBillById(String rwbId) {
		Assert.notNull(rwbId, SystemType.MUST_NOT_BE_NULL);
		Optional<RoadwayBill> rwb = rwbRepository.findById(rwbId);
		if (rwb.isEmpty())
			throw new NoSuchElementException(rwbId + ": " + SystemType.NO_SUCH_ELEMENT);
		return rwb.get();
	}

	@Override

	public List<RoadwayBill> filterRoadwayBill(Example<RoadwayBill> rwbExample) {
		Assert.notNull(rwbExample, SystemType.MUST_NOT_BE_NULL);
		return rwbRepository.findAll(rwbExample);
	}

	@Override

	public void deleteRoadwayBillById(String rwbId) {
		Assert.notNull(rwbId, SystemType.MUST_NOT_BE_NULL);
		rwbRepository.deleteById(rwbId);
	}

	@Override
	public RentalVehicle addRentalVehicle(RentalVehicle rentalVehicle) {
		Assert.notNull(rentalVehicle, SystemType.MUST_NOT_BE_NULL);
		return rentalVehicleRepository.save(rentalVehicle);
	}

	@Override
	public JobFuelDetail getJobFuelDetailById(Integer jobFuelDetailId) {
		Assert.notNull(jobFuelDetailId, "Job Fuel Detail ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<JobFuelDetail> jfdInDB = jfdRepository.findById(jobFuelDetailId);
		if (jfdInDB.isEmpty())
			throw new NoSuchElementException(jobFuelDetailId + ": " + SystemType.NO_SUCH_ELEMENT);
		return jfdInDB.get();
	}

	@Override
	public RentalVehicle getRentalVehicleById(Integer rentalVehicleId) {
		Assert.notNull(rentalVehicleId, "Rental Vehicle ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<RentalVehicle> rentalVehicleInDB = rentalVehicleRepository.findById(rentalVehicleId);
		if (rentalVehicleInDB.isEmpty())
			throw new NoSuchElementException(rentalVehicleId + ": " + SystemType.NO_SUCH_ELEMENT);
		return rentalVehicleInDB.get();
	}

	@Override
	public Stop getStopById(String stopId) {
		Assert.notNull(stopId, "Stop ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Stop> stop = stopRepository.findById(stopId);
		if (stop.isEmpty())
			throw new NoSuchElementException(stopId + ": " + SystemType.NO_SUCH_ELEMENT);
		return stop.get();
	}

	@Override
	public StopProgress getStopProgressById(Integer stopProgressId) {
		Assert.notNull(stopProgressId, "Stop Progress ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<StopProgress> stopProgressInDB = stopProgressRepository.findById(stopProgressId);
		if (stopProgressInDB.isEmpty())
			throw new NoSuchElementException(stopProgressId + ": " + SystemType.NO_SUCH_ELEMENT);
		return stopProgressInDB.get();
	}

	@Override
	public Product getProductById(Integer productId) {
		Assert.notNull(productId, "Product ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Product> productInDB = productRepository.findById(productId);
		if (productInDB.isEmpty())
			throw new NoSuchElementException(productId + ": " + SystemType.NO_SUCH_ELEMENT);
		return productInDB.get();
	}

	@Override
	public Expense updateExpense(Expense expenseNew) {
		Assert.notNull(expenseNew, "Expense: " + SystemType.MUST_NOT_BE_NULL);
		return expenseRepository.save(expenseNew);
	}

	@Override
	public Income updateIncome(Income incomeNew) {
		Assert.notNull(incomeNew, "Income: " + SystemType.MUST_NOT_BE_NULL);
		return incomeRepository.save(incomeNew);
	}

	@Override
	public List<Stop> getAllStop() {
		return stopRepository.findAll();
	}

	@Override
	public List<Stop> filterStop(Example<Stop> stopExample) {
		return stopRepository.findAll(stopExample);
	}

	@Override
	public void deleteStopById(String stopId) {
		stopRepository.deleteById(stopId);
	}

	@Override
	public List<StopProgress> getAllStopProgress() {
		return stopProgressRepository.findAll();
	}

	@Override
	public List<StopProgress> filterStopProgress(Example<StopProgress> stopProgressExample) {
		return stopProgressRepository.findAll(stopProgressExample);
	}

	@Override
	public void deleteStopProgressById(Integer stopProgressId) {
		stopProgressRepository.deleteById(stopProgressId);
	}

	@Override
	public List<Product> getAllProduct() {
		return productRepository.findAll();
	}

	@Override
	public List<Product> filterProduct(Example<Product> productExample) {
		return productRepository.findAll(productExample);
	}

	@Override
	public void deleteProductById(Integer productId) {
		productRepository.deleteById(productId);
	}

	@Override
	public List<Income> getAllIncome() {
		return incomeRepository.findAll();
	}

	@Override
	public Income getIncomeById(Integer incomeId) {
		Optional<Income> income = incomeRepository.findById(incomeId);
		if (income.isEmpty())
			throw new NoSuchElementException(incomeId + ": " + SystemType.NO_SUCH_ELEMENT);
		return income.get();
	}

	@Override
	public List<Income> filterIncome(Example<Income> incomeExample) {
		return incomeRepository.findAll(incomeExample);
	}

	@Override
	public void deleteIncomeById(Integer incomeId) {
		incomeRepository.deleteById(incomeId);
	}

	@Override
	public List<Expense> getAllExpense() {
		return expenseRepository.findAll();
	}

	@Override
	public Expense getExpenseById(Integer expenseId) {
		Optional<Expense> expense = expenseRepository.findById(expenseId);
		if (expense.isEmpty())
			throw new NoSuchElementException(expenseId + ": " + SystemType.NO_SUCH_ELEMENT);
		return expense.get();
	}

	@Override
	public List<Expense> filterExpense(Example<Expense> expenseExample) {
		return expenseRepository.findAll(expenseExample);
	}

	@Override
	public void deleteExpenseById(Integer expenseId) {
		expenseRepository.deleteById(expenseId);
	}

	@Override
	public List<JobFuelDetail> getAllJobFuelDetail() {
		return jfdRepository.findAll();
	}

	@Override
	public List<JobFuelDetail> filterJobFuelDetail(Example<JobFuelDetail> jobFuelDetailExample) {
		return jfdRepository.findAll(jobFuelDetailExample);
	}

	@Override
	public void deleteJobFuelDetailById(Integer jobFuelDetailId) {
		jfdRepository.deleteById(jobFuelDetailId);
	}

	@Override
	public List<RoadwayBill> findRoadwayBillsByIdAndTractor(String rwbId, String tractorNumber) {
		return rwbRepository.findAllByIdAndTractor(rwbId, tractorNumber);
	}

	@Override
	public List<RoadwayBill> findRoadwayBillsByIdAndRentalVehicle(String rwbId, String tractorNumber) {
		return rwbRepository.findAllByIdAndRentalVehicle(rwbId, tractorNumber);
	}

	@Override
	public long countJobByStatus(DJobStatus status) {
		Job job = new Job();
		job.setJobStatus(status);
		return jobRepository.count(Example.of(job));
	}

	@Override
	public long countRoadwayBillByStatus(DRWBStatus status) {
		RoadwayBill rwb = new RoadwayBill();
		rwb.setRateStatus(null);
		rwb.setRwbStatus(status);
		return rwbRepository.count(Example.of(rwb));
	}

	@Override
	public List<Job> getClosedJobInRange(Date start, Date end) {
		Assert.notNull(start, "Range start must not be null");
		Assert.notNull(end, "Range end must not be null");
		return jobRepository.getClosedJobInRange(start, end);
	}
}
