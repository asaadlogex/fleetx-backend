package com.fleetX.dao.implementation;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IAdminDAO;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;
import com.fleetX.repository.AdminCompanyRepository;
import com.fleetX.repository.AssetCombinationRepository;
import com.fleetX.repository.FuelCardRepository;
import com.fleetX.repository.RoleRepository;
import com.fleetX.repository.RouteRepository;
import com.fleetX.repository.UserRepository;
import com.fleetX.service.IAssetService;

@Component
public class AdminDAO implements IAdminDAO {

	@Autowired
	AdminCompanyRepository adminCompanyRepository;
	@Autowired
	RouteRepository routeRepository;
	@Autowired
	AssetCombinationRepository assetCombinationRepository;
	@Autowired
	FuelCardRepository fuelCardRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	IAssetService assetService;

	@Override
	public AdminCompany createAdminCompany(AdminCompany adminCompany) throws Exception {
		Assert.notNull(adminCompany, "Admin Company: " + SystemType.MUST_NOT_BE_NULL);
		return adminCompanyRepository.save(adminCompany);
	}

	@Override
	public List<AdminCompany> findAllAdminCompany() {
		return adminCompanyRepository.findAll();
	}

	@Override
	public AdminCompany findAdminCompanyById(String adminCompanyId) {
		Assert.notNull(adminCompanyId, "Admin Company ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<AdminCompany> company = adminCompanyRepository.findById(adminCompanyId);
		if (company.isEmpty())
			throw new NoSuchElementException(adminCompanyId + ": " + SystemType.NO_SUCH_ELEMENT);
		return company.get();
	}

	@Override
	public List<AdminCompany> filterAdminCompany(Example<AdminCompany> adminCompanyExample) {
		return adminCompanyRepository.findAll(adminCompanyExample);
	}

	@Override
	public AdminCompany updateAdminCompany(AdminCompany companyNew) {
		Assert.notNull(companyNew, "Admin Company: " + SystemType.MUST_NOT_BE_NULL);
		return adminCompanyRepository.save(companyNew);
	}

	@Override
	public void deleteAdminCompany(String adminCompanyId) {
		Assert.notNull(adminCompanyId, "Admin Company ID: " + SystemType.MUST_NOT_BE_NULL);
		adminCompanyRepository.deleteById(adminCompanyId);
	}

	@Override
	public Route addRoute(Route route) {
		Assert.notNull(route, "Route: " + SystemType.MUST_NOT_BE_NULL);
		return routeRepository.save(route);
	}

	@Override
	public List<Route> getAllRoute() {
		return routeRepository.findAll();
	}

	@Override
	public List<Route> filterRoute(Example<Route> routeExample) {
		return routeRepository.findAll(routeExample);
	}

	@Override

	public Route updateRoute(Route routeNew) {
		Assert.notNull(routeNew, "Route: " + SystemType.MUST_NOT_BE_NULL);
		return routeRepository.save(routeNew);
	}

	@Override

	public void deleteRouteById(String routeId) {
		Assert.notNull(routeId, "Route ID: " + SystemType.MUST_NOT_BE_NULL);
		routeRepository.deleteById(routeId);
	}

	@Override
	public Route findRouteById(String routeId) {
		Assert.notNull(routeId, "Route ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Route> routeInDB = routeRepository.findById(routeId);
		if (routeInDB.isEmpty())
			throw new NoSuchElementException(routeId + ": " + SystemType.NO_SUCH_ELEMENT);
		return routeInDB.get();
	}

	@Override
	public FuelCard addFuelCard(FuelCard fuelCard) {
		Assert.notNull(fuelCard, "Fuel Card: " + SystemType.MUST_NOT_BE_NULL);
		return fuelCardRepository.save(fuelCard);
	}

	@Override
	public List<FuelCard> getAllFuelCard() {
		return fuelCardRepository.findAll();
	}

	@Override
	public List<FuelCard> filterFuelCard(Example<FuelCard> fuelCardExample) {
		return fuelCardRepository.findAll(fuelCardExample);
	}

	@Override
	public FuelCard updateFuelCard(FuelCard fuelCardNew) {
		Assert.notNull(fuelCardNew, "Fuel Card: " + SystemType.MUST_NOT_BE_NULL);
		return fuelCardRepository.save(fuelCardNew);
	}

	@Override
	public void deleteFuelCardById(Integer fuelCardId) {
		Assert.notNull(fuelCardId, "Fuel Card ID: " + SystemType.MUST_NOT_BE_NULL);
		fuelCardRepository.deleteById(fuelCardId);
	}

	@Override
	public FuelCard findFuelCardById(Integer fuelCardId) {
		Assert.notNull(fuelCardId, "Fuel Card ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<FuelCard> fuelCardInDB = fuelCardRepository.findById(fuelCardId);
		if (fuelCardInDB.isEmpty())
			throw new NoSuchElementException(fuelCardId + ": " + SystemType.NO_SUCH_ELEMENT);
		return fuelCardInDB.get();
	}

	// ROLES

	@Override
	public Role addRole(Role role) {
		Assert.notNull(role, "Role: " + SystemType.MUST_NOT_BE_NULL);
		return roleRepository.save(role);
	}

	@Override
	public Role updateRole(Role role) {
		Assert.notNull(role, "Role: " + SystemType.MUST_NOT_BE_NULL);
		return roleRepository.save(role);
	}

	@Override
	public List<Role> getAllRole() {
		return roleRepository.findAll();
	}

	@Override
	public Role getRoleByRoleName(String roleName) {
		Assert.notNull(roleName, "Role name: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Role> role = roleRepository.findById(roleName);
		if (role.isEmpty())
			throw new NoSuchElementException(roleName + ": " + SystemType.NO_SUCH_ELEMENT);
		return role.get();
	}

	@Override
	public void deleteRoleByRoleName(String roleName) {
		Assert.notNull(roleName, "Role name: " + SystemType.MUST_NOT_BE_NULL);
		roleRepository.deleteById(roleName);
	}

	@Override
	public void deleteAllRole() {
		roleRepository.deleteAll();
	}

	@Override
	public List<String> getPrivilegesByRoleName(String roleName) {
		Assert.notNull(roleName, "Role name: " + SystemType.MUST_NOT_BE_NULL);
		return roleRepository.findPrivilegesByRoleName(roleName);
	}

	// USERS

	@Override
	public User addUser(User user) {
		Assert.notNull(user, "User: " + SystemType.MUST_NOT_BE_NULL);
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUser() {
		return userRepository.findAll();
	}

	@Override
	public User getUserByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		Optional<User> user = userRepository.findById(username);
		if (user.isEmpty())
			throw new NoSuchElementException(username + ": " + SystemType.NO_SUCH_ELEMENT);
		return user.get();
	}

	@Override
	public void deleteUserByUsername(String username) throws IllegalArgumentException, NoSuchElementException {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		userRepository.deleteById(username);
	}

	@Override
	public void deleteAllUser() {
		userRepository.deleteAll();
	}

	@Override
	public User updateUser(User userNew) {
		Assert.notNull(userNew, "User: " + SystemType.MUST_NOT_BE_NULL);
		return userRepository.save(userNew);
	}

	@Override
	public List<String> getAdditionalPrivilegesByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		return userRepository.findAdditionalPrivilegesByUsername(username);
	}

	@Override
	public List<String> getRoleNamesByUsername(String username) {
		Assert.notNull(username, "Username: " + SystemType.MUST_NOT_BE_NULL);
		return userRepository.findRoleNamesByUsername(username);
	}
}
