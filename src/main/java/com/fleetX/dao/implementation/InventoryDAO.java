package com.fleetX.dao.implementation;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IInventoryDAO;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.ItemPurchaseDetail;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.maintenance.WorkOrderItemDetail;
import com.fleetX.repository.InventoryAdjustmentRepository;
import com.fleetX.repository.InventoryRepository;
import com.fleetX.repository.ItemPurchaseDetailRepository;
import com.fleetX.repository.ItemRepository;
import com.fleetX.repository.PurchaseOrderRepository;
import com.fleetX.repository.WorkOrderItemDetailRepository;
import com.fleetX.repository.WorkOrderRepository;

@Component
public class InventoryDAO implements IInventoryDAO {
	@Autowired
	ItemRepository itemRepository;
	@Autowired
	InventoryRepository inventoryRepository;
	@Autowired
	PurchaseOrderRepository purchaseOrderRepository;
	@Autowired
	ItemPurchaseDetailRepository itemPurchaseDetailRepository;
	@Autowired
	WorkOrderRepository workOrderRepository;
	@Autowired
	WorkOrderItemDetailRepository workOrderItemDetailRepository;
	@Autowired
	InventoryAdjustmentRepository inventoryAdjustmentRepository;

	@Override
	public List<Inventory> getAllInventory() {
		return inventoryRepository.findAll();
	}

	@Override
	public Inventory getInventoryById(Integer id) {
		Optional<Inventory> inventory = inventoryRepository.findById(id);
		if (inventory.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return inventory.get();
	}

	@Override
	public Inventory addInventory(Inventory inventory) {
		inventory.setInventoryId(null);
		return inventoryRepository.save(inventory);
	}

	@Override
	public Inventory updateInventory(Inventory inventory) {
		return inventoryRepository.save(inventory);
	}

	@Override
	public void deleteInventoryById(Integer id) {
		inventoryRepository.deleteById(id);
	}

	@Override
	public List<Inventory> filterInventoryByExample(Example<Inventory> inventoryExample) {
		List<Inventory> inventories = inventoryRepository.findAll(inventoryExample);
		if (inventories != null && inventories.size() > 0)
			return inventories;
		else
			return null;
	}

	@Override
	public Item addItem(Item item) {
		item.setItemId(null);
		return itemRepository.save(item);
	}

	@Override
	public List<Item> getAllItem() {
		return itemRepository.findAll();
	}

	@Override
	public Item getItemById(Integer id) {
		Optional<Item> item = itemRepository.findById(id);
		if (item.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return item.get();
	}

	@Override
	public Item updateItem(Item item) {
		return itemRepository.save(item);
	}

	@Override
	public void deleteItemById(Integer id) {
		itemRepository.deleteById(id);
	}

	@Override
	public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder) {
		purchaseOrder.setPurchaseOrderId(null);
		return purchaseOrderRepository.save(purchaseOrder);
	}

	@Override
	public List<PurchaseOrder> getAllPurchaseOrder() {
		return purchaseOrderRepository.findAll();
	}

	@Override
	public PurchaseOrder getPurchaseOrderById(Integer id) {
		Optional<PurchaseOrder> purchaseOrder = purchaseOrderRepository.findById(id);
		if (purchaseOrder.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return purchaseOrder.get();
	}

	@Override
	public PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrder) {
		return purchaseOrderRepository.save(purchaseOrder);
	}

	@Override
	public void deletePurchaseOrderById(Integer id) {
		purchaseOrderRepository.deleteById(id);
	}

	@Override
	public ItemPurchaseDetail addItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetail) {
		itemPurchaseDetail.setItemPurchaseDetailId(null);
		return itemPurchaseDetailRepository.save(itemPurchaseDetail);
	}

	@Override
	public ItemPurchaseDetail getItemPurchaseDetailById(Integer id) {
		Optional<ItemPurchaseDetail> itemPurchaseDetail = itemPurchaseDetailRepository.findById(id);
		if (itemPurchaseDetail.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return itemPurchaseDetail.get();
	}

	@Override
	public ItemPurchaseDetail updateItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetail) {
		return itemPurchaseDetailRepository.save(itemPurchaseDetail);
	}

	@Override
	public void deleteItemPurchaseDetailById(Integer id) {
		itemPurchaseDetailRepository.deleteById(id);
	}

	@Override
	public WorkOrder addWorkOrder(WorkOrder workOrder) {
		workOrder.setWorkOrderId(null);
		return workOrderRepository.save(workOrder);
	}

	@Override
	public List<WorkOrder> getAllWorkOrder() {
		return workOrderRepository.findAll();
	}

	@Override
	public WorkOrder getWorkOrderById(Integer id) {
		Optional<WorkOrder> workOrder = workOrderRepository.findById(id);
		if (workOrder.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return workOrder.get();
	}

	@Override
	public WorkOrder updateWorkOrder(WorkOrder workOrder) {
		return workOrderRepository.save(workOrder);
	}

	@Override
	public void deleteWorkOrderById(Integer id) {
		workOrderRepository.deleteById(id);
	}

	@Override
	public WorkOrderItemDetail addWorkOrderItemDetail(WorkOrderItemDetail workOrderItemDetail) {
		workOrderItemDetail.setWorkOrderItemDetailId(null);
		return workOrderItemDetailRepository.save(workOrderItemDetail);
	}

	@Override
	public WorkOrderItemDetail getWorkOrderItemDetailById(Integer id) {
		Optional<WorkOrderItemDetail> workOrderItemDetail = workOrderItemDetailRepository.findById(id);
		if (workOrderItemDetail.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return workOrderItemDetail.get();
	}

	@Override
	public WorkOrderItemDetail updateWorkOrderItemDetail(WorkOrderItemDetail workOrderItemDetail) {
		return workOrderItemDetailRepository.save(workOrderItemDetail);
	}

	@Override
	public void deleteWorkOrderItemDetailById(Integer id) {
		workOrderItemDetailRepository.deleteById(id);
	}

	@Override
	public InventoryAdjustment addInventoryAdjustment(InventoryAdjustment inventoryAdjustment) {
		inventoryAdjustment.setInventoryAdjustmentId(null);
		return inventoryAdjustmentRepository.save(inventoryAdjustment);
	}

	@Override
	public List<InventoryAdjustment> getAllInventoryAdjustment() {
		return inventoryAdjustmentRepository.findAll();
	}

	@Override
	public InventoryAdjustment getInventoryAdjustmentById(Integer id) {
		Optional<InventoryAdjustment> inventoryAdjustment = inventoryAdjustmentRepository.findById(id);
		if (inventoryAdjustment.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return inventoryAdjustment.get();
	}

}
