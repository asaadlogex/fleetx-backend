package com.fleetX.dao.implementation;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IEmployeeDAO;
import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;
import com.fleetX.repository.EmployeeContractRepository;
import com.fleetX.repository.EmployeeRepository;
import com.fleetX.repository.HeavyVehicleExperienceRepository;
import com.fleetX.repository.MedicalTestReportRepository;
import com.fleetX.repository.ReferenceRepository;
import com.fleetX.repository.TrainingProfileRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IDropDownService;

@Component

public class EmployeeDAO implements IEmployeeDAO {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	EmployeeContractRepository employeeContractRepository;
	@Autowired
	TrainingProfileRepository trainingProfileRepository;
	@Autowired
	MedicalTestReportRepository medicalTestReportRepository;
	@Autowired
	HeavyVehicleExperienceRepository heavyVehicleExperienceRepository;
	@Autowired
	ReferenceRepository referenceRepository;
	@Autowired
	IAdminService adminService;
	@Autowired
	IDropDownService dropDownService;

	@Override
	public Employee createEmployee(Employee employee) {
		Assert.notNull(employee, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		return employeeRepository.save(employee);
	}

	@Override
	public EmployeeContract addEmployeeContract(EmployeeContract employeeContract) {
		Assert.notNull(employeeContract, SystemType.MUST_NOT_BE_NULL);
		return employeeContractRepository.save(employeeContract);
	}

	@Override
	public EmployeeContract updateEmployeeContract(EmployeeContract employeeContract) {
		Assert.notNull(employeeContract, SystemType.MUST_NOT_BE_NULL);
		return employeeContractRepository.save(employeeContract);
	}

	@Override
	public Employee updateEmployee(Employee employeeNew) {
		Assert.notNull(employeeNew, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		return employeeRepository.save(employeeNew);
	}

	@Override
	public Employee findEmployeeById(String employeeId) {
		Assert.notNull(employeeId, "Employee ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Employee> employee = employeeRepository.findById(employeeId);
		if (employee.isEmpty())
			throw new NoSuchElementException(employeeId + ": " + SystemType.NO_SUCH_ELEMENT);
		return employee.get();
	}

	@Override
	public void deleteEmployee(String employeeId) {
		Assert.notNull(employeeId, "Employee ID: " + SystemType.MUST_NOT_BE_NULL);
		employeeRepository.deleteById(employeeId);
	}

	@Override
	public List<Employee> findAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public void deleteEmployeeContracts(List<EmployeeContract> employeeContracts) {
		Assert.notNull(employeeContracts, "Employee Contracts: " + SystemType.MUST_NOT_BE_NULL);
		employeeContractRepository.deleteAll(employeeContracts);
	}

	@Override
	public List<Employee> filterEmployeeByExample(Example<Employee> employeeExample) {

		return employeeRepository.findAll(employeeExample);
	}

	@Override
	public TrainingProfile addTrainingProfile(TrainingProfile trainingProfile) {
		Assert.notNull(trainingProfile, "Training Profile: " + SystemType.MUST_NOT_BE_NULL);
		return trainingProfileRepository.save(trainingProfile);
	}

	@Override
	public MedicalTestReport addMedicalTestReport(MedicalTestReport medicalTestReport) {
		Assert.notNull(medicalTestReport, "Medical Test Report: " + SystemType.MUST_NOT_BE_NULL);
		return medicalTestReportRepository.save(medicalTestReport);
	}

	@Override
	public HeavyVehicleExperience addHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience) {
		Assert.notNull(heavyVehicleExperience, "Heavy Vehicle Experience: " + SystemType.MUST_NOT_BE_NULL);
		return heavyVehicleExperienceRepository.save(heavyVehicleExperience);
	}

	@Override
	public Reference addReference(Reference reference) {
		Assert.notNull(reference, "Reference: " + SystemType.MUST_NOT_BE_NULL);
		return referenceRepository.save(reference);
	}

	@Override
	public TrainingProfile updateTrainingProfile(TrainingProfile trainingProfileNew) {
		Assert.notNull(trainingProfileNew, SystemType.MUST_NOT_BE_NULL);
		return trainingProfileRepository.save(trainingProfileNew);
	}

	@Override
	public MedicalTestReport updateMedicalTestReport(MedicalTestReport medicalTestReportNew) {
		Assert.notNull(medicalTestReportNew, SystemType.MUST_NOT_BE_NULL);
		return medicalTestReportRepository.save(medicalTestReportNew);
	}

	@Override
	public HeavyVehicleExperience updateHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperienceNew) {
		Assert.notNull(heavyVehicleExperienceNew, SystemType.MUST_NOT_BE_NULL);
		return heavyVehicleExperienceRepository.save(heavyVehicleExperienceNew);
	}

	@Override
	public Reference updateReference(Reference referenceNew) {
		Assert.notNull(referenceNew, SystemType.MUST_NOT_BE_NULL);
		return referenceRepository.save(referenceNew);
	}

	@Override
	public TrainingProfile findTrainingProfileById(String trainingProfileId) {
		Assert.notNull(trainingProfileId, SystemType.MUST_NOT_BE_NULL);
		Optional<TrainingProfile> trainingProfileInDB = trainingProfileRepository.findById(trainingProfileId);
		if (trainingProfileInDB.isEmpty())
			throw new NoSuchElementException(trainingProfileId + ": " + SystemType.NO_SUCH_ELEMENT);
		return trainingProfileInDB.get();
	}

	@Override
	public MedicalTestReport findMedicalTestReportById(String medicalTestReportId) {
		Assert.notNull(medicalTestReportId, SystemType.MUST_NOT_BE_NULL);
		Optional<MedicalTestReport> medicalTestReportInDB = medicalTestReportRepository.findById(medicalTestReportId);
		if (medicalTestReportInDB.isEmpty())
			throw new NoSuchElementException(medicalTestReportId + ": " + SystemType.NO_SUCH_ELEMENT);
		return medicalTestReportInDB.get();
	}

	@Override
	public HeavyVehicleExperience findHeavyVehicleExperienceById(String heavyVehicleExperienceId) {
		Assert.notNull(heavyVehicleExperienceId, SystemType.MUST_NOT_BE_NULL);
		Optional<HeavyVehicleExperience> heavyVehicleExperienceInDB = heavyVehicleExperienceRepository
				.findById(heavyVehicleExperienceId);
		if (heavyVehicleExperienceInDB.isEmpty())
			throw new NoSuchElementException(heavyVehicleExperienceId + ": " + SystemType.NO_SUCH_ELEMENT);
		return heavyVehicleExperienceInDB.get();
	}

	@Override
	public Reference findReferenceById(String referenceId) {
		Assert.notNull(referenceId, SystemType.MUST_NOT_BE_NULL);
		Optional<Reference> referenceInDB = referenceRepository.findById(referenceId);
		if (referenceInDB.isEmpty())
			throw new NoSuchElementException(referenceId + ": " + SystemType.NO_SUCH_ELEMENT);
		return referenceInDB.get();
	}

	@Override
	public void deleteTrainingProfileById(String trainingProfileId) {
		Assert.notNull(trainingProfileId, "Training Profile ID: " + SystemType.MUST_NOT_BE_NULL);
		trainingProfileRepository.deleteById(trainingProfileId);
	}

	@Override
	public void deleteMedicalTestReportById(String medicalTestReportId) {
		Assert.notNull(medicalTestReportId, "Medical Test Report ID: " + SystemType.MUST_NOT_BE_NULL);
		medicalTestReportRepository.deleteById(medicalTestReportId);
	}

	@Override
	public void deleteHeavyVehicleExperienceById(String heavyVehicleExperienceId) {
		Assert.notNull(heavyVehicleExperienceId, "Heavy Vehicle Experience ID: " + SystemType.MUST_NOT_BE_NULL);
		heavyVehicleExperienceRepository.deleteById(heavyVehicleExperienceId);
	}

	@Override
	public void deleteReferenceById(String referenceId) {
		Assert.notNull(referenceId, "Reference ID: " + SystemType.MUST_NOT_BE_NULL);
		referenceRepository.deleteById(referenceId);
	}

	@Override
	public long countEmployeeByPositionAndStatus(DPosition position, String status) {
		Employee employee = new Employee();
		employee.setPosition(position);
		employee.setStatus(status);
		return employeeRepository.count(Example.of(employee));
	}
}
