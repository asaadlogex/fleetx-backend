package com.fleetX.dao.implementation;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import com.fleetX.config.SystemType;
import com.fleetX.dao.IAccountDAO;
import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.FixedLineItem;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.LineItem;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;
import com.fleetX.repository.AdjustmentRepository;
import com.fleetX.repository.DHInvoiceRepository;
import com.fleetX.repository.FixedLineItemRepository;
import com.fleetX.repository.InvoicePaymentRepository;
import com.fleetX.repository.InvoiceRepository;
import com.fleetX.repository.LineItemRepository;
import com.fleetX.repository.ReimbursementInfoRepository;

@Component
public class AccountDAO implements IAccountDAO {
	@Autowired
	AdjustmentRepository adjustmentRepository;
	@Autowired
	InvoiceRepository invoiceRepository;
	@Autowired
	LineItemRepository lineItemRepository;
	@Autowired
	FixedLineItemRepository fixedLineItemRepository;
	@Autowired
	ReimbursementInfoRepository reimbursementInfoRepository;
	@Autowired
	InvoicePaymentRepository invoicePaymentRepository;
	@Autowired
	DHInvoiceRepository dhInvoiceRepository;

	@Override
	public Adjustment addAdjustmentEntry(Adjustment adjustment) {
		adjustment.setAdjustmentId(null);
		return adjustmentRepository.save(adjustment);
	}

	@Override
	public List<Adjustment> getAllAdjustments() {
		return adjustmentRepository.findAll();
	}

	@Override
	public void deleteAdjustmentById(Integer id) {
		if (adjustmentRepository.existsById(id))
			adjustmentRepository.deleteById(id);
	}

	@Override
	public List<Adjustment> filterAdjustment(String accountHead, Date start, Date end) {
		return adjustmentRepository.findAdjustments(accountHead, start, end);
	}

	@Override
	public Invoice addInvoice(Invoice invoice) {
		return invoiceRepository.save(invoice);
	}

	@Override
	public Invoice getInvoiceById(Integer invoiceId) {
		Optional<Invoice> invoice = invoiceRepository.findById(invoiceId);
		if (invoice.isEmpty())
			throw new NoSuchElementException(invoiceId + ": " + SystemType.NO_SUCH_ELEMENT);
		return invoice.get();
	}

	@Override
	public List<Invoice> getAlIInvoice() {
		return invoiceRepository.findAll();
	}

	@Override
	public Invoice updateInvoice(Invoice invoiceNew) {
		return invoiceRepository.save(invoiceNew);
	}

	@Override
	public void deleteInvoiceById(Integer invoiceId) {
		invoiceRepository.deleteById(invoiceId);
	}

	@Override
	public LineItem addLineItem(LineItem lineItem) {
		lineItem.setLineItemId(null);
		return lineItemRepository.save(lineItem);
	}

	@Override
	public List<Invoice> filterInvoice(Company customer, DInvoiceType invoiceType, DPaymentStatus paymentStatus,
			Date startDate, Date endDate, Integer month, Integer year) {
		return invoiceRepository.filterInvoice(customer, invoiceType, paymentStatus, startDate, endDate, month, year);
	}

	@Override
	public FixedLineItem addFixedLineItem(FixedLineItem lineItem) {
		lineItem.setFixedLineItemId(null);
		return fixedLineItemRepository.save(lineItem);
	}

	@Override
	public InvoicePayment addInvoicePayment(InvoicePayment invoicePayment) {
		invoicePayment.setInvoicePaymentId(null);
		return invoicePaymentRepository.save(invoicePayment);
	}

	@Override
	public ReimbursementInfo addReimbursementInfo(ReimbursementInfo reimbursementInfo) {
		reimbursementInfo.setReimbursementInfoId(null);
		return reimbursementInfoRepository.save(reimbursementInfo);
	}

	@Override
	public List<ReimbursementInfo> getAllReimbursementInfo() {
		return reimbursementInfoRepository.findAll();
	}

	@Override
	public ReimbursementInfo getReimbursementInfoById(Integer id) {
		Optional<ReimbursementInfo> reimbursementInfo = reimbursementInfoRepository.findById(id);
		if (reimbursementInfo.isEmpty())
			throw new NoSuchElementException(id + ": " + SystemType.NO_SUCH_ELEMENT);
		return reimbursementInfo.get();
	}

	@Override
	public DHInvoice addDHInvoice(DHInvoice invoice) {
		invoice.setDhInvoiceId(null);
		return dhInvoiceRepository.save(invoice);
	}

	@Override
	public DHInvoice getDHInvoiceById(Integer invoiceId) {
		Optional<DHInvoice> invoice = dhInvoiceRepository.findById(invoiceId);
		if (invoice.isEmpty())
			throw new NoSuchElementException(invoiceId + ": " + SystemType.NO_SUCH_ELEMENT);
		return invoice.get();
	}

	@Override
	public List<DHInvoice> getAlIDHInvoice() {
		return dhInvoiceRepository.findAll();
	}

	@Override
	public DHInvoice updateDHInvoice(DHInvoice invoiceNew) {
		return dhInvoiceRepository.save(invoiceNew);
	}

	@Override
	public void deleteDHInvoiceById(Integer invoiceId) {
		dhInvoiceRepository.deleteById(invoiceId);
	}

	@Override
	public long countInvoiceByPaymentStatus(DPaymentStatus status) {
		Invoice invoice = new Invoice();
		invoice.setPaymentStatus(status);
		return invoiceRepository.count(Example.of(invoice));
	}

}
