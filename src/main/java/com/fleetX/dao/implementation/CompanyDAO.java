package com.fleetX.dao.implementation;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.dao.ICompanyDAO;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DContractType;
import com.fleetX.entity.order.Warehouse;
import com.fleetX.repository.CompanyRepository;
import com.fleetX.repository.WarehouseRepository;

@Component

public class CompanyDAO implements ICompanyDAO {

	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	WarehouseRepository warehouseRepository;

	@Override

	public Company createCompany(Company company) throws Exception {
		Assert.notNull(company, "Company: " + SystemType.MUST_NOT_BE_NULL);
		return companyRepository.save(company);
	}

	@Override

	public List<Company> findAllCompany() {
		return companyRepository.findAll();
	}

	@Override

	public Company findCompanyById(String companyId) {
		Assert.notNull(companyId, "Company ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Company> company = companyRepository.findById(companyId);
		if (company.isEmpty())
			throw new NoSuchElementException(companyId + ": " + SystemType.NO_SUCH_ELEMENT);
		return company.get();
	}

	@Override

	public Company updateCompany(Company companyNew) {
		Assert.notNull(companyNew, "Company: " + SystemType.MUST_NOT_BE_NULL);
		return companyRepository.save(companyNew);
	}

	@Override

	public void deleteCompany(String companyId) {
		Assert.notNull(companyId, "Company ID: " + SystemType.MUST_NOT_BE_NULL);
		companyRepository.deleteById(companyId);
	}

	@Override

	public List<Company> filterCompanyByExample(Example<Company> companyExample) {
		return companyRepository.findAll(companyExample);
	}

	@Override

	public Warehouse addWarehouse(Warehouse consigneeDetail) {
		Assert.notNull(consigneeDetail, "Warehouse Details: " + SystemType.MUST_NOT_BE_NULL);
		return warehouseRepository.save(consigneeDetail);
	}

	@Override

	public List<Warehouse> getAllWarehouse() {
		return warehouseRepository.findAll();
	}

	@Override

	public Warehouse updateWarehouse(Warehouse warehouseOld) {
		Assert.notNull(warehouseOld, "Warehouse Details: " + SystemType.MUST_NOT_BE_NULL);
		return warehouseRepository.save(warehouseOld);
	}

	@Override

	public Warehouse getWarehouseById(Integer warehouseId) {
		Assert.notNull(warehouseId, "Warehouse ID: " + SystemType.MUST_NOT_BE_NULL);
		Optional<Warehouse> warehouseInDB = warehouseRepository.findById(warehouseId);
		if (warehouseInDB.isEmpty())
			throw new NoSuchElementException(warehouseId + ": " + SystemType.NO_SUCH_ELEMENT);
		return warehouseInDB.get();
	}

	@Override

	public List<Warehouse> filterWarehouse(Example<Warehouse> warehouseExample) {
		return warehouseRepository.findAll(warehouseExample);
	}

	@Override

	public void deleteWarehouseById(Integer warehouseId) {
		Assert.notNull(warehouseId, "Warehouse ID: " + SystemType.MUST_NOT_BE_NULL);
		warehouseRepository.deleteById(warehouseId);
	}
}
