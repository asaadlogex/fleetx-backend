package com.fleetX.dao;

import java.util.List;

import org.springframework.data.domain.Example;

import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.ItemPurchaseDetail;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.maintenance.WorkOrderItemDetail;

public interface IInventoryDAO {
	public Inventory addInventory(Inventory inventory);
	public List<Inventory> getAllInventory();
	public Inventory getInventoryById(Integer id);
	public Inventory updateInventory(Inventory inventory);
	public void deleteInventoryById(Integer id);
	public List<Inventory> filterInventoryByExample(Example<Inventory> inventoryExample);
	
	public Item addItem(Item item);
	public List<Item> getAllItem();
	public Item getItemById(Integer id);
	public Item updateItem(Item item);
	public void deleteItemById(Integer id);
	
	public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder);
	public List<PurchaseOrder> getAllPurchaseOrder();
	public PurchaseOrder getPurchaseOrderById(Integer id);
	public PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrder);
	public void deletePurchaseOrderById(Integer id);
	
	public ItemPurchaseDetail addItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetail);
	public ItemPurchaseDetail getItemPurchaseDetailById(Integer id);
	public ItemPurchaseDetail updateItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetail);
	public void deleteItemPurchaseDetailById(Integer id);
	
	public WorkOrder addWorkOrder(WorkOrder workOrder);
	public List<WorkOrder> getAllWorkOrder();
	public WorkOrder getWorkOrderById(Integer id);
	public WorkOrder updateWorkOrder(WorkOrder workOrder);
	public void deleteWorkOrderById(Integer id);
	
	public WorkOrderItemDetail addWorkOrderItemDetail(WorkOrderItemDetail workOrderItemDetail);
	public WorkOrderItemDetail getWorkOrderItemDetailById(Integer id);
	public WorkOrderItemDetail updateWorkOrderItemDetail(WorkOrderItemDetail workOrderItemDetail);
	public void deleteWorkOrderItemDetailById(Integer id);

	public InventoryAdjustment addInventoryAdjustment(InventoryAdjustment inventoryAdjustment);
	public List<InventoryAdjustment> getAllInventoryAdjustment();
	public InventoryAdjustment getInventoryAdjustmentById(Integer id);
}
