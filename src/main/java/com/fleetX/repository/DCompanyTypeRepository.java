package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DCompanyType;

public interface DCompanyTypeRepository extends JpaRepository<DCompanyType, Integer>,QueryByExampleExecutor<DCompanyType>{
	public List<DCompanyType> findAllByStatus(String status);

	public boolean existsByValue(String value);

}
