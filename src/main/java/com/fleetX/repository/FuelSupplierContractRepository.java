package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.FuelSupplierContract;

public interface FuelSupplierContractRepository extends JpaRepository<FuelSupplierContract, String>, QueryByExampleExecutor<FuelSupplierContract> {

}
