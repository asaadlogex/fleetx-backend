package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DStopProgressType;

public interface DStopProgressTypeRepository extends JpaRepository<DStopProgressType, Integer> {
	List<DStopProgressType> findAllByStatus(String status);

	boolean existsByValue(String value);
}
