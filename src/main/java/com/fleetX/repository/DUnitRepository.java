package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DUnit;

public interface DUnitRepository extends JpaRepository<DUnit, Integer> {

	List<DUnit> findAllByStatus(String statusActive);

	boolean existsByCode(String code);

	boolean existsByValue(String value);

}
