package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DInvoiceStatus;

public interface DInvoiceStatusRepository extends JpaRepository<DInvoiceStatus, Integer> {

	DInvoiceStatus findByValue(String value);

	List<DInvoiceStatus> findAllByStatus(String status);

}
