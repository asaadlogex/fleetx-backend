package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DLoadStatus;

public interface DLoadStatusRepository extends QueryByExampleExecutor<DLoadStatus>, JpaRepository<DLoadStatus, Integer> {

	public List<DLoadStatus> findAllByStatus(String status);

	public boolean existsByValue(String value);
}
