package com.fleetX.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.dropdown.DContractType;

public interface CompanyContractRepository
		extends JpaRepository<CompanyContract, String>, QueryByExampleExecutor<CompanyContract> {

	@Query("select contract from CompanyContract contract where (:companyId is null or contract.customer.companyId=:companyId) and (:date is null or contract.contractStart<=:date) and (:date is null or contract.contractEnd>=:date)")
	List<CompanyContract> filterContract(@Param("companyId") String companyId, @Param("date") Date date);

	List<CompanyContract> findAllByContractType(DContractType contractType);

}
