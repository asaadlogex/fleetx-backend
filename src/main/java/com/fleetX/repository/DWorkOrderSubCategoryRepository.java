package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DWorkOrderSubCategory;

public interface DWorkOrderSubCategoryRepository extends JpaRepository<DWorkOrderSubCategory, Integer> {

	List<DWorkOrderSubCategory> findAllByStatus(String status);

}
