package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DIncomeType;

public interface DIncomeTypeRepository extends JpaRepository<DIncomeType, Integer>, QueryByExampleExecutor<DIncomeType> {
	DIncomeType findByValue(String value);
	List<DIncomeType> findAllByStatus(String status);
	boolean existsByValue(String value);
}
