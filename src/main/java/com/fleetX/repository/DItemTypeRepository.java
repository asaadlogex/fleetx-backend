package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DItemType;

public interface DItemTypeRepository extends JpaRepository<DItemType, Integer> {

	List<DItemType> findAllByStatus(String statusActive);

	boolean existsByValue(String value);

	boolean existsByCode(String code);

}
