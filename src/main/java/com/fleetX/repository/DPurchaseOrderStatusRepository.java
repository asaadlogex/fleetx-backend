package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DPurchaseOrderStatus;

public interface DPurchaseOrderStatusRepository extends JpaRepository<DPurchaseOrderStatus, Integer> {
	DPurchaseOrderStatus findByValue(String value);

	List<DPurchaseOrderStatus> findAllByStatus(String statusActive);

}
