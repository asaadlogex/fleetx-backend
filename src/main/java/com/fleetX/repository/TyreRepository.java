package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.Tyre;

public interface TyreRepository extends JpaRepository<Tyre, String>,QueryByExampleExecutor<Tyre> {

}
