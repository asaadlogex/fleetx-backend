package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DVehicleOwnership;

public interface DVehicleOwnershipRepository extends JpaRepository<DVehicleOwnership, Integer>, QueryByExampleExecutor<DVehicleOwnership> {
	public List<DVehicleOwnership> findAllByStatus(String status);

	public DVehicleOwnership findByValue(String value);

	public boolean existsByValue(String value);
}
