package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DCompanyDocumentType;

public interface DCompanyDocumentTypeRepository extends JpaRepository<DCompanyDocumentType, Integer> {
	public List<DCompanyDocumentType> findAllByStatus(String status);

	public boolean existsByValue(String value);
}
