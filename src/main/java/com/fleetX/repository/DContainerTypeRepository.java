package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DContainerType;


public interface DContainerTypeRepository extends JpaRepository<DContainerType, Integer> {
	public List<DContainerType> findAllByStatus(String status);

	public boolean existsByValue(String value);


}
