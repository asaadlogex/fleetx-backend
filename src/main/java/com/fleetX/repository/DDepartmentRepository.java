package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DDepartment;

public interface DDepartmentRepository extends JpaRepository<DDepartment, Integer>,QueryByExampleExecutor<DDepartment> {
	public List<DDepartment> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DDepartment findByValue(String value);
}
