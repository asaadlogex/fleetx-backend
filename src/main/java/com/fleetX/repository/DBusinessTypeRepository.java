package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DBusinessType;

public interface DBusinessTypeRepository extends JpaRepository<DBusinessType, Integer>,QueryByExampleExecutor<DBusinessType> {
	public DBusinessType findByValue(String value);
	public List<DBusinessType> findAllByStatus(String status);
	public boolean existsByValue(String value);
}
