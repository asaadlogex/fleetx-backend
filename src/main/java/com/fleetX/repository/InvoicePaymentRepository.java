package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.account.InvoicePayment;

public interface InvoicePaymentRepository extends JpaRepository<InvoicePayment, Integer> {

}
