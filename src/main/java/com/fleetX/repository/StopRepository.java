package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.order.Stop;

public interface StopRepository extends JpaRepository<Stop, String>, QueryByExampleExecutor<Stop> {

}
