package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.order.RentalVehicle;

public interface RentalVehicleRepository extends JpaRepository<RentalVehicle, Integer>, QueryByExampleExecutor<RentalVehicle> {

}
