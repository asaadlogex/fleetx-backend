package com.fleetX.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DInvoiceType;
import com.fleetX.entity.dropdown.DPaymentStatus;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

	@Query("select invoice from Invoice invoice where (:customer is null or invoice.recipient=:customer) and (:invoiceType is null or invoice.invoiceType=:invoiceType) and (:paymentStatus is null or invoice.paymentStatus=:paymentStatus) and (:start is null or invoice.invoiceDate>=:start) and (:end is null or invoice.invoiceDate<=:end) and (:year is null or invoice.invoiceYear=:year) and (:month is null or invoice.invoiceMonth=:month) ")
	List<Invoice> filterInvoice(@Param("customer") Company customer, @Param("invoiceType") DInvoiceType invoiceType,
			@Param("paymentStatus") DPaymentStatus paymentStatus, @Param("start") Date startDate,
			@Param("end") Date endDate, @Param("month") Integer month, @Param("year") Integer year);

}
