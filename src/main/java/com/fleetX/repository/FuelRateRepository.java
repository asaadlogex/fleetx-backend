package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.FuelRate;

public interface FuelRateRepository extends JpaRepository<FuelRate, Integer>,QueryByExampleExecutor<FuelRate> {

}
