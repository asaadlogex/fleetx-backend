package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DEmployeeDocument;

public interface DEmployeeDocumentRepository extends JpaRepository<DEmployeeDocument, Integer>,QueryByExampleExecutor<DEmployeeDocument> {
	public List<DEmployeeDocument> findAllByStatus(String status);

	public boolean existsByValue(String value);
}
