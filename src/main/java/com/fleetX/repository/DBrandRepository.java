package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DBrand;

public interface DBrandRepository extends JpaRepository<DBrand, Integer> {

	List<DBrand> findAllByStatus(String statusActive);

	boolean existsByValue(String value);

	boolean existsByCode(String code);

}
