package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DTrailerType;

public interface DTrailerTypeRepository extends JpaRepository<DTrailerType, Integer>,QueryByExampleExecutor<DTrailerType> {
	public List<DTrailerType> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DTrailerType findByValue(String value);
}
