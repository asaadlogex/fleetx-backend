package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.Tracker;

public interface TrackerRepository extends JpaRepository<Tracker, String>,QueryByExampleExecutor<Tracker> {

}
