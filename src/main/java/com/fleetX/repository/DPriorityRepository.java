package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DPriority;

public interface DPriorityRepository extends JpaRepository<DPriority, Integer> {

	List<DPriority> findAllByStatus(String value);

}
