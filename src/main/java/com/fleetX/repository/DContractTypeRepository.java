package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DContractType;

public interface DContractTypeRepository extends JpaRepository<DContractType, Integer>,QueryByExampleExecutor<DContractType> {
	public List<DContractType> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DContractType findByValue(String value);
}
