package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.Company;

public interface CompanyRepository extends JpaRepository<Company, String>, QueryByExampleExecutor<Company> {

	@Query("select company from Company company where company.businessType.value=:type")
	List<Company> findAllByType(String type);
}
