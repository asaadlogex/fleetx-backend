package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.maintenance.WorkOrder;

public interface WorkOrderRepository extends JpaRepository<WorkOrder, Integer> {

}
