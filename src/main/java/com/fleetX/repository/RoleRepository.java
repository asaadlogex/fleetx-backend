package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.Role;

public interface RoleRepository extends JpaRepository<Role, String>,QueryByExampleExecutor<Role> {

	@Query(nativeQuery = true,value = "select privilege_name from roles_privileges as rp where rp.role_name=:roleName")
	List<String> findPrivilegesByRoleName(String roleName);
}
