package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DReimbursementStatus;

public interface DReimbursementStatusRepository extends JpaRepository<DReimbursementStatus, Integer>, QueryByExampleExecutor<DReimbursementStatus> {

	DReimbursementStatus findByValue(String reimbursementStatus);

	List<DReimbursementStatus> findAllByStatus(String status);

	boolean existsByValue(String value);

}
