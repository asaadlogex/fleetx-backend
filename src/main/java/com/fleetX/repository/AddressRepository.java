package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>, QueryByExampleExecutor<Address> {
}
