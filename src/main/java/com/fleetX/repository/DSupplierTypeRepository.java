package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DSupplierType;

public interface DSupplierTypeRepository extends JpaRepository<DSupplierType, Integer>,QueryByExampleExecutor<DSupplierType> {
	public List<DSupplierType> findAllByStatus(String status);

	public boolean existsByValue(String value);

}
