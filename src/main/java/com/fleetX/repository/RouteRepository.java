package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.Route;

public interface RouteRepository extends JpaRepository<Route, String>,QueryByExampleExecutor<Route> {

}
