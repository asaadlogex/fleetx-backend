package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DWorkOrderCategory;

public interface DWorkOrderCategoryRepository extends JpaRepository<DWorkOrderCategory, Integer> {

	List<DWorkOrderCategory> findAllByStatus(String status);

}
