package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DAssetType;

public interface DAssetTypeRepository extends JpaRepository<DAssetType,Integer> {
	public List<DAssetType> findAllByStatus(String status);
	
	public boolean existsByValue(String value);
}
