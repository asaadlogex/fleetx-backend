package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.employee.EmployeeContract;

public interface EmployeeContractRepository extends JpaRepository<EmployeeContract, Integer>,QueryByExampleExecutor<EmployeeContract>{

}
