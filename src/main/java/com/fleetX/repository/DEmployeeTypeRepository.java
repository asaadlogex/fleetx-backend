package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DEmployeeType;

public interface DEmployeeTypeRepository extends JpaRepository<DEmployeeType, Integer>,QueryByExampleExecutor<DEmployeeType> {
	public List<DEmployeeType> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DEmployeeType findByValue(String stringCellValue);
}

