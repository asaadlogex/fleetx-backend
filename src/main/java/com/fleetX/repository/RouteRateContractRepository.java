package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.RouteRateContract;

public interface RouteRateContractRepository extends JpaRepository<RouteRateContract,Integer>,QueryByExampleExecutor<RouteRateContract> {

}
