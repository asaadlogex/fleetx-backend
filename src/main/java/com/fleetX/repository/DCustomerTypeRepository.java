package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DCustomerType;

public interface DCustomerTypeRepository extends JpaRepository<DCustomerType, Integer>,QueryByExampleExecutor<DCustomerType> {

	public List<DCustomerType> findAllByStatus(String status);

	public boolean existsByValue(String value);
}
