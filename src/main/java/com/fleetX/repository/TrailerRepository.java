package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.Trailer;

public interface TrailerRepository extends JpaRepository<Trailer, String>,QueryByExampleExecutor<Trailer> {

}
