package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DInvoiceType;

public interface DInvoiceTypeRepository extends JpaRepository<DInvoiceType, Integer> {
	DInvoiceType findByValue(String value);

	List<DInvoiceType> findAllByStatus(String statusActive);
}
