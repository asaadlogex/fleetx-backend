package com.fleetX.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DBaseStation;
import com.fleetX.entity.dropdown.DJobStatus;
import com.fleetX.entity.dropdown.DReimbursementStatus;
import com.fleetX.entity.order.Job;

public interface JobRepository extends JpaRepository<Job, String>, QueryByExampleExecutor<Job> {

	@Query(nativeQuery = true, value = "select job_id from job where job_start_date >=:start and job_start_date<=:end")
	List<String> getJobIdsInRange(Date start, Date end);

	@Query(value = "select job from Job job where (:jobId is null or job.jobId=:jobId) and (:start is null or job.startDate>=:start) and (:end is null or job.startDate<=:end) and (:status is null or job.jobStatus=:status) and (:rStatus is null or job.reimbursementStatus=:rStatus) and (:baseStation is null or job.endBaseStation=:baseStation)")
	List<Job> filterJobs(@Param(value = "jobId") String jobId, @Param(value = "start") Date start,
			@Param(value = "end") Date end, @Param(value = "status") DJobStatus status,
			@Param(value = "rStatus") DReimbursementStatus rStatus,
			@Param(value = "baseStation") DBaseStation baseStation);

	@Query(value = "select job from Job job where (:start is null or job.endDate>=:start) and (:end is null or job.endDate<=:end)")
	List<Job> getClosedJobInRange(@Param(value = "start") Date start, @Param(value = "end") Date end);

}
