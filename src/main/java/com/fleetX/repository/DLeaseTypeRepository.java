package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DLeaseType;

public interface DLeaseTypeRepository extends JpaRepository<DLeaseType, Integer>,QueryByExampleExecutor<DLeaseType>{
	public List<DLeaseType> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DLeaseType findByValue(String value);
}
