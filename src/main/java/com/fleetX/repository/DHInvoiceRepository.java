package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.account.DHInvoice;

public interface DHInvoiceRepository extends JpaRepository<DHInvoice, Integer> {

}
