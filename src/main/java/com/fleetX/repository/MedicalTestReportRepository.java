package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.employee.MedicalTestReport;

public interface MedicalTestReportRepository extends JpaRepository<MedicalTestReport, String>,QueryByExampleExecutor<MedicalTestReport> {

}
