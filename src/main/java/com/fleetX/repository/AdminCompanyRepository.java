package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.AdminCompany;

public interface AdminCompanyRepository extends JpaRepository<AdminCompany, String>,QueryByExampleExecutor<AdminCompany> {
}
