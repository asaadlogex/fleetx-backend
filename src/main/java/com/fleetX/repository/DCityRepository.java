package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DCity;

public interface DCityRepository extends JpaRepository<DCity, Integer>,QueryByExampleExecutor<DCity> {
	public List<DCity> findAllByStatus(String status);
	public DCity findByCode(String cityName);
	public boolean existsByValue(String value);
	public boolean existsByCode(String code);
	
	public List<DCity> findAllByDescriptionAndStatus(String string, String status);
	public DCity findByValue(String stringCellValue);
}
