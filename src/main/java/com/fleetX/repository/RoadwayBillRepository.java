package com.fleetX.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.dropdown.DInvoiceStatus;
import com.fleetX.entity.dropdown.DState;
import com.fleetX.entity.order.RoadwayBill;

public interface RoadwayBillRepository extends JpaRepository<RoadwayBill, String>, QueryByExampleExecutor<RoadwayBill> {

	@Query(nativeQuery = true, value = "select * from roadway_bill as rwb where (:rwbId is null or rwb.rwb_id=:rwbId) and (:jobId is null or rwb.job_id=:jobId) and (:startDate is null or rwb.rwb_date>=:startDate) and (:endDate is null or rwb.rwb_date<=:endDate) and (:rateStatus is null or rwb.rate_status=:rateStatus)")
	List<RoadwayBill> filterRoadwayBills(@Param(value = "rwbId") String rwbId, @Param(value = "jobId") String jobId,
			@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate,
			@Param(value = "rateStatus") Boolean rateStatus);

	@Query(value = "select rwb from RoadwayBill rwb where (:rwbId is null or rwb.rwbId=:rwbId) and (:tractorNumber is null or rwb.tractor.numberPlate=:tractorNumber)")
	List<RoadwayBill> findAllByIdAndTractor(@Param(value = "rwbId") String rwbId,
			@Param(value = "tractorNumber") String tractorNumber);

	@Query(value = "select rwb from RoadwayBill rwb where (:rwbId is null or rwb.rwbId=:rwbId) and (:tractorNumber is null or rwb.rentalVehicle.vehicleNumber=:tractorNumber)")
	List<RoadwayBill> findAllByIdAndRentalVehicle(@Param(value = "rwbId") String rwbId,
			@Param(value = "tractorNumber") String tractorNumber);

	@Query(value = "select rwb from RoadwayBill rwb where (:startDate is null or rwb.rwbDate>=:startDate) and (:endDate is null or rwb.rwbDate<=:endDate) and (:customer is null or (rwb.customer=:customer or rwb.broker=:customer)) and (:taxState is null or rwb.taxState=:taxState) and (:invoiceStatus is null or rwb.invoiceStatus=:invoiceStatus)")
	List<RoadwayBill> filterRoadwayBills(@Param(value = "startDate") Date startDate,
			@Param(value = "endDate") Date endDate, @Param(value = "customer") Company customer,
			@Param(value = "taxState") DState taxState, @Param(value = "invoiceStatus") DInvoiceStatus invoiceStatus);

}
