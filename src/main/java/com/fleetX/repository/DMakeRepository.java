package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DMake;

public interface DMakeRepository extends JpaRepository<DMake, Integer> ,QueryByExampleExecutor<DMake>{
	public List<DMake> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DMake findByValue(String value);
}
