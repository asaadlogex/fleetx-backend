package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DPaymentMode;

public interface DPaymentModeRepository extends JpaRepository<DPaymentMode, Integer>, QueryByExampleExecutor<DPaymentMode> {

	List<DPaymentMode> findAllByStatus(String statusActive);

	boolean existsByValue(String value);
}
