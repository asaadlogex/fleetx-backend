package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DRWBStatus;

public interface DRWBStatusRepository extends JpaRepository<DRWBStatus, Integer>, QueryByExampleExecutor<DRWBStatus> {

	List<DRWBStatus> findAllByStatus(String statusActive);
	
	DRWBStatus findByValue(String value);

	boolean existsByValue(String value);

}
