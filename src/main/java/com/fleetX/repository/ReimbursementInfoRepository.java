package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.account.ReimbursementInfo;

public interface ReimbursementInfoRepository extends JpaRepository<ReimbursementInfo, Integer> {

}
