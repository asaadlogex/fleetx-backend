package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.company.FuelCard;

public interface FuelCardRepository extends JpaRepository<FuelCard, Integer>,QueryByExampleExecutor<FuelCard> {

}
