package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.maintenance.WorkOrderItemDetail;

public interface WorkOrderItemDetailRepository extends JpaRepository<WorkOrderItemDetail, Integer> {

}
