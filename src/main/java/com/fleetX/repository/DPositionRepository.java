package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DPosition;

public interface DPositionRepository extends JpaRepository<DPosition, Integer>,QueryByExampleExecutor<DPosition> {
	public DPosition findByValue(String value);
	public List<DPosition> findAllByStatus(String status);
	public boolean existsByValue(String value);
}
