package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.order.Income;

public interface IncomeRepository extends JpaRepository<Income, Integer>, QueryByExampleExecutor<Income> {

}
