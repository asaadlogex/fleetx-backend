package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer>, QueryByExampleExecutor<Contact> {

}
