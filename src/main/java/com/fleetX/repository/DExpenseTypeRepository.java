package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DExpenseType;

public interface DExpenseTypeRepository extends JpaRepository<DExpenseType, Integer>, QueryByExampleExecutor<DExpenseType> {
	List<DExpenseType> findAllByStatus(String status);
	DExpenseType findByValue(String value);
	boolean existsByValue(String value);
}
