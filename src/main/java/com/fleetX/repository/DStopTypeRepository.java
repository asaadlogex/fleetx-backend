package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DStopType;

public interface DStopTypeRepository extends JpaRepository<DStopType, Integer>, QueryByExampleExecutor<DStopType> {
	DStopType findByValue(String value);
	public List<DStopType> findAllByStatus(String status);
	boolean existsByValue(String value);
}
