package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DPrivilege;

public interface DPrivilegeRepository extends JpaRepository<DPrivilege, Integer>,QueryByExampleExecutor<DPrivilege> {
	public DPrivilege findByValue(String value);
	public List<DPrivilege> findAllByStatus(String status);
	
	@Query(nativeQuery = true,value = "select status from d_privilege as p where p.privilege_name=:privilegeName")
	public String findStatusByPrivilegeName(String privilegeName);
	public boolean existsByValue(String value);
}
