package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DJobStatus;

public interface DJobStatusRepository extends JpaRepository<DJobStatus, Integer>, QueryByExampleExecutor<DJobStatus> {

	List<DJobStatus> findAllByStatus(String statusActive);

	boolean existsByValue(String value);

	DJobStatus findByValue(String status);

}
