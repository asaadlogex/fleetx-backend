package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.account.LineItem;

public interface LineItemRepository extends JpaRepository<LineItem, Integer> {

}
