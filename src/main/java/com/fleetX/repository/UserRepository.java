package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.User;

public interface UserRepository extends JpaRepository<User, String>, QueryByExampleExecutor<User> {

	@Query(nativeQuery = true, value = "select privilege_name from additional_privileges as ap where ap.username=:username")
	List<String> findAdditionalPrivilegesByUsername(String username);
	
	@Query(nativeQuery = true, value = "select role_name from users_roles as ur where ur.username=:username")
	List<String> findRoleNamesByUsername(String username);
}
