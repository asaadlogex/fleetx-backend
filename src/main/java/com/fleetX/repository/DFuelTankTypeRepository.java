package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DFuelTankType;

public interface DFuelTankTypeRepository extends JpaRepository<DFuelTankType, Integer>,QueryByExampleExecutor<DFuelTankType>{

	List<DFuelTankType> findAllByStatus(String statusActive);

	boolean existsByValue(String value);

}
