package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DItemCategory;

public interface DItemCategoryRepository extends JpaRepository<DItemCategory, Integer> {

	List<DItemCategory> findAllByStatus(String statusActive);

	boolean existsByCode(String code);

	boolean existsByValue(String value);

}
