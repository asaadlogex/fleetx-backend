package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DBaseStation;

public interface DBaseStationRepository extends JpaRepository<DBaseStation, Integer>,QueryByExampleExecutor<DBaseStation> {
	public List<DBaseStation> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DBaseStation findByValue(String baseStation);
}
