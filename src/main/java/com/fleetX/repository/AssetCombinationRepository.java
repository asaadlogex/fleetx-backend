package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.AssetCombination;

public interface AssetCombinationRepository extends JpaRepository<AssetCombination, String>,QueryByExampleExecutor<AssetCombination>  {

}
