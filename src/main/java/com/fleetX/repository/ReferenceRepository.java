package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.employee.Reference;

public interface ReferenceRepository extends JpaRepository<Reference, String>,QueryByExampleExecutor<Reference> {

}
