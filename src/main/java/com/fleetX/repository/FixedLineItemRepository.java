package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.account.FixedLineItem;

public interface FixedLineItemRepository extends JpaRepository<FixedLineItem, Integer> {

}
