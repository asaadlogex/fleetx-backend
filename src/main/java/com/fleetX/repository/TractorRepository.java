package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.Tractor;

public interface TractorRepository extends JpaRepository<Tractor, String>, QueryByExampleExecutor<Tractor> {
	public Tractor findByNumberPlate(String number);
}
