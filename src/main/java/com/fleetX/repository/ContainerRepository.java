package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.Container;

public interface ContainerRepository extends JpaRepository<Container, String>,QueryByExampleExecutor<Container> {

}
