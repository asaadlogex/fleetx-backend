package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DCategory;

public interface DCategoryRepository extends JpaRepository<DCategory, Integer>,QueryByExampleExecutor<DCategory>{

	public List<DCategory> findAllByStatus(String status);

	public boolean existsByValue(String value);
}
