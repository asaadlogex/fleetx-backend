package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DChannel;

public interface DChannelRepository extends JpaRepository<DChannel, Integer> {
	public DChannel findByValue(String value);
	public List<DChannel> findAllByStatus(String status);
	public boolean existsByValue(String value);
}
