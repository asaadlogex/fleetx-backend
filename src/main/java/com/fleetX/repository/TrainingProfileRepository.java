package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.employee.TrainingProfile;

public interface TrainingProfileRepository extends JpaRepository<TrainingProfile, String> ,QueryByExampleExecutor<TrainingProfile>{

}
