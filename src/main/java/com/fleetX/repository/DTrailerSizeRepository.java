package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DTrailerSize;

public interface DTrailerSizeRepository extends JpaRepository<DTrailerSize, Integer>,QueryByExampleExecutor<DTrailerSize> {
	public List<DTrailerSize> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DTrailerSize findByValue(String value);
}
