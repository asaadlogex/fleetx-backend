package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.order.Warehouse;

public interface WarehouseRepository extends JpaRepository<Warehouse, Integer>, QueryByExampleExecutor<Warehouse> {

}
