package com.fleetX.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fleetX.entity.account.Adjustment;

public interface AdjustmentRepository extends JpaRepository<Adjustment, Integer> {
	@Query(value = "select adjustment from Adjustment adjustment where adjustment.date>=:start and adjustment.date<=:end")
	public List<Adjustment> filterAdjustment(@Param(value = "start") Date start, @Param(value = "end") Date end);

	@Query(value = "select adjustment from Adjustment adjustment where (:key is null or adjustment.accountHead=:key) and (:start is null or adjustment.date>=:start) and (:end is null or adjustment.date<=:end)")
	public List<Adjustment> findAdjustments(@Param(value = "key") String key, @Param(value = "start") Date start,
			@Param(value = "end") Date end);
}
