package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.maintenance.Inventory;

public interface InventoryRepository extends JpaRepository<Inventory, Integer>, QueryByExampleExecutor<Inventory> {

}
