package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DWorkOrderStatus;

public interface DWorkOrderStatusRepository extends JpaRepository<DWorkOrderStatus, Integer> {
	DWorkOrderStatus findByValue(String value);

	List<DWorkOrderStatus> findAllByStatus(String status);

}
