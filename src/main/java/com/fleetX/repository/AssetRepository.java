package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.asset.Asset;

public interface AssetRepository extends JpaRepository<Asset, String>,QueryByExampleExecutor<Asset> {

}
