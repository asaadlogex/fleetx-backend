package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.maintenance.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}
