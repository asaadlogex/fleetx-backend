package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DFormation;

public interface DFormationRepository extends JpaRepository<DFormation, Integer> ,QueryByExampleExecutor<DFormation>{
	public List<DFormation> findAllByStatus(String status);

	public boolean existsByValue(String value);
}
