package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.maintenance.PurchaseOrder;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Integer> {

}
