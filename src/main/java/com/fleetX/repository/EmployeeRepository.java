package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DPosition;
import com.fleetX.entity.employee.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>,QueryByExampleExecutor<Employee> {

	public List<Employee> findAllByPosition(DPosition driverPosition);
}
