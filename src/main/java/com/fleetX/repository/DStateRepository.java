package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.dropdown.DState;

public interface DStateRepository extends JpaRepository<DState, Integer>, QueryByExampleExecutor<DState> {

	public List<DState> findAllByStatus(String status);

	public boolean existsByValue(String value);

	public DState findByValue(String taxState);

}
