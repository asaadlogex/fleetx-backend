package com.fleetX.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.fleetX.entity.order.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>, QueryByExampleExecutor<Product> {

}
