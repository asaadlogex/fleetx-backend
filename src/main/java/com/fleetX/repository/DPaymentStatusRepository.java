package com.fleetX.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fleetX.entity.dropdown.DPaymentStatus;

public interface DPaymentStatusRepository extends JpaRepository<DPaymentStatus, Integer> {

	List<DPaymentStatus> findAllByStatus(String statusActive);

	DPaymentStatus findByValue(String paymentStatusPending);

}
