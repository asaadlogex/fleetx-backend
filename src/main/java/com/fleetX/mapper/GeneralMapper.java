package com.fleetX.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.AddressDTO;
import com.fleetX.dto.ContactDTO;
import com.fleetX.dto.RoleDTO;
import com.fleetX.dto.UserDTO;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.service.IAdminService;

@Component
public class GeneralMapper {
	@Autowired
	IAdminService adminService;
	@Autowired
	SystemUtil systemUtil;

	public Role mapToRole(RoleDTO roleDto) {
		Assert.notNull(roleDto, "Role: " + SystemType.MUST_NOT_BE_NULL);
		Role role = new Role();
		role.setRoleName(roleDto.getRoleName());
		role.setPrivileges(roleDto.getPrivileges());
		return role;
	}

	public User mapToUser(UserDTO userDto) {
		Assert.notNull(userDto, "User: " + SystemType.MUST_NOT_BE_NULL);
		User user = new User();
		user.setUsername(userDto.getUsername());
		user.setPassword(userDto.getPassword());
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setStatus(userDto.getStatus());
		List<Role> roles = new ArrayList<>();
		Role role;
		if (userDto.getRoles() != null && userDto.getRoles().size() > 0) {
			for (RoleDTO roleDto : userDto.getRoles()) {
				role = adminService.getRoleByRoleName(roleDto.getRoleName());
				roles.add(role);
			}
		}
		user.setRoles(roles);
		if (userDto.getAdditionalPrivileges() != null && userDto.getAdditionalPrivileges().size() > 0) {
			user.setAdditionalPrivileges(userDto.getAdditionalPrivileges());
		}
		return user;
	}

	public UserDTO mapToUserDTO(User user) {
		Assert.notNull(user, "User: " + SystemType.MUST_NOT_BE_NULL);
		UserDTO userDto = new UserDTO();
		userDto.setUsername(user.getUsername());
//		userDto.setPassword(user.getPassword());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setStatus(user.getStatus());
		List<RoleDTO> roleDtos = new ArrayList<>();
		if (user.getRoles() != null && user.getRoles().size() > 0) {
			for (Role role : user.getRoles()) {
				roleDtos.add(mapToRoleDTO(role));
			}
		}
		userDto.setRoles(roleDtos);
		if (user.getAdditionalPrivileges() != null && user.getAdditionalPrivileges().size() > 0) {
			userDto.setAdditionalPrivileges(user.getAdditionalPrivileges());
		}
		return userDto;
	}

	public RoleDTO mapToRoleDTO(Role role) {
		Assert.notNull(role, "Role: " + SystemType.MUST_NOT_BE_NULL);
		RoleDTO roleDto = new RoleDTO();
		roleDto.setRoleName(role.getRoleName());
		roleDto.setPrivileges(role.getPrivileges());
		return roleDto;
	}
	
	public List<ContactDTO> mapToContactDTO(List<Contact> contacts) {
		Assert.notNull(contacts, "Contacts: "+SystemType.MUST_NOT_BE_NULL);
		List<ContactDTO> contactDtos = new ArrayList<>();
		for (Contact contact : contacts) {
			contactDtos.add(mapToContactDTO(contact));
		}
		return contactDtos;
	}

	public List<Contact> mapToContact(List<ContactDTO> contactDtos) {
		Assert.notNull(contactDtos, "Contacts: "+SystemType.MUST_NOT_BE_NULL);
		List<Contact> contacts = new ArrayList<>();
		for (ContactDTO contact : contactDtos) {
			contacts.add(mapToContact(contact));
		}
		return contacts;
	}

	public List<Address> mapToAddress(List<AddressDTO> addresseDtos) {
		Assert.notNull(addresseDtos, "Addresses: "+SystemType.MUST_NOT_BE_NULL);
		List<Address> addresses = new ArrayList<>();
		for (AddressDTO address : addresseDtos) {
			addresses.add(mapToAddress(address));
		}
		return addresses;
	}

	public List<AddressDTO> mapToAddressDTO(List<Address> addresses) {
		Assert.notNull(addresses, "Addresses: "+SystemType.MUST_NOT_BE_NULL);
		List<AddressDTO> addresseDtos = new ArrayList<>();
		for (Address address : addresses) {
			addresseDtos.add(mapToAddressDTO(address));
		}
		return addresseDtos;
	}

	public AddressDTO mapToAddressDTO(Address address) {
		Assert.notNull(address, "Address: "+SystemType.MUST_NOT_BE_NULL);
		AddressDTO addressDto = new AddressDTO();
		addressDto.setAddressId(address.getAddressId());
		addressDto.setStreet1(address.getStreet1());
		addressDto.setStreet2(address.getStreet2());
		addressDto.setState(address.getState());
		addressDto.setZipCode(address.getZipCode());
		addressDto.setLatitude(address.getLatitude());
		addressDto.setLongitude(address.getLongitude());
		addressDto.setCity(address.getCity());
		addressDto.setCountry(address.getCountry());
		return addressDto;
	}

	public Address mapToAddress(AddressDTO addressDto) {
		Assert.notNull(addressDto, "Address: "+SystemType.MUST_NOT_BE_NULL);
		Address address = new Address();
		address.setAddressId(addressDto.getAddressId());
		address.setCity(addressDto.getCity());
		address.setCountry(addressDto.getCountry());
		address.setLatitude(addressDto.getLatitude());
		address.setLongitude(addressDto.getLongitude());
		address.setState(addressDto.getState());
		address.setStreet1(addressDto.getStreet1());
		address.setStreet2(addressDto.getStreet2());
		address.setZipCode(addressDto.getZipCode());
		return address;
	}

	public ContactDTO mapToContactDTO(Contact contact) {
		Assert.notNull(contact, "Contact: "+SystemType.MUST_NOT_BE_NULL);
		ContactDTO contactDto = new ContactDTO();
		contactDto.setContactId(contact.getContactId());
		contactDto.setChannel(contact.getChannel());
		contactDto.setData(contact.getData());
		return contactDto;
	}

	public Contact mapToContact(ContactDTO contactDto) {
		Assert.notNull(contactDto, "Contact: "+SystemType.MUST_NOT_BE_NULL);
		Contact contact = null;
		if (systemUtil.validateContact(contactDto)) {
			contact = new Contact();
			contact.setContactId(contactDto.getContactId());
			contact.setChannel(contactDto.getChannel());
			contact.setData(contactDto.getData());
			return contact;
		} else
			throw new IllegalArgumentException();
	}
}
