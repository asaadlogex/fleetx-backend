package com.fleetX.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.ExpenseDTO;
import com.fleetX.dto.IncomeDTO;
import com.fleetX.dto.JobDTO;
import com.fleetX.dto.JobFuelDetailDTO;
import com.fleetX.dto.ProductDTO;
import com.fleetX.dto.RentalVehicleDTO;
import com.fleetX.dto.RoadwayBillDTO;
import com.fleetX.dto.RoadwayBillSummaryDTO;
import com.fleetX.dto.StopDTO;
import com.fleetX.dto.StopProgressDTO;
import com.fleetX.dto.WarehouseDTO;
import com.fleetX.entity.Address;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RentalVehicle;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.entity.order.Warehouse;
import com.fleetX.repository.DChannelRepository;
import com.fleetX.repository.JobRepository;
import com.fleetX.repository.StopRepository;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;
import com.fleetX.service.IEmployeeService;
import com.fleetX.service.IOrderService;

@Component
public class OrderMapper {
	@Autowired
	AssetMapper assetMapper;
	@Autowired
	IAssetService assetService;
	@Autowired
	EmployeeMapper employeeMapper;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	GeneralMapper generalMapper;
	@Autowired
	IOrderService orderService;
	@Autowired
	IAdminService adminService;
	@Autowired
	IContractService contractService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	StopRepository stopRepository;
	@Autowired
	JobRepository jobRepository;
	@Autowired
	DChannelRepository channelRepository;
	@Autowired
	SystemUtil systemUtil;

	public Job mapToJob(JobDTO jobDto) throws ParseException {
		Assert.notNull(jobDto, "Job: " + SystemType.MUST_NOT_BE_NULL);
		Job job = new Job();
		job.setJobId(jobDto.getJobId());
		if (jobDto.getStartDate() != null)
			job.setStartDate(systemUtil.convertToDate(jobDto.getStartDate()));
		if (jobDto.getEndDate() != null)
			job.setEndDate(systemUtil.convertToDate(jobDto.getEndDate()));
		job.setStartBaseStation(jobDto.getStartBaseStation());
		job.setEndBaseStation(jobDto.getEndBaseStation());
		job.setJobAdvance(jobDto.getJobAdvance());
		job.setStartKm(jobDto.getStartKm());
		job.setEndKm(jobDto.getEndKm());
		job.setTotalKm(jobDto.getTotalKm());
		job.setJobStatus(jobDto.getJobStatus());
		return job;
	}

	private RentalVehicle mapToRentalVehicle(RentalVehicleDTO rentalVehicleDto) {
		Assert.notNull(rentalVehicleDto, "Rental Vehicle: " + SystemType.MUST_NOT_BE_NULL);
		RentalVehicle rentalVehicle = new RentalVehicle();
		rentalVehicle.setRentalVehicleId(rentalVehicleDto.getRentalVehicleId());
		rentalVehicle.setEquipmentType(rentalVehicleDto.getEquipmentType());
		rentalVehicle.setEquipmentSize(rentalVehicleDto.getEquipmentSize());
		rentalVehicle.setVehicleNumber(rentalVehicleDto.getVehicleNumber());
		if (rentalVehicleDto.getBroker() != null) {
			rentalVehicle.setBroker(companyService.findCompanyById(rentalVehicleDto.getBroker().getCompanyId()));
		}
		return rentalVehicle;
	}

	public JobFuelDetail mapToJobFuelDetail(JobFuelDetailDTO jfdDto) throws ParseException {
		Assert.notNull(jfdDto, "Job Fuel Detail: " + SystemType.MUST_NOT_BE_NULL);
		JobFuelDetail jfd = new JobFuelDetail();
		jfd.setJobFuelDetailId(jfdDto.getJobFuelDetailId());
		jfd.setCurrentKm(jfdDto.getCurrentKm());
		jfd.setFuelInLitre(jfdDto.getFuelInLitre());
		if (jfdDto.getFuelSlipDate() != null)
			jfd.setFuelDate(systemUtil.convertToDate(jfdDto.getFuelSlipDate()));
		jfd.setFuelSlipNumber(jfdDto.getFuelSlipNumber());
		jfd.setFuelTankType(jfdDto.getFuelTankType());
		jfd.setRatePerLitre(jfdDto.getRatePerLitre());
		if (jfdDto.getSupplier() != null)
			jfd.setSupplier(companyService.findCompanyById(jfdDto.getSupplier().getCompanyId()));
		jfd.setTotalAmount(jfdDto.getTotalAmount());
		if (jfdDto.getFuelCard() != null)
			jfd.setFuelCard(adminService.findFuelCardById(jfdDto.getFuelCard().getFuelCardId()));
		String jobId = jfdDto.getJobId();
		if (jobId != null) {
			jfd.setJob(orderService.getJobById(jobId));
		}
		if (jfdDto.getTractor() != null)
			jfd.setTractor(assetService.findTractorById(jfdDto.getTractor().getTractorId()));
		if (jfdDto.getTrailer() != null)
			jfd.setTrailer(assetService.findTrailerById(jfdDto.getTrailer().getTrailerId()));
		return jfd;
	}

	public RoadwayBill mapToRoadwayBill(RoadwayBillDTO rwbDto) throws ParseException {
		Assert.notNull(rwbDto, "Roadway Bill: " + SystemType.MUST_NOT_BE_NULL);
		RoadwayBill rwb = new RoadwayBill();
		String jobId = rwbDto.getJobId();
		rwb.setJob(orderService.getJobByIdAndStatus(jobId, SystemType.STATUS_JOB_OPEN));
		rwb.setRwbId(rwbDto.getRwbId());
		rwb.setIsEmpty(rwbDto.getIsEmpty());
		rwb.setWeightInTon(rwbDto.getWeightInTon());
		rwb.setPodUrl(rwbDto.getPodUrl());
		rwb.setRwbStatus(rwbDto.getRwbStatus());
		rwb.setEquipmentType(rwbDto.getEquipmentType());
		rwb.setInvoiceStatus(rwbDto.getInvoiceStatus());
		rwb.setTaxState(rwbDto.getTaxState());
		rwb.setTmsKm(rwbDto.getTmsKm());
		rwb.setTrackerKm(rwbDto.getTrackerKm());
		rwb.setIsArea(rwbDto.getIsArea());
		rwb.setCity(rwbDto.getCity());
		rwb.setVehicleOwnership(rwbDto.getVehicleOwnership());
		rwb.setAssetCombinationId(rwbDto.getAssetCombinationId());
		if (rwbDto.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {
			rwb.setRentalVehicle(mapToRentalVehicle(rwbDto.getRentalVehicle()));
		} else {
			if (rwbDto.getContainer() != null)
				rwb.setContainer(assetService.findContainerById(rwbDto.getContainer().getContainerId()));
			if (rwbDto.getTractor() != null)
				rwb.setTractor(assetService.findTractorById(rwbDto.getTractor().getTractorId()));
			if (rwbDto.getTrailer() != null)
				rwb.setTrailer(assetService.findTrailerById(rwbDto.getTrailer().getTrailerId()));
			if (rwbDto.getTracker() != null)
				rwb.setTracker(assetService.findTrackerById(rwbDto.getTracker().getTrackerId()));
			if (rwbDto.getDriver1() != null)
				rwb.setDriver1(employeeService.findEmployeeById(rwbDto.getDriver1().getEmployeeId()));
			if (rwbDto.getDriver2() != null)
				rwb.setDriver2(employeeService.findEmployeeById(rwbDto.getDriver2().getEmployeeId()));
		}
		if (rwbDto.getRwbDate() != null)
			rwb.setRwbDate(systemUtil.convertToDate(rwbDto.getRwbDate()));
		if (rwbDto.getRoute() != null) {
			rwb.setRoute(adminService.findRouteById(rwbDto.getRoute().getRouteId()));
			rwb.setTotalKm(rwb.getRoute().getAvgDistanceInKM());
		}
		rwb.setPaymentMode(rwbDto.getPaymentMode());
		rwb.setOrderNumber(rwbDto.getOrderNumber());
		rwb.setLoadNumber(rwbDto.getLoadNumber());
		rwb.setInstructions(rwbDto.getInstructions());
		rwb.setCustomerType(rwbDto.getCustomerType());
		rwb.setBolNumber(rwbDto.getBolNumber());
		if (rwbDto.getCustomer() != null)
			rwb.setCustomer(companyService.findCompanyById(rwbDto.getCustomer().getCompanyId()));
		else if (rwbDto.getBroker() != null) {
			rwb.setBroker(companyService.findCompanyById(rwbDto.getBroker().getCompanyId()));
		}
		return rwb;
	}

	public Expense mapToExpense(ExpenseDTO expenseDto) {
		Assert.notNull(expenseDto, "Expense: " + SystemType.MUST_NOT_BE_NULL);
		Expense expense = new Expense();
		expense.setExpenseId(expenseDto.getExpenseId());
		expense.setAmount(expenseDto.getAmount());
		expense.setExpenseType(expenseDto.getExpenseType());
		expense.setDescription(expenseDto.getDescription());
		if (expenseDto.getRoadwayBillId() != null) {
			expense.setRoadwayBill(orderService.getRoadwayBillById(expenseDto.getRoadwayBillId()));
		}
		return expense;
	}

	public Income mapToIncome(IncomeDTO incomeDto) {
		Assert.notNull(incomeDto, "Income: " + SystemType.MUST_NOT_BE_NULL);
		Income income = new Income();
		income.setIncomeId(incomeDto.getIncomeId());
		income.setAmount(incomeDto.getAmount());
		income.setIncomeType(incomeDto.getIncomeType());
		income.setDescription(incomeDto.getDescription());
		if (incomeDto.getRoadwayBillId() != null) {
			income.setRoadwayBill(orderService.getRoadwayBillById(incomeDto.getRoadwayBillId()));
		}
		return income;
	}

	public Product mapToProduct(ProductDTO productDto) {
		Assert.notNull(productDto, "Product: " + SystemType.MUST_NOT_BE_NULL);
		Product product = new Product();
		product.setProductId(productDto.getProductId());
		product.setCategory(productDto.getCategory());
		product.setCommodity(productDto.getCommodity());
		product.setPackageCount(productDto.getPackageCount());
		product.setWeight(productDto.getWeight());
		product.setWeightUnit(productDto.getWeightUnit());
		product.setTemperature(productDto.getTemperature());
		if (productDto.getRoadwayBillId() != null) {
			product.setRoadwayBill(orderService.getRoadwayBillById(productDto.getRoadwayBillId()));
		}
		return product;
	}

	public StopProgress mapToStopProgress(StopProgressDTO stopProgressDto) throws ParseException {
		Assert.notNull(stopProgressDto, "Stop Progress: " + SystemType.MUST_NOT_BE_NULL);
		StopProgress stopProgress = new StopProgress();
		stopProgress.setStopProgressId(stopProgressDto.getStopProgressId());
		String arrival = stopProgressDto.getArrivalTime();
		String departure = stopProgressDto.getDepartureTime();
		if (arrival != null)
			stopProgress.setArrivalTime(systemUtil.convertToDateTime(arrival));
		if (departure != null)
			stopProgress.setDepartureTime(systemUtil.convertToDateTime(departure));
		stopProgress.setDetentionInHrs(stopProgressDto.getDetentionInHrs());
		if (stopProgressDto.getWarehouse() != null)
			stopProgress.setWarehouse(mapToWarehouse(stopProgressDto.getWarehouse()));
		if (stopProgressDto.getRoadwayBillId() != null) {
			RoadwayBill rwb = orderService.getRoadwayBillById(stopProgressDto.getRoadwayBillId());
			stopProgress.setRoadwayBill(rwb);
			List<Stop> stops = rwb.getStops();
			Stop stop = null;
			if (stopProgressDto.getStopId() != null) {
				if (stops != null)
					for (Stop st : stops) {
						if (st.getStopId().equals(stopProgressDto.getStopId()))
							stop = st;
					}
			}
			stopProgress.setStop(stop);
		}
		stopProgress.setStopProgressType(stopProgressDto.getStopProgressType());
		stopProgress.setVehicleOwnership(stopProgressDto.getVehicleOwnership());
		if (stopProgressDto.getContainer() != null)
			stopProgress.setContainer(assetService.findContainerById(stopProgressDto.getContainer().getContainerId()));
		if (stopProgressDto.getTractor() != null)
			stopProgress.setTractor(assetService.findTractorById(stopProgressDto.getTractor().getTractorId()));
		if (stopProgressDto.getTrailer() != null)
			stopProgress.setTrailer(assetService.findTrailerById(stopProgressDto.getTrailer().getTrailerId()));
		if (stopProgressDto.getDriver1() != null)
			stopProgress.setDriver1(employeeService.findEmployeeById(stopProgressDto.getDriver1().getEmployeeId()));
		if (stopProgressDto.getDriver2() != null)
			stopProgress.setDriver2(employeeService.findEmployeeById(stopProgressDto.getDriver2().getEmployeeId()));

		if (stopProgressDto.getRentalVehicle() != null)
			stopProgress.setRentalVehicle(mapToRentalVehicle(stopProgressDto.getRentalVehicle()));
		return stopProgress;
	}

	public Stop mapToStop(StopDTO stopDto) throws ParseException {
		Assert.notNull(stopDto, "Stop: " + SystemType.MUST_NOT_BE_NULL);
		Stop stop = new Stop();
		stop.setStopId(stopDto.getStopId());
		stop.setStopSequenceNumber(stopDto.getStopSequenceNumber());
		stop.setStopType(stopDto.getStopType());
		stop.setReferenceNumber(stopDto.getReferenceNumber());
		String appointmentStart = stopDto.getAppointmentStartTime();
		String appointmentEnd = stopDto.getAppointmentEndTime();
		if (appointmentStart != null)
			stop.setAppointmentStartTime(systemUtil.convertToDateTime(appointmentStart));
		if (appointmentEnd != null)
			stop.setAppointmentEndTime(systemUtil.convertToDateTime(appointmentEnd));
		if (stopDto.getWarehouse() != null)
			stop.setWarehouse(mapToWarehouse(stopDto.getWarehouse()));
		if (stopDto.getRoadwayBillId() != null)
			stop.setRoadwayBill(orderService.getRoadwayBillById(stopDto.getRoadwayBillId()));
		return stop;
	}

	public Warehouse mapToWarehouse(WarehouseDTO warehouseDto) {
		Assert.notNull(warehouseDto, "Warehouse: " + SystemType.MUST_NOT_BE_NULL);
		Warehouse warehouse = new Warehouse();
		warehouse.setWarehouseId(warehouseDto.getWarehouseId());
		warehouse.setWarehouseName(warehouseDto.getName());
		List<Address> addresses = new ArrayList<>();
		addresses.add(generalMapper.mapToAddress(warehouseDto.getAddress()));
		warehouse.setAddresses(addresses);
		warehouse.setPhone(warehouseDto.getPhone());
		warehouse.setEmail(warehouseDto.getEmail());
		return warehouse;
	}

	public JobDTO mapToJobDTO(Job job) throws ParseException {
		Assert.notNull(job, "Job: " + SystemType.MUST_NOT_BE_NULL);
		JobDTO jobDto = new JobDTO();
		jobDto.setJobId(job.getJobId());

		if (job.getStartDate() != null)
			jobDto.setStartDate(systemUtil.convertToDateString(job.getStartDate()));
		if (job.getEndDate() != null)
			jobDto.setEndDate(systemUtil.convertToDateString(job.getEndDate()));

		if (job.getRentalVehicle() != null)
			jobDto.setRentalVehicle(mapToRentalVehicleDTO(job.getRentalVehicle()));
		if (job.getContainer() != null)
			jobDto.setContainer(assetMapper.mapToContainerDTO(job.getContainer()));
		if (job.getTractor() != null)
			jobDto.setTractor(assetMapper.mapToTractorSummaryDTO(job.getTractor()));
		if (job.getTrailer() != null)
			jobDto.setTrailer(assetMapper.mapToTrailerSummaryDTO(job.getTrailer()));
		if (job.getTracker() != null)
			jobDto.setTracker(assetMapper.mapToTrackerDTO(job.getTracker()));
		if (job.getDriver1() != null)
			jobDto.setDriver1(employeeMapper.mapToEmployeeSummaryDTO(job.getDriver1()));
		if (job.getDriver2() != null)
			jobDto.setDriver2(employeeMapper.mapToEmployeeSummaryDTO(job.getDriver2()));
		jobDto.setStartBaseStation(job.getStartBaseStation());
		jobDto.setEndBaseStation(job.getEndBaseStation());
		jobDto.setJobAdvance(job.getJobAdvance());
		jobDto.setStartKm(job.getStartKm());
		jobDto.setEndKm(job.getEndKm());
		jobDto.setOdometerKm(job.getOdometerKm());
		jobDto.setTotalKm(job.getTotalKm());
		jobDto.setTmsKm(job.getTmsKm());
		jobDto.setTrackerKm(job.getTrackerKm());
		jobDto.setJobStatus(job.getJobStatus());
		jobDto.setReimbursementStatus(job.getReimbursementStatus());
		if (job.getJobFuelDetails() != null && job.getJobFuelDetails().size() > 0)
			jobDto.setJobFuelDetails(mapToJobFuelDetailsDTO(job.getJobFuelDetails()));
		Double jobReimbursement = 0d;
		if (job.getRoadwayBills() != null && job.getRoadwayBills().size() > 0) {
			List<RoadwayBillSummaryDTO> roadwayBillDtos = new ArrayList<>();
			for (RoadwayBill rwb : job.getRoadwayBills()) {
				RoadwayBillSummaryDTO rwbSummary = mapToRoadwayBillSummaryDTO(rwb);
				jobReimbursement += rwbSummary.getReimbursementExpense();
				roadwayBillDtos.add(rwbSummary);
			}
			jobDto.setRoadwayBills(roadwayBillDtos);
		}
		jobDto.setReimbursementExpense(jobReimbursement);
		return jobDto;
	}

	private List<JobFuelDetailDTO> mapToJobFuelDetailsDTO(List<JobFuelDetail> jobFuelDetails) throws ParseException {
		Assert.notNull(jobFuelDetails, "Job Fuel Details: " + SystemType.MUST_NOT_BE_NULL);
		List<JobFuelDetailDTO> jfdDtos = new ArrayList<>();
		for (JobFuelDetail jobFuelDetail : jobFuelDetails) {
			jfdDtos.add(mapToJobFuelDetailDTO(jobFuelDetail));
		}
		return jfdDtos;
	}

	public JobFuelDetailDTO mapToJobFuelDetailDTO(JobFuelDetail jfd) throws ParseException {
		Assert.notNull(jfd, "Job Fuel Detail: " + SystemType.MUST_NOT_BE_NULL);
		JobFuelDetailDTO jfdDto = new JobFuelDetailDTO();
		jfdDto.setJobFuelDetailId(jfd.getJobFuelDetailId());
		jfdDto.setCurrentKm(jfd.getCurrentKm());
		jfdDto.setFuelInLitre(jfd.getFuelInLitre());
		if (jfd.getFuelDate() != null)
			jfdDto.setFuelSlipDate(systemUtil.convertToDateString(jfd.getFuelDate()));
		if (jfd.getTractor() != null)
			jfdDto.setTractor(assetMapper.mapToTractorSummaryDTO(jfd.getTractor()));
		if (jfd.getTrailer() != null)
			jfdDto.setTrailer(assetMapper.mapToTrailerSummaryDTO(jfd.getTrailer()));
		jfdDto.setFuelSlipNumber(jfd.getFuelSlipNumber());
		jfdDto.setFuelTankType(jfd.getFuelTankType());
		jfdDto.setRatePerLitre(jfd.getRatePerLitre());
		jfdDto.setSupplier(companyMapper.mapToCompanySummaryDTO(jfd.getSupplier()));
		jfdDto.setTotalAmount(jfd.getTotalAmount());
		if (jfd.getFuelCard() != null)
			jfdDto.setFuelCard(companyMapper.mapToFuelCardDTO(jfd.getFuelCard()));
		jfdDto.setJobId(jfd.getJob().getJobId());
		return jfdDto;
	}

	private RentalVehicleDTO mapToRentalVehicleDTO(RentalVehicle rentalVehicle) throws ParseException {
		Assert.notNull(rentalVehicle, "Rental Vehicle: " + SystemType.MUST_NOT_BE_NULL);
		RentalVehicleDTO rentalVehicleDto = new RentalVehicleDTO();
		rentalVehicleDto.setRentalVehicleId(rentalVehicle.getRentalVehicleId());
		rentalVehicleDto.setEquipmentType(rentalVehicle.getEquipmentType());
		rentalVehicleDto.setEquipmentSize(rentalVehicle.getEquipmentSize());
		rentalVehicleDto.setVehicleNumber(rentalVehicle.getVehicleNumber());
		rentalVehicleDto.setBroker(companyMapper.mapToCompanyDTO(rentalVehicle.getBroker()));
		return rentalVehicleDto;
	}

	public RoadwayBillDTO mapToRoadwayBillDTO(RoadwayBill rwb) throws ParseException {
		Assert.notNull(rwb, "Roadway Bill: " + SystemType.MUST_NOT_BE_NULL);
		RoadwayBillDTO rwbDto = new RoadwayBillDTO();
		rwbDto.setJobId(rwb.getJob().getJobId());
		rwbDto.setRwbId(rwb.getRwbId());
		rwbDto.setIsEmpty(rwb.getIsEmpty());
		rwbDto.setRwbStatus(rwb.getRwbStatus());
		rwbDto.setWeightInTon(rwb.getWeightInTon());
		rwbDto.setRateStatus(rwb.getRateStatus());
		rwbDto.setEquipmentType(rwb.getEquipmentType());
		rwbDto.setVehicleOwnership(rwb.getVehicleOwnership());
		rwbDto.setTotalKm(rwb.getTotalKm());
		rwbDto.setTmsKm(rwb.getTmsKm());
		rwbDto.setTrackerKm(rwb.getTrackerKm());
		rwbDto.setPodUrl(rwb.getPodUrl());
		rwbDto.setInvoiceStatus(rwb.getInvoiceStatus());
		rwbDto.setTaxState(rwb.getTaxState());
		rwbDto.setIsArea(rwb.getIsArea());
		rwbDto.setCity(rwb.getCity());
		rwbDto.setAssetCombinationId(rwb.getAssetCombinationId());
		if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {
			if (rwb.getRentalVehicle() != null)
				rwbDto.setRentalVehicle(mapToRentalVehicleDTO(rwb.getRentalVehicle()));
		} else {
			if (rwb.getContainer() != null)
				rwbDto.setContainer(assetMapper.mapToContainerDTO(rwb.getContainer()));
			if (rwb.getTractor() != null)
				rwbDto.setTractor(assetMapper.mapToTractorSummaryDTO(rwb.getTractor()));
			if (rwb.getTrailer() != null)
				rwbDto.setTrailer(assetMapper.mapToTrailerSummaryDTO(rwb.getTrailer()));
			if (rwb.getTracker() != null)
				rwbDto.setTracker(assetMapper.mapToTrackerDTO(rwb.getTracker()));
			if (rwb.getDriver1() != null)
				rwbDto.setDriver1(employeeMapper.mapToEmployeeSummaryDTO(rwb.getDriver1()));
			if (rwb.getDriver2() != null)
				rwbDto.setDriver2(employeeMapper.mapToEmployeeSummaryDTO(rwb.getDriver2()));
		}
		if (rwb.getRwbDate() != null)
			rwbDto.setRwbDate(systemUtil.convertToDateString(rwb.getRwbDate()));
		if (rwb.getRoute() != null)
			rwbDto.setRoute(companyMapper.mapToRouteDTO(rwb.getRoute()));
		rwbDto.setPaymentMode(rwb.getPaymentMode());
		rwbDto.setOrderNumber(rwb.getOrderNumber());
		rwbDto.setLoadNumber(rwb.getLoadNumber());
		rwbDto.setInstructions(rwb.getInstructions());
		rwbDto.setCustomerType(rwb.getCustomerType());
		rwbDto.setBolNumber(rwb.getBolNumber());
		if (rwb.getStops() != null && rwb.getStops().size() > 0) {
			rwbDto.setStops(mapToStopsDTO(rwb.getStops()));
		}
		if (rwb.getStopProgresses() != null && rwb.getStopProgresses().size() > 0) {
			rwbDto.setStopProgresses(mapToStopProgressesDTO(rwb.getStopProgresses()));
			List<StopProgress> stopProgresses = rwb.getStopProgresses();
			Date arrival = stopProgresses.get(0).getArrivalTime();
			Date departure;
			if (stopProgresses.size() > 1) {
				departure = stopProgresses.get(stopProgresses.size() - 1).getDepartureTime();
			} else {
				departure = stopProgresses.get(0).getDepartureTime();
			}
			Double duration = systemUtil.differenceInDateTime(arrival, departure);
			rwbDto.setDurationInHrs(duration);
		}
		if (rwb.getProducts() != null && rwb.getProducts().size() > 0)
			rwbDto.setProducts(mapToProductsDTO(rwb.getProducts()));
		if (rwb.getIncomes() != null && rwb.getIncomes().size() > 0)
			rwbDto.setIncomes(mapToIncomesDTO(rwb.getIncomes()));
		if (rwb.getExpenses() != null && rwb.getExpenses().size() > 0)
			rwbDto.setExpenses(mapToExpensesDTO(rwb.getExpenses()));
		if (rwb.getCustomer() != null)
			rwbDto.setCustomer(companyMapper.mapToCompanySummaryDTO(rwb.getCustomer()));
		else if (rwb.getBroker() != null) {
			rwbDto.setBroker(companyMapper.mapToCompanySummaryDTO(rwb.getBroker()));
		}
		Double totalIncome = getTotalIncomeFromRwb(rwb);
		Double totalExpense = getTotalExpenseFromRwb(rwb);
		Double netIncome = totalIncome - totalExpense;
		rwbDto.setTotalIncome(totalIncome);
		rwbDto.setTotalExpense(totalExpense);
		rwbDto.setNetIncome(netIncome);
		return rwbDto;
	}

	private Double getTotalExpenseFromRwb(RoadwayBill rwb) {
		Double total = 0d;
		List<Expense> expenses = rwb.getExpenses();
		if (expenses.size() > 0)
			for (Expense expense : expenses) {
				total += expense.getAmount();
			}
		return total;
	}

	private Double getTotalIncomeFromRwb(RoadwayBill rwb) {
		Double total = 0d;
		List<Income> incomes = rwb.getIncomes();
		if (incomes.size() > 0)
			for (Income income : incomes) {
				total += income.getAmount();
			}
		return total;
	}

	private List<StopDTO> mapToStopsDTO(List<Stop> stops) throws ParseException {
		Assert.notNull(stops, "Stops: " + SystemType.MUST_NOT_BE_NULL);
		List<StopDTO> stopDtos = new ArrayList<>();
		for (Stop stop : stops) {
			stopDtos.add(mapToStopDTO(stop));
		}
		return stopDtos;
	}

	public StopDTO mapToStopDTO(Stop stop) throws ParseException {
		Assert.notNull(stop, "Stop: " + SystemType.MUST_NOT_BE_NULL);
		StopDTO stopDto = new StopDTO();
		stopDto.setStopId(stop.getStopId());
		stopDto.setStopSequenceNumber(stop.getStopSequenceNumber());
		stopDto.setStopType(stop.getStopType());
		stopDto.setReferenceNumber(stop.getReferenceNumber());
		Date appointmentStart = stop.getAppointmentStartTime();
		Date appointmentEnd = stop.getAppointmentEndTime();
		if (stop.getAppointmentStartTime() != null)
			stopDto.setAppointmentStartTime(systemUtil.convertToDateTimeString(appointmentStart));
		if (stop.getAppointmentEndTime() != null)
			stopDto.setAppointmentEndTime(systemUtil.convertToDateTimeString(appointmentEnd));
		stopDto.setWarehouse(mapToWarehouseDTO(stop.getWarehouse()));
		if (stop.getRoadwayBill() != null)
			stopDto.setRoadwayBillId(stop.getRoadwayBill().getRwbId());
		return stopDto;

	}

	public WarehouseDTO mapToWarehouseDTO(Warehouse warehouse) {
		Assert.notNull(warehouse, "Warehouse: " + SystemType.MUST_NOT_BE_NULL);
		WarehouseDTO warehouseDto = new WarehouseDTO();
		warehouseDto.setWarehouseId(warehouse.getWarehouseId());
		warehouseDto.setName(warehouse.getWarehouseName());
		warehouseDto.setAddress(generalMapper.mapToAddressDTO(warehouse.getAddresses()).get(0));
		warehouseDto.setPhone(warehouse.getPhone());
		warehouseDto.setEmail(warehouse.getEmail());
		return warehouseDto;

	}

	private List<StopProgressDTO> mapToStopProgressesDTO(List<StopProgress> stopProgresses) throws ParseException {
		Assert.notNull(stopProgresses, "Stop Progresses: " + SystemType.MUST_NOT_BE_NULL);
		List<StopProgressDTO> stopProgressDtos = new ArrayList<>();
		for (StopProgress stopProgress : stopProgresses) {
			stopProgressDtos.add(mapToStopProgressDTO(stopProgress));
		}
		return stopProgressDtos;
	}

	public StopProgressDTO mapToStopProgressDTO(StopProgress stopProgress) throws ParseException {
		Assert.notNull(stopProgress, "Stop Progress: " + SystemType.MUST_NOT_BE_NULL);
		StopProgressDTO stopProgressDto = new StopProgressDTO();
		stopProgressDto.setStopProgressId(stopProgress.getStopProgressId());
		Date arrival = stopProgress.getArrivalTime();
		Date departure = stopProgress.getDepartureTime();
		if (stopProgress.getArrivalTime() != null)
			stopProgressDto.setArrivalTime(systemUtil.convertToDateTimeString(arrival));
		if (stopProgress.getDepartureTime() != null)
			stopProgressDto.setDepartureTime(systemUtil.convertToDateTimeString(departure));
		stopProgressDto.setDetentionInHrs(stopProgress.getDetentionInHrs());
		stopProgressDto.setVehicleOwnership(stopProgress.getVehicleOwnership());

		if (stopProgress.getContainer() != null)
			stopProgressDto.setContainer(assetMapper.mapToContainerDTO(stopProgress.getContainer()));
		if (stopProgress.getTractor() != null)
			stopProgressDto.setTractor(assetMapper.mapToTractorSummaryDTO(stopProgress.getTractor()));
		if (stopProgress.getTrailer() != null)
			stopProgressDto.setTrailer(assetMapper.mapToTrailerSummaryDTO(stopProgress.getTrailer()));
		if (stopProgress.getDriver1() != null)
			stopProgressDto.setDriver1(employeeMapper.mapToEmployeeSummaryDTO(stopProgress.getDriver1()));
		if (stopProgress.getDriver2() != null)
			stopProgressDto.setDriver2(employeeMapper.mapToEmployeeSummaryDTO(stopProgress.getDriver2()));
		if (stopProgress.getRentalVehicle() != null)
			stopProgressDto.setRentalVehicle(mapToRentalVehicleDTO(stopProgress.getRentalVehicle()));
		stopProgressDto.setStopProgressType(stopProgress.getStopProgressType());
		if (stopProgress.getRoadwayBill() != null)
			stopProgressDto.setRoadwayBillId(stopProgress.getRoadwayBill().getRwbId());
		if (stopProgress.getStop() != null)
			stopProgressDto.setStopId(stopProgress.getStop().getStopId());
		stopProgressDto.setWarehouse(mapToWarehouseDTO(stopProgress.getWarehouse()));
		return stopProgressDto;
	}

	private List<ProductDTO> mapToProductsDTO(List<Product> products) {
		Assert.notNull(products, "Products: " + SystemType.MUST_NOT_BE_NULL);
		List<ProductDTO> productDtos = new ArrayList<>();
		for (Product product : products) {
			productDtos.add(mapToProductDTO(product));
		}
		return productDtos;
	}

	public ProductDTO mapToProductDTO(Product product) {
		Assert.notNull(product, "Product: " + SystemType.MUST_NOT_BE_NULL);
		ProductDTO productDto = new ProductDTO();
		productDto.setProductId(product.getProductId());
		productDto.setCategory(product.getCategory());
		productDto.setCommodity(product.getCommodity());
		productDto.setPackageCount(product.getPackageCount());
		productDto.setWeight(product.getWeight());
		productDto.setWeightUnit(product.getWeightUnit());
		productDto.setTemperature(product.getTemperature());
		if (product.getRoadwayBill() != null) {
			productDto.setRoadwayBillId(product.getRoadwayBill().getRwbId());
		}
		return productDto;
	}

	private List<ExpenseDTO> mapToExpensesDTO(List<Expense> expenses) {
		Assert.notNull(expenses, "Expenses: " + SystemType.MUST_NOT_BE_NULL);
		List<ExpenseDTO> expenseDtos = new ArrayList<>();
		for (Expense expense : expenses) {
			expenseDtos.add(mapToExpenseDTO(expense));
		}
		return expenseDtos;
	}

	public ExpenseDTO mapToExpenseDTO(Expense expense) {
		Assert.notNull(expense, "Expense: " + SystemType.MUST_NOT_BE_NULL);
		ExpenseDTO expenseDto = new ExpenseDTO();
		expenseDto.setExpenseId(expense.getExpenseId());
		expenseDto.setAmount(expense.getAmount());
		expenseDto.setExpenseType(expense.getExpenseType());
		expenseDto.setDescription(expense.getDescription());
		expenseDto.setRoadwayBillId(expense.getRoadwayBill().getRwbId());
		return expenseDto;
	}

	private List<IncomeDTO> mapToIncomesDTO(List<Income> incomes) {
		Assert.notNull(incomes, "Incomes: " + SystemType.MUST_NOT_BE_NULL);
		List<IncomeDTO> incomeDtos = new ArrayList<>();
		for (Income income : incomes) {
			incomeDtos.add(mapToIncomeDTO(income));
		}
		return incomeDtos;
	}

	public IncomeDTO mapToIncomeDTO(Income income) {
		Assert.notNull(income, "Income: " + SystemType.MUST_NOT_BE_NULL);
		IncomeDTO incomeDto = new IncomeDTO();
		incomeDto.setIncomeId(income.getIncomeId());
		incomeDto.setAmount(income.getAmount());
		incomeDto.setIncomeType(income.getIncomeType());
		incomeDto.setDescription(income.getDescription());
		incomeDto.setRoadwayBillId(income.getRoadwayBill().getRwbId());
		return incomeDto;
	}

	public RoadwayBillSummaryDTO mapToRoadwayBillSummaryDTO(RoadwayBill rwb) throws ParseException {
		Assert.notNull(rwb, "Roadway Bill: " + SystemType.MUST_NOT_BE_NULL);
		RoadwayBillSummaryDTO rwbDto = new RoadwayBillSummaryDTO();
		rwbDto.setRwbId(rwb.getRwbId());
		rwbDto.setRwbStatus(rwb.getRwbStatus());
		rwbDto.setVehicleOwnership(rwb.getVehicleOwnership());
		rwbDto.setRateStatus(rwb.getRateStatus());
		rwbDto.setTrackerKm(rwb.getTrackerKm());
		rwbDto.setReimbursementExpense(orderService.getReimbursementExpenseOfRoadwayBill(rwb));
		rwbDto.setTotalExpense(orderService.getExpenseOfRoadwayBill(rwb));
		if (rwb.getTractor() != null) {
			rwbDto.setTractorNumber(rwb.getTractor().getNumberPlate());
			if (rwb.getTrailer() != null) {
				rwbDto.setTrailerNumber(rwb.getTrailer().getAsset().getVin());
				rwbDto.setTrailerTypeAndSize(rwb.getTrailer().getTrailerType().getValue() + " "
						+ rwb.getTrailer().getTrailerSize().getValue());
			}
			if (rwb.getContainer() != null) {
				rwbDto.setContainerNumber(rwb.getContainer().getContainerNumber());
			}
			if (rwb.getTracker() != null) {
				rwbDto.setTrackerNumber(rwb.getTracker().getTrackerNumber());
			}
			if (rwb.getDriver1() != null) {
				rwbDto.setDriver1(rwb.getDriver1().getFirstName() + " " + rwb.getDriver1().getLastName());
			}
			if (rwb.getDriver2() != null) {
				rwbDto.setDriver2(rwb.getDriver2().getFirstName() + " " + rwb.getDriver2().getLastName());
			}
		} else if (rwb.getRentalVehicle() != null) {
			rwbDto.setTractorNumber(rwb.getRentalVehicle().getVehicleNumber());
			rwbDto.setVehicleOwner(rwb.getRentalVehicle().getBroker().getCompanyName());
			rwbDto.setTrailerTypeAndSize(rwb.getRentalVehicle().getEquipmentType().getValue() + " "
					+ rwb.getRentalVehicle().getEquipmentSize().getValue());
		}
		if (rwb.getRwbDate() != null)
			rwbDto.setRwbDate(systemUtil.convertToDateString(rwb.getRwbDate()));
		if (rwb.getRoute() != null)
			rwbDto.setRoute(companyMapper.mapToRouteDTO(rwb.getRoute()));
		rwbDto.setPaymentMode(rwb.getPaymentMode());
		rwbDto.setOrderNumber(rwb.getOrderNumber());
		rwbDto.setCustomerType(rwb.getCustomerType());
		rwbDto.setInvoiceStatus(rwb.getInvoiceStatus());
		rwbDto.setTaxState(rwb.getTaxState());
		Double totalIncome = 0d;
		for (Income income : rwb.getIncomes()) {
			totalIncome += income.getAmount();
		}
		rwbDto.setTotalIncome(totalIncome);
		if (rwb.getCustomer() != null) {
			rwbDto.setCustomer(rwb.getCustomer().getCompanyName());
		} else if (rwb.getBroker() != null) {
			rwbDto.setBroker(rwb.getBroker().getCompanyName());
		}

		List<StopProgress> stopProgresses = rwb.getStopProgresses();
		if (stopProgresses.size() > 0) {
			Date arrival = stopProgresses.get(0).getArrivalTime();
			Date departure;
			if (stopProgresses.size() > 1) {
				departure = stopProgresses.get(stopProgresses.size() - 1).getDepartureTime();
			} else {
				departure = stopProgresses.get(0).getDepartureTime();
			}
			Double duration = systemUtil.differenceInDateTime(arrival, departure);
			rwbDto.setDurationInHrs(duration);
		}
		return rwbDto;
	}
}
