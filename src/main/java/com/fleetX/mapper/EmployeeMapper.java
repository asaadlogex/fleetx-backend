package com.fleetX.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.EmployeeContractDTO;
import com.fleetX.dto.EmployeeDTO;
import com.fleetX.dto.EmployeeSummaryDTO;
import com.fleetX.dto.HeavyVehicleExperienceDTO;
import com.fleetX.dto.MedicalTestReportDTO;
import com.fleetX.dto.ReferenceDTO;
import com.fleetX.dto.TrainingProfileDTO;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;
import com.fleetX.service.IAdminService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IEmployeeService;

@Component
public class EmployeeMapper {
	@Autowired
	IAdminService adminService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	GeneralMapper generalMapper;
	@Autowired
	SystemUtil systemUtil;

	public Employee mapToEmployee(EmployeeDTO employeeDto) throws ParseException {
		Assert.notNull(employeeDto, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		Employee employee = new Employee();
		employee.setEmployeeId(employeeDto.getEmployeeId());
		employee.setStatus(employeeDto.getStatus());
		employee.setPosition(employeeDto.getPosition());
		employee.setPictureUrl(employeeDto.getPictureUrl());
		employee.setNtnNumber(employeeDto.getNtnNumber());
		employee.setNextOfKinRelation(employeeDto.getNextOfKinRelation());
		employee.setNextOfKin(employeeDto.getNextOfKin());
		employee.setFirstName(employeeDto.getFirstName());
		employee.setLastName(employeeDto.getLastName());
		employee.setEmployeeNumber(employeeDto.getEmployeeNumber());
		employee.setDepartment(employeeDto.getDepartment());
		if (employeeDto.getDateOfBirth() != null)
			employee.setDateOfBirth(systemUtil.convertToDate(employeeDto.getDateOfBirth()));
		employee.setCnic(employeeDto.getCnic());
		if (employeeDto.getCnicExpiry() != null)
			employee.setCnicExpiry(systemUtil.convertToDate(employeeDto.getCnicExpiry()));
		employee.setAcAddress(generalMapper.mapToAddress(employeeDto.getBusinessAddress()));
		if (employeeDto.getEmployeeAddresses() != null && employeeDto.getEmployeeAddresses().size() > 0)
			employee.setEmployeeAddresses(generalMapper.mapToAddress(employeeDto.getEmployeeAddresses()));
		if (employeeDto.getEmployeeContacts() != null && employeeDto.getEmployeeContacts().size() > 0)
			employee.setEmployeeContacts(generalMapper.mapToContact(employeeDto.getEmployeeContacts()));
		if (employeeDto.getEmployeeContracts() != null && employeeDto.getEmployeeContracts().size() > 0)
			employee.setEmployeeContracts(mapToEmployeeContracts(employeeDto.getEmployeeContracts()));
		employee.setAdminCompany(adminService.findAdminCompanyById(employeeDto.getCompanyId()));
		employee.setEmployeeType(employeeDto.getEmployeeType());
		if (employeeDto.getEmployeeType().getValue().equals(SystemType.CONTRACTUAL)) {
			employee.setSupplier(companyService.findCompanyById(employeeDto.getSupplierId()));
		}
		if (employeeDto.getPosition().getValue().equals(SystemType.POSITION_DRIVER)) {
			employee.setLicenceNumber(employeeDto.getLicenceNumber());
			if (employeeDto.getLicenceExpiry() != null)
				employee.setLicenceExpiry(systemUtil.convertToDate(employeeDto.getLicenceExpiry()));
			employee.setLicenceClass(employeeDto.getLicenceClass());
		}
		return employee;
	}

	private List<EmployeeContract> mapToEmployeeContracts(List<EmployeeContractDTO> employeeContracts)
			throws ParseException {
		Assert.notNull(employeeContracts, "Employee Contracts: " + SystemType.MUST_NOT_BE_NULL);
		List<EmployeeContract> contacts = new ArrayList<>();
		for (EmployeeContractDTO contract : employeeContracts) {
			contacts.add(mapToEmployeeContract(contract));
		}
		return contacts;
	}

	private EmployeeContract mapToEmployeeContract(EmployeeContractDTO employeeContractDto) throws ParseException {
		Assert.notNull(employeeContractDto, "Employee Contract: " + SystemType.MUST_NOT_BE_NULL);
		EmployeeContract contract = new EmployeeContract();
		contract.setEmployeeContractId(employeeContractDto.getEmployeeContractId());
		contract.setContractStatus(employeeContractDto.getContractStatus());
		if (employeeContractDto.getStartDate() != null)
			contract.setStartDate(systemUtil.convertToDate(employeeContractDto.getStartDate()));
		if (employeeContractDto.getEndDate() != null)
			contract.setEndDate(systemUtil.convertToDate(employeeContractDto.getEndDate()));
		contract.setSalary(employeeContractDto.getSalary());
		contract.setWorkingExperience(employeeContractDto.getWorkingExperience());
		contract.setSeparationReason(employeeContractDto.getSeparationReason());
		contract.setPreviousEmployer1(employeeContractDto.getPreviousEmployer1());
		contract.setPreviousEmployer2(employeeContractDto.getPreviousEmployer2());
		return contract;
	}

	public EmployeeDTO mapToEmployeeDTO(Employee employee) throws ParseException {
		Assert.notNull(employee, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		EmployeeDTO employeeDto = new EmployeeDTO();
		employeeDto.setEmployeeId(employee.getEmployeeId());
		employeeDto.setCompanyId(employee.getAdminCompany().getAdminCompanyId());
		employeeDto.setEmployeeType(employee.getEmployeeType());
		if (employeeDto.getEmployeeType().getValue().equals(SystemType.CONTRACTUAL)) {
			if (employee.getSupplier() != null)
				employeeDto.setSupplierId(employee.getSupplier().getCompanyId());
		}
		employeeDto.setStatus(employee.getStatus());
		employeeDto.setPosition(employee.getPosition());
		employeeDto.setPictureUrl(employee.getPictureUrl());
		employeeDto.setNtnNumber(employee.getNtnNumber());
		employeeDto.setNextOfKinRelation(employee.getNextOfKinRelation());
		employeeDto.setNextOfKin(employee.getNextOfKin());
		employeeDto.setFirstName(employee.getFirstName());
		employeeDto.setLastName(employee.getLastName());
		employeeDto.setEmployeeNumber(employee.getEmployeeNumber());
		employeeDto.setDepartment(employee.getDepartment());
		if (employee.getDateOfBirth() != null)
			employeeDto.setDateOfBirth(systemUtil.convertToDateString(employee.getDateOfBirth()));
		employeeDto.setCnic(employee.getCnic());
		if (employee.getCnicExpiry() != null)
			employeeDto.setCnicExpiry(systemUtil.convertToDateString(employee.getCnicExpiry()));
		employeeDto.setBusinessAddress(generalMapper.mapToAddressDTO(employee.getAcAddress()));
		employeeDto.setEmployeeAddresses(generalMapper.mapToAddressDTO(employee.getEmployeeAddresses()));
		employeeDto.setEmployeeContacts(generalMapper.mapToContactDTO(employee.getEmployeeContacts()));
		employeeDto.setEmployeeContracts(mapToEmployeeContractDTO(employee.getEmployeeContracts()));
		if (employee.getPosition().getValue().equals(SystemType.POSITION_DRIVER)) {
			employeeDto.setLicenceNumber(employee.getLicenceNumber());
			if (employee.getLicenceExpiry() != null)
				employeeDto.setLicenceExpiry(systemUtil.convertToDateString(employee.getLicenceExpiry()));
			employeeDto.setLicenceClass(employee.getLicenceClass());
		}
		return employeeDto;
	}

	private List<EmployeeContractDTO> mapToEmployeeContractDTO(List<EmployeeContract> employeeContracts)
			throws ParseException {
		Assert.notNull(employeeContracts, "Employee Coontracts: " + SystemType.MUST_NOT_BE_NULL);
		List<EmployeeContractDTO> contracts = new ArrayList<>();
		for (EmployeeContract contract : employeeContracts) {
			contracts.add(mapToEmployeeContractDTO(contract));
		}
		return contracts;
	}

	private EmployeeContractDTO mapToEmployeeContractDTO(EmployeeContract employeeContract) throws ParseException {
		Assert.notNull(employeeContract, "Employee Contract: " + SystemType.MUST_NOT_BE_NULL);
		EmployeeContractDTO contract = new EmployeeContractDTO();
		contract.setEmployeeContractId(employeeContract.getEmployeeContractId());
		contract.setContractStatus(employeeContract.getContractStatus());
		if (employeeContract.getStartDate() != null)
			contract.setStartDate(systemUtil.convertToDateString(employeeContract.getStartDate()));
		if (employeeContract.getEndDate() != null)
			contract.setEndDate(systemUtil.convertToDateString(employeeContract.getEndDate()));
		contract.setSalary(employeeContract.getSalary());
		contract.setWorkingExperience(employeeContract.getWorkingExperience());
		contract.setSeparationReason(employeeContract.getSeparationReason());
		contract.setPreviousEmployer1(employeeContract.getPreviousEmployer1());
		contract.setPreviousEmployer2(employeeContract.getPreviousEmployer2());
		return contract;
	}

	public TrainingProfile mapToTrainingProfile(TrainingProfileDTO trainingProfileDto) throws ParseException {
		Assert.notNull(trainingProfileDto, "Training Profile: " + SystemType.MUST_NOT_BE_NULL);
		TrainingProfile trainingProfile = new TrainingProfile();
		Employee employee = employeeService.findEmployeeById(trainingProfileDto.getDriverId());
		trainingProfile.setEmployee(employee);
		trainingProfile.setTrainingProfileId(trainingProfileDto.getTrainingProfileId());
		trainingProfile.setTrainerName(trainingProfileDto.getTrainerName());
		trainingProfile.setTrainingType(trainingProfileDto.getTrainingType());
		trainingProfile.setTrainingStation(trainingProfileDto.getTrainingStation());
		if (trainingProfileDto.getIssuedDate() != null)
			trainingProfile.setIssuedDate(systemUtil.convertToDate(trainingProfileDto.getIssuedDate()));
		if (trainingProfileDto.getExpiryDate() != null)
			trainingProfile.setExpiryDate(systemUtil.convertToDate(trainingProfileDto.getExpiryDate()));
		trainingProfile.setTrainingCertificateUrl(trainingProfileDto.getTrainingCertificateUrl());
		trainingProfile.setAttendanceSheetUrl(trainingProfileDto.getAttendanceSheetUrl());
		trainingProfile.setPictureWithTrainerUrl(trainingProfileDto.getPictureWithTrainerUrl());
		return trainingProfile;
	}

	public TrainingProfileDTO mapToTrainingProfileDTO(TrainingProfile trainingProfile) throws ParseException {
		Assert.notNull(trainingProfile, "Training Profile: " + SystemType.MUST_NOT_BE_NULL);
		TrainingProfileDTO trainingProfileDto = new TrainingProfileDTO();
		trainingProfileDto.setDriverId(trainingProfile.getEmployee().getEmployeeId());
		trainingProfileDto.setTrainingProfileId(trainingProfile.getTrainingProfileId());
		trainingProfileDto.setTrainerName(trainingProfile.getTrainerName());
		trainingProfileDto.setTrainingType(trainingProfile.getTrainingType());
		trainingProfileDto.setTrainingStation(trainingProfile.getTrainingStation());
		if (trainingProfile.getIssuedDate() != null)
			trainingProfileDto.setIssuedDate(systemUtil.convertToDateString(trainingProfile.getIssuedDate()));
		if (trainingProfile.getExpiryDate() != null)
			trainingProfileDto.setExpiryDate(systemUtil.convertToDateString(trainingProfile.getExpiryDate()));
		trainingProfileDto.setTrainingCertificateUrl(trainingProfile.getTrainingCertificateUrl());
		trainingProfileDto.setAttendanceSheetUrl(trainingProfile.getAttendanceSheetUrl());
		trainingProfileDto.setPictureWithTrainerUrl(trainingProfile.getPictureWithTrainerUrl());
		return trainingProfileDto;
	}

	public MedicalTestReport mapToMedicalTestReport(MedicalTestReportDTO medicalTestReportDTO) throws ParseException {
		Assert.notNull(medicalTestReportDTO, "Medical Test Report: " + SystemType.MUST_NOT_BE_NULL);
		Employee employee = employeeService.findEmployeeById(medicalTestReportDTO.getEmployeeId());
		MedicalTestReport medicalTestReport = new MedicalTestReport();
		medicalTestReport.setEmployee(employee);
		medicalTestReport.setMtrId(medicalTestReportDTO.getMtrId());
		medicalTestReport.setMtrType(medicalTestReportDTO.getMtrType());
		medicalTestReport.setMtrStation(medicalTestReportDTO.getMtrStation());
		medicalTestReport.setMtrHospital(medicalTestReportDTO.getMtrHospital());
		if (medicalTestReportDTO.getMtrDate() != null)
			medicalTestReport.setMtrDate(systemUtil.convertToDate(medicalTestReportDTO.getMtrDate()));
		medicalTestReport.setMtrResult(medicalTestReportDTO.getMtrResult());
		medicalTestReport.setUrl(medicalTestReportDTO.getUrl());
		return medicalTestReport;
	}

	public MedicalTestReportDTO mapToMedicalTestReportDTO(MedicalTestReport medicalTestReport) throws ParseException {
		Assert.notNull(medicalTestReport, "Medical Test Report: " + SystemType.MUST_NOT_BE_NULL);
		MedicalTestReportDTO medicalTestReportDTO = new MedicalTestReportDTO();
		medicalTestReportDTO.setEmployeeId(medicalTestReport.getEmployee().getEmployeeId());
		medicalTestReportDTO.setMtrId(medicalTestReport.getMtrId());
		medicalTestReportDTO.setMtrType(medicalTestReport.getMtrType());
		medicalTestReportDTO.setMtrStation(medicalTestReport.getMtrStation());
		medicalTestReportDTO.setMtrHospital(medicalTestReport.getMtrHospital());
		if (medicalTestReport.getMtrDate() != null)
			medicalTestReportDTO.setMtrDate(systemUtil.convertToDateString(medicalTestReport.getMtrDate()));
		medicalTestReportDTO.setMtrResult(medicalTestReport.getMtrResult());
		medicalTestReportDTO.setUrl(medicalTestReport.getUrl());
		return medicalTestReportDTO;
	}

	public HeavyVehicleExperience mapToHeavyVehicleExperience(HeavyVehicleExperienceDTO heavyVehicleExperienceDTO)
			throws ParseException {
		Assert.notNull(heavyVehicleExperienceDTO, "Heavy Vehicle Experience: " + SystemType.MUST_NOT_BE_NULL);
		Employee employee = employeeService.findEmployeeById(heavyVehicleExperienceDTO.getEmployeeId());
		HeavyVehicleExperience heavyVehicleExperience = new HeavyVehicleExperience();
		heavyVehicleExperience.setEmployee(employee);
		heavyVehicleExperience.setHeavyVehicleExperienceId(heavyVehicleExperienceDTO.getHeavyVehicleExperienceId());
		heavyVehicleExperience.setExperienceCompanyName(heavyVehicleExperienceDTO.getExperienceCompanyName());
		heavyVehicleExperience.setTrucksOperated(heavyVehicleExperienceDTO.getTrucksOperated());
		if (heavyVehicleExperienceDTO.getExperienceStartDate() != null)
			heavyVehicleExperience.setExperienceStartDate(
					systemUtil.convertToDate(heavyVehicleExperienceDTO.getExperienceStartDate()));
		if (heavyVehicleExperienceDTO.getExperienceEndDate() != null)
			heavyVehicleExperience
					.setExperienceEndDate(systemUtil.convertToDate(heavyVehicleExperienceDTO.getExperienceEndDate()));
		return heavyVehicleExperience;
	}

	public HeavyVehicleExperienceDTO mapToHeavyVehicleExperienceDTO(HeavyVehicleExperience heavyVehicleExperience)
			throws ParseException {
		Assert.notNull(heavyVehicleExperience, "Heavy Vehicle Experience: " + SystemType.MUST_NOT_BE_NULL);
		HeavyVehicleExperienceDTO heavyVehicleExperienceDTO = new HeavyVehicleExperienceDTO();
		heavyVehicleExperienceDTO.setEmployeeId(heavyVehicleExperience.getEmployee().getEmployeeId());
		heavyVehicleExperienceDTO.setHeavyVehicleExperienceId(heavyVehicleExperience.getHeavyVehicleExperienceId());
		heavyVehicleExperienceDTO.setExperienceCompanyName(heavyVehicleExperience.getExperienceCompanyName());
		heavyVehicleExperienceDTO.setTrucksOperated(heavyVehicleExperience.getTrucksOperated());
		if (heavyVehicleExperience.getExperienceStartDate() != null)
			heavyVehicleExperienceDTO.setExperienceStartDate(
					systemUtil.convertToDateString(heavyVehicleExperience.getExperienceStartDate()));
		if (heavyVehicleExperience.getExperienceEndDate() != null)
			heavyVehicleExperienceDTO.setExperienceEndDate(
					systemUtil.convertToDateString(heavyVehicleExperience.getExperienceEndDate()));
		return heavyVehicleExperienceDTO;
	}

	public Reference mapToReference(ReferenceDTO referenceDto) {
		Assert.notNull(referenceDto, "Reference: " + SystemType.MUST_NOT_BE_NULL);
		Employee employee = employeeService.findEmployeeById(referenceDto.getEmployeeId());
		Reference reference = new Reference();
		reference.setEmployee(employee);
		reference.setReferenceId(referenceDto.getReferenceId());
		reference.setReferenceName(referenceDto.getReferenceName());
		reference.setReferenceCnic(referenceDto.getReferenceCnic());
		reference.setReferenceContact(referenceDto.getReferenceContact());
		reference.setReferenceRelation(referenceDto.getReferenceRelation());
		reference.setReferenceAddress(referenceDto.getReferenceAddress());
		reference.setCnicUrl(referenceDto.getCnicUrl());
		reference.setReferenceFormUrl(referenceDto.getReferenceFormUrl());
		return reference;
	}

	public ReferenceDTO mapToReferenceDTO(Reference reference) {
		Assert.notNull(reference, "Reference: " + SystemType.MUST_NOT_BE_NULL);
		ReferenceDTO referenceDto = new ReferenceDTO();
		referenceDto.setEmployeeId(reference.getEmployee().getEmployeeId());
		referenceDto.setReferenceId(reference.getReferenceId());
		referenceDto.setReferenceName(reference.getReferenceName());
		referenceDto.setReferenceCnic(reference.getReferenceCnic());
		referenceDto.setReferenceContact(reference.getReferenceContact());
		referenceDto.setReferenceRelation(reference.getReferenceRelation());
		referenceDto.setReferenceAddress(reference.getReferenceAddress());
		referenceDto.setCnicUrl(reference.getCnicUrl());
		referenceDto.setReferenceFormUrl(reference.getReferenceFormUrl());
		return referenceDto;
	}

	public EmployeeSummaryDTO mapToEmployeeSummaryDTO(Employee employee) throws ParseException {
		Assert.notNull(employee, "Employee: " + SystemType.MUST_NOT_BE_NULL);
		EmployeeSummaryDTO employeeDto = new EmployeeSummaryDTO();
		employeeDto.setEmployeeId(employee.getEmployeeId());
		employeeDto.setStatus(employee.getStatus());
		employeeDto.setPosition(employee.getPosition());
		employeeDto.setEmployeeName(employee.getFirstName() + " " + employee.getLastName());
		employeeDto.setEmployeeNumber(employee.getEmployeeNumber());
		employeeDto.setDepartment(employee.getDepartment());
		if (employee.getDateOfBirth() != null)
			employeeDto.setDateOfBirth(systemUtil.convertToDateString(employee.getDateOfBirth()));
		employeeDto.setCnic(employee.getCnic());
		if (employee.getPosition().getValue().equals(SystemType.POSITION_DRIVER)) {
			employeeDto.setLicenceNumber(employee.getLicenceNumber());
		}
		return employeeDto;
	}
}
