package com.fleetX.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.DHInvoiceDTO;
import com.fleetX.dto.DHLineItemDTO;
import com.fleetX.dto.InvoiceDTO;
import com.fleetX.dto.InvoicePaymentDTO;
import com.fleetX.dto.JobDTO;
import com.fleetX.dto.LineItemDTO;
import com.fleetX.dto.RecipientDTO;
import com.fleetX.dto.ReimbursementInfoDTO;
import com.fleetX.dto.ReimbursementInfoSummaryDTO;
import com.fleetX.dto.SenderDTO;
import com.fleetX.dto.TrialBalanceEntryDTO;
import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.DHInvoice;
import com.fleetX.entity.account.FixedLineItem;
import com.fleetX.entity.account.Invoice;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.LineItem;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.service.IAccountService;
import com.fleetX.service.IOrderService;

@Component
public class AccountMapper {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	CompanyMapper companyMapper;
	@Autowired
	OrderMapper orderMapper;
	@Autowired
	IOrderService orderService;
	@Autowired
	IAccountService accountService;

	public Adjustment mapToAdjustment(TrialBalanceEntryDTO trialBalanceEntryDto) throws ParseException {
		Adjustment adjustment = new Adjustment();
		adjustment.setAccountHead(trialBalanceEntryDto.getAccountHead());
		adjustment.setAccountType(trialBalanceEntryDto.getAccountType());
		adjustment.setAmount(trialBalanceEntryDto.getAmount());
		adjustment.setDate(systemUtil.convertToDate(trialBalanceEntryDto.getDate()));
		return adjustment;
	}

	public TrialBalanceEntryDTO mapToTrialBalanceDTO(Adjustment adjustment) throws ParseException {
		TrialBalanceEntryDTO trialBalanceEntryDto = new TrialBalanceEntryDTO();
		trialBalanceEntryDto.setId(adjustment.getAdjustmentId());
		trialBalanceEntryDto.setAccountHead(adjustment.getAccountHead());
		trialBalanceEntryDto.setAccountType(adjustment.getAccountType());
		trialBalanceEntryDto.setAmount(adjustment.getAmount());
		trialBalanceEntryDto.setDate(systemUtil.convertToDateString(adjustment.getDate()));
		return trialBalanceEntryDto;
	}

	public InvoiceDTO mapToInvoiceDTO(Invoice invoice) {
		InvoiceDTO invoiceDto = new InvoiceDTO();
		invoiceDto.setInvoiceId(invoice.getInvoiceId());
		invoiceDto.setInvoiceDate(systemUtil.convertToDateString(invoice.getInvoiceDate()));
		invoiceDto.setTaxPercent(invoice.getTax() + "%");
		Double tax = 0d;
		if (invoice.getTax() > 0)
			tax = (invoice.getTax() / 100) * invoice.getTotalAmount();
		invoiceDto.setTax(systemUtil.roundOff(tax));
		invoiceDto.setGrossAmount(systemUtil.roundOff(invoice.getTotalAmount()));
		invoiceDto.setTotalAmount(systemUtil.roundOff(invoice.getTotalAmount() + tax));
		invoiceDto.setAmountInWords(systemUtil.getAmountInWords(invoiceDto.getTotalAmount()));
		invoiceDto.setAmountPaid(invoice.getAmountPaid());
		invoiceDto.setRemainingAmount(systemUtil.roundOff(invoiceDto.getTotalAmount() - invoiceDto.getAmountPaid()));
		invoiceDto.setNote(invoice.getNote());
		invoiceDto.setPaymentStatus(invoice.getPaymentStatus());
		invoiceDto.setTaxState(invoice.getTaxState());
		invoiceDto.setInvoiceType(invoice.getInvoiceType());
		invoiceDto.setMonth(invoice.getInvoiceMonth());
		invoiceDto.setYear(invoice.getInvoiceYear());
		invoiceDto.setRecipient(mapToRecipientDTO(invoice.getRecipient()));
		invoiceDto.setSender(mapToSenderDTO(invoice.getSender()));
		List<LineItemDTO> lineItemDtos = new ArrayList<>();
		LineItemDTO lineItemDto;
		if (invoice.getInvoiceType().getValue().equals(SystemType.INVOICE_TYPE_FIXED)) {
			if (invoice.getFixedLineItems() != null)
				for (FixedLineItem lineItem : invoice.getFixedLineItems()) {
					lineItemDto = mapToLineItemDTO(lineItem);
					lineItemDto.setTotalAmount(invoice.getTotalAmount() / invoice.getFixedLineItems().size());
					lineItemDtos.add(lineItemDto);
				}
		} else {
			if (invoice.getLineItems() != null)
				for (LineItem lineItem : invoice.getLineItems()) {
					lineItemDtos.add(mapToLineItemDTO(lineItem));
				}
		}
		invoiceDto.setLineItems(lineItemDtos);
		if (invoice.getDhInvoice() != null)
			invoiceDto.setDhInvoice(mapToDHInvoiceDTO(invoice.getDhInvoice()));
		return invoiceDto;
	}

	private LineItemDTO mapToLineItemDTO(LineItem lineItem) {
		LineItemDTO lineItemDto = new LineItemDTO();
		lineItemDto.setLineItemId(lineItem.getLineItemId());
		RoadwayBill rwb = lineItem.getRoadwayBill();
		lineItemDto.setRwbId(rwb.getRwbId());
		lineItemDto.setDate(systemUtil.convertToDateString(rwb.getRwbDate()));
		lineItemDto.setRoute(companyMapper.mapToRouteDTO(rwb.getRoute()));
		if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
			lineItemDto.setVehicleNumber(rwb.getTractor().getNumberPlate());
		else
			lineItemDto.setVehicleNumber(rwb.getRentalVehicle().getVehicleNumber());
		lineItemDto.setLoadNumber(rwb.getLoadNumber());
		List<Income> incomes = rwb.getIncomes();
		Double totalIncome = 0d;
		for (Income income : incomes) {
			totalIncome += income.getAmount();
			switch (income.getIncomeType().getValue()) {
			case SystemType.INCOME_FREIGHT_RATE:
				lineItemDto.setTripCharges(income.getAmount());
				break;
			case SystemType.INCOME_LOADING_CHARGES:
				lineItemDto.setLoadingCharges(income.getAmount());
				break;
			case SystemType.INCOME_OFFLOADING_CHARGES:
				lineItemDto.setOffLoadingCharges(income.getAmount());
				break;
			case SystemType.INCOME_OTHER_CHARGES:
				lineItemDto.setOtherCharges(income.getAmount());
				break;
			default:
				continue;
			}
		}
		lineItemDto.setRatePerKm(rwb.getRatePerKm());
		lineItemDto.setRatePerTon(rwb.getRatePerTon());
		lineItemDto.setWeightInTon(rwb.getWeightInTon());
		lineItemDto.setStandardKm(rwb.getRoute().getAvgDistanceInKM());
		lineItemDto.setTotalAmount(totalIncome);
		return lineItemDto;
	}

	private LineItemDTO mapToLineItemDTO(FixedLineItem lineItem) {
		LineItemDTO lineItemDto = new LineItemDTO();
		lineItemDto.setLineItemId(lineItem.getFixedLineItemId());
		AssetCombination ac = lineItem.getAssetCombination();
		if (ac.getTractor() != null)
			lineItemDto.setVehicleNumber(ac.getTractor().getNumberPlate());
		if (ac.getTrailer() != null)
			lineItemDto.setVehicleTypeAndSize(
					ac.getTrailer().getTrailerType().getValue() + " " + ac.getTrailer().getTrailerSize().getValue());
		return lineItemDto;
	}

	private SenderDTO mapToSenderDTO(AdminCompany sender) {
		SenderDTO senderDto = new SenderDTO();
		senderDto.setName(sender.getAdminCompanyName());
		senderDto.setAddress(sender.getAdminCompanyAddresses().get(0).toString());
		senderDto.setNTN(sender.getNtnNumber());
		senderDto.setSTRN(sender.getStrnNumber());
		senderDto.setPRA(sender.getPra());
		senderDto.setBRA(sender.getBra());
		senderDto.setKRA(sender.getKra());
		senderDto.setSRA(sender.getSra());
		return senderDto;
	}

	private RecipientDTO mapToRecipientDTO(Company recipient) {
		RecipientDTO recipientDto = new RecipientDTO();
		recipientDto.setName(recipient.getCompanyName());
		recipientDto.setAddress(recipient.getCompanyAddresses().get(0).toString());
		recipientDto.setNTN(recipient.getNtnNumber());
		recipientDto.setSTRN(recipient.getStrnNumber());
		recipientDto.setPRA(recipient.getPra());
		recipientDto.setBRA(recipient.getBra());
		recipientDto.setKRA(recipient.getKra());
		recipientDto.setSRA(recipient.getSra());
		return recipientDto;
	}

	public ReimbursementInfo mapToReimbursementInfo(ReimbursementInfoSummaryDTO reimbursementDto)
			throws ParseException {
		ReimbursementInfo reimbursementInfo = new ReimbursementInfo();
		reimbursementInfo.setReimbursementInfoId(reimbursementDto.getReimbursementInfoId());
		reimbursementInfo.setReimbursementDate(systemUtil.convertToDate(reimbursementDto.getReimbursementDate()));
		reimbursementInfo.setReimbursementAmount(reimbursementDto.getReimbursementAmount());
		reimbursementInfo.setReferenceNumber(reimbursementDto.getReferenceNumber());
		reimbursementInfo.setReimbursementNote(reimbursementDto.getReimbursementNote());
		List<Job> jobs = new ArrayList<>();
		for (String jobId : reimbursementDto.getJobIds()) {
			jobs.add(orderService.getJobById(jobId));
		}
		reimbursementInfo.setJobs(jobs);
		return reimbursementInfo;
	}

	public ReimbursementInfoSummaryDTO mapToReimbursementInfoSummaryDTO(ReimbursementInfo reimbursement) {
		ReimbursementInfoSummaryDTO reimbursementDto = new ReimbursementInfoSummaryDTO();
		reimbursementDto.setReimbursementInfoId(reimbursement.getReimbursementInfoId());
		reimbursementDto.setReimbursementDate(systemUtil.convertToDateString(reimbursement.getReimbursementDate()));
		reimbursementDto.setReimbursementAmount(reimbursement.getReimbursementAmount());
		reimbursementDto.setReferenceNumber(reimbursement.getReferenceNumber());
		reimbursementDto.setReimbursementNote(reimbursement.getReimbursementNote());
		List<String> jobIds = new ArrayList<>();
		for (Job job : reimbursement.getJobs()) {
			jobIds.add(job.getJobId());
		}
		reimbursementDto.setJobIds(jobIds);
		return reimbursementDto;
	}

	public ReimbursementInfoDTO mapToReimbursementInfoDTO(ReimbursementInfo reimbursement) throws ParseException {
		ReimbursementInfoDTO reimbursementDto = new ReimbursementInfoDTO();
		reimbursementDto.setReimbursementInfoId(reimbursement.getReimbursementInfoId());
		reimbursementDto.setReimbursementDate(systemUtil.convertToDateString(reimbursement.getReimbursementDate()));
		reimbursementDto.setReimbursementAmount(reimbursement.getReimbursementAmount());
		reimbursementDto.setReferenceNumber(reimbursement.getReferenceNumber());
		reimbursementDto.setReimbursementNote(reimbursement.getReimbursementNote());
		List<JobDTO> jobs = new ArrayList<>();
		for (Job job : reimbursement.getJobs()) {
			jobs.add(orderMapper.mapToJobDTO(job));
		}
		reimbursementDto.setJobs(jobs);
		return reimbursementDto;
	}

	public InvoicePayment mapToInvoicePayment(InvoicePaymentDTO invoicePaymentDto) throws ParseException {
		InvoicePayment invoicePayment = new InvoicePayment();
		invoicePayment.setInvoicePaymentId(invoicePaymentDto.getInvoicePaymentId());
		invoicePayment.setPaymentAmount(invoicePaymentDto.getPaymentAmount());
		invoicePayment.setReferenceNumber(invoicePaymentDto.getReferenceNumber());
		invoicePayment.setPaymentNote(invoicePaymentDto.getPaymentNote());
		invoicePayment.setPaymentDate(systemUtil.convertToDate(invoicePaymentDto.getPaymentDate()));
		if (invoicePaymentDto.getInvoiceId() != null)
			invoicePayment.setInvoice(accountService.getInvoiceById(invoicePaymentDto.getInvoiceId()));
		if (invoicePaymentDto.getDhInvoiceId() != null)
			invoicePayment.setDhInvoice(accountService.getDHInvoiceById(invoicePaymentDto.getDhInvoiceId()));

		return invoicePayment;
	}

	public InvoicePaymentDTO mapToInvoicePaymentDTO(InvoicePayment invoicePayment) throws ParseException {
		InvoicePaymentDTO invoicePaymentDto = new InvoicePaymentDTO();
		invoicePaymentDto.setInvoicePaymentId(invoicePayment.getInvoicePaymentId());
		invoicePaymentDto.setPaymentAmount(invoicePayment.getPaymentAmount());
		invoicePaymentDto.setReferenceNumber(invoicePayment.getReferenceNumber());
		invoicePaymentDto.setPaymentNote(invoicePayment.getPaymentNote());
		invoicePaymentDto.setPaymentDate(systemUtil.convertToDateString(invoicePayment.getPaymentDate()));
		if (invoicePayment.getInvoice() != null)
			invoicePaymentDto.setInvoiceId(invoicePayment.getInvoice().getInvoiceId());
		if (invoicePayment.getDhInvoice() != null)
			invoicePaymentDto.setInvoiceId(invoicePayment.getDhInvoice().getDhInvoiceId());
		return invoicePaymentDto;
	}

	public DHInvoiceDTO mapToDHInvoiceDTO(DHInvoice invoice) {

		DHInvoiceDTO invoiceDto = new DHInvoiceDTO();
		invoiceDto.setInvoiceId(invoice.getDhInvoiceId());
		invoiceDto.setDhInvoiceId(invoice.getInvoice().getInvoiceId() + "-DH");
		invoiceDto.setInvoiceDate(systemUtil.convertToDateString(invoice.getInvoiceDate()));
		invoiceDto.setTaxPercent(invoice.getTax() + "%");
		Double tax = 0d;
		if (invoice.getTax() > 0)
			tax = (invoice.getTax() / 100) * invoice.getTotalAmount();
		invoiceDto.setTax(systemUtil.roundOff(tax));
		invoiceDto.setGrossAmount(systemUtil.roundOff(invoice.getTotalAmount()));
		invoiceDto.setTotalAmount(systemUtil.roundOff(invoice.getTotalAmount() + tax));
		invoiceDto.setAmountInWords(systemUtil.getAmountInWords(invoiceDto.getTotalAmount()));
		invoiceDto.setAmountPaid(invoice.getAmountPaid());
		invoiceDto.setRemainingAmount(systemUtil.roundOff(invoiceDto.getTotalAmount() - invoiceDto.getAmountPaid()));
		invoiceDto.setNote(invoice.getNote());
		invoiceDto.setPaymentStatus(invoice.getPaymentStatus());
		invoiceDto.setTaxState(invoice.getTaxState());
		invoiceDto.setInvoiceType(invoice.getInvoiceType());
		invoiceDto.setMonth(invoice.getInvoiceMonth());
		invoiceDto.setYear(invoice.getInvoiceYear());
		invoiceDto.setRecipient(mapToRecipientDTO(invoice.getRecipient()));
		invoiceDto.setSender(mapToSenderDTO(invoice.getSender()));
		List<DHLineItemDTO> lineItemDtos = new ArrayList<>();
		if (invoice.getLineItems() != null)
			for (LineItem lineItem : invoice.getLineItems()) {
				lineItemDtos.addAll(mapToDHLineItemDTO(lineItem));
			}
		invoiceDto.setDhLineItems(lineItemDtos);
		return invoiceDto;
	}

	private List<DHLineItemDTO> mapToDHLineItemDTO(LineItem lineItem) {
		List<DHLineItemDTO> lineItemDtos = new ArrayList<>();
		DHLineItemDTO lineItemDto = null;
		RoadwayBill rwb = lineItem.getRoadwayBill();
		List<StopProgress> pickups = orderService.getStopProgressesByType(rwb, SystemType.STOP_TYPE_PICKUP);
		List<StopProgress> deliveries = orderService.getStopProgressesByType(rwb, SystemType.STOP_TYPE_DELIVERY);
		for (StopProgress pickup : pickups) {
			if (pickup.getDetentionInHrs() >= 24) {
				lineItemDto = new DHLineItemDTO();
				lineItemDto.setLineItemId(lineItem.getLineItemId());
				lineItemDto.setRwbId(rwb.getRwbId());
				lineItemDto.setDate(systemUtil.convertToDateString(rwb.getRwbDate()));
				lineItemDto.setRoute(companyMapper.mapToRouteDTO(rwb.getRoute()));
				if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
					lineItemDto.setVehicleNumber(rwb.getTractor().getNumberPlate());
				else
					lineItemDto.setVehicleNumber(rwb.getRentalVehicle().getVehicleNumber());
				lineItemDto.setLoadNumber(rwb.getLoadNumber());
				lineItemDto.setStopType(pickup.getStopProgressType());
				Double totalAmount = 0d;
				int halting = (int) Math.floor(pickup.getDetentionInHrs() / 24);
				totalAmount = halting * rwb.getHaltingRate();
				lineItemDto.setDh(halting);
				lineItemDto.setDhRate(rwb.getHaltingRate());
				lineItemDto.setTotalAmount(totalAmount);
				lineItemDto.setArrivalTime(systemUtil.convertToDateTimeString(pickup.getArrivalTime()));
				lineItemDto.setDepartureTime(systemUtil.convertToDateTimeString(pickup.getDepartureTime()));
				lineItemDto.setTotalAmount(totalAmount);
				lineItemDtos.add(lineItemDto);
			}
		}
		for (StopProgress delivery : deliveries) {
			if (delivery.getDetentionInHrs() >= 24) {
				lineItemDto = new DHLineItemDTO();
				lineItemDto.setLineItemId(lineItem.getLineItemId());
				lineItemDto.setRwbId(rwb.getRwbId());
				lineItemDto.setDate(systemUtil.convertToDateString(rwb.getRwbDate()));
				lineItemDto.setRoute(companyMapper.mapToRouteDTO(rwb.getRoute()));
				if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN))
					lineItemDto.setVehicleNumber(rwb.getTractor().getNumberPlate());
				else
					lineItemDto.setVehicleNumber(rwb.getRentalVehicle().getVehicleNumber());
				lineItemDto.setLoadNumber(rwb.getLoadNumber());
				lineItemDto.setStopType(delivery.getStopProgressType());
				Double totalAmount = 0d;
				int detention = (int) Math.floor(delivery.getDetentionInHrs() / 24);
				totalAmount = detention * rwb.getDetentionRate();
				lineItemDto.setDh(detention);
				lineItemDto.setDhRate(rwb.getDetentionRate());
				lineItemDto.setTotalAmount(totalAmount);
				lineItemDto.setArrivalTime(systemUtil.convertToDateTimeString(delivery.getArrivalTime()));
				lineItemDto.setDepartureTime(systemUtil.convertToDateTimeString(delivery.getDepartureTime()));
				lineItemDto.setTotalAmount(totalAmount);
				lineItemDtos.add(lineItemDto);
			}
		}
		return lineItemDtos;
	}
}
