package com.fleetX.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.AdminCompanyDTO;
import com.fleetX.dto.CompanyContractDTO;
import com.fleetX.dto.CompanyContractSummaryDTO;
import com.fleetX.dto.CompanyDTO;
import com.fleetX.dto.CompanySummaryDTO;
import com.fleetX.dto.FuelCardDTO;
import com.fleetX.dto.FuelRateDTO;
import com.fleetX.dto.FuelSupplierContractDTO;
import com.fleetX.dto.RouteDTO;
import com.fleetX.dto.RouteRateContractDTO;
import com.fleetX.dto.RouteRateDTO;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.Route;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IContractService;

@Component
public class CompanyMapper {

	@Autowired
	IAdminService adminService;
	@Autowired
	IAssetService assetService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IContractService contractService;
	@Autowired
	GeneralMapper generalMapper;

	public Company mapToCompany(CompanyDTO companyDto) {
		Assert.notNull(companyDto, "Company: " + SystemType.MUST_NOT_BE_NULL);
		Company company = new Company();
		company.setCompanyId(companyDto.getCompanyId());
		company.setCompanyCode(companyDto.getCompanyCode());
		company.setCompanyName(companyDto.getCompanyName());
		company.setCompanyFormation(companyDto.getFormation());
		company.setCompanyType(companyDto.getCompanyType());
		company.setBra(companyDto.getBra());
		company.setSra(companyDto.getSra());
		company.setKra(companyDto.getKra());
		company.setPra(companyDto.getPra());
		company.setNtnNumber(companyDto.getNtnNumber());
		company.setStatus(companyDto.getStatus());
		company.setStrnNumber(companyDto.getStrnNumber());
		if (companyDto.getAddresses() != null && companyDto.getAddresses().size() > 0)
			company.setCompanyAddresses(generalMapper.mapToAddress(companyDto.getAddresses()));
		if (companyDto.getContacts() != null && companyDto.getContacts().size() > 0)
			company.setCompanyContacts(generalMapper.mapToContact(companyDto.getContacts()));
		company.setBusinessType(companyDto.getBusinessType());
		company.setSupplierType(companyDto.getSupplierType());
		return company;
	}

	public AdminCompany mapToAdminCompany(AdminCompanyDTO companyDto) {
		Assert.notNull(companyDto, "Admin Company: " + SystemType.MUST_NOT_BE_NULL);
		AdminCompany company = new AdminCompany();
		company.setAdminCompanyId(companyDto.getCompanyId());
		company.setAdminCompanyCode(companyDto.getCompanyCode());
		company.setAdminCompanyName(companyDto.getCompanyName());
		company.setAdminCompanyFormation(companyDto.getFormation());
		company.setBra(companyDto.getBra());
		company.setSra(companyDto.getSra());
		company.setKra(companyDto.getKra());
		company.setPra(companyDto.getPra());
		company.setNtnNumber(companyDto.getNtnNumber());
		company.setStatus(companyDto.getStatus());
		company.setStrnNumber(companyDto.getStrnNumber());
		if (companyDto.getAddresses() != null && companyDto.getAddresses().size() > 0)
			company.setAdminCompanyAddresses(generalMapper.mapToAddress(companyDto.getAddresses()));
		if (companyDto.getContacts() != null && companyDto.getContacts().size() > 0)
			company.setAdminCompanyContacts(generalMapper.mapToContact(companyDto.getContacts()));
		company.setBusinessType(companyDto.getBusinessType());
		return company;
	}

	public AdminCompanyDTO mapToAdminCompanyDTO(AdminCompany adminCompany) {
		Assert.notNull(adminCompany, "Admin Company: " + SystemType.MUST_NOT_BE_NULL);
		AdminCompanyDTO companyDto = new AdminCompanyDTO();
		companyDto.setCompanyId(adminCompany.getAdminCompanyId());
		companyDto.setCompanyName(adminCompany.getAdminCompanyName());
		companyDto.setFormation(adminCompany.getAdminCompanyFormation());
		companyDto.setCompanyCode(adminCompany.getAdminCompanyCode());
		companyDto.setBra(adminCompany.getBra());
		companyDto.setSra(adminCompany.getSra());
		companyDto.setKra(adminCompany.getKra());
		companyDto.setPra(adminCompany.getPra());
		companyDto.setNtnNumber(adminCompany.getNtnNumber());
		companyDto.setStatus(adminCompany.getStatus());
		companyDto.setStrnNumber(adminCompany.getStrnNumber());
		if (adminCompany.getAdminCompanyAddresses() != null && adminCompany.getAdminCompanyAddresses().size() > 0)
			companyDto.setAddresses(generalMapper.mapToAddressDTO(adminCompany.getAdminCompanyAddresses()));
		if (adminCompany.getAdminCompanyContacts() != null && adminCompany.getAdminCompanyContacts().size() > 0)
			companyDto.setContacts(generalMapper.mapToContactDTO(adminCompany.getAdminCompanyContacts()));
		companyDto.setBusinessType(adminCompany.getBusinessType());
		return companyDto;
	}

	public CompanyDTO mapToCompanyDTO(Company company) throws ParseException {
		Assert.notNull(company, "Company: " + SystemType.MUST_NOT_BE_NULL);
		CompanyDTO companyDto = new CompanyDTO();
		companyDto.setCompanyId(company.getCompanyId());
		companyDto.setCompanyName(company.getCompanyName());
		companyDto.setFormation(company.getCompanyFormation());
		companyDto.setCompanyType(company.getCompanyType());
		companyDto.setCompanyCode(company.getCompanyCode());
		companyDto.setBra(company.getBra());
		companyDto.setSra(company.getSra());
		companyDto.setKra(company.getKra());
		companyDto.setPra(company.getPra());
		companyDto.setNtnNumber(company.getNtnNumber());
		companyDto.setStrnNumber(company.getStrnNumber());
		companyDto.setStatus(company.getStatus());
		companyDto.setBusinessType(company.getBusinessType());
		companyDto.setSupplierType(company.getSupplierType());
		companyDto.setAddresses(generalMapper.mapToAddressDTO(company.getCompanyAddresses()));
		companyDto.setContacts(generalMapper.mapToContactDTO(company.getCompanyContacts()));
		List<CompanyContractSummaryDTO> companyContractDtos = new ArrayList<>();
		List<CompanyContract> companyContracts = company.getCompanyContracts();
		if (companyContracts != null && companyContracts.size() > 0)
			for (CompanyContract companyContract : companyContracts) {
				companyContractDtos.add(mapToCompanyContractSummaryDTO(companyContract));
			}
		companyDto.setContracts(companyContractDtos);
		return companyDto;
	}

	public Route mapToRoute(RouteDTO routeDto) {
		Assert.notNull(routeDto, "Route: " + SystemType.MUST_NOT_BE_NULL);
		Route route = new Route();
		route.setRouteId(routeDto.getRouteId());
		route.setRouteTo(routeDto.getRouteTo());
		route.setRouteFrom(routeDto.getRouteFrom());
		route.setAvgDistanceInKM(routeDto.getAvgDistanceInKM());
		return route;
	}

	public RouteDTO mapToRouteDTO(Route route) {
		Assert.notNull(route, "Route: " + SystemType.MUST_NOT_BE_NULL);
		RouteDTO routeDto = new RouteDTO();
		routeDto.setRouteId(route.getRouteId());
		routeDto.setRouteTo(route.getRouteTo());
		routeDto.setRouteFrom(route.getRouteFrom());
		routeDto.setAvgDistanceInKM(route.getAvgDistanceInKM());
		return routeDto;
	}

	public CompanyContract mapToCompanyContract(CompanyContractDTO companyContractDto) throws ParseException {
		Assert.notNull(companyContractDto, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		CompanyContract companyContract = new CompanyContract();
		companyContract.setCompanyContractId(companyContractDto.getCompanyContractId());
		companyContract.setContractType(companyContractDto.getContractType());
		companyContract.setAdminCompany(adminService.findAdminCompanyById(companyContractDto.getAdminCompanyId()));
		companyContract.setCustomer(companyService.findCompanyById(companyContractDto.getCustomerId()));
		if (companyContractDto.getContractStart() != null)
			companyContract.setContractStart(systemUtil.convertToDate(companyContractDto.getContractStart()));
		if (companyContractDto.getContractEnd() != null)
			companyContract.setContractEnd(systemUtil.convertToDate(companyContractDto.getContractEnd()));
		companyContract.setFixedRate(companyContractDto.getFixedRate());
		companyContract.setDetentionRate(companyContractDto.getDetentionRate());
		companyContract.setHaltingRate(companyContractDto.getHaltingRate());
		companyContract.setStatus(companyContractDto.getContractStatus());
		List<String> assetCombinationsDto = companyContractDto.getAssetCombinationId();
		if (assetCombinationsDto != null && assetCombinationsDto.size() > 0) {
			List<AssetCombination> assetCombinations = new ArrayList<>();
			for (String assetCombinationId : assetCombinationsDto) {
				assetCombinations.add(assetService.findAssetCombinationById(assetCombinationId));
			}
			companyContract.setAssetCombinations(assetCombinations);
		}
//		List<RouteRateContractDTO> routeRateContractDtos = companyContractDto.getRouteRateContracts();
//		List<RouteRateContract> routeRateContracts = new ArrayList<>();
//		for (RouteRateContractDTO routeRateContractDto : routeRateContractDtos) {
//			RouteRateContract routeRateContract = new RouteRateContract();
//			routeRateContract.setRoute(adminService.findRouteById(routeRateContractDto.getRoute().getRouteId()));
//			List<RouteRate> routeRates = new ArrayList<>();
//			if (routeRateContractDto.getRouteRates() != null && routeRateContractDto.getRouteRates().size() > 0) {
//				for (RouteRateDTO routeRateDto : routeRateContractDto.getRouteRates()) {
//					RouteRate routeRate = mapToRouteRate(routeRateDto);
//					if (routeRateDto.getRatePerKm() != null) {
//						routeRate.setTotalAmount(
//								routeRate.getRatePerKm() * routeRateContractDto.getRoute().getAvgDistanceInKM());
//					} else {
//						routeRate.setTotalAmount(routeRateDto.getTotalAmount());
//					}
//					routeRates.add(routeRate);
//				}
//			}
//			routeRateContract.setRouteRates(routeRates);
//			routeRateContracts.add(routeRateContract);
//		}
//		companyContract.setRouteRateContracts(routeRateContracts);
		companyContract.setContractDocUrl(companyContractDto.getContractDocUrl());
		return companyContract;
	}

	private RouteRate mapToRouteRate(RouteRateDTO routeRateDto) throws ParseException {
		Assert.notNull(routeRateDto, "Route Rate: " + SystemType.MUST_NOT_BE_NULL);
		RouteRate routeRate = new RouteRate();
		routeRate.setRouteRateId(routeRateDto.getRouteRateId());
		routeRate.setTrailerType(routeRateDto.getTrailerType());
		routeRate.setTrailerSize(routeRateDto.getTrailerSize());
		routeRate.setWeightLow(routeRateDto.getWeightLow());
		routeRate.setWeightHigh(routeRateDto.getWeightHigh());
		routeRate.setRatePerKm(routeRateDto.getRatePerKm());
		routeRate.setRatePerTon(routeRateDto.getRatePerTon());
		if (routeRateDto.getEffectiveFrom() != null)
			routeRate.setEffectiveFrom(systemUtil.convertToDate(routeRateDto.getEffectiveFrom()));
		if (routeRateDto.getEffectiveTill() != null)
			routeRate.setEffectiveTill(systemUtil.convertToDate(routeRateDto.getEffectiveTill()));
		routeRate.setStatus(routeRateDto.getStatus());
		if (routeRateDto.getTotalAmount() != null)
			routeRate.setTotalAmount(routeRateDto.getTotalAmount());
		return routeRate;
	}

	public CompanyContractDTO mapToCompanyContractDTO(CompanyContract companyContract) throws ParseException {
		Assert.notNull(companyContract, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		CompanyContractDTO companyContractDto = new CompanyContractDTO();
		companyContractDto.setCompanyContractId(companyContract.getCompanyContractId());
		companyContractDto.setContractType(companyContract.getContractType());
		companyContractDto.setAdminCompanyId(companyContract.getAdminCompany().getAdminCompanyId());
		companyContractDto.setCustomerId(companyContract.getCustomer().getCompanyId());
		if (companyContract.getContractStart() != null)
			companyContractDto.setContractStart(systemUtil.convertToDateString(companyContract.getContractStart()));
		if (companyContract.getContractEnd() != null)
			companyContractDto.setContractEnd(systemUtil.convertToDateString(companyContract.getContractEnd()));
		companyContractDto.setFixedRate(companyContract.getFixedRate());
		companyContractDto.setDetentionRate(companyContract.getDetentionRate());
		companyContractDto.setHaltingRate(companyContract.getHaltingRate());
		companyContractDto.setContractStatus(companyContract.getStatus());
		List<String> assetCombinationIds = new ArrayList<>();
		List<AssetCombination> assetCombinations = companyContract.getAssetCombinations();
		for (AssetCombination assetCombination : assetCombinations) {
			assetCombinationIds.add(assetCombination.getAssetCombinationId());
		}
		companyContractDto.setAssetCombinationId(assetCombinationIds);
		List<RouteRateContractDTO> routeRateContractDtos = new ArrayList<>();
		List<RouteRateContract> routeRateContracts = companyContract.getRouteRateContracts();
		for (RouteRateContract routeRateContract : routeRateContracts) {

			routeRateContractDtos.add(mapToRouteRateContractDTO(routeRateContract));
		}
		companyContractDto.setRouteRateContracts(routeRateContractDtos);
		companyContractDto.setContractDocUrl(companyContract.getContractDocUrl());
		return companyContractDto;
	}

	public RouteRateContractDTO mapToRouteRateContractDTO(RouteRateContract routeRateContract) throws ParseException {
		RouteRateContractDTO routeRateContractDto = new RouteRateContractDTO();
		routeRateContractDto.setRouteRateContractId(routeRateContract.getRouteRateContractId());
		routeRateContractDto.setRoute(mapToRouteDTO(routeRateContract.getRoute()));
		List<RouteRateDTO> routeRateDtos = new ArrayList<>();
		for (RouteRate routeRate : routeRateContract.getRouteRates()) {
			routeRateDtos.add(mapToRouteRateDTO(routeRate));
		}
		routeRateContractDto.setRouteRates(routeRateDtos);
		return routeRateContractDto;
	}

	public RouteRateDTO mapToRouteRateDTO(RouteRate routeRate) throws ParseException {
		Assert.notNull(routeRate, "Route Rate: " + SystemType.MUST_NOT_BE_NULL);
		RouteRateDTO routeRateDto = new RouteRateDTO();
		routeRateDto.setRouteRateId(routeRate.getRouteRateId());
		routeRateDto.setTrailerType(routeRate.getTrailerType());
		routeRateDto.setTrailerSize(routeRate.getTrailerSize());
		routeRateDto.setWeightLow(routeRate.getWeightLow());
		routeRateDto.setWeightHigh(routeRate.getWeightHigh());
		routeRateDto.setRatePerKm(routeRate.getRatePerKm());
		routeRateDto.setRatePerTon(routeRate.getRatePerTon());
		if (routeRate.getEffectiveFrom() != null)
			routeRateDto.setEffectiveFrom(systemUtil.convertToDateString(routeRate.getEffectiveFrom()));
		if (routeRate.getEffectiveTill() != null)
			routeRateDto.setEffectiveTill(systemUtil.convertToDateString(routeRate.getEffectiveTill()));
		if (routeRate.getTotalAmount() != null)
			routeRateDto.setTotalAmount(routeRate.getTotalAmount());
		routeRateDto.setStatus(routeRate.getStatus());
		return routeRateDto;
	}

	public FuelCard mapToFuelCard(FuelCardDTO fuelCardDto) throws ParseException {
		Assert.notNull(fuelCardDto, "Fuel Card: " + SystemType.MUST_NOT_BE_NULL);
		FuelCard fuelCard = new FuelCard();
		fuelCard.setFuelCardId(fuelCardDto.getFuelCardId());
		fuelCard.setFuelCardNumber(fuelCardDto.getFuelCardNumber());
		if (fuelCardDto.getExpiry() != null)
			fuelCard.setExpiry(systemUtil.convertToDate(fuelCardDto.getExpiry()));
		fuelCard.setLimit(fuelCardDto.getLimit());
		fuelCard.setStatus(fuelCardDto.getStatus());
		String supplierId = fuelCardDto.getSupplierId();
		String customerId = fuelCardDto.getCustomerId();
		String adminCompanyId = fuelCardDto.getAdminCompanyId();
		Company supplier = companyService.findCompanyById(supplierId);
		fuelCard.setSupplier(supplier);
		if (customerId != null)
			fuelCard.setCustomer(companyService.findCompanyById(customerId));
		else if (adminCompanyId != null)
			fuelCard.setAdminCompany(adminService.findAdminCompanyById(adminCompanyId));
		return fuelCard;
	}

	public FuelCardDTO mapToFuelCardDTO(FuelCard fuelCard) throws ParseException {
		Assert.notNull(fuelCard, "Fuel Card: " + SystemType.MUST_NOT_BE_NULL);
		FuelCardDTO fuelCardDto = new FuelCardDTO();
		fuelCardDto.setFuelCardId(fuelCard.getFuelCardId());
		fuelCardDto.setFuelCardNumber(fuelCard.getFuelCardNumber());
		fuelCardDto.setExpiry(systemUtil.convertToDateString(fuelCard.getExpiry()));
		fuelCardDto.setLimit(fuelCard.getLimit());
		fuelCardDto.setStatus(fuelCard.getStatus());
		fuelCardDto.setSupplierId(fuelCard.getSupplier().getCompanyId());
		Company customer = fuelCard.getCustomer();
		AdminCompany adminCompany = fuelCard.getAdminCompany();
		if (customer != null)
			fuelCardDto.setCustomerId(customer.getCompanyId());
		else if (adminCompany != null)
			fuelCardDto.setAdminCompanyId(adminCompany.getAdminCompanyId());
		return fuelCardDto;
	}

	public FuelSupplierContract mapToFuelSupplierContract(FuelSupplierContractDTO fscDto) throws ParseException {
		Assert.notNull(fscDto, "Fuel Supplier Contract: " + SystemType.MUST_NOT_BE_NULL);
		FuelSupplierContract fsc = new FuelSupplierContract();
		fsc.setFscId(fscDto.getFscId());
		fsc.setAdminCompany(adminService.findAdminCompanyById(fscDto.getAdminCompany().getCompanyId()));
		fsc.setSupplier(companyService.findCompanyById(fscDto.getSupplier().getCompanyId()));
		fsc.setFscStatus(fscDto.getFscStatus());
		if (fscDto.getFuelRates() != null && fscDto.getFuelRates().size() > 0) {
			List<FuelRate> fuelRates = new ArrayList<>();
			if (fscDto.getFuelRates() != null && fscDto.getFuelRates().size() > 0) {
				for (FuelRateDTO fuelRateDto : fscDto.getFuelRates()) {
					fuelRates.add(mapToFuelRate(fuelRateDto));
				}
			}
			fsc.setFuelRates(fuelRates);
		}
		return fsc;
	}

	private FuelRate mapToFuelRate(FuelRateDTO fuelRateDto) throws ParseException {
		Assert.notNull(fuelRateDto, "Fuel Rate: " + SystemType.MUST_NOT_BE_NULL);
		FuelRate fuelRate = new FuelRate();
		fuelRate.setFuelRateId(fuelRateDto.getFuelRateId());
		if (fuelRateDto.getStartDate() != null)
			fuelRate.setEffectiveFrom(systemUtil.convertToDate(fuelRateDto.getStartDate()));
		if (fuelRateDto.getEndDate() != null)
			fuelRate.setEffectiveTill(systemUtil.convertToDate(fuelRateDto.getEndDate()));
		fuelRate.setRatePerLitre(fuelRateDto.getRatePerLitre());
		if (fuelRateDto.getFscId() != null)
			fuelRate.setFuelSupplierContract(contractService.findFuelSupplierContractById(fuelRateDto.getFscId()));
		return fuelRate;
	}

	public FuelSupplierContractDTO mapToFuelSupplierContractDTO(FuelSupplierContract fsc) throws ParseException {
		Assert.notNull(fsc, "Fuel Supplier Contract: " + SystemType.MUST_NOT_BE_NULL);
		FuelSupplierContractDTO fscDto = new FuelSupplierContractDTO();
		fscDto.setFscId(fsc.getFscId());
		fscDto.setAdminCompany(mapToAdminCompanyDTO(fsc.getAdminCompany()));
		fscDto.setSupplier(mapToCompanyDTO(fsc.getSupplier()));
		fscDto.setFscStatus(fsc.getFscStatus());
		if (fsc.getFuelRates() != null && fsc.getFuelRates().size() > 0) {
			List<FuelRateDTO> fuelRateDtos = new ArrayList<>();
			for (FuelRate fuelRate : fsc.getFuelRates()) {
				fuelRateDtos.add(mapToFuelRateDTO(fuelRate));
			}
			fscDto.setFuelRates(fuelRateDtos);
		}
		return fscDto;
	}

	private FuelRateDTO mapToFuelRateDTO(FuelRate fuelRate) throws ParseException {
		Assert.notNull(fuelRate, "Fuel Rate: " + SystemType.MUST_NOT_BE_NULL);
		FuelRateDTO fuelRateDto = new FuelRateDTO();
		fuelRateDto.setFuelRateId(fuelRate.getFuelRateId());
		if (fuelRate.getEffectiveFrom() != null)
			fuelRateDto.setStartDate(systemUtil.convertToDateString(fuelRate.getEffectiveFrom()));
		if (fuelRate.getEffectiveTill() != null)
			fuelRateDto.setEndDate(systemUtil.convertToDateString(fuelRate.getEffectiveTill()));
		fuelRateDto.setRatePerLitre(fuelRate.getRatePerLitre());
		if (fuelRate.getFuelSupplierContract() != null)
			fuelRateDto.setFscId(fuelRate.getFuelSupplierContract().getFscId());
		return fuelRateDto;
	}

	public RouteRateContract mapToRouteRateContract(RouteRateContractDTO routeRateContractDto) throws ParseException {
		RouteRateContract routeRateContract = new RouteRateContract();
		routeRateContract.setRoute(mapToRoute(routeRateContractDto.getRoute()));
		routeRateContract.setRouteRateContractId(routeRateContractDto.getRouteRateContractId());
		routeRateContract.setStatus(routeRateContractDto.getStatus());
		List<RouteRateDTO> routeRateDtos = routeRateContractDto.getRouteRates();
		List<RouteRate> routeRates = new ArrayList<>();
		for (RouteRateDTO routeRateDTO : routeRateDtos) {
			routeRates.add(mapToRouteRate(routeRateDTO));
		}
		routeRateContract.setRouteRates(routeRates);
		return routeRateContract;
	}

	public CompanySummaryDTO mapToCompanySummaryDTO(Company company) throws ParseException {
		Assert.notNull(company, "Company: " + SystemType.MUST_NOT_BE_NULL);
		CompanySummaryDTO companyDto = new CompanySummaryDTO();
		companyDto.setCompanyId(company.getCompanyId());
		companyDto.setCompanyName(company.getCompanyName());
		companyDto.setStatus(company.getStatus());
		companyDto.setBusinessType(company.getBusinessType());
		companyDto.setAddress(generalMapper.mapToAddressDTO(company.getCompanyAddresses()).get(0));
		return companyDto;
	}

	public CompanyContractSummaryDTO mapToCompanyContractSummaryDTO(CompanyContract companyContract)
			throws ParseException {
		Assert.notNull(companyContract, "Company Contract: " + SystemType.MUST_NOT_BE_NULL);
		CompanyContractSummaryDTO companyContractDto = new CompanyContractSummaryDTO();
		companyContractDto.setCompanyContractId(companyContract.getCompanyContractId());
		companyContractDto.setContractType(companyContract.getContractType());
		companyContractDto.setCustomerName(companyContract.getCustomer().getCompanyName());
		companyContractDto.setContractStatus(companyContract.getStatus());
		return companyContractDto;
	}

}
