package com.fleetX.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dto.DimensionDTO;
import com.fleetX.dto.InventoryAdjustmentDTO;
import com.fleetX.dto.InventoryDTO;
import com.fleetX.dto.InventoryDetailDTO;
import com.fleetX.dto.InventoryTransactionInfoDTO;
import com.fleetX.dto.ItemDTO;
import com.fleetX.dto.ItemPurchaseDetailDTO;
import com.fleetX.dto.PurchaseOrderDTO;
import com.fleetX.dto.WorkOrderDTO;
import com.fleetX.dto.WorkOrderItemDetailDTO;
import com.fleetX.entity.maintenance.Inventory;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.InventoryTransactionInfo;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.ItemPurchaseDetail;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.maintenance.WorkOrderItemDetail;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.ICompanyService;
import com.fleetX.service.IInventoryService;

@Component
public class InventoryMapper {
	@Autowired
	SystemUtil systemUtil;
	@Autowired
	IInventoryService inventoryService;
	@Autowired
	IAdminService adminService;
	@Autowired
	ICompanyService companyService;
	@Autowired
	IAssetService assetService;
	@Autowired
	GeneralMapper generalMapper;
	@Autowired
	AssetMapper assetMapper;
	@Autowired
	CompanyMapper companyMapper;

	public Item mapToItem(ItemDTO itemDto) {
		Assert.notNull(itemDto, "Item DTO: " + SystemType.MUST_NOT_BE_NULL);
		Item item = new Item();
		item.setItemId(itemDto.getItemId());
		item.setItemName(itemDto.getItemName());
		item.setItemSku(itemDto.getItemSku());
		item.setItemPrice(itemDto.getItemPrice());
		item.setItemType(itemDto.getItemType());
		item.setWeightInKg(itemDto.getWeightInKg());
		item.setItemUpc(itemDto.getItemUpc());
		item.setItemEan(itemDto.getItemEan());
		item.setItemIsbn(itemDto.getItemIsbn());
		item.setItemMpn(itemDto.getItemMpn());
		item.setItemCategory(itemDto.getItemCategory());
		item.setItemUnit(itemDto.getItemUnit());
		item.setCriticalQty(itemDto.getCriticalQty());
		item.setItemBrand(itemDto.getItemBrand());
		item.setIsRefundable(itemDto.getIsRefundable());
		DimensionDTO dimension = itemDto.getItemDimension();
		if (dimension != null) {
			item.setItemLength(dimension.getLength());
			item.setItemHeight(dimension.getHeight());
			item.setItemBreadth(dimension.getBreadth());
		}
		item.setItemPicUrl(itemDto.getItemPicUrl());
		return item;
	}

	public ItemDTO mapToItemDTO(Item item) {
		Assert.notNull(item, "Item: " + SystemType.MUST_NOT_BE_NULL);
		ItemDTO itemDto = new ItemDTO();
		itemDto.setItemId(item.getItemId());
		itemDto.setItemName(item.getItemName());
		itemDto.setItemSku(item.getItemSku());
		itemDto.setItemPrice(item.getItemPrice());
		itemDto.setItemType(item.getItemType());
		itemDto.setWeightInKg(item.getWeightInKg());
		itemDto.setItemUpc(item.getItemUpc());
		itemDto.setItemEan(item.getItemEan());
		itemDto.setItemIsbn(item.getItemIsbn());
		itemDto.setItemMpn(item.getItemMpn());
		itemDto.setItemCategory(item.getItemCategory());
		itemDto.setItemUnit(item.getItemUnit());
		itemDto.setItemBrand(item.getItemBrand());
		itemDto.setIsRefundable(item.getIsRefundable());
		DimensionDTO dimension = new DimensionDTO();
		dimension.setLength(item.getItemLength());
		dimension.setHeight(item.getItemHeight());
		dimension.setBreadth(item.getItemBreadth());
		itemDto.setItemDimension(dimension);
		itemDto.setCriticalQty(item.getCriticalQty());
		itemDto.setItemPicUrl(item.getItemPicUrl());
		List<Inventory> inventories = inventoryService.filterInventory(item.getItemId());
		if (inventories != null && inventories.size() > 0) {
			itemDto.setInventory(mapToInventoryDetailDTO(inventories.get(0)));
		}
		return itemDto;
	}

	private InventoryDetailDTO mapToInventoryDetailDTO(Inventory inventory) {
		if (inventory == null)
			return null;
		InventoryDetailDTO inventoryDto = new InventoryDetailDTO();
		inventoryDto.setInventoryId(inventory.getInventoryId());
		inventoryDto.setStartingQty(inventory.getStartingQty());
		inventoryDto.setStartingCost(inventory.getStartingCost());
		inventoryDto.setCriticalQty(inventory.getCriticalQty());
		inventoryDto.setExpectedQty(inventory.getExpectedQty());
		inventoryDto.setQtyOnHand(inventory.getQtyOnHand());
		inventoryDto.setQtyCommitted(inventory.getQtyCommitted());
		inventoryDto.setAvgCost(inventory.getAvgCost());
		return inventoryDto;
	}

	public PurchaseOrder mapToPurchaseOrder(PurchaseOrderDTO purchaseOrderDto) throws ParseException {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setPurchaseOrderId(purchaseOrderDto.getPurchaseOrderId());
		if (purchaseOrderDto.getVendor() != null)
			purchaseOrder.setVendor(companyService.findCompanyById(purchaseOrderDto.getVendor().getCompanyId()));
		purchaseOrder.setOrderedOn(systemUtil.convertToDate(purchaseOrderDto.getOrderedOn()));
		purchaseOrder.setExpectedOn(systemUtil.convertToDate(purchaseOrderDto.getExpectedOn()));
		purchaseOrder.setReceivedOn(systemUtil.convertToDate(purchaseOrderDto.getReceivedOn()));
		purchaseOrder.setReferenceNumber(purchaseOrderDto.getReferenceNumber());
		purchaseOrder.setPurchaseOrderStatus(purchaseOrderDto.getPurchaseOrderStatus());
		List<ItemPurchaseDetailDTO> itemPurchaseDetailDtos = purchaseOrderDto.getItemPurchaseDetail();
		List<ItemPurchaseDetail> itemPurchaseDetails = null;
		ItemPurchaseDetail itemPurchaseDetail = null;
		Double grossAmount = 0d;
		if (itemPurchaseDetailDtos != null && itemPurchaseDetailDtos.size() > 0) {
			itemPurchaseDetails = new ArrayList<>();
			for (ItemPurchaseDetailDTO itemPurchaseDetailDto : itemPurchaseDetailDtos) {
				itemPurchaseDetail = mapToItemPurchaseDetail(itemPurchaseDetailDto);
				if (itemPurchaseDetail.getAmount() != null)
					grossAmount += itemPurchaseDetail.getAmount();
				itemPurchaseDetails.add(itemPurchaseDetail);
			}
		}
		purchaseOrder.setGrossAmount(grossAmount);
		purchaseOrder.setDiscountInAmount(
				purchaseOrderDto.getDiscountInAmount() == null ? 0 : purchaseOrderDto.getDiscountInAmount());
		if (purchaseOrder.getDiscountInAmount() != null)
			purchaseOrder.setTotalAmount(grossAmount - purchaseOrder.getDiscountInAmount());
		purchaseOrder.setItemPurchaseDetail(itemPurchaseDetails);
		return purchaseOrder;
	}

	public PurchaseOrderDTO mapToPurchaseOrderDTO(PurchaseOrder purchaseOrder) throws ParseException {
		PurchaseOrderDTO purchaseOrderDto = new PurchaseOrderDTO();
		purchaseOrderDto.setPurchaseOrderId(purchaseOrder.getPurchaseOrderId());
		purchaseOrderDto.setVendor(companyMapper.mapToCompanySummaryDTO(purchaseOrder.getVendor()));
		purchaseOrderDto.setOrderedOn(systemUtil.convertToDateString(purchaseOrder.getOrderedOn()));
		purchaseOrderDto.setExpectedOn(systemUtil.convertToDateString(purchaseOrder.getExpectedOn()));
		purchaseOrderDto.setReceivedOn(systemUtil.convertToDateString(purchaseOrder.getReceivedOn()));
		purchaseOrderDto.setReferenceNumber(purchaseOrder.getReferenceNumber());
		purchaseOrderDto.setPurchaseOrderStatus(purchaseOrder.getPurchaseOrderStatus());
		purchaseOrderDto.setDiscountInAmount(purchaseOrder.getDiscountInAmount());
		purchaseOrderDto.setGrossAmount(purchaseOrder.getGrossAmount());
		purchaseOrderDto.setTotalAmount(purchaseOrder.getTotalAmount());
		List<ItemPurchaseDetail> itemPurchaseDetails = purchaseOrder.getItemPurchaseDetail();
		List<ItemPurchaseDetailDTO> itemPurchaseDetailDtos = null;
		if (itemPurchaseDetails != null && itemPurchaseDetails.size() > 0) {
			itemPurchaseDetailDtos = new ArrayList();
			for (ItemPurchaseDetail itemPurchaseDetail : itemPurchaseDetails) {
				itemPurchaseDetailDtos.add(mapToItemPurchaseDetailDTO(itemPurchaseDetail));
			}
		}
		purchaseOrderDto.setItemPurchaseDetail(itemPurchaseDetailDtos);
		return purchaseOrderDto;
	}

	private ItemPurchaseDetail mapToItemPurchaseDetail(ItemPurchaseDetailDTO itemPurchaseDetailDto) {
		ItemPurchaseDetail itemPurchaseDetail = new ItemPurchaseDetail();
		itemPurchaseDetail.setItemPurchaseDetailId(itemPurchaseDetailDto.getItemPurchaseDetailId());
		itemPurchaseDetail.setItem(mapToItem(itemPurchaseDetailDto.getItem()));
		itemPurchaseDetail.setQtyOrdered(
				itemPurchaseDetailDto.getQtyOrderedTotal() == null ? 0 : itemPurchaseDetailDto.getQtyOrderedTotal());
		itemPurchaseDetail.setQtyReceived(
				itemPurchaseDetailDto.getQtyReceived() == null ? 0 : itemPurchaseDetailDto.getQtyReceived());
		itemPurchaseDetail.setQtyRemaining(itemPurchaseDetail.getQtyOrdered() - itemPurchaseDetail.getQtyReceived());
		itemPurchaseDetail.setRate(itemPurchaseDetailDto.getRate());
		if (itemPurchaseDetailDto.getRate() != null && itemPurchaseDetailDto.getQtyOrderedTotal() != null)
			itemPurchaseDetail.setAmount(itemPurchaseDetailDto.getRate() * itemPurchaseDetailDto.getQtyOrderedTotal());
		return itemPurchaseDetail;
	}

	private ItemPurchaseDetailDTO mapToItemPurchaseDetailDTO(ItemPurchaseDetail itemPurchaseDetail) {
		ItemPurchaseDetailDTO itemPurchaseDetailDto = new ItemPurchaseDetailDTO();
		itemPurchaseDetailDto.setItemPurchaseDetailId(itemPurchaseDetail.getItemPurchaseDetailId());
		itemPurchaseDetailDto.setItem(mapToItemDTO(itemPurchaseDetail.getItem()));
		itemPurchaseDetailDto.setQtyOrderedTotal(itemPurchaseDetail.getQtyOrdered());
		itemPurchaseDetailDto.setQtyReceivedTotal(itemPurchaseDetail.getQtyReceived());
		itemPurchaseDetailDto.setQtyRemaining(itemPurchaseDetail.getQtyRemaining());
		itemPurchaseDetailDto.setRate(itemPurchaseDetail.getRate());
		itemPurchaseDetailDto.setAmount(itemPurchaseDetail.getAmount());
		return itemPurchaseDetailDto;
	}

	public InventoryDTO mapToInventoryDTO(Inventory inventory) {
		InventoryDTO inventoryDto = new InventoryDTO();
		inventoryDto.setInventoryId(inventory.getInventoryId());
		inventoryDto.setInventoryStartDate(systemUtil.convertToDateString(inventory.getInventoryStartDate()));
		inventoryDto.setLastUpdatedOn(systemUtil.convertToDateString(inventory.getLastUpdatedOn()));
		inventoryDto.setStartingQty(inventory.getStartingQty());
		inventoryDto.setStartingCost(inventory.getStartingCost());
		inventoryDto.setCriticalQty(inventory.getCriticalQty());
		inventoryDto.setExpectedQty(inventory.getExpectedQty());
		inventoryDto.setQtyOnHand(inventory.getQtyOnHand());
		inventoryDto.setQtyCommitted(inventory.getQtyCommitted());
		inventoryDto.setAdjustedUp(inventory.getAdjustedUp());
		inventoryDto.setAdjustedDown(inventory.getAdjustedDown());
		inventoryDto.setQtyIn(inventory.getQtyIn());
		inventoryDto.setQtyOut(inventory.getQtyOut());
		inventoryDto.setAvgCost(inventory.getAvgCost());
		inventoryDto.setItem(mapToItemDTO(inventory.getItem()));
		return inventoryDto;
	}

//	public InventoryWarehouse mapToInventoryWarehouse(InventoryWarehouseDTO inventoryWarehouseDto) {
//		InventoryWarehouse inventoryWarehouse = new InventoryWarehouse();
//		inventoryWarehouse.setInventoryWarehouseId(inventoryWarehouseDto.getInventoryWarehouseId());
//		inventoryWarehouse.setName(inventoryWarehouseDto.getName());
//		inventoryWarehouse.setAddress(generalMapper.mapToAddress(inventoryWarehouseDto.getAddress()));
//		inventoryWarehouse.setContactNumber(inventoryWarehouseDto.getContactNumber());
//		inventoryWarehouse.setContactPerson(inventoryWarehouseDto.getContactPerson());
//		inventoryWarehouse.setAdminCompany(adminService.findAdminCompanyById(SystemType.ADMIN_COMPANY_ID));
//		return inventoryWarehouse;
//	}
//
//	public InventoryWarehouseDTO mapToInventoryWarehouseDTO(InventoryWarehouse inventoryWarehouse) {
//		InventoryWarehouseDTO inventoryWarehouseDto = new InventoryWarehouseDTO();
//		inventoryWarehouseDto.setInventoryWarehouseId(inventoryWarehouse.getInventoryWarehouseId());
//		inventoryWarehouseDto.setName(inventoryWarehouse.getName());
//		inventoryWarehouseDto.setAddress(generalMapper.mapToAddressDTO(inventoryWarehouse.getAddress()));
//		inventoryWarehouseDto.setContactNumber(inventoryWarehouse.getContactNumber());
//		inventoryWarehouseDto.setContactPerson(inventoryWarehouse.getContactPerson());
//		return inventoryWarehouseDto;
//	}

	public Inventory mapToInventory(InventoryDTO inventoryDto) throws ParseException {
		Inventory inventory = new Inventory();
		inventory.setInventoryId(inventoryDto.getInventoryId());
		inventory.setInventoryStartDate(systemUtil.convertToDate(inventoryDto.getInventoryStartDate()));
		inventory.setLastUpdatedOn(systemUtil.convertToDate(inventoryDto.getLastUpdatedOn()));
		inventory.setStartingQty(inventoryDto.getStartingQty());
		inventory.setStartingCost(inventoryDto.getStartingCost());
		inventory.setCriticalQty(inventoryDto.getCriticalQty());
		inventory.setExpectedQty(inventoryDto.getExpectedQty());
		inventory.setQtyOnHand(inventoryDto.getQtyOnHand());
		inventory.setQtyCommitted(inventoryDto.getQtyCommitted());
		inventory.setAdjustedUp(inventoryDto.getAdjustedUp());
		inventory.setAdjustedDown(inventoryDto.getAdjustedDown());
		inventory.setQtyIn(inventoryDto.getQtyIn());
		inventory.setQtyOut(inventoryDto.getQtyOut());
		inventory.setAvgCost(inventoryDto.getAvgCost());
		if (inventoryDto.getItem() != null)
			inventory.setItem(inventoryService.getItemById(inventoryDto.getItem().getItemId()));
		return inventory;
	}

	public InventoryTransactionInfoDTO mapToInventoryTransactionInfoDTO(InventoryTransactionInfo info) {
		InventoryTransactionInfoDTO infoDto = new InventoryTransactionInfoDTO();
		infoDto.setInventoryTransactionInfoId(info.getInventoryTransactionInfoId());
		infoDto.setAction(info.getAction());
		infoDto.setDate(systemUtil.convertToDateString(info.getDate()));
		infoDto.setCost(info.getCost());
		infoDto.setDescription(info.getDescription());
		infoDto.setQty(info.getQty());
		infoDto.setQtyOnHand(info.getQtyOnHand());
		infoDto.setTotalValue(info.getTotalValue());
		if (info.getInventory() != null)
			infoDto.setInventoryId(info.getInventory().getInventoryId());
		if (info.getPurchaseOrder() != null)
			infoDto.setPurchaseOrderId(info.getPurchaseOrder().getPurchaseOrderId());
		if (info.getWorkOrder() != null)
			infoDto.setWorkOrderId(info.getWorkOrder().getWorkOrderId());
		return infoDto;
	}

	public WorkOrder mapToWorkOrder(WorkOrderDTO workOrderDto) throws ParseException {
		WorkOrder workOrder = new WorkOrder();
		workOrder.setWorkOrderId(workOrderDto.getWorkOrderId());
		workOrder.setWorkOrderStatus(workOrderDto.getWorkOrderStatus());
		workOrder.setWorkOrderCategory(workOrderDto.getWorkOrderCategory());
		workOrder.setWorkOrderSubCategory(workOrderDto.getWorkOrderSubCategory());
		if (workOrderDto.getWorkshop() != null)
			workOrder.setWorkshop(companyService.findCompanyById(workOrderDto.getWorkshop().getCompanyId()));
		workOrder.setWorkOrderDate(systemUtil.convertToDate(workOrderDto.getWorkOrderDate()));
		workOrder.setBaseStation(workOrderDto.getBaseStation());
		workOrder.setPriority(workOrderDto.getPriority());
		workOrder.setReferenceNumber(workOrderDto.getReferenceNumber());
		workOrder.setActivityDetail(workOrderDto.getActivityDetail());
		workOrder.setSenderName(workOrderDto.getSenderName());
		workOrder.setTotalAmount(workOrderDto.getTotalAmount());
		if (workOrderDto.getTractor() != null)
			workOrder.setTractor(assetService.findTractorById(workOrderDto.getTractor().getTractorId()));
		workOrder.setTractorKm(workOrderDto.getTractorKm());
		List<WorkOrderItemDetail> workOrderItemDetails = new ArrayList<>();
		for (WorkOrderItemDetailDTO workOrderItemDetailDto : workOrderDto.getWorkOrderItemDetails()) {
			workOrderItemDetails.add(mapToWorkOrderItemDetail(workOrderItemDetailDto));
		}
		workOrder.setWorkOrderItemDetail(workOrderItemDetails);
		return workOrder;
	}

	public WorkOrderDTO mapToWorkOrderDTO(WorkOrder workOrder) throws ParseException {
		WorkOrderDTO workOrderDto = new WorkOrderDTO();
		workOrderDto.setWorkOrderId(workOrder.getWorkOrderId());
		workOrderDto.setWorkOrderStatus(workOrder.getWorkOrderStatus());
		workOrderDto.setWorkOrderCategory(workOrder.getWorkOrderCategory());
		workOrderDto.setWorkOrderSubCategory(workOrder.getWorkOrderSubCategory());
		if (workOrder.getWorkshop() != null)
			workOrderDto.setWorkshop(companyMapper.mapToCompanySummaryDTO(workOrder.getWorkshop()));
		workOrderDto.setWorkOrderDate(systemUtil.convertToDateString(workOrder.getWorkOrderDate()));
		workOrderDto.setBaseStation(workOrder.getBaseStation());
		workOrderDto.setPriority(workOrder.getPriority());
		workOrderDto.setReferenceNumber(workOrder.getReferenceNumber());
		workOrderDto.setTotalAmount(workOrder.getTotalAmount());
		workOrderDto.setActivityDetail(workOrder.getActivityDetail());
		workOrderDto.setSenderName(workOrder.getSenderName());
		if (workOrder.getTractor() != null)
			workOrderDto.setTractor(assetMapper.mapToTractorSummaryDTO(workOrder.getTractor()));
		workOrderDto.setTractorKm(workOrder.getTractorKm());
		List<WorkOrderItemDetailDTO> workOrderItemDetailDtos = new ArrayList<>();
		for (WorkOrderItemDetail workOrderItemDetail : workOrder.getWorkOrderItemDetail()) {
			workOrderItemDetailDtos.add(mapToWorkOrderItemDetailDTO(workOrderItemDetail));
		}
		workOrderDto.setWorkOrderItemDetails(workOrderItemDetailDtos);
		return workOrderDto;
	}

	private WorkOrderItemDetailDTO mapToWorkOrderItemDetailDTO(WorkOrderItemDetail wprkOrderItemDetail) {
		WorkOrderItemDetailDTO workOrderItemDetailDto = new WorkOrderItemDetailDTO();
		workOrderItemDetailDto.setItem(mapToItemDTO(wprkOrderItemDetail.getItem()));
		workOrderItemDetailDto.setQtyUsed(wprkOrderItemDetail.getQtyUsed());
		workOrderItemDetailDto.setRate(wprkOrderItemDetail.getRate());
		workOrderItemDetailDto.setAmount(wprkOrderItemDetail.getAmount());
		workOrderItemDetailDto.setWorkOrderItemDetailId(wprkOrderItemDetail.getWorkOrderItemDetailId());
		return workOrderItemDetailDto;
	}

	private WorkOrderItemDetail mapToWorkOrderItemDetail(WorkOrderItemDetailDTO workOrderItemDetailDto) {
		WorkOrderItemDetail workOrderItemDetail = new WorkOrderItemDetail();
		workOrderItemDetail.setItem(mapToItem(workOrderItemDetailDto.getItem()));
		workOrderItemDetail.setQtyUsed(workOrderItemDetailDto.getQtyUsed());
		workOrderItemDetail.setRate(workOrderItemDetailDto.getRate());
		workOrderItemDetail.setAmount(workOrderItemDetailDto.getAmount());
		workOrderItemDetail.setWorkOrderItemDetailId(workOrderItemDetailDto.getWorkOrderItemDetailId());
		return workOrderItemDetail;
	}

	public InventoryAdjustment mapToInventoryAdjustment(InventoryAdjustmentDTO inventoryAdjustmentDto)
			throws ParseException {
		InventoryAdjustment inventoryAdjustment = new InventoryAdjustment();
		inventoryAdjustment.setInventoryAdjustmentId(inventoryAdjustmentDto.getInventoryAdjustmentId());
		inventoryAdjustment.setInventoryAdjustmentDate(
				systemUtil.convertToDate(inventoryAdjustmentDto.getInventoryAdjustmentDate()));
		inventoryAdjustment.setItem(mapToItem(inventoryAdjustmentDto.getItem()));
		inventoryAdjustment.setAdjustmentQty(inventoryAdjustmentDto.getAdjustmentQty());
		inventoryAdjustment.setAdjustmentReason(inventoryAdjustmentDto.getAdjustmentReason());
		inventoryAdjustment.setReferenceNumber(inventoryAdjustmentDto.getReferenceNumber());
		return inventoryAdjustment;
	}

	public InventoryAdjustmentDTO mapToInventoryAdjustment(InventoryAdjustment inventoryAdjustment)
			throws ParseException {
		InventoryAdjustmentDTO inventoryAdjustmentDto = new InventoryAdjustmentDTO();
		inventoryAdjustmentDto.setInventoryAdjustmentId(inventoryAdjustment.getInventoryAdjustmentId());
		inventoryAdjustmentDto.setInventoryAdjustmentDate(
				systemUtil.convertToDateString(inventoryAdjustment.getInventoryAdjustmentDate()));
		inventoryAdjustmentDto.setItem(mapToItemDTO(inventoryAdjustment.getItem()));
		inventoryAdjustmentDto.setAdjustmentQty(inventoryAdjustment.getAdjustmentQty());
		inventoryAdjustmentDto.setAdjustmentReason(inventoryAdjustment.getAdjustmentReason());
		inventoryAdjustmentDto.setReferenceNumber(inventoryAdjustment.getReferenceNumber());
		return inventoryAdjustmentDto;
	}

}
