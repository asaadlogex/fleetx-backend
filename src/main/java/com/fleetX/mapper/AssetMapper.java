package com.fleetX.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.dao.implementation.CompanyDAO;
import com.fleetX.dto.AssetCombinationDTO;
import com.fleetX.dto.AssetDTO;
import com.fleetX.dto.ContainerDTO;
import com.fleetX.dto.EmployeeSummaryDTO;
import com.fleetX.dto.TrackerDTO;
import com.fleetX.dto.TractorDTO;
import com.fleetX.dto.TractorSummaryDTO;
import com.fleetX.dto.TrailerDTO;
import com.fleetX.dto.TrailerSummaryDTO;
import com.fleetX.dto.TyreDTO;
import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.entity.employee.Employee;
import com.fleetX.service.IAdminService;
import com.fleetX.service.IAssetService;
import com.fleetX.service.IEmployeeService;

@Component
public class AssetMapper {

	@Autowired
	CompanyDAO companyDAO;
	@Autowired
	IAssetService assetService;
	@Autowired
	IAdminService adminService;
	@Autowired
	IEmployeeService employeeService;
	@Autowired
	EmployeeMapper employeeMapper;
	@Autowired
	SystemUtil systemUtil;

	public Asset mapToAsset(AssetDTO assetDto) throws ParseException {
		Assert.notNull(assetDto, "Asset: " + SystemType.MUST_NOT_BE_NULL);
		Asset asset = new Asset();
		asset.setAssetId(assetDto.getAssetId());
		asset.setAssetMake(assetDto.getAssetMake());
		asset.setColor(assetDto.getColor());
		asset.setGrossWeight(assetDto.getGrossWeight());
		if (assetDto.getInduction() != null)
			asset.setInduction(systemUtil.convertToDate(assetDto.getInduction()));
		asset.setModel(assetDto.getModel());
		asset.setNumberOfTyres(assetDto.getNumberOfTyres());
		asset.setStatus(assetDto.getStatus());
		asset.setTyreSize(assetDto.getTyreSize());
		asset.setVin(assetDto.getVin());
		asset.setWeightUnit(assetDto.getWeightUnit());
		asset.setYear(assetDto.getYear());
		asset.setLeaseType(assetDto.getLeaseType());
		if (assetDto.getLeaseType() != null)
			if (assetDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
				asset.setSupplier(companyDAO.findCompanyById(assetDto.getSupplierId()));
			}
		if (assetDto.getTyres() != null && assetDto.getTyres().size() > 0)
			asset.setTyres(mapToTyres(assetDto.getTyres()));
		return asset;
	}

	private List<Tyre> mapToTyres(List<TyreDTO> tyresDto) {
		Assert.notNull(tyresDto, "Tyres: " + SystemType.MUST_NOT_BE_NULL);
		List<Tyre> tyres = new ArrayList<>();
		for (TyreDTO tyreDto : tyresDto) {
			tyres.add(mapToTyre(tyreDto));
		}
		return tyres;
	}

	public Tyre mapToTyre(TyreDTO tyreDto) {
		Assert.notNull(tyreDto, "Tyre: " + SystemType.MUST_NOT_BE_NULL);
		Tyre tyre = new Tyre();
		tyre.setTyreId(tyreDto.getTyreId());
		tyre.setMake(tyreDto.getMake());
		tyre.setStartingKm(tyreDto.getStartingKm());
		tyre.setStatus(tyreDto.getStatus());
		tyre.setTotalKm(tyreDto.getTotalKm());
		tyre.setTyreId(tyreDto.getTyreId());
		tyre.setLeaseType(tyreDto.getLeaseType());
		tyre.setTyreSerialNumber(tyreDto.getTyreSerialNumber());
		if (tyreDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			tyre.setSupplier(companyDAO.findCompanyById(tyreDto.getSupplierId()));
		}
		tyre.setAdminCompany(adminService.findAdminCompanyById(tyreDto.getAdminCompanyId()));
		return tyre;
	}

	public Container mapToContainer(ContainerDTO containerDto) throws ParseException {
		Assert.notNull(containerDto, "Container: " + SystemType.MUST_NOT_BE_NULL);
		Container container = new Container();
		container.setContainerId(containerDto.getContainerId());
		container.setLeaseType(containerDto.getLeaseType());
		container.setContainerSize(containerDto.getContainerSize());
		container.setContainerType(containerDto.getContainerType());
		container.setContainerWeight(containerDto.getContainerWeight());
		container.setContainerWeightUnit(containerDto.getContainerWeightUnit());
		container.setStatus(containerDto.getStatus());
		container.setContainerMake(containerDto.getContainerMake());
		container.setContainerNumber(containerDto.getContainerNumber());
		container.setContainerYear(containerDto.getContainerYear());
		if (containerDto.getPurchaseDate() != null)
			container.setPurchaseDate(systemUtil.convertToDate(containerDto.getPurchaseDate()));
		if (containerDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			container.setSupplier(companyDAO.findCompanyById(containerDto.getSupplierId()));
		}
		container.setAdminCompany(adminService.findAdminCompanyById(containerDto.getAdminCompanyId()));
		return container;
	}

	public Tractor mapToTractor(TractorDTO tractorDto) throws ParseException {
		Assert.notNull(tractorDto, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		Tractor tractor = new Tractor();
		tractor.setTractorId(tractorDto.getTractorId());
		tractor.setAsset(mapToAsset(tractorDto.getAsset()));
		tractor.setCurrentOdometer(tractorDto.getCurrentOdometer());
		tractor.setEngineNumber(tractorDto.getEngineNumber());
		tractor.setHp(tractorDto.getHp());
		tractor.setEngineType(tractorDto.getEngineType());
		tractor.setFuelCapacity(tractorDto.getFuelCapacity());
		tractor.setNumberOfFuelTanks(tractorDto.getNumberOfFuelTanks());
		tractor.setNumberOfGears(tractorDto.getNumberOfGears());
		tractor.setNumberPlate(tractorDto.getNumberPlate());
		tractor.setOdometerUnit(tractorDto.getOdometerUnit());
		tractor.setStartingOdometer(tractorDto.getStartingOdometer());
		tractor.setTransmission(tractorDto.getTransmission());
		tractor.setAdminCompany(adminService.findAdminCompanyById(tractorDto.getAdminCompanyId()));
		return tractor;
	}

	public Tracker mapToTracker(TrackerDTO trackerDto) throws ParseException {
		Assert.notNull(trackerDto, "Tracker: " + SystemType.MUST_NOT_BE_NULL);
		Tracker tracker = new Tracker();
		tracker.setTrackerId(trackerDto.getTrackerId());
		tracker.setContractTerms(trackerDto.getContractTerms());
		tracker.setLeaseType(trackerDto.getLeaseType());
		tracker.setTrackerName(trackerDto.getTrackerName());
		tracker.setTrackerNumber(trackerDto.getTrackerNumber());
		if (trackerDto.getInstallationDate() != null)
			tracker.setInstallationDate(systemUtil.convertToDate(trackerDto.getInstallationDate()));
		if (trackerDto.getReturnDate() != null)
			tracker.setReturnDate(systemUtil.convertToDate(trackerDto.getReturnDate()));
		tracker.setStatus(trackerDto.getStatus());
		if (trackerDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			if (trackerDto.getSupplierId() != null)
				tracker.setSupplier(companyDAO.findCompanyById(trackerDto.getSupplierId()));
		}
		tracker.setAdminCompany(adminService.findAdminCompanyById(trackerDto.getAdminCompanyId()));
		return tracker;
	}

	public Trailer mapToTrailer(TrailerDTO trailerDto) throws ParseException {
		Assert.notNull(trailerDto, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		Trailer trailer = new Trailer();
		trailer.setTrailerId(trailerDto.getTrailerId());
		trailer.setAsset(mapToAsset(trailerDto.getAsset()));
		trailer.setTrailerSize(trailerDto.getTrailerSize());
		trailer.setTrailerType(trailerDto.getTrailerType());
		trailer.setAdminCompany(adminService.findAdminCompanyById(trailerDto.getAdminCompanyId()));
		return trailer;
	}

	public AssetDTO mapToAssetDTO(Asset asset) throws ParseException {
		Assert.notNull(asset, "Asset: " + SystemType.MUST_NOT_BE_NULL);
		AssetDTO assetDto = new AssetDTO();
		assetDto.setAssetId(asset.getAssetId());
		assetDto.setAssetMake(asset.getAssetMake());
		assetDto.setColor(asset.getColor());
		assetDto.setGrossWeight(asset.getGrossWeight());
		if (asset.getInduction() != null)
			assetDto.setInduction(systemUtil.convertToDateString(asset.getInduction()));
		assetDto.setLeaseType(asset.getLeaseType());
		assetDto.setModel(asset.getModel());
		assetDto.setNumberOfTyres(asset.getNumberOfTyres());
		assetDto.setStatus(asset.getStatus());
		assetDto.setTyreSize(asset.getTyreSize());
		assetDto.setVin(asset.getVin());
		assetDto.setWeightUnit(asset.getWeightUnit());
		assetDto.setYear(asset.getYear());
		if (assetDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			if (asset.getSupplier() != null)
				assetDto.setSupplierId(asset.getSupplier().getCompanyId());
		}
		if (asset.getTyres() != null && asset.getTyres().size() > 0)
			assetDto.setTyres(mapToTyresDTO(asset.getTyres()));
		return assetDto;
	}

	private List<TyreDTO> mapToTyresDTO(List<Tyre> tyres) {
		Assert.notNull(tyres, "Tyres: " + SystemType.MUST_NOT_BE_NULL);
		List<TyreDTO> tyresDto = new ArrayList<>();
		for (Tyre tyre : tyres) {
			tyresDto.add(mapToTyreDTO(tyre));
		}
		return tyresDto;
	}

	public TyreDTO mapToTyreDTO(Tyre tyre) {
		Assert.notNull(tyre, "Tyre: " + SystemType.MUST_NOT_BE_NULL);
		TyreDTO tyreDto = new TyreDTO();
		tyreDto.setMake(tyre.getMake());
		tyreDto.setStartingKm(tyre.getStartingKm());
		tyreDto.setStatus(tyre.getStatus());
		tyreDto.setTotalKm(tyre.getTotalKm());
		tyreDto.setTyreId(tyre.getTyreId());
		tyreDto.setLeaseType(tyre.getLeaseType());
		tyreDto.setTyreSerialNumber(tyre.getTyreSerialNumber());
		if (tyreDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			if (tyre.getSupplier() != null)
				tyreDto.setSupplierId(tyre.getSupplier().getCompanyId());
		}
		tyreDto.setAdminCompanyId(tyre.getAdminCompany().getAdminCompanyId());
		return tyreDto;
	}

	public ContainerDTO mapToContainerDTO(Container container) throws ParseException {
		Assert.notNull(container, "Container: " + SystemType.MUST_NOT_BE_NULL);
		ContainerDTO containerDto = new ContainerDTO();
		containerDto.setContainerId(container.getContainerId());
		containerDto.setAdminCompanyId(container.getAdminCompany().getAdminCompanyId());
		containerDto.setLeaseType(container.getLeaseType());
		containerDto.setContainerSize(container.getContainerSize());
		containerDto.setContainerType(container.getContainerType());
		containerDto.setContainerWeight(container.getContainerWeight());
		containerDto.setContainerWeightUnit(container.getContainerWeightUnit());
		containerDto.setStatus(container.getStatus());
		containerDto.setContainerMake(container.getContainerMake());
		containerDto.setContainerNumber(container.getContainerNumber());
		containerDto.setContainerYear(container.getContainerYear());
		containerDto.setPurchaseDate(systemUtil.convertToDateString(container.getPurchaseDate()));
		if (containerDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			if (container.getSupplier() != null)
				containerDto.setSupplierId(container.getSupplier().getCompanyId());
		}
		return containerDto;
	}

	public TractorDTO mapToTractorDTO(Tractor tractor) throws ParseException {
		Assert.notNull(tractor, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		TractorDTO tractorDto = new TractorDTO();
		tractorDto.setTractorId(tractor.getTractorId());
		tractorDto.setAsset(mapToAssetDTO(tractor.getAsset()));
		tractorDto.setCurrentOdometer(tractor.getCurrentOdometer());
		tractorDto.setEngineNumber(tractor.getEngineNumber());
		tractorDto.setHp(tractor.getHp());
		tractorDto.setEngineType(tractor.getEngineType());
		tractorDto.setFuelCapacity(tractor.getFuelCapacity());
		tractorDto.setNumberOfFuelTanks(tractor.getNumberOfFuelTanks());
		tractorDto.setNumberOfGears(tractor.getNumberOfGears());
		tractorDto.setNumberPlate(tractor.getNumberPlate());
		tractorDto.setOdometerUnit(tractor.getOdometerUnit());
		tractorDto.setStartingOdometer(tractor.getStartingOdometer());
		tractorDto.setTransmission(tractor.getTransmission());
		if (tractor.getAdminCompany() != null)
			tractorDto.setAdminCompanyId(tractor.getAdminCompany().getAdminCompanyId());
		return tractorDto;
	}

	public TrackerDTO mapToTrackerDTO(Tracker tracker) throws ParseException {
		Assert.notNull(tracker, "Tracker: " + SystemType.MUST_NOT_BE_NULL);
		TrackerDTO trackerDto = new TrackerDTO();
		trackerDto.setTrackerId(tracker.getTrackerId());
		if (tracker.getInstallationDate() != null)
			trackerDto.setInstallationDate(systemUtil.convertToDateString(tracker.getInstallationDate()));
		if (tracker.getReturnDate() != null)
			trackerDto.setReturnDate(systemUtil.convertToDateString(tracker.getReturnDate()));
		trackerDto.setContractTerms(tracker.getContractTerms());
		trackerDto.setLeaseType(tracker.getLeaseType());
		trackerDto.setStatus(tracker.getStatus());
		trackerDto.setTrackerName(tracker.getTrackerName());
		trackerDto.setTrackerNumber(tracker.getTrackerNumber());
		if (trackerDto.getLeaseType().getValue().equals(SystemType.CONTRACTUAL)) {
			if (tracker.getSupplier() != null)
				trackerDto.setSupplierId(tracker.getSupplier().getCompanyId());
		}
		if (tracker.getAdminCompany() != null)
			trackerDto.setAdminCompanyId(tracker.getAdminCompany().getAdminCompanyId());
		return trackerDto;
	}

	public TrailerDTO mapToTrailerDTO(Trailer trailer) throws ParseException {
		Assert.notNull(trailer, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		TrailerDTO trailerDto = new TrailerDTO();
		trailerDto.setTrailerId(trailer.getTrailerId());
		trailerDto.setAsset(mapToAssetDTO(trailer.getAsset()));
		trailerDto.setTrailerSize(trailer.getTrailerSize());
		trailerDto.setTrailerType(trailer.getTrailerType());
		if (trailer.getAdminCompany() != null)
			trailerDto.setAdminCompanyId(trailer.getAdminCompany().getAdminCompanyId());
		return trailerDto;
	}

	public AssetCombination mapToAssetCombination(AssetCombinationDTO assetCombinationDto) {
		Assert.notNull(assetCombinationDto, "Asset Combination: " + SystemType.MUST_NOT_BE_NULL);
		AssetCombination assetCombination = new AssetCombination();
		assetCombination.setNote(assetCombinationDto.getNote());
		TractorSummaryDTO tractorDto = assetCombinationDto.getTractor();
		TrailerSummaryDTO trailerDto = assetCombinationDto.getTrailer();
		TrackerDTO trackerDto = assetCombinationDto.getTracker();
		ContainerDTO containerDto = assetCombinationDto.getContainer();
		EmployeeSummaryDTO driver1Dto = assetCombinationDto.getDriver1();
		EmployeeSummaryDTO driver2Dto = assetCombinationDto.getDriver2();

		Tractor tractor = null;
		Trailer trailer = null;
		Tracker tracker = null;
		Container container = null;
		Employee driver1 = null;
		Employee driver2 = null;

		if (tractorDto != null)
			tractor = assetService.findTractorById(tractorDto.getTractorId());
		if (trailerDto != null)
			trailer = assetService.findTrailerById(trailerDto.getTrailerId());
		if (trackerDto != null)
			tracker = assetService.findTrackerById(trackerDto.getTrackerId());
		if (containerDto != null)
			container = assetService.findContainerById(containerDto.getContainerId());
		if (driver1Dto != null)
			driver1 = employeeService.findEmployeeById(driver1Dto.getEmployeeId());
		if (driver2Dto != null)
			driver2 = employeeService.findEmployeeById(driver2Dto.getEmployeeId());

		assetCombination.setAssetCombinationId(assetCombinationDto.getAssetCombinationId());
		assetCombination.setTractor(tractor);
		assetCombination.setTrailer(trailer);
		assetCombination.setTracker(tracker);
		assetCombination.setContainer(container);
		assetCombination.setDriver1(driver1);
		assetCombination.setDriver2(driver2);
		assetCombination.setStatus(assetCombinationDto.getStatus());
		return assetCombination;
	}

	public AssetCombinationDTO mapToAssetCombinationDTO(AssetCombination assetCombination) throws ParseException {
		Assert.notNull(assetCombination, "Asset Combination: " + SystemType.MUST_NOT_BE_NULL);
		AssetCombinationDTO assetCombinationDto = new AssetCombinationDTO();
		assetCombinationDto.setAssetCombinationId(assetCombination.getAssetCombinationId());
		assetCombinationDto.setNote(assetCombination.getNote());

		Tractor tractor = assetCombination.getTractor();
		Trailer trailer = assetCombination.getTrailer();
		Tracker tracker = assetCombination.getTracker();
		Container container = assetCombination.getContainer();
		Employee driver1 = assetCombination.getDriver1();
		Employee driver2 = assetCombination.getDriver2();

		if (tractor != null)
			assetCombinationDto.setTractor(mapToTractorSummaryDTO(tractor));
		if (trailer != null)
			assetCombinationDto.setTrailer(mapToTrailerSummaryDTO(trailer));
		if (tracker != null)
			assetCombinationDto.setTracker(mapToTrackerDTO(tracker));
		if (container != null)
			assetCombinationDto.setContainer(mapToContainerDTO(container));
		if (driver1 != null)
			assetCombinationDto.setDriver1(employeeMapper.mapToEmployeeSummaryDTO(driver1));
		if (driver2 != null)
			assetCombinationDto.setDriver2(employeeMapper.mapToEmployeeSummaryDTO(driver2));
		assetCombinationDto.setStatus(assetCombination.getStatus());
		return assetCombinationDto;
	}

	public TractorSummaryDTO mapToTractorSummaryDTO(Tractor tractor) throws ParseException {
		Assert.notNull(tractor, "Tractor: " + SystemType.MUST_NOT_BE_NULL);
		Asset asset = tractor.getAsset();
		TractorSummaryDTO tractorDto = new TractorSummaryDTO();
		tractorDto.setTractorId(tractor.getTractorId());
		tractorDto.setAssetMake(asset.getAssetMake());
		tractorDto.setModel(asset.getModel());
		tractorDto.setStatus(asset.getStatus());
		tractorDto.setVin(asset.getVin());
		tractorDto.setEngineNumber(tractor.getEngineNumber());
		tractorDto.setNumberPlate(tractor.getNumberPlate());
		return tractorDto;
	}

	public TrailerSummaryDTO mapToTrailerSummaryDTO(Trailer trailer) throws ParseException {
		Assert.notNull(trailer, "Trailer: " + SystemType.MUST_NOT_BE_NULL);
		TrailerSummaryDTO trailerDto = new TrailerSummaryDTO();
		Asset asset = trailer.getAsset();
		trailerDto.setTrailerId(trailer.getTrailerId());
		trailerDto.setTrailerSize(trailer.getTrailerSize());
		trailerDto.setTrailerType(trailer.getTrailerType());
		trailerDto.setLeaseType(asset.getLeaseType());
		trailerDto.setVin(asset.getVin());
		trailerDto.setStatus(asset.getStatus());
		return trailerDto;
	}
}
