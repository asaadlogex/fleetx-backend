package com.fleetX.validator;

import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;

public interface IAssetValidator {
	public void validateContainer(Container container);
	public void validateTractor(Tractor tractor);
	public void validateTrailer(Trailer trailer);
	public void validateTracker(Tracker tracker);
	public void validateTyre(Tyre tyre);
	public void validateAssetCombination(AssetCombination assetCombination);
}
