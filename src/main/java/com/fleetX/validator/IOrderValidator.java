package com.fleetX.validator;

import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RentalVehicle;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;

public interface IOrderValidator {
	public void validateJob(Job job);
	public void validateRoadwayBill(RoadwayBill roadwayBill);
	public void validateRentalVehicle(RentalVehicle rentalVehicle);
	public void validateJobFuelDetail(JobFuelDetail jobFuelDetail);
	public void validateStopProgress(StopProgress stopProgress);
	public void validateStop(Stop stop);
	public void validateProduct(Product product);
	public void validateIncome(Income income);
	public void validateExpense(Expense expense);
}
