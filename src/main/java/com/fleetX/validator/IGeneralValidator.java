package com.fleetX.validator;

import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.dropdown.DCity;

public interface IGeneralValidator {
	public void validateAddress(Address address);
	public void validateContact(Contact contact);
	
	public void validateRole(Role role);
	public void validateUser(User user);
	
	public void validateCity(DCity city);
	public void validateArea(DCity city);
}
