package com.fleetX.validator.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.entity.company.Company;
import com.fleetX.entity.order.Warehouse;
import com.fleetX.validator.ICompanyValidator;
import com.fleetX.validator.IGeneralValidator;

@Service
public class CompanyValidator implements ICompanyValidator {
	@Autowired
	IGeneralValidator generalValidator;

	@Override
	public void validateCompany(Company company) {
		if (company.getCompanyName() == null || company.getCompanyName().isBlank())
			throw new IllegalArgumentException();
		if (company.getBusinessType() == null)
			throw new IllegalArgumentException();
		else if (company.getBusinessType().getValue().equals(SystemType.BUSINESS_TYPE_SUPPLIER)
				&& company.getSupplierType() == null)
			throw new IllegalArgumentException();
		if (company.getCompanyAddresses() == null || company.getCompanyAddresses().size() < 1)
			throw new IllegalArgumentException();
		else
			company.getCompanyAddresses().stream().forEach(x -> generalValidator.validateAddress(x));
		if (company.getCompanyContacts() == null || company.getCompanyContacts().size() < 1)
			throw new IllegalArgumentException();
		else
			company.getCompanyContacts().stream().forEach(x -> generalValidator.validateContact(x));
	}

	@Override
	public void validateWarehouse(Warehouse warehouse) {
		if (warehouse.getAddresses() == null || warehouse.getAddresses().size() < 1)
			throw new IllegalArgumentException();
		else
			warehouse.getAddresses().stream().forEach(x -> generalValidator.validateAddress(x));
	}

}
