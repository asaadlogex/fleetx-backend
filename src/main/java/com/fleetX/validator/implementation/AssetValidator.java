package com.fleetX.validator.implementation;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fleetX.config.SystemType;
import com.fleetX.entity.asset.Asset;
import com.fleetX.entity.asset.AssetCombination;
import com.fleetX.entity.asset.Container;
import com.fleetX.entity.asset.Tracker;
import com.fleetX.entity.asset.Tractor;
import com.fleetX.entity.asset.Trailer;
import com.fleetX.entity.asset.Tyre;
import com.fleetX.entity.employee.Employee;
import com.fleetX.validator.IAssetValidator;

@Service
public class AssetValidator implements IAssetValidator {

	@Override
	public void validateContainer(Container container) {
		if (container.getAdminCompany() == null)
			throw new IllegalArgumentException("Admin company is required!");
		if (container.getContainerNumber() == null || container.getContainerNumber().isBlank())
			throw new IllegalArgumentException("Container number is required!");
		if (container.getContainerType() == null)
			throw new IllegalArgumentException("Container type is required!");
		if (container.getLeaseType() == null)
			throw new IllegalArgumentException("Lease type is required!");
		else if (container.getLeaseType().getValue().equals(SystemType.CONTRACTUAL) && container.getSupplier() == null)
			throw new IllegalArgumentException("Supplier is required!");
	}

	@Override
	public void validateTractor(Tractor tractor) {
		if (tractor.getAdminCompany() == null)
			throw new IllegalArgumentException("Admin company is required!");
		if (tractor.getEngineNumber() == null)
			throw new IllegalArgumentException("Engine number is required!");
		if (tractor.getAsset() == null)
			throw new IllegalArgumentException("Asset details are required!");
		else
			validateAsset(tractor.getAsset());
	}

	@Override
	public void validateTrailer(Trailer trailer) {
		if (trailer.getAdminCompany() == null)
			throw new IllegalArgumentException("Admin company is required!");
		if (trailer.getTrailerType() == null)
			throw new IllegalArgumentException("Trailer type is required!");
		if (trailer.getTrailerSize() == null)
			throw new IllegalArgumentException("Trailer size is required!");
		if (trailer.getAsset() == null)
			throw new IllegalArgumentException("Asset details are required!");
		else
			validateAsset(trailer.getAsset());
	}

	@Override
	public void validateTracker(Tracker tracker) {
		if (tracker.getAdminCompany() == null)
			throw new IllegalArgumentException("Admin company is required!");
		if (tracker.getTrackerNumber() == null)
			throw new IllegalArgumentException("Tracker number is required!");
		if (tracker.getLeaseType() == null)
			throw new IllegalArgumentException("Lease type is required!");
		else if (tracker.getLeaseType().getValue().equals(SystemType.CONTRACTUAL) && tracker.getSupplier() == null)
			throw new IllegalArgumentException();
		if (tracker.getInstallationDate() == null)
			throw new IllegalArgumentException("Tracker installation date is required!");
	}

	@Override
	public void validateTyre(Tyre tyre) {
		if (tyre.getAdminCompany() == null)
			throw new IllegalArgumentException("Admin company is required!");
		if (tyre.getLeaseType() == null)
			throw new IllegalArgumentException("Lease type is required!");
		else if (tyre.getLeaseType().equals(SystemType.CONTRACTUAL) && tyre.getSupplier() == null)
			throw new IllegalArgumentException("Supplier ID is required!");
		if (tyre.getTyreSerialNumber() == null || tyre.getTyreSerialNumber().isBlank())
			throw new IllegalArgumentException("Tyre serial number is required!");
//		if (tyre.getMake() == null)
//			throw new IllegalArgumentException("Tyre make is required!");
	}

	@Override
	public void validateAssetCombination(AssetCombination assetCombination) {
		Tractor tractor = assetCombination.getTractor();
		Trailer trailer = assetCombination.getTrailer();
		Tracker tracker = assetCombination.getTracker();
		Container container = assetCombination.getContainer();
		Employee driver1 = assetCombination.getDriver1();
		Employee driver2 = assetCombination.getDriver2();

		Assert.notNull(tractor, "Tractor or Trailer: " + SystemType.MUST_NOT_BE_NULL);
		Assert.notNull(trailer, "Tractor or Trailer: " + SystemType.MUST_NOT_BE_NULL);
		if (driver1 == null && driver2 == null)
			throw new IllegalArgumentException("Driver: " + SystemType.MUST_NOT_BE_NULL);
		if (driver1 != null && driver2 != null && driver1.getEmployeeId().equals(driver2.getEmployeeId()))
			throw new IllegalArgumentException("Driver-1 and Driver-2 cannot be same");
		if (trailer.getTrailerType().getValue().equalsIgnoreCase(SystemType.TRAILER_TYPE_BOX)) {
			Assert.notNull(container, "Container: " + SystemType.MUST_NOT_BE_NULL);
		}

		if (!tractor.getAsset().getStatus().equals(SystemType.STATUS_ACTIVE)) {
			throw new IllegalArgumentException("Tractor: " + SystemType.NOT_ACTIVE);
		}
		if (!trailer.getAsset().getStatus().equals(SystemType.STATUS_ACTIVE)) {
			throw new IllegalArgumentException("Trailer: " + SystemType.NOT_ACTIVE);
		}
		if (tracker != null) {
			if (!tracker.getStatus().equals(SystemType.STATUS_ACTIVE))
				throw new IllegalArgumentException("Tracker: " + SystemType.NOT_ACTIVE);
		}
		if (container != null) {
			if (!container.getStatus().equals(SystemType.STATUS_ACTIVE))
				throw new IllegalArgumentException("Container: " + SystemType.NOT_ACTIVE);
		}
		if (driver1 != null) {
			if (!driver1.getStatus().equals(SystemType.STATUS_ACTIVE))
				throw new IllegalArgumentException("Driver-1: " + SystemType.NOT_ACTIVE);
		}
		if (driver2 != null) {
			if (!driver2.getStatus().equals(SystemType.STATUS_ACTIVE))
				throw new IllegalArgumentException("Driver-2: " + SystemType.NOT_ACTIVE);
		}
	}

	private void validateAsset(Asset asset) {
		if (asset.getLeaseType() == null)
			throw new IllegalArgumentException("Lease type is required!");
		else if (asset.getLeaseType().getValue().equals(SystemType.CONTRACTUAL) && asset.getSupplier() == null)
			throw new IllegalArgumentException("Supplier ID is required!");
		Integer nTyres = asset.getNumberOfTyres();
		if (nTyres == null || nTyres <= 0)
			throw new IllegalArgumentException("Invalid number of tyres!");
//		if (asset.getTyres() == null)
//			throw new IllegalArgumentException("Tyres are required!");
//		else if (asset.getTyres().size() > nTyres + 1)
//			throw new IllegalArgumentException("Invalid number of tyres!");
		if (asset.getVin() == null || asset.getVin().isBlank())
			throw new IllegalArgumentException("Vin number is required!");
//		if (asset.getAssetMake() == null)
//			throw new IllegalArgumentException("Asset make is required!");
	}

}
