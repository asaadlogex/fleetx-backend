package com.fleetX.validator.implementation;

import java.text.ParseException;
import java.time.DateTimeException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.employee.EmployeeContract;
import com.fleetX.validator.IContractValidator;

@Service
public class ContractValidator implements IContractValidator {

	@Autowired
	SystemUtil systemUtil;

	@Override
	public void validateCompanyContract(CompanyContract companyContract) {
		if (companyContract.getAdminCompany() == null)
			throw new IllegalArgumentException("Admin company is required!");
		if (companyContract.getCustomer() == null)
			throw new IllegalArgumentException("Customer is required!");
		if (companyContract.getContractStart() == null)
			throw new IllegalArgumentException("Contract start date is required!");
		if (companyContract.getContractType() == null)
			throw new IllegalArgumentException("Contract type is required!");
		else if (companyContract.getContractType().getValue().equals(SystemType.CONTRACT_TYPE_DEDICATED)
				&& companyContract.getFixedRate() == null)
			throw new IllegalArgumentException("Fixed rate is required!");
		else if (!companyContract.getContractType().getValue().equals(SystemType.CONTRACT_TYPE_DEDICATED)
				&& (companyContract.getDetentionRate() == null || companyContract.getHaltingRate() == null))
			throw new IllegalArgumentException("Detention rate and Halting rate are required!");
	}

	@Override
	public void validateEmployeeContract(EmployeeContract employeeContract) {
		if (employeeContract.getEmployee() == null)
			throw new IllegalArgumentException("Employee is required!");
		if (employeeContract.getStartDate() == null)
			throw new IllegalArgumentException("Contract start date is required!");
	}

	@Override
	public void validateRouteRateContract(RouteRateContract routeRateContract) {
		if (routeRateContract.getRoute() == null)
			throw new IllegalArgumentException("Route is required!");
		try {
			if (routeRateContract.getRouteRates() == null || routeRateContract.getRouteRates().size() < 1)
				throw new IllegalArgumentException("Route rates are required!");
			else
				for (RouteRate routeRate : routeRateContract.getRouteRates()) {
					validateRouteRate(routeRate, routeRateContract.getCompanyContract());
				}
		} catch (ParseException e) {
			throw new DateTimeException("Incorrect date order!");
		}
	}

	@Override
	public void validateRouteRate(RouteRate routeRate, CompanyContract companyContract) {
//		CompanyContract companyContract = routeRate.getRouteRateContract().getCompanyContract();
		Date contractStart = companyContract.getContractStart();
		Date contractEnd = companyContract.getContractEnd();
		String contractType = companyContract.getContractType().getValue();
		Date start = routeRate.getEffectiveFrom();
		Date end = routeRate.getEffectiveTill();
		if (routeRate.getTrailerType() == null)
			throw new IllegalArgumentException("Trailer type is required!");
		if (routeRate.getTrailerSize() == null)
			throw new IllegalArgumentException("Trailer size is required!");
//		if (routeRate.getWeightLow() == null || routeRate.getWeightLow() < 0)
//			throw new IllegalArgumentException("Invalid weight range!");
//		if (routeRate.getWeightHigh() == null || routeRate.getWeightHigh() < routeRate.getWeightLow())
//			throw new IllegalArgumentException("Invalid weight range!");
		if (!contractType.equals(SystemType.CONTRACT_TYPE_PER_TON))
			routeRate.setRatePerTon(null);
		if (start == null)
			throw new IllegalArgumentException("Start date is required!");
		if (!systemUtil.isStartBeforeEnd(contractStart, start))
			throw new IllegalArgumentException("Incorrect date-time order!");
		if (end != null) {
			if (!systemUtil.isStartBeforeEnd(start, end))
				throw new IllegalArgumentException("Incorrect date-time order!");
			if (!systemUtil.isStartBeforeEnd(end, contractEnd))
				throw new IllegalArgumentException("Incorrect date-time order!");
		}
	}

	@Override
	public void validateFuelSupplierContract(FuelSupplierContract fuelSupplierContract) {
		if (fuelSupplierContract.getAdminCompany() == null)
			throw new IllegalArgumentException();
		if (fuelSupplierContract.getSupplier() == null)
			throw new IllegalArgumentException();
		if (fuelSupplierContract.getFuelRates() == null || fuelSupplierContract.getFuelRates().size() < 1)
			throw new IllegalArgumentException();
		else
			fuelSupplierContract.getFuelRates().stream().forEach(x -> validateFuelRate(x));
	}

	@Override
	public void validateFuelRate(FuelRate fuelRate) {
		if (fuelRate.getRatePerLitre() == null)
			throw new IllegalArgumentException();
		Date start = fuelRate.getEffectiveFrom();
		Date end = fuelRate.getEffectiveTill();
		if (start == null)
			throw new IllegalArgumentException();
		if (end != null) {
			if (!systemUtil.isStartBeforeEnd(start, end))
				throw new IllegalArgumentException("Incorrect date-time order!");
		}
	}

}
