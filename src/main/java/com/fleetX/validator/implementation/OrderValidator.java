package com.fleetX.validator.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.entity.dropdown.DRWBStatus;
import com.fleetX.entity.order.Expense;
import com.fleetX.entity.order.Income;
import com.fleetX.entity.order.Job;
import com.fleetX.entity.order.JobFuelDetail;
import com.fleetX.entity.order.Product;
import com.fleetX.entity.order.RentalVehicle;
import com.fleetX.entity.order.RoadwayBill;
import com.fleetX.entity.order.Stop;
import com.fleetX.entity.order.StopProgress;
import com.fleetX.validator.ICompanyValidator;
import com.fleetX.validator.IGeneralValidator;
import com.fleetX.validator.IOrderValidator;

@Service
public class OrderValidator implements IOrderValidator {

	@Autowired
	IGeneralValidator generalValidator;
	@Autowired
	ICompanyValidator companyValidator;
	@Autowired
	SystemUtil systemUtil;

	@Override
	public void validateJob(Job job) {
		if (job.getStartDate() == null)
			throw new IllegalArgumentException("Job start date is required!");
		if (job.getStartKm() == null || job.getStartKm() < 0)
			throw new IllegalArgumentException("Job start KM is required!");
		if (job.getStartBaseStation() == null)
			throw new IllegalArgumentException("Job start base station is required!");
		if (job.getJobAdvance() == null || job.getJobAdvance() < 0)
			throw new IllegalArgumentException("Job advance is required!");
	}

	@Override
	public void validateRoadwayBill(RoadwayBill rwb) {
		if (rwb.getJob() == null)
			throw new IllegalArgumentException("Job is required!");
		else if (rwb.getJob().getJobStatus().equals(SystemType.STATUS_JOB_CLOSE))
			throw new IllegalArgumentException("Job is already closed!");
		else if (!systemUtil.isStartBeforeEnd(rwb.getJob().getStartDate(), rwb.getRwbDate()))
			throw new IllegalArgumentException("RWB date should not be earlier than Job start date!");
		if (rwb.getVehicleOwnership() == null)
			throw new IllegalArgumentException("Vehicle ownership is required!");
		if (rwb.getIsEmpty()) {
			if (rwb.getProducts() != null || rwb.getStops() != null || rwb.getStopProgresses() != null)
				throw new IllegalArgumentException("Invalid data for empty roadway bill");
		} else {
			if (rwb.getCustomerType() == null)
				throw new IllegalArgumentException("Customer type is required!");
			else if (rwb.getCustomerType().getValue().equals(SystemType.CUSTOMER_TYPE_CONTRACTUAL)
					&& rwb.getCustomer() == null) {
				throw new IllegalArgumentException("Customer is required!");
			} else if (rwb.getCustomerType().getValue().equals(SystemType.CUSTOMER_TYPE_OPEN_MARKET)
					&& rwb.getBroker() == null)
				throw new IllegalArgumentException("Broker is required!");
//			if (rwb.getOrderNumber() == null)
//				throw new IllegalArgumentException("Order number is required!");
			if (rwb.getPaymentMode() == null)
				throw new IllegalArgumentException("Payment mode is required!");
		}
		if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {
			if (rwb.getRentalVehicle() == null)
				throw new IllegalArgumentException("Rental vehicle details are required!");
			else
				validateRentalVehicle(rwb.getRentalVehicle());
		} else if (rwb.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN)) {
			if (rwb.getTractor() == null)
				throw new IllegalArgumentException("Tractor is required!");
			if (rwb.getAssetCombinationId() == null)
				throw new IllegalArgumentException("Asset combination ID is required!");
		}

		if (rwb.getRoute() == null)
			throw new IllegalArgumentException("Route is required!");
		if (rwb.getRwbDate() == null)
			throw new IllegalArgumentException("Roadway bill date is required!");
		if (rwb.getRwbStatus().equals(SystemType.STATUS_RWB_EMPTY)) {
			List<StopProgress> stopProgresses = rwb.getStopProgresses();
			for (StopProgress stopProgress : stopProgresses) {
				if (stopProgress.getArrivalTime() == null || stopProgress.getDepartureTime() == null)
					throw new IllegalArgumentException("Cannot change status!");
			}
		}
	}

	@Override
	public void validateRentalVehicle(RentalVehicle rentalVehicle) {
		if (rentalVehicle.getVehicleNumber() == null)
			throw new IllegalArgumentException("Vehicle number is required!");
		if (rentalVehicle.getBroker() == null)
			throw new IllegalArgumentException("Broker is required!");
		if (rentalVehicle.getEquipmentType() == null)
			throw new IllegalArgumentException("Equipment type is required!");
		if (rentalVehicle.getEquipmentSize() == null)
			throw new IllegalArgumentException("Equipment size is required!");
	}

	@Override
	public void validateJobFuelDetail(JobFuelDetail jobFuelDetail) {
		if (jobFuelDetail.getJob() == null)
			throw new IllegalArgumentException("Job is required!");
		if (jobFuelDetail.getFuelDate() == null)
			throw new IllegalArgumentException("Fuel date is required!");
		if (jobFuelDetail.getFuelInLitre() == null || jobFuelDetail.getFuelInLitre() < 0)
			throw new IllegalArgumentException("Fuel quantity (in litres) is required!");
		if (jobFuelDetail.getFuelTankType() == null)
			throw new IllegalArgumentException("Fuel tank type is required!");
		if (jobFuelDetail.getFuelTankType().getValue().equals(SystemType.FUEL_TANK_TYPE_NORMAL)) {
			if (jobFuelDetail.getTractor() == null)
				throw new IllegalArgumentException("Tractor is required!");
		} else {
			if (jobFuelDetail.getTrailer() == null)
				throw new IllegalArgumentException("Trailer is required!");
		}
		if (jobFuelDetail.getSupplier() == null)
			throw new IllegalArgumentException("Fuel supplier is required!");
	}

	@Override
	public void validateStopProgress(StopProgress stopProgress) {
		if (stopProgress.getRoadwayBill() == null)
			throw new IllegalArgumentException("Roadway Bill is required!");
		if (stopProgress.getRoadwayBill().getRwbStatus().equals(SystemType.STATUS_RWB_COMPLETED))
			throw new IllegalArgumentException("Roadway Bill is already completed!");
		if (stopProgress.getVehicleOwnership() == null)
			throw new IllegalArgumentException("Vehicle ownership is required!");
		else if (stopProgress.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_RENTAL)) {
			if (stopProgress.getRentalVehicle() == null)
				throw new IllegalArgumentException("Rental vehicle details are required!");
			else
				validateRentalVehicle(stopProgress.getRentalVehicle());
		} else if (stopProgress.getVehicleOwnership().getValue().equals(SystemType.OWNERSHIP_OWN)
				&& stopProgress.getTractor() == null)
			throw new IllegalArgumentException("Tractor is required!");
		Job job = stopProgress.getRoadwayBill().getJob();
		RoadwayBill rwb = stopProgress.getRoadwayBill();
		if (job.getJobStatus().equals(SystemType.STATUS_JOB_CLOSE))
			throw new IllegalArgumentException("Job is already closed!");
		Date arrival = stopProgress.getArrivalTime();
		Date departure = stopProgress.getDepartureTime();
		if (arrival != null || departure != null)
			if (!systemUtil.isStartBeforeEnd(stopProgress.getArrivalTime(), stopProgress.getDepartureTime()))
				throw new IllegalArgumentException("Incorrect date-time order!");
		Date rwbDate = rwb.getRwbDate();
		Date jobEnd = job.getEndDate();
		if (!systemUtil.isDateInBetween(arrival, rwbDate, jobEnd)
				|| !systemUtil.isDateInBetween(departure, rwbDate, jobEnd))
			throw new IllegalArgumentException("Incorrect date-time order!");
		if (stopProgress.getStop() == null) {
			if (stopProgress.getWarehouse() == null)
				throw new IllegalArgumentException("Warehouse is required!");
			else
				companyValidator.validateWarehouse(stopProgress.getWarehouse());
		}
	}

	@Override
	public void validateStop(Stop stop) {
		if (stop.getRoadwayBill() == null)
			throw new IllegalArgumentException("Roadway Bill is required!");
		else {
			DRWBStatus rwbStatus = stop.getRoadwayBill().getRwbStatus();
			if (rwbStatus.getValue().equals(SystemType.STATUS_RWB_COMPLETED))
				throw new IllegalArgumentException("Roadway Bill is already completed!");
			else if (rwbStatus.getValue().equals(SystemType.STATUS_RWB_DISPATCHED)) {
				if (stop.getStopType() == null)
					throw new IllegalArgumentException("Stop type is required!");
				else if (stop.getStopType().getValue().equals(SystemType.STOP_TYPE_PICKUP)
						|| stop.getStopType().getValue().equals(SystemType.STOP_TYPE_DELIVERY))
					throw new IllegalArgumentException(
							"RWB is dispatched. No more pickup and delivery stops can be added!");
			}
		}
		Date appStart = stop.getAppointmentStartTime();
		Date appEnd = stop.getAppointmentEndTime();
		if (appStart == null)
			throw new IllegalArgumentException("Appointment start time are required!");
		else if (!systemUtil.isStartBeforeEnd(appStart, appEnd))
			throw new IllegalArgumentException("Incorrect date-time order!");

		Job job = stop.getRoadwayBill().getJob();
		if (job.getJobStatus().equals(SystemType.STATUS_JOB_CLOSE))
			throw new IllegalArgumentException("Job already closed!");
		else {
			Date jobStart = job.getStartDate();
			Date jobEnd = job.getEndDate();
			if (!systemUtil.isDateInBetween(appStart, jobStart, jobEnd)
					|| !systemUtil.isDateInBetween(appEnd, jobStart, jobEnd))
				throw new IllegalArgumentException("Incorrect date-time order!");
		}

		if (stop.getStopType() == null)
			throw new IllegalArgumentException("Stop type is required!");
		if (stop.getWarehouse() == null)
			throw new IllegalArgumentException("Warehouse is required!");
		else
			companyValidator.validateWarehouse(stop.getWarehouse());
		if (stop.getStopSequenceNumber() == null || stop.getStopSequenceNumber() <= 0)
			throw new IllegalArgumentException("Stop sequence number is required!");

	}

	@Override
	public void validateProduct(Product product) {
		if (product.getRoadwayBill() == null)
			throw new IllegalArgumentException("Roadway Bill is required!");
		else if (product.getRoadwayBill().getRwbStatus().equals(SystemType.STATUS_RWB_COMPLETED))
			throw new IllegalArgumentException("Roadway Bill is already completed!");
	}

	@Override
	public void validateIncome(Income income) {
		if (income.getRoadwayBill() == null)
			throw new IllegalArgumentException("Roadway Bill is required!");
		else if (income.getRoadwayBill().getRwbStatus().equals(SystemType.STATUS_RWB_COMPLETED))
			throw new IllegalArgumentException("Roadway Bill is already completed!");
		if (income.getIncomeType() == null)
			throw new IllegalArgumentException("Income type is required!");
		if (income.getAmount() == null || income.getAmount() < 0)
			throw new IllegalArgumentException("Income amount is required!");
	}

	@Override
	public void validateExpense(Expense expense) {
		if (expense.getRoadwayBill() == null)
			throw new IllegalArgumentException("Roadway Bill is required!");
		else if (expense.getRoadwayBill().getRwbStatus().equals(SystemType.STATUS_RWB_COMPLETED))
			throw new IllegalArgumentException("Roadway Bill is already completed!");
		if (expense.getExpenseType() == null)
			throw new IllegalArgumentException("Expense type is required!");
		if (expense.getAmount() == null || expense.getAmount() < 0)
			throw new IllegalArgumentException("Expense amount is required!");
	}

}
