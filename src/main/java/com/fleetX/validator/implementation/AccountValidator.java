package com.fleetX.validator.implementation;

import org.springframework.stereotype.Service;

import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.ReimbursementInfo;
import com.fleetX.validator.IAccountValidator;

@Service
public class AccountValidator implements IAccountValidator {

	@Override
	public void validateAdjustmentEntry(Adjustment adjustment) {
		if (adjustment.getAccountHead() == null)
			throw new IllegalArgumentException("Account head is required!");
		if (adjustment.getAccountType() == null)
			throw new IllegalArgumentException("Account type is required!");
		if (adjustment.getAmount() <= 0)
			throw new IllegalArgumentException("Invalid amount!");
		if (adjustment.getDate() == null)
			throw new IllegalArgumentException("Adjustment date is required!");
	}

	@Override
	public void validateReimbursementInfo(ReimbursementInfo reimbursementInfo) {
		if (reimbursementInfo.getReferenceNumber() == null)
			throw new IllegalArgumentException("Reference number is required!");
		if (reimbursementInfo.getReimbursementAmount() == null || reimbursementInfo.getReimbursementAmount() < 0)
			throw new IllegalArgumentException("Invalid reimbursement amount!");
		if (reimbursementInfo.getReimbursementDate() == null)
			throw new IllegalArgumentException("Reimbursement date is required!");
		if (reimbursementInfo.getJobs() == null || reimbursementInfo.getJobs().size() < 1)
			throw new IllegalArgumentException("Jobs are required!");
	}

	@Override
	public void validateInvoicePayment(InvoicePayment invoicePayment) {
		if (invoicePayment.getPaymentAmount() == null || invoicePayment.getPaymentAmount() <= 0)
			throw new IllegalArgumentException("Invalid payment amount!");
		if (invoicePayment.getPaymentDate() == null)
			throw new IllegalArgumentException("Payment date is required!");
		if (invoicePayment.getReferenceNumber() == null || invoicePayment.getReferenceNumber().isBlank())
			throw new IllegalArgumentException("Reference number is required!");
		if (invoicePayment.getInvoice() == null && invoicePayment.getDhInvoice() == null)
			throw new IllegalArgumentException("Invoice ID is required!");
	}

}
