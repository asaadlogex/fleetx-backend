package com.fleetX.validator.implementation;

import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.ItemPurchaseDetail;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;
import com.fleetX.entity.maintenance.WorkOrderItemDetail;
import com.fleetX.validator.IInventoryValidator;

@Service
public class InventoryValidator implements IInventoryValidator {

	@Override
	public void validateItem(Item item) {
		if (item.getItemName() == null)
			throw new IllegalArgumentException("Item name is required!");
		if (item.getItemSku() == null)
			throw new IllegalArgumentException("Item SKU is required!");
		if (item.getItemType() == null)
			throw new IllegalArgumentException("Item type is required!");
		else if (item.getItemType().getValue().equals(SystemType.ITEM_TYPE_GOODS)) {
			if (item.getItemUnit() == null)
				throw new IllegalArgumentException("Item unit is required!");
			if (item.getItemCategory() == null)
				throw new IllegalArgumentException("Item category is required!");
			if (item.getItemBrand() == null)
				throw new IllegalArgumentException("Item brand is required!");
		}
	}

	@Override
	public void validatePurchaseOrder(PurchaseOrder purchaseOrder) {
		if (purchaseOrder.getVendor() == null)
			throw new IllegalArgumentException("Vendor is required!");
		if (purchaseOrder.getItemPurchaseDetail() == null || purchaseOrder.getItemPurchaseDetail().size() == 0)
			throw new IllegalArgumentException("Item purchase details are required!");
		else {
			for (ItemPurchaseDetail itemPurchaseDetail : purchaseOrder.getItemPurchaseDetail()) {
				validateItemPurchaseDetail(itemPurchaseDetail);
			}
		}
	}

	private void validateItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetail) {
		if (itemPurchaseDetail.getItem() == null)
			throw new IllegalArgumentException("Item is required!");
		if (itemPurchaseDetail.getQtyOrdered() == null || itemPurchaseDetail.getQtyOrdered() <= 0)
			throw new IllegalArgumentException("Quantity ordered is required!");
		if (itemPurchaseDetail.getRate() == null || itemPurchaseDetail.getRate() <= 0)
			throw new IllegalArgumentException("Item rate is required!");

	}

	@Override
	public void validateWorkOrder(WorkOrder workOrder) {
		if (workOrder.getTractor() == null)
			throw new IllegalArgumentException("Tractor is required!");
		if (workOrder.getTractorKm() == null)
			throw new IllegalArgumentException("Tractor KM is required!");
		if (workOrder.getWorkOrderDate() == null)
			throw new IllegalArgumentException("Work order date is required!");
		if (workOrder.getWorkOrderCategory() == null)
			throw new IllegalArgumentException("Work order category is required!");
		if (workOrder.getWorkOrderSubCategory() == null)
			throw new IllegalArgumentException("Work order sub category is required!");
		if (workOrder.getPriority() == null)
			throw new IllegalArgumentException("Work order priority is required!");
		if (workOrder.getWorkshop() == null)
			throw new IllegalArgumentException("Workshop is required!");
		if (workOrder.getWorkOrderItemDetail() == null || workOrder.getWorkOrderItemDetail().size() == 0)
			throw new IllegalArgumentException("Work order item details are required!");
		else {
			for (WorkOrderItemDetail workOrderItemDetail : workOrder.getWorkOrderItemDetail()) {
				validateWorkOrderItemDetail(workOrderItemDetail);
			}
		}

	}

	private void validateWorkOrderItemDetail(WorkOrderItemDetail workOrderItemDetail) {
		if (workOrderItemDetail.getItem() == null)
			throw new IllegalArgumentException("Item is required!");
		if (workOrderItemDetail.getQtyUsed() == null || workOrderItemDetail.getQtyUsed() <= 0)
			throw new IllegalArgumentException("Quantity used is required!");
		if (workOrderItemDetail.getRate() == null || workOrderItemDetail.getRate() <= 0)
			throw new IllegalArgumentException("Item rate is required!");
	}

	@Override
	public void validateInventoryAdjustment(InventoryAdjustment inventoryAdjustment) {
		if (inventoryAdjustment.getAdjustmentQty() == null || inventoryAdjustment.getAdjustmentQty() == 0)
			throw new IllegalArgumentException("Inventory adjustment quantity must not be zero!");
		if (inventoryAdjustment.getInventoryAdjustmentDate() == null)
			throw new IllegalArgumentException("Inventory adjustment date is required!");
		if (inventoryAdjustment.getItem() == null)
			throw new IllegalArgumentException("Inventory adjustment item is required!");
	}

}
