package com.fleetX.validator.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;
import com.fleetX.validator.IEmployeeValidator;
import com.fleetX.validator.IGeneralValidator;

@Service
public class EmployeeValidator implements IEmployeeValidator {
	@Autowired
	IGeneralValidator generalValidator;

	@Override
	public void validateEmployee(Employee employee) {
		if (employee.getAdminCompany() == null)
			throw new IllegalArgumentException();
		if (employee.getAcAddress() == null)
			throw new IllegalArgumentException();
		if (employee.getEmployeeAddresses() == null || employee.getEmployeeAddresses().size() < 1)
			throw new IllegalArgumentException();
		else
			employee.getEmployeeAddresses().stream().forEach(x -> generalValidator.validateAddress(x));
		if (employee.getEmployeeContacts() == null || employee.getEmployeeContacts().size() < 1)
			throw new IllegalArgumentException();
		else
			employee.getEmployeeContacts().stream().forEach(x -> generalValidator.validateContact(x));
		if (employee.getEmployeeType() == null)
			throw new IllegalArgumentException();
		else if (employee.getEmployeeType().getValue().equals(SystemType.CONTRACTUAL) && employee.getSupplier() == null)
			throw new IllegalArgumentException();
	}

	@Override
	public void validateTrainingProfile(TrainingProfile trainingProfile) {
	}

	@Override
	public void validateMedicalTestReport(MedicalTestReport medicalTestReport) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateReference(Reference reference) {
		// TODO Auto-generated method stub

	}

}
