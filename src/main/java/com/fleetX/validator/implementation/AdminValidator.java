package com.fleetX.validator.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;
import com.fleetX.validator.IAdminValidator;
import com.fleetX.validator.IGeneralValidator;

@Service
public class AdminValidator implements IAdminValidator {
	@Autowired
	IGeneralValidator generalValidator;

	@Override
	public void validateAdminCompany(AdminCompany adminCompany) {
		List<Address> addresses = adminCompany.getAdminCompanyAddresses();
		List<Contact> contacts = adminCompany.getAdminCompanyContacts();
		if (addresses == null || addresses.size() < 1)
			throw new IllegalArgumentException("Atleast one admin company address required!");
		else
			addresses.stream().forEach(x -> generalValidator.validateAddress(x));
		if (contacts == null || contacts.size() < 1)
			throw new IllegalArgumentException("Atleast one admin company contact required!");
		else
			contacts.stream().forEach(x -> generalValidator.validateContact(x));
		if (adminCompany.getAdminCompanyFormation() == null)
			throw new IllegalArgumentException("Admin company formation required!");
		if (adminCompany.getAdminCompanyName() == null)
			throw new IllegalArgumentException("Admin company name required!");
		if (adminCompany.getBusinessType() == null)
			throw new IllegalArgumentException("Admin company business type required!");
	}

	@Override
	public void validateFuelCard(FuelCard fuelCard) {
		if (fuelCard.getAdminCompany() == null && fuelCard.getCustomer() == null)
			throw new IllegalArgumentException();
		if (fuelCard.getFuelCardNumber() == null)
			throw new IllegalArgumentException("Fuel card number required!");
	}

	@Override
	public void validateRoute(Route route) {
		if (route.getAvgDistanceInKM() == null)
			throw new IllegalArgumentException("Route's average distance (in KM) required!");
	}

}
