package com.fleetX.validator.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fleetX.config.SystemType;
import com.fleetX.config.SystemUtil;
import com.fleetX.entity.Address;
import com.fleetX.entity.Contact;
import com.fleetX.entity.Role;
import com.fleetX.entity.User;
import com.fleetX.entity.dropdown.DCity;
import com.fleetX.validator.IGeneralValidator;

@Service
public class GeneralValidator implements IGeneralValidator {
	@Autowired
	SystemUtil systemUtil;

	@Override
	public void validateAddress(Address address) {
		if ((address.getStreet1() == null || address.getStreet1().isBlank())
				&& (address.getStreet2() == null || address.getStreet2().isBlank()))
			throw new IllegalArgumentException("Street-1 or Street-2 is required!");
		if (address.getCountry() == null || address.getCountry().isBlank())
			throw new IllegalArgumentException("Country name is required!");
		if (address.getState() == null)
			throw new IllegalArgumentException("State is required!");
		if (address.getCity() == null)
			throw new IllegalArgumentException("City is required!");
	}

	@Override
	public void validateContact(Contact contact) {
		if (contact.getChannel() == null)
			throw new IllegalArgumentException("Contact channel is required!");
		if (contact.getData() == null)
			throw new IllegalArgumentException("Contact data is required!");
		String channel = contact.getChannel().getValue();
		String data = contact.getData();
		switch (channel) {
		case SystemType.CHANNEL_PHONE:
			if (!systemUtil.validatePhone(data))
				throw new IllegalArgumentException("Invalid phone number!");
			break;
		case SystemType.CHANNEL_EMAIL:
			if (!systemUtil.validateEmail(data))
				throw new IllegalArgumentException("Invalid e-mail address!");
			break;
		case SystemType.CHANNEL_MOBILE:
			if (!systemUtil.validateMobile(data))
				throw new IllegalArgumentException("Invalid mobile number!");
			break;
		default:
			throw new IllegalArgumentException();
		}
	}

	@Override
	public void validateRole(Role role) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateCity(DCity city) {
		if (city.getCode() == null || city.getValue() == null)
			throw new IllegalArgumentException("City code and value are required!");
		if (city.getState() == null)
			throw new IllegalArgumentException("State info is required!");
	}

	@Override
	public void validateArea(DCity city) {
		if (city.getCode() == null || city.getValue() == null)
			throw new IllegalArgumentException("Area code and value are required!");
	}

}
