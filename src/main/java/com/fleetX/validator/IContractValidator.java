package com.fleetX.validator;

import com.fleetX.entity.company.CompanyContract;
import com.fleetX.entity.company.FuelRate;
import com.fleetX.entity.company.FuelSupplierContract;
import com.fleetX.entity.company.RouteRate;
import com.fleetX.entity.company.RouteRateContract;
import com.fleetX.entity.employee.EmployeeContract;

public interface IContractValidator {
	public void validateCompanyContract(CompanyContract companyContract);
	public void validateEmployeeContract(EmployeeContract employeeContract);
	public void validateRouteRateContract(RouteRateContract routeRateContract);
	public void validateRouteRate(RouteRate routeRate,CompanyContract companyContract);
	public void validateFuelSupplierContract(FuelSupplierContract fuelSupplierContract);
	public void validateFuelRate(FuelRate fuelRate);
}
