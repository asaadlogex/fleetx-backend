package com.fleetX.validator;

import com.fleetX.entity.account.Adjustment;
import com.fleetX.entity.account.InvoicePayment;
import com.fleetX.entity.account.ReimbursementInfo;

public interface IAccountValidator {
	public void validateAdjustmentEntry(Adjustment adjustment);

	public void validateReimbursementInfo(ReimbursementInfo reimbursementInfo);

	public void validateInvoicePayment(InvoicePayment invoicePayment);
}
