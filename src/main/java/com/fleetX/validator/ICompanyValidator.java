package com.fleetX.validator;

import com.fleetX.entity.company.Company;
import com.fleetX.entity.order.Warehouse;

public interface ICompanyValidator {
	public void validateCompany(Company company);
	public void validateWarehouse(Warehouse warehouse);
}
