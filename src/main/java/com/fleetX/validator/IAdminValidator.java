package com.fleetX.validator;

import com.fleetX.entity.company.AdminCompany;
import com.fleetX.entity.company.FuelCard;
import com.fleetX.entity.company.Route;

public interface IAdminValidator {
	public void validateAdminCompany(AdminCompany adminCompany);
	public void validateFuelCard(FuelCard fuelCard);
	public void validateRoute(Route route);
}
