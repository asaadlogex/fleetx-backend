package com.fleetX.validator;

import com.fleetX.entity.maintenance.InventoryAdjustment;
import com.fleetX.entity.maintenance.Item;
import com.fleetX.entity.maintenance.PurchaseOrder;
import com.fleetX.entity.maintenance.WorkOrder;

public interface IInventoryValidator {
	public void validateItem(Item item);
	public void validatePurchaseOrder(PurchaseOrder purchaseOrder);
	public void validateWorkOrder(WorkOrder workOrder);
	public void validateInventoryAdjustment(InventoryAdjustment inventoryAdjustment);
}
