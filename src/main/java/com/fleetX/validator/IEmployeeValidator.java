package com.fleetX.validator;

import com.fleetX.entity.employee.Employee;
import com.fleetX.entity.employee.HeavyVehicleExperience;
import com.fleetX.entity.employee.MedicalTestReport;
import com.fleetX.entity.employee.Reference;
import com.fleetX.entity.employee.TrainingProfile;

public interface IEmployeeValidator {
	public void validateEmployee(Employee employee);
	public void validateTrainingProfile(TrainingProfile trainingProfile);
	public void validateMedicalTestReport(MedicalTestReport medicalTestReport);
	public void validateHeavyVehicleExperience(HeavyVehicleExperience heavyVehicleExperience);
	public void validateReference(Reference reference);
		
}
